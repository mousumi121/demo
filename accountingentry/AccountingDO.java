/**
 * Copyright  2006   Intellect Design Arena Ltd. All rights reserved.
 *
 *    These materials are confidential and proprietary to Intellect Design Arena Ltd.
 *  and no part of these materials should be reproduced, published, transmitted or
 *  distributed in any form or by any means, electronic, mechanical, photocopying,
 *  recording or otherwise, or stored in any information storage or retrieval system
 *  of any nature nor should the materials be disclosed to third parties or used in
     *  any other manner for which this is not authorized, without the prior express
 *  written authorization of Intellect Design Arena Ltd.
 *
 * <p>Title       : Intellect Liquidity</p>
 * <p>Description : This class is the Data Object Class for Account details </p>
 * <p>SCF NO      : SCF-POOL-001  </p>
 * <p>Copyright   : Copyright 2016 Intellect Design Arena Limited. All rights reserved.</p>
 * <p>Company     : Intellect Design Arena Limited</p>
 * <p>Date of Creation : 21-Feb-2006</p>
 * <p>Source      : AccountingDO.java </p>
 * <p>Package     : com.intellectdesign.cash.accountingentry </p>
 * @author Ritesh Grover
 * @version 1.0
 * <p>------------------------------------------------------------------------------------</p>
 * <p>MODIFICATION HISTORY:</p>
 * <p>------------------------------------------------------------------------------------</p>
 * <p>SERIAL      AUTHOR		    DATE		SCF	DESCRIPTION</p>
 * <p>------------------------------------------------------------------------------------</p>
 * <p>1           Ritesh Grover    21-Feb-2006	POOL-001	Initial Version.</p>
 * <p>2           Gopal K		   12-Apr-2006	ENH_1110	Added isExpiringToday to identify whether participant is ending today or not</p>
 */
package com.intellectdesign.cash.accountingentry;

import com.polaris.intellect.commons.logging.Log;
import com.polaris.intellect.commons.logging.LogFactory;

public class AccountingDO {

	private static Log mLog =  LogFactory.getLog(AccountingDO.class.getName());

	/**
	 * Default Constructor
	 */
	public AccountingDO(){
	//Do nothing	
	}
	
	private String _cod_ccy = null;
	private String _nbr_coreaccount = null;
	private String _cod_branch = null;
	private String _cod_gl = null;
	private String _cod_bnk_lgl = null;
	private String _nbr_ibanacct = null;
	private String _nbr_ownaccount = null;
	
	private String crBeneficiaryAccount= null;
	private String drBeneficiaryAccount= null;
	
	//ENH_1110 Starts
	private boolean isExpiringToday = false;
	//ENH_1110 Ends
	/**
	 * @return Returns the _cod_bnk_lgl.
	 */
	public String get_cod_bnk_lgl() {
		return _cod_bnk_lgl;
	}
	/**
	 * @param _cod_bnk_lgl The _cod_bnk_lgl to set.
	 */
	public void set_cod_bnk_lgl(String _cod_bnk_lgl) {
		this._cod_bnk_lgl = _cod_bnk_lgl;
	}
	/**
	 * @return Returns the _cod_branch.
	 */
	public String get_cod_branch() {
		return _cod_branch;
	}
	/**
	 * @param _cod_branch The _cod_branch to set.
	 */
	public void set_cod_branch(String _cod_branch) {
		this._cod_branch = _cod_branch;
	}
	/**
	 * @return Returns the _cod_gl.
	 */
	public String get_cod_gl() {
		return _cod_gl;
	}
	/**
	 * @param _cod_gl The _cod_gl to set.
	 */
	public void set_cod_gl(String _cod_gl) {
		this._cod_gl = _cod_gl;
	}
	/**
	 * @return Returns the _cod_party_ccy.
	 */
	public String get_cod_ccy() {
		return _cod_ccy;
	}
	/**
	 * @param _cod_party_ccy The _cod_party_ccy to set.
	 */
	public void set_cod_ccy(String _cod_party_ccy) {
		this._cod_ccy = _cod_party_ccy;
	}
	/**
	 * @return Returns the _nbr_coreaccount.
	 */
	public String get_nbr_coreaccount() {
		return _nbr_coreaccount;
	}
	/**
	 * @param _nbr_coreaccount The _nbr_coreaccount to set.
	 */
	public void set_nbr_coreaccount(String _nbr_coreaccount) {
		this._nbr_coreaccount = _nbr_coreaccount;
	}
	/**
	 * @return Returns the _nbr_ibanacct.
	 */
	public String get_nbr_ibanacct() {
		return _nbr_ibanacct;
	}
	/**
	 * @param _nbr_ibanacct The _nbr_ibanacct to set.
	 */
	public void set_nbr_ibanacct(String _nbr_ibanacct) {
		this._nbr_ibanacct = _nbr_ibanacct;
	}
	/**
	 * @return Returns the _nbr_ownaccount.
	 */
	public String get_nbr_ownaccount() {
		return _nbr_ownaccount;
	}
	/**
	 * @param _nbr_ownaccount The _nbr_ownaccount to set.
	 */
	public void set_nbr_ownaccount(String _nbr_ownaccount) {
		this._nbr_ownaccount = _nbr_ownaccount;
	}

	public String toString() {
		return ( 
			new StringBuffer("[")
			.append("Currency Code='"+_cod_ccy+"'")
			.append(",Core Account='"+_nbr_coreaccount+"'")
			.append(",Branch Code='"+_cod_branch+"'")
			.append(",GL Code='"+_cod_gl+"'")
			.append(",BankLedger Code='"+_cod_bnk_lgl+"'")
			.append(",IBAN Accnt No='"+_nbr_ibanacct+"'")
			.append(",NBR Accnt No='"+_nbr_ownaccount+"'")
			.append("]")
		).toString();
	}
	/**
	 * @return
	 */
	public String getCrBeneficiaryAccount() {
		return crBeneficiaryAccount;
	}

	/**
	 * @return
	 */
	public String getDrBeneficiaryAccount() {
		return drBeneficiaryAccount;
	}

	/**
	 * @param string
	 */
	public void setCrBeneficiaryAccount(String string) {
		crBeneficiaryAccount = string;
	}

	/**
	 * @param string
	 */
	public void setDrBeneficiaryAccount(String string) {
		drBeneficiaryAccount = string;
	}
	//ENH_1110 Starts
	/**
	 * Indicates whether participant is ending today or not. 
	 * @return
	 */
    public boolean isExpiringToday() {
        return isExpiringToday;
    }
    /**
     * Sets whether participant is ending today or not.
     * @param isExpiringToday
     */
    public void setExpiringToday(boolean isExpiringToday) {
        this.isExpiringToday = isExpiringToday;
    }
    // ENH_1110 Ends

}
