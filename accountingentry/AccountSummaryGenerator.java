/**
 * *    These materials are confidential and proprietary to Intellect Design Arena Ltd.
 *  and no part of these materials should be reproduced, published, transmitted or
 *  distributed in any form or by any means, electronic, mechanical, photocopying,
 *  recording or otherwise, or stored in any information storage or retrieval system
 *  of any nature nor should the materials be disclosed to third parties or used in
 *  any other manner for which this is not authorized, without the prior express
 *  written authorization of Intellect Design Arena Ltd.
 *
 * <p>Title       : Intellect Liquidity</p>
 * <p>Description : This class geneartes the the Account Summary Information and populates
 * 					 OPOOL_ACCOUNTDTLS,OPOOL_ACCOUNTSUMMARY AND OPOOL_POSTINGSUMMARY table </p>
 * <p>Copyright   : Copyright 2016 Intellect Design Arena Limited. All rights reserved.</p>
 * <p>Company     : Intellect Design Arena Limited</p>
 * <p>Date of Creation : </p>
 * <p>Source      : AccountSummaryGenerator.java </p>
 * <p>Package     : com.intellectdesign.cash.accountingentry  </p>
 * @author 
 * @version 1.0
 * <p>------------------------------------------------------------------------------------</p>
 * <p>MODIFICATION HISTORY:</p>
 * <p>------------------------------------------------------------------------------------</p>
 * <p>SERIAL      AUTHOR		    DATE					SCF						DESCRIPTION</p>
 * <p>------------------------------------------------------------------------------------</p>
 * <p>1           Devendra Desai    11/06/2007 		CCB_ENH_ IS687 		Initial Version.</p>
 * <p>2				Amey Kulkarni		16/07/2007		ENH_LIQ_0005			Liquidity Execution - ORBICASH_GLMST changed to OLM_GLMST</p>
 * <p>3				Milind Patil		22/11/2007		SUP_IS965				Sun Performance Tuning changes</p>
 * <p>4           Devendra Desai    04/09/2007  SUP_IS764  When participant ends, all the unposted amount for different transaction codes needs to be given to the participant and also round up the amount to be posted then so that no further carryfwd is posted	      
 * <p>5           Devendra Desai    09/10/2007  SUP_IS800  Also CHECK IF DAT_FINAL_SETTLEMENT has changed. 
 * <p>6           Malini Shetty     22/11/2007  SUP_IS964  When participant ends, all the unposted amount for different transaction codes needs to be given to the participant and also round up the amount to be posted then so that no further carryfwd is accrued
 * <p>7			Prasad Narra 		30/11/2007	SUP_389		When participant ends, then date final settlement is added to no of days delay as configarable. 
 * <p>8           Shantanu Kanungo 30/11/2007  SUP_HSBC_394  Not updating the date accrual in Opool_accrualsummery in case of settlement date is changed.
 * <p>9           Devendra Desai 18/12/2007  ENH_IS997  Retrofit includes SUP_IS964,CCB_SUP_IS760,SUP_HSBC_394
 * <p>10           Monika Sharma	   06/02/2008  ENH_IS1059   Corrected Error Codes when throwing LMSException.
 * <p>11		  Apurva Sharma    18/03/2008  ENH_IS1167	For participants ending ,value date=end date + ENDDATE_DELAY (from the executionconstants.properties). 
 * <p>12		Milind Patil		17/03/2008	ENH_IS1166		Transaction code enhancement for IEF model</P>
 * <p>13		  Apurva Sharma     20/03/2008  SUP_IS1192	For participants ending ,Posting Start Date should be start of the latest posting cycle for the remaining unposted amounts  
 * <p>14			Monika Sharma	19/06/2008	SUP_IS957		Update FinalsettlementDate for account irrespective of their Credit/Debit position for same Posting Date </p>
 * <p>15		Milind Patil		27/08/2008			ENH_IS1297			StandAlone BVT Interest Calculation Changes</p>
 * <p>16		Dibu Joy		    19/09/2008			  		           Intercompany Changes for Unposted Amt</p>
 * <p>17		  Rohit Mankar		22/09/2008		ENH_IS1297			Changes Made For INTEREST POSTING CYCLE</p>
 * <p>18		  Dibu Joy		    13/10/2008					        Intercompany changes for unposted Amt</p>
 * <p>19		Milind Patil		15/10/2008		ENH_IS1297			External BVT Changes.</p>
 * <p>20        C.Sathiyaseelan  		01/10/2008		ENH_IS1297		BVT Product Rate Change
 * <p>21        Dibu Joy  		     11/11/2008		ENH_IS1297		BVT Intercompany
 * <p>22        Dibu Joy  		     01/12/2008		ENH_IS1297		BVT Intercompany
 * <p>23		Aniruddh singh       22-Dec-2008	SUP_IS1587          BVT Posting cycle changes			 </p>
 * <p>24        Apurva Sharma 		 03/01/2008		NT_UT_BUFIX_03_Jan_09	Party End is not working   </p>
 * <p>25        Apurva Sharma 		 03/01/2008		NT_UT_BUFIX_09_Jan_09	Party End is not working-Transaction code is not proper   </p>
 * <p>26		Nagamani 	          08/01/2009	SUP_IS1612		ConnectionLeakage fixing </p>
 * <p>27		Aniruddh singh       15-Jan-2009	          		For Connection leak issue			 </p>
 * <p>28		Rushikesh Potdar   16-Jan-2009	          			Commented HoldUnhold code to create Baseline on 10_2			 </p>
 * <p>29        Apurva Sharma 		 18/02/2009		NT_UT_BUFIX_18_Feb_09	System should accept a 'hold' reqeuest to be effective from the start date of the pool.</p>
 * <p>30        Sadanand Borkar	     21/02/2009						Changes for Calculation of Interest using Loan Details</p>
 * <p>31        Sadanand Borkar	     05/04/2009						Retro : BV Interest Changes </p>
 * <p>32        Sadanand Borkar	     10/04/2009						BVT Changes </p>
 * <p>33        Jitendra Behera	     16/04/2009		ENH_IS1298		BVT Changes - Ammount carryforward to be considered - getCacheUnpostedAmount()</p>
 * <p>34        Dibu Joy  		     14/04/2009		SUP_IS1845		modified popuplatePostingsummary method for Flg_samevalDate</p>
 * <p>35        Manoranjan  Das	     07/05/2009		ENH_IS1913		Pooling Retro</p>
 * <p>36		Milind Patil		 26/05/2009			ENH_IS1731 	Exec audit trailing changes
 * <p>37		Gargi Kekre		 	21/08/2009			ENH_2521   	Retro Activity
 * <p>38		Gargi Kekre	    	14/09/2009	ENH_2521   	Retro Activity of JPM LIQ_RELEASE9.10_IUT013_27_9.
 * <p>39		Devendra Desai	    19/01/2010	 SIT2DEV Retro -DD    	Retro Activity 
 * <p>40		12/01/2010         	Sampada     Retro from9.8
 * <p>41		Sandeep M.			09/03/2010	SUP_IS2378		update final sett/posting date only for posting date>business date retro
 * <p>42		Sandeep M.			09/03/2010	SUP_IS2925		changes done for account registration failure> retro
 * <p>43    	Rajwinder Singh	    01/09/2010	ENH_10.3_154		BVT Cycle related changes</p>
 * <p>44        Rajesh.M            01/10/2010     ENH_10.3_153 Interest Type wise allocation changes
 * <p>45        Sampada Ranaware        15/10/2010     			Account Type Linkage
 * <p>46		Rajesh Kumar			22/10/2010	HSBC_BUG_FIX	Posting amount was not proper
 * <p>47        Ravikumar Maladi        04/01/2011  Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005	
 * <p>48        Sampada Ranaware        07/01/2011     			Retro from JPMC LIQ_REL_PROD_9.11_016_E - SIT  Unique constraint error comes in Opool_accountdtls in case of Reallocation and CENTRAL model
 * <p>49       	Sandeep M.		  01/02/2011		JTest changes		JTest JDBC changes</p>
 * <p>50        Sampada Ranaware        11/03/2011     		Issue fix - Do not generate Zero Bankshare entries

 * <p>51        Sandeep Mane            28/03/2011          Issue No 54 Fix
 * <p>50        Sampada Ranaware        11/03/2011     		SIT issue fix starts - txn.ID_PARENT_POOL added 	Do not generate accrual entry of subpools if cod_counter_account is 'S' at subproduct level.
 * <p>51        Sandeep Mane	       02/02/2011     		JPMC RETRO ISSUE 	Transaction Code query corrected
 * <p>52       	Sanket Joshi		   20/05/2011			Sanity_V10.5   	changes done to calculate sum of calculated amount for opool accountdtls </p>
 * <p>53       	Sanket Joshi		   26/05/2011			Sanity_V10.5   	changes done for allocatedamount when  FLG_INTACCPOST_INTTYP  is set to 'N'  </p>
 * <p>53       	Sandeep Mane		   16/07/2011			NED_ISSUE FIXED   	In Table OPOOL_POSTINGSUMMARY_BVT, wrong entry is going in the column DAT_POSTING_START  </p>
 * <p>54		Nitin W 			   09-Aug-2011			Issue fix ned 	Back value adjustments for previous cycle should be able to capatilize during intra-day and current cycle adjustments should be capatilized as part of EOD <p>
 * <p>55        Atima Ashesh		   12/08/2011  Ned_issue_70 entries in opool_postingsummary_bvt for "POSTING_GEN" was getting updated for 2nd bvt on same account for same period. It should generate one more row with "POSTING_GEN" <p>
 * <p>56       	Shivraj M		   	   16/08/2011			MashReq_ISSUE FIXED   	In cod_txn interestType is appended even though cod_interest_type flag is no in executionConstants.property file  </p>
 * <p>57       	Sandeep M		   	   17/08/2011			TI_FEE_CHANGES   	As per the interest concern, Liquidity populates adjustments in BVT tables (opool_txnpool_bvtdiff  & opool_txnpool_bvtdiff _int). But in case turn interest as Fee this functionality is missing.  </p>
 * <P>58        Nitin W				   31-Aug-2011          StandAlone Issue fix   in  case of interest Type wise bvt TYPE interest need to be consider </P>
 * <P>59        Sanket Joshi		   04-Nov-2011         SIT_ISSUE_BVT   Changes done statement parmeter is not get properly set when  flg_bvt_cycle 'Y'
 * <p>58       	Sandeep M		   	   17/02/2012			ANZ_UAT_ISSUE FIXED   	If seleceted thep articipant posting interest as None. In this scenario , Counter account value is null issue fixed  </p>
 * <p>59  		Asit Sharma       	   13/03/2012  			FindBug issue fix on 13MARCH2012</p>
 * <p>60		14/03/2012          Harikesh Singh		    FindBug issue fix
 * <p>61       	Sandeep M		   	   07/12/2011			MASHREQ_BVT_ISSUE   	Issue fixed for bvt getting failed  </p>
 * <p>62       	Sandeep M		   	   22/12/2011			SA Interest end ned issue   	Stand alone account capitalization happened on end date of interest rate attached. </p>
 * <p>63       	Sandeep M		   	   19/01/2012			SA_BVT_POSTING_CYCLE   	BVT Cycle related changes for SA</p>
 * <p>64	    Vijay Kadam		       18/04/2012			Ned_Retro  </p>
 * <p>60   Prateek Aole			11/05/2012		 		        		JTest Fixes</p>
 * <p>61    kalpana Manjare			15/10/2012		 		Retro_Mashreq        		Retro from Mashreq LIQ_RELEASE10.5_SIT014.12.17
 * <p>62    Maulik Trivedi			15/10/2012		 		Liquidity Volume Testing Optimized_query       		NEDBANK VOLUME TESTING OPTIMIZATION
 *<P>60         Nitin W				   26-May-2012			FLG_POSTCYCLE  check the flg for immediate accural and Posting for the Pool
 * <P>61        Nitin 				   01-Jun-2012          JPMC_HOOK
 * <p>62       	Sandeep M		   	   17/02/2012			ANZ_UAT_ISSUE FIXED   	If seleceted thep articipant posting interest as None. In this scenario , Counter account value is null issue fixed  </p>
 * <p>62       	Sanket  J		   	   31/08/2012			BVT_ISSSUE_01  	Changes done for cross cycle bvt issue</p>
 * <p>63       	Atima Ashesh	   	   05/09/2012			ANZ_SIT_ISSUE  	 opool_txnpoolhdr table shows the txt status as "Allocated" for JP Host
 * <p>64 		Nitin W					10-Sep-2012		NEW_ACCOUNTING_ENTRY_FORMAT   New accounting format according the allocation source pool ID </p>
 * <p>65       	Sandeep M		   	   05/10/2012			JPMC_IS FIXED   	Follow posting cycle related changes for pooling   </p>
 * <p>66       	Sandeep M		   	   20/11/2012			JPMC_SUP 16387 FIXED   	Changes for BVT Accrual failed due to BVT posted on account which has already ended in the Pool.   </p>
 * <p>66       	Sandeep M		   	   25/12/2012			ANZ_IS FIXED   	java.sql.SQLException: Invalid column index issue fixed </p>
 * <p>66       Sanket Joshi			   29-Dec-2012     ANZ_ISSUE_5233   primary key issue on accrualsummary table for SA BVT  
 * <p>67     Sanket Joshi			02/01/2013		ANZ_BVI_ISSUE_1		changes done ICL BVI issue  
 * <p>66		Abhishek Sawant 30-11-2012			Carry Forward changes - Added previous day carry forward to raw pool advantage or interest
 * <p>68	Vipul Depal			13-Dec-2012		ENH_BVT_PARTY_DEL 	BVT Deletion changes added </p>
 * <p>69	Sandeep Mane			21-jan-2013		JPMC_312 ISSUE FIXED 	Unique Constraint error in opool_accrualsummary - jpmc issue fixed </p>
 * <p>69    Nishant Sapdhare    28-Feb-2013	    ENH_ICLTAX 		     Stamp duty tax added on agreement level and Business tax on inco level screens.
 * <p>70    Manorath Dalvi		24-MAY-2013							 Interest and Advantage Seperate Settlement Dates For JPMC
 * <p>68	Sandeep Mane			21-jan-2013		JPMC_312 ISSUE FIXED 	Unique Constraint error in opool_accrualsummary - jpmc issue fixed </p>
 * <p>69     HArikesh Singh         22-MAR-2013     SUP_ANZ_4110             BVT cycle issue
 * <p>70     Sanket Joshi           06-MAY-2013     SUP_ANZ_NULLPOINTER     Nullponter exception found during execution
 * <p>70     Sandeep Mane         	10-May-2013     ANZ_UAT_IS4818           When we execute multiple BVTs with same value date then the second BVT is not getting processed. It is giving primary key exception.
 * <P>71    Nitin W				11-May-2013		ANZ_Defect#4879	dat_posting_start required in case of FLAG_BVT_CYCLE - Y only<P>
 * <p>72   Sandeep M.			26-JUNE-2013		SUP_JPMCIS17438	Incorrect BV adjustment amount is compounded resulting in incorrect cross-cycle Post BV figures issue fixed
 * <P>73   Sandeep M				02-Aug-2013		JPMC_BVT_ISSUE	Prior Cycle - if BVT fired after [Posting Date + Settlement days]<P>
 * <p>72    Raghruamireddy.G    30/08/2013          Retro from JPMC_ LIQUIDITY_11.4.1_BUGFIX JPMC_POOL_ENDDATE_DELAY to 11.5 

 * <p>71    Rajesh	    		07-June-2013	JPMC_620 		     ACCOUNT_ACCRUAL added check in processCycleWiseBvtTXNCodesForPool().
* <P>74   Sandeep M.            13-Aug-2013		JPMC_POOL_ENDDATE_DELAY     adding delay for participant <P>
* <p>75   Sandeep M				31-Dec-2013		Reallocate to Self  changes </p>
* <p>76   Sandeep M				21-Jan-2014		LS114_21326_21336 </p> 
* <p>77   Subramanyam Nakka    05/02/2014       11.6_Retro_Cycle_3
* <p>78   Amit Kumar 			13/02/2014   	JTest Changes 11.5.1   JTest Fixes</p>
* <p>78 Raghurami Reddy.G       25/05/2014      ANZ_DEMO_Simulation invalid index issue fix
* <p>79  Anita Suhanda			11/06/2014		RBS-6518
* <p>80   Anita Suhanda			13/06/2014		RBS-6545
* <p>90   Shraddha sawant 		27 jun 2014		BOI 616 fix tax was not getting calculated for loan hence not deducting from net interest .
* <p>91	  Shraddha Sawant		16 Oct 2014		RBS_201 fix for maturity repayment process
*<p>92    Sachchidanand         31/10/2014      RBS_236 settlement date incorrectly updating in case of non working day
*<P>93    Sachchidanand         12/11/2014      RBS_Internal_fix INCO is not required
* <p>94	  Santosh Barada		14 Nov 2014		JPMC_PERFORMANCE_ISSUE_FIX</p>
* <p>95	  MEgha Maheshwari		19 feb 2015		Jtest 11.7 fix</p>
* <p>96	  Nitin W				14/11/2014		SA_POSTING_BVT_CHANGE	 In case of Consolidate  posting cycle , entry should not populate in posting_bvt table 	
* <p>97   Abhishek Sawant      19-01-2015		HSBC_IN21607119     Fix for BVT Failure for AU1HUB Account
* <p>98    Sandeep M.              07-Jul-2014  15.1_fix_defect_346 
* 06/08/2015		Joshi Manish 			SUP_ISS_III_7305		Inter Company BVT CR
* <p>		20/08/2015			Dipankar Biswas		15.1.1_Resource_Leaks_Fix  Fix</p>
* <p>100  Bibek Satapathy 	 	  	09/09/2015  ENH_15.2_LOG_FIXES   Logger enabling Check Added </p> 
* <p>96    Sandeep M		21-Jul-2015 		SUP_ISS_IV_7296				max cursor issue fixed	</p>
* <p>97    Sandeep M(Retro by Deepak D)	  06-May-2016		SUP_ISS_III_7718 	invalide index issue fixed </p>
* <p>98	   Sandeep M(Retro By Deepak D)	  09-May-2016		SUP_ISS_IV_7602 	Incorrect bvt calculation</p>
* <p>98	   Sandeep M.[Retro by Ajay kr]	  20/01/2016		SUP_ISS_II_7461 invalide index issue fixed	
* <p>98   Sandeep M.           23/04/2016      SUP_ISS_V_7808    Calculated Amount was incorrect
* <p>99    Amit Kumar Jha                 04/08/2016          iRelease_issueNo_61 </p>
* <p>100   Apurva Goyani          		  03-Oct-2016        ISSUE_2416  Fixes </p>
*<p>101   Rahul Yerande		05-Apr-2016 		LMS_45_PERF_CERTIFICATION_PF24				Performance Changes	</p>
*<p>101   Rahul Yerande		22-Apr-2016 		LMS_45_PERF_CERTIFICATION_PF38				Performance Changes	</p>
*<p>101   Rahul Yerande		22-Apr-2016 		LMS_45_PERF_CERTIFICATION_PF103			Performance Changes	</p>
*<0>100   Sandeep M.      	26-Jan-2015     SUP_ISS_III_7774	issue where it fails to update the second lot status of the BVT transactions  if the lot size exceeds than 100
*<p>102   DeekshaP          03-Oct-2016       Issue_2408, ISSUE_2416, ISSUE_2407 , ISSUE_2428 , ISSUE_2452 & ISSUE_2495 Fixes </p>
*<p>103   Sandeep M(Apurva Goyani )	  30/11/2016		I_REL_231	Unique Constant error issue fixed for periodic interest type	 </p>
*<p>104  Sandeep M.           27/07/2016      SUP_ISS_II_7834    SA trasaction code changes </p>
*<p>105   Deepak Dewangan     06/01/2017      SUP_ISS_XI_8009   		Retro - Error while saving simulation pool. </p> 
*<p>106   Sandeep M(Apurva Goyani )	     12/04/2017      SUP_ISS_XX_8097   		Retro - Pooling batch is getting halted when Reallocation pool gets closed with end date.. </p> 
*<p>106   Sandeep Mane     20/02/2017         SUP_ISS_I_1709  A non-numeric character was found where a numeric was expected
*<p>107	  Sandeep Mane	  25 Jun 2015		SUP_ISS_I_7247 invalid index issue fixed for pooling</p>
 * <p>108	    Sandeep M.		    28/02/2017		SUP_ENH_III_8106	 DCC changes </p>
*/      
package com.intellectdesign.cash.accountingentry;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;

import javax.naming.NamingException;

import com.orbitech.frameworks.mline.Constants;
import com.orbitech.frameworks.mline.DBUtils;
import com.orbitech.frameworks.mline.database.CachedRowSet;
import com.intellectdesign.cash.info.PerformanceUtil;
import com.intellectdesign.cash.lms.constants.LMSConstants;
import com.intellectdesign.cash.lms.execution.pooling.allocation.constant.AllocationConstants;
import com.intellectdesign.cash.lms.execution.pooling.constant.ExecutionConstants;
import com.intellectdesign.cash.lms.execution.pooling.statelessbean.holdunholdinfo.HoldUnholdProcessorSBSL;
import com.intellectdesign.cash.lms.execution.pooling.util.ExecutionUtility;
import com.intellectdesign.cash.lms.execution.pooling.util.IPoolInfo;
import com.intellectdesign.cash.lms.interco.constants.InterCoConstants;
import com.intellectdesign.cash.lms.tax.statelessbean.TaxHandlerSLSB;
import com.intellectdesign.cash.sweeps.util.LMSConnectionUtility;
import com.intellectdesign.cash.sweeps.util.LMSUtility;
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
import com.intellectdesign.cash.util.SQLErrorCodesLoader;
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
import com.intellectdesign.exception.LMSException;
import com.polaris.intellect.commons.logging.Log;
import com.polaris.intellect.commons.logging.LogFactory;
import com.intellectdesign.cash.util.Ejb3Locator;
//NT_UT_BUFIX_18_Feb_09 End  //ENH_IS1913 ends
/**
 * @author devendra.desai
 *
 */


public class AccountSummaryGenerator {
	private static Log logger = LogFactory.getLog(AccountSummaryGenerator.class.getName());
//ENH_IS997 starts
	//SUP_IS764 starts
	//boolean  entries[]={false,false,false,false,false,false,false,false,false};
	//ENH_10.3_154 Starts
	//boolean  entries[]={false,false,false,false,false,false,false,false,false,false,false};//For Process Unposted Amount one more entries has been added SUP_IS1192
	boolean  entries[]={false,false,false,false,false,false,false,false,false,false,false,false,false};//For separate BVT Adjustments one more entry added.//Reallocate to Self
	//ENH_10.3_154 Ends
	//SUP_IS764 ends
//ENH_IS997 Ends
	public static String NORMAL_INTEREST="NORMAL_INTEREST",
	NORMAL_ALLOC_REALLOC_REBATE="NORMAL_ALLOC_REALLOC_REBATE",
	BANKSHARE_TREASURYSHARE="BANKSHARE_TREASURYSHARE",NORMAL_ACCRUAL="NORMAL_ACCRUAL";
	BigDecimal ZERO = new BigDecimal(0);
	HashMap cacheAmtUnposted = new HashMap(),accrualDatesCache = new HashMap(),postingDatesCache = new HashMap(),postingDatesCacheCurr =new HashMap(), postingDatesCachePrev =new HashMap() ,cacheAmtUnpostedCentralCcy = new HashMap(); ;//Reallocate to Self
	//ENH_10.3_154 Starts
	HashMap cacheAmtUnpostedtemp = new HashMap();
	HashMap cacheAmtUnpostedCentralCcytemp = new HashMap();//Reallocate to Self
	
	//ENH_10.3_154 Ends
//ENH_IS997 starts
	
	//SUP_IS764 starts
	private ArrayList accPoolIdList = new ArrayList();
	//SUP_IS764 ends
//ENH_IS997 Ends
	public static Properties interestCalculatorSql = null;//ENH_IS_1297 BVT Changes
	public SimpleDateFormat REF_DATE_FORMAT = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");
	public AccountSummaryGenerator()
	{
	
	}


	/**
	 *  This method contains method call getCacheUnpostedAmount() and processTXNCodes()
	 * @param lotId
	 * @param businessDate
	 * @param client
	 * @param insertAccountDtls
	 * @param updateAccountDtls
	 * @param insertAccrualSummary
	 * @param insertPostingSummary
	 * @param updateAccrualSummary
	 * @param updatePostingSummary
	 * @param insertUnpostedAmt
	 * @param updateUnpostedAmt
	 * @param selectAccrualSummary
	 * @param selectPostingSummary
	 * @param selectAccountDtls
	 * @param updateAccountDtlsForDates
	 * @param updateAccuralSummaryForDates
	 * @param insertPosting 
	 * @return
	 * @throws LMSException
	 */
//ENH_IS997 starts
/*	public boolean[] generateAccountSummaryInfo(String lotId,java.util.Date businessDate, String client,
			PreparedStatement insertAccountDtls,PreparedStatement updateAccountDtls,PreparedStatement insertAccrualSummary,
			PreparedStatement insertPostingSummary,PreparedStatement updateAccrualSummary,
			PreparedStatement updatePostingSummary,PreparedStatement insertUnpostedAmt,
			PreparedStatement updateUnpostedAmt,PreparedStatement selectAccrualSummary,
			PreparedStatement selectPostingSummary,PreparedStatement selectAccountDtls,
			PreparedStatement updateAccountDtlsForDates,PreparedStatement updateAccuralSummaryForDates,
			PreparedStatement updatePostingSummaryForDates)throws LMSException */
	//ENH_10.3_154 Starts
	/*public boolean[] generateAccountSummaryInfo(long processId ,String lotId,java.util.Date businessDate, String client,//Added for performance benchmarking start
			PreparedStatement insertAccountDtls,PreparedStatement updateAccountDtls,PreparedStatement insertAccrualSummary,
			PreparedStatement insertPostingSummary,PreparedStatement updateAccrualSummary,
			PreparedStatement updatePostingSummary,PreparedStatement insertUnpostedAmt,
			PreparedStatement updateUnpostedAmt,PreparedStatement selectAccrualSummary,
			PreparedStatement selectPostingSummary,PreparedStatement selectAccountDtls,
			PreparedStatement updateAccountDtlsForDates,PreparedStatement updateAccuralSummaryForDates,
			PreparedStatement updatePostingSummaryForDates, PreparedStatement insertPosting, 
			*//** ****** JTEST FIX start ************  on 14DEC09*//*
			//PreparedStatement insertAccrual)throws LMSException
			PreparedStatement insertAccrual, Connection conn)throws LMSException
			*//** ****** JTEST FIX end **************  on 14DEC09*/
	public boolean[] generateAccountSummaryInfo(long processId ,String lotId,java.util.Date businessDate, String client,long simId,//Added for performance benchmarking start
			PreparedStatement insertAccountDtls,PreparedStatement updateAccountDtls,PreparedStatement insertAccrualSummary,
			PreparedStatement insertPostingSummary,PreparedStatement updateAccrualSummary,
			PreparedStatement updatePostingSummary,PreparedStatement insertUnpostedAmt,
			//PreparedStatement updateUnpostedAmt,PreparedStatement selectAccrualSummary,//Reallocate to Self
			PreparedStatement updateUnpostedAmt,PreparedStatement updateUnpostedAmtCentralCcy,PreparedStatement selectAccrualSummary,
			PreparedStatement selectPostingSummary,PreparedStatement selectAccountDtls,
			PreparedStatement updateAccountDtlsForDates,PreparedStatement updateAccuralSummaryForDates,
			PreparedStatement updatePostingSummaryForDates, PreparedStatement insertPosting, 
			PreparedStatement insertAccrual, PreparedStatement updateBVTAccrualSummaryForDates, 
			PreparedStatement updateBVTPostingSummaryForDates, Connection conn)throws LMSException
	//ENH_10.3_154 Ends
	{
//SIT2DEV Retro -DD start
				//SUP_IS_Connection_Leak starts
				//JTest changes on 01-feb-11 starts
				//Statement stmt=null;
				PreparedStatement stmt = null;
				//JTest changes on 01-feb-11 ends
				ResultSet rs = null;
				//SUP_IS_Connection_Leak ends
//SIT2DEV Retro -DD end
//ENH_IS997 Ends			
		if (logger.isDebugEnabled()){
			logger.debug("Entering");
		}
//SUP_IS2925 starts
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				String bussDate = formatter.format(businessDate);
//SUP_IS2925 ends
		try
		{
//SIT2DEV Retro -DD start
		//Retro From 9.8 starts				
				StringBuilder sbuf = new StringBuilder();
				
				//String nbrAcctUnposted=null;
				//String amtType=null;
			//	BigDecimal amtUnposted=ZERO;
				//JTest changes on 01-feb-11 starts
				//conn = LMSConnectionUtility.getConnection();
				//stmt = conn.createStatement();
				String poolid=null;
				String poolstatus=null;
				sbuf.setLength(0);
				//SUP_IS2925 starts
				//sbuf.append("SELECT DISTINCT FIELD1,FIELD7 FROM OLM_EXCEPTION_DATA WHERE FIELD7 IS NOT NULL");
				//sbuf.append("SELECT DISTINCT FIELD1,FIELD7 FROM OLM_EXCEPTION_DATA WHERE FIELD7 IS NOT NULL AND FIELD3 = TO_DATE('" + bussDate +"', 'DD-Mon-YYYY') AND field7 = 'true' ");//Add condition for Field7 Change as per HSBC
				//sbuf.append("SELECT DISTINCT FIELD1,FIELD7 FROM OLM_EXCEPTION_DATA WHERE FIELD7 IS NOT NULL AND FIELD3 = TO_DATE(?, 'DD-Mon-YYYY') AND field7 = ? ");
				sbuf.append("SELECT DISTINCT FIELD1,FIELD7 FROM OLM_EXCEPTION_DATA WHERE FIELD7 IS NOT NULL AND FIELD3 = ? AND field7 = ? ");//SUP_ISS_I_1709
				//SUP_IS2925 ends
				stmt = conn.prepareStatement(sbuf.toString());
				//rs=stmt.executeQuery(sbuf.toString());
				stmt.setString(1, bussDate);
				stmt.setString(2, "true");
				//JTest changes on 01-feb-11 ends
				rs=stmt.executeQuery();				
				//SUP_IS2925 starts
				HashMap pooldetails = new HashMap();
				while(rs.next()){
					 poolid =rs.getString("FIELD1");
					 poolstatus =rs.getString("FIELD7");
				if (logger.isDebugEnabled()){
					logger.debug("In Account summary Generater pool id ::"+poolid);
					logger.debug("In Account summary Generater ::"+poolstatus);
				}
				if(poolid!=null){
					pooldetails.put("ID_POOL",poolid );
					pooldetails.put("VALID",  poolstatus);
						pooldetails.put(poolid,poolstatus );
				}else{
					pooldetails.put("ID_POOL","0" );
					pooldetails.put("VALID", "false");	
						pooldetails.put("0","false" );
				 }
				}
				LMSConnectionUtility.close(stmt); //15.1.1_Resource_Leaks_Fix
//SUP_IS2925 ends
		if (logger.isDebugEnabled()){
			logger.debug("Entering");
			logger.debug("Entering value of tht flag :::::"+pooldetails.get("ID_POOL"));
			logger.debug("Entering value of tht flag :::::"+pooldetails.get("VALID"));
			
		}//SUP_IS1322
		//Retro From 9.8 Ends
//SIT2DEV Retro -DD end
			int eventId=(int)ExecutionConstants.POOLING_EOD;//SUP_IS1898
			//cache the unposted amount. 
		//Added for performance benchmarking start
			//getCacheUnpostedAmount(lotId);
			getCacheUnpostedAmount(processId,lotId);
			getCacheUnpostedAmountCentralCcy(processId,lotId);//Reallocate to Self
			//Liquidity Volume Testing Optimized_query starts
			//PERFORMANCE_CHANGE_PERF60
			/*
			sbuf.setLength(0);
			sbuf.append("insert into OPOOL_RP_GL(id_pool , cod_gl,nbr_lotid) ( SELECT id_pool, cod_gl  ,?")
			.append(" FROM opool_txnpoolhdr, (SELECT DISTINCT id_rootpool   FROM opool_txnpoolhdr where  nbr_lotid = ? and  nbr_processid = ? ) root  ")
			.append(" WHERE id_pool = root.id_rootpool AND nbr_lotid = ? )");
			
		
			stmt = conn.prepareStatement(sbuf.toString());
			stmt.setLong(1, Long.parseLong(lotId));
			stmt.setLong(2, Long.parseLong(lotId));
			stmt.setLong(3, processId);
			stmt.setLong(4, Long.parseLong(lotId));
			//JTest changes on 01-feb-11 ends
			int count = stmt.executeUpdate();
			if (logger.isDebugEnabled()){
				logger.debug(" OPOOL_RP_GL Query :"+ sbuf.toString());
				logger.debug(" OPOOL_RP_GL Count :"+count );
			}*/
			//PERFORMANCE_CHANGE_PERF60
		//Liquidity Volume Testing Optimized_query ends
			
//SIT2DEV Retro -DD start
			processTXNCodes(processId, lotId,businessDate, client,simId, pooldetails,insertAccountDtls, updateAccountDtls,
		//Added for performance benchmarking ends
					insertAccrualSummary, insertPostingSummary, updateAccrualSummary, updatePostingSummary,
					//insertUnpostedAmt,updateUnpostedAmt,selectAccrualSummary,selectPostingSummary,//Reallocate to Self
					insertUnpostedAmt,updateUnpostedAmt,updateUnpostedAmtCentralCcy,selectAccrualSummary,selectPostingSummary,
					selectAccountDtls,updateAccountDtlsForDates,updateAccuralSummaryForDates,
					//ENH_10.3_154 Starts
					//updatePostingSummaryForDates);
					updatePostingSummaryForDates, updateBVTAccrualSummaryForDates, updateBVTPostingSummaryForDates);
					//ENH_10.3_154 Ends
//SIT2DEV Retro -DD end
//ENH_IS997 starts
			//SUP_IS964 starts
			//ENH_IS1297 RETROFIT STARTS 
			//processUnpostedAmt(businessDate,updateUnpostedAmt,insertPosting);
			//processUnpostedAmt(businessDate,updateUnpostedAmt,insertPosting,insertAccrual);
			//processUnpostedAmt(businessDate,updateUnpostedAmt,insertPosting,insertAccrual,updatePostingSummary,selectPostingSummary,updateAccrualSummary,selectAccrualSummary);
//SIT2DEV Retro -DD start
			//Retro From 9.8 Add parameter pooldetails
			/** ****** JTEST FIX start ************  on 14DEC09*/
			//processUnpostedAmt(businessDate,pooldetails,updateUnpostedAmt,insertPosting,insertAccrual,updatePostingSummary,selectPostingSummary,updateAccrualSummary,selectAccrualSummary,eventId); // SUP_IS1898
			//Reallocate to Self
			//processUnpostedAmt(businessDate,pooldetails,updateUnpostedAmt,insertPosting,insertAccrual,updatePostingSummary,selectPostingSummary,updateAccrualSummary,selectAccrualSummary,eventId, conn); // SUP_IS1898
			processUnpostedAmt(businessDate,pooldetails,updateUnpostedAmt,updateUnpostedAmtCentralCcy,insertPosting,insertAccrual,updatePostingSummary,selectPostingSummary,updateAccrualSummary,selectAccrualSummary,eventId, conn); // SUP_IS1898
			//Reallocate to Self
			/** ****** JTEST FIX end **************  on 14DEC09*/
//SIT2DEV Retro -DD end
			//ENH_IS1297 RETROFIT ENDS
			//SUP_IS964 ends
//ENH_IS997 Ends
			//Liquidity Volume Testing Optimized_query starts
			////PERFORMANCE_CHANGE_PERF60
			/*
			sbuf.setLength(0);
			sbuf.append("DELETE FROM OPOOL_RP_GL WHERE nbr_lotid = ? ");
			stmt = conn.prepareStatement(sbuf.toString());
			stmt.setLong(1, Long.parseLong(lotId));
			stmt.executeUpdate();			
			*/
			//PERFORMANCE_CHANGE_PERF60
			//Liquidity Volume Testing Optimized_query ends
		}
			
		catch(Exception e)
		{
			logger.fatal("General Exception: ", e);
			//ENH_IS1059   Starts
			//throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, e.getMessage());
			throw new LMSException("LMS01000","for Lot Id :- "+lotId +" Host System :-"+client+ " and Business Date :- "+businessDate, e);
			//ENH_IS1059   Ends
//SIT2DEV Retro -DD start
		}finally{
			LMSConnectionUtility.close(rs);
			LMSConnectionUtility.close(stmt);
			//LMSConnectionUtility.close(conn);//JTest changes on 01-feb-11
		}
//SIT2DEV Retro -DD end
		/*
		 * entries will be set true in insertAccountDtls(),populateAccrualSummary() and populatePosting Summary
		 * indicates  corresponding preparedStatement need to be executed in processAccountingEntries() method in 
		 * LotProcessorSBSLBean. 
		 */
		if (logger.isDebugEnabled())
			logger.debug("Leaving ");
		return entries.clone();
	}
//ENH_IS997 Starts
//SUP_IS764 starts
	
	/**
	 * This method gets the unposted amount  from OPOOL_ACCOUNT_UNPOSTED for each poolid+account pair and posts into  OPOOL_POSTINGSUMMARY
	 * @param businessDate
	 * @param updateUnpostedAmt 
	 * @param insertPosting 
	 * @throws LMSException
//SIT2DEV Retro -DD start
	 */
	//SUP_IS1898 START
	//public boolean[] processUnpostedAmt(java.util.Date businessDate, PreparedStatement updateUnpostedAmt, PreparedStatement insertPosting, PreparedStatement insertAccrual, PreparedStatement updatePostingSummary,PreparedStatement selectPostingSummary,PreparedStatement updateAccountSummary,PreparedStatement selectAccrualSummary) throws LMSException {
	/** ****** JTEST FIX start ************  on 14DEC09*/
	//public boolean[] processUnpostedAmt(java.util.Date businessDate,HashMap pooldetails, PreparedStatement updateUnpostedAmt, PreparedStatement insertPosting, PreparedStatement insertAccrual, PreparedStatement updatePostingSummary,PreparedStatement selectPostingSummary,PreparedStatement updateAccountSummary,PreparedStatement selectAccrualSummary,int eventId) throws LMSException {
	//public boolean[] processUnpostedAmt(java.util.Date businessDate,HashMap pooldetails, PreparedStatement updateUnpostedAmt, PreparedStatement insertPosting, PreparedStatement insertAccrual, PreparedStatement updatePostingSummary,PreparedStatement selectPostingSummary,PreparedStatement updateAccountSummary,PreparedStatement selectAccrualSummary,int eventId, Connection conn) throws LMSException {//Reallocate to Self
	public boolean[] processUnpostedAmt(java.util.Date businessDate,HashMap pooldetails, PreparedStatement updateUnpostedAmt, PreparedStatement updateUnpostedAmtCentralCcy, PreparedStatement insertPosting, PreparedStatement insertAccrual, PreparedStatement updatePostingSummary,PreparedStatement selectPostingSummary,PreparedStatement updateAccountSummary,PreparedStatement selectAccrualSummary,int eventId, Connection conn) throws LMSException {
	/** ****** JTEST FIX end **************  on 14DEC09*/
    //SUP_IS1898 END
//SIT2DEV Retro -DD end
		if (logger.isDebugEnabled())
			logger.debug("Entering ");
		
			
		Set set = null;
		java.util.Iterator itr = null;
		//SUP_389 Starts
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(businessDate);
	// Retro from LIQUIDITY_11.4.1_BUGFIX to 11.5 starts
		//calendar.add(Calendar.DATE,ExecutionConstants.ENDDATE_DELAY);  //  JPMC_POOL_ENDDATE_DELAY
	// Retro from LIQUIDITY_11.4.1_BUGFIX to 11.5 ends
//		//SUP_389 Ends
		java.sql.Date businessDat  = new java.sql.Date(businessDate.getTime());
		//ENH_IS1297 RETROFIT STARTS
		//String item = null, //SUP_IS1192 - NOT GETTING USED
		String key=null;
		//ENH_IS1297 RETROFIT ENDS
		String itemList[] = null;
		String currency = null;
		BigDecimal amt = new BigDecimal(0);
		BigDecimal amtCentralCcy = new BigDecimal(0);//Reallocate to Self
		ExecutionUtility util=new ExecutionUtility();
		BigDecimal[] precision_carryfwd ={ZERO,ZERO};
		//ENH_IS1297 RETROFIT STARTS
		AccountInfoDO accountInfoDo = null;//SUP_IS1192
		boolean recordPresent=true;//SUP_IS1192
		String accountInfoString=null;//SUP_IS1192
		//ENH_IS1297 RETROFIT ENDS
		String typEntity=null;//SUP_IS1898
		int rowCount=0;//SUP_ISS_I_7247
		try
		{
			if (logger.isDebugEnabled())
			{
				logger.debug("cacheAmtUnposted: "+cacheAmtUnposted);
					
			}
//SUP_IS1898 START
			if(eventId==ExecutionConstants.INCO_INTEREST_CALC)
				typEntity=ExecutionConstants.INCO;
			else
				typEntity=ExecutionConstants.ACCOUNT;
//SUP_IS1898 END
			//iterate thru accPoolIdList 
			int size = accPoolIdList.size() ;
			//ENH_IS1297 RETROFIT STARTS
			//Added for Debug : SUP_IS1192
			if (logger.isDebugEnabled()){
				 logger.debug("SIZE OF LIST accPoolIdList:"+size);
				
				}
			for(int count = 0;count<size;count++)
			{
				//SUP_IS1192 Starts
				accountInfoDo =(AccountInfoDO)accPoolIdList.get(count);				
				//item = (String)accPoolIdList.get(count);
				if (logger.isDebugEnabled()){
				 logger.debug("item before :"+accountInfoDo.toString());
					//logger.debug("item before :"+item);
				}
				//separate poolid+account ,currency
				//itemList = LMSUtility.split(item,ExecutionConstants.SEPARATOR);//Comment as not get used SUP_IS1192
				if (logger.isDebugEnabled())
				{
					//logger.debug("Pool Id  :"+itemList[0]);
					//logger.debug("Account No  :"+itemList[1]);
					//logger.debug("Currency :"+itemList[2]);
					//logger.debug("Posting Start Date :"+itemList[3]);//SUP_IS1192 
					logger.debug("Pool Id  :"+accountInfoDo.getID_POOL());
					logger.debug("Account No  :"+accountInfoDo.getPARTICIPANT_ID());
					logger.debug("Currency :"+accountInfoDo.getCOD_CCY());
					logger.debug("Posting Start Date :"+accountInfoDo.getDAT_POSTING_START());
					logger.debug("Type Paricipant :"+accountInfoDo.getTYP_PARTICIPANT());
					logger.debug("Businessdate :"+businessDat);
				}
					// Retro from JPMC_ LIQUIDITY_11.4.1_BUGFIX JPMC to 11.5 starts
				//if((pooldetails != null && pooldetails.containsKey(accountInfoDo.getID_POOL()) && ((String)pooldetails.get(accountInfoDo.getID_POOL())).equalsIgnoreCase("true")))//JPMC_IS FIXED//SUP_ISS_I_7247
				if(accountInfoDo.getCOD_TXN().substring(0, 1).equalsIgnoreCase("R") //SUP_ISS_I_7247
						|| accountInfoDo.getCOD_TXN().substring(0, 1).equalsIgnoreCase("I"))//SUP_ISS_I_7247
				{
					logger.debug("Poooooooooling");
					calendar.add(Calendar.DATE,ExecutionConstants.POOL_ENDDATE_DELAY);  //  JPMC_POOL_ENDDATE_DELAY
				}else{
					calendar.add(Calendar.DATE,ExecutionConstants.ENDDATE_DELAY);  //  JPMC_POOL_ENDDATE_DELAY
				}
					// Retro from LIQUIDITY_11.4.1_BUGFIX JPMCto 11.5 starts ends
				//item = itemList[0]+ExecutionConstants.SEPARATOR+itemList[1];
				//currency = itemList[2];
				 currency = accountInfoDo.getCOD_CCY();
				if (logger.isDebugEnabled())
				{
					logger.debug("item after(poolid+account) :"+accountInfoDo.toString()); 
					//logger.debug("item after(poolid+account) :"+item);
					
				}
					//SUP_IS1192 Ends
				set = cacheAmtUnposted != null ? cacheAmtUnposted.keySet(): null;
				itr=set != null ? set.iterator() : null;
				while(itr!= null && itr.hasNext())
				{
					//check where item is part of key
					key=(String)itr.next();
					itemList = LMSUtility.split(key,ExecutionConstants.SEPARATOR);

					//SUP_IS1192 Starts
					if (logger.isDebugEnabled())
					{
						/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//						logger.debug("key  :"+key.toString());
						logger.debug("key  :"+key);
//						logger.debug("cache itemList  :"+itemList.toString());
						logger.debug("cache itemList  :"+Arrays.toString(itemList));
						/** ****** FindBugs Fix End ***********  on 14DEC09*/
						logger.debug("cache unposted Pool Id  :"+itemList[0]);
						logger.debug("cache unposted Account No  :"+itemList[1]);
						logger.debug("cache unposted TXN Code  :"+itemList[2]);
						logger.debug("party end Pool Id  :"+accountInfoDo.getID_POOL());
						logger.debug("party end Account No  :"+accountInfoDo.getPARTICIPANT_ID());
						if(accountInfoDo.getSOURCE_POOL_ID().equalsIgnoreCase("NA")) //SUP_ANZ_NULLPOINTER starts
							logger.debug("Non pool account");
						else
							logger.debug("cache unposted Source Pool Id  :"+itemList[3]); //SUP_ANZ_NULLPOINTER ends
					}
					//if( key.indexOf(item)!= -1 ) //SUP_IS1192
					//poolId+ExecutionConstants.SEPARATOR+nbrAcctUnposted+ExecutionConstants.SEPARATOR+amtType										
					//if( key.indexOf(accountInfoString)!= -1 ) //SUP_IS1192	
					//if(accountInfoDo.getID_POOL().equals(itemList[0]) &&  accountInfoDo.getPARTICIPANT_ID().equals(itemList[1])) //SUP_IS1192
					if(accountInfoDo.getID_POOL().equals(itemList[0]) &&  accountInfoDo.getPARTICIPANT_ID().equals(itemList[1]) && (!accountInfoDo.getSOURCE_POOL_ID().equalsIgnoreCase("NA") && accountInfoDo.getSOURCE_POOL_ID().equalsIgnoreCase(itemList[3])) ) //SUP_IS1192 //SUP_ANZ_NULLPOINTER 
					{						
						logger.debug("Inside If condtion ----apppppp");
						entries[5] = true;
						//get pool id , Account No and txn code 
						//itemList = LMSUtility.split(key,ExecutionConstants.SEPARATOR);//Comment as not get used SUP_IS1192
						if (logger.isDebugEnabled())
						{
						//Comment as not get used and new fields added for debugging for SUP_IS1192 
							//logger.debug("Pool Id  :"+itemList[0]);
							//logger.debug("Account No  :"+itemList[1]);
							//logger.debug("Txn Code  :"+itemList[2]);
							logger.debug("Pool Id  :"+accountInfoDo.getID_POOL());
							logger.debug("Account No  :"+accountInfoDo.getPARTICIPANT_ID());
							logger.debug("Txn Code  :"+accountInfoDo.getCOD_TXN());
							logger.debug(" SOURCE_POOL_ID  :"+ accountInfoDo.getSOURCE_POOL_ID());
						}
						//SUP_IS1192 Starts
						//set place holders for UPDATE OPOOL_ACCOUNT_UNPOSTED query 
						/*updateUnpostedAmt.setDate(1,LMSUtility.truncSqlDate(businessDat));
						updateUnpostedAmt.setString(2,itemList[1]);
						updateUnpostedAmt.setString(3,itemList[0]);
						updateUnpostedAmt.setString(4,itemList[2]);
						updateUnpostedAmt.addBatch();*/
						//SUP_ISS_I_7247 STARTS
						rowCount=1;
						updateUnpostedAmt.setDate(rowCount++,LMSUtility.truncSqlDate(businessDat));
						
						//ENH_IS1731 Exec audit trailing changes Starts
//Retro_Mashreq
						//updateUnpostedAmt.setString(2,com.polaris.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT.format(new java.util.Date()));
						updateUnpostedAmt.setString(rowCount++,REF_DATE_FORMAT.format(new java.util.Date()));
						updateUnpostedAmt.setDate(rowCount++,LMSUtility.truncSqlDate(businessDat));
						updateUnpostedAmt.setString(rowCount++,accountInfoDo.getPARTICIPANT_ID());
						updateUnpostedAmt.setString(rowCount++,accountInfoDo.getID_POOL());
						updateUnpostedAmt.setString(rowCount++,itemList[2]);
						updateUnpostedAmt.setString(rowCount++,ExecutionConstants.POSTING_TYPE);
						updateUnpostedAmt.setString(rowCount++,accountInfoDo.getSOURCE_POOL_ID()); // NEW_ACCOUNTING_ENTRY_FORMAT 
						if(accountInfoDo.getSimId()!=0)
							updateUnpostedAmt.setLong(rowCount++, accountInfoDo.getSimId());
						//ENH_IS1731 Exec audit trailing changes Ends
						updateUnpostedAmt.addBatch();
						//Reallocate to Self
						entries[12] = true;
						rowCount=1;
						updateUnpostedAmtCentralCcy.setDate(rowCount++,LMSUtility.truncSqlDate(businessDat));
						updateUnpostedAmtCentralCcy.setString(rowCount++,REF_DATE_FORMAT.format(new java.util.Date()));
						updateUnpostedAmtCentralCcy.setDate(rowCount++,LMSUtility.truncSqlDate(businessDat));
						updateUnpostedAmtCentralCcy.setString(rowCount++,accountInfoDo.getPARTICIPANT_ID());
						updateUnpostedAmtCentralCcy.setString(rowCount++,accountInfoDo.getID_POOL());
						updateUnpostedAmtCentralCcy.setString(rowCount++,itemList[2]);
						updateUnpostedAmtCentralCcy.setString(rowCount++,ExecutionConstants.POSTING_TYPE);
						updateUnpostedAmtCentralCcy.setString(rowCount++,accountInfoDo.getSOURCE_POOL_ID()); // NEW_ACCOUNTING_ENTRY_FORMAT 
						if(accountInfoDo.getSimId()!=0)
							updateUnpostedAmtCentralCcy.setLong(rowCount++, accountInfoDo.getSimId());
							//SUP_ISS_I_7247 ENDS
						updateUnpostedAmtCentralCcy.addBatch();
						
						//get unposted amt from cache
						amt = (BigDecimal)cacheAmtUnposted.get(key);
						// JPMC_CONSOL_PERFORMANCE_TESTING 	issue fixed					
						if(cacheAmtUnpostedCentralCcy.get(key)!=null)
							amtCentralCcy = (BigDecimal)cacheAmtUnpostedCentralCcy.get(key);
						
						//Reallocate to Self
						//cacheAmtUnposted.remove(key);
						itr.remove();
						if (logger.isDebugEnabled())
						{
							logger.debug("After Removing cacheAmtUnposted: "+cacheAmtUnposted);
							logger.debug("After Removing amtCentralCcy: "+amtCentralCcy);
							logger.debug("key is  processUnpostedAmt "+key);
								
						}
				
						if (logger.isDebugEnabled()){
							logger.debug("amt Before Round up :"+amt);
						}
						//Round up amount to be posted
						if (logger.isDebugEnabled())
						{
							logger.debug("currency for Round up " + currency);
						
						}
						/** ****** JTEST FIX start ************  on 14DEC09*/
//						precision_carryfwd = util.getTruncatedAmt(amt,ExecutionConstants.ROUNDOFF_ACCRUAL_POSTING,currency);
						precision_carryfwd = util.getTruncatedAmt(amt,ExecutionConstants.ROUNDOFF_ACCRUAL_POSTING,currency, conn);
						/** ****** JTEST FIX end **************  on 14DEC09*/
				
						amt=precision_carryfwd[0];
						//Reallocate to Self
						precision_carryfwd = util.getTruncatedAmt(amtCentralCcy,ExecutionConstants.ROUNDOFF_ACCRUAL_POSTING,currency, conn);
						amtCentralCcy=precision_carryfwd[0];
						if (logger.isDebugEnabled())
						{
							logger.debug("amt after Round up" + amt);
							logger.debug("amt after Round up" + amtCentralCcy);
						
						}
						
						if(amtCentralCcy.compareTo(new BigDecimal(0)) == 0){
							
							if (logger.isDebugEnabled())
							{
								logger.debug("as amtCentralCcy is zero do not post");
							
							}
							//continue;
						}
						//Reallocate to Self
						if(amt.compareTo(new BigDecimal(0)) == 0){
							
							if (logger.isDebugEnabled())
							{
								logger.debug("as amt is zero do not post");
							
							}
							continue;
						}
						//ENH_IS1297 RETROFIT STARTS
						//SUP_IS1192  Starts function call  has  added to check wheather record is present or not					 
						//NT_UT_BUFIX_09_Jan_09 Start  //ENH_IS1913 starts
						if (logger.isDebugEnabled())
						{
							logger.debug("Before setting the code --itemList[2]"+accountInfoDo.getCOD_TXN());
						
						}
						accountInfoDo.setCOD_TXN(itemList[2]);
						if (logger.isDebugEnabled())
						{
							logger.debug("After setting the code --itemList[2]"+accountInfoDo.getCOD_TXN());
						
						}
						//NT_UT_BUFIX_09_Jan_09 Start end //ENH_IS1913 ends
						//int rowCount = 1;//SUP_ISS_III_7718
						recordPresent=checkRecordInPostingSummary(accountInfoDo,selectPostingSummary);
						if(recordPresent==false)
						{
							if (logger.isDebugEnabled())
							{
								logger.debug("In insert Posting summary ");
							
							}
						//SUP_ISS_III_7718 starts
							rowCount = 1;
						//SUP_ISS_II_7461 starts	
						insertPosting.setString(rowCount++,accountInfoDo.getID_POOL());
						insertPosting.setString(rowCount++,accountInfoDo.getPARTICIPANT_ID());
						//NT_UT_BUFIX_09_Jan_09 Start //ENH_IS1913 starts
						//insertPosting.setString(3,accountInfoDo.getCOD_TXN());
						insertPosting.setString(rowCount++,itemList[2]); 
						//NT_UT_BUFIX_09_Jan_09 End //ENH_IS1913 ends
						insertPosting.setDate(rowCount++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING_START()));
						//SUP_IS1192 Ends
						//ENH_IS1297 RETROFIT ENDS
						insertPosting.setDate(rowCount++,LMSUtility.truncSqlDate(businessDat));
						//insertPosting.setDate(5,LMSUtility.truncSqlDate(businessDat)); SUP_389
						insertPosting.setDate(rowCount++,LMSUtility.truncSqlDate(new java.sql.Date(calendar.getTime().getTime()))); //+1 SUP_389
						//ENH_IS1167 starts
						// Using executionconstants.properties file to get ENDDATE_DELAY value and adding it to participant end date value	
						//insertPosting.setDate(7,LMSUtility.truncSqlDate(businessDat));
						insertPosting.setDate(rowCount++,LMSUtility.truncSqlDate(new java.sql.Date(calendar.getTime().getTime())));
						//ENH_IS1167  ends
						insertPosting.setFloat(rowCount++,0F);
						insertPosting.setBigDecimal(rowCount++,amt);
						insertPosting.setString(rowCount++,ExecutionConstants.POSTING_GENERATED_STATUS);
						//ENH_IS1297 RETROFIT STARTS
						
						//ENH_IS1731 Exec audit trailing changes Starts
						insertPosting.setString(rowCount++,ExecutionConstants.ACCOUNT);
						//ENH_IS1297 RETROFIT ENDS
						insertPosting.setString(rowCount++,accountInfoDo.getCOD_TXN().substring(3, 4));
						// FindBug issue fix starts
						//insertPosting.setString(12,com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT.format(new java.util.Date()));
						insertPosting.setString(rowCount++,REF_DATE_FORMAT.format(new java.util.Date()));
						// FindBug issue fix ends
						insertPosting.setDate(rowCount++,LMSUtility.truncSqlDate(businessDat));
						//Reallocate to Self
						insertPosting.setString(rowCount++,accountInfoDo.getSOURCE_POOL_ID()); // NEW_ACCOUNTING_ENTRY_FORMAT 
						insertPosting.setBigDecimal(rowCount++,amtCentralCcy);//SUP_ISS_II_7461
						if(accountInfoDo.getSimId() != 0)
								insertPosting.setLong(rowCount++, accountInfoDo.getSimId());
						//Reallocate to Self
						//ENH_IS1731 Exec audit trailing changes Ends
						//SUP_ISS_III_7718 ends
						//SUP_ISS_II_7461 ends						
						insertPosting.addBatch();
						//ENH_IS1297 RETROFIT STARTS
						//SUP_IS1192 Starts if record is already present then update
							entries[9]=true;
												
						}
						else
						{
							if (logger.isDebugEnabled())
							{
								logger.debug("In Update Posting summary ");
								logger.debug("In AMT_ACCRUEDNOTPOSTED AMOUNT-- "+accountInfoDo.getAMT_ACCRUEDNOTPOSTED());
								logger.debug("In AMT_ACCRUEDNOTPOSTED Code transaction -- "+accountInfoDo.getCOD_TXN());
								logger.debug("In ADDED AMOUNT -- "+amt);
								logger.debug("In Update Posting summary update added AMT  " +accountInfoDo.getAMT_ACCRUEDNOTPOSTED().add(amt));
								logger.debug("In COD_TXN  " +accountInfoDo.getCOD_TXN());
								logger.debug(" accountInfoDo.getSOURCE_POOL_ID() "+ accountInfoDo.getSOURCE_POOL_ID());
							
							}
							//SUP_ISS_III_7718 starts
							rowCount=1;
							updatePostingSummary.setBigDecimal(rowCount++,new BigDecimal(0));
//SIT2DEV Retro -DD start
							//Retro From 9.8 Starts
							//HSBC_QF 13-05-2009 Star
//							updatePostingSummary.setBigDecimal(2,accountInfoDo.getAMT_ACCRUEDNOTPOSTED().add(amt));
							updatePostingSummary.setBigDecimal(rowCount++,accountInfoDo.getAMT_POSTED().add(accountInfoDo.getAMT_ACCRUEDNOTPOSTED().add(amt)));
							//HSBC_QF 13-05-2009 Ends
							//Retro From 9.8 Ends
//SIT2DEV Retro -DD end
							updatePostingSummary.setString(rowCount++,ExecutionConstants.POSTING_GENERATED_STATUS);
							updatePostingSummary.setDate(rowCount++,LMSUtility.truncSqlDate(new java.sql.Date(calendar.getTime().getTime())));
							updatePostingSummary.setDate(rowCount++,LMSUtility.truncSqlDate(new java.sql.Date(calendar.getTime().getTime())));
							updatePostingSummary.setDate(rowCount++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING()));	
							
							//ENH_IS1731 Exec audit trailing changes Starts
							// FindBug issue fix starts
							//updatePostingSummary.setString(7,com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT.format(new java.util.Date()));
							updatePostingSummary.setString(rowCount++,REF_DATE_FORMAT.format(new java.util.Date()));//Harikesh
							// FindBug issue fix ends
							updatePostingSummary.setDate(rowCount++,LMSUtility.truncSqlDate(businessDat));
							updatePostingSummary.setBigDecimal(rowCount++,accountInfoDo.getAMT_CAL_CENTRAL_CCY().add(amtCentralCcy));//Reallocate to Self
							updatePostingSummary.setString(rowCount++,accountInfoDo.getPARTICIPANT_ID());
							updatePostingSummary.setString(rowCount++,accountInfoDo.getCOD_TXN());
							updatePostingSummary.setDate(rowCount++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING_START()));
							updatePostingSummary.setString(rowCount++,accountInfoDo.getID_POOL());
							updatePostingSummary.setString(rowCount++,accountInfoDo.getTYP_PARTICIPANT());
							updatePostingSummary.setString(rowCount++,accountInfoDo.getSOURCE_POOL_ID());		//NEW_ACCOUNTING_ENTRY_FORMAT 
							if(accountInfoDo.getSimId()!=0)
								updatePostingSummary.setLong(rowCount++, accountInfoDo.getSimId());
							//ENH_IS1731 Exec audit trailing changes ends
							//SUP_ISS_III_7718 ends
							updatePostingSummary.addBatch();
							entries[4]=true;
						}
						
						//set place holders for INSERT INTO OPOOL_POSTINGSUMMARY  query 
						/*insertPosting.setString(1,itemList[0]);
						insertPosting.setString(2,itemList[1]);
						insertPosting.setString(3,itemList[2]);
						//SUP_IS1192 Starts
						//insertPosting.setDate(4,LMSUtility.truncSqlDate(businessDat));
						java.util.Date postingStartD  =LMSUtility.StringtoDate(postingStartDat,LMSConstants.DATE_FORMAT);
						 insertPosting.setDate(4,LMSUtility.truncSqlDate(new java.sql.Date(postingStartD.getTime()))); 
		    				//SUP_IS1192 Ends
						insertPosting.setDate(5,LMSUtility.truncSqlDate(businessDat));
					 	//insertPosting.setDate(5,LMSUtility.truncSqlDate(businessDat)); SUP_389
						insertPosting.setDate(6,LMSUtility.truncSqlDate(new java.sql.Date(calendar.getTime().getTime()))); //+1 SUP_389
						 //ENH_IS1167 starts 
						//insertPosting.setDate(7,LMSUtility.truncSqlDate(businessDat));
						//used the calendar(which has businessDat+ExecutionConstants.ENDDATE_DELAY)
						insertPosting.setDate(7,LMSUtility.truncSqlDate(new java.sql.Date(calendar.getTime().getTime())));
						//ENH_IS1167  ends
						insertPosting.setFloat(8,0F);
						insertPosting.setBigDecimal(9,amt);
			 			insertPosting.setString(10,ExecutionConstants.POSTING_GENERATED_STATUS);
						
						insertPosting.addBatch(); */
						////SUP_IS1192 Ends
						//SUP_IS964 starts
						//ENH_IS1297 RETROFIT STARTS
						
						//set place holders for INSERT INTO OPOOL_ACCRUALSUMMARY  query 
						//SUP_IS1192 Starts
						recordPresent=checkRecordInAccrualSummary(accountInfoDo,selectAccrualSummary);
						if(recordPresent==false)
						{
							if (logger.isDebugEnabled())
							{
								logger.debug("In insert Accrual summary ");
							
							}
						//insertAccrual.setString(1,itemList[0]);
						//insertAccrual.setString(2,itemList[1]);
						//insertAccrual.setString(3,itemList[2]);*/
						//SUP_ISS_III_7718 starts
							rowCount=1;
						insertAccrual.setString(rowCount++,accountInfoDo.getID_POOL());
						insertAccrual.setString(rowCount++,accountInfoDo.getPARTICIPANT_ID());
						//NT_UT_BUFIX_09_Jan_09 Start //ENH_IS1913 starts
						//insertAccrual.setString(3,accountInfoDo.getCOD_TXN());
						insertAccrual.setString(rowCount++,itemList[2]);
						//NT_UT_BUFIX_09_Jan_09 End //ENH_IS1913 ends
						//SUP_IS1192 Ends
						//ENH_IS1297 RETROFIT ENDS
						insertAccrual.setDate(rowCount++,LMSUtility.truncSqlDate(businessDat));
						insertAccrual.setDate(rowCount++,LMSUtility.truncSqlDate(businessDat));
						insertAccrual.setDate(rowCount++,LMSUtility.truncSqlDate(businessDat));
						//insertAccrual.setDate(7,LMSUtility.truncSqlDate(businessDat)); SUP_389
						insertAccrual.setDate(rowCount++,LMSUtility.truncSqlDate(new java.sql.Date(calendar.getTime().getTime()))); //+1 SUP_389
						insertAccrual.setDate(rowCount++,LMSUtility.truncSqlDate(businessDat));
						insertAccrual.setFloat(rowCount++,0F);
						insertAccrual.setBigDecimal(rowCount++,amt);
						insertAccrual.setString(rowCount++,ExecutionConstants.ACCRUAL_GENERATED_STATUS);
						insertAccrual.setString(rowCount++,ExecutionConstants.ACCOUNT);
						//ENH_IS1731 Exec audit trailing changes Starts
						// FindBug issue fix starts
						//insertAccrual.setString(13,com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT.format(new java.util.Date()));
						insertAccrual.setString(rowCount++,REF_DATE_FORMAT.format(new java.util.Date()));//Harikesh
						// FindBug issue fix ends
						insertAccrual.setDate(rowCount++,LMSUtility.truncSqlDate(businessDat));
						//ENH_IS1731 Exec audit trailing changes Ends
						//Retro from 9.8 SUP_IS1374 Start
						insertAccrual.setDate(rowCount++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING_START()));//DAT_POSTING_START was goin null 
						//Retro from 9.8 SUP_IS1374 Ends
						insertAccrual.setString(rowCount++,accountInfoDo.getSOURCE_POOL_ID()); //NEW_ACCOUNTING_ENTRY_FORMAT 
						if(accountInfoDo.getSimId() != 0)
							insertAccrual.setLong(rowCount++, accountInfoDo.getSimId());
						//SUP_ISS_III_7718 starts
						insertAccrual.addBatch();
						//ENH_IS1297 RETROFIT STARTS
					////SUP_IS1192 Starts if record is already present then update
						entries[10] = true;

						}//if recordPresent ends
						else{
							if (logger.isDebugEnabled())
							{
								logger.debug("In update Accrual summary ");
							
							}
							//SUP_ISS_III_7718 starts
							rowCount=1;
							updateAccountSummary.setBigDecimal(rowCount++,new BigDecimal(0));
							updateAccountSummary.setBigDecimal(rowCount++,accountInfoDo.getAMT_ACCRUED().add(amt));//adding unposted amt
							if (logger.isDebugEnabled())
							{
								logger.debug("In update AMT_ACCRUED " +accountInfoDo.getAMT_ACCRUED());
								logger.debug("In update AMT " +amt);
								logger.debug("In update added AMT  " +accountInfoDo.getAMT_ACCRUED().add(amt));
							
							}
							updateAccountSummary.setString(rowCount++,ExecutionConstants.ACCRUAL_GENERATED_STATUS);				
							updateAccountSummary.setDate(rowCount++,LMSUtility.truncSqlDate(businessDat));
							updateAccountSummary.setDate(rowCount++,LMSUtility.truncSqlDate(new java.sql.Date(calendar.getTime().getTime())));
							//ENH_IS1731 Exec audit trailing changes Starts
							updateAccountSummary.setDate(rowCount++,LMSUtility.truncSqlDate(businessDat));	
							// FindBug issue fix starts
							//updateAccountSummary.setString(7,com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT.format(new java.util.Date()));
							updateAccountSummary.setString(rowCount++,REF_DATE_FORMAT.format(new java.util.Date()));//Harikesh
							// FindBug issue fix ends
							updateAccountSummary.setDate(rowCount++,LMSUtility.truncSqlDate(businessDat));
							//SETTING WHRER CLAUSE PLACE HOLDERS
							//Changes done in setting IN, out parameter starts
							updateAccountSummary.setString(rowCount++,accountInfoDo.getPARTICIPANT_ID());
							updateAccountSummary.setString(rowCount++,accountInfoDo.getCOD_TXN());
							updateAccountSummary.setDate(rowCount++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_ACCRUAL_START()));
							updateAccountSummary.setString(rowCount++,accountInfoDo.getID_POOL());
							updateAccountSummary.setString(rowCount++,accountInfoDo.getTYP_PARTICIPANT());
							//Changes done in setting IN, out parameter ends
							//ENH_IS1731 Exec audit trailing changes Ends
							updateAccountSummary.setString(rowCount++,accountInfoDo.getSOURCE_POOL_ID()); // NEW_ACCOUNTING_ENTRY_FORMAT
							if(accountInfoDo.getSimId()!=0)
								updateAccountSummary.setLong(rowCount++, accountInfoDo.getSimId());
							//SUP_ISS_III_7718 ends
							updateAccountSummary.addBatch();
							entries[2] = true;
							//SUP_IS1192 Ends					
					//SUP_IS964 ends
					}
				   }					
					
				}
			
				
			}
			
			
		}
		catch(SQLException e)
		{
			logger.fatal("Exception in processUnpostedAmt(): ", e);
			//ENH_IS1059 Starts
			//throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, e.getMessage());
			throw new LMSException("LMS01001","", e);
			//ENH_IS1059  Ends
		}
		
		if (logger.isDebugEnabled()){
			logger.debug("Leaving ");
		}
		return entries;
		
	}
//SUP_IS764 ends
//ENH_IS997 Ends
	
	/**
	 * This method is basically used to populate cacheAmtUnposted Hash Map
	 * @throws LMSException
	 */
	//ENH_IS1297 Starts...BVT Changes
	//public void getCacheUnpostedAmount() throws LMSException
	//public void getCacheUnpostedAmount(String accrualType, long idBVT) throws LMSException //Intercompany Changes
	public void getCacheUnpostedAmount(int eventId,String accrualType, long idBVT) throws LMSException
	{
	//ENH_IS1297 Ends...BVT Changes
		logger.debug("Entering in getCacheUnpostedAmount() customised method");
		Connection conn = null;
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		Statement stmt=null;
		PreparedStatement pStmt=null;
		/** ****** JTEST FIX end **************  on 14DEC09*/
		String nbrAcctUnposted=null;
		String amtType=null;
		// FindBug issue fix starts
		BigDecimal amtUnposted=ZERO;
		// FindBug issue fix ends
		ResultSet rs = null;
		String poolId = "NONPOOL_ACCT";
		StringBuilder sbufUnpostedAmt = new StringBuilder("");
		String NORMAL_ACCRUAL="NORMAL_ACCRUAL";
		String source_pool_id = null ; // NEW_ACCOUNTING_ENTRY_FORMAT
		if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
			logger.debug("Accrual Type is "+accrualType+" And hence BVT ID is: ["+idBVT+"]");//ENH_IS1297 Starts...BVT Changes
		try{
			
			sbufUnpostedAmt.append(" SELECT SUM(AMOUNT)SUM_AMT, NBR_OWNACCOUNT,AMOUNT_TYPE,ID_POOL ,SOURCE_POOL_ID");
			sbufUnpostedAmt.append(" FROM OPOOL_ACCOUNT_UNPOSTED");
			sbufUnpostedAmt.append(" WHERE USE_DATE IS NULL ");
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sbufUnpostedAmt.append(" AND POSTING_TYPE = '"+NORMAL_ACCRUAL+"'");
			sbufUnpostedAmt.append(" AND POSTING_TYPE = ?");
			
			//ENH_IS1297 Starts...BVT Changes
			/*if (accrualType.equalsIgnoreCase(ExecutionConstants.BVT_TXN) && idBVT != 0) {
				sbufUnpostedAmt.append(" AND UNPOSTED_DUETO = 'BVTAMOUNT_CARRYFORWARD'");
			}else{
			    //ENH_IS1298
				sbufUnpostedAmt.append(" AND UNPOSTED_DUETO = 'AMOUNT_CARRYFORWARD'");
			    //ENH_IS1298
			}*/
			//sbufUnpostedAmt.append(" AND UNPOSTED_DUETO = ?");//NED_ISSUE FIXED STARTS
			//ENH_IS1297 Ends...BVT Changes
			/** ****** JTEST FIX end **************  on 14DEC09*/
			
			//intercompany chnages
			if(eventId==ExecutionConstants.INCO_INTEREST_CALC){
				sbufUnpostedAmt.append(" AND UNPOSTED_DUETO = ?");//NED_ISSUE FIXED STARTS
				//Intercompany Interest type changes start
				//sbufUnpostedAmt.append(" AND AMOUNT_TYPE IN ('CCINCI','CCINDI','NCINCI','NCINDI') ");
				sbufUnpostedAmt.append(" AND ID_POOL <> ? AND SOURCE_POOL_ID ='NA' ");//LMS_45_PERF_CERTIFICATION_PF103
			    /** ****** JTEST FIX end **************  on 14DEC09*/
                //Intercompany Interest type changes start
			}
			//NED_ISSUE FIXED STARTS
			else{
				if (accrualType.equalsIgnoreCase(ExecutionConstants.BVT_TXN) && idBVT != 0) 
					sbufUnpostedAmt.append(" AND UNPOSTED_DUETO = ?");
				else
					sbufUnpostedAmt.append(" AND UNPOSTED_DUETO IN(?,?)");
				
				/** ****** JTEST FIX start ************  on 14DEC09*/
//				sbufUnpostedAmt.append(" AND ID_POOL = '"+poolId+"' ");
				sbufUnpostedAmt.append(" AND ID_POOL = ? ");
				/** ****** JTEST FIX end **************  on 14DEC09*/
			}
			//NED_ISSUE FIXED ENDS
			sbufUnpostedAmt.append(" GROUP BY NBR_OWNACCOUNT,AMOUNT_TYPE,ID_POOL,SOURCE_POOL_ID");
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("Amount Unposted Query  =  "+ sbufUnpostedAmt.toString());
			
			conn = LMSConnectionUtility.getConnection();
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			stmt = conn.createStatement();
//			rs=stmt.executeQuery(sbufUnpostedAmt.toString());
			pStmt = conn.prepareStatement(sbufUnpostedAmt.toString());
			pStmt.setString(1, NORMAL_ACCRUAL);
			if (accrualType.equalsIgnoreCase(ExecutionConstants.BVT_TXN) && idBVT != 0) {
				//Issue No 54 Fix Starts-- Sandeep Mane commented
				//pStmt.setString(2, "+'BVTAMOUNT_CARRYFORWARD'+");
				pStmt.setString(2, "BVTAMOUNT_CARRYFORWARD");
			}else {
				//pStmt.setString(2, "+'AMOUNT_CARRYFORWARD'+");
				pStmt.setString(2, "AMOUNT_CARRYFORWARD");
			//NED_ISSUE FIXED STARTS
				if(eventId!=ExecutionConstants.INCO_INTEREST_CALC){
					pStmt.setString(3, "AMOUNT_LESSTHAN_MIN_AMOUNT");
				}
			//NED_ISSUE FIXED ENDS
				//Issue No 54 Fix Ends
			}
			if(eventId==ExecutionConstants.INCO_INTEREST_CALC){
				pStmt.setString(3, poolId);//LMS_45_PERF_CERTIFICATION_PF103
			}else {
			//NED_ISSUE FIXED STARTS
				if (accrualType.equalsIgnoreCase(ExecutionConstants.BVT_TXN) && idBVT != 0) 
					pStmt.setString(3, poolId);
				else
					pStmt.setString(4, poolId);
			//NED_ISSUE FIXED ENDS
			}
			rs = pStmt.executeQuery();
			/** ****** JTEST FIX end **************  on 14DEC09*/
			while (rs.next())
			{
				nbrAcctUnposted = rs.getString("NBR_OWNACCOUNT");
				amtUnposted = rs.getBigDecimal("SUM_AMT");
				amtType=rs.getString("AMOUNT_TYPE");
				poolId = rs.getString("ID_POOL");
				
				source_pool_id = rs.getString("SOURCE_POOL_ID"); // NEW_ACCOUNTING_ENTRY_FORMAT 
				//cacheAmtUnposted.put(poolId+ExecutionConstants.SEPARATOR+nbrAcctUnposted+ExecutionConstants.SEPARATOR+amtType,amtUnposted);
				cacheAmtUnposted.put(poolId+ExecutionConstants.SEPARATOR+nbrAcctUnposted+ExecutionConstants.SEPARATOR+amtType+ExecutionConstants.SEPARATOR+source_pool_id,amtUnposted); // NEW_ACCOUNTING_ENTRY_FORMAT
				
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("Amount Unposted Details: AMOUNT = "+ amtUnposted +
							",Account = "+ nbrAcctUnposted +" TXN_CODE ="+amtType+" POOLID = "+poolId);
			}
		}
		/** ****** FindBugs Fix Start ***********  on 14DEC09*/
		catch(SQLException e) {
			logger.fatal(
					"Exception setting values into OPOOL_ACCOUNT_UNPOSTED ", e);
			
			throw new LMSException("LMS01000", "Populating unposted amount cache failed ",e);
		}
		catch(NamingException e) {
			logger.fatal(
					"Exception setting values into OPOOL_ACCOUNT_UNPOSTED ", e);
			
			throw new LMSException("LMS01000", "Populating unposted amount cache failed ",e);
		}
		/** ****** FindBugs Fix End ***********  on 14DEC09*/
		finally{
			LMSConnectionUtility.close(rs);
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			LMSConnectionUtility.close(stmt);
			LMSConnectionUtility.close(pStmt);
			/** ****** JTEST FIX end **************  on 14DEC09*/
			LMSConnectionUtility.close(conn);
		}
		
		logger.debug("Leaving");
		
	}
	/**
	 * This method is basically used to populate cacheAmtUnposted Hash Map
	 * @param lotId
	 * @throws LMSException
	 */
	//Added for performance benchmarking start
	//public void getCacheUnpostedAmount(String lotId) throws LMSException
	public void getCacheUnpostedAmount(long processId,String lotId) throws LMSException
	//Added for performance benchmarking ends
	{
		Connection conn = null;
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		Statement stmt=null;
		PreparedStatement pStmt=null;
		/** ****** JTEST FIX end **************  on 14DEC09*/
		String nbrAcctUnposted=null;
		String amtType=null;
		BigDecimal amtUnposted=ZERO;
		ResultSet rs = null;
//ENH_IS997 starts
		String poolId = null;//SUP_IS764
		String source_pool_id = null ; // NEW_ACCOUNTING_ENTRY_FORMAT
//ENH_IS997 Ends

		if (logger.isDebugEnabled()){
			logger.debug("Entering");
		}

		try{

			StringBuilder sbufUnpostedAmt = new StringBuilder(" SELECT ")
//ENH_IS997 Starts
			//SUP_IS764 starts
						//.append(" SUM(AMOUNT)SUM_AMT, NBR_OWNACCOUNT,AMOUNT_TYPE ")
						.append(" SUM(AMOUNT)SUM_AMT, NBR_OWNACCOUNT,AMOUNT_TYPE,ID_POOL ,SOURCE_POOL_ID") // NEW_ACCOUNTING_ENTRY_FORMAT
			//SUP_IS764 ends			
//ENH_IS997 Ends
						.append(" FROM OPOOL_ACCOUNT_UNPOSTED unPost") //Added for performance benchmarking 
						.append(" WHERE USE_DATE IS NULL ")
						/** ****** JTEST FIX start ************  on 14DEC09*/
//						.append(" AND POSTING_TYPE = '"+NORMAL_ACCRUAL+"'")
						.append(" AND POSTING_TYPE = ? ")
						.append(" AND UNPOSTED_DUETO <> 'AMOUNT_CARRYFORWARD_CENTRAL_CCY' ")//Reallocate to Self
						/** ****** JTEST FIX end **************  on 14DEC09*/
						//Added for performance benchmarking start
						/*.append(" AND ID_POOL IN ")
						.append(" (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR ")*/
						.append(" AND EXISTS ")
						.append(" (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR hdr ")
						//Added for performance benchmarking ends
						/** ****** JTEST FIX start ************  on 14DEC09*/
						//Added for performance benchmarking start
//						.append(" WHERE NBR_LOTID= " + lotId + ")")
						.append(" WHERE hdr.NBR_LOTID= ? ")
						.append(" AND hdr.NBR_PROCESSID = ? ")
						.append(" AND hdr.ID_POOL=unPost.ID_POOL) ")
						//Added for performance benchmarking ends
						/** ****** JTEST FIX end **************  on 14DEC09*/
						//Also takes up the unposted amounts of BVT while normal execution
//ENH_IS997 starts
//						.append(" GROUP BY NBR_OWNACCOUNT,AMOUNT_TYPE ");
						.append(" GROUP BY NBR_OWNACCOUNT,AMOUNT_TYPE,ID_POOL, SOURCE_POOL_ID"); //// NEW_ACCOUNTING_ENTRY_FORMAT
//ENH_IS997 Ends

			if (logger.isDebugEnabled()) 
			{
				logger.debug("Amount Unposted Query  =  "+ sbufUnpostedAmt.toString());
			}

			//Preparing the Statements
			conn = LMSConnectionUtility.getConnection();
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			stmt = conn.createStatement();
//			rs=stmt.executeQuery(sbufUnpostedAmt.toString());
			
			pStmt = conn.prepareStatement(sbufUnpostedAmt.toString());
			pStmt.setString(1, NORMAL_ACCRUAL);
			pStmt.setString(2, lotId);
			pStmt.setLong(3, processId);//Added for performance benchmarking start
			rs= pStmt.executeQuery();
			/** ****** JTEST FIX end **************  on 14DEC09*/
			//Populating Unposted Amount into the HashMaps
			while (rs.next())
			{
				nbrAcctUnposted = rs.getString("NBR_OWNACCOUNT");
				amtUnposted = rs.getBigDecimal("SUM_AMT");
				amtType=rs.getString("AMOUNT_TYPE");
//ENH_IS997 starts
				poolId = rs.getString("ID_POOL");//SUP_IS764
//ENH_IS997 Ends
				source_pool_id = rs.getString("SOURCE_POOL_ID"); // NEW_ACCOUNTING_ENTRY_FORMAT 
				//Add this in the HashMap for AmountUnposted
				//Key = poolId+nbrAcctUnposted+amtType
				//Value = amtUnposted
//ENH_IS997 Starts
				//SUP_IS764 starts
				//cacheAmtUnposted.put(nbrAcctUnposted+amtType,amtUnposted);
				//cacheAmtUnposted.put(poolId+ExecutionConstants.SEPARATOR+nbrAcctUnposted+ExecutionConstants.SEPARATOR+amtType,amtUnposted);
				cacheAmtUnposted.put(poolId+ExecutionConstants.SEPARATOR+nbrAcctUnposted+ExecutionConstants.SEPARATOR+amtType+ExecutionConstants.SEPARATOR+source_pool_id,amtUnposted); // NEW_ACCOUNTING_ENTRY_FORMAT 
				//SUP_IS764 ends
				
				if (logger.isDebugEnabled()) 
				{
					logger.debug("Amount Unposted Details: AMOUNT = "+ amtUnposted +
							",Account = "+ nbrAcctUnposted +" TXN_CODE ="+amtType+" POOLID = "+poolId + "source_pool_id "+ source_pool_id);
				}
				
			}
//ENH_IS997 Ends
		/** ****** FindBugs Fix Start ***********  on 14DEC09*/
		}catch(SQLException e){
			logger.fatal(
				  "Exception setting values into OPOOL_ACCOUNT_UNPOSTED ", e);
			 //ENH_IS1059 Start
			 // throw new LMSException("101302", e.toString());
			 throw new LMSException("LMS01000", "Populating unposted amount cache failed for Lot id :-"+lotId,e);
			 //ENH_IS1059 Ends
		}catch(NamingException e){
			logger.fatal(
				  "Exception setting values into OPOOL_ACCOUNT_UNPOSTED ", e);
			 //ENH_IS1059 Start
			 // throw new LMSException("101302", e.toString());
			 throw new LMSException("LMS01000", "Populating unposted amount cache failed for Lot id :-"+lotId,e);
			 //ENH_IS1059 Ends
		/** ****** FindBugs Fix End ***********  on 14DEC09*/
		}finally{
			LMSConnectionUtility.close(rs);
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			LMSConnectionUtility.close(stmt);
			LMSConnectionUtility.close(pStmt);
			/** ****** JTEST FIX end **************  on 14DEC09*/
			LMSConnectionUtility.close(conn);
		}//CLOSE CONNECTIN IN FINALLY
		if (logger.isDebugEnabled()){
			logger.debug("Leaving");
		}
	}
	//Reallocate to Self starts
	
	/**
	 * This method is basically used to populate cacheAmtUnposted Hash Map
	 * @param lotId
	 * @throws LMSException
	 */
	//Added for performance benchmarking start
	//public void getCacheUnpostedAmount(String lotId) throws LMSException
	public void getCacheUnpostedAmountCentralCcy(long processId,String lotId) throws LMSException
	//Added for performance benchmarking ends
	{
		Connection conn = null;
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		Statement stmt=null;
		PreparedStatement pStmt=null;
		/** ****** JTEST FIX end **************  on 14DEC09*/
		String nbrAcctUnposted=null;
		String amtType=null;
		BigDecimal amtUnposted=ZERO;
		ResultSet rs = null;
//ENH_IS997 starts
		String poolId = null;//SUP_IS764
		String source_pool_id = null ; // NEW_ACCOUNTING_ENTRY_FORMAT
//ENH_IS997 Ends

		if (logger.isDebugEnabled()){
			logger.debug("Entering");
		}

		try{

			StringBuilder sbufUnpostedAmt = new StringBuilder(" SELECT ")
//ENH_IS997 Starts
			//SUP_IS764 starts
						//.append(" SUM(AMOUNT)SUM_AMT, NBR_OWNACCOUNT,AMOUNT_TYPE ")
						.append(" SUM(AMOUNT)SUM_AMT, NBR_OWNACCOUNT,AMOUNT_TYPE,ID_POOL ,SOURCE_POOL_ID") // NEW_ACCOUNTING_ENTRY_FORMAT//Reallocate to Self
			//SUP_IS764 ends			
//ENH_IS997 Ends
						.append(" FROM OPOOL_ACCOUNT_UNPOSTED unPost") //Added for performance benchmarking 
						.append(" WHERE USE_DATE IS NULL ")
						/** ****** JTEST FIX start ************  on 14DEC09*/
//						.append(" AND POSTING_TYPE = '"+NORMAL_ACCRUAL+"'")
						.append(" AND POSTING_TYPE = ? ")
						.append(" AND UNPOSTED_DUETO = 'AMOUNT_CARRYFORWARD_CENTRAL_CCY'")
						/** ****** JTEST FIX end **************  on 14DEC09*/
						//Added for performance benchmarking start
						/*.append(" AND ID_POOL IN ")
						.append(" (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR ")*/
						.append(" AND EXISTS ")
						.append(" (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR hdr ")
						//Added for performance benchmarking ends
						/** ****** JTEST FIX start ************  on 14DEC09*/
						//Added for performance benchmarking start
//						.append(" WHERE NBR_LOTID= " + lotId + ")")
						.append(" WHERE hdr.NBR_LOTID= ? ")
						.append(" AND hdr.NBR_PROCESSID = ? ")
						.append(" AND hdr.ID_POOL=unPost.ID_POOL) ")
						//Added for performance benchmarking ends
						/** ****** JTEST FIX end **************  on 14DEC09*/
						//Also takes up the unposted amounts of BVT while normal execution
//ENH_IS997 starts
//						.append(" GROUP BY NBR_OWNACCOUNT,AMOUNT_TYPE ");
						.append(" GROUP BY NBR_OWNACCOUNT,AMOUNT_TYPE,ID_POOL, SOURCE_POOL_ID"); //// NEW_ACCOUNTING_ENTRY_FORMAT//Reallocate to Self
//ENH_IS997 Ends

			if (logger.isDebugEnabled()) 
			{
				logger.debug("Amount Unposted Query  =  "+ sbufUnpostedAmt.toString());
			}

			//Preparing the Statements
			conn = LMSConnectionUtility.getConnection();
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			stmt = conn.createStatement();
//			rs=stmt.executeQuery(sbufUnpostedAmt.toString());
			
			pStmt = conn.prepareStatement(sbufUnpostedAmt.toString());
			pStmt.setString(1, NORMAL_ACCRUAL);
			pStmt.setString(2, lotId);
			pStmt.setLong(3, processId);//Added for performance benchmarking start
			rs= pStmt.executeQuery();
			/** ****** JTEST FIX end **************  on 14DEC09*/
			//Populating Unposted Amount into the HashMaps
			while (rs.next())
			{
				nbrAcctUnposted = rs.getString("NBR_OWNACCOUNT");
				amtUnposted = rs.getBigDecimal("SUM_AMT");
				amtType=rs.getString("AMOUNT_TYPE");
//ENH_IS997 starts
				poolId = rs.getString("ID_POOL");//SUP_IS764
//ENH_IS997 Ends
				source_pool_id = rs.getString("SOURCE_POOL_ID"); // NEW_ACCOUNTING_ENTRY_FORMAT 
				//Add this in the HashMap for AmountUnposted
				//Key = poolId+nbrAcctUnposted+amtType
				//Value = amtUnposted
//ENH_IS997 Starts
				//SUP_IS764 starts
				//cacheAmtUnposted.put(nbrAcctUnposted+amtType,amtUnposted);
				//cacheAmtUnposted.put(poolId+ExecutionConstants.SEPARATOR+nbrAcctUnposted+ExecutionConstants.SEPARATOR+amtType,amtUnposted);
				cacheAmtUnpostedCentralCcy.put(poolId+ExecutionConstants.SEPARATOR+nbrAcctUnposted+ExecutionConstants.SEPARATOR+amtType+ExecutionConstants.SEPARATOR+source_pool_id,amtUnposted); // NEW_ACCOUNTING_ENTRY_FORMAT
				//SUP_IS764 ends
				
				if (logger.isDebugEnabled()) 
				{
					logger.debug("Amount Unposted Details: AMOUNT = "+ amtUnposted +
							",Account = "+ nbrAcctUnposted +" TXN_CODE ="+amtType+" POOLID = "+poolId + "source_pool_id "+ source_pool_id);
				}
				
			}
//ENH_IS997 Ends
		/** ****** FindBugs Fix Start ***********  on 14DEC09*/
		}catch(SQLException e){
			logger.fatal(
				  "Exception setting values into OPOOL_ACCOUNT_UNPOSTED ", e);
			 //ENH_IS1059 Start
			 // throw new LMSException("101302", e.toString());
			 throw new LMSException("LMS01000", "Populating unposted amount cache failed for Lot id :-"+lotId,e);
			 //ENH_IS1059 Ends
		}catch(NamingException e){
			logger.fatal(
				  "Exception setting values into OPOOL_ACCOUNT_UNPOSTED ", e);
			 //ENH_IS1059 Start
			 // throw new LMSException("101302", e.toString());
			 throw new LMSException("LMS01000", "Populating unposted amount cache failed for Lot id :-"+lotId,e);
			 //ENH_IS1059 Ends
		/** ****** FindBugs Fix End ***********  on 14DEC09*/
		}finally{
			LMSConnectionUtility.close(rs);
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			LMSConnectionUtility.close(stmt);
			LMSConnectionUtility.close(pStmt);
			/** ****** JTEST FIX end **************  on 14DEC09*/
			LMSConnectionUtility.close(conn);
		}//CLOSE CONNECTIN IN FINALLY
		if (logger.isDebugEnabled()){
			logger.debug("Leaving");
		}
	}
	//Reallocate to Self ends
	/**
	 * This method is basically used to populate cacheAmtUnposted Hash Map
	 * @param lotId
	 * @throws LMSException
	 */
	//Changed Method Name
	//private void getCacheUnpostedAmountBVT() throws LMSException
	private void getCacheUnpostedAmountBVTForPool() throws LMSException
	{
		Connection conn = null;
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		Statement stmt=null;
		PreparedStatement pStmt = null;
		/** ****** JTEST FIX end **************  on 14DEC09*/
		String nbrAcctUnposted=null;
		String amtType=null;
		BigDecimal amtUnposted=ZERO;
		ResultSet rs = null;
		String poolId = null;//SUP_IS764
		String source_pool_id = null ; // NEW_ACCOUNTING_ENTRY_FORMAT

		if (logger.isDebugEnabled()){
			logger.debug("Entering");
		}
		try{

			StringBuilder sbufUnpostedAmt = new StringBuilder(" SELECT ")
			//SUP_IS764 starts
						//.append(" SUM(AMOUNT)SUM_AMT, NBR_OWNACCOUNT,AMOUNT_TYPE ")
						.append(" SUM(AMOUNT)SUM_AMT, NBR_OWNACCOUNT,AMOUNT_TYPE,ID_POOL ,SOURCE_POOL_ID")
			//SUP_IS764 ends			
						.append(" FROM OPOOL_ACCOUNT_UNPOSTED")
						.append(" WHERE USE_DATE IS NULL ")
						/** ****** JTEST FIX start ************  on 14DEC09*/
//						.append(" AND POSTING_TYPE = '"+NORMAL_ACCRUAL+"'")
						.append(" AND POSTING_TYPE = ?")
						/** ****** JTEST FIX end **************  on 14DEC09*/
						.append(" AND UNPOSTED_DUETO = 'BVTAMOUNT_CARRYFORWARD'")
						//Also takes up the unposted amounts of BVT while normal execution
						.append(" GROUP BY NBR_OWNACCOUNT,AMOUNT_TYPE,ID_POOL ,SOURCE_POOL_ID ");

			if (logger.isDebugEnabled()) 
			{
				logger.debug("Amount Unposted Query  =  "+ sbufUnpostedAmt.toString());
			}
			//Preparing the Statements
			conn = LMSConnectionUtility.getConnection();
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			stmt = conn.createStatement();
//			rs=stmt.executeQuery(sbufUnpostedAmt.toString());
			pStmt = conn.prepareStatement(sbufUnpostedAmt.toString());
			pStmt.setString(1, NORMAL_ACCRUAL);
			rs=pStmt.executeQuery();
			/** ****** JTEST FIX end **************  on 14DEC09*/
			//Populating Unposted Amount into the HashMaps
			while (rs.next())
			{
				nbrAcctUnposted = rs.getString("NBR_OWNACCOUNT");
				amtUnposted = rs.getBigDecimal("SUM_AMT");
				amtType=rs.getString("AMOUNT_TYPE");
				
				poolId = rs.getString("ID_POOL");//SUP_IS764
				source_pool_id = rs.getString("SOURCE_POOL_ID"); // NEW_ACCOUNTING_ENTRY_FORMAT 
				//Add this in the HashMap for AmountUnposted
				//Key = poolId+nbrAcctUnposted+amtType
				//Value = amtUnposted
				//SUP_IS764 starts
				//cacheAmtUnposted.put(nbrAcctUnposted+amtType,amtUnposted);
				//cacheAmtUnposted.put(poolId+ExecutionConstants.SEPARATOR+nbrAcctUnposted+ExecutionConstants.SEPARATOR+amtType,amtUnposted);
				cacheAmtUnposted.put(poolId+ExecutionConstants.SEPARATOR+nbrAcctUnposted+ExecutionConstants.SEPARATOR+amtType+ExecutionConstants.SEPARATOR+source_pool_id,amtUnposted); // NEW_ACCOUNTING_ENTRY_FORMAT
				//SUP_IS764 ends
				if (logger.isDebugEnabled()) 
				{
					logger.debug("Amount Unposted Details: AMOUNT = "+ amtUnposted +
							",Account = "+ nbrAcctUnposted +" TXN_CODE ="+amtType+" POOLID = "+poolId + "  source_pool_id "+ source_pool_id);
				}
				
			}
			//ENH_10.3_154 Starts
			cacheAmtUnpostedtemp.putAll(cacheAmtUnposted);
			//ENH_10.3_154 Ends
		/** ****** FindBugs Fix Start ***********  on 14DEC09*/
		}catch(SQLException e){
			logger.fatal(
				  "Exception setting values into OPOOL_ACCOUNT_UNPOSTED ", e);
			 //ENH_IS1059 Start
			 // throw new LMSException("101302", e.toString());
			 throw new LMSException("LMS01000", "Populating unposted amount cache failed for Lot id :-",e);
			 //ENH_IS1059 Ends
		}catch(NamingException e){
			logger.fatal(
				  "Exception setting values into OPOOL_ACCOUNT_UNPOSTED ", e);
			 //ENH_IS1059 Start
			 // throw new LMSException("101302", e.toString());
			 throw new LMSException("LMS01000", "Populating unposted amount cache failed for Lot id :-",e);
			 //ENH_IS1059 Ends
		/** ****** FindBugs Fix End ***********  on 14DEC09*/
		}finally{
			LMSConnectionUtility.close(rs);
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			LMSConnectionUtility.close(stmt);
			LMSConnectionUtility.close(pStmt);
			/** ****** JTEST FIX end **************  on 14DEC09*/
			LMSConnectionUtility.close(conn);
		}//CLOSE CONNECTIN IN FINALLY
		if (logger.isDebugEnabled()){
			logger.debug("Leaving");
		}
	}
	
	//Reallocate to Self starts
	/**
	 * This method is basically used to populate cacheAmtUnposted Hash Map
	 * @param lotId
	 * @throws LMSException
	 */
	//Changed Method Name
	//private void getCacheUnpostedAmountBVT() throws LMSException
	private void getCacheUnpostedAmountCentralCcyBVTForPool() throws LMSException
	{
		Connection conn = null;
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		Statement stmt=null;
		PreparedStatement pStmt = null;
		/** ****** JTEST FIX end **************  on 14DEC09*/
		String nbrAcctUnposted=null;
		String amtType=null;
		BigDecimal amtUnposted=ZERO;
		ResultSet rs = null;
		String poolId = null;//SUP_IS764
		String source_pool_id = null ; // NEW_ACCOUNTING_ENTRY_FORMAT

		if (logger.isDebugEnabled()){
			logger.debug("Entering");
		}
		try{

			StringBuilder sbufUnpostedAmt = new StringBuilder(" SELECT ")
			//SUP_IS764 starts
						//.append(" SUM(AMOUNT)SUM_AMT, NBR_OWNACCOUNT,AMOUNT_TYPE ")
						.append(" SUM(AMOUNT)SUM_AMT, NBR_OWNACCOUNT,AMOUNT_TYPE,ID_POOL ,SOURCE_POOL_ID")
			//SUP_IS764 ends			
						.append(" FROM OPOOL_ACCOUNT_UNPOSTED")
						.append(" WHERE USE_DATE IS NULL ")
						/** ****** JTEST FIX start ************  on 14DEC09*/
//						.append(" AND POSTING_TYPE = '"+NORMAL_ACCRUAL+"'")
						.append(" AND POSTING_TYPE = ?")
						/** ****** JTEST FIX end **************  on 14DEC09*/
						.append(" AND UNPOSTED_DUETO = 'BVTAMOUNT_CARRYFORWARD_CENTRAL_CCY' ")
						//Also takes up the unposted amounts of BVT while normal execution
						.append(" GROUP BY NBR_OWNACCOUNT,AMOUNT_TYPE,ID_POOL ,SOURCE_POOL_ID ");

			if (logger.isDebugEnabled()) 
			{
				logger.debug("Amount Unposted Query  =  "+ sbufUnpostedAmt.toString());
			}
			//Preparing the Statements
			conn = LMSConnectionUtility.getConnection();
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			stmt = conn.createStatement();
//			rs=stmt.executeQuery(sbufUnpostedAmt.toString());
			pStmt = conn.prepareStatement(sbufUnpostedAmt.toString());
			pStmt.setString(1, NORMAL_ACCRUAL);
			rs=pStmt.executeQuery();
			/** ****** JTEST FIX end **************  on 14DEC09*/
			//Populating Unposted Amount into the HashMaps
			while (rs.next())
			{
				nbrAcctUnposted = rs.getString("NBR_OWNACCOUNT");
				amtUnposted = rs.getBigDecimal("SUM_AMT");
				amtType=rs.getString("AMOUNT_TYPE");
				
				poolId = rs.getString("ID_POOL");//SUP_IS764
				source_pool_id = rs.getString("SOURCE_POOL_ID"); // NEW_ACCOUNTING_ENTRY_FORMAT 
				//Add this in the HashMap for AmountUnposted
				//Key = poolId+nbrAcctUnposted+amtType
				//Value = amtUnposted
				//SUP_IS764 starts
				//cacheAmtUnposted.put(nbrAcctUnposted+amtType,amtUnposted);
				//cacheAmtUnposted.put(poolId+ExecutionConstants.SEPARATOR+nbrAcctUnposted+ExecutionConstants.SEPARATOR+amtType,amtUnposted);
				cacheAmtUnpostedCentralCcy.put(poolId+ExecutionConstants.SEPARATOR+nbrAcctUnposted+ExecutionConstants.SEPARATOR+amtType+ExecutionConstants.SEPARATOR+source_pool_id,amtUnposted); // NEW_ACCOUNTING_ENTRY_FORMAT
				//SUP_IS764 ends
				if (logger.isDebugEnabled()) 
				{
					logger.debug("Amount Unposted Details: AMOUNT = "+ amtUnposted +
							",Account = "+ nbrAcctUnposted +" TXN_CODE ="+amtType+" POOLID = "+poolId + "  source_pool_id "+ source_pool_id);
				}
				
			}
			//ENH_10.3_154 Starts
			cacheAmtUnpostedCentralCcytemp.putAll(cacheAmtUnpostedCentralCcy);
			//ENH_10.3_154 Ends
		/** ****** FindBugs Fix Start ***********  on 14DEC09*/
		}catch(SQLException e){
			logger.fatal(
				  "Exception setting values into OPOOL_ACCOUNT_UNPOSTED ", e);
			 //ENH_IS1059 Start
			 // throw new LMSException("101302", e.toString());
			 throw new LMSException("LMS01000", "Populating unposted amount cache failed for Lot id :-",e);
			 //ENH_IS1059 Ends
		}catch(NamingException e){
			logger.fatal(
				  "Exception setting values into OPOOL_ACCOUNT_UNPOSTED ", e);
			 //ENH_IS1059 Start
			 // throw new LMSException("101302", e.toString());
			 throw new LMSException("LMS01000", "Populating unposted amount cache failed for Lot id :-",e);
			 //ENH_IS1059 Ends
		/** ****** FindBugs Fix End ***********  on 14DEC09*/
		}finally{
			LMSConnectionUtility.close(rs);
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			LMSConnectionUtility.close(stmt);
			LMSConnectionUtility.close(pStmt);
			/** ****** JTEST FIX end **************  on 14DEC09*/
			LMSConnectionUtility.close(conn);
		}//CLOSE CONNECTIN IN FINALLY
		if (logger.isDebugEnabled()){
			logger.debug("Leaving");
		}
	}
//Reallocate to Self ends
	/**
	 * This method prepares the queries (for differnt transaction ) and calls the populateAccountInfo method to execute the same.
	 * @param lotId
	 * @param businessDate
	 * @param client
	 * @param insertAccountDtls
	 * @param updateAccountDtls
	 * @param insertAccrualSummary
	 * @param insertPostingSummary
	 * @param updateAccrualSummary
	 * @param updatePostingSummary
	 * @param insertUnpostedAmt
	 * @param updateUnpostedAmt
	 * @param selectAccrualSummary
	 * @param selectPostingSummary
	 * @param selectAccountDtls
	 * @param updateAccountDtlsForDates
	 * @param updateAccuralSummaryForDates
	 * @throws Exception
	 */
	//ENH_10.3_154 Starts
	//SIT2DEV Retro -DD start
	//Added for performance benchmarking start
	/*private void processTXNCodes(long processId ,String lotId, java.util.Date businessDate, String client, HashMap pooldetails,PreparedStatement insertAccountDtls,PreparedStatement updateAccountDtls,
	//Added for performance benchmarking ends
			PreparedStatement insertAccrualSummary, PreparedStatement insertPostingSummary, 
			PreparedStatement updateAccrualSummary, PreparedStatement updatePostingSummary,
			PreparedStatement insertUnpostedAmt , PreparedStatement updateUnpostedAmt,
			PreparedStatement selectAccrualSummary,PreparedStatement selectPostingSummary
			,PreparedStatement selectAccountDtls, PreparedStatement updateAccountDtlsForDates,
			PreparedStatement updateAccuralSummaryForDates,PreparedStatement updatePostingSummaryForDates, PreparedStatement updateBVTAccrualSummaryForDates, PreparedStatement updateBVTPostingSummaryForDates) throws Exception*/ 
	//SIT2DEV Retro -DD end
	private void processTXNCodes(long processId ,String lotId, java.util.Date businessDate, String client, long simId, HashMap pooldetails,PreparedStatement insertAccountDtls,PreparedStatement updateAccountDtls,
		PreparedStatement insertAccrualSummary, PreparedStatement insertPostingSummary, 
		PreparedStatement updateAccrualSummary, PreparedStatement updatePostingSummary,
		//PreparedStatement insertUnpostedAmt , PreparedStatement updateUnpostedAmt,//Reallocate to Self
		PreparedStatement insertUnpostedAmt , PreparedStatement updateUnpostedAmt, PreparedStatement updateUnpostedAmtCentralCcy,
		PreparedStatement selectAccrualSummary,PreparedStatement selectPostingSummary,
		PreparedStatement selectAccountDtls, PreparedStatement updateAccountDtlsForDates,
		PreparedStatement updateAccuralSummaryForDates,PreparedStatement updatePostingSummaryForDates, 
		PreparedStatement updateBVTAccrualSummaryForDates, PreparedStatement updateBVTPostingSummaryForDates) throws Exception
	//ENH_10.3_154 Ends
	{
		if (logger.isDebugEnabled()){
			logger.debug("Entering");
		}
		
		String dateFormat = LMSConstants.DATE_FORMAT;
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
		SimpleDateFormat formatter = new SimpleDateFormat(dateFormat,Locale.US);
		/** ****** JTEST FIX end **************  on 14DEC09*/
		String businessdate = formatter.format(businessDate);
		
		boolean allowIntTyp = ExecutionConstants.FLG_INTACC_POST_INTTYP ; // ENH_10.3_153
		
		boolean flg_newAccountingentry = ExecutionConstants.FlG_NEW_ACCOUNTING_ENTRY_FORMAT; // NEW_ACCOUNTING_ENTRY_FORMAT
		StringBuilder StrNCNDquery = new StringBuilder(0);
		//SUP_IS965....Sun Volume performance teting changes
		//Select Query for TXNCODE - RCINC/RCIND (Normal credit and debit interest)
//SIT2DEV Retro -DD start
		StrNCNDquery.append(" SELECT TXN.COD_GL POOLCOD_GL, TXN.COD_GL ROOTGL,TXN.DAT_FIRST_POSTING, PARTY.COD_GL PARTYCOD_GL ,GL.COD_CALENDAR PARTY_COD_CALENDAR,TXN.DAT_LASTPOSTINGCALC,TXN.PSTCYCLE_CHNG,TXN.ID_ROOTPOOL,PARTY.TYP_PARTICIPANT,TXN.DAT_POST_CREDITDELAY,TXN.DAT_POST_DEBITDELAY, TXN.ID_POOL, PARTY.PARTICIPANT_ID,TXN.DAT_START, NVL(TXN.DAT_END, TO_DATE('31-Dec-9999','DD-Mon-YYYY') ) POOL_END, ") 
		.append(" NVL(PARTY.DAT_END, TO_DATE('31-Dec-9999','DD-Mon-YYYY') )  PARTY_END,PARTY.DAT_BUSINESS_POOLRUN BUSDT, TXN.COD_POST_CYCLE,")
		.append(" POST_CYCLE.NBR_SETTLE_DAYS,POST_CYCLE.NBR_DELAY_DAYS_CR,POST_CYCLE.NBR_DELAY_DAYS_DR,POST_CYCLE.COD_NON_WORKING_DAY, POST_CYCLE.FLG_ISWORKINGDAY, ") ;//JPMC_SUPIS18836
//SIT2DEV Retro -DD end
		if(!allowIntTyp)
		{
			StrNCNDquery.append(" PARTY.COD_TXN || 'I' COD_TXN ,TXN.COD_COUNTER_ACCOUNT, TYP_POSTING FLG_REALPOSTING,FLG_REALPOOL,FLG_CONSOLIDATION FLG_POOL_CONSOLIDATED, ") ; // ENH_10.3_153			
		}
		else
		{
			StrNCNDquery.append(" SUBSTR( PARTY.COD_TXN, 0 , 4 ) ||  partyInt.flg_drcr || 'I' COD_TXN ,TXN.COD_COUNTER_ACCOUNT, TYP_POSTING FLG_REALPOSTING,FLG_REALPOOL,FLG_CONSOLIDATION FLG_POOL_CONSOLIDATED, ") ; // ENH_10.3_153			
		}
		 //Interest and Advantage Seperate Settlement Dates For JPMC
		StrNCNDquery.append(" FLG_DEF_DRCR_CONSOL FLG_ACC_CONSOLIDATED, ");
		if("Y".equalsIgnoreCase(ExecutionConstants.FLG_IS_INTEREST_ADVANTAGE_SEPERATE_SETTLEMENT)){
			StrNCNDquery.append(" (CASE ")
			.append(" WHEN (txn.flg_acc_accrual_pricing_feed = 'N') ")
			.append(" THEN txn.dat_post_settlement ")
			.append(" ELSE (CASE ")
			.append("  WHEN (SUBSTR (cod_txn, 5, 1) = 'C') ")
			.append(" THEN txn.dat_post_creditdelay ")
			.append(" ELSE txn.dat_post_debitdelay ")
			.append("  END ")
			.append("  ) ")
			.append(" END ")
			.append(" ) dat_final_settlement, ");
		}else{
			StrNCNDquery.append(" 	(CASE ")
		.append("                    WHEN (SUBSTR (cod_txn, 5, 1) = 'C') ")
		.append(" 				   THEN  TXN.DAT_POST_CREDITDELAY ")
		.append(" 				   ELSE  TXN.DAT_POST_DEBITDELAY ")
		.append(" 			END )DAT_FINAL_SETTLEMENT,          ") ;
		}
		//Interest and Advantage Seperate Settlement Dates For JPMC
		// ENH_10.3_153 Starts
		if(allowIntTyp)
		{	
			StrNCNDquery.append("              ((CASE ")
			 .append("                    WHEN partyInt.FLG_DRCR = 'D'  AND (   TXN.TYP_ACCTINT_DRACT = 'POST'  ")
			 .append("                          OR (    TXN.AMT_POOL_INTCALC_BALANCE < 0 ")
			 .append("                              AND TXN.TYP_ACCTINT_DRACT = 'SUPPDR' ")
			 .append("                             ) ")
			 .append("                          OR (    TXN.AMT_POOL_INTCALC_BALANCE >= 0 ")
			 .append("                              AND TXN.TYP_ACCTINT_DRACT = 'SUPPCR' ")
			 .append("                             ) ")
			 .append("                         ) ")
			 .append("                        THEN partyInt.amt_interest  ")
			 .append("                    ELSE 0 ")
			 .append("                END ")
			 .append("               ) ")
			 .append("              ) DR_INT, ")
			 .append("                 (CASE ")
			 .append("                    WHEN partyInt.FLG_DRCR = 'C'  AND (   TXN.TYP_ACCTINT_CRACT = 'POST' ")
			 .append("                          OR (    TXN.AMT_POOL_INTCALC_BALANCE < 0 ")
			 .append("                              AND TXN.TYP_ACCTINT_CRACT = 'SUPPDR' ")
			 .append("                             ) ")
			 .append("                          OR (    TXN.AMT_POOL_INTCALC_BALANCE >= 0 ")
			 .append("                              AND TXN.TYP_ACCTINT_CRACT = 'SUPPCR'  ")
			 .append("                             ) ")
			 .append("                         ) ")
			 .append("                        THEN partyInt.amt_interest ")
			 .append("                    ELSE 0 ")
			 .append("                END ")
			 .append("               ) ")
			 .append("               CR_INT,   ") ;
		}
		else 
		{	
			StrNCNDquery.append("              ((CASE ")
				 .append("                    WHEN (   TXN.TYP_ACCTINT_DRACT = 'POST'  ")
				 .append("                          OR (    TXN.AMT_POOL_INTCALC_BALANCE < 0 ")
				 .append("                              AND TXN.TYP_ACCTINT_DRACT = 'SUPPDR' ")
				 .append("                             ) ")
				 .append("                          OR (    TXN.AMT_POOL_INTCALC_BALANCE >= 0 ")
				 .append("                              AND TXN.TYP_ACCTINT_DRACT = 'SUPPCR' ")
				 .append("                             ) ")
				 .append("                         ) ")
				 .append("                        THEN PARTY.AMT_DR_INTEREST ")
				 .append("                    ELSE 0 ")
				 .append("                END ")
				 .append("               ) ")
				 .append("              ) DR_INT, ")
				 .append("                 (CASE ")
				 .append("                    WHEN (   TXN.TYP_ACCTINT_CRACT = 'POST' ")
				 .append("                          OR (    TXN.AMT_POOL_INTCALC_BALANCE < 0 ")
				 .append("                              AND TXN.TYP_ACCTINT_CRACT = 'SUPPDR' ")
				 .append("                             ) ")
				 .append("                          OR (    TXN.AMT_POOL_INTCALC_BALANCE >= 0 ")
				 .append("                              AND TXN.TYP_ACCTINT_CRACT = 'SUPPCR'  ")
				 .append("                             ) ")
				 .append("                         ) ")
				 .append("                        THEN PARTY.AMT_CR_INTEREST ")
				 .append("                    ELSE 0 ")
				 .append("                END ")
				 .append("               ) ")
				 .append("               CR_INT,   ") ;
		}
		// ENH_10.3_153 Ends
		StrNCNDquery.append(" 			  (CASE ")
		.append("                    WHEN (  SUBSTR (PARTY.COD_TXN, 5, 1) = 'C')  ")
		.append(" 				   THEN  PARTY.AMT_MIN_CR ")
		.append(" 				   ELSE  PARTY.AMT_MIN_DR ")
		.append(" 			END ) MINAMT , DAT_ADJUSTEDACCRUAL , DAT_ACCRUAL, TXN.DAT_POSTING, DAT_LASTPOSTING,   ")
		.append(" 			DAT_POST_SETTLEMENT  SETTLEMENT_DATE  , PARTY.COD_PARTY_CCY COD_CCY , txn.FLG_SAMEVALDAT ,TXN.DAT_LASTACCRUAL")
		//ENH_10.3_154 Starts
		//SUP_ENH_III_8106 starts
		//.append(" 			, gl.FLG_BACKVALUE_POSTING")
		.append(" 			, 'Y' FLG_BACKVALUE_POSTING")
		//SUP_ENH_III_8106 ends
		// FLG_POSTINGCHANGES
		// NEW_ACCOUNTING_ENTRY_FORMAT
		.append(" , TXN.FLG_POSTCYCLE ")
		// NEW_ACCOUNTING_ENTRY_FORMAT
		.append(" , TXN.ID_POOL SOURCE_POOL_ID,0 AMT_ALLOCATION_CENTRAL_CCY, 0 RATE_EXRATE_CENTRAL_CCY ");//Reallocate to Self
		//Interest and Advantage Seperate Settlement Dates For JPMC
		//if("Y".equalsIgnoreCase(ExecutionConstants.FLG_IS_INTEREST_ADVANTAGE_SEPERATE_SETTLEMENT)){
			StrNCNDquery.append(" , txn.flg_acc_accrual_pricing_feed FLG_ACC_ACCRUAL_PRICING_FEED "); //Issue_2495
		//}
		//Interest and Advantage Seperate Settlement Dates For JPMC
		//ENH_10.3_154 Ends
//SIT2DEV Retro -DD start
		/* ENH_10.3_153 Starts */
			if(allowIntTyp)
			{
				StrNCNDquery.append(" , partyInt.TYP_INTEREST TYP_INTEREST ") // ENH_10.3_153
							.append("    FROM OPOOL_TXNPOOL_PARTYDTLS PARTY,opool_txnpool_partyintdtls partyInt, OPOOL_TXNPOOLHDR TXN ,ORATE_POSTING_CYCLE POST_CYCLE, ") ;
			}
			else
			{
				StrNCNDquery.append("    FROM OPOOL_TXNPOOL_PARTYDTLS PARTY, OPOOL_TXNPOOLHDR TXN ,ORATE_POSTING_CYCLE POST_CYCLE, ") ;
			}
		/* ENH_10.3_153 Ends */
			//Liquidity Volume Testing Optimized_query starts
//		StrNCNDquery.append(" (SELECT id_pool, cod_gl ")
//		.append("  FROM opool_txnpoolhdr, (SELECT DISTINCT id_rootpool ")
//	
//		.append("                                      FROM opool_txnpoolhdr where nbr_lotid = "+lotId+" and nbr_processid = "+processId+" ) root ")
//			.append("  WHERE id_pool = root.id_rootpool AND nbr_lotid = '"+lotId+"' ) TEMP ,OLM_GLMST GL")
			StrNCNDquery.append("  OLM_GLMST GL")//LMS_45_PERF_CERTIFICATION_PF67
		//Liquidity Volume Testing Optimized_query ends
		
//SIT2DEV Retro -DD end
	//Added for performance benchmarking start
		//.append("    WHERE PARTY.ID_POOL = TXN.ID_POOL ")//Performance change
		.append("    WHERE PARTY.ID_POOL = TXN.ID_POOL and txn.nbr_processid = "+processId) ;
		/* ENH_10.3_153 Starts */
		if(allowIntTyp)
		{
			StrNCNDquery.append("   AND party.NBR_PROCESSID =partyInt.NBR_PROCESSID  ")
						.append("   AND party.ID_POOL=partyInt.ID_POOL   ") 
						.append("   AND party.DAT_BUSINESS_POOLRUN=partyInt.DAT_BUSINESS_POOLRUN   ") 
						.append("   AND party.PARTICIPANT_ID=partyInt.PARTICIPANT_ID   ")
						.append("   AND party.TYP_PARTICIPANT=partyInt.TYP_PARTICIPANT   ")
						.append("   and partyInt.AMT_INTEREST != 0 and partyInt.ACCOUNT_ACCRUAL = 'Y' ") ;
		}		
		/* ENH_10.3_153 Ends */
   
	//Added for performance benchmarking ends
		//Liquidity Volume Testing Optimized_query starts
		StrNCNDquery.append("      AND TXN.NBR_PROCESSID = PARTY.NBR_PROCESSID ")//LMS_45_PERF_CERTIFICATION_PF67
		.append("      AND TXN.DAT_BUSINESS_POOLRUN = PARTY.DAT_BUSINESS_POOLRUN  ")
		.append("      AND TXN.TXT_STATUS = 'ALLOCATED'   ")
		.append("      AND PARTY.TYP_PARTICIPANT = 'ACCOUNT'  ")
		.append("      AND PARTY.ID_BVT = '0' ")
		.append(" 	 AND   PARTY.DAT_BUSINESS_POOLRUN = TO_DATE('"+ businessdate+"','" + dateFormat + "')  ")
		.append(" 	 AND TXN.NBR_LOTID = "+lotId+" AND TXN.COD_POST_CYCLE = POST_CYCLE.COD_POST_CYCLE   ")
		//Liquidity Volume Testing Optimized_query ends

		//SUP_IS965..Sun volume performance testing
		//.append("    AND TYP_POSTING <> 'N'")
//SIT2DEV Retro -DD start
		.append("    AND TYP_POSTING <> 'N'")
		//.append("    AND TXN.ID_PARENT_POOL IS NULL")//SUP_IS1364 //LMS_45_PERF_CERTIFICATION_PF67 //ISSUE_2452 Fixes
		.append("    AND GL.COD_GL= PARTY.COD_GL ");//SUP_IS1364
//SIT2DEV Retro -DD end
		//.append("    ORDER BY PARTICIPANT_ID ");
		//SUP_IS965..Sun volume performance testing

//TXN.COD_POST_CYCLE = POST_CYCLE.COD_POST_CYCLE  col : FLG_SAMEVALDAT
		if (logger.isDebugEnabled()){
			logger.debug("SELECT QUERY FOR TXNCODE - RCINC/RCIND (NORMAL CREDIT AND DEBIT INTEREST) : "+ StrNCNDquery.toString());
		}
		//Call method to prepare data object 
//SIT2DEV Retro -DD start
//Retro form 9.8
//		SUP_IS1322 - one more paramenter pooltype has been passed
		populateAccountInfo(NORMAL_INTEREST,lotId,businessDate, client, simId, pooldetails,insertAccountDtls,updateAccountDtls, insertAccrualSummary,
//SIT2DEV Retro -DD end
				insertPostingSummary, updateAccrualSummary, updatePostingSummary,insertUnpostedAmt,
				//updateUnpostedAmt,StrNCNDquery.toString(),selectAccrualSummary,selectPostingSummary,//Reallocate to Self
				updateUnpostedAmt,updateUnpostedAmt,StrNCNDquery.toString(),selectAccrualSummary,selectPostingSummary,
				//ENH_10.3_154 Starts
				selectAccountDtls,updateAccountDtlsForDates,updateAccuralSummaryForDates,updatePostingSummaryForDates,
				updateBVTAccrualSummaryForDates,updateBVTPostingSummaryForDates);
				//ENH_10.3_154 Ends
		//Select Query for 
		//TXNCODE - RCANC/RCAND (Normal credit and debit advantage to participants and Central A/C)
		//TXNCODE - RCPNC/RCPND (Normal Reallocation to participants)
		//TXNCODE - RCRNC/RCRND (Normal Rebate to participants)

		StrNCNDquery.replace(0,StrNCNDquery.length(),"");
//SIT2DEV Retro -DD start
		//SIT issue fix starts - txn.ID_PARENT_POOL added //LMS_45_PERF_CERTIFICATION_PF38 start
		StrNCNDquery.append(" SELECT TXN.COD_GL POOLCOD_GL,'"+client+"' ROOTGL,TXN.DAT_FIRST_POSTING,gl.COD_CALENDAR PARTY_COD_CALENDAR,ALLOC.COD_GL PARTYCOD_GL,ALLOC.TYP_PARTICIPANT,TXN.DAT_POST_CREDITDELAY,TXN.DAT_POST_DEBITDELAY,TXN.ID_POOL,txn.ID_PARENT_POOL, ALLOC.PARTICIPANT_ID PARTICIPANT_ID,ALLOC.COD_TXN || 'A' COD_TXN,FLG_ALLOCATION,FLG_REAL,TXN.FLG_REALPOOL, ")
		
		//SIT issue fix ends
		.append(" TXN.COD_POST_CYCLE,DAT_LASTPOSTINGCALC,PSTCYCLE_CHNG,  ")
		.append(" POST_CYCLE.NBR_SETTLE_DAYS,POST_CYCLE.NBR_DELAY_DAYS_CR,POST_CYCLE.NBR_DELAY_DAYS_DR,POST_CYCLE.COD_NON_WORKING_DAY, POST_CYCLE.FLG_ISWORKINGDAY, ") //JPMC_SUPIS18836
//SIT2DEV Retro -DD end
		.append(" TYP_SETTLE,TXN.DAT_START,TXN.DAT_BUSINESS_POOLRUN BUSDT, ")
		.append(" NVL(TXN.DAT_END, TO_DATE('31-Dec-9999','DD-Mon-YYYY') ) POOL_END, NVL(PARTY.DAT_END, TO_DATE('31-Dec-9999','DD-Mon-YYYY') )  PARTY_END, ") 
		
		//ENH_IS1166 Starts
//SIT2DEV Retro -DD start
		//.append(" (CASE WHEN (EQV_AMT_ALLOCATION < 0 AND SUBSTR (ALLOC.COD_TXN , 5, 1) = 'D' ) THEN EQV_AMT_ALLOCATION * -1 ELSE  EQV_AMT_ALLOCATION END )AMT_CALCULATED,  TXN.DAT_LASTACCRUAL, DAT_ADJUSTEDACCRUAL, TXN.DAT_ACCRUAL, TXN.DAT_LASTPOSTING,  ")
		/* ENH_10.3_153 starts */
		
		
		.append(" 			  (CASE ")
		.append("                    WHEN (  SUBSTR (ALLOC.COD_TXN , 5, 1) = 'D')  ")
		.append(" 				   THEN  alloc.EQV_AMT_ALLOCATION * -1 ")
		.append(" 				   ELSE  alloc.EQV_AMT_ALLOCATION ")
		.append(" 			END ) AMT_CALCULATED , "); 
		
		
		
		if(allowIntTyp)
		{
			StrNCNDquery.append(" TXN.DAT_LASTACCRUAL, DAT_ADJUSTEDACCRUAL, TXN.DAT_ACCRUAL, TXN.DAT_LASTPOSTING,  ") ;			
		}
		else
		{
			//StrNCNDquery.append(" (CASE WHEN (SUBSTR (ALLOC.COD_TXN , 5, 1) = 'D' ) THEN alloc.EQV_AMT_ALLOCATION * -1 ELSE  alloc.EQV_AMT_ALLOCATION END )AMT_CALCULATED,  TXN.DAT_LASTACCRUAL, DAT_ADJUSTEDACCRUAL, TXN.DAT_ACCRUAL, TXN.DAT_LASTPOSTING,  ") ;//JPMC RETRO ISSUE FIXED			
			 StrNCNDquery.append("   TXN.DAT_LASTACCRUAL, DAT_ADJUSTEDACCRUAL, TXN.DAT_ACCRUAL, TXN.DAT_LASTPOSTING,  ") ;//Sanity_V10.5--Above query commented
		}
		/* ENH_10.3_153 ends */
//SIT2DEV Retro -DD end
		//ENH_IS1166 Ends
		
		StrNCNDquery.append("  TXN.DAT_POSTING,TXN.DAT_POST_SETTLEMENT SETTLEMENT_DATE , TYP_POSTING FLG_REALPOSTING,FLG_CONSOLIDATION FLG_POOL_CONSOLIDATED, ")
		.append("  FLG_DEF_DRCR_CONSOL FLG_ACC_CONSOLIDATED,TXN.COD_COUNTER_ACCOUNT, ")
		.append(" 			 (CASE ")
		.append("                    WHEN (  SUBSTR (ALLOC.COD_TXN, 5, 1) = 'C')  ")
		.append(" 				   THEN  TXN.DAT_POST_CREDITDELAY  ")
		.append(" 				   ELSE  TXN.DAT_POST_DEBITDELAY  ")
		.append(" 			       END  ")
		.append(" 			)DAT_FINAL_SETTLEMENT   , ")
		.append(" 			  (CASE ")
		.append("                    WHEN (  SUBSTR (PARTY.COD_TXN , 5, 1) = 'C')  ")
		.append(" 				   THEN  PARTY.AMT_MIN_CR ")
		.append(" 				   ELSE  PARTY.AMT_MIN_DR ")
		.append(" 			END ) MINAMT ,ALLOC.COD_CCY_ACCOUNT COD_CCY,TXN.FLG_SAMEVALDAT,TXN.DAT_LASTACCRUAL")
		//ENH_10.3_154 Starts
//Retro_Mashreq
		//.append(" 			, gl.FLG_BACKVALUE_POSTING") ;
		//SUP_ENH_III_8106 starts
		//.append(" 			, gl.FLG_BACKVALUE_POSTING,alloc.narration") //MASHREQ_4647
		.append(" 			, 'Y' FLG_BACKVALUE_POSTING,alloc.narration") 
		//SUP_ENH_III_8106 ends
		// FLG_POSTINGCHANGES
		.append(" , TXN.FLG_POSTCYCLE ");
		//ENH_10.3_154 Ends
		/* ENH_10.3_153 starts */
		if(allowIntTyp)
		{
			StrNCNDquery.append("       ,ALLOC.TYP_INTEREST ") ;
			//Retro from JPMC LIQ_REL_PROD_9.11_016_E - SIT Starts
			//StrNCNDquery.append("             FROM  (" +  getTxnPoolAllocationDtls()   + ")  ALLOC, ");
		}
		// NEW_ACCOUNTING_ENTRY_FORMAT
		//StrNCNDquery.append("       , SOURCE_POOL_ID") ;
		StrNCNDquery.append("       , alloc.SOURCE_POOL_ID,alloc.AMT_ALLOCATION_CENTRAL_CCY, alloc.RATE_EXRATE_CENTRAL_CCY ") ;//Reallocate to Self
		/*else
		{
			StrNCNDquery.append("             FROM OPOOL_TXNPOOL_ALLOCATIONDTLS ALLOC, ") ;
		}*/
		StrNCNDquery.append("             FROM  OPOOL_TXNPOOL_ALLOCATIONDTLS  ALLOC, orate_interest_type int_type  ");
		//Retro from JPMC LIQ_REL_PROD_9.11_016_E - SIT Ends
		/* ENH_10.3_153 ends */
		StrNCNDquery.append(" 			, OPOOL_TXNPOOL_PARTYDTLS PARTY ")
//SIT2DEV Retro -DD start
		.append("            ,  OPOOL_TXNPOOLHDR TXN  ,ORATE_POSTING_CYCLE POST_CYCLE  ")
		//Liquidity Volume Testing Optimized_query
		/*
		 * Commented below part of query for Liquidity Volume Testing Optimized_query
		 */
/*		.append(" (SELECT id_pool, cod_gl ")
		.append("  FROM opool_txnpoolhdr, (SELECT DISTINCT id_rootpool ")
		.append("                                      FROM opool_txnpoolhdr) root ")
		.append("  WHERE id_pool = root.id_rootpool AND nbr_lotid = '"+lotId+"' ) TEMP,")*/
		//PERFORMANCE COMMENT .append("OPOOL_RP_GL TEMP,")
		//Liquidity Volume Testing Optimized_query
		.append(" , OLM_GLMST gl")
//SIT2DEV Retro -DD end
		.append("            WHERE TXN.ID_POOL = ALLOC.ID_POOL   ")
		//Liquidity Volume Testing Optimized_query
		.append("   AND  TXN.nbr_lotid = "+lotId+ " ")
		.append("   AND  TXN.nbr_processid = "+processId+ " ")

		//Liquidity Volume Testing Optimized_query
		.append("              AND TXN.NBR_PROCESSID = ALLOC.NBR_PROCESSID    ")
		.append("              AND TXN.DAT_BUSINESS_POOLRUN = ALLOC.DAT_BUSINESS_POOLRUN   ")
		.append(" 			 AND PARTY.PARTICIPANT_ID = ALLOC.PARTICIPANT_ID  ")
		.append(" 			 AND PARTY.DAT_BUSINESS_POOLRUN = TXN.DAT_BUSINESS_POOLRUN  ")
		.append(" 			 AND PARTY.NBR_PROCESSID = TXN.NBR_PROCESSID  ")
		.append(" 			 AND PARTY.ID_POOL = TXN.ID_POOL ")
		.append("           AND TXN.TXT_STATUS = 'ALLOCATED'   ")
		.append("              AND   PARTY.TYP_PARTICIPANT =  'ACCOUNT' ")
		.append("             AND ALLOC.typ_participant = PARTY.TYP_PARTICIPANT AND ALLOC.id_bvt = 0 ") // ISSUE_2428 Fixes
	
		.append(" 			AND ALLOC.TYP_INTEREST=INT_TYPE.TYP_INTEREST(+) ")
		.append(" 			AND NVL(INT_TYPE.flg_interestypeas_fee,'N')='N' ");

		
		if(!allowIntTyp)
		{
			 StrNCNDquery.append("              AND ALLOC.ID_BVT = '0'   "); // ENH_10.3_153			
		}
		StrNCNDquery.append(" 	and TXN.COD_POST_CYCLE = POST_CYCLE.COD_POST_CYCLE")
//SIT2DEV Retro -DD start
		.append(" 			AND GL.COD_GL=ALLOC.COD_GL ")
//SIT2DEV Retro -DD end
		//SUP_IS965..Sun volume Performance teting
		//.append(" 			AND  ALLOC.DAT_BUSINESS_POOLRUN = TO_DATE('"+ businessdate+"','" + dateFormat + "')    ")
		.append(" 			AND  ALLOC.DAT_BUSINESS_POOLRUN = TO_DATE('"+ businessdate+"','" + dateFormat + "')    ");
		//.append(" UNION ")
		if (logger.isDebugEnabled())
			logger.debug("SELECT QUERY FOR TXNCODE - RCANC/RCAND ,RCPNC/RCPND,RCRNC/RCRND 1: "+ StrNCNDquery.toString());

		
		//LMS_45_PERF_CERTIFICATION_PF38 end
//SIT2DEV Retro -DD start
		//Call method to prepare data object 
		populateAccountInfo(NORMAL_ALLOC_REALLOC_REBATE,lotId,businessDate, client,  simId, pooldetails,insertAccountDtls,updateAccountDtls,
//SIT2DEV Retro -DD end
				insertAccrualSummary, insertPostingSummary, updateAccrualSummary, updatePostingSummary,
				//insertUnpostedAmt,updateUnpostedAmt,StrNCNDquery.toString(),selectAccrualSummary,//Reallocate to Self
				insertUnpostedAmt,updateUnpostedAmt,updateUnpostedAmtCentralCcy,StrNCNDquery.toString(),selectAccrualSummary,
				//ENH_10.3_154 Starts
				//selectPostingSummary,selectAccountDtls,updateAccountDtlsForDates,updateAccuralSummaryForDates,updatePostingSummaryForDates);
				selectPostingSummary,selectAccountDtls,updateAccountDtlsForDates,updateAccuralSummaryForDates,updatePostingSummaryForDates,
				updateBVTAccrualSummaryForDates,updateBVTPostingSummaryForDates);
				//ENH_10.3_154 Ends
		
		StrNCNDquery.replace(0,StrNCNDquery.length(),"");
//ENH_IS997 starts
		//CCB_SUP_IS760 Starts LAST CHAR OF TXNCODE WILL BE 'C' FOR CENTRAL ACCOUNTS
		//.append(" SELECT ALLOC.TYP_PARTICIPANT,TXN.DAT_POST_CREDITDELAY,TXN.DAT_POST_DEBITDELAY,TXN.ID_POOL,  ALLOC.PARTICIPANT_ID PARTICIPANT_ID,ALLOC.COD_TXN || 'A' COD_TXN,FLG_ALLOCATION,FLG_REAL,TXN.FLG_REALPOOL, ")
//		StrNCNDquery.append(" SELECT ALLOC.TYP_PARTICIPANT,TXN.DAT_POST_CREDITDELAY,TXN.DAT_POST_DEBITDELAY,TXN.ID_POOL,  ALLOC.PARTICIPANT_ID PARTICIPANT_ID,ALLOC.COD_TXN || 'A' COD_TXN,FLG_ALLOCATION,FLG_REAL,TXN.FLG_REALPOOL, ")
//SIT2DEV Retro -DD start
		//SIT issue fix starts - txn.ID_PARENT_POOL added
		StrNCNDquery.append(" SELECT TXN.COD_GL POOLCOD_GL, TXN.COD_GL ROOTGL,TXN.DAT_FIRST_POSTING,GL.COD_CALENDAR PARTY_COD_CALENDAR,ALLOC.COD_GL PARTYCOD_GL,ALLOC.TYP_PARTICIPANT,TXN.DAT_POST_CREDITDELAY,TXN.DAT_POST_DEBITDELAY,TXN.ID_POOL, txn.ID_PARENT_POOL,  ALLOC.PARTICIPANT_ID PARTICIPANT_ID,ALLOC.COD_TXN || 'C' COD_TXN,FLG_ALLOCATION,FLG_REAL,TXN.FLG_REALPOOL, ")
		//SIT issue fix ends
//SIT2DEV Retro -DD end
		//SUP_IS965..Sun volume Performance testing
		//CCB_SUP_IS760 Ends
//ENH_IS997 Ends
//SIT2DEV Retro -DD start
		.append(" TXN.COD_POST_CYCLE,DAT_LASTPOSTINGCALC,PSTCYCLE_CHNG,")
		.append(" POST_CYCLE.NBR_SETTLE_DAYS,POST_CYCLE.NBR_DELAY_DAYS_CR,POST_CYCLE.NBR_DELAY_DAYS_DR,POST_CYCLE.COD_NON_WORKING_DAY, POST_CYCLE.FLG_ISWORKINGDAY, ") //JPMC_SUPIS18836
//SIT2DEV Retro -DD end
		.append(" TYP_SETTLE,TXN.DAT_START, TXN.DAT_BUSINESS_POOLRUN BUSDT, ")
		.append(" NVL(TXN.DAT_END, TO_DATE('31-Dec-9999','dd-Mon-yyyy') ) POOL_END,NVL(TXN.DAT_END, TO_DATE('31-Dec-9999','dd-Mon-yyyy') ) PARTY_END, ")
		//Retro from JPMC LIQ_REL_PROD_9.11_016_E - SIT Starts
		//.append(" (CASE WHEN (TOTAL_DECISION_AMT < 0 AND SUBSTR (ALLOC.COD_TXN , 5, 1) = 'D') THEN EQV_AMT_ALLOCATION * -1 ELSE  EQV_AMT_ALLOCATION END )AMT_CALCULATED,  TXN.DAT_LASTACCRUAL, DAT_ADJUSTEDACCRUAL, TXN.DAT_ACCRUAL, TXN.DAT_LASTPOSTING,  				      ")
		.append(" AMT_CALCULATED,  TXN.DAT_LASTACCRUAL, DAT_ADJUSTEDACCRUAL, TXN.DAT_ACCRUAL, TXN.DAT_LASTPOSTING,  				      ")
		//Retro from JPMC LIQ_REL_PROD_9.11_016_E - SIT Ends
		.append(" TXN.DAT_POSTING,TXN.DAT_POST_SETTLEMENT SETTLEMENT_DATE ,	TYP_POSTING FLG_REALPOSTING,FLG_CONSOLIDATION FLG_POOL_CONSOLIDATED, ")
		.append(" 'Y' FLG_ACC_CONSOLIDATED,TXN.COD_COUNTER_ACCOUNT, ")
		.append(" 			 (CASE ")
		.append("                    WHEN (    SUBSTR (ALLOC.COD_TXN, 5, 1) = 'C' ) ")
		.append(" 				   THEN  TXN.DAT_POST_CREDITDELAY  ")
		.append(" 				   ELSE  TXN.DAT_POST_DEBITDELAY  ")
		.append(" 			       END  ")
		.append(" 			)DAT_FINAL_SETTLEMENT,0 MINAMT ,ALLOC.COD_CCY_ACCOUNT COD_CCY ,TXN.FLG_SAMEVALDAT,TXN.DAT_LASTACCRUAL ")  
		//ENH_10.3_154 Starts
////Retro_Mashreq
		//.append(" 			, gl.FLG_BACKVALUE_POSTING") ;
		//SUP_ENH_III_8106 starts
		//.append(" 			, gl.FLG_BACKVALUE_POSTING,alloc.narration")  //MASHREQ_4647
		.append(" 			, 'Y' FLG_BACKVALUE_POSTING,alloc.narration")
		//SUP_ENH_III_8106 ends
		// FLG_POSTINGCHANGES
		.append(" , TXN.FLG_POSTCYCLE ");
		//ENH_10.3_154 Ends
		if(allowIntTyp)
		{
			StrNCNDquery.append(", ALLOC.TYP_INTEREST ") ;// ENH_10.3_153
		}
		// NEW_ACCOUNTING_ENTRY_FORMAT
		StrNCNDquery.append("       , SOURCE_POOL_ID ,alloc.AMT_ALLOCATION_CENTRAL_CCY, alloc.RATE_EXRATE_CENTRAL_CCY  ") ; //Reallocate to Self

		//Retro from JPMC LIQ_REL_PROD_9.11_016_E - SIT Starts
		//StrNCNDquery.append("             FROM OPOOL_TXNPOOL_ALLOCATIONDTLS ALLOC,    ")
		StrNCNDquery.append("             FROM  (" +  getTxnPoolAllocationDtls("CENTRAL")   + ")  ALLOC, ")
		//Retro from JPMC LIQ_REL_PROD_9.11_016_E - SIT Ends
//SIT2DEV Retro -DD start
		.append("              OPOOL_TXNPOOLHDR TXN ,ORATE_POSTING_CYCLE POST_CYCLE,  ")
	//Liquidity Volume Testing Optimized_query
		/*
		 * Commented below part of query for Liquidity Volume Testing Optimized_query
		 */
/*		.append(" (SELECT id_pool, cod_gl ")
		.append("  FROM opool_txnpoolhdr, (SELECT DISTINCT id_rootpool ")
		.append("                                      FROM opool_txnpoolhdr) root ")
		.append("  WHERE id_pool = root.id_rootpool AND nbr_lotid = '"+lotId+"' ) TEMP,")*/
		//.append("OPOOL_RP_GL TEMP,") //LMS_45_PERF_CERTIFICATION_PF67
		//Liquidity Volume Testing Optimized_query
		.append("OLM_GLMST gl")
//SIT2DEV Retro -DD end
		.append("            WHERE TXN.ID_POOL = ALLOC.ID_POOL   ")
			//Liquidity Volume Testing Optimized_query
		//LMS_45_PERF_CERTIFICATION_PF67 START
		/*
		.append("   AND  TEMP.nbr_lotid = "+lotId+ " ")
		*/
		//LMS_45_PERF_CERTIFICATION_PF67 END
		//Liquidity Volume Testing Optimized_query
		.append("              AND TXN.NBR_PROCESSID = ALLOC.NBR_PROCESSID   ")
		.append("              AND TXN.DAT_BUSINESS_POOLRUN = ALLOC.DAT_BUSINESS_POOLRUN   ")
		.append("          AND TXN.TXT_STATUS = 'ALLOCATED'   ")
		//Retro from JPMC LIQ_REL_PROD_9.11_016_E - SIT Starts
		//.append("              AND   ALLOC.TYP_PARTICIPANT =  'CENTRAL'    ")
		//.append("              AND ALLOC.ID_BVT = '0'   ")
		//Retro from JPMC LIQ_REL_PROD_9.11_016_E - SIT Ends
		.append(" 		AND TXN.NBR_LOTID = "+lotId+" AND TXN.COD_POST_CYCLE = POST_CYCLE.COD_POST_CYCLE  ")
//SIT2DEV Retro -DD start
		//.append(" 			AND TXN.ID_PARENT_POOL IS NULL ")//LMS_45_PERF_CERTIFICATION_PF67 //ISSUE_2452 Fixes
		.append(" 			AND GL.COD_GL=ALLOC.COD_GL ")
		
				.append("   AND  TXN.nbr_processid = "+processId+ " ")
//SIT2DEV Retro -DD end 
		.append(" 		AND  ALLOC.DAT_BUSINESS_POOLRUN = TO_DATE('"+ businessdate+"','" + dateFormat + "')      ");

		
		if (logger.isDebugEnabled()){

		//SUP_IS965...sun Volume performance testing
		//logger.debug("SELECT QUERY FOR TXNCODE - RCANC/RCAND ,RCPNC/RCPND,RCRNC/RCRND : "+ StrNCNDquery.toString());
			logger.debug("SELECT QUERY FOR TXNCODE - RCANC/RCAND ,RCPNC/RCPND,RCRNC/RCRND : "+ StrNCNDquery.toString());
		//SUP_IS965...sun Volume performance testing
		}
//SIT2DEV Retro -DD start
		//Call method to prepare data object 
		populateAccountInfo(NORMAL_ALLOC_REALLOC_REBATE,lotId,businessDate, client, simId, pooldetails, insertAccountDtls,updateAccountDtls,
//SIT2DEV Retro -DD end
				insertAccrualSummary, insertPostingSummary, updateAccrualSummary, updatePostingSummary,
				//insertUnpostedAmt,updateUnpostedAmt,StrNCNDquery.toString(),selectAccrualSummary,//Reallocate to Self
				insertUnpostedAmt,updateUnpostedAmt,updateUnpostedAmtCentralCcy,StrNCNDquery.toString(),selectAccrualSummary,
				//ENH_10.3_154 Starts
				//selectPostingSummary,selectAccountDtls,updateAccountDtlsForDates,updateAccuralSummaryForDates,updatePostingSummaryForDates);
				selectPostingSummary,selectAccountDtls,updateAccountDtlsForDates,updateAccuralSummaryForDates,updatePostingSummaryForDates,
				updateBVTAccrualSummaryForDates,updateBVTPostingSummaryForDates);
				//ENH_10.3_154 Ends
		//Select Query for 
		//TXNCODE - RCANC/RCAND (Normal credit and debit advantage to Bank and Treasury A/C) 
		//Need to multiply by -1 if amount is -ve , also while setting TXNCODE SET it as ICANCA/ICANDA of Pool is Info
		StrNCNDquery.replace(0,StrNCNDquery.length(),"");
//SIT2DEV Retro -DD start
		//ENH_10.3_154 Starts
		//StrNCNDquery.append(" SELECT BANK_TREASURY.* , ACC.cod_gl PARTYCOD_GL,gl.COD_CALENDAR PARTY_COD_CALENDAR,ACC.COD_CCY COD_CCY ,TEMP.cod_gl ROOTGL FROM (SELECT TXN.DAT_FIRST_POSTING,TXN.DAT_POST_CREDITDELAY,TXN.DAT_POST_DEBITDELAY," )
		StrNCNDquery.append(" SELECT BANK_TREASURY.* , ACC.cod_gl PARTYCOD_GL,gl.COD_CALENDAR PARTY_COD_CALENDAR,ACC.COD_CCY COD_CCY ,BANK_TREASURY.poolcod_gl ROOTGL ")
		//SUP_ENH_III_8106 starts
		//.append(" , gl.FLG_BACKVALUE_POSTING ")
		.append(" , 'Y' FLG_BACKVALUE_POSTING ")
		//SUP_ENH_III_8106 ends
		.append(" FROM (SELECT TXN.DAT_FIRST_POSTING,TXN.DAT_POST_CREDITDELAY,TXN.DAT_POST_DEBITDELAY," )
		//ENH_10.3_154 Ends

		.append("ID_ROOTPOOL,TXN.COD_POST_CYCLE,TXN.DAT_LASTPOSTINGCALC,TXN.PSTCYCLE_CHNG,")
		.append(" POST_CYCLE.NBR_SETTLE_DAYS,POST_CYCLE.NBR_DELAY_DAYS_CR,POST_CYCLE.NBR_DELAY_DAYS_DR,POST_CYCLE.COD_NON_WORKING_DAY, POST_CYCLE.FLG_ISWORKINGDAY, ") //JPMC_SUPIS18836
//SIT2DEV Retro -DD end
		//if amount is -ve then multiply by -ve as TXNCODE is D
//SIT2DEV Retro -DD start
		.append(" (CASE WHEN (NVL (TOTAL_DECISION_AMT, 0) < 0 ) THEN (AMT_BANKSHARE_ADV * -1) ELSE  AMT_BANKSHARE_ADV END) AMT_CALCULATED, ")
//SIT2DEV Retro -DD end
		//.append(" NVL (AMT_BANKSHARE_ADV, 0) AMT_CALCULATED ,")
		.append(" TXN.ID_POOL, NBR_ROLE_OWNACCOUNT PARTICIPANT_ID,  ")//MASHREQ_4760
		.append(" NVL(TXN.DAT_END,TO_DATE('31-Dec-9999','DD-Mon-YYYY') ) POOL_END,    ")
		.append(" 		  (CASE  ")
//SIT2DEV Retro -DD stat
		.append("             WHEN NVL (TOTAL_DECISION_AMT, 0) >= 0 AND TXN.FLG_REALPOOL = 'Y' ")//REAL CR 
//SIT2DEV Retro -DD end
//ENH_IS997 starts

		//CCB_SUP_IS760 Starts LAST CHAR OF TXNCODE WILL BE 'B' FOR BANKSHARE ACCOUNTS 
		//.append("                 THEN 'RCANCA'   ")
		.append("                 THEN 'RCANCB'   ")
//SIT2DEV Retro -DD start
		.append("             WHEN NVL (TOTAL_DECISION_AMT, 0) < 0  AND TXN.FLG_REALPOOL = 'Y' ")//REAL DR
//SIT2DEV Retro -DD end

		//.append("                 THEN 'RCANDA'   ")
		.append("                 THEN 'RCANDB'   ")
//SIT2DEV Retro -DD strart
		.append("             WHEN NVL (TOTAL_DECISION_AMT, 0) >= 0 AND TXN.FLG_REALPOOL = 'N' ")//INFO CR 
//SIT2DEV Retro -DD  end

		//.append("                 THEN 'ICANCA'   ")
		.append("                 THEN 'ICANCB'   ")
//SIT2DEV Retro -DD start
		.append("             WHEN NVL (TOTAL_DECISION_AMT, 0) < 0  AND TXN.FLG_REALPOOL = 'N' ")//INFO DR
//SIT2DEV Retro -DD end

		//.append("                 THEN 'ICANDA'   ")
		.append("                 THEN 'ICANDB'   ")
		//CCB_SUP_IS760 Ends
//ENH_IS997 ends
		//.append("             ELSE 'RCANDA'   ")
		.append("         END  ")
		.append("        )COD_TXN,  ")
		.append("        'BANKSHARE' TYP_PARTICIPANT,  TXN.DAT_START, ")
		.append(" 		 TXN.DAT_LASTACCRUAL, DAT_ADJUSTEDACCRUAL,TXN.ID_PARENT_POOL, ")//LMS_45_PERF_CERTIFICATION_PF67
		.append("        TXN.DAT_ACCRUAL, TXN.DAT_LASTPOSTING, TXN.DAT_POSTING, ")
		.append("        TXN.DAT_POST_SETTLEMENT SETTLEMENT_DATE,TO_DATE('31-Dec-9999','DD-Mon-YYYY') PARTY_END,TXN.DAT_BUSINESS_POOLRUN BUSDT, ")
		.append(" 	   TXN.COD_COUNTER_ACCOUNT,TYP_POSTING FLG_REALPOSTING,TXN.FLG_REALPOOL,FLG_CONSOLIDATION FLG_POOL_CONSOLIDATED, ")
		.append(" 	  (CASE  ")
//SIT2DEV Retro -DD start
		.append("             WHEN NVL (TOTAL_DECISION_AMT, 0) >= 0 ")
//SIT2DEV Retro -DD end
		.append("                 THEN TXN.DAT_POST_CREDITDELAY   ")
		.append("             ELSE TXN.DAT_POST_DEBITDELAY    ")
//SIT2DEV Retro -DD start
		.append("          END )DAT_FINAL_SETTLEMENT ,TXN.FLG_SAMEVALDAT , TXN.COD_GL POOLCOD_GL")
		// FLG_POSTINGCHANGES
		.append(" , TXN.FLG_POSTCYCLE ");
//SIT2DEV Retro -DD end
		if(allowIntTyp)
		{
			StrNCNDquery.append(",  '' TYP_INTEREST "); // ENH_10.3_153
		}
		// NEW_ACCOUNTING_ENTRY_FORMAT
		StrNCNDquery.append(", txn.ID_POOL SOURCE_POOL_ID ,0 AMT_ALLOCATION_CENTRAL_CCY,0 RATE_EXRATE_CENTRAL_CCY"); //Reallocate to Self
		StrNCNDquery.append("   FROM OPOOL_TXNPOOLHDR TXN, OPOOL_GLLINK BLELINK ,OPOOL_GLLINK_DTLS blelinkdtls,ORATE_POSTING_CYCLE POST_CYCLE")
		//ANZ_SIT_ISSUE starts
		.append("  WHERE (amt_bankshare_adv <> 0 or amt_bankshare_adv is not null) ")

		//.append(" 	AND  (PERCENT_BANKSHARE IS NOT NULL)  ") //Issue fix - Do not generate Zero Bankshare entries
		//ANZ_SIT_ISSUE ends
		.append("  AND  BLELINK.COD_GL = TXN.COD_GL  AND  blelink.GLLINK_MASTER_ID = blelinkdtls.GLLINK_MASTER_ID AND blelinkdtls.NBR_ROLE_ID=7 AND TXN.TXT_STATUS = 'ALLOCATED' ")//MASHREQ_4760
		//.append("    AND BLELINK.COD_BNK_DEL = TXN.COD_BASE_BNK_LGL ")//MASHREQ_4760
		/* ISSUE_2407 Start */
		.append("    AND (BLELINK.COD_BNK_DEL = TXN.COD_BASE_BNK_LGL ")//MASHREQ_4760
		.append("    OR (BLELINK.COD_BNK_DEL IS NULL AND NOT EXISTS (SELECT 1 FROM OPOOL_GLLINK glink,OPOOL_GLLINK_DTLS dtls WHERE  ")
		.append("    TXN.COD_GL=glink.COD_GL AND TXN.COD_BASE_BNK_LGL= glink.COD_BNK_DEL AND TXN.COD_BASE_CCY_POOL = glink.COD_CCY  ")
		.append("    AND dtls.GLLINK_MASTER_ID = glink.GLLINK_MASTER_ID AND dtls.NBR_ROLE_ID  =7)))  ")
		/* ISSUE_2407 End */
		.append("    AND BLELINK.COD_CCY = TXN.COD_BASE_CCY_POOL ")
		.append("    AND TXN.ID_BVT = '0' ")
		.append("    AND TXN.COD_ALLOCATIONSUM = 'A'  ")
		.append(" AND TXN.NBR_LOTID = "+lotId+"   AND TXN.COD_POST_CYCLE = POST_CYCLE.COD_POST_CYCLE    ")
		.append("   AND  TXN.DAT_BUSINESS_POOLRUN = TO_DATE('"+ businessdate+"','" + dateFormat + "')        ")
		.append(" UNION ")
//SIT2DEV Retro -DD start
		.append(" SELECT TXN.DAT_FIRST_POSTING,TXN.DAT_POST_CREDITDELAY,TXN.DAT_POST_DEBITDELAY, ")
		.append("ID_ROOTPOOL,TXN.COD_POST_CYCLE,TXN.DAT_LASTPOSTINGCALC,TXN.PSTCYCLE_CHNG,")
		.append(" POST_CYCLE.NBR_SETTLE_DAYS,POST_CYCLE.NBR_DELAY_DAYS_CR,POST_CYCLE.NBR_DELAY_DAYS_DR,POST_CYCLE.COD_NON_WORKING_DAY,POST_CYCLE.FLG_ISWORKINGDAY, ") //JPMC_SUPIS18836
//SIT2DEV Retro -DD end
		//if amount is -ve then multiply by -ve as TXNCODE is D
//SIT2DEV Retro -DD start
		.append(" (CASE WHEN (NVL (TOTAL_DECISION_AMT, 0) < 0 ) THEN (AMT_TREASURY_ADV * -1) ELSE  AMT_TREASURY_ADV END) AMT_CALCULATED,TXN.ID_POOL, ")
//SIT2DEV Retro -DD end
		//.append("		NVL (AMT_TREASURY_ADV, 0) AMT_CALCULATED, TXN.ID_POOL, ") 
		.append("        (CASE ") 
//SIT2DEV Retro -DD start
		.append("             WHEN NVL (TOTAL_DECISION_AMT, 0) >= 0 ")
//SIT2DEV Retro -DD end
		.append("                 THEN ACCT_TREASURY_CR  ")
		.append("             ELSE ACCT_TREASURY_DR  ")
		.append("         END  ")
		.append("        ) PARTICIPANT_ID, NVL(TXN.DAT_END, TO_DATE('31-Dec-9999','DD-Mon-YYYY') ) POOL_END, ")
		.append(" 		  (CASE  ")
//SIT2DEV Retro -DD start
		.append("             WHEN NVL (TOTAL_DECISION_AMT, 0) >= 0 AND TXN.FLG_REALPOOL = 'Y' ")//REAL CR 
//SIT2DEV Retro -DD end
//ENH_IS997 starts

		//CCB_SUP_IS760 starts LAST CHAR OF TXNCODE WILL BE 'T' FOR TREASURYSHARE ACCOUNTS
		//.append("                 THEN 'RCANCA'   ")
		.append("                 THEN 'RCANCT'   ")//760
//SIT2DEV Retro -DD start
		.append("             WHEN NVL (TOTAL_DECISION_AMT, 0) < 0  AND TXN.FLG_REALPOOL = 'Y' ")//REAL DR 
//SIT2DEV Retro -DD end

		//.append("                 THEN 'RCANDA'   ")
		.append("                 THEN 'RCANDT'   ")//760
//SIT2DEV Retro -DD start
		.append("             WHEN NVL (TOTAL_DECISION_AMT, 0) >= 0 AND TXN.FLG_REALPOOL = 'N' ")//INFO CR
//SIT2DEV Retro -DD end

		//.append("                 THEN 'ICANCA'   ")
		.append("                 THEN 'ICANCT'   ")//760
//SIT2DEV Retro -DD start
		.append("             WHEN NVL (TOTAL_DECISION_AMT, 0) < 0  AND TXN.FLG_REALPOOL = 'N' ")//INFO DR
//SIT2DEV Retro -DD end

		//.append("                 THEN 'ICANDA'   ")
		.append("                 THEN 'ICANDT'   ")//760
		//CCB_SUP_IS760 Ends
//ENH_IS997 ends

		//.append("             ELSE 'RCANDA'   ")
		.append("         END  ")
		.append("        )COD_TXN,  ")
		.append("        'TREASURYSHARE' TYP_PARTICIPANT, TXN.DAT_START, ")
		.append(" 		 TXN.DAT_LASTACCRUAL, DAT_ADJUSTEDACCRUAL,TXN.ID_PARENT_POOL ,")//LMS_45_PERF_CERTIFICATION_PF67
		.append("        TXN.DAT_ACCRUAL, TXN.DAT_LASTPOSTING, TXN.DAT_POSTING, ")
		.append("        TXN.DAT_POST_SETTLEMENT SETTLEMENT_DATE,TO_DATE('31-Dec-9999','DD-Mon-YYYY') PARTY_END,TXN.DAT_BUSINESS_POOLRUN BUSDT, ")
		.append(" 	   TXN.COD_COUNTER_ACCOUNT,TYP_POSTING FLG_REALPOSTING,TXN.FLG_REALPOOL,FLG_CONSOLIDATION FLG_POOL_CONSOLIDATED, ")
		.append(" (CASE  ")
//SIT2DEV Retro -DD start
		.append("             WHEN NVL (TOTAL_DECISION_AMT, 0) >= 0 ")
//SIT2DEV Retro -DD end
		.append("                 THEN TXN.DAT_POST_CREDITDELAY   ")
		.append("             ELSE TXN.DAT_POST_DEBITDELAY    ")
		.append("         END  ")
//SIT2DEV Retro -DD start
		.append("        )DAT_FINAL_SETTLEMENT ,TXN.FLG_SAMEVALDAT , TXN.COD_GL POOLCOD_GL") 
		// FLG_POSTINGCHANGES
		.append(" , TXN.FLG_POSTCYCLE ");
//SIT2DEV Retro -DD eend
		if(allowIntTyp)
		{
			StrNCNDquery.append(",  '' TYP_INTEREST ") ; // ENH_10.3_153	
		}
		// NEW_ACCOUNTING_ENTRY_FORMAT
		StrNCNDquery.append(", txn.ID_POOL SOURCE_POOL_ID,0 AMT_ALLOCATION_CENTRAL_CCY, 0 RATE_EXRATE_CENTRAL_CCY"); //Reallocate to Self
		StrNCNDquery.append("   FROM OPOOL_TXNPOOLHDR TXN ,ORATE_POSTING_CYCLE POST_CYCLE")
		//ANZ_SIT_ISSUE Fix starts
		.append("  WHERE  (amt_treasury_adv <> 0 or amt_treasury_adv is not null)  ") 
		//.append("   AND PERCENT_TREASURYADV IS NOT NULL  ") //Issue fix - Do not generate Zero Bankshare entries
		//ANZ_SIT_ISSUE Fix ends
		.append("   AND TXN.TXT_STATUS = 'ALLOCATED' ")
		.append("    AND TXN.ID_BVT = '0' ")
		.append("  AND TXN.NBR_LOTID = "+lotId+" AND TXN.COD_POST_CYCLE = POST_CYCLE.COD_POST_CYCLE ")
		.append("  AND  TXN.DAT_BUSINESS_POOLRUN = TO_DATE('"+ businessdate+"','" + dateFormat + "')  ) BANK_TREASURY,  ")  
//SIT2DEV Retro -DD start
		.append(" ORBICASH_ACCOUNTS ACC,")
		//Liquidity Volume Testing Optimized_query
/*		.append("(SELECT id_pool, cod_gl ")
		.append("  FROM opool_txnpoolhdr, (SELECT DISTINCT id_rootpool ")
		.append("                                      FROM opool_txnpoolhdr) root ")
		.append("  WHERE id_pool = root.id_rootpool AND nbr_lotid = '"+lotId+"' ) TEMP , OLM_GLMST gl ")
		.append("  WHERE ACC.NBR_OWNACCOUNT = BANK_TREASURY.PARTICIPANT_ID")*/
		//Liquidity Volume Testing Optimized_query
		
		.append(" OLM_GLMST gl ")//LMS_45_PERF_CERTIFICATION_PF67
		.append("  WHERE ACC.NBR_OWNACCOUNT = BANK_TREASURY.PARTICIPANT_ID")
		//.append("  AND  BANK_TREASURY.ID_PARENT_POOL IS NULL AND  GL.COD_GL=ACC.COD_GL");//LMS_45_PERF_CERTIFICATION_PF67 //ISSUE_2452 Fixes
		.append("  AND  GL.COD_GL=ACC.COD_GL");
//SIT2DEV Retro -DD end
		
		if (logger.isDebugEnabled()){
			logger.debug("SELECT QUERY FOR TXNCODE - RCANC/RCAND (Normal credit and debit advantage to Bank and Treasury A/C): "+ StrNCNDquery.toString());
		}
//SIT2DEV Retro -DD start
		//Call method to prepare data object 
		populateAccountInfo(BANKSHARE_TREASURYSHARE,lotId,businessDate, client, simId, pooldetails, insertAccountDtls,updateAccountDtls, insertAccrualSummary,
				insertPostingSummary, updateAccrualSummary, updatePostingSummary,insertUnpostedAmt,
				//updateUnpostedAmt,StrNCNDquery.toString(),selectAccrualSummary,selectPostingSummary,//Reallocate to Self
				updateUnpostedAmt,updateUnpostedAmtCentralCcy,StrNCNDquery.toString(),selectAccrualSummary,selectPostingSummary,
				//ENH_10.3_154 starts
				//selectAccountDtls,updateAccountDtlsForDates,updateAccuralSummaryForDates,updatePostingSummaryForDates);
				selectAccountDtls,updateAccountDtlsForDates,updateAccuralSummaryForDates,updatePostingSummaryForDates,
				updateBVTAccrualSummaryForDates,updateBVTPostingSummaryForDates);
				//ENH_10.3_154 Ends
		//ENH_IS1833 starts here
		//for enhancement margin margin
		StrNCNDquery.replace(0,StrNCNDquery.length(),"");
		StrNCNDquery.append(" SELECT bank_treasury.*, acc.cod_gl partycod_gl,gl.cod_calendar party_cod_calendar, acc.cod_ccy cod_ccy,bank_treasury.poolcod_gl rootgl");
		//ENH_10.3_154 Starts
		//SUP_ENH_III_8106 starts
		//StrNCNDquery.append(" , gl.FLG_BACKVALUE_POSTING ");
		StrNCNDquery.append(" , 'Y' FLG_BACKVALUE_POSTING ");
		//SUP_ENH_III_8106 ends
		//ENH_10.3_154 Ends
		StrNCNDquery.append(" FROM (SELECT txn.ID_PARENT_POOL,txn.dat_first_posting, txn.dat_post_creditdelay, txn.dat_post_debitdelay, id_rootpool, txn.cod_post_cycle,");
		StrNCNDquery.append(" txn.dat_lastpostingcalc, txn.pstcycle_chng, post_cycle.nbr_settle_days, post_cycle.nbr_delay_days_cr,");
		StrNCNDquery.append(" post_cycle.nbr_delay_days_dr, post_cycle.cod_non_working_day, POST_CYCLE.FLG_ISWORKINGDAY, ") ;//JPMC_SUPIS18836
		 //ENH_IS1945  IEF_Allocation Fix Starts
		StrNCNDquery.append(" (CASE WHEN (NVL (txn.TOTAL_DECISION_AMT, 0) < 0) THEN (NVL (amt_bankshare_enh, 0) * -1) ELSE NVL (amt_bankshare_enh, 0)");
//Retro_Mashreq
		//StrNCNDquery.append(" END ) amt_calculated, txn.id_pool, acct_bankshare participant_id,"); //MASHREQ_4760
		StrNCNDquery.append(" END ) amt_calculated, txn.id_pool, NBR_ROLE_OWNACCOUNT participant_id,");//MASHREQ_4760
		StrNCNDquery.append(" NVL (txn.dat_end, TO_DATE ('31-Dec-9999', 'DD-Mon-YYYY')) pool_end,");
		
		StrNCNDquery.append(" (CASE WHEN NVL (txn.TOTAL_DECISION_AMT, 0) >= 0  AND txn.flg_realpool = 'Y' THEN 'RCENCB'");
		StrNCNDquery.append(" WHEN NVL (txn.TOTAL_DECISION_AMT, 0) < 0  AND txn.flg_realpool = 'Y'  THEN 'RCENDB'");
		StrNCNDquery.append(" WHEN NVL (txn.TOTAL_DECISION_AMT, 0) >= 0 AND txn.flg_realpool = 'N' THEN 'ICENCB'");
		StrNCNDquery.append(" WHEN NVL (txn.TOTAL_DECISION_AMT, 0) < 0 AND txn.flg_realpool = 'N'  THEN 'ICENDB'  END ) cod_txn,");
		//IEF Changes for Allocation Ends
		StrNCNDquery.append(" 'BANKSHARE' typ_participant, txn.dat_start, txn.dat_lastaccrual, dat_adjustedaccrual, txn.dat_accrual,");
		StrNCNDquery.append(" txn.dat_lastposting, txn.dat_posting,txn.dat_post_settlement settlement_date,");
		StrNCNDquery.append(" TO_DATE ('31-Dec-9999', 'DD-Mon-YYYY') party_end, txn.dat_business_poolrun busdt, txn.cod_counter_account,");
		StrNCNDquery.append(" typ_posting flg_realposting, txn.flg_realpool,flg_consolidation flg_pool_consolidated,");
		StrNCNDquery.append(" (CASE WHEN NVL (TOTAL_DECISION_AMT, 0) > 0 THEN  txn.dat_post_creditdelay ELSE txn.dat_post_debitdelay END)"); //ENH_IS1945  IEF_Allocation Fix
		StrNCNDquery.append(" dat_final_settlement, txn.flg_samevaldat, txn.cod_gl poolcod_gl");
		// FLG_POSTINGCHANGES
		StrNCNDquery.append(" , TXN.FLG_POSTCYCLE ");
		if(allowIntTyp)
		{
			StrNCNDquery.append(",  '' TYP_INTEREST ") ; // ENH_10.3_153
		}
		// NEW_ACCOUNTING_ENTRY_FORMAT
		StrNCNDquery.append(", txn.ID_POOL SOURCE_POOL_ID,0 AMT_ALLOCATION_CENTRAL_CCY, 0 RATE_EXRATE_CENTRAL_CCY"); //Reallocate to Self
		StrNCNDquery.append(" FROM opool_txnpoolhdr txn, OPOOL_GLLINK blelink,OPOOL_GLLINK_DTLS blelinkdtls,orate_posting_cycle post_cycle");//MASHREQ_4760
		StrNCNDquery.append(" WHERE (amt_bankshare_enh IS NOT NULL) AND blelink.cod_gl = txn.cod_gl");
		//StrNCNDquery.append(" AND txn.txt_status = 'ALLOCATED' AND blelink.cod_bnk_lgl = txn.cod_base_bnk_lgl");//MASHREQ_4760
		StrNCNDquery.append("  AND txn.txt_status = 'ALLOCATED' AND blelink.GLLINK_MASTER_ID = blelinkdtls.GLLINK_MASTER_ID AND blelinkdtls.NBR_ROLE_ID=7 ");//MASHREQ_4760
		/* ISSUE_2407 Start */
		StrNCNDquery.append("    AND (BLELINK.COD_BNK_DEL = TXN.COD_BASE_BNK_LGL ");
		StrNCNDquery.append("    OR (BLELINK.COD_BNK_DEL IS NULL AND NOT EXISTS (SELECT 1 FROM OPOOL_GLLINK glink,OPOOL_GLLINK_DTLS dtls WHERE  ");
		StrNCNDquery.append("    TXN.COD_GL=glink.COD_GL AND TXN.COD_BASE_BNK_LGL= glink.COD_BNK_DEL AND TXN.COD_BASE_CCY_POOL = glink.COD_CCY  ");
		StrNCNDquery.append("    AND dtls.GLLINK_MASTER_ID = glink.GLLINK_MASTER_ID AND dtls.NBR_ROLE_ID  =7)))  ");
		/* ISSUE_2407 End */
		StrNCNDquery.append(" AND blelink.cod_ccy = txn.cod_base_ccy_pool AND txn.id_bvt = '0'");
		StrNCNDquery.append(" AND txn.cod_allocationsum = 'A' AND txn.nbr_lotid = "+lotId+"");
		StrNCNDquery.append(" AND txn.cod_post_cycle = post_cycle.cod_post_cycle AND txn.dat_business_poolrun = TO_DATE('"+ businessdate+"','" + dateFormat + "')");
		
		StrNCNDquery.append(" UNION");
		
		StrNCNDquery.append(" SELECT txn.ID_PARENT_POOL,txn.dat_first_posting, txn.dat_post_creditdelay, txn.dat_post_debitdelay, id_rootpool, txn.cod_post_cycle,");
		StrNCNDquery.append(" txn.dat_lastpostingcalc, txn.pstcycle_chng, post_cycle.nbr_settle_days, post_cycle.nbr_delay_days_cr,");
		StrNCNDquery.append(" post_cycle.nbr_delay_days_dr, post_cycle.cod_non_working_day,POST_CYCLE.FLG_ISWORKINGDAY, ") ;//JPMC_SUPIS18836
		 //ENH_IS1945  IEF_Allocation Fix Starts
		StrNCNDquery.append(" (CASE  WHEN (NVL (TOTAL_DECISION_AMT, 0) < 0) THEN (NVL (amt_treasury_enh, 0) * -1)");
		StrNCNDquery.append(" ELSE NVL (amt_treasury_enh, 0) END ) amt_calculated,txn.id_pool,");

		StrNCNDquery.append(" (CASE WHEN NVL (TOTAL_DECISION_AMT, 0) >= 0  THEN acct_treasury_cr  ELSE acct_treasury_dr END) participant_id,");
		StrNCNDquery.append(" NVL (txn.dat_end, TO_DATE ('31-Dec-9999', 'DD-Mon-YYYY')) pool_end,");
		StrNCNDquery.append(" (CASE WHEN NVL (TOTAL_DECISION_AMT, 0) > =0  AND txn.flg_realpool = 'Y' THEN 'RCENCT'");
		StrNCNDquery.append(" WHEN NVL (TOTAL_DECISION_AMT, 0) < 0    AND txn.flg_realpool = 'Y' THEN 'RCENDT'");
		StrNCNDquery.append(" WHEN NVL (TOTAL_DECISION_AMT, 0) >= 0    AND txn.flg_realpool = 'N' THEN 'ICENCT'");
		StrNCNDquery.append(" WHEN NVL (TOTAL_DECISION_AMT, 0) < 0    AND txn.flg_realpool = 'N' THEN 'ICENDT' END) cod_txn,");
		
		StrNCNDquery.append(" 'TREASURYSHARE' typ_participant, txn.dat_start, txn.dat_lastaccrual, dat_adjustedaccrual, txn.dat_accrual,");
		StrNCNDquery.append(" txn.dat_lastposting, txn.dat_posting,   txn.dat_post_settlement settlement_date,");
		StrNCNDquery.append(" TO_DATE ('31-Dec-9999', 'DD-Mon-YYYY') party_end, txn.dat_business_poolrun busdt, txn.cod_counter_account,");
		StrNCNDquery.append(" typ_posting flg_realposting, txn.flg_realpool, flg_consolidation flg_pool_consolidated,");
		
		StrNCNDquery.append(" (CASE WHEN NVL (TOTAL_DECISION_AMT, 0) >= 0 THEN  txn.dat_post_creditdelay ELSE txn.dat_post_debitdelay END");
		StrNCNDquery.append(" ) dat_final_settlement, txn.flg_samevaldat, txn.cod_gl poolcod_gl");
		// FLG_POSTINGCHANGES
		StrNCNDquery.append(" , TXN.FLG_POSTCYCLE ");
		 //ENH_IS1945  IEF_Allocation Fix Ends
		if(allowIntTyp)
		{
			StrNCNDquery.append(",  '' TYP_INTEREST ") ; // ENH_10.3_153			
		}		
		// NEW_ACCOUNTING_ENTRY_FORMAT
		StrNCNDquery.append(", txn.ID_POOL SOURCE_POOL_ID,0 AMT_ALLOCATION_CENTRAL_CCY, 0 RATE_EXRATE_CENTRAL_CCY "); //Reallocate to Self
		StrNCNDquery.append(" FROM opool_txnpoolhdr txn, orate_posting_cycle post_cycle");
		StrNCNDquery.append(" WHERE amt_treasury_enh IS NOT NULL");
		StrNCNDquery.append(" AND txn.txt_status = 'ALLOCATED'   AND txn.id_bvt = '0'");
		StrNCNDquery.append(" AND txn.nbr_lotid = "+lotId+"  AND txn.cod_post_cycle = post_cycle.cod_post_cycle");
		StrNCNDquery.append(" AND txn.dat_business_poolrun =   TO_DATE ('"+ businessdate+"', '" + dateFormat + "')) bank_treasury,");
		StrNCNDquery.append(" orbicash_accounts acc,");
		//Liquidity Volume Testing Optimized_query
		/*		.append("(SELECT id_pool, cod_gl ")
				.append("  FROM opool_txnpoolhdr, (SELECT DISTINCT id_rootpool ")
				.append("                                      FROM opool_txnpoolhdr) root ")
				.append("  WHERE id_pool = root.id_rootpool AND nbr_lotid = '"+lotId+"' ) TEMP , OLM_GLMST gl ")
				.append("  WHERE ACC.NBR_OWNACCOUNT = BANK_TREASURY.PARTICIPANT_ID")*/
				//Liquidity Volume Testing Optimized_query
				
		StrNCNDquery.append(" OLM_GLMST gl "); //
				//Liquidity Volume Testing Optimized_query end
		StrNCNDquery.append(" WHERE acc.nbr_ownaccount = bank_treasury.participant_id");
		//StrNCNDquery.append(" and bank_treasury.ID_PARENT_POOL IS NULL  AND gl.cod_gl = acc.cod_gl");//ISSUE_2452 Fixes
		StrNCNDquery.append(" AND gl.cod_gl = acc.cod_gl");
		
		if (logger.isDebugEnabled()){
			logger.debug("SELECT QUERY for Enhancement : "+ StrNCNDquery.toString());
		}
		
		populateAccountInfo(BANKSHARE_TREASURYSHARE,lotId,businessDate, client, simId, pooldetails, insertAccountDtls,updateAccountDtls, insertAccrualSummary,
				insertPostingSummary, updateAccrualSummary, updatePostingSummary,insertUnpostedAmt,
				//updateUnpostedAmt,StrNCNDquery.toString(),selectAccrualSummary,selectPostingSummary,//Reallocate to Self
				updateUnpostedAmt,updateUnpostedAmtCentralCcy,StrNCNDquery.toString(),selectAccrualSummary,selectPostingSummary,
				//ENH_10.3_154 Starts
				//selectAccountDtls,updateAccountDtlsForDates,updateAccuralSummaryForDates,updatePostingSummaryForDates);
				selectAccountDtls,updateAccountDtlsForDates,updateAccuralSummaryForDates,updatePostingSummaryForDates,
				updateBVTAccrualSummaryForDates,updateBVTPostingSummaryForDates);
				//ENH_10.3_154 Ends
		
		//for premium margin
		StrNCNDquery.replace(0,StrNCNDquery.length(),"");
		StrNCNDquery.append(" SELECT bank_treasury.*, acc.cod_gl partycod_gl,gl.cod_calendar party_cod_calendar, acc.cod_ccy cod_ccy,bank_treasury.poolcod_gl rootgl");
		//ENH_10.3_154 Starts
		//SUP_ENH_III_8106 starts
		//StrNCNDquery.append(" , gl.FLG_BACKVALUE_POSTING ");
		StrNCNDquery.append(" , 'Y' FLG_BACKVALUE_POSTING ");
		//SUP_ENH_III_8106 ends
		// FLG_POSTINGCHANGES
		StrNCNDquery.append(" , FLG_POSTCYCLE ");
		//ENH_10.3_154 Ends
		if(allowIntTyp)
		{
			StrNCNDquery.append(" , '' TYP_INTEREST ");// ENH_10.3_153			
		}
		

		StrNCNDquery.append(" FROM (SELECT txn.id_parent_pool, txn.dat_first_posting, txn.dat_post_creditdelay, txn.dat_post_debitdelay, id_rootpool, txn.cod_post_cycle,");
		StrNCNDquery.append(" txn.dat_lastpostingcalc, txn.pstcycle_chng, post_cycle.nbr_settle_days, post_cycle.nbr_delay_days_cr,");
		StrNCNDquery.append(" post_cycle.nbr_delay_days_dr, post_cycle.cod_non_working_day, POST_CYCLE.FLG_ISWORKINGDAY, ") ;//JPMC_SUPIS18836
		 //ENH_IS1945  IEF_Allocation Fix Starts
		StrNCNDquery.append(" (CASE WHEN (NVL (TOTAL_DECISION_AMT, 0) < 0) THEN (NVL (AMT_BANKSHARE_PREMIUM, 0) * -1) ELSE NVL (AMT_BANKSHARE_PREMIUM, 0)");
//Retro_Mashreq
		//StrNCNDquery.append(" END ) amt_calculated, txn.id_pool, acct_bankshare participant_id,"); //MASHREQ_4760
		StrNCNDquery.append(" END ) amt_calculated, txn.id_pool, NBR_ROLE_OWNACCOUNT participant_id,");//MASHREQ_4760
		
		StrNCNDquery.append(" NVL (txn.dat_end, TO_DATE ('31-Dec-9999', 'DD-Mon-YYYY')) pool_end,");
		StrNCNDquery.append(" (CASE WHEN NVL (TOTAL_DECISION_AMT, 0) >= 0  AND txn.flg_realpool = 'Y' THEN 'RCSNCB'");
		StrNCNDquery.append(" WHEN NVL (TOTAL_DECISION_AMT, 0) < 0  AND txn.flg_realpool = 'Y'  THEN 'RCSNDB'");
		StrNCNDquery.append(" WHEN NVL (TOTAL_DECISION_AMT, 0) >= 0 AND txn.flg_realpool = 'N' THEN 'ICSNCB'");
		StrNCNDquery.append(" WHEN NVL (TOTAL_DECISION_AMT, 0) < 0 AND txn.flg_realpool = 'N'  THEN 'ICSNDB'  END ) cod_txn,");
		StrNCNDquery.append(" 'BANKSHARE' typ_participant, txn.dat_start, txn.dat_lastaccrual, dat_adjustedaccrual, txn.dat_accrual,");
		StrNCNDquery.append(" txn.dat_lastposting, txn.dat_posting,txn.dat_post_settlement settlement_date,");
		StrNCNDquery.append(" TO_DATE ('31-Dec-9999', 'DD-Mon-YYYY') party_end, txn.dat_business_poolrun busdt, txn.cod_counter_account,");
		StrNCNDquery.append(" typ_posting flg_realposting, txn.flg_realpool,flg_consolidation flg_pool_consolidated,");

		StrNCNDquery.append(" (CASE WHEN NVL (TOTAL_DECISION_AMT, 0) > 0 THEN txn.dat_post_debitdelay  ELSE txn.dat_post_creditdelay END)");
		  //ENH_IS1945  IEF_Allocation Fix
		StrNCNDquery.append(" dat_final_settlement, txn.flg_samevaldat, txn.cod_gl poolcod_gl");
		// FLG_POSTINGCHANGES
		StrNCNDquery.append(" , txn.FLG_POSTCYCLE FLG_POSTCYCLE");
		
		if(allowIntTyp)
		{
			StrNCNDquery.append(" , '' TYP_INTEREST ");// ENH_10.3_153		
		}
		// NEW_ACCOUNTING_ENTRY_FORMAT
		StrNCNDquery.append(", txn.ID_POOL SOURCE_POOL_ID,0 AMT_ALLOCATION_CENTRAL_CCY, 0 RATE_EXRATE_CENTRAL_CCY "); //Reallocate to Self
		//StrNCNDquery.append(" FROM opool_txnpoolhdr txn, opool_blelink blelink, orate_posting_cycle post_cycle");//MASHREQ_4760
		StrNCNDquery.append(" FROM opool_txnpoolhdr txn, OPOOL_GLLINK blelink, OPOOL_GLLINK_DTLS blelinkdtls, orate_posting_cycle post_cycle");//MASHREQ_4760
		StrNCNDquery.append(" WHERE (amt_bankshare_premium IS NOT NULL) AND blelink.cod_gl = txn.cod_gl");
		//StrNCNDquery.append(" AND txn.txt_status = 'ALLOCATED' AND blelink.cod_bnk_lgl = txn.cod_base_bnk_lgl"); //MASHREQ_4760
		StrNCNDquery.append(" AND txn.txt_status = 'ALLOCATED'  AND blelink.GLLINK_MASTER_ID = blelinkdtls.GLLINK_MASTER_ID AND blelinkdtls.NBR_ROLE_ID=7 ");//MASHREQ_4760
		/* ISSUE_2407 Start */
		StrNCNDquery.append("    AND (BLELINK.COD_BNK_DEL = TXN.COD_BASE_BNK_LGL ");
		StrNCNDquery.append("    OR (BLELINK.COD_BNK_DEL IS NULL AND NOT EXISTS (SELECT 1 FROM OPOOL_GLLINK glink,OPOOL_GLLINK_DTLS dtls WHERE  ");
		StrNCNDquery.append("    TXN.COD_GL=glink.COD_GL AND TXN.COD_BASE_BNK_LGL= glink.COD_BNK_DEL AND TXN.COD_BASE_CCY_POOL = glink.COD_CCY  ");
		StrNCNDquery.append("    AND dtls.GLLINK_MASTER_ID = glink.GLLINK_MASTER_ID AND dtls.NBR_ROLE_ID  =7)))  ");
		/* ISSUE_2407 End */
		StrNCNDquery.append(" AND blelink.cod_ccy = txn.cod_base_ccy_pool AND txn.id_bvt = '0'");
		StrNCNDquery.append(" AND txn.cod_allocationsum = 'A' AND txn.nbr_lotid = "+lotId+"");
		StrNCNDquery.append(" AND txn.cod_post_cycle = post_cycle.cod_post_cycle AND txn.dat_business_poolrun = TO_DATE('"+ businessdate+"','" + dateFormat + "')");
		
		StrNCNDquery.append(" UNION");
		
		StrNCNDquery.append(" SELECT txn.id_parent_pool,txn.dat_first_posting, txn.dat_post_creditdelay, txn.dat_post_debitdelay, id_rootpool, txn.cod_post_cycle,");
		StrNCNDquery.append(" txn.dat_lastpostingcalc, txn.pstcycle_chng, post_cycle.nbr_settle_days, post_cycle.nbr_delay_days_cr,");
		StrNCNDquery.append(" post_cycle.nbr_delay_days_dr, post_cycle.cod_non_working_day,POST_CYCLE.FLG_ISWORKINGDAY, ") ;//JPMC_SUPIS18836
		 //ENH_IS1945  IEF_Allocation Fix
		StrNCNDquery.append(" (CASE  WHEN (NVL (TOTAL_DECISION_AMT, 0) < 0) THEN (NVL (amt_treasury_premium, 0) * -1)");
		StrNCNDquery.append(" ELSE NVL (amt_treasury_premium, 0) END ) amt_calculated,txn.id_pool,");

		StrNCNDquery.append(" (CASE WHEN NVL (TOTAL_DECISION_AMT, 0) >= 0  THEN acct_treasury_cr ELSE  acct_treasury_dr END) participant_id,");
		StrNCNDquery.append(" NVL (txn.dat_end, TO_DATE ('31-Dec-9999', 'DD-Mon-YYYY')) pool_end,");
		StrNCNDquery.append(" (CASE WHEN NVL (TOTAL_DECISION_AMT, 0) >= 0  AND txn.flg_realpool = 'Y' THEN 'RCSNCT'");
		StrNCNDquery.append(" WHEN NVL (TOTAL_DECISION_AMT, 0) < 0    AND txn.flg_realpool = 'Y' THEN 'RCSNDT'");
		StrNCNDquery.append(" WHEN NVL (TOTAL_DECISION_AMT, 0) >= 0    AND txn.flg_realpool = 'N' THEN 'ICSNCT'");
		StrNCNDquery.append(" WHEN NVL (TOTAL_DECISION_AMT, 0) < 0    AND txn.flg_realpool = 'N' THEN 'ICSNDT' END) cod_txn,");

		StrNCNDquery.append(" 'TREASURYSHARE' typ_participant, txn.dat_start, txn.dat_lastaccrual, dat_adjustedaccrual, txn.dat_accrual,");
		StrNCNDquery.append(" txn.dat_lastposting, txn.dat_posting,   txn.dat_post_settlement settlement_date,");
		StrNCNDquery.append(" TO_DATE ('31-Dec-9999', 'DD-Mon-YYYY') party_end, txn.dat_business_poolrun busdt, txn.cod_counter_account,");
		StrNCNDquery.append(" typ_posting flg_realposting, txn.flg_realpool, flg_consolidation flg_pool_consolidated,");

		StrNCNDquery.append(" (CASE WHEN NVL (TOTAL_DECISION_AMT, 0) > 0 THEN txn.dat_post_creditdelay ELSE txn.dat_post_debitdelay END");
		 //ENH_IS1945  IEF_Allocation Fix
		StrNCNDquery.append(" ) dat_final_settlement, txn.flg_samevaldat, txn.cod_gl poolcod_gl");
		StrNCNDquery.append(" , txn.FLG_POSTCYCLE FLG_POSTCYCLE ");
		
		if(allowIntTyp)
		{
			StrNCNDquery.append(" , '' TYP_INTEREST ");// ENH_10.3_153			
		}
		// NEW_ACCOUNTING_ENTRY_FORMAT
		StrNCNDquery.append(", txn.ID_POOL SOURCE_POOL_ID,0 AMT_ALLOCATION_CENTRAL_CCY, 0 RATE_EXRATE_CENTRAL_CCY "); //Reallocate to Self
		StrNCNDquery.append(" FROM opool_txnpoolhdr txn, orate_posting_cycle post_cycle");
		StrNCNDquery.append(" WHERE amt_treasury_premium IS NOT NULL");
		StrNCNDquery.append(" AND txn.txt_status = 'ALLOCATED'   AND txn.id_bvt = '0'");
		StrNCNDquery.append(" AND txn.nbr_lotid = "+lotId+"  AND txn.cod_post_cycle = post_cycle.cod_post_cycle");
		StrNCNDquery.append(" AND txn.dat_business_poolrun =   TO_DATE ('"+ businessdate+"', '" + dateFormat + "')) bank_treasury,");
		StrNCNDquery.append(" orbicash_accounts acc,");
		//Liquidity Volume Testing Optimized_query
		/*		.append("(SELECT id_pool, cod_gl ")
				.append("  FROM opool_txnpoolhdr, (SELECT DISTINCT id_rootpool ")
				.append("                                      FROM opool_txnpoolhdr) root ")
				.append("  WHERE id_pool = root.id_rootpool AND nbr_lotid = '"+lotId+"' ) TEMP , OLM_GLMST gl ")
				.append("  WHERE ACC.NBR_OWNACCOUNT = BANK_TREASURY.PARTICIPANT_ID")*/
				//Liquidity Volume Testing Optimized_query
				
		StrNCNDquery.append("  OLM_GLMST gl ");//PERFORMANCE_CHANGE_PERF60
				//Liquidity Volume Testing Optimized_query end
		StrNCNDquery.append(" WHERE acc.nbr_ownaccount = bank_treasury.participant_id");
		StrNCNDquery.append("  AND gl.cod_gl = acc.cod_gl");//PERFORMANCE_CHANGE_PERF60 //ISSUE_2452 Fixes
		
		if (logger.isDebugEnabled()){
			logger.debug("SELECT QUERY for premium : "+ StrNCNDquery.toString());
		}
		
		populateAccountInfo(BANKSHARE_TREASURYSHARE,lotId,businessDate, client, simId, pooldetails, insertAccountDtls,updateAccountDtls, insertAccrualSummary,
//SIT2DEV Retro -DD end
				insertPostingSummary, updateAccrualSummary, updatePostingSummary,insertUnpostedAmt,
				//updateUnpostedAmt,StrNCNDquery.toString(),selectAccrualSummary,selectPostingSummary,//Reallocate to Self
				updateUnpostedAmt, updateUnpostedAmtCentralCcy,StrNCNDquery.toString(),selectAccrualSummary,selectPostingSummary,
				//ENH_10.3_154 starts
				//selectAccountDtls,updateAccountDtlsForDates,updateAccuralSummaryForDates,updatePostingSummaryForDates);
				selectAccountDtls,updateAccountDtlsForDates,updateAccuralSummaryForDates,updatePostingSummaryForDates,
				updateBVTAccrualSummaryForDates,updateBVTPostingSummaryForDates);
				//ENH_10.3_154 Ends
		if (logger.isDebugEnabled()){
			logger.debug("Leaving");
		}
	}

	/**
	 * This method execute query ie strQuery and sets the data in accountInfo data object and call method processAccountInfo 
	 * @param txnType
	 * @param lotId
	 * @param businessDate
	 * @param client
	 * @param insertAccountDtls
	 * @param updateAccountDtls
	 * @param insertAccrualSummary
	 * @param insertPostingSummary
	 * @param updateAccrualSummary
	 * @param updatePostingSummary
	 * @param insertUnpostedAmt
	 * @param updateUnpostedAmt
	 * @param strQuery
	 * @param selectAccrualSummary
	 * @param selectPostingSummary
	 * @param selectAccountDtls
	 * @param updateAccountDtlsForDates
	 * @param updateAccuralSummaryForDates
	 * @throws Exception
	 */
	//ENH_10.3_154 Starts
	/*private void populateAccountInfo(String txnType,String lotId, java.util.Date businessDate,
//SIT2DEV Retro -DD start
			String client,HashMap pooldetails, PreparedStatement insertAccountDtls,PreparedStatement updateAccountDtls,PreparedStatement insertAccrualSummary, 
//SIT2DEV Retro -DD end
			PreparedStatement insertPostingSummary, PreparedStatement updateAccrualSummary, 
			PreparedStatement updatePostingSummary,PreparedStatement insertUnpostedAmt ,
			PreparedStatement updateUnpostedAmt ,String strQuery,
			PreparedStatement selectAccrualSummary,PreparedStatement selectPostingSummary
			,PreparedStatement selectAccountDtls ,PreparedStatement updateAccountDtlsForDates,
			PreparedStatement updateAccuralSummaryForDates,PreparedStatement updatePostingSummaryForDates) throws Exception {*/
	private void populateAccountInfo(String txnType,String lotId, java.util.Date businessDate,
			String client,long simId, HashMap pooldetails, PreparedStatement insertAccountDtls,PreparedStatement updateAccountDtls,PreparedStatement insertAccrualSummary, 
			PreparedStatement insertPostingSummary, PreparedStatement updateAccrualSummary, 
			PreparedStatement updatePostingSummary,PreparedStatement insertUnpostedAmt ,
			//PreparedStatement updateUnpostedAmt ,String strQuery,
			PreparedStatement updateUnpostedAmt ,PreparedStatement updateUnpostedAmtCentralCcy, String strQuery,//Reallocate to Self
			PreparedStatement selectAccrualSummary,PreparedStatement selectPostingSummary,
			PreparedStatement selectAccountDtls ,PreparedStatement updateAccountDtlsForDates,
			PreparedStatement updateAccuralSummaryForDates,PreparedStatement updatePostingSummaryForDates,
			PreparedStatement updateBVTAccrualSummaryForDates, PreparedStatement updateBVTPostingSummaryForDates) throws Exception {
	//ENH_10.3_154 Ends
		if (logger.isDebugEnabled()){
			logger.debug("Entering");
		}
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		Statement stmt = null;
		PreparedStatement pStmt = null;
		/** ****** JTEST FIX end **************  on 14DEC09*/
		Connection conn = null;
		ResultSet res = null;
		boolean recordExists = false;
		AccountInfoDO accountInfoDo = null;
		java.sql.Date businessdate = new java.sql.Date(businessDate.getTime());
		java.util.Date incDateUtil = null;
		java.sql.Date incDateSql = null;
		// JPMC_HOOK
		Class cls = null;
		IPoolInfo  iPoolInfoHelper = null;
		
		try
		{
			conn = LMSConnectionUtility.getConnection();
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			stmt = conn.createStatement();
//			res = stmt.executeQuery(strQuery);
			pStmt = conn.prepareStatement(strQuery);
			PerformanceUtil.printTimeStamp("AccountSummaryGenerator", "populateAccountInfo Query Execution", txnType, "Start", "", String.valueOf(lotId),"",client,"");
			res = pStmt.executeQuery();
			PerformanceUtil.printTimeStamp("AccountSummaryGenerator", "populateAccountInfo Query Execution", txnType, "End", "", String.valueOf(lotId),"",client,"");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			accountInfoDo = new AccountInfoDO();
			GregorianCalendar gDate = new GregorianCalendar();
			String interestType = null ; // ENH_10.3_153
			
			//LMS_45_PERF_CERTIFICATION_PF24 start
			
			CachedRowSet lCachedRowSet = DBUtils.getCachedRowSetImpl();
			lCachedRowSet.populate(res);
			LMSConnectionUtility.close(res);
			LMSConnectionUtility.close(pStmt);
			
			
			
			HashMap<String,AccrualInfoDO> accuralInfoMap = new HashMap<String,AccrualInfoDO>();
			HashMap<String,PostingInfoDO> postingInfoMap = new HashMap<String,PostingInfoDO>();
			HashMap<String,AccountDtlsInfoDO> accountDtlsInfoMap = new HashMap<String,AccountDtlsInfoDO>();
			
			
			PerformanceUtil.printTimeStamp("AccountSummaryGenerator", "populateDetailsForAccountInfo", "", "Start", "", String.valueOf(lotId),"",client,"");
			populateDetailsForAccountInfo(conn,lCachedRowSet,-1,simId,true,accuralInfoMap,postingInfoMap,accountDtlsInfoMap);
			
			PerformanceUtil.printTimeStamp("AccountSummaryGenerator", "populateDetailsForAccountInfo", "", "End", "", String.valueOf(lotId),"",client,"");

			
			while(lCachedRowSet.next())
			{
				recordExists = true;
				//Set resultset data  into Data Object
//SIT2DEV Retro -DD start
				//Retro From 9.8 Starts
				//SUP_IS1364 Start
				java.util.Date pLast_actualDate_post = lCachedRowSet.getDate("DAT_LASTPOSTING");
				java.util.Date pLast_calculatedDate_post = lCachedRowSet.getDate("DAT_LASTPOSTINGCALC");
				String pstngCycleChng = lCachedRowSet.getString("PSTCYCLE_CHNG");	
				java.util.Date actualDate_post = null;
				//java.util.Date adjustedDate_post = null;
				java.util.Date settlementDate_post = null;
				java.util.Date crDelayDate_post = null;
				java.util.Date drDelayDate_post = null;
				//Date dat_first_posting = res.getDate("DAT_FIRST_POSTING");
				String calendar = lCachedRowSet.getString("PARTY_COD_CALENDAR");
				String frequency = lCachedRowSet.getString("COD_POST_CYCLE");
				//JPMC_SUPIS18836 STARTS
				//Interest and Advantage Seperate Settlement Dates For JPMC
				String accrualPricingFeed = null;
				//Issue_2495 Fixes Start
				/*if("Y".equalsIgnoreCase(ExecutionConstants.FLG_IS_INTEREST_ADVANTAGE_SEPERATE_SETTLEMENT) && txnType.equalsIgnoreCase(NORMAL_INTEREST)){
					accrualPricingFeed =  lCachedRowSet.getString("FLG_ACC_ACCRUAL_PRICING_FEED");
				}*/	
				if(txnType.equalsIgnoreCase(NORMAL_INTEREST))
				{
					accrualPricingFeed =  lCachedRowSet.getString("FLG_ACC_ACCRUAL_PRICING_FEED");
				}	
				//Issue_2495 Fixes end
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("accrualPricingFeed:"+accrualPricingFeed);
				accountInfoDo.setAccrualPricingInterfaceFeed(accrualPricingFeed);
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("getAccrualPricingInterfaceFeed:"+accountInfoDo.getAccrualPricingInterfaceFeed());
				
				String isWorkingDayFlag = lCachedRowSet.getString("FLG_ISWORKINGDAY");
				boolean isWorkingDay = false;
				if(isWorkingDayFlag.equalsIgnoreCase("Y")){
					isWorkingDay = true;
				}
				ArrayList calendarCode = new ArrayList();
				calendarCode.add(calendar);
				//Interest and Advantage Seperate Settlement Dates For JPMC
				//JPMC_SUPIS18836 ENDS
				//ENH_10.3_154 Starts
				String cod_txn = null;
				if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y"))
					cod_txn = lCachedRowSet.getString("COD_TXN")+ExecutionConstants.NORMAL_POST_CYCLE;
				else
					cod_txn = lCachedRowSet.getString("COD_TXN");
				if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
					logger.debug("cod_txn : "+cod_txn);
					//ENH_10.3_154 Ends
					logger.debug("Root GL "+lCachedRowSet.getString("ROOTGL"));
					logger.debug("Party GL "+lCachedRowSet.getString("PARTYCOD_GL"));
					logger.debug("Party GL "+lCachedRowSet.getString("POOLCOD_GL"));
				}
				if(!lCachedRowSet.getString("ROOTGL").equalsIgnoreCase(lCachedRowSet.getString("PARTYCOD_GL"))||!lCachedRowSet.getString("POOLCOD_GL").equalsIgnoreCase(lCachedRowSet.getString("PARTYCOD_GL")))
				{	
					if(logger.isDebugEnabled()){
						logger.debug("IN CROSS HOST CONDITION");
					}
					if (pLast_actualDate_post == null || pLast_calculatedDate_post == null || "Y".equalsIgnoreCase(pstngCycleChng))
					{
						
							actualDate_post = lCachedRowSet.getDate("DAT_FIRST_POSTING");
							//adjustedDate_post = res.getDate("DAT_FIRST_POSTING");
							
							
							int lNbr_settle_days = lCachedRowSet.getInt("NBR_SETTLE_DAYS");
							int lNbr_delay_days_cr = lCachedRowSet.getInt("NBR_DELAY_DAYS_CR");
							int lNbr_delay_days_dr = lCachedRowSet.getInt("NBR_DELAY_DAYS_DR");
							String lHolidayFlag = lCachedRowSet.getString("COD_NON_WORKING_DAY");  
							if(logger.isDebugEnabled()){
								logger.debug("no of settle days:"+lNbr_settle_days+"no of credit delay periods:"+lNbr_delay_days_cr+"no of debit delay periods:"+lNbr_delay_days_dr+"Non working day:"+lHolidayFlag);
							}
							GregorianCalendar temp = new GregorianCalendar();
							temp.setTime(actualDate_post);
							temp.add(Calendar.DATE, lNbr_settle_days);
						//	settlementDate_post = temp.getTime();
							temp = null;
							//JPMC_SUPIS18836 STARTS
							/*
							crDelayDate_post = ExecutionUtility.getAnotherWorkingDay(calendar, actualDate_post, 
									lHolidayFlag.charAt(0), 0, 0, lNbr_settle_days+lNbr_delay_days_cr);
							drDelayDate_post = ExecutionUtility.getAnotherWorkingDay(calendar, actualDate_post, 
									lHolidayFlag.charAt(0), 0, 0, lNbr_settle_days+lNbr_delay_days_dr);
							//Interest and Advantage Seperate Settlement Dates For JPMC
							settlementDate_post = ExecutionUtility.getAnotherWorkingDay(calendar, actualDate_post, 
									lHolidayFlag.charAt(0), 0, 0, lNbr_settle_days);*/
							
							settlementDate_post = ExecutionUtility.getNthWorkingDay(calendarCode, actualDate_post, 
									lHolidayFlag.charAt(0), lNbr_settle_days,isWorkingDay);
							crDelayDate_post = ExecutionUtility.getNthWorkingDay(calendarCode, settlementDate_post, 
									lHolidayFlag.charAt(0), lNbr_delay_days_cr,isWorkingDay);
							drDelayDate_post = ExecutionUtility.getNthWorkingDay(calendarCode, settlementDate_post, 
									lHolidayFlag.charAt(0), lNbr_delay_days_dr,isWorkingDay);
							//Interest and Advantage Seperate Settlement Dates For JPMC
							if(logger.isDebugEnabled()){
								logger.debug("In populateAccountInfo(Cross Host) :: isWorkingDay::"+isWorkingDay+" settlementDate_post: "+settlementDate_post+" crDelayDate_post: "+crDelayDate_post+" drDelayDate_post: "+drDelayDate_post);
							}
							//JPMC_SUPIS18836 ENDS
							
							//pLast_actualDate_post = lDat_start;
							pLast_calculatedDate_post = businessdate;
						} else {
							
							
							java.util.Date[] postingDates = ExecutionUtility.getNextPostingCycleDate(conn,frequency,pLast_actualDate_post,pLast_calculatedDate_post,calendar,businessdate,"E");
							//actualDate_post = postingDates[0];
							//adjustedDate_post = postingDates[1];
							settlementDate_post = postingDates[2];
							crDelayDate_post = postingDates[3];
							drDelayDate_post = postingDates[4]; 
						}
					
					//Interest and Advantage Seperate Settlement Dates For JPMC
					if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
						logger.debug("ExecutionConstants.FLG_IS_INTEREST_ADVANTAGE_SEPERATE_SETTLEMENT:="+ExecutionConstants.FLG_IS_INTEREST_ADVANTAGE_SEPERATE_SETTLEMENT);
						logger.debug("txnType:="+txnType);
					}
					if("Y".equalsIgnoreCase(ExecutionConstants.FLG_IS_INTEREST_ADVANTAGE_SEPERATE_SETTLEMENT) && txnType.equalsIgnoreCase(NORMAL_INTEREST) && "N".equalsIgnoreCase(accrualPricingFeed)){
						logger.debug("Setting the Settlement dates-");
						accountInfoDo.setDAT_FINAL_SETTLEMENT(new java.sql.Date(settlementDate_post.getTime()));
						accountInfoDo.setDAT_POST_DEBITDELAY(new java.sql.Date(settlementDate_post.getTime()));
						accountInfoDo.setDAT_POST_CREDITDELAY(new java.sql.Date(settlementDate_post.getTime()));
					}else{
					//Set AccoutDo dates here 
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
					//if(cod_txn.substring(4,5).equalsIgnoreCase("C"))
					if(lCachedRowSet.getString("COD_TXN").substring(4,5).equalsIgnoreCase("C"))
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
					{//(java.sql.Date) crDelayDate_post
						accountInfoDo.setDAT_FINAL_SETTLEMENT(new java.sql.Date(crDelayDate_post.getTime()));
						accountInfoDo.setDAT_POST_CREDITDELAY(new java.sql.Date(crDelayDate_post.getTime()));
						accountInfoDo.setDAT_POST_DEBITDELAY(new java.sql.Date(drDelayDate_post.getTime()));
					}else
					{
						accountInfoDo.setDAT_FINAL_SETTLEMENT(new java.sql.Date(drDelayDate_post.getTime()));
						accountInfoDo.setDAT_POST_DEBITDELAY(new java.sql.Date(drDelayDate_post.getTime()));
						accountInfoDo.setDAT_POST_CREDITDELAY(new java.sql.Date(crDelayDate_post.getTime()));
					}
					}
					//Interest and Advantage Seperate Settlement Dates For JPMC
				}else
				{
					//Interest and Advantage Seperate Settlement Dates For JPMC
					if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
						logger.debug("ExecutionConstants.FLG_IS_INTEREST_ADVANTAGE_SEPERATE_SETTLEMENT:="+ExecutionConstants.FLG_IS_INTEREST_ADVANTAGE_SEPERATE_SETTLEMENT);
						logger.debug("txnType:="+txnType);
					}
					if("Y".equalsIgnoreCase(ExecutionConstants.FLG_IS_INTEREST_ADVANTAGE_SEPERATE_SETTLEMENT) && txnType.equalsIgnoreCase(NORMAL_INTEREST) && "N".equalsIgnoreCase(accrualPricingFeed)){
						if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
							logger.debug("Setting the Settlement dates2-");
							logger.debug("ID_POOL="+lCachedRowSet.getString("ID_POOL")+" ::PARTICIPANT_ID="+lCachedRowSet.getString("PARTICIPANT_ID"));
							logger.debug("res.getDate(DAT_FINAL_SETTLEMENT)2:-"+lCachedRowSet.getDate("DAT_FINAL_SETTLEMENT"));
						}
						accountInfoDo.setDAT_FINAL_SETTLEMENT(lCachedRowSet.getDate("DAT_FINAL_SETTLEMENT"));
						accountInfoDo.setDAT_POST_CREDITDELAY(lCachedRowSet.getDate("DAT_FINAL_SETTLEMENT"));
						accountInfoDo.setDAT_POST_DEBITDELAY(lCachedRowSet.getDate("DAT_FINAL_SETTLEMENT"));
					}else{
						accountInfoDo.setDAT_FINAL_SETTLEMENT(lCachedRowSet.getDate("DAT_FINAL_SETTLEMENT"));
						accountInfoDo.setDAT_POST_CREDITDELAY(lCachedRowSet.getDate("DAT_POST_CREDITDELAY"));
						accountInfoDo.setDAT_POST_DEBITDELAY(lCachedRowSet.getDate("DAT_POST_DEBITDELAY"));
					}
					//Interest and Advantage Seperate Settlement Dates For JPMC
				}
				
				if(logger.isDebugEnabled()){
					logger.debug("accountInfoDo.setDAT_FINAL_SETTLEMENT = " +accountInfoDo.getDAT_FINAL_SETTLEMENT());
				}
				//SUP_IS1364 End
				//accountInfoDo.setDAT_POST_CREDITDELAY(res.getDate("DAT_POST_CREDITDELAY")); //SUP_IS1364
				//accountInfoDo.setDAT_POST_DEBITDELAY(res.getDate("DAT_POST_DEBITDELAY"));//SUP_IS1364
				//Retro from 9.8 Ends
				accountInfoDo.setID_POOL(lCachedRowSet.getString("ID_POOL"));
				accountInfoDo.setPARTICIPANT_ID(lCachedRowSet.getString("PARTICIPANT_ID"));
				accountInfoDo.setPOOL_DAT_START(lCachedRowSet.getDate("DAT_START"));
				accountInfoDo.setPOOL_DAT_END(lCachedRowSet.getDate("POOL_END"));
					//Retro From 9.8 Starts
				//SUP_IS1459 Starts 
				/*if Pool is ended then the participant account with txnType as treasury should have the same end date as that of Pool 
				 * otherwise it will (31-Dec-9999) as selected from strQuery*/
				if(txnType.equalsIgnoreCase("BANKSHARE_TREASURYSHARE"))
					accountInfoDo.setPARTY_DAT_END (accountInfoDo.getPOOL_DAT_END());
				
				else 
					accountInfoDo.setPARTY_DAT_END (lCachedRowSet.getDate("PARTY_END"));
				//SUP_IS1459 Ends 
			//Retro from 9.8
//SIT2DEV Retro -DD end
				accountInfoDo.setDAT_BUSINESS(lCachedRowSet.getDate("BUSDT"));
				accountInfoDo.setDAT_LASTACCRUAL(lCachedRowSet.getDate("DAT_LASTACCRUAL"));
				accountInfoDo.setTYP_PARTICIPANT(lCachedRowSet.getString("TYP_PARTICIPANT"));
				if(lCachedRowSet.getDate("DAT_LASTACCRUAL")==null)
				{
					//set DAT_ACCRUAL_START = DATE_POOL_START
					if(simId !=0)
					{
						accountInfoDo.setDAT_ACCRUAL_START(lCachedRowSet.getDate("DAT_ADJUSTEDACCRUAL"));//should be last Acc date+1
					}
					else
					{
						accountInfoDo.setDAT_ACCRUAL_START(lCachedRowSet.getDate("DAT_START"));//should be last Acc date+1
					}
				}
				else
				{
					//set DAT_ACCRUAL_START = DAT_LASTACCRUAL + 1 
					gDate.setTime(lCachedRowSet.getDate("DAT_LASTACCRUAL"));
					gDate.add(Calendar.DATE,1);
					incDateUtil =  gDate.getTime();
					incDateSql = new java.sql.Date(incDateUtil.getTime());
					accountInfoDo.setDAT_ACCRUAL_START(incDateSql);
					
				}
				if(lCachedRowSet.getDate("DAT_LASTPOSTING") == null)
				{
					//set DAT_POSTING_START = DATE_POOL_START
					accountInfoDo.setDAT_POSTING_START(lCachedRowSet.getDate("DAT_START"));//should be last Posting +1
				}
				else
				{
					//set DAT_POSTING_START = DAT_LASTPOSTING + 1 
					gDate.setTime(lCachedRowSet.getDate("DAT_LASTPOSTING"));
					gDate.add(Calendar.DATE,1);
					incDateUtil =  gDate.getTime();
					incDateSql = new java.sql.Date(incDateUtil.getTime());
					accountInfoDo.setDAT_POSTING_START(incDateSql);
				}
 // ENH_10.3_153 Starts
				// accountInfoDo.setCOD_TXN(res.getString("COD_TXN"));// ENH_10.3_153
				if(!ExecutionConstants.FLG_INTACC_POST_INTTYP) // If it is 'N'
				{
					interestType = Constants.EMPTY_STRING ;
				}
				else
				{
					
					interestType = lCachedRowSet.getString("TYP_INTEREST") ;
					
					if(interestType == null || interestType.equals(AllocationConstants.DEFAULT))
					{
						interestType = Constants.EMPTY_STRING ;
					}
				}
				
				accountInfoDo.setCOD_TXN(cod_txn + interestType);// ENH_10.3_153				
				
	// ENH_10.3_153 Ends			
				//1=NORMAL_INTEREST,2= NORMAL_ALLOC_REALLOC_REBATE,3=BANKSHARE_TREASURYSHARE
				
				accountInfoDo.setCOD_COUNTER_ACCOUNT(lCachedRowSet.getString("COD_COUNTER_ACCOUNT"));
				accountInfoDo.setFLG_REALPOSTING(lCachedRowSet.getString("FLG_REALPOSTING"));
				
				
				if(txnType.equalsIgnoreCase(NORMAL_ALLOC_REALLOC_REBATE))
				{
					accountInfoDo.setFLG_ALLOCATION(lCachedRowSet.getString("FLG_ALLOCATION"));
					//SIT issue fix starts - txn.ID_PARENT_POOL added
					accountInfoDo.setID_PARENT_POOL(lCachedRowSet.getString("ID_PARENT_POOL"));
					//SIT issue fix ends
				}
				if(txnType.equalsIgnoreCase(NORMAL_INTEREST))
				{
					accountInfoDo.setDR_INT(lCachedRowSet.getBigDecimal("DR_INT"));
					accountInfoDo.setCR_INT(lCachedRowSet.getBigDecimal("CR_INT"));
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
					//if(cod_txn.substring(4,5).equalsIgnoreCase("C"))
					if(!"Y".equalsIgnoreCase(lCachedRowSet.getString("FLG_ACC_ACCRUAL_PRICING_FEED"))){ //Issue_2495 Fixes Start
					if(lCachedRowSet.getString("COD_TXN").substring(4,5).equalsIgnoreCase("C"))
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
					{
						accountInfoDo.setAMT_CALCULATED(lCachedRowSet.getBigDecimal("CR_INT"));
					}
					else
					{
						accountInfoDo.setAMT_CALCULATED(lCachedRowSet.getBigDecimal("DR_INT"));
						
					}
					}
					else 
					{
						accountInfoDo.setAMT_CALCULATED(ZERO);
						if(logger.isDebugEnabled())
							logger.debug("accountInfoDo.getAMT_CALCULATED() :::::: "+accountInfoDo.getAMT_CALCULATED()); //Issue_2495 Fixes end					
					}
					accountInfoDo.setCOD_COUNTER_ACCOUNT(lCachedRowSet.getString("COD_COUNTER_ACCOUNT"));
					accountInfoDo.setFLG_REALPOSTING(lCachedRowSet.getString("FLG_REALPOSTING"));
				}
				else
				{
					//1- 1  MAPPING FOR 2,3 AMT_CALCULATED
					accountInfoDo.setAMT_CALCULATED(lCachedRowSet.getBigDecimal("AMT_CALCULATED"));
					//SIT issue fix starts - txn.ID_PARENT_POOL added
					//Do not generate accrual entry so keep cod_counter_account as it is i.e. 'S' for subpools
					//if(txnType.equalsIgnoreCase(NORMAL_ALLOC_REALLOC_REBATE) && accountInfoDo.getCOD_COUNTER_ACCOUNT().equals("S")) //ANZ_UAT_ISSUE FIXED
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("accountInfoDo.getCOD_COUNTER_ACCOUNT() :::::: "+accountInfoDo.getCOD_COUNTER_ACCOUNT()); //ANZ_UAT_ISSUE FIXED
					if(txnType.equalsIgnoreCase(NORMAL_ALLOC_REALLOC_REBATE) && "S".equals(accountInfoDo.getCOD_COUNTER_ACCOUNT()))//ANZ_UAT_ISSUE FIXED
					{
//Retro_Mashreq
						//if(accountInfoDo.getID_PARENT_POOL() == null)  	//For rootpool //MASHREQ_4647
						if(accountInfoDo.getID_PARENT_POOL() == null || ("Allocating Sub Pool Advantage".equalsIgnoreCase(lCachedRowSet.getString("NARRATION"))) ||("Allocating Sub Pool Interest".equalsIgnoreCase(lCachedRowSet.getString("NARRATION"))) ) //MASHREQ_4647
							accountInfoDo.setCOD_COUNTER_ACCOUNT("NA");
					}
					else{
					accountInfoDo.setCOD_COUNTER_ACCOUNT("NA");
					}
					//SIT issue fix ends
					accountInfoDo.setFLG_REALPOSTING("NA");
				}
				accountInfoDo.setAMT_CAL_CENTRAL_CCY(lCachedRowSet.getBigDecimal("AMT_ALLOCATION_CENTRAL_CCY"));//Reallocate to Self
				accountInfoDo.setRATE_EXRATE_CENTRAL_CCY(lCachedRowSet.getBigDecimal("RATE_EXRATE_CENTRAL_CCY"));//Reallocate to Self
				accountInfoDo.setFLG_REALPOOL(lCachedRowSet.getString("FLG_REALPOOL"));
				accountInfoDo.setFLG_POOL_CONSOLIDATED(lCachedRowSet.getString("FLG_POOL_CONSOLIDATED"));
				
				if(txnType.equalsIgnoreCase(BANKSHARE_TREASURYSHARE))
				{
					
					accountInfoDo.setFLG_ACC_CONSOLIDATED("NA");
					accountInfoDo.setMINAMT(ZERO);
				}
				else
				{	
					accountInfoDo.setFLG_ACC_CONSOLIDATED(lCachedRowSet.getString("FLG_ACC_CONSOLIDATED"));	
					if(lCachedRowSet.getBigDecimal("MINAMT")!= null)
					{
						accountInfoDo.setMINAMT(lCachedRowSet.getBigDecimal("MINAMT"));
					}
					else
					{	
						accountInfoDo.setMINAMT(ZERO);
					}
				}
//SIT2DEV Retro -DD start
			//Retro from 9.8 Start Commented DAT_FINAL_SETTELMENT 
				//accountInfoDo.setDAT_FINAL_SETTLEMENT(res.getDate("DAT_FINAL_SETTLEMENT"));//SUP_IS1364
				//Retro From 9.8 Ends				
//SIT2DEV Retro -DD end
				accountInfoDo.setDAT_ADJUSTEDACCRUAL(lCachedRowSet.getDate("DAT_ADJUSTEDACCRUAL"));
				accountInfoDo.setDAT_ACCRUAL(lCachedRowSet.getDate("DAT_ACCRUAL"));
				accountInfoDo.setDAT_POSTING(lCachedRowSet.getDate("DAT_POSTING")); 
				accountInfoDo.setOLD_DAT_POSTING(lCachedRowSet.getDate("DAT_POSTING"));
				accountInfoDo.setDAT_LASTPOSTING(lCachedRowSet.getDate("DAT_LASTPOSTING"));
				accountInfoDo.setSETTLEMENT_DATE(lCachedRowSet.getDate("SETTLEMENT_DATE"));
				accountInfoDo.setDAT_CURRENT_BUSINESS(lCachedRowSet.getDate("BUSDT"));//COMMON FOR 1,2,3
				
				//SET COD_CCY IN DO
				accountInfoDo.setCOD_CCY(lCachedRowSet.getString("COD_CCY"));
				accountInfoDo.setFLG_SAMEVALDAT(lCachedRowSet.getString("FLG_SAMEVALDAT"));
				/*
				 *  
				 *  Set the Value Date 
				 *  SameAsPosting date -> value Date = Final Settlement Date() 	if FLG_SAMEVALDAT  'Y'
				 *	WithNonworkingDayAdj -> valueDate = DAT_POSTING + 1    if 	FLG_SAMEVALDAT  'A'
				 *	withoutNonWorkingDay -> valueDate = DAT_POSTING +1 	if FLG_SAMEVALDAT  'N'
				 */
//SIT2DEV Retro -DD start
				//Retro From 9.8 Start
				//SUP_ISS_XI_8009 Retro starts
				//if(lCachedRowSet.getString("FLG_SAMEVALDAT").equalsIgnoreCase("Y"))
				if(accountInfoDo.getFLG_SAMEVALDAT() != null && accountInfoDo.getFLG_SAMEVALDAT().equalsIgnoreCase("Y"))
				//SUP_ISS_XI_8009 Retro ends
				{
					//SUP_IS1364 Starts
					//accountInfoDo.setDAT_VALUE(res.getDate("DAT_FINAL_SETTLEMENT"));
					//get Date final Settlement from AccountInfoDo as accountInfoDo contains the Calculated Final Settelment date based on Account level Calendar  
					accountInfoDo.setDAT_VALUE(accountInfoDo.getDAT_FINAL_SETTLEMENT());
					//SUP_IS1364 Ends
				}
				//Reto From 9.8 Ends
//SIT2DEV Retro -DD end
				else
				{
							
					gDate.setTime(lCachedRowSet.getDate("DAT_POSTING"));
					gDate.add(Calendar.DATE,1);
					incDateUtil =  gDate.getTime();
					incDateSql = new java.sql.Date(incDateUtil.getTime());
					accountInfoDo.setDAT_VALUE(incDateSql);
					if (logger.isDebugEnabled())
					{
					
						logger.debug("INCREMENTED DATE " + incDateSql);
						
					}
				}
				accountInfoDo.setSimId(simId);
				if (logger.isDebugEnabled()){
					logger.debug(" DATA OBJECT DETAILS " + accountInfoDo.toString());
				}
//SIT2DEV Retro -DD start
				
				//ENH_10.3_154 Starts
				accountInfoDo.setFLG_BACKVALUE_POSTING(lCachedRowSet.getString("FLG_BACKVALUE_POSTING"));
				//ENH_10.3_154 Ends
				//FLG_POSTCYCLE_CHANGES
				accountInfoDo.setFLG_POSTCYCLE(lCachedRowSet.getString("FLG_POSTCYCLE"));
				
				// NEW_ACCOUNTING_ENTRY_FORMAT
				accountInfoDo.setSOURCE_POOL_ID(lCachedRowSet.getString("SOURCE_POOL_ID")); 
				
				if (logger.isDebugEnabled()){
					logger.debug(" DATA OBJECT DETAILS IS >>>>> " + accountInfoDo.toString());
				}
				// JPMC_HOOK_POSTINGDAY
				ExecutionUtility.getExtraInfoHook().postingDayAdj(calendar,frequency,accountInfoDo);
				
				
				processAccountInfo(lotId,businessdate,client,pooldetails,insertAccountDtls,updateAccountDtls, insertAccrualSummary,  
//SIT2DEV Retro -DD end
						insertPostingSummary,  updateAccrualSummary, updatePostingSummary,insertUnpostedAmt,
						//updateUnpostedAmt,accountInfoDo,selectAccrualSummary,selectPostingSummary,//Reallocate to Self
						updateUnpostedAmt,updateUnpostedAmtCentralCcy,accountInfoDo,selectAccrualSummary,selectPostingSummary,
						//ENH_10.3_154 Starts
						/** ****** JTEST FIX start ************  on 14DEC09*/
						//selectAccountDtls,updateAccountDtlsForDates,updateAccuralSummaryForDates,updatePostingSummaryForDates);
						//selectAccountDtls,updateAccountDtlsForDates,updateAccuralSummaryForDates,updatePostingSummaryForDates, conn);
						/** ****** JTEST FIX end **************  on 14DEC09*/
						selectAccountDtls,updateAccountDtlsForDates,updateAccuralSummaryForDates,updatePostingSummaryForDates, conn,
						updateBVTAccrualSummaryForDates,updateBVTPostingSummaryForDates,accuralInfoMap,postingInfoMap,accountDtlsInfoMap);
						//ENH_10.3_154 Ends
			
				accountInfoDo.clearObject();
					
			}
			if(!recordExists)
			{
				logger.fatal(" No Transcation Found FOR TXN-CODE  RCINC/RCIND (Normal credit and debit interest)");        
			}
			
		}
		finally
		{
			//SUP_IS1612 starts'
			//SUP_IS1612 ends
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			LMSConnectionUtility.close(stmt);
			LMSConnectionUtility.close(pStmt);
			/** ****** JTEST FIX end **************  on 14DEC09*/
			LMSConnectionUtility.close(conn);
		}
		if (logger.isDebugEnabled()){
			logger.debug("Leaving");
		}
		
		//LMS_45_PERF_CERTIFICATION_PF24 end

	}

	/**
	 * This method  calls  populateAccuralSummary , populatePostingSummary ,populateAccountDtls and updateFinalSettlementAndValueDate method
	 * @param lotId
	 * @param businessdate
	 * @param client
	 * @param insertAccountDtls
	 * @param updateAccountDtls
	 * @param insertAccountSummary
	 * @param insertPostingSummary
	 * @param updateAccountSummary
	 * @param updatePostingSummary
	 * @param insertUnpostedAmt
	 * @param updateUnpostedAmt
	 * @param accountInfoDo
	 * @param selectAccrualSummary
	 * @param selectPostingSummary
	 * @param selectAccountDtls
	 * @param updateAccountDtlsForDates
	 * @param updateAccuralSummaryForDates
	 * @throws Exception
	 */
//SIT2DEV Retro -DD start
	private void processAccountInfo(String lotId, java.sql.Date businessdate, String client,HashMap pooldetails, 
//SIT2DEV Retro -DD end
			PreparedStatement insertAccountDtls, PreparedStatement updateAccountDtls,PreparedStatement insertAccountSummary, 
			PreparedStatement insertPostingSummary, PreparedStatement updateAccountSummary, 
			PreparedStatement updatePostingSummary,PreparedStatement insertUnpostedAmt,
			//PreparedStatement updateUnpostedAmt,AccountInfoDO accountInfoDo,//Reallocate to Self
			PreparedStatement updateUnpostedAmt,PreparedStatement updateUnpostedAmtCentralCcy, AccountInfoDO accountInfoDo,
			PreparedStatement selectAccrualSummary,PreparedStatement selectPostingSummary,
			PreparedStatement selectAccountDtls,PreparedStatement updateAccountDtlsForDates,
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			PreparedStatement updateAccuralSummaryForDates , PreparedStatement updatePostingSummaryForDates
			PreparedStatement updateAccuralSummaryForDates , PreparedStatement updatePostingSummaryForDates, Connection conn
			/** ****** JTEST FIX end **************  on 14DEC09*/
			//ENH_10.3_154 Starts //			//LMS_45_PERF_CERTIFICATION_PF24 start

			, PreparedStatement updateBVTAccrualSummaryForDates, PreparedStatement updateBVTPostingSummaryForDates, HashMap<String, AccrualInfoDO> accuralInfoMap, HashMap<String, PostingInfoDO> postingInfoMap, HashMap<String, AccountDtlsInfoDO> accountDtlsInfoMap
			//ENH_10.3_154 Ends  //LMS_45_PERF_CERTIFICATION_PF24 end
			) throws Exception 
	{	
		if (logger.isDebugEnabled()){
			logger.debug("Entering ");
		}
		
		
	
//SIT2DEV Retro -DD start
		//prePopulateAccuralSummary(lotId,businessdate, client,accountInfoDo);
		/** ****** JTEST FIX start ************  on 14DEC09*/
		//populateAccuralSummary(lotId,businessdate, client,pooldetails, insertAccountSummary, updateAccountSummary,insertUnpostedAmt,updateUnpostedAmt,accountInfoDo,selectAccrualSummary);
		//Reallocate to Self
		//populateAccuralSummary(lotId,businessdate, client,pooldetails, insertAccountSummary, updateAccountSummary,insertUnpostedAmt,updateUnpostedAmt,accountInfoDo,selectAccrualSummary, conn);
		populateAccuralSummary(lotId,businessdate, client,pooldetails, insertAccountSummary, updateAccountSummary,insertUnpostedAmt,updateUnpostedAmt,updateUnpostedAmtCentralCcy,accountInfoDo,selectAccrualSummary, conn,accuralInfoMap);////LMS_45_PERF_CERTIFICATION_PF24
		//Reallocate to Self
		/** ****** JTEST FIX end **************  on 14DEC09*/
//SIT2DEV Retro -DD end
		
		//prePopulatePostingSummary(lotId,businessdate, client,accountInfoDo);
//SIT2DEV Retro -DD start
		populatePostingSummary(lotId,businessdate, client,pooldetails, insertPostingSummary,  updatePostingSummary,accountInfoDo,selectPostingSummary,postingInfoMap);//LMS_45_PERF_CERTIFICATION_PF24
//SIT2DEV Retro -DD end
		
//SIT2DEV Retro -DD start
			
		populateAccountDtls( businessdate,  client,pooldetails, 
//SIT2DEV Retro -DD end
				insertAccountDtls,updateAccountDtls,
			accountInfoDo,selectAccountDtls,accountDtlsInfoMap );//LMS_45_PERF_CERTIFICATION_PF24
		
		//check whether DAT_POSTING is changed or not
//ENH_IS997 starts
		//SUP_IS800 STARTS
		//also check DAT_FINAL_SETTLEMENT 
		//if(accountInfoDo.getOLD_DAT_POSTING()!= null && ! accountInfoDo.getDAT_POSTING().toString().equalsIgnoreCase(accountInfoDo.getOLD_DAT_POSTING().toString()))
		//ENH_IS1297 RETROFIT STARTS
		//SUP_IS957 Start
		//if((accountInfoDo.getOLD_DAT_POSTING()!= null && ! accountInfoDo.getDAT_POSTING().toString().equalsIgnoreCase(accountInfoDo.getOLD_DAT_POSTING().toString()))
			//||(accountInfoDo.getOLD_DAT_FINAL_SETTLEMENT()!= null && ! accountInfoDo.getDAT_FINAL_SETTLEMENT().toString().equalsIgnoreCase(accountInfoDo.getOLD_DAT_FINAL_SETTLEMENT().toString())))
		//{
		//SUP_IS800 ENDS
//ENH_IS997 ends
			entries[8]=true;
			//ENH_10.3_154 Starts
			//updateFinalSettlementAndValueDate(updateAccountDtlsForDates,updateAccuralSummaryForDates,updatePostingSummaryForDates,accountInfoDo);
			updateFinalSettlementAndValueDate(updateAccountDtlsForDates,updateAccuralSummaryForDates,updatePostingSummaryForDates,updateBVTAccrualSummaryForDates,updateBVTPostingSummaryForDates,accountInfoDo);
			//ENH_10.3_154 Ends
		
		//}
			
		if (logger.isDebugEnabled()){
			logger.debug("Leaving ");
		}
	}
	
	
	

	/**
	 * This method set the parameter into updateAccountDtlsForDates prepareStatement ,updatePostingSummaryForDates,updateAccuralSummaryForDates
	 * @param updateAccountDtlsForDates
	 * @param updateAccuralSummaryForDates
	 * @param updatePostingSummaryForDates
	 * @param accountInfoDo
	 * @throws LMSException
	 */
	//ENH_10.3_154 Starts
	//public void updateFinalSettlementAndValueDate(PreparedStatement updateAccountDtlsForDates,PreparedStatement updateAccuralSummaryForDates ,PreparedStatement updatePostingSummaryForDates,AccountInfoDO accountInfoDo) throws LMSException
	public void updateFinalSettlementAndValueDate(PreparedStatement updateAccountDtlsForDates, PreparedStatement updateAccuralSummaryForDates, PreparedStatement updatePostingSummaryForDates, PreparedStatement updateBVTAccuralSummaryForDates, PreparedStatement updateBVTPostingSummaryForDates,AccountInfoDO accountInfoDo) throws LMSException
	//ENH_10.3_154 Ends
	{

		if (logger.isDebugEnabled()){
			logger.debug("Entering ");
		}
		try
		{
			if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
				logger.debug("Account info ID POOL "+ accountInfoDo.getID_POOL());
				logger.debug("Account info ID POOL "+ accountInfoDo.getSOURCE_POOL_ID());
				logger.debug("xecutionConstants.FLAG_BVT_CYCLE" + ExecutionConstants.FLAG_BVT_CYCLE);
				logger.debug("accountInfoDo is updateFinalSettlementAndValueDate  A " + accountInfoDo.toString()); ////ANZ_Defect#4879
			}
			java.sql.Date businesDate = accountInfoDo.getDAT_CURRENT_BUSINESS();//ENH_IS1731 Exec audit trailing changes
			//Setting parameter in updateAccountDtlsForDates
			int cnt =1;
			updateAccountDtlsForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POST_CREDITDELAY()));
			updateAccountDtlsForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_VALUE()));
			updateAccountDtlsForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING()));
			
			//ENH_IS1731 Exec audit trailing changes Starts
			// FindBug issue fix starts
			//updateAccountDtlsForDates.setString(4,com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT.format(new java.util.Date()));
			updateAccountDtlsForDates.setString(cnt++,REF_DATE_FORMAT.format(new java.util.Date()));//Harikesh
			// FindBug issue fix ends
			//Place holder for where Clause 
			updateAccountDtlsForDates.setString(cnt++,accountInfoDo.getPARTICIPANT_ID());
			//Retro From 9.8 SUP_IS1374 Start
			//updateAccountDtlsForDates.setDate(6,LMSUtility.truncSqlDate(accountInfoDo.getOLD_DAT_POSTING()));
			updateAccountDtlsForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING_START()));
			//Retro From 9.8 SUP_IS1374 Ends
			updateAccountDtlsForDates.setString(cnt++,accountInfoDo.getID_POOL());
			updateAccountDtlsForDates.setString(cnt++,"C");
			//ENH_IS1731 Exec audit trailing changes Ends
			//ENH_IS1297 RETROFIT STARTS
			//updateAccountDtlsForDates.setString(8,accountInfoDo.getTYP_PARTICIPANT());//SUP_IS957
			//ENH_IS1297 RETROFIT ENDS
			//SUP_IS2378 starts
			updateAccountDtlsForDates.setDate(cnt++,LMSUtility.truncSqlDate(new java.sql.Date(accountInfoDo.getDAT_CURRENT_BUSINESS().getTime())));
			updateAccountDtlsForDates.setString(cnt++,accountInfoDo.getSOURCE_POOL_ID()); //NEW_ACCOUNTING_ENTRY_FORMAT 
			//SUP_IS2378 ends
			updateAccountDtlsForDates.setString(cnt++, accountInfoDo.getTYP_PARTICIPANT());//ANZ_PRODISSUE 
			//ENH_10.3_154 Starts
			//if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y")) //ANZ_Defect_6549_1 //11.6_Retro_Cycle_3
			if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y") && !(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)))
				updateAccountDtlsForDates.setString(cnt++, "B");
			//ENH_10.3_154 Ends
			if(accountInfoDo.getSimId() !=0)
				updateAccountDtlsForDates.setLong(cnt++, accountInfoDo.getSimId());
			updateAccountDtlsForDates.addBatch();
			
			//Setting parameter in updateAccountDtlsForDates
			cnt =1;
			updateAccountDtlsForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POST_DEBITDELAY()));
			updateAccountDtlsForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_VALUE()));
			updateAccountDtlsForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING()));
			
			//ENH_IS1731 Exec audit trailing changes Starts
			// FindBug issue fix starts
			//updateAccountDtlsForDates.setString(4,com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT.format(new java.util.Date()));
			updateAccountDtlsForDates.setString(cnt++,REF_DATE_FORMAT.format(new java.util.Date()));//Harikesh
			// FindBug issue fix ends
			//Place holder for where Clause 
			updateAccountDtlsForDates.setString(cnt++,accountInfoDo.getPARTICIPANT_ID());
			//Retro From 9.8 SUP_IS1374 Start
			//updateAccountDtlsForDates.setDate(6,LMSUtility.truncSqlDate(accountInfoDo.getOLD_DAT_POSTING()));
			updateAccountDtlsForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING_START()));
			//Retro From 9.8 SUP_IS1374 Ends
			updateAccountDtlsForDates.setString(cnt++,accountInfoDo.getID_POOL());
			updateAccountDtlsForDates.setString(cnt++,"D");
			//ENH_IS1731 Exec audit trailing changes Ends
			//ENH_IS1297 RETROFIT STARTS
			//updateAccountDtlsForDates.setString(8,accountInfoDo.getTYP_PARTICIPANT());//SUP_IS957
			//SUP_IS2378 starts
			updateAccountDtlsForDates.setDate(cnt++,LMSUtility.truncSqlDate(new java.sql.Date(accountInfoDo.getDAT_CURRENT_BUSINESS().getTime())));
			//SUP_IS2378 ends
			//ENH_IS1297 RETROFIT ENDS
			//ENH_10.3_154 Starts
			updateAccountDtlsForDates.setString(cnt++,accountInfoDo.getSOURCE_POOL_ID()); //NEW_ACCOUNTING_ENTRY_FORMAT 
			updateAccountDtlsForDates.setString(cnt++, accountInfoDo.getTYP_PARTICIPANT());//ANZ_PRODISSUE 
			//if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y"))
			if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y") && !(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)))
				updateAccountDtlsForDates.setString(cnt++, "B");
			if(accountInfoDo.getSimId() !=0)
				updateAccountDtlsForDates.setLong(cnt++, accountInfoDo.getSimId());
			//ENH_10.3_154 Ends
			updateAccountDtlsForDates.addBatch();
			
			///FOR CREBIT ....//Setting parameter in updateAccuralSummaryForDates
			cnt =1;
			//Interest and Advantage Seperate Settlement Dates For JPMC
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("ExecutionConstants.FLG_IS_INTEREST_ADVANTAGE_SEPERATE_SETTLEMENT:="+ExecutionConstants.FLG_IS_INTEREST_ADVANTAGE_SEPERATE_SETTLEMENT);
			String accrualPricingInterfaceFeed = accountInfoDo.getAccrualPricingInterfaceFeed();
			if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
				logger.debug("getCOD_TXN:="+accountInfoDo.getCOD_TXN());
				logger.debug("getCOD_TXN().substring(2, 3):="+accountInfoDo.getCOD_TXN().substring(2, 3));
			}
			if("Y".equalsIgnoreCase(ExecutionConstants.FLG_IS_INTEREST_ADVANTAGE_SEPERATE_SETTLEMENT) && accountInfoDo.getCOD_TXN().substring(2, 3).equalsIgnoreCase("I") && !(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)) && "N".equalsIgnoreCase(accrualPricingInterfaceFeed)&& !(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN))){
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("Updating accrual summary for credit:"+accountInfoDo.getDAT_FINAL_SETTLEMENT());
				updateAccuralSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_FINAL_SETTLEMENT()));
			}else{
				logger.debug("IN else loop 2838");
				updateAccuralSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POST_CREDITDELAY()));
			}
			//Interest and Advantage Seperate Settlement Dates For JPMC
			//ENH_IS1731 Exec audit trailing changes Starts
			//ENH_IS997 starts
			//SUP_HSBC_394 starts
			//updateAccuralSummaryForDates.setDate(2,LMSUtility.truncSqlDate(accountInfoDo.getDAT_ACCRUAL()));
			updateAccuralSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING()));
			// FindBug issue fix starts
			//updateAccuralSummaryForDates.setString(3,com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT.format(new java.util.Date()));
			updateAccuralSummaryForDates.setString(cnt++,REF_DATE_FORMAT.format(new java.util.Date()));//Harikesh
			// FindBug issue fix ends
			updateAccuralSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(businesDate));
			//Place holder for where Clause 
			updateAccuralSummaryForDates.setString(cnt++,accountInfoDo.getPARTICIPANT_ID());
			//updateAccuralSummaryForDates.setDate(6,LMSUtility.truncSqlDate(accountInfoDo.getOLD_DAT_POSTING()));
			//Retro From 9.8 SUP_IS1374 Start
			//updateAccuralSummaryForDates.setDate(6,LMSUtility.truncSqlDate(accountInfoDo.getOLD_DAT_POSTING()));
			updateAccuralSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING_START()));
			//Retro From 9.8 SUP_IS1374 Ends
			updateAccuralSummaryForDates.setString(cnt++,accountInfoDo.getID_POOL());
			updateAccuralSummaryForDates.setString(cnt++,"C");
			//ENH_IS1731 Exec audit trailing changes Ends
			//ENH_IS1297 RETROFIT STARTS
			//updateAccuralSummaryForDates.setString(7,accountInfoDo.getTYP_PARTICIPANT());//SUP_IS957
			//ENH_IS1297 RETROFIT ENDS
			//SUP_HSBC_394 ends
//ENH_IS997 ends
			//SUP_IS2378 starts
			updateAccuralSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(new java.sql.Date(accountInfoDo.getDAT_CURRENT_BUSINESS().getTime())));
			updateAccuralSummaryForDates.setString(cnt++,accountInfoDo.getSOURCE_POOL_ID()); //NEW_ACCOUNTING_ENTRY_FORMAT 
			//SUP_IS2378 ends
			updateAccuralSummaryForDates.setString(cnt++, accountInfoDo.getTYP_PARTICIPANT());//ANZ_PRODISSUE 
			//Interest and Advantage Seperate Settlement Dates For JPMC
			if(!(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)) && !(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN))){
				updateAccuralSummaryForDates.setString(cnt++,accountInfoDo.getCOD_TXN().substring(2, 3));
			}
			//Interest and Advantage Seperate Settlement Dates For JPMC
			//ENH_10.3_154 Starts
			//if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y"))
			if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y") && !(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)))
				updateAccuralSummaryForDates.setString(cnt++, "B");
			//ENH_10.3_154 Ends
			if(accountInfoDo.getSimId() !=0)
				updateAccuralSummaryForDates.setLong(cnt++, accountInfoDo.getSimId());
			updateAccuralSummaryForDates.addBatch();
			
			///FOR DEBIT....//Setting parameter in updateAccuralSummaryForDates 
			cnt =1;
			//Interest and Advantage Seperate Settlement Dates For JPMC
			if("Y".equalsIgnoreCase(ExecutionConstants.FLG_IS_INTEREST_ADVANTAGE_SEPERATE_SETTLEMENT) && accountInfoDo.getCOD_TXN().substring(2, 3).equalsIgnoreCase("I") && !(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)) && "N".equalsIgnoreCase(accrualPricingInterfaceFeed)&& !(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN))){
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("Updating accrual summary for debit:"+accountInfoDo.getDAT_FINAL_SETTLEMENT());
				updateAccuralSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_FINAL_SETTLEMENT()));
			}else{
				updateAccuralSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POST_DEBITDELAY()));
			}
			//Interest and Advantage Seperate Settlement Dates For JPMC
//ENH_IS997 starts
			//SUP_HSBC_394 starts
			//updateAccuralSummaryForDates.setDate(2,LMSUtility.truncSqlDate(accountInfoDo.getDAT_ACCRUAL()));
			updateAccuralSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING()));
			//ENH_IS1731 Exec audit trailing changes Starts
			// FindBug issue fix starts
			//updateAccuralSummaryForDates.setString(3,com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT.format(new java.util.Date()));
			updateAccuralSummaryForDates.setString(cnt++,REF_DATE_FORMAT.format(new java.util.Date()));//Harikesh
			// FindBug issue fix ends
			updateAccuralSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(businesDate));
			//Place holder for where Clause 
			updateAccuralSummaryForDates.setString(cnt++,accountInfoDo.getPARTICIPANT_ID());
			//Retro From 9.8 SUP_IS1374 Start
			//updateAccuralSummaryForDates.setDate(6,LMSUtility.truncSqlDate(accountInfoDo.getOLD_DAT_POSTING()));
			updateAccuralSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING_START()));
			//Retro From 9.8 SUP_IS1374 Ends
			updateAccuralSummaryForDates.setString(cnt++,accountInfoDo.getID_POOL());
			updateAccuralSummaryForDates.setString(cnt++,"D");
			//ENH_IS1731 Exec audit trailing changes Ends
			//ENH_IS1297 RETROFIT STARTS
			//updateAccuralSummaryForDates.setString(7,accountInfoDo.getTYP_PARTICIPANT());//SUP_IS957
			//ENH_IS1297 RETROFIT ENDS
			//SUP_HSBC_394 ends
//ENH_IS997 starts
			//SUP_IS2378 starts
			updateAccuralSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(new java.sql.Date(accountInfoDo.getDAT_CURRENT_BUSINESS().getTime())));
			updateAccuralSummaryForDates.setString(cnt++,accountInfoDo.getSOURCE_POOL_ID()); //NEW_ACCOUNTING_ENTRY_FORMAT 
			//SUP_IS2378 ends
			updateAccuralSummaryForDates.setString(cnt++, accountInfoDo.getTYP_PARTICIPANT());//ANZ_PRODISSUE 
			//Interest and Advantage Seperate Settlement Dates For JPMC
			if(!(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT))&& !(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN))){
			updateAccuralSummaryForDates.setString(cnt++,accountInfoDo.getCOD_TXN().substring(2, 3));
			}
			//Interest and Advantage Seperate Settlement Dates For JPMC
			//ENH_10.3_154 Starts
			//if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y"))
			if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y") && !(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)))
				updateAccuralSummaryForDates.setString(cnt++, "B");
			if(accountInfoDo.getSimId() !=0)
				updateAccuralSummaryForDates.setLong(cnt++, accountInfoDo.getSimId());
			//ENH_10.3_154 Ends
			updateAccuralSummaryForDates.addBatch();
			
			///FOR CREBIT....//Setting parameter in updatePostingSummaryForDates
			cnt =1;
			//Interest and Advantage Seperate Settlement Dates For JPMC
			if("Y".equalsIgnoreCase(ExecutionConstants.FLG_IS_INTEREST_ADVANTAGE_SEPERATE_SETTLEMENT) && accountInfoDo.getCOD_TXN().substring(2, 3).equalsIgnoreCase("I") && !(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)) && "N".equalsIgnoreCase(accrualPricingInterfaceFeed)&& !(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN))){
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("Updating posting summary for credit:"+accountInfoDo.getDAT_FINAL_SETTLEMENT());
				updatePostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_FINAL_SETTLEMENT()));
			}else{
				updatePostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POST_CREDITDELAY()));
			}
			//Interest and Advantage Seperate Settlement Dates For JPMC
			updatePostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING()));
			updatePostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_VALUE()));
			
			//ENH_IS1731 Exec audit trailing changes Starts
			// FindBug issue fix starts
			//updatePostingSummaryForDates.setString(4,com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT.format(new java.util.Date()));
			updatePostingSummaryForDates.setString(cnt++,REF_DATE_FORMAT.format(new java.util.Date()));//Harikesh
			// FindBug issue fix ends
			updatePostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(businesDate));
			//Place holder for where Clause 
			updatePostingSummaryForDates.setString(cnt++,accountInfoDo.getPARTICIPANT_ID());
			//Retro From 9.8 SUP_IS1374 Start
			//updatePostingSummaryForDates.setDate(7,LMSUtility.truncSqlDate(accountInfoDo.getOLD_DAT_POSTING()));
			updatePostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING_START()));
			//Retro From 9.8 SUP_IS1374 Ends
			updatePostingSummaryForDates.setString(cnt++,accountInfoDo.getID_POOL());
			updatePostingSummaryForDates.setString(cnt++,"C");
			//ENH_IS1731 Exec audit trailing changes Ends
			//ENH_IS1297 RETROFIT STARTS
			//updatePostingSummaryForDates.setString(8,accountInfoDo.getTYP_PARTICIPANT());//SUP_IS957
			//SUP_IS2378 starts
			updatePostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(new java.sql.Date(accountInfoDo.getDAT_CURRENT_BUSINESS().getTime())));
			updatePostingSummaryForDates.setString(cnt++,accountInfoDo.getSOURCE_POOL_ID()); //NEW_ACCOUNTING_ENTRY_FORMAT 
			updatePostingSummaryForDates.setString(cnt++, accountInfoDo.getTYP_PARTICIPANT());//ANZ_PRODISSUE 
			//SUP_IS2378 ends
			//ENH_IS1297 RETROFIT ENDS
			//Interest and Advantage Seperate Settlement Dates For JPMC
			if(!(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT))&& !(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN))){
				updatePostingSummaryForDates.setString(cnt++,accountInfoDo.getCOD_TXN().substring(2, 3));
			}
			//Interest and Advantage Seperate Settlement Dates For JPMC
			//ENH_10.3_154 Starts
			//if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y"))
			if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y") && !(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)))
				updatePostingSummaryForDates.setString(cnt++, "B");
			if(accountInfoDo.getSimId() !=0)
				updatePostingSummaryForDates.setLong(cnt++, accountInfoDo.getSimId());
			//ENH_10.3_154 Ends
			updatePostingSummaryForDates.addBatch();
			
			//FOR DEBIT ....//Setting parameter in updatePostingSummaryForDates
			cnt =1;
			//Interest and Advantage Seperate Settlement Dates For JPMC
			if("Y".equalsIgnoreCase(ExecutionConstants.FLG_IS_INTEREST_ADVANTAGE_SEPERATE_SETTLEMENT) && accountInfoDo.getCOD_TXN().substring(2, 3).equalsIgnoreCase("I") && !(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)) && "N".equalsIgnoreCase(accrualPricingInterfaceFeed)&& !(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN))){
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("Updating posting summary for debit:"+accountInfoDo.getDAT_FINAL_SETTLEMENT());
				updatePostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_FINAL_SETTLEMENT()));
			}else{
				updatePostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POST_DEBITDELAY()));
			}
			//Interest and Advantage Seperate Settlement Dates For JPMC
			updatePostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING()));
			updatePostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_VALUE()));
			//ENH_IS1731 Exec audit trailing changes Starts
			// FindBug issue fix starts
			//updatePostingSummaryForDates.setString(cnt++,com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT.format(new java.util.Date()));
			updatePostingSummaryForDates.setString(cnt++,REF_DATE_FORMAT.format(new java.util.Date()));//Harikesh
			// FindBug issue fix ends
			updatePostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(businesDate));
			//Place holder for where Clause 
			updatePostingSummaryForDates.setString(cnt++,accountInfoDo.getPARTICIPANT_ID());
			//Retro From 9.8 SUP_IS1374 Start
			//updatePostingSummaryForDates.setDate(7,LMSUtility.truncSqlDate(accountInfoDo.getOLD_DAT_POSTING()));
			updatePostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING_START()));
			//Retro From 9.8 SUP_IS1374 Ends
			updatePostingSummaryForDates.setString(cnt++,accountInfoDo.getID_POOL());
			updatePostingSummaryForDates.setString(cnt++,"D");
			//ENH_IS1731 Exec audit trailing changes Ends
			//ENH_IS1297 RETROFIT STARTS
			//updatePostingSummaryForDates.setString(8,accountInfoDo.getTYP_PARTICIPANT());//SUP_IS957
			//SUP_IS2378 starts
			updatePostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(new java.sql.Date(accountInfoDo.getDAT_CURRENT_BUSINESS().getTime())));
			updatePostingSummaryForDates.setString(cnt++,accountInfoDo.getSOURCE_POOL_ID()); //NEW_ACCOUNTING_ENTRY_FORMAT 
			updatePostingSummaryForDates.setString(cnt++, accountInfoDo.getTYP_PARTICIPANT());//ANZ_PRODISSUE 
			//SUP_IS2378 ends
			//ENH_IS1297 RETROFIT ENDS
			//Interest and Advantage Seperate Settlement Dates For JPMC
			if(!(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT))&& !(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN))){
				updatePostingSummaryForDates.setString(cnt++,accountInfoDo.getCOD_TXN().substring(2, 3));
			}
			//Interest and Advantage Seperate Settlement Dates For JPMC
			//ENH_10.3_154 Starts
			//if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y"))
			if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y") && !(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)))
				updatePostingSummaryForDates.setString(cnt++, "B");
			//ENH_10.3_154 Ends
			if(accountInfoDo.getSimId() !=0)
				updatePostingSummaryForDates.setLong(cnt++, accountInfoDo.getSimId());
			updatePostingSummaryForDates.addBatch();
		
			String separateBvtAdjustmentAllowed = accountInfoDo.getFLG_BACKVALUE_POSTING();
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug(" separateBvtAdjustmentAllowed " + separateBvtAdjustmentAllowed);
			/*if(separateBvtAdjustmentAllowed != null && separateBvtAdjustmentAllowed.equalsIgnoreCase(ExecutionConstants.YES))
			{*/ //SUP_ISS_III_7305
				entries[11] = true;
				//ANZ_Defect#4879 STARTS
				cnt =1;
				///FOR CREBIT ....//Setting parameter in updateBVTAccuralSummaryForDates
				//Interest and Advantage Seperate Settlement Dates For JPMC
				if("Y".equalsIgnoreCase(ExecutionConstants.FLG_IS_INTEREST_ADVANTAGE_SEPERATE_SETTLEMENT) && accountInfoDo.getCOD_TXN().substring(2, 3).equalsIgnoreCase("I") && !(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)) && "N".equalsIgnoreCase(accrualPricingInterfaceFeed)&& !(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN))){
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("Updating accrual summary for credit BVT:"+accountInfoDo.getDAT_FINAL_SETTLEMENT());
					updateBVTAccuralSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_FINAL_SETTLEMENT()));
				}else{
					updateBVTAccuralSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POST_CREDITDELAY()));
				}
				//Interest and Advantage Seperate Settlement Dates For JPMC
				updateBVTAccuralSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING()));
				// FindBug issue fix starts
				//updateBVTAccuralSummaryForDates.setString(3,com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT.format(new java.util.Date()));
				updateBVTAccuralSummaryForDates.setString(cnt++,REF_DATE_FORMAT.format(new java.util.Date()));//Harikesh
				// FindBug issue fix ends
				updateBVTAccuralSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(businesDate));
				updateBVTAccuralSummaryForDates.setString(cnt++,accountInfoDo.getPARTICIPANT_ID());
				updateBVTAccuralSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING_START()));
				updateBVTAccuralSummaryForDates.setString(cnt++,accountInfoDo.getID_POOL());
				updateBVTAccuralSummaryForDates.setString(cnt++,"C");
				updateBVTAccuralSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(new java.sql.Date(accountInfoDo.getDAT_CURRENT_BUSINESS().getTime())));
				updateBVTAccuralSummaryForDates.setString(cnt++,accountInfoDo.getSOURCE_POOL_ID()); // NEW_ACCOUNTING_ENTRY_FORMAT//ANZ_IS FIXED
				updateBVTAccuralSummaryForDates.setString(cnt++,accountInfoDo.getTYP_PARTICIPANT());//ANZ_PRODISSUE 
				//Interest and Advantage Seperate Settlement Dates For JPMC
				if(!(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT))&& !(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN))){
				updateBVTAccuralSummaryForDates.setString(cnt++,accountInfoDo.getCOD_TXN().substring(2, 3));
				}
				//Interest and Advantage Seperate Settlement Dates For JPMC
				if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y")) //#ANZ_Defect#6549 //11.6_Retro_Cycle_3
				//if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y") && !(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)))
				{
					updateBVTAccuralSummaryForDates.setString(cnt++, "B");//ANZ_IS FIXED
				}
				updateBVTAccuralSummaryForDates.addBatch();
				cnt =1;
				///FOR DEBIT....//Setting parameter in updateBVTAccuralSummaryForDates 
				//Interest and Advantage Seperate Settlement Dates For JPMC
				if("Y".equalsIgnoreCase(ExecutionConstants.FLG_IS_INTEREST_ADVANTAGE_SEPERATE_SETTLEMENT) && accountInfoDo.getCOD_TXN().substring(2, 3).equalsIgnoreCase("I") && !(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)) && "N".equalsIgnoreCase(accrualPricingInterfaceFeed)&& !(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN))&& !(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN))){
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("Updating accrual summary for debit BVT:"+accountInfoDo.getDAT_FINAL_SETTLEMENT());
					updateBVTAccuralSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_FINAL_SETTLEMENT()));
				}else{
					updateBVTAccuralSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POST_DEBITDELAY()));
				}
				//Interest and Advantage Seperate Settlement Dates For JPMC
				updateBVTAccuralSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING()));
				// FindBug issue fix starts
				//updateBVTAccuralSummaryForDates.setString(3,com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT.format(new java.util.Date()));
				updateBVTAccuralSummaryForDates.setString(cnt++,REF_DATE_FORMAT.format(new java.util.Date()));//Harikesh
				// FindBug issue fix ends
				updateBVTAccuralSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(businesDate));
				updateBVTAccuralSummaryForDates.setString(cnt++,accountInfoDo.getPARTICIPANT_ID());
				updateBVTAccuralSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING_START()));
				updateBVTAccuralSummaryForDates.setString(cnt++,accountInfoDo.getID_POOL());
				updateBVTAccuralSummaryForDates.setString(cnt++,"D");
				updateBVTAccuralSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(new java.sql.Date(accountInfoDo.getDAT_CURRENT_BUSINESS().getTime())));
				updateBVTAccuralSummaryForDates.setString(cnt++,accountInfoDo.getSOURCE_POOL_ID()); // NEW_ACCOUNTING_ENTRY_FORMAT
				updateBVTAccuralSummaryForDates.setString(cnt++,accountInfoDo.getTYP_PARTICIPANT());//ANZ_PRODISSUE 
				//Interest and Advantage Seperate Settlement Dates For JPMC
				if(!(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT))&& !(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN))){
				updateBVTAccuralSummaryForDates.setString(cnt++,accountInfoDo.getCOD_TXN().substring(2, 3));
				}
				//Interest and Advantage Seperate Settlement Dates For JPMC
				if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y"))//#ANZ_Defect#6549//11.6_Retro_Cycle_3
				//if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y") && !(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)))
				{
					updateBVTAccuralSummaryForDates.setString(cnt++, "B");//ANZ_PRODISSUE 
				}
				updateBVTAccuralSummaryForDates.addBatch();
				cnt =1;
				///FOR CREBIT....//Setting parameter in updateBVTPostingSummaryForDates 
				//Interest and Advantage Seperate Settlement Dates For JPMC
				if("Y".equalsIgnoreCase(ExecutionConstants.FLG_IS_INTEREST_ADVANTAGE_SEPERATE_SETTLEMENT) && accountInfoDo.getCOD_TXN().substring(2, 3).equalsIgnoreCase("I") && !(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)) && "N".equalsIgnoreCase(accrualPricingInterfaceFeed)&& !(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN))){
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("Updating posting summary for credit BVT:"+accountInfoDo.getDAT_FINAL_SETTLEMENT());
					updateBVTPostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_FINAL_SETTLEMENT()));
				}else{
					updateBVTPostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POST_CREDITDELAY()));
				}
				//Interest and Advantage Seperate Settlement Dates For JPMC
				updateBVTPostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING()));
				updateBVTPostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_VALUE()));
				// FindBug issue fix starts
				//updateBVTPostingSummaryForDates.setString(4,com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT.format(new java.util.Date()));
				updateBVTPostingSummaryForDates.setString(cnt++,REF_DATE_FORMAT.format(new java.util.Date()));//HArikesh
				// FindBug issue fix ends
				updateBVTPostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(businesDate));
				//11.6_Retro_Cycle_3 start
				//#ANZ_Defect#6549 STARTS
				//if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y"))
					updateBVTPostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING()));
				//#ANZ_Defect#6549 ENDS
				//11.6_Retro_Cycle_3 ends
				updateBVTPostingSummaryForDates.setString(cnt++,accountInfoDo.getPARTICIPANT_ID());
				
				if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y"))
					updateBVTPostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING_START()));
				
				updateBVTPostingSummaryForDates.setString(cnt++,accountInfoDo.getID_POOL());
				updateBVTPostingSummaryForDates.setString(cnt++,"C");
				updateBVTPostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(new java.sql.Date(accountInfoDo.getDAT_CURRENT_BUSINESS().getTime())));
				updateBVTPostingSummaryForDates.setString(cnt++,accountInfoDo.getSOURCE_POOL_ID()); // NEW_ACCOUNTING_ENTRY_FORMAT
				updateBVTPostingSummaryForDates.setString(cnt++,accountInfoDo.getTYP_PARTICIPANT());//ANZ_PRODISSUE 
				//Interest and Advantage Seperate Settlement Dates For JPMC
				if(!(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT))&& !(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN))){
				updateBVTPostingSummaryForDates.setString(cnt++,accountInfoDo.getCOD_TXN().substring(2, 3));
				}
				//Interest and Advantage Seperate Settlement Dates For JPMC
				if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y")) //#ANZ_Defect#6549//11.6_Retro_Cycle_3
				//if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y") && !(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)))
				{
					updateBVTPostingSummaryForDates.setString(cnt++, "B");//ANZ_PRODISSUE 
				}
				updateBVTPostingSummaryForDates.addBatch();
				cnt =1;
				//FOR DEBIT ....//Setting parameter in updateBVTPostingSummaryForDates
				//Interest and Advantage Seperate Settlement Dates For JPMC
				if("Y".equalsIgnoreCase(ExecutionConstants.FLG_IS_INTEREST_ADVANTAGE_SEPERATE_SETTLEMENT) && accountInfoDo.getCOD_TXN().substring(2, 3).equalsIgnoreCase("I") && !(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)) && "N".equalsIgnoreCase(accrualPricingInterfaceFeed)&& !(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN))){
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("Updating posting summary for debit BVT:"+accountInfoDo.getDAT_FINAL_SETTLEMENT());
					updateBVTPostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_FINAL_SETTLEMENT()));
				}else{
					updateBVTPostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POST_DEBITDELAY()));
				}
				//Interest and Advantage Seperate Settlement Dates For JPMC
				updateBVTPostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING()));
				updateBVTPostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_VALUE()));
				// FindBug issue fix starts
				//updateBVTPostingSummaryForDates.setString(4,com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT.format(new java.util.Date()));
				updateBVTPostingSummaryForDates.setString(cnt++,REF_DATE_FORMAT.format(new java.util.Date()));//Harikesh
				// FindBug issue fix ends
				updateBVTPostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(businesDate));
				
                //11.6_Retro_Cycle_3 start
				//#ANZ_Defect#6549 STARTS
				//if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y"))
					updateBVTPostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING()));
				//#ANZ_Defect#6549 ENDS
				//11.6_Retro_Cycle_3 ends
				updateBVTPostingSummaryForDates.setString(cnt++,accountInfoDo.getPARTICIPANT_ID());
				if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y")) // ENH_MODEL1_VAR Issue fix for BV posting as separate
					updateBVTPostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING_START()));
				updateBVTPostingSummaryForDates.setString(cnt++,accountInfoDo.getID_POOL());
				updateBVTPostingSummaryForDates.setString(cnt++,"D");
				updateBVTPostingSummaryForDates.setDate(cnt++,LMSUtility.truncSqlDate(new java.sql.Date(accountInfoDo.getDAT_CURRENT_BUSINESS().getTime())));
				updateBVTPostingSummaryForDates.setString(cnt++,accountInfoDo.getSOURCE_POOL_ID()); // NEW_ACCOUNTING_ENTRY_FORMAT
				updateBVTPostingSummaryForDates.setString(cnt++,accountInfoDo.getTYP_PARTICIPANT());//ANZ_PRODISSUE 
				//Interest and Advantage Seperate Settlement Dates For JPMC
				if(!(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT))&& !(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN))){
				updateBVTPostingSummaryForDates.setString(cnt++,accountInfoDo.getCOD_TXN().substring(2, 3));
				}
				//Interest and Advantage Seperate Settlement Dates For JPMC
				if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y")) //#ANZ_Defect#6549//11.6_Retro_Cycle_3
				//if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y") && !(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)))
				{
					updateBVTPostingSummaryForDates.setString(cnt++, "B");//ANZ_PRODISSUE 
				}
				updateBVTPostingSummaryForDates.addBatch();
				 //ANZ_Defect#4879 STARTS
			//}//SUP_ISS_III_7305
			
		}
		catch(SQLException e)
		{
	
			logger.fatal("Exception :: " , e);
			//ENH_IS1059 Starts  
			//throw new LMSException("Error occured in updateFinalSettlementAndValueDate", e.getMessage());
			throw new LMSException("LMS01000","Updation of Accounting details ,Posting Summary,Accural Summary and value date failed for Pool Id:-" +accountInfoDo.getID_POOL()+", Participant Id:-"+accountInfoDo.getPARTICIPANT_ID()+" and Participant type:- "+accountInfoDo.getTYP_PARTICIPANT(), e);
			//ENH_IS1059  Ends
		}
		if (logger.isDebugEnabled()){
			logger.debug("Leaving ");
		}
		
	}

	/**
	 * This method set parameter into insertAccountDtls perparedStatment 
	 * @param insertAccountDtls
	 * @param accountInfoDo
	 * @throws LMSException
	 */
	private void insertAccountDetails( PreparedStatement insertAccountDtls, AccountInfoDO accountInfoDo) throws LMSException 
	{
		if (logger.isDebugEnabled()){
			logger.debug("Entering ");
		}
		try
		{
			insertAccountDtls.setString(1,accountInfoDo.getID_POOL());
			insertAccountDtls.setString(2,accountInfoDo.getPARTICIPANT_ID());
			String codTxn = accountInfoDo.getCOD_TXN(); 
			insertAccountDtls.setString(3,codTxn);
			insertAccountDtls.setDate(4,LMSUtility.truncSqlDate(accountInfoDo.getDAT_CURRENT_BUSINESS()));
			insertAccountDtls.setDate(5,LMSUtility.truncSqlDate(accountInfoDo.getDAT_BUSINESS()));
			insertAccountDtls.setDate(6,LMSUtility.truncSqlDate(accountInfoDo.getDAT_FINAL_SETTLEMENT()));
			insertAccountDtls.setBigDecimal(7,accountInfoDo.getAMT_CALCULATED());
			insertAccountDtls.setDate(8,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING()));
			insertAccountDtls.setString(9,accountInfoDo.getFLG_REALPOOL());
			insertAccountDtls.setString(10,accountInfoDo.getFLG_ACC_CONSOLIDATED());
			insertAccountDtls.setString(11,accountInfoDo.getFLG_POOL_CONSOLIDATED());
			
			insertAccountDtls.setString(12,accountInfoDo.getFLG_REALPOSTING());
			insertAccountDtls.setDate(13,LMSUtility.truncSqlDate(accountInfoDo.getDAT_VALUE()));
			//ENH_IS1731 Exec audit trailing changes Starts
			insertAccountDtls.setString(14,accountInfoDo.getTYP_PARTICIPANT());
			// FindBug issue fix starts
			//insertAccountDtls.setString(15,com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT.format(new java.util.Date()));
			insertAccountDtls.setString(15,REF_DATE_FORMAT.format(new java.util.Date()));//Harikesh
			// FindBug issue fix ends
			//ENH_IS1731 Exec audit trailing changes Ends
			//Retro from 9.8 SUP_IS1374 Start
			insertAccountDtls.setDate(16,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING_START()));	//HSBC_SUP1374
			insertAccountDtls.setString(17,accountInfoDo.getSOURCE_POOL_ID());	//NEW_ACCOUNTING_ENTRY_FORMAT 
			if(accountInfoDo.getSimId() != 0)
				insertAccountDtls.setLong(18, accountInfoDo.getSimId());
			//Retro from 9.8 SUP_IS1374 Ends 
			insertAccountDtls.addBatch();
		}
		catch(SQLException e)
		{

			logger.fatal("Exception :: " , e);
			//ENH_IS1059 Starts	
			//throw new LMSException("Error occured in insertAccountDetails", e.getMessage());
			throw new LMSException("LMS01000","Insertion of Account details failed for Pool Id :-"+accountInfoDo.getID_POOL()+", Participant Id:-"+accountInfoDo.getPARTICIPANT_ID()+" and Participant type:- "+accountInfoDo.getTYP_PARTICIPANT(), e);
			//ENH_IS1059 Ends
			
		}
		if (logger.isDebugEnabled()){
			logger.debug("Leaving ");
		}
		
	}
	
	
	/**
	 * This method basically deals with accountSummary table it desides whether accrual  cycle strat or end and does the update or insert operation 
	 * @param lotId
	 * @param businessDate
	 * @param client
	 * @param insertAccrualSummary
	 * @param updateAccrualSummary
	 * @param insertUnpostedAmt
	 * @param updateUnpostedAmt
	 * @param accountInfoDo
	 * @param selectAccrualSummary
	 * @param accuralInfoMap 
	 * @throws Exception
	 */
//SIT2DEV Retro -DD start
	public void populateAccuralSummary(String lotId, java.sql.Date businessDate, String client,  
			HashMap pooldetails,PreparedStatement insertAccrualSummary, PreparedStatement updateAccrualSummary,
//SIT2DEV Retro -DD end
			//PreparedStatement insertUnpostedAmt,PreparedStatement updateUnpostedAmt,//Reallocate to Self
			PreparedStatement insertUnpostedAmt,PreparedStatement updateUnpostedAmt,PreparedStatement updateUnpostedAmtCentralCcy,
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			AccountInfoDO accountInfoDo,PreparedStatement selectAccrualSummary
			AccountInfoDO accountInfoDo,PreparedStatement selectAccrualSummary, Connection conn, HashMap<String, AccrualInfoDO> accuralInfoMap//LMS_45_PERF_CERTIFICATION_PF24
			/** ****** JTEST FIX end **************  on 14DEC09*/
			) throws Exception 
	{
		if (logger.isDebugEnabled()){
			logger.debug("Entering ");
		}
		boolean cycleStart = true;
		BigDecimal amtAccrued = ZERO;
		BigDecimal amtUnaccrued = ZERO;
		BigDecimal calculatedAmount = ZERO;
		BigDecimal amtFromUnPosted = ZERO;//from cache
		//Reallocate to Self
		BigDecimal amtFromUnPostedCentralCcy = ZERO;//from cache
		BigDecimal calculatedAmtCentralCcy = ZERO;
		BigDecimal amtAccruedCentralCcy = ZERO;
		//Reallocate to Self
//ENH_IS997 starts
		String key = null;//SUP_IS764
		//get the Transaction Code 
//ENH_IS997 ends
		String codTxn =  accountInfoDo.getCOD_TXN(); 
		
		//Identify the Accrual cycle start 
		cycleStart  = checkRecordInAccrualSummary(accountInfoDo,accuralInfoMap);//LMS_45_PERF_CERTIFICATION_PF24
	
		calculatedAmount = accountInfoDo.getAMT_CALCULATED()!=null?accountInfoDo.getAMT_CALCULATED():ZERO;
		
		amtUnaccrued = accountInfoDo.getAMT_UNACCRUED();
		
		calculatedAmtCentralCcy = accountInfoDo.getAMT_CAL_CENTRAL_CCY();//Reallocate to Self
		
		
		if (logger.isDebugEnabled()){
			logger.debug("businessDate " + businessDate.toString());
			logger.debug("cycleStart : "+cycleStart);
			logger.debug("accountInfoDo.getTXT_STATUS() : "+accountInfoDo.getTXT_STATUS());
			
		}
		//RBS-6518 Starts  
		if(cycleStart == false && ExecutionConstants.NOT_APPL.equalsIgnoreCase(accountInfoDo.getTXT_STATUS())) {	
		// RBS - 6545 Start
		//if(cycleStart == false && (accountInfoDo.getTXT_STATUS()!=null) && ExecutionConstants.NOT_APPL.equalsIgnoreCase(accountInfoDo.getTXT_STATUS())) {
		//if(cycleStart == false && (accountInfoDo.getTXT_STATUS()==null || ExecutionConstants.NOT_APPL.equalsIgnoreCase(accountInfoDo.getTXT_STATUS()))) {	
		//RBS-6518 Ends
		// RBS - 6545 Ends
			
			logger.debug("Inside if condition : not existing and not applicale "+cycleStart); //sampada
			return;
		}
		// Check for Accrual Cycle End	
//SIT2DEV Retro -DD start
		//Retro From 9.8 Starts
		// SUP_IS1322 STARTS
		// if(businessDate.toString().equalsIgnoreCase(accountInfoDo.getDAT_ACCRUAL().toString())
		// ||
		// businessDate.toString().equalsIgnoreCase(accountInfoDo.getPARTY_DAT_END().toString())
		// )
		if (businessDate.toString().equalsIgnoreCase(
				accountInfoDo.getDAT_ACCRUAL().toString())
				|| businessDate.toString().equalsIgnoreCase(
						accountInfoDo.getPARTY_DAT_END().toString())
//SUP_IS2925 starts
				//|| (pooldetails!=null && (pooldetails.get("ID_POOL").toString().equalsIgnoreCase(accountInfoDo.getID_POOL().toString()) && pooldetails.get("VALID").toString().equalsIgnoreCase("true"))))
				|| (pooldetails != null && (pooldetails.containsKey(accountInfoDo.getID_POOL())&& ((String)pooldetails.get(accountInfoDo.getID_POOL())).equalsIgnoreCase("true"))))
//SUP_IS2925 ends
		// SUP_IS1322 ENDS
		{
		//Retro from9.8 Ends
//SIT2DEV Retro -DD end
			if (logger.isDebugEnabled()){
				logger.debug("Accrual Cycle END ");
			}
			//get unposted amount from cache 
//ENH_IS997 starts
			//SUP_IS764 starts
			//if(cacheAmtUnposted.containsKey(accountInfoDo.getPARTICIPANT_ID()+codTxn))
			// NEW_ACCOUNTING_ENTRY_FORMAT
			//key = accountInfoDo.getID_POOL()+ExecutionConstants.SEPARATOR+accountInfoDo.getPARTICIPANT_ID()+ExecutionConstants.SEPARATOR+codTxn;
			//key = accountInfoDo.getID_POOL()+ExecutionConstants.SEPARATOR+accountInfoDo.getSOURCE_POOL_ID()+ExecutionConstants.SEPARATOR+accountInfoDo.getPARTICIPANT_ID()+ExecutionConstants.SEPARATOR+codTxn;
			key = accountInfoDo.getID_POOL()+ExecutionConstants.SEPARATOR+accountInfoDo.getPARTICIPANT_ID()+ExecutionConstants.SEPARATOR+codTxn+ExecutionConstants.SEPARATOR+accountInfoDo.getSOURCE_POOL_ID();
			if (logger.isDebugEnabled()){
				logger.debug("appp---KEY VALUE ---- 1"+key );
				logger.debug("cacheAmtUnposted ---- 1"+cacheAmtUnposted.toString() );
				
			}
			if(cacheAmtUnposted.containsKey(key))
			//SUP_IS764 ends
//ENH_IS997 ends
			{
//ENH_IS997 starts
				if (logger.isDebugEnabled()){
					logger.debug("inside if cacheAmtUnposted contain key----" );
				}
				//SUP_IS764 starts
				//amtFromUnPosted = (BigDecimal)cacheAmtUnposted.get(accountInfoDo.getPARTICIPANT_ID()+codTxn);
				amtFromUnPosted = (BigDecimal)cacheAmtUnposted.get(key);
				//SUP_IS764 ends
				//also remove the entry from the HashMap as it has already been used
				//SUP_IS764 starts
				//cacheAmtUnposted.remove(accountInfoDo.getPARTICIPANT_ID()+codTxn);
				cacheAmtUnposted.remove(key);
				//SUP_IS764 ends
//ENH_IS997 ends
				
			}
			//Reallocate to Self
			if(cacheAmtUnpostedCentralCcy.containsKey(key))
			{
				if (logger.isDebugEnabled()){
					logger.debug("inside if cacheAmtUnposted contain key----" );
				}
				amtFromUnPostedCentralCcy = (BigDecimal)cacheAmtUnpostedCentralCcy.get(key);
				cacheAmtUnpostedCentralCcy.remove(key);
			}
			//Reallocate to Self
			if (logger.isDebugEnabled()){
				logger.debug("calculatedAmount is >> "+calculatedAmount+ " amtUnaccrued is >>> "+ amtUnaccrued  + " amtFromUnPosted is "+ amtFromUnPosted);
			}
			if( amtFromUnPosted.compareTo(ZERO) != 0 || accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase("CENTRAL"))
			{
				
				//update unposted amount as used ie set USE_DATE =Current Business Date
				entries[5]=true;
				updateUnpostedAmtToAcctUnpostedTbl(updateUnpostedAmt,accountInfoDo,ExecutionConstants.POOLING_EOD);//Carry Forward Changes
			}
			//Reallocate to Self starts
			if( amtFromUnPostedCentralCcy.compareTo(ZERO) != 0 && accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase("ACCOUNT"))
			{
				
				//update unposted amount as used ie set USE_DATE =Current Business Date
				entries[12]=true;
				updateUnpostedAmtToAcctUnpostedTblCentral(updateUnpostedAmtCentralCcy,accountInfoDo,ExecutionConstants.POOLING_EOD);//Carry Forward Changes
			}
			//amtAccrued = calculatedAmount + amtUnaccrued + amtFromUnPosted
			//Carry Forward Changes Start
			if(ExecutionConstants.NEW_CARRY_FORWARD_LOGIC.equalsIgnoreCase("Y")){
				if(!(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase("CENTRAL"))){
					amtAccrued = calculatedAmount.add(amtUnaccrued).add(amtFromUnPosted);
				//amtAccruedCentralCcy = calculatedAmtCentralCcy.add(amtFromUnPostedCentralCcy);
				}
				else
					amtAccrued = calculatedAmount.add(amtUnaccrued);
				}
			else{
				amtAccrued = calculatedAmount.add(amtUnaccrued).add(amtFromUnPosted);
				//amtAccruedCentralCcy = calculatedAmtCentralCcy.add(amtFromUnPostedCentralCcy);
			}
			//Reallocate to Self ends
			//Carry Forward Changes End
			//Apply Caryforward logic to amtAccrued
			
			ExecutionUtility util=new ExecutionUtility();
			BigDecimal[] precision_carryfwd ={ZERO,ZERO};
			BigDecimal[] precision_carryfwdCentralCcy ={ZERO,ZERO};//Reallocate to Self
//ENH_IS997 starts
			//SUP_IS764 starts
			//if today is not participant end  then apply carry foward logic
			if(! businessDate.toString().equalsIgnoreCase(accountInfoDo.getPARTY_DAT_END().toString()))
			{	
			//SUP_IS764 ends	
//ENH_IS997 ends
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			precision_carryfwd = util.getTruncatedAmt(amtAccrued,ExecutionConstants.ACCRUAL,accountInfoDo.getCOD_CCY());
			precision_carryfwd = util.getTruncatedAmt(amtAccrued,ExecutionConstants.ACCRUAL,accountInfoDo.getCOD_CCY(), conn);
			//Reallocate to Self
			if(calculatedAmtCentralCcy.compareTo(new BigDecimal("0"))!=0 && (accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase("ACCOUNT"))){
				if (logger.isDebugEnabled()){
					logger.debug("amtAccrued----" +amtAccrued+", RATE_EXRATE_CENTRAL_CCY----"+accountInfoDo.getRATE_EXRATE_CENTRAL_CCY());
				}
				calculatedAmtCentralCcy=amtAccrued.multiply(new BigDecimal(accountInfoDo.getRATE_EXRATE_CENTRAL_CCY().doubleValue()));
				if (logger.isDebugEnabled()){
					logger.debug("calculatedAmtCentralCcy----" +calculatedAmtCentralCcy);
				}
			}
			
			if(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase("ACCOUNT")){
				amtAccruedCentralCcy = calculatedAmtCentralCcy.add(amtFromUnPostedCentralCcy);
			}
			precision_carryfwdCentralCcy = util.getTruncatedAmt(amtAccruedCentralCcy,ExecutionConstants.ACCRUAL,accountInfoDo.getCOD_CCY(), conn);
			//Reallocate to Self
			/** ****** JTEST FIX end **************  on 14DEC09*/
			//set Truncated amount to amtAccured
			amtAccrued=precision_carryfwd[0];
			amtAccruedCentralCcy=precision_carryfwdCentralCcy[0];//Reallocate to Self
			
			
				
			if (logger.isDebugEnabled())
			{
				logger.debug("Truncated amtAccrued" + amtAccrued);
				logger.debug("Carry Forward Amount " + precision_carryfwd[1]);
				logger.debug("Carry Forward Central ccyAmount " + precision_carryfwdCentralCcy[1]);//Reallocate to Self
			}
			//Add Carry Forward Amount to OPOOL_ACCOUNT_UNPOSTED table
			if( precision_carryfwd[1].compareTo(ZERO) != 0)
			{
				entries[6]=true;
				addAmtToAcctUnpostedTbl(insertUnpostedAmt,precision_carryfwd[1],"AMOUNT_CARRYFORWARD",accountInfoDo,"NORMAL");
			}
			//Reallocate to Self
			if( precision_carryfwdCentralCcy[1].compareTo(ZERO) != 0)
			{
				entries[6]=true;
				addAmtToAcctUnpostedTbl(insertUnpostedAmt,precision_carryfwdCentralCcy[1],"AMOUNT_CARRYFORWARD_CENTRAL_CCY",accountInfoDo,"NORMAL");
			}
			//Reallocate to Self
			//if((amtAccrued).abs().compareTo(accountInfoDo.getMINAMT()) if -1 add to unposted table 
			//AMT_ACCRUED = AMT_TO_BE_POSTED = AMT_UNACCRUED=0 TXT_STATUS = NO_ACC
			//else as follows
			if(amtAccrued.abs().compareTo(accountInfoDo.getMINAMT())== -1 )
			{
				if (logger.isDebugEnabled())
				{
					logger.debug("AMOUNT_LESSTHAN_MIN_AMOUNT");
				}
				entries[6]=true;
				addAmtToAcctUnpostedTbl(insertUnpostedAmt,amtAccrued,"AMOUNT_LESSTHAN_MIN_AMOUNT",accountInfoDo,"NORMAL");
				accountInfoDo.setAMT_ACCRUED(ZERO);
			
				accountInfoDo.setAMT_TO_BE_POSTED(ZERO);
				
				accountInfoDo.setAMT_UNACCRUED(ZERO);
				
				accountInfoDo.setTXT_STATUS(ExecutionConstants.NOACCRUAL_STATUS);
				
				
			}
			else
			{
				if (logger.isDebugEnabled())
				{
					logger.debug("AMOUNT_GREATERTHAN_MIN_AMOUNT");
				}
				accountInfoDo.setAMT_ACCRUED(amtAccrued);
				accountInfoDo.setAMT_CAL_CENTRAL_CCY(amtAccruedCentralCcy);//Reallocate to Self
				//amountToBePosted = amtAccrued
				accountInfoDo.setAMT_TO_BE_POSTED(amtAccrued);
				//Resetting amtUnaccrued to 0 
				accountInfoDo.setAMT_UNACCRUED(ZERO);
				//Status = ACC_GEN
				accountInfoDo.setTXT_STATUS(ExecutionConstants.ACCRUAL_GENERATED_STATUS);
				}
//ENH_IS997
			//SUP_IS764 starts	
			}
			//SUP_IS764 ends
//ENH_IS997
			/*In normal execution , when participant ends we need to set the 
			DAT_ACCRUAL
			DAT_POSTING
			DAT_FINAL_SETTLEMENT businessDate
			DAT_VALUE  as businessDate*/
//ENH_IS997 starts
			//SUP_IS764 starts	
//SIT2DEV Retro -DD start
				//Retro from 9.8 starts
			// SUP_IS1322 STARTS
//SUP_IS2925 starts
			//if(businessDate.toString().equalsIgnoreCase(accountInfoDo.getPARTY_DAT_END().toString()) || (pooldetails!=null && (pooldetails.get("ID_POOL").toString().equalsIgnoreCase(accountInfoDo.getID_POOL().toString()) && pooldetails.get("VALID").toString().equalsIgnoreCase("true"))))
			if(businessDate.toString().equalsIgnoreCase(accountInfoDo.getPARTY_DAT_END().toString()) || (pooldetails != null && pooldetails.containsKey(accountInfoDo.getID_POOL()) && ((String)pooldetails.get(accountInfoDo.getID_POOL())).equalsIgnoreCase("true")) && accountInfoDo.getFLG_POSTCYCLE().equalsIgnoreCase(ExecutionConstants.NO))//JPMC_IS FIXED
//SUP_IS2925 ends
			//else//need to uncomment
				//SUP_IS1322 ENDS
			// SUP_IS764 ends
			//Retro from 9.8 Ends
//SIT2DEV Retro -DD end
			{
					logger.fatal("PARTY END ");
					
					//SUP_ISS_V_2772 STARTS
					Calendar calendar = Calendar.getInstance();
					if (!accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT) && !(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN))){
					//SUP_ISS_XX_8097 
						accountInfoDo.setDAT_ACCRUAL(businessDate);
						//accountInfoDo.setDAT_POSTING(businessDate);
						//ENH_IS997 starts
						//SUP_389 starts
						//Calendar calendar = Calendar.getInstance();
						calendar.setTime(businessDate);
						//SUP_ISS_XX_8097 starts
						calendar.add(Calendar.DATE,ExecutionConstants.POOL_ENDDATE_DELAY);
						//accountInfoDo.setDAT_VALUE(new java.sql.Date(calendar.getTime().getTime()));
						accountInfoDo.setDAT_ADJUSTEDACCRUAL(businessDate);
						//SUP_ISS_XX_8097 ends
					}
				//accountInfoDo.setDAT_ACCRUAL(businessDate);
				//accountInfoDo.setDAT_POSTING(businessDate);
//ENH_IS997 starts
				//SUP_389 starts
				//Calendar calendar = Calendar.getInstance();
				//calendar.setTime(businessDate);
					//SUP_ISS_V_2772 ENDS
				// Retro from JPMC_ LIQUIDITY_11.4.1_BUGFIX JPMC to 11.5 starts
				//calendar.add(Calendar.DATE,ExecutionConstants.ENDDATE_DELAY); JPMC_POOL_ENDDATE_DELAY
				// Retro from JPMC_ LIQUIDITY_11.4.1_BUGFIX JPMC to 11.5 ends
				//added by dibu on 16-Dec-2008 for SEB start	
				//NT_UT_BUFIX_03_Jan_09 Start  -Null check added in if condition 
				//if (accountInfoDo.getFLG_POSTCYCLE().equalsIgnoreCase(ExecutionConstants.NO)) 
				//NT_UT_BUFIX_09_Jan_09 Start
				logger.fatal("PARTY END  POOL ID----"+accountInfoDo.getID_POOL());
				//Added For Calculating Settlement Date Starts
				//if (!(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT))&& !(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCO)))
				//if (!(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT))&& !(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN)))
				// FLG_POSTCYCLE_CHANGES NITIN FOLLOW POSTING CYCLE FLG - JPMC
					if (!(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT))&& !(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN)) && (accountInfoDo.getFLG_POSTCYCLE()!=null &&  accountInfoDo.getFLG_POSTCYCLE().equalsIgnoreCase(ExecutionConstants.NO)))
				//Added For Calculating Settlement Date Ends
				{
				// Retro from JPMC_ LIQUIDITY_11.4.1_BUGFIX JPMC to 11.5 starts
						/*  Issue 2408 Start */
						Calendar calendar_date = Calendar.getInstance();
						calendar_date.setTime(businessDate);
						calendar_date.add(Calendar.DATE,ExecutionConstants.POOL_ENDDATE_DELAY);// JPMC_POOL_ENDDATE_DELAY Start
				// Retro from JPMC_ LIQUIDITY_11.4.1_BUGFIX JPMC to 11.5 ends
					logger.fatal("Inside else of end FLG_POSTCYCLE == 'N' ");
					accountInfoDo.setDAT_FINAL_SETTLEMENT(new java.sql.Date(calendar_date.getTime().getTime()));
					
					//accountInfoDo.setDAT_POST_DEBITDELAY(businessDate);
					accountInfoDo.setDAT_POST_DEBITDELAY(new java.sql.Date(calendar_date.getTime().getTime()));
					accountInfoDo.setDAT_POST_CREDITDELAY(new java.sql.Date(calendar_date.getTime().getTime()));
					/*  Issue 2408 End */
				}
				//NT_UT_BUFIX_09_Jan_09 End
				//Added For Calculating Settlement Date Starts
				//if (accountInfoDo.getFLG_POSTCYCLE()!=null &&  accountInfoDo.getFLG_POSTCYCLE().equalsIgnoreCase(ExecutionConstants.NO))
				if (accountInfoDo.getFLG_POSTCYCLE()!=null &&  accountInfoDo.getFLG_POSTCYCLE().equalsIgnoreCase(ExecutionConstants.NO) 
					&& accountInfoDo.getTYP_INTPOSTOPTION()!=null && accountInfoDo.getTYP_INTPOSTOPTION().equalsIgnoreCase(ExecutionConstants.CYCLE) 
					&& accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN))
				//Added For Calculating Settlement Date Ends
				{
					logger.fatal("Entering if FLG_POSTCYCLE == 'N'");
					//NT_UT_BUFIX_03_Jan_09 End
                //added by dibu on 16-Dec-2008 for SEB end
				//accountInfoDo.setDAT_FINAL_SETTLEMENT(businessDate);
					
					String calcode=null;
					java.util.Date finalSettlementDate = null;
					java.util.Date busDate=null;
					// Retro from JPMC_ LIQUIDITY_11.4.1_BUGFIX JPMC to 11.5 starts
					calendar.add(Calendar.DATE,ExecutionConstants.ENDDATE_DELAY);// JPMC_POOL_ENDDATE_DELAY Start
					// Retro from JPMC_ LIQUIDITY_11.4.1_BUGFIX JPMC to 11.5 ends
					busDate=calendar.getTime();
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("actual date before calculating nonworking day >>>>"+busDate);
					calcode=com.intellectdesign.cash.lms.execution.sweeps.util.ExecutionUtility.getCalCode(client);
					finalSettlementDate = ExecutionUtility.getAnotherWorkingDay(calcode, busDate, 
							'N', 0, 0, 0);
					
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("finalSettlementDate  after calculation >>>>"+finalSettlementDate);
					
					//accountInfoDo.setDAT_FINAL_SETTLEMENT(new java.sql.Date(calendar.getTime().getTime()));
					//accountInfoDo.setDAT_POST_DEBITDELAY(new java.sql.Date(calendar.getTime().getTime()));
					//accountInfoDo.setDAT_POST_CREDITDELAY(new java.sql.Date(calendar.getTime().getTime()));
					//accountInfoDo.setDAT_VALUE(new java.sql.Date(calendar.getTime().getTime()));
					accountInfoDo.setDAT_FINAL_SETTLEMENT(new java.sql.Date(finalSettlementDate.getTime()));
					accountInfoDo.setDAT_POST_DEBITDELAY(new java.sql.Date(finalSettlementDate.getTime()));
					accountInfoDo.setDAT_POST_CREDITDELAY(new java.sql.Date(finalSettlementDate.getTime()));
					accountInfoDo.setDAT_VALUE(new java.sql.Date(finalSettlementDate.getTime()));
				}
				//accountInfoDo.setDAT_POST_CREDITDELAY(businessDate);
				//SUP_389 ends

//ENH_IS997 ends
		
				//ENH_IS1167 STARTS				
     			// Using executionconstants.properties file to get ENDDATE_DELAY value and adding it to participant end date value	
				//accountInfoDo.setDAT_VALUE(businessDate);
				//SUP_ISS_V_2772 Start
				 /*accountInfoDo.setDAT_VALUE(new java.sql.Date(calendar.getTime().getTime()));
				 //ENH_IS1167 ENDS
				accountInfoDo.setDAT_ADJUSTEDACCRUAL(businessDate);*/
				//SUP_ISS_V_2772 ENDS
//ENH_IS997 starts
				//ENH_IS1297 RETROFIT STARTS
				
				//SUP_IS764 starts
				//add poolId+account+currency to accPoolIdList if today is participant end date
				//SUP_IS1192 Starts
				/*
				 * Added the participants that are ending into a List(global variable)
				 * so that they can be used for processing in the processUnpostedAmount
				 */
				
				//if(!accPoolIdList.contains(accountInfoDo)){
					//accPoolIdList.add(accountInfoDo);
					AccountInfoDO accountInfoDoEnd = new AccountInfoDO();
					accountInfoDoEnd.setID_POOL(accountInfoDo.getID_POOL());
					accountInfoDoEnd.setPARTICIPANT_ID(accountInfoDo.getPARTICIPANT_ID());
					accountInfoDoEnd.setCOD_CCY(accountInfoDo.getCOD_CCY());
					accountInfoDoEnd.setDAT_POSTING_START(accountInfoDo.getDAT_POSTING_START());
					accountInfoDoEnd.setDAT_ACCRUAL_START(accountInfoDo.getDAT_ACCRUAL_START());
					accountInfoDoEnd.setTYP_PARTICIPANT(accountInfoDo.getTYP_PARTICIPANT());
					accountInfoDoEnd.setAMT_ACCRUEDNOTPOSTED(accountInfoDo.getAMT_ACCRUEDNOTPOSTED());
					accountInfoDoEnd.setAMT_POSTED(accountInfoDo.getAMT_POSTED());
					accountInfoDoEnd.setDAT_POSTING(accountInfoDo.getDAT_POSTING());
					accountInfoDoEnd.setTYP_PARTICIPANT(accountInfoDo.getTYP_PARTICIPANT());
					accountInfoDoEnd.setCOD_TXN(accountInfoDo.getCOD_TXN());
					accountInfoDoEnd.setTXT_STATUS(accountInfoDo.getTXT_STATUS());
					accountInfoDoEnd.setSOURCE_POOL_ID(accountInfoDo.getSOURCE_POOL_ID()); //// NEW_ACCOUNTING_ENTRY_FORMAT 
					accountInfoDoEnd.setSimId(accountInfoDo.getSimId());//SIMULATION
					accPoolIdList.add(accountInfoDoEnd);
					if (logger.isDebugEnabled()){
						logger.debug("Account Info Object has been created : "+accountInfoDoEnd.toString()+"Size of list:::"+accPoolIdList.size() );				
					}
				//commented the below as we are adding the accountInfoDo object in the accPoolIdList
				//key=accountInfoDo.getID_POOL()+ExecutionConstants.SEPARATOR+ accountInfoDo.getPARTICIPANT_ID()+ExecutionConstants.SEPARATOR+ accountInfoDo.getCOD_CCY()+ExecutionConstants.SEPARATOR+ accountInfoDo.getCOD_TXN(); //SUP_IS1192
				//if(!accPoolIdList.contains(key))
				//accPoolIdList.add(key);
				//SUP_IS1192 Ends
				//ENH_IS1297 RETROFIT ENDS
				
				//do not apply carry forward logic just round up amtAccrued 
					/** ****** JTEST FIX start ************  on 14DEC09*/
//					precision_carryfwd = util.getTruncatedAmt(amtAccrued,ExecutionConstants.ROUNDOFF_ACCRUAL_POSTING,accountInfoDo.getCOD_CCY());
					precision_carryfwd = util.getTruncatedAmt(amtAccrued,ExecutionConstants.ROUNDOFF_ACCRUAL_POSTING,accountInfoDo.getCOD_CCY(), conn);
					/** ****** JTEST FIX end **************  on 14DEC09*/
				
				//set Truncated amount to amtAccured
				amtAccrued=precision_carryfwd[0];
				if (logger.isDebugEnabled())
				{
					logger.debug("amtAccrued with round up" + amtAccrued);
				
				}
				accountInfoDo.setAMT_ACCRUED(amtAccrued);
				//amountToBePosted = amtAccrued
				accountInfoDo.setAMT_TO_BE_POSTED(amtAccrued);
				//Resetting amtUnaccrued to 0 
				accountInfoDo.setAMT_UNACCRUED(ZERO);
				//Status = ACC_GEN
				accountInfoDo.setTXT_STATUS(ExecutionConstants.ACCRUAL_GENERATED_STATUS);
				//SUP_IS764 ends
//ENH_IS997 ends
			}
		}
		else
		{
			if (logger.isDebugEnabled()){
				logger.debug("Accrual Cycle NOT END ");
			}
			//amtUnaccrued=calculatedAmount+amtUnaccrued;
			amtUnaccrued = calculatedAmount.add(amtUnaccrued);
			
			//if Accrual Cycle starts then get unposted amt 
			accountInfoDo.setAMT_UNACCRUED(amtUnaccrued);
			//Resetting amtAccrued to 0 
			accountInfoDo.setAMT_ACCRUED(ZERO);
			//status = NO_ACC
			accountInfoDo.setTXT_STATUS(ExecutionConstants.NOACCRUAL_STATUS);

		}
		if (logger.isDebugEnabled()){
			logger.debug(" DATA OBJECT DETAILS " + accountInfoDo.toString());
		}
		
	
		//IF COUNTER ACCOUNT IS SETTLEMENT OR IF IT IS REALLOCATION THEN DONOT INSERT INTO ACCRUAL TABLE
		
		if("S".equalsIgnoreCase(accountInfoDo.getCOD_COUNTER_ACCOUNT()) || "R".equalsIgnoreCase(accountInfoDo.getFLG_ALLOCATION()) 
				|| "T".equalsIgnoreCase(accountInfoDo.getFLG_ALLOCATION()))
		{
			return;
		}
		if(cycleStart == false)
		{
			insertAccrualSummary(insertAccrualSummary,accountInfoDo);
			entries[1]=true;
		}
		else
		{
			updateAccrualSummary(updateAccrualSummary,accountInfoDo);
			entries[2]=true;
		}
		
		if (logger.isDebugEnabled()){
			logger.debug("Leaving ");
		}
		
	}
	
	
	/**
	 * This method set the use date for unposted amount 
	 * @param updateUnpostedAmt
	 * @param accountInfoDo
	 * @throws LMSException
	 */
	private void updateUnpostedAmtToAcctUnpostedTbl(PreparedStatement updateUnpostedAmt,
			AccountInfoDO accountInfoDo , long eventId) throws LMSException 
	{	
		if (logger.isDebugEnabled()){
			logger.debug("Entering");
		}
		//Rollback JTest changes starts
		//JTest Changes 11.5.1.Starts
		/*Connection conn = null;
		PreparedStatement pstmt=null;
		ResultSet lRset = null;*/
		//Rollback JTest changes ends
		//JTest Changes 11.5.1.Ends
		
		try
		{
			//Carry Forward Changes start
		if(ExecutionConstants.NEW_CARRY_FORWARD_LOGIC.equalsIgnoreCase("Y") && accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase("CENTRAL")
				&& eventId == ExecutionConstants.POOLING_EOD){
			setSqlProperties();
			//JTest Changes 11.5.1.Starts
			//Rollback JTest changes starts
			Connection conn = updateUnpostedAmt.getConnection();
			PreparedStatement pstmt=null;
			ResultSet lRset = null;
			//JTest Changes 11.5.1.Ends
			//conn = updateUnpostedAmt.getConnection();
			//Rollback JTest changes ends
			try{//Jtest 11.7 fix
				String strGetcentralAccounts = (String) interestCalculatorSql.get("getCentralaccts");
				pstmt = conn.prepareStatement(strGetcentralAccounts);
				pstmt.setString(1, "AMOUNT_CARRYFORWARD");
				pstmt.setString(2, accountInfoDo.getCOD_TXN());
				pstmt.setString(3, accountInfoDo.getID_POOL());
				lRset = pstmt.executeQuery();
				while(lRset.next()){
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("lRset Central" +lRset.getString(1));
					updateUnpostedAmt.setDate(1,LMSUtility.truncSqlDate(accountInfoDo.getDAT_CURRENT_BUSINESS()));
					
					//ENH_IS1731 Exec audit trailing changes Starts
					updateUnpostedAmt.setString(2,REF_DATE_FORMAT.format(new java.util.Date()));
					updateUnpostedAmt.setDate(3,LMSUtility.truncSqlDate(accountInfoDo.getDAT_CURRENT_BUSINESS()));
					updateUnpostedAmt.setString(4,lRset.getString(1));
					updateUnpostedAmt.setString(5,accountInfoDo.getID_POOL());
					updateUnpostedAmt.setString(6,accountInfoDo.getCOD_TXN());
					//ENH_IS1297 RETROFIT STARTS
					//Comment removed by MILIND
					updateUnpostedAmt.setString(7,ExecutionConstants.POSTING_TYPE);
					updateUnpostedAmt.setString(8, accountInfoDo.getSOURCE_POOL_ID()); // NEW_ACCOUNTING_ENTRY_FORMAT
					//ENH_IS1297 RETROFIT ENDS
					//ENH_IS1731 Exec audit trailing changes Ends
					
					updateUnpostedAmt.addBatch();
				}
			}
			//Jtest 11.7 fix start
			finally{
				
				//LMSConnectionUtility.close(conn);
				LMSConnectionUtility.close(pstmt);
				LMSConnectionUtility.close(lRset);
			}
			//Jtest 11.7 fix end
			// Carry Forward Changes End
		}		
		else
		{
		updateUnpostedAmt.setDate(1,LMSUtility.truncSqlDate(accountInfoDo.getDAT_CURRENT_BUSINESS()));
		
		//ENH_IS1731 Exec audit trailing changes Starts
//Retro_Mashreq
		//updateUnpostedAmt.setString(2,com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT.format(new java.util.Date()));
		updateUnpostedAmt.setString(2,REF_DATE_FORMAT.format(new java.util.Date()));
		updateUnpostedAmt.setDate(3,LMSUtility.truncSqlDate(accountInfoDo.getDAT_CURRENT_BUSINESS()));
		updateUnpostedAmt.setString(4,accountInfoDo.getPARTICIPANT_ID());
		updateUnpostedAmt.setString(5,accountInfoDo.getID_POOL());
		updateUnpostedAmt.setString(6,accountInfoDo.getCOD_TXN());
		//ENH_IS1297 RETROFIT STARTS
		//Comment removed by MILIND
		updateUnpostedAmt.setString(7,ExecutionConstants.POSTING_TYPE);
		updateUnpostedAmt.setString(8, accountInfoDo.getSOURCE_POOL_ID()); // NEW_ACCOUNTING_ENTRY_FORMAT
		if(accountInfoDo.getSimId()!=0)
			updateUnpostedAmt.setLong(9, accountInfoDo.getSimId());
		//ENH_IS1297 RETROFIT ENDS
		//ENH_IS1731 Exec audit trailing changes Ends
		updateUnpostedAmt.addBatch();
		}
			if (logger.isDebugEnabled()){
				logger.debug("updateUnpostedAmtToAcctUnpostedTbl " + accountInfoDo.getDAT_CURRENT_BUSINESS() );
				logger.debug("updateUnpostedAmtToAcctUnpostedTbl " + accountInfoDo.getPARTICIPANT_ID() );
				logger.debug("updateUnpostedAmtToAcctUnpostedTbl " + accountInfoDo.getID_POOL() );
				logger.debug("updateUnpostedAmtToAcctUnpostedTbl " + accountInfoDo.getCOD_TXN() );
				logger.debug("updateUnpostedAmtToAcctUnpostedTbl " + accountInfoDo.getSOURCE_POOL_ID() ); //NEW_ACCOUNTING_ENTRY_FORMAT
			}
		}
		catch(SQLException e)
		{
			logger.fatal("Exception :: " , e);
			//ENH_IS1059  Starts
			//throw new LMSException("Error occured in updateUnpostedAmtToAcctUnpostedTbl", e.getMessage());
			throw new LMSException("LMS01000","Updation of  unposted amount in OPOOL_ACCOUNT_UNPOSTED table failed for Transaction Id :- "+accountInfoDo.getCOD_TXN()+" and Pool Id :- "+accountInfoDo.getID_POOL(), e);
			//ENH_IS1059  Ends
		}
		//JTest Changes 11.5.1.Starts
		finally{
			//Rollback JTest changes starts
			/*LMSConnectionUtility.close(conn);
			LMSConnectionUtility.close(pstmt);
			LMSConnectionUtility.close(lRset);*/
			//Rollback JTest changes ends
			}
		//JTest Changes 11.5.1.Ends
		if (logger.isDebugEnabled()){
			logger.debug("Leaving");
			}
	}
	//Reallocate to Self starts
	/**
	 * This method set the use date for unposted amount 
	 * @param updateUnpostedAmt
	 * @param accountInfoDo
	 * @throws LMSException
	 */
	private void updateUnpostedAmtToAcctUnpostedTblCentral(PreparedStatement updateUnpostedAmt,
			AccountInfoDO accountInfoDo , long eventId) throws LMSException 
	{	
		if (logger.isDebugEnabled()){
			logger.debug("Entering");
		}
		try
		{
			
		updateUnpostedAmt.setDate(1,LMSUtility.truncSqlDate(accountInfoDo.getDAT_CURRENT_BUSINESS()));
		
		updateUnpostedAmt.setString(2,REF_DATE_FORMAT.format(new java.util.Date()));
		updateUnpostedAmt.setDate(3,LMSUtility.truncSqlDate(accountInfoDo.getDAT_CURRENT_BUSINESS()));
		updateUnpostedAmt.setString(4,accountInfoDo.getPARTICIPANT_ID());
		updateUnpostedAmt.setString(5,accountInfoDo.getID_POOL());
		updateUnpostedAmt.setString(6,accountInfoDo.getCOD_TXN());
		updateUnpostedAmt.setString(7,ExecutionConstants.POSTING_TYPE);
		updateUnpostedAmt.setString(8, accountInfoDo.getSOURCE_POOL_ID()); 
		if(accountInfoDo.getSimId()!=0)
			updateUnpostedAmt.setLong(9, accountInfoDo.getSimId());
		updateUnpostedAmt.addBatch();
		if (logger.isDebugEnabled()){
			logger.debug("updateUnpostedAmtToAcctUnpostedTbl " + accountInfoDo.getDAT_CURRENT_BUSINESS() );
			logger.debug("updateUnpostedAmtToAcctUnpostedTbl " + accountInfoDo.getPARTICIPANT_ID() );
			logger.debug("updateUnpostedAmtToAcctUnpostedTbl " + accountInfoDo.getID_POOL() );
			logger.debug("updateUnpostedAmtToAcctUnpostedTbl " + accountInfoDo.getCOD_TXN() );
			logger.debug("updateUnpostedAmtToAcctUnpostedTbl " + accountInfoDo.getSOURCE_POOL_ID() ); //NEW_ACCOUNTING_ENTRY_FORMAT
		}
		}
		catch(SQLException e)
		{
			logger.fatal("Exception :: " , e);
			//ENH_IS1059  Starts
			//throw new LMSException("Error occured in updateUnpostedAmtToAcctUnpostedTbl", e.getMessage());
			throw new LMSException("LMS01000","Updation of  unposted amount in OPOOL_ACCOUNT_UNPOSTED table failed for Transaction Id :- "+accountInfoDo.getCOD_TXN()+" and Pool Id :- "+accountInfoDo.getID_POOL(), e);
			//ENH_IS1059  Ends
		}
		if (logger.isDebugEnabled()){
			
			logger.debug("Leaving");
		}
	}
//Reallocate to Self ends
	/**
	 *  This method add the unposted amount to unposted amount table
	 * @param insertUnpostedAmt
	 * @param AmtCarryForward
	 * @param reason
	 * @param accountInfoDo
	 * @param type
	 * @throws LMSException
	 */
	public void addAmtToAcctUnpostedTbl(PreparedStatement insertUnpostedAmt, 
			BigDecimal AmtCarryForward, String reason,AccountInfoDO accountInfoDo,String type) throws LMSException 
	{
		if (logger.isDebugEnabled()){
			logger.debug("Entering");
		}
		
		try
		{
			insertUnpostedAmt.setString(1,accountInfoDo.getPARTICIPANT_ID());
			insertUnpostedAmt.setBigDecimal(2,AmtCarryForward);
			insertUnpostedAmt.setString(3,reason);
			insertUnpostedAmt.setString(4,NORMAL_ACCRUAL);
			if(type.equalsIgnoreCase("NORMAL"))
			{
				insertUnpostedAmt.setString(5,accountInfoDo.getCOD_TXN());
			}
			if(type.equalsIgnoreCase("BVT"))
			{
				//ENH_IS1297 RETROFIT STARTS
				//insertUnpostedAmt.setString(5,setTxnCode(accountInfoDo.getCOD_TXN(),"BVT"));
				insertUnpostedAmt.setString(5,(accountInfoDo.getCOD_TXN())); ////SUP_QF07
				//ENH_IS1297 RETROFIT ENDS
			}
			insertUnpostedAmt.setDate(6,LMSUtility.truncSqlDate(accountInfoDo.getDAT_CURRENT_BUSINESS()));
			insertUnpostedAmt.setString(7,accountInfoDo.getID_POOL());
			
			//ENH_IS1731 Exec audit trailing changes Starts
//Retro_Mashreq
			//insertUnpostedAmt.setString(8,com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT.format(new java.util.Date()));
			insertUnpostedAmt.setString(8,REF_DATE_FORMAT.format(new java.util.Date()));
			insertUnpostedAmt.setDate(9,LMSUtility.truncSqlDate(accountInfoDo.getDAT_CURRENT_BUSINESS()));
			//ENH_IS1731 Exec audit trailing changes Ends
			// NEW_ACCOUNTING_ENTRY_FORMAT 
			if (logger.isDebugEnabled()){
				logger.debug("accountInfoDo.getSOURCE_POOL_ID()addAmtToAcctUnpostedTbl  is " + accountInfoDo.getSOURCE_POOL_ID());
				logger.debug("accountInfoDo.getID_POOL() addAmtToAcctUnpostedTbl is " + accountInfoDo.getID_POOL());
				logger.debug("FlG_NEW_ACCOUNTING_ENTRY_FORMAT  addAmtToAcctUnpostedTbl is " + ExecutionConstants.FlG_NEW_ACCOUNTING_ENTRY_FORMAT);
				logger.debug("reason is " + reason);
				logger.debug("type is  " + type);
			}
			if(ExecutionConstants.FlG_NEW_ACCOUNTING_ENTRY_FORMAT)
			{
				insertUnpostedAmt.setString(10, accountInfoDo.getSOURCE_POOL_ID());
			}else
			{
				insertUnpostedAmt.setString(10, accountInfoDo.getID_POOL());
			}
			// NEW_ACCOUNTING_ENTRY_FORMAT  end
			if(accountInfoDo.getSimId() != 0)
				insertUnpostedAmt.setLong(11, accountInfoDo.getSimId());
			//ENH_IS1731 Exec audit trailing changes Ends
			insertUnpostedAmt.addBatch();
		}
		catch(SQLException e)
		{
			logger.fatal("Exception :: " , e);
			//ENH_IS1059  Starts
			//throw new LMSException("Error occured in addAmtToAcctUnpostedTbl", e.getMessage());
			throw new LMSException("LMS01000","Insertion of  unposted amount in OPOOL_ACCOUNT_UNPOSTED table failed for Transaction Id :- "+accountInfoDo.getCOD_TXN()+" and Pool Id:- "+accountInfoDo.getID_POOL()+".", e);
			//ENH_IS1059  Ends
		}
		if (logger.isDebugEnabled()){
			logger.debug("Leaving");
		}
	}

	/**
	 * This method deals with posting summary table. Method identifies posting cycle start or end and does the update or insert accordingly.   
	 * @param lotId
	 * @param businessDate
	 * @param client
	 * @param insertPostingSummary
	 * @param updatePostingSummary
	 * @param accountInfoDo
	 * @param selectPostingSummary
	 * @param postingInfoMap 
	 * @throws Exception
	 */
//SIT2DEV Retro -DD start
//	SUP_IS1322 - one more paramenter pooltype has been passed
	public void populatePostingSummary(String lotId, java.sql.Date businessDate,
			String client,HashMap pooldetails,PreparedStatement insertPostingSummary, 
//SIT2DEV Retro -DD end
			PreparedStatement updatePostingSummary, AccountInfoDO accountInfoDo,
			PreparedStatement selectPostingSummary, HashMap<String, PostingInfoDO> postingInfoMap) throws Exception // LMS_45_PERF_CERTIFICATION_PF24
	{
		if (logger.isDebugEnabled()){
			logger.debug("Entering ");
		}
		BigDecimal amtAccuredNotPosted =ZERO;
		BigDecimal amountToBePosted = ZERO;
		//Reallocate to Self
		BigDecimal amountToBePostedCentralCcy = ZERO;
		BigDecimal postedAmount=ZERO, amtPosted = ZERO, postedAmountCentralCcy=ZERO; //Sampada
		//Reallocate to Self
		Date incDateUtil = null;
		GregorianCalendar gDate = new GregorianCalendar();
		java.sql.Date incDateSql = null;
		boolean cycleStart = true;
		boolean isCapitalisationDate = false;
		boolean flgPostingToBeDone = true;
		//Identify the Posting cycle start 
		cycleStart  = checkRecordInPostingSummary(accountInfoDo,postingInfoMap);//LMS_45_PERF_CERTIFICATION_PF24
		amtAccuredNotPosted = accountInfoDo.getAMT_ACCRUEDNOTPOSTED(); 
		amtPosted = accountInfoDo.getAMT_POSTED(); //Sampada
		amountToBePosted=accountInfoDo.getAMT_TO_BE_POSTED();
		//Reallocate to Self
		//if (!(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCO) && accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN) )  ){ // LS114_21326_21336
		//RBS_Internal_fix  starts INCO is not required
		if (!( accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN) )  ){ // LS114_21326_21336
			//RBS_Internal_fix  ends INCO is not required

			amountToBePostedCentralCcy = accountInfoDo.getAMT_CAL_CENTRAL_CCY();
			postedAmountCentralCcy = accountInfoDo.getAMT_POSTED_CENTRAL_CCY();
		}// LS114_21326_21336
		//Reallocate to Self
		//Identify Accrual Cycle End
		if (logger.isDebugEnabled()){
			logger.debug("businessDate " + businessDate.toString());
			logger.debug("DAT_POSTING " + accountInfoDo.getDAT_POSTING().toString());
			logger.debug("cycleStart : "+cycleStart);
			logger.debug("accountInfoDo.getTXT_STATUS() : "+accountInfoDo.getTXT_STATUS());
			
		}	
		if(cycleStart == false && ExecutionConstants.NOT_APPL.equalsIgnoreCase(accountInfoDo.getTXT_STATUS())) {
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("Inside if condition : not existing and not applicale "+cycleStart); //sampada
			return;
		}
		//NT_UT_BUFIX_18_Feb_09 Start //ENH_IS1913 starts
		if (!(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)))
		{
			if(!(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCO)))
			{
			boolean holdreq=chkPendingHoldReq(accountInfoDo);
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("holdreq -- "+holdreq);
			if(holdreq)
			{
			logger.debug("Hold req. is present in populate posting summary");
			accountInfoDo.setTXT_STATUS("HOLD");
			}
			}
		}
		//NT_UT_BUFIX_18_Feb_09 End //ENH_IS1913 ends

		//Posting Check for Inter Company : On Loan End Date i.e. on Maturity 
		//interest engine should skip marking posting entry status to 'POSTING_GEN' 
		//as the same would be done during Maturity processing for Inter Company Interest
		//Instead the calculated interest should be added to accrued not posted with 'NO_POSTING' status
		// FindBug issue fix starts
		//flgPostingToBeDone = checkPostingToBeDone(businessDate, accountInfoDo);//Harikesh	
		// FindBug issue fix ends

//SIT2DEV Retro -DD start

		//if((businessDate.toString().equalsIgnoreCase(accountInfoDo.getDAT_POSTING().toString()) || businessDate.toString().equalsIgnoreCase(accountInfoDo.getPARTY_DAT_END().toString()) ||(pooldetails!=null && (pooldetails.get("ID_POOL").toString().equalsIgnoreCase(accountInfoDo.getID_POOL().toString()) && pooldetails.get("VALID").toString().equalsIgnoreCase("true")))) && flgPostingToBeDone)
//SIT2DEV Retro -DD end
//SUP_IS2925 starts
		if(businessDate.toString().equalsIgnoreCase(accountInfoDo.getDAT_POSTING().toString())
				|| businessDate.toString().equalsIgnoreCase(accountInfoDo.getPARTY_DAT_END().toString())
//SUP_IS2925 starts
				||(pooldetails != null && pooldetails.containsKey(accountInfoDo.getID_POOL())&& ((String)pooldetails.get(accountInfoDo.getID_POOL())).equalsIgnoreCase("true")))
//SUP_IS2925 ends
		{
			isCapitalisationDate = true;
			if (logger.isDebugEnabled()){
				logger.debug("POSTING Cycle END ");
			}
			
			if (logger.isDebugEnabled())
			{
				
				logger.debug("BEFORE amtAccuredNotPosted : "+amtAccuredNotPosted);
				logger.debug("amountToBePosted :  "+amountToBePosted);
				logger.debug("amtPosted : "+amtPosted); //sampada
				//Reallocate to Self
				logger.debug("amountToBePostedCentralCcy : "+amountToBePostedCentralCcy); //sampada
				logger.debug("postedAmountCentralCcy : "+postedAmountCentralCcy); //sampada	
				//Reallocate to Self
				
			}
			//Set AMT_POSTED
			//postedAmount=amtAccuredNotPosted  + amountToBePosted
			//Sampada Starts
			if(amtAccuredNotPosted.compareTo(ZERO) != 0){
				postedAmount=amtAccuredNotPosted.add(amountToBePosted);
			}else{
			 	postedAmount = amtPosted.add(amountToBePosted); 
			}
			//Sampada Ends
			//Reallocate to Self
			//RBS_Internal_fix  starts INCO is not required
			//if (!(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCO) && accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN) )  ){ // LS114_21326_21336
			if (!( accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN) )  ){ // LS114_21326_21336
			//RBS_Internal_fix  ends INCO is not required
				postedAmountCentralCcy = amountToBePostedCentralCcy.add(postedAmountCentralCcy);
			}// LS114_21326_21336
			if (logger.isDebugEnabled())
			{
				logger.debug("postedAmount: "+postedAmount);
				logger.debug("postedAmountCentralCcy: "+postedAmountCentralCcy);
							
			}
			accountInfoDo.setAMT_POSTED_CENTRAL_CCY(postedAmountCentralCcy);
			//Reallocate to Self
			accountInfoDo.setAMT_POSTED(postedAmount);
			//Resetting AMT_ACCRUEDNOTPOSTED to 0
			accountInfoDo.setAMT_ACCRUEDNOTPOSTED(ZERO);
			//status = POST_GEN
			accountInfoDo.setTXT_STATUS(ExecutionConstants.POSTING_GENERATED_STATUS);
			
			
			//ENH_IS1297 - INTEREST POSTING CYCLE Starts
			if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
				logger.debug("accountInfoDo.getAMT_POSTED Rohit: "+accountInfoDo.getAMT_POSTED());
				logger.debug("accountInfoDo.getAMT_ACCRUEDNOTPOSTED Rohit: "+accountInfoDo.getAMT_ACCRUEDNOTPOSTED());
				logger.debug("accountInfoDo.getTXT_STATUS Rohit: "+accountInfoDo.getTXT_STATUS());
				logger.debug("Business Date Rohit: "+businessDate);
				logger.debug("accountInfoDo.getPARTY_DAT_END() Rohit: "+accountInfoDo.getPARTY_DAT_END());
				logger.debug("accountInfoDo.getFLG_POSTCYCLE() Rohit: "+accountInfoDo.getFLG_POSTCYCLE());
			}
			//logger.debug("pooldetails " + pooldetails.toString());//nullpointer issue fixed

			//ENH_IS1297 ... Changes for pooling Execution
			//if(businessDate.toString().equalsIgnoreCase(accountInfoDo.getPARTY_DAT_END().toString()) && accountInfoDo.getFLG_POSTCYCLE().equalsIgnoreCase(ExecutionConstants.NO))
//SIT2DEV Retro -DD start
//SUP_IS2925 starts

			//if(businessDate.toString().equalsIgnoreCase(accountInfoDo.getPARTY_DAT_END().toString())|| (pooldetails!= null && (pooldetails.get("ID_POOL").toString().equalsIgnoreCase(accountInfoDo.getID_POOL().toString()) && pooldetails.get("VALID").toString().equalsIgnoreCase("true"))))
			if(businessDate.toString().equalsIgnoreCase(accountInfoDo.getPARTY_DAT_END().toString()) || (pooldetails != null && pooldetails.containsKey(accountInfoDo.getID_POOL())&& ((String)pooldetails.get(accountInfoDo.getID_POOL())).equalsIgnoreCase("true")))
//SUP_IS2925 ends
//SIT2DEV Retro -DD end
			{
				//Intercompany Changes Start
				//if (accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)) {
				// FLG_POSTCYCLE
				//if ((accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT))||(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN))) {
				//if ((accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT))||(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN)) || (pooldetails != null && pooldetails.containsKey(accountInfoDo.getID_POOL())&& ((String)pooldetails.get(accountInfoDo.getID_POOL())).equalsIgnoreCase("true")) ) {//JPMC_IS FIXED
                //Intercompany Changes End
					if (accountInfoDo.getFLG_POSTCYCLE().equalsIgnoreCase(ExecutionConstants.NO)) {
						accountInfoDo.setDAT_POSTING(businessDate);
					//SUP_IS1845 start //ENH_IS1913 starts
						if(accountInfoDo.getFLG_SAMEVALDAT().equalsIgnoreCase("Y"))
						{
							accountInfoDo.setDAT_VALUE(accountInfoDo.getDAT_FINAL_SETTLEMENT());
						}
						else
						{
							logger.debug("flag same value date is N and flag posting cycle is Y");		
							gDate.setTime(accountInfoDo.getDAT_POSTING());
							gDate.add(Calendar.DATE,1);
							incDateUtil =  gDate.getTime();
							incDateSql = new java.sql.Date(incDateUtil.getTime());
							accountInfoDo.setDAT_VALUE(incDateSql);
							if (logger.isDebugEnabled())
							{
								
								logger.debug("INCREMENTED DATE " + incDateSql);
								
							}
						}
					//SUP_IS1845 End  //ENH_IS1913 ends
					}
					else{
						if(accountInfoDo.getFLG_SAMEVALDAT().equalsIgnoreCase("Y"))
						{
							accountInfoDo.setDAT_VALUE(accountInfoDo.getDAT_FINAL_SETTLEMENT());
						}
						else
						{
							logger.debug("flag same value date is N and flag posting cycle is Y");		
							gDate.setTime(accountInfoDo.getDAT_POSTING());
							gDate.add(Calendar.DATE,1);
							incDateUtil =  gDate.getTime();
							incDateSql = new java.sql.Date(incDateUtil.getTime());
							accountInfoDo.setDAT_VALUE(incDateSql);
							if (logger.isDebugEnabled())
							{
								
								logger.debug("INCREMENTED DATE " + incDateSql);
								
							}
						}
					}
					//JPMC_IS FIXED STARTS
				/*}else {
					accountInfoDo.setDAT_POSTING(businessDate);
					accountInfoDo.setDAT_FINAL_SETTLEMENT(businessDate); //NT_UT_BUFIX_09_Jan_09 - settelment should be done for end party in pool 
				}*/
					//JPMC_IS FIXED ENDS
				logger.fatal("PARTY END ");
			}
			
			//if(businessDate.toString().equalsIgnoreCase(accountInfoDo.getPARTY_DAT_END().toString()) && accountInfoDo.getFLG_POSTCYCLE().equalsIgnoreCase(ExecutionConstants.YES))
            //Intercompany Changes Start
			//if(businessDate.toString().equalsIgnoreCase(accountInfoDo.getPARTY_DAT_END().toString()) && accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT) && accountInfoDo.getFLG_POSTCYCLE().equalsIgnoreCase(ExecutionConstants.YES))
			// FLG_POSTCYCLE
			//if(businessDate.toString().equalsIgnoreCase(accountInfoDo.getPARTY_DAT_END().toString()) && (accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)||(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN)) || (pooldetails != null && pooldetails.containsKey(accountInfoDo.getID_POOL())&& ((String)pooldetails.get(accountInfoDo.getID_POOL())).equalsIgnoreCase("true"))) && accountInfoDo.getFLG_POSTCYCLE().equalsIgnoreCase(ExecutionConstants.YES))
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("accountInfoDo.getFLG_POSTCYCLE() >>>> "+accountInfoDo.getFLG_POSTCYCLE());
			//if((businessDate.toString().equalsIgnoreCase(accountInfoDo.getPARTY_DAT_END().toString()) && accountInfoDo.getFLG_POSTCYCLE().equalsIgnoreCase(ExecutionConstants.YES)) || (businessDate.toString().equalsIgnoreCase(accountInfoDo.getPARTY_DAT_END().toString()) && accountInfoDo.getFLG_POSTCYCLE().equalsIgnoreCase(ExecutionConstants.NO) && (accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN) || accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCO))))//JPMC_IS FIXED //BOI 616 fix
			if(businessDate.toString().equalsIgnoreCase(accountInfoDo.getPARTY_DAT_END().toString()) && accountInfoDo.getFLG_POSTCYCLE().equalsIgnoreCase(ExecutionConstants.YES) )//JPMC_IS FIXED //BOI 616 fix
			//Intercompany Changes End
			//ENH_IS1297 Ends

			{
				accountInfoDo.setAMT_POSTED(ZERO);
				accountInfoDo.setAMT_ACCRUEDNOTPOSTED(postedAmount);
				accountInfoDo.setTXT_STATUS(ExecutionConstants.NOPOSTING_STATUS);
				logger.debug("NOPOSTING status when flg post cycle Y>>>> ");
			}
			//ENH_IS1297 - INTEREST POSTING CYCLE Ends
			//added by BOI issue 145
			//Resultset rs1=null;
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("RPI case>>>> "+accountInfoDo.getTYP_REPAYMENT()+" PARTY_DAT_END>> "+accountInfoDo.getPARTY_DAT_END());
			if(businessDate.toString().equalsIgnoreCase(accountInfoDo.getPARTY_DAT_END().toString()) && (accountInfoDo.getTYP_REPAYMENT().equalsIgnoreCase("RPI") || accountInfoDo.getTYP_REPAYMENT().equalsIgnoreCase("PPI") ||  accountInfoDo.getTYP_REPAYMENT().equalsIgnoreCase("RP"))){ // RBS_201
				accountInfoDo.setAMT_POSTED(ZERO);
				accountInfoDo.setAMT_ACCRUEDNOTPOSTED(postedAmount);
				accountInfoDo.setTXT_STATUS(ExecutionConstants.NOPOSTING_STATUS);
				logger.debug("NOPOSTING status when flg post cycle N>>>> RPI/PPI/RP case ");
				
			}
			//end by BOI issue 145

		}
		else
		{
			if (logger.isDebugEnabled())
			{
				logger.debug("POSTING Cycle NOT END ");
				logger.debug("BEFORE amtAccuredNotPosted : "+amtAccuredNotPosted);
				logger.debug("amountToBePosted :  "+amountToBePosted);
				//Reallocate to Self
				logger.debug("amountToBePostedCentralCcy : "+amountToBePostedCentralCcy); //sampada
				logger.debug("postedAmountCentralCcy : "+postedAmountCentralCcy); //sampada	
				//Reallocate to Self
				
			}
			//amtAccuredNotPosted = amtAccuredNotPosted  + amountToBePosted;
			amtAccuredNotPosted = amtAccuredNotPosted.add(amountToBePosted);
			//Reallocate to Self
		//	RBS_Internal_fix INCO is not required
			if (!accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCOLOAN)){ // LS114_21326_21336
		//	RBS_Internal_fix ends INCO is not required
				postedAmountCentralCcy = amountToBePostedCentralCcy.add(postedAmountCentralCcy);
			} // LS114_21326_21336
			if (logger.isDebugEnabled())
			{
				logger.debug("AFETER amtAccuredNotPosted : "+amtAccuredNotPosted);
				logger.debug("AFETER postedAmountCentralCcy : "+postedAmountCentralCcy);
			}
			
			accountInfoDo.setAMT_POSTED_CENTRAL_CCY(postedAmountCentralCcy);
			//Reallocate to Self
			accountInfoDo.setAMT_ACCRUEDNOTPOSTED(amtAccuredNotPosted);
			//Resetting AMT_POSTED to 0 
			accountInfoDo.setAMT_POSTED(ZERO);
			//status = NO_ACC
			accountInfoDo.setTXT_STATUS(ExecutionConstants.NOPOSTING_STATUS);

		}
		
		if (logger.isDebugEnabled()){
			logger.debug(" DATA OBJECT DETAILS " + accountInfoDo.toString());
		}
		
		if(cycleStart==false)
		{
//			NT_UT_BUFIX_18_Feb_09 Start //ENH_IS1913 starts
			if (!(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)))
			{
			logger.debug("calling function chkHoldReq() ");
			if(!(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCO)))
			{
			boolean holdreq=chkPendingHoldReq(accountInfoDo);
			if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
				logger.debug("holdreq -- "+holdreq);
			}
				if(holdreq)
				{
				logger.debug("Hold req. is present in updatePostingSummary");
				accountInfoDo.setTXT_STATUS("HOLD");
				if(isCapitalisationDate){
					accountInfoDo.setAMT_POSTED(ZERO);
					//Resetting AMT_ACCRUEDNOTPOSTED to 0
					accountInfoDo.setAMT_ACCRUEDNOTPOSTED(postedAmount);
				} 
				}
			}
			}
			//NT_UT_BUFIX_18_Feb_09 End  //ENH_IS1913 starts
			
			String ltxt_status = chkHold(accountInfoDo,accountInfoDo.getDAT_LASTPOSTING());
			if("HOLD".equals(ltxt_status)){
				accountInfoDo.setTXT_STATUS("HOLD");
			}
			//accountInfoDo.setAMT_ACCRUEDNOTPOSTED(0D);
			insertPostingSummary(insertPostingSummary,accountInfoDo);
			entries[3]=true;
		}
		else
		{
			if (logger.isDebugEnabled()){
				logger.debug(" In else block " + accountInfoDo.toString());
				logger.debug(" In else block accountInfoDo.getDAT_POSTING() ->" + accountInfoDo.getDAT_POSTING());
			}
//			NT_UT_BUFIX_18_Feb_09 Start //ENH_IS1913 starts
			if (!(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)))
			{
			logger.debug("calling function chkHoldReq() ");
			if(!(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCO)))
			{
			boolean holdreq=chkPendingHoldReq(accountInfoDo);
			if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
				logger.debug("holdreq -- "+holdreq);
			}
				if(holdreq)
				{
				logger.debug("Hold req. is present in updatePostingSummary");
				accountInfoDo.setTXT_STATUS("HOLD");
				if(isCapitalisationDate){
					accountInfoDo.setAMT_POSTED(ZERO);
					//Resetting AMT_ACCRUEDNOTPOSTED to 0
					accountInfoDo.setAMT_ACCRUEDNOTPOSTED(postedAmount);
				}
				
				}
			}
			}
			//NT_UT_BUFIX_18_Feb_09 End //ENH_IS1913 ends
			
			
			String ltxt_status = chkHold(accountInfoDo,accountInfoDo.getDAT_POSTING());
			if("HOLD".equals(ltxt_status)){
				if(isCapitalisationDate){
					accountInfoDo.setAMT_POSTED(ZERO);
					//Resetting AMT_ACCRUEDNOTPOSTED to 0
					accountInfoDo.setAMT_ACCRUEDNOTPOSTED(postedAmount);
				}
				accountInfoDo.setTXT_STATUS("HOLD");
			}
			updatePostingSummary(updatePostingSummary,accountInfoDo);
			entries[4]=true;
		}
		if (logger.isDebugEnabled()){
			logger.debug("Leaving ");
		}
	}
	
	//NT_UT_BUFIX_18_Feb_09 Start //ENH_IS1913 starts
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
	//Intellect Liquidity DB Failover Solution starts
	//private boolean chkPendingHoldReq(AccountInfoDO accountInfoDo){
	private boolean chkPendingHoldReq(AccountInfoDO accountInfoDo) throws LMSException{
	//Intellect Liquidity DB Failover Solution ends
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		logger.debug("Entering chkHold function");
		String typ_hold = "";
		String cod_gl = "";
		String cod_pool = "";
		String cod_acc = "";
		String id_pool = "";
		Connection lconn = null;
		PreparedStatement lPstmt1 = null;
		PreparedStatement lPstmt2 = null;
		ResultSet lRst1 = null;
		ResultSet lRst2 = null;
		boolean holdreq =false;
		try {
			lconn = LMSConnectionUtility.getConnection();
			String chkholdreq = "SELECT TYP_HOLD ,COD_GL,COD_POOL,COD_ACC FROM OPOOL_HOLD_UNHOLD WHERE DAT_HOLD = ? ";
			lPstmt1 = lconn.prepareStatement(chkholdreq);
			if (logger.isDebugEnabled()){
				logger.debug("PARAMETER 1 > "+accountInfoDo.getID_POOL());
				logger.debug("PARAMETER 2 > "+accountInfoDo.getPARTICIPANT_ID());
				logger.debug("PARAMETER 3 > "+accountInfoDo.getDAT_BUSINESS());
				logger.debug("Query to be executed > "+chkholdreq);
			}
			lPstmt1.setDate(1,LMSUtility.truncSqlDate(accountInfoDo.getDAT_BUSINESS()));
			lRst1 = lPstmt1.executeQuery();
			logger.debug("after the executed");
			
			if(lRst1!=null && lRst1.next()){ 
				logger.debug("Inside result --"); 
				typ_hold = lRst1.getString("TYP_HOLD"); 
				cod_gl= lRst1.getString("COD_GL");
				cod_pool= lRst1.getString("COD_POOL");
				/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//				cod_acc= lRst1.getString("COD_ACC");
				/** ****** FindBugs Fix End ***********  on 14DEC09*/
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("typ_hold value is"+typ_hold);
				if(cod_pool!=null && !cod_pool.equalsIgnoreCase("")){ 	//Find_Bug fix
				String chkrootpool = "SELECT ID_ROOTPOOL FROM OPOOL_POOLHDR WHERE ID_POOL = ? ";
				lPstmt2 = lconn.prepareStatement(chkrootpool);
				lPstmt2.setString(1,accountInfoDo.getID_POOL());
				lRst2 = lPstmt2.executeQuery();
				logger.debug("after the executed");
				
				if(lRst2!=null && lRst2.next()){ 
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("Inside result cod_pool -->"+cod_pool); 
					id_pool = lRst2.getString("ID_ROOTPOOL");
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("ID_POOL --"+id_pool);  
					if(cod_pool.equalsIgnoreCase(id_pool) || cod_pool.equalsIgnoreCase(accountInfoDo.getID_POOL())){
						
						/*EJBHomeFactory factory = EJBHomeFactory.getFactory();
						HoldUnholdProcessorSBSLHome slsbHome = (HoldUnholdProcessorSBSLHome)factory.lookupHome(HoldUnholdProcessorSBSLHome.class);
						HoldUnholdProcessorSBSL slsb = slsbHome.create();*/
						
						HoldUnholdProcessorSBSL slsb = (HoldUnholdProcessorSBSL) Ejb3Locator.doLookup("HoldUnholdProcessorSBSLBean", HoldUnholdProcessorSBSL.class);
						logger.debug("Before Calling the executeHold"); 
						slsb.executeHold(typ_hold,cod_gl); 
						holdreq=true;
					}
					else
					{
						holdreq=false;
					}
				}
				}
				else
				{
					holdreq=false;
				}
			}
			else
			{
				holdreq=false;
			}
			
			logger.debug("Hold request status -> "+holdreq); 
			
	
		} catch (NamingException e) {
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
			//Intellect Liquidity DB Failover Solution starts
			  logger.fatal("Naming exception has occured", e);
			  throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE," Naming exception has occured ", e);
			  //Intellect Liquidity DB Failover Solution ends
		} catch (SQLException e) {
			//Intellect Liquidity DB Failover Solution starts
			  logger.fatal("SQLException has occured", e);
			  if(SQLErrorCodesLoader.isConnectionError(e)){
			 	  throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE," SQLException has occured ", e);
			  }
			  //Intellect Liquidity DB Failover Solution ends
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		}
		 catch (Exception e) {
				logger.fatal("Exception occured:",e);
		}
		finally{
			LMSConnectionUtility.close(lRst1);
			LMSConnectionUtility.close(lPstmt1);
			LMSConnectionUtility.close(lRst2);
			LMSConnectionUtility.close(lPstmt2);
			LMSConnectionUtility.close(lconn);
		}
		
		logger.debug("Leaving chkHoldReq function");
		return holdreq;
		
	}
	
	
	//NT_UT_BUFIX_18_Feb_09 End //ENH_IS1913 ends
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
	//Intellect Liquidity DB Failover Solution starts
	private String chkHold(AccountInfoDO accountInfoDo,java.sql.Date date) throws LMSException{
	//Intellect Liquidity DB Failover Solution ends
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		logger.debug("Entering chkHold function");
		String ltxt_Status = "";
		Connection lconn = null;
		PreparedStatement lPstmt = null;
		ResultSet lRst = null;
		try {
			lconn = LMSConnectionUtility.getConnection();
			String chkquery = "SELECT TXT_STATUS FROM OPOOL_POSTINGSUMMARY WHERE ID_POOL = ? AND NBR_OWNACCOUNT = ? AND COD_TXN =? AND DAT_POSTING = ? AND  TXT_STATUS='HOLD'";
			if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
				logger.debug("Query-->"+chkquery);
				logger.debug("Values"+accountInfoDo.toString());
				logger.debug("Date:"+date);
			}
			lPstmt = lconn.prepareStatement(chkquery);
			
			lPstmt.setString(1,accountInfoDo.getID_POOL());
			lPstmt.setString(2,accountInfoDo.getPARTICIPANT_ID());
			lPstmt.setString(3,accountInfoDo.getCOD_TXN());
			lPstmt.setDate(4,LMSUtility.truncSqlDate(date));
			
			lRst = lPstmt.executeQuery();
			if(lRst.next()){
				ltxt_Status = lRst.getString("TXT_STATUS");
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("TXT_STATUS value is"+ltxt_Status);
			}
		} catch (NamingException e) {
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
			//Intellect Liquidity DB Failover Solution starts
			  logger.fatal("Naming exception has occured", e);
			  throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE," Naming exception has occured ", e);
			  //Intellect Liquidity DB Failover Solution ends
		} catch (SQLException e) {
			//Intellect Liquidity DB Failover Solution starts
			  logger.fatal("SQLException has occured", e);
			  if(SQLErrorCodesLoader.isConnectionError(e)){
		 	  		throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE," SQLException has occured ", e);
		  }
			  //Intellect Liquidity DB Failover Solution ends
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		}//ENH_IS1913 starts
		 catch (Exception e) {
				logger.fatal("Exception occured:",e);
		}//ENH_IS1913 ends
		finally{
			LMSConnectionUtility.close(lRst);
			LMSConnectionUtility.close(lPstmt);
			LMSConnectionUtility.close(lconn);
		}
		
		logger.debug("Leaving chkHold function");
		return ltxt_Status;
	}

	/**
	 * This method deals with account detailtable. For every txn there is an entry in this table
	 * @param businessDate
	 * @param client
	 * @param insertAccountDtls
	 * @param updateAccountDtls
	 * @param accountInfoDo
	 * @param selectAccountDtls
	 * @param accountDtlsInfoMap 
	 * @throws LMSException
	 */
//SIT2DEV Retro -DD start
	public boolean [] populateAccountDtls(Date businessDate, String client,HashMap pooldetails , 
//SIT2DEV Retro -DD end
			PreparedStatement insertAccountDtls,PreparedStatement updateAccountDtls,
			AccountInfoDO accountInfoDo,PreparedStatement selectAccountDtls, HashMap<String, AccountDtlsInfoDO> accountDtlsInfoMap ) throws LMSException //LMS_45_PERF_CERTIFICATION_PF24
	{
		if (logger.isDebugEnabled()){
			logger.debug("Entering ");
		}
		
		boolean recordExists = false;
		recordExists = checkRecordInAccountDtls(accountInfoDo,accountDtlsInfoMap);//LMS_45_PERF_CERTIFICATION_PF24
		if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
			logger.debug("recordExists : "+recordExists);
			logger.debug("accountInfoDo.getTXT_STATUS() : "+accountInfoDo.getTXT_STATUS());
		}
		if(recordExists == false && ExecutionConstants.NOT_APPL.equalsIgnoreCase(accountInfoDo.getTXT_STATUS())) {
			logger.debug("Inside if condition : not existing and not applicale "); //sampada
			return entries;
		}
		if(recordExists == true)
		{
			if (logger.isDebugEnabled()){
				logger.debug(" DATA OBJECT DETAILS updateAccountDtls  " + accountInfoDo.toString());
			}
			//updateAccountDtls();
			entries[7]=true;
			updateAccountDtls(updateAccountDtls,accountInfoDo);
			
		}
		else
		{
			entries[0]=true;
			if (logger.isDebugEnabled()){
				logger.debug(" DATA OBJECT DETAILS insertAccountDetails " + accountInfoDo.toString());
			}
			insertAccountDetails(insertAccountDtls,accountInfoDo);
		}
		
		if (logger.isDebugEnabled()){
			logger.debug("Leaving ");
		}
		return entries;
		
	}
	/**This method set the parameter into updatePostingSummary statement
	 * @param updatePostingSummary
	 * @param accountInfoDo
	 * @throws LMSException
	 */
	private void updatePostingSummary(PreparedStatement updatePostingSummary, AccountInfoDO accountInfoDo) throws LMSException {
		if (logger.isDebugEnabled()){
			logger.debug("Entering ");
		}
		try
		{
			//NT_UT_BUFIX_18_Feb_09 Start //ENH_IS1913 starts
			if (!(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)))
			{
			logger.debug("calling function chkHoldReq() ");
			if(!(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCO)))
			{
			boolean holdreq=chkPendingHoldReq(accountInfoDo);
			if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
				logger.debug("holdreq -- "+holdreq);
			}
				if(holdreq)
				{
				logger.debug("Hold req. is present in updatePostingSummary");
				accountInfoDo.setTXT_STATUS("HOLD");
				}
			}
			}
			//NT_UT_BUFIX_18_Feb_09 End  //ENH_IS1913 ends
			updatePostingSummary.setBigDecimal(1,accountInfoDo.getAMT_ACCRUEDNOTPOSTED());
			updatePostingSummary.setBigDecimal(2,accountInfoDo.getAMT_POSTED());
			updatePostingSummary.setString(3,accountInfoDo.getTXT_STATUS());
			updatePostingSummary.setDate(4,LMSUtility.truncSqlDate(accountInfoDo.getDAT_VALUE()));
			updatePostingSummary.setDate(5,LMSUtility.truncSqlDate(accountInfoDo.getDAT_FINAL_SETTLEMENT()));
			//ENH_IS1731 Exec audit trailing changes Starts
			updatePostingSummary.setDate(6,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING()));
//Retro_Mashreq
			//updatePostingSummary.setString(7,com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT.format(new java.util.Date()));
			updatePostingSummary.setString(7,REF_DATE_FORMAT.format(new java.util.Date()));
			updatePostingSummary.setDate(8,LMSUtility.truncSqlDate(accountInfoDo.getDAT_CURRENT_BUSINESS()));
			//SETTING WHRER CLAUSE PLACE HOLDERS
			//Reallocate to Self
			updatePostingSummary.setBigDecimal(9,accountInfoDo.getAMT_POSTED_CENTRAL_CCY());
			updatePostingSummary.setString(10,accountInfoDo.getPARTICIPANT_ID());
			updatePostingSummary.setString(11,accountInfoDo.getCOD_TXN());
			
			updatePostingSummary.setDate(12,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING_START()));
			updatePostingSummary.setString(13,accountInfoDo.getID_POOL());
			updatePostingSummary.setString(14,accountInfoDo.getTYP_PARTICIPANT());
			updatePostingSummary.setString(15,accountInfoDo.getSOURCE_POOL_ID());
			
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("accountInfoDo.getSOURCE_POOL_ID() updatePostingSummary  " + accountInfoDo.getSOURCE_POOL_ID());
			if(accountInfoDo.getSimId()!=0)
				updatePostingSummary.setLong(16, accountInfoDo.getSimId()); //ANZ_DEMO_Simulation
			//ENH_IS1731 Exec audit trailing changes Ends
			//Reallocate to Self
			updatePostingSummary.addBatch();
		}
		catch(SQLException e)
		{
			logger.fatal("Exception :: " , e);
			//ENH_IS1059  Starts
			//throw new LMSException("Error occured in updatePostingSummary", e.toString());
			throw new LMSException("LMS01000","Updation of  posting summary in OPOOL_POSTINGSUMMARY table failed for Transaction Id  :- "+accountInfoDo.getCOD_TXN()+" Pool Id  :- "+accountInfoDo.getID_POOL() +" Participant Id :- "+accountInfoDo.getPARTICIPANT_ID()+" and Type Participant Type is "+accountInfoDo.getTYP_PARTICIPANT()+".", e);
			//ENH_IS1059  Ends
		}
		if (logger.isDebugEnabled()){
			logger.debug("Leaving ");
		}
		
	}

	/**This method set the parameter into updateAccountSummary statement
	 * @param updateAccountSummary
	 * @param accountInfoDo
	 * @throws LMSException
	 */
	private void updateAccrualSummary(PreparedStatement updateAccountSummary, AccountInfoDO accountInfoDo) throws LMSException 
	{
		if (logger.isDebugEnabled())
		{
			logger.debug("Entering");
		}
		
		try
		{
			updateAccountSummary.setBigDecimal(1,accountInfoDo.getAMT_UNACCRUED());
			updateAccountSummary.setBigDecimal(2,accountInfoDo.getAMT_ACCRUED());
		
			updateAccountSummary.setString(3,accountInfoDo.getTXT_STATUS());
		
			updateAccountSummary.setDate(4,LMSUtility.truncSqlDate(accountInfoDo.getDAT_ADJUSTEDACCRUAL()));
			updateAccountSummary.setDate(5,LMSUtility.truncSqlDate(accountInfoDo.getDAT_FINAL_SETTLEMENT()));
			updateAccountSummary.setDate(6,LMSUtility.truncSqlDate(accountInfoDo.getDAT_ACCRUAL()));
			//ENH_IS1731 Exec audit trailing changes Starts
			// FindBug issue fix starts
			//updateAccountSummary.setString(7,com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT.format(new java.util.Date()));
			updateAccountSummary.setString(7,REF_DATE_FORMAT.format(new java.util.Date()));//Harikesh
			// FindBug issue fix ends
			updateAccountSummary.setDate(8,LMSUtility.truncSqlDate(accountInfoDo.getDAT_CURRENT_BUSINESS()));
			//SETTING WHRER CLAUSE PLACE HOLDERS
			updateAccountSummary.setString(9,accountInfoDo.getPARTICIPANT_ID());
			updateAccountSummary.setString(10,accountInfoDo.getCOD_TXN());
			updateAccountSummary.setDate(11,LMSUtility.truncSqlDate(accountInfoDo.getDAT_ACCRUAL_START()));
			updateAccountSummary.setString(12,accountInfoDo.getID_POOL());
			updateAccountSummary.setString(13,accountInfoDo.getTYP_PARTICIPANT());
			//ENH_IS1731 Exec audit trailing changes Ends
			// NEW_ACCOUNTING_ENTRY_FORMAT
			updateAccountSummary.setString(14 , accountInfoDo.getSOURCE_POOL_ID()); //NEW_ACCOUNTING_ENTRY_FORMAT
			if(accountInfoDo.getSimId()!= 0)
				updateAccountSummary.setLong(15, accountInfoDo.getSimId());
			//ENH_IS1731 Exec audit trailing changes Ends
			updateAccountSummary.addBatch();
		}
		catch(SQLException e)
		{
			logger.fatal("Exception :: " , e);
			//ENH_IS1059  Start
			//throw new LMSException("Error occured in updateAccrualSummary", e.toString());
			throw new LMSException("LMS01000","Updation of  accural summary in OPOOL_ACCRUALSUMMARY table failed for Transaction Id  :-"+accountInfoDo.getCOD_TXN()+" Pool Id  :- "+accountInfoDo.getID_POOL()+ " Participant Id :- "+accountInfoDo.getPARTICIPANT_ID()+" and Type Participant Type is "+accountInfoDo.getTYP_PARTICIPANT()+".", e);
			//ENH_IS1059  Ends
		
		}
		if (logger.isDebugEnabled()){
			logger.debug("Leaving ");
		}
		
	}

	/**
	 * This method set the parameters into insertAccrualSummary statement 
	 * @param insertAccrualSummary
	 * @param accountInfoDo
	 * @throws LMSException
	 */
	private void insertAccrualSummary(PreparedStatement insertAccrualSummary, AccountInfoDO accountInfoDo) throws LMSException
	{
		if (logger.isDebugEnabled()){
			logger.debug("Entering ");
		}
		try
		{
			insertAccrualSummary.setString(1,accountInfoDo.getID_POOL());
			insertAccrualSummary.setString(2,accountInfoDo.getPARTICIPANT_ID());
			insertAccrualSummary.setString(3,accountInfoDo.getCOD_TXN());
			insertAccrualSummary.setDate(4,LMSUtility.truncSqlDate(accountInfoDo.getDAT_ACCRUAL_START()));
			insertAccrualSummary.setDate(5,LMSUtility.truncSqlDate(accountInfoDo.getDAT_ACCRUAL()));
			insertAccrualSummary.setDate(6,LMSUtility.truncSqlDate(accountInfoDo.getDAT_ADJUSTEDACCRUAL()));
			insertAccrualSummary.setBigDecimal(7,accountInfoDo.getAMT_UNACCRUED());
			insertAccrualSummary.setBigDecimal(8,accountInfoDo.getAMT_ACCRUED());
			insertAccrualSummary.setDate(9,LMSUtility.truncSqlDate(accountInfoDo.getDAT_FINAL_SETTLEMENT()));
			insertAccrualSummary.setString(10,accountInfoDo.getTXT_STATUS());
			insertAccrualSummary.setDate(11,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING()));
			insertAccrualSummary.setString(12,accountInfoDo.getTYP_PARTICIPANT());	
			//ENH_IS1731 Exec audit trailing changes Starts
			// FindBug issue fix starts
			//insertAccrualSummary.setString(13,com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT.format(new java.util.Date()));
			insertAccrualSummary.setString(13,REF_DATE_FORMAT.format(new java.util.Date()));//Harikesh
			// FindBug issue fix ends
			insertAccrualSummary.setDate(14,LMSUtility.truncSqlDate(accountInfoDo.getDAT_CURRENT_BUSINESS()));
			//ENH_IS1731 Exec audit trailing changes Ends
			//Retro from 9.8 SUP_ISS1374  Start
			insertAccrualSummary.setDate(15,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING_START()));	
			//Retro from 9.8 SUP_IS1374 Ends 
			// NEW_ACCOUNTING_ENTRY_FORMAT
			insertAccrualSummary.setString(16,accountInfoDo.getSOURCE_POOL_ID()); //NEW_ACCOUNTING_ENTRY_FORMAT
			if(accountInfoDo.getSimId() != 0)
					insertAccrualSummary.setLong(17, accountInfoDo.getSimId());
			//Retro from 9.8 SUP_IS1374 Ends 
			insertAccrualSummary.addBatch();
		}
		catch(SQLException e )
		{

			logger.fatal("Exception :: " , e);
			//ENH_IS1059 Starts  
			//throw new LMSException("Error occured in insertAccrualSummary", e.toString());
			throw new LMSException("LMS01000","Insertion of  accural summary in OPOOL_ACCRUALSUMMARY table failed for Transaction Id  :- "+accountInfoDo.getCOD_TXN()+", Pool Id  :- "+accountInfoDo.getID_POOL()+",  Participant Id :- "+ accountInfoDo.getPARTICIPANT_ID()+" and Type Participant Type is "+accountInfoDo.getTYP_PARTICIPANT()+".", e);
			//ENH_IS1059  Ends
		}
		if (logger.isDebugEnabled()){
			logger.debug("Leaving ");
		}
	}
	
	/**
	 * This method set the parameter into insertPostingSummary statement
	 * @param insertPostingSummary
	 * @param accountInfoDo
	 * @throws LMSException
	 */
	private void insertPostingSummary(PreparedStatement insertPostingSummary, AccountInfoDO accountInfoDo) throws LMSException 
	{
		if (logger.isDebugEnabled()){
			logger.debug("Entering ");
		}
		try
		{
//			NT_UT_BUFIX_18_Feb_09 Start //ENH_IS1913 starts
			if (!(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)))
			{
				if(!(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCO)))
				{
				boolean holdreq=chkPendingHoldReq(accountInfoDo);
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("holdreq -- "+holdreq);
				if(holdreq)
				{
				logger.debug("Hold req. is present in populate posting summary");
				 accountInfoDo.setTXT_STATUS("HOLD");
				//	accountInfoDo.setAMT_POSTED(ZERO);
					//Resetting AMT_ACCRUEDNOTPOSTED to 0
				//	accountInfoDo.setAMT_ACCRUEDNOTPOSTED(accountInfoDo.getAMT_ACCRUEDNOTPOSTED());
				}
				} 
			} 
			//NT_UT_BUFIX_18_Feb_09 End starts
			
			
			insertPostingSummary.setString(1,accountInfoDo.getID_POOL());
			insertPostingSummary.setString(2,accountInfoDo.getPARTICIPANT_ID());
			insertPostingSummary.setString(3,accountInfoDo.getCOD_TXN());
			insertPostingSummary.setDate(4,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING_START()));
			insertPostingSummary.setDate(5,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING()));
			insertPostingSummary.setDate(6,LMSUtility.truncSqlDate(accountInfoDo.getDAT_FINAL_SETTLEMENT()));
			
			insertPostingSummary.setBigDecimal(7,accountInfoDo.getAMT_ACCRUEDNOTPOSTED());
			insertPostingSummary.setBigDecimal(8,accountInfoDo.getAMT_POSTED());
			insertPostingSummary.setString(9,accountInfoDo.getTXT_STATUS());
			insertPostingSummary.setDate(10,LMSUtility.truncSqlDate(accountInfoDo.getDAT_VALUE()));
			//ENH_IS1731 Exec audit trailing changes Starts
			insertPostingSummary.setString(11,accountInfoDo.getTYP_PARTICIPANT());
			// FindBug issue fix starts
			//insertPostingSummary.setString(12,com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT.format(new java.util.Date()));
			insertPostingSummary.setString(12,REF_DATE_FORMAT.format(new java.util.Date()));//Harikesh
			// FindBug issue fix ends
			insertPostingSummary.setDate(13,LMSUtility.truncSqlDate(accountInfoDo.getDAT_CURRENT_BUSINESS()));
			//NEW_ACCOUNTING_ENTRY_FORMAT
			insertPostingSummary.setString(14,accountInfoDo.getSOURCE_POOL_ID());
			//Reallocate to Self
			insertPostingSummary.setBigDecimal(15,accountInfoDo.getAMT_POSTED_CENTRAL_CCY());
			if(accountInfoDo.getSimId() != 0)
				insertPostingSummary.setLong(16, accountInfoDo.getSimId());
				//Reallocate to Self
			//ENH_IS1731 Exec audit trailing changes Ends
			insertPostingSummary.addBatch(); 
			}
		catch(SQLException e)
		{
			logger.fatal("Exception :: " , e);
			//ENH_IS1059 Starts  
			//throw new LMSException("Error occured in insertPostingSummary", e.toString());
			throw new LMSException("LMS01000","Insertion of  posting summary in OPOOL_POSTINGSUMMARY table failed for Transaction Id  :- "+accountInfoDo.getCOD_TXN()+", Pool Id  :- "+accountInfoDo.getID_POOL()+", Participant Id :- "+ accountInfoDo.getPARTICIPANT_ID()+" and Type Participant Type is "+accountInfoDo.getTYP_PARTICIPANT()+".", e);
			//ENH_IS1059  Ends
		}
		
		if (logger.isDebugEnabled()){
			logger.debug("Leaving ");
		}
	}


	
	

	
	
	
	/**
	 * This method checks whether an entry is there in  AccrualSummary table for particular txn code and sets the some data into Data Object
	 * @param accountInfoDo
	 * @param selectAccrualSummary
	 * @return
	 * @throws LMSException
	 */
	private boolean checkRecordInAccrualSummary(AccountInfoDO accountInfoDo,PreparedStatement selectAccrualSummary) throws LMSException
	{
		
		
		ResultSet res = null;
		boolean found = false;
		if (logger.isDebugEnabled())
		{
			logger.debug("Entering ");
			logger.debug(" DATA OBJECT DETAILS checkRecordInAccrualSummary " + accountInfoDo.toString());
		}
		
		try
		{
			
			
			selectAccrualSummary.setString(1,accountInfoDo.getPARTICIPANT_ID());
			selectAccrualSummary.setString(2,accountInfoDo.getCOD_TXN());
			selectAccrualSummary.setDate(3,LMSUtility.truncSqlDate(accountInfoDo.getDAT_ACCRUAL_START()));
			selectAccrualSummary.setString(4,accountInfoDo.getID_POOL());
			selectAccrualSummary.setString(5,accountInfoDo.getTYP_PARTICIPANT());	
			//SUP_ANZ_4110 starts
			//selectAccrualSummary.setDate(6,LMSUtility.truncSqlDate(accountInfoDo.getDAT_INITIAL_POSTING()));
			selectAccrualSummary.setString(6,accountInfoDo.getSOURCE_POOL_ID());
			//SUP_ANZ_4110 ends
			if(accountInfoDo.getSimId()!=0)
				selectAccrualSummary.setLong(7, accountInfoDo.getSimId());
			if (logger.isDebugEnabled())
			{
				logger.debug("Check Record for checkRecordInAccrualSummary " + accountInfoDo.getPARTICIPANT_ID() );
				logger.debug("Check Record for checkRecordInAccrualSummary " + accountInfoDo.getCOD_TXN() );
				logger.debug("Check Record for checkRecordInAccrualSummary " + accountInfoDo.getDAT_ACCRUAL_START() );
				logger.debug("Check Record for checkRecordInAccrualSummary " + accountInfoDo.getID_POOL() );
				logger.debug("Check Record for checkRecordInAccrualSummary " + accountInfoDo.getTYP_PARTICIPANT() );
				logger.debug("Check Record for checkRecordInAccrualSummary " + accountInfoDo.getDAT_INITIAL_POSTING() );
				logger.debug("Check Record for checkRecordInAccrualSummary " + accountInfoDo.getSOURCE_POOL_ID() );
			}
			
			res = selectAccrualSummary.executeQuery();
			while(res.next())
			{
				found = true;
				
				accountInfoDo.setAMT_UNACCRUED(res.getBigDecimal("AMT_UNACCRUED"));
				accountInfoDo.setOLD_DAT_ACCRUAL(res.getDate("DAT_ACCRUAL"));
				accountInfoDo.setOLD_DAT_ADJUSTEDACCRUAL(res.getDate("DAT_ACCRUAL_ADJUSTED"));
				//FOR BVT AMT_ACCRUED is needed
				accountInfoDo.setAMT_ACCRUED(res.getBigDecimal("AMT_ACCRUED"));
				if (logger.isDebugEnabled())
				{
					logger.debug("IN DB amtUnaccured " + res.getDouble("AMT_UNACCRUED"));
					logger.debug("IN DB amtUnaccured " + res.getDouble("AMT_ACCRUED"));
				}
			}
			
			
		}
		catch(SQLException e)
		{
			logger.fatal("Exception :: " , e);
			//ENH_IS1059  Starts
			//throw new LMSException("Error occured in checkRecordInAccountSummary", e.toString());
			throw new LMSException("LMS01000","Record already exist in OPOOL_ACCRUALSUMMARY table for Transaction Id "+accountInfoDo.getCOD_TXN()+", Pool Id :-"+accountInfoDo.getID_POOL()+", Participant :- "+accountInfoDo.getPARTICIPANT_ID()+" and Participant Type :"+accountInfoDo.getTYP_PARTICIPANT()+".", e);
			//ENH_IS1059  Ends
		}finally
		{
	        LMSConnectionUtility.close(res);
		}
		if (logger.isDebugEnabled()){
			logger.debug("Leaving ");
		}
		return found;
		
	}
		
	/**
	 * This method checks whether an entry is there in  PostingSummary table for particular txn code and sets the some data into Data Object
	 * @param accountInfoDo
	 * @param selectPostingSummary
	 * @return
	 * @throws LMSException
	 */
	private boolean checkRecordInPostingSummary(AccountInfoDO accountInfoDo,PreparedStatement selectPostingSummary) throws LMSException
	{
		if (logger.isDebugEnabled())
		{
			logger.debug("Entering ");
			logger.debug(" DATA OBJECT DETAILS checkRecordInPostingSummary " + accountInfoDo.toString());
		}
		
		ResultSet res = null;
		boolean found = false;
		//added by BOI issue 145
		PreparedStatement selectTypRepay=null;
		Connection conn = null;
		//end by BOI issue 145
		try
		{
			if (logger.isDebugEnabled())
			{
				logger.debug("COD_TXN in checkRecordInPostingSummary "+accountInfoDo.getCOD_TXN());
				logger.debug("getSOURCE_POOL_ID in checkRecordInPostingSummary "+accountInfoDo.getSOURCE_POOL_ID());
			  }
			
			selectPostingSummary.setString(1,accountInfoDo.getPARTICIPANT_ID());
			selectPostingSummary.setString(2,accountInfoDo.getCOD_TXN());
			selectPostingSummary.setDate(3,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING_START()));
			selectPostingSummary.setString(4,accountInfoDo.getID_POOL());
			selectPostingSummary.setString(5,accountInfoDo.getTYP_PARTICIPANT());
			selectPostingSummary.setString(6,accountInfoDo.getSOURCE_POOL_ID()); // NEW_ACCOUNTING_ENTRY_FORMAT
			if(accountInfoDo.getSimId()!=0)
				selectPostingSummary.setLong(7, accountInfoDo.getSimId());
			res = selectPostingSummary.executeQuery();
			while(res.next())
			{
				found = true;
				accountInfoDo.setAMT_ACCRUEDNOTPOSTED(res.getBigDecimal("AMT_ACCRUEDNOTPOSTED"));
				accountInfoDo.setAMT_POSTED(res.getBigDecimal("AMT_POSTED"));
				accountInfoDo.setOLD_DAT_FINAL_SETTLEMENT(res.getDate("DAT_FINAL_SETTLEMENT"));
				
				accountInfoDo.setOLD_DAT_POSTING(res.getDate("DAT_POSTING"));
				accountInfoDo.setAMT_POSTED_CENTRAL_CCY(res.getBigDecimal("AMT_POSTED_CENTRAL_CCY"));//Reallocate to Self
				
				if (logger.isDebugEnabled())
				{
					logger.debug("IN DB amtAccuredNotPosted " + res.getDouble("AMT_ACCRUEDNOTPOSTED"));
					logger.debug("IN DB AMT_POSTED " + res.getDouble("AMT_POSTED"));
					logger.debug("IN DB AMT_POSTED_CENTRAL_CCY " + res.getDouble("AMT_POSTED_CENTRAL_CCY"));//Reallocate to Self
				}
			}
			//added by BOI issue 145
			
			conn = LMSConnectionUtility.getConnection(); 
			String sqlString = "select TYP_REPAYMENT from OLM_INCO_DEFINITION where COD_INCOID =?";
			selectTypRepay= conn.prepareStatement(sqlString);
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("RPI case accountInfoDo.getID_POOL() "+accountInfoDo.getID_POOL());
			selectTypRepay.setString(1, accountInfoDo.getID_POOL());
			LMSConnectionUtility.close(res); //15.1.1_Resource_Leaks_Fix
			res =null;
			res=selectTypRepay.executeQuery();
			String typ_repayment="";
			if(res.next()){
				typ_repayment=res.getString("TYP_REPAYMENT");
			}
			accountInfoDo.setTYP_REPAYMENT(typ_repayment);
			//end by BOI issue 145
			
			
		}
		catch(Exception e)
		{
			logger.fatal("Exception :: " , e);
			//ENH_IS1059  Starts
			//throw new LMSException("Error occured in checkRecordInPostigSummary", e.toString());
			throw new LMSException("LMS01000","Record already exist in OPOOL_POSTINGSUMMARY table for Transaction Id "+accountInfoDo.getCOD_TXN()+", Pool Id :- "+accountInfoDo.getID_POOL()+", Participant :- "+accountInfoDo.getPARTICIPANT_ID()+" and Participant Type :"+accountInfoDo.getTYP_PARTICIPANT()+".", e);
			//ENH_IS1059  Ends
		}finally
		{
		    LMSConnectionUtility.close(res);
		    //added by BOI issue 145
		    LMSConnectionUtility.close(selectTypRepay);
		    LMSConnectionUtility.close(conn);
		    
		    //end by BOI issue 145
	    }
		if (logger.isDebugEnabled()){
			logger.debug("Leaving ");
		}
		return found;
	}

	
	
	/** This method call the processBvtTXNCodes method
	 * @param bvtId
	 * @param businessDate
	 * @param client
	 * @param insertAccountDtls
	 * @param updateAccountDtls
	 * @param insertAccrualSummary
	 * @param insertPostingSummary
	 * @param updateAccrualSummary
	 * @param updatePostingSummary
	 * @param insertUnpostedAmt
	 * @param selectAccountDtls
	 * @param selectAccrualSummary
	 * @param selectPostingSummary
	 * @return
	 * @throws LMSException
	 */
	public boolean[] generateBVTAccountSummaryInfo(String bvtId,java.util.Date businessDate, String client,
			PreparedStatement insertAccountDtls,PreparedStatement updateAccountDtls,
			PreparedStatement insertAccrualSummary,
			PreparedStatement insertPostingSummary,PreparedStatement updateAccrualSummary,
			PreparedStatement updatePostingSummary, PreparedStatement insertUnpostedAmt,
			PreparedStatement selectAccountDtls ,PreparedStatement selectAccrualSummary ,
			//ENH_IS1297 RETROFIT STARTS
			PreparedStatement selectPostingSummary,PreparedStatement updateUnpostedAmt,PreparedStatement updateUnpostedAmtCentralCcy)throws LMSException//Reallocate to Self
			//ENH_IS1297 RETROFIT ENDS
	{
			
		if (logger.isDebugEnabled()){
			logger.debug("Entering");
		}
		try
		{
			//ENH_IS1297 RETROFIT STARTS
			//cache the unposted amount. 
			//getCacheUnpostedAmountBVT();
			getCacheUnpostedAmountBVTForPool(); //NEW_ACCOUNTING_ENTRY_FORMAT
			getCacheUnpostedAmountCentralCcyBVTForPool();//Reallocate to Self
			//ENH_IS1297 RETROFIT ENDS
			
			processBvtTXNCodes(bvtId,businessDate, client, insertAccountDtls,updateAccountDtls, 
					insertAccrualSummary, insertPostingSummary, updateAccrualSummary, 
					//ENH_IS1297 RETROFIT STARTS
					updatePostingSummary,insertUnpostedAmt,selectAccountDtls ,selectAccrualSummary ,selectPostingSummary,updateUnpostedAmt,updateUnpostedAmtCentralCcy);//Reallocate to Self
					//ENH_IS1297 RETROFIT ENDS
		
		}
			
		catch(Exception e)
		{
			logger.fatal("General Exception: ", e);
			
			//ENH_IS1059  Starts
			//throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, e.getMessage());
			throw new LMSException("LMS01000","Error occurred while generating BVTAccount Summary Information for BVT Id "+bvtId+" and Host System "+client+".", e);
			//ENH_IS1059  Ends
		}
		/*
		 * entries will be set true in insertAccountDtls(),populateAccrualSummary() and populatePosting Summary
		 * indicates  corresponding preparedStatement need to be executed in processAccountingEntries() method in 
		 * LotProcessorSBSLBean. 
		 */
		if (logger.isDebugEnabled()){
			logger.debug("Leaving ");
		}
		return entries;
	}

	//ENH_10.3_154 Starts
	/** This method call the processCycleWiseBvtTXNCodes method
	 * @param bvtId
	 * @param businessDate
	 * @param client
	 * @param insertAccountDtls
	 * @param updateAccountDtls
	 * @param insertAccrualSummary
	 * @param insertPostingSummary
	 * @param updateAccrualSummary
	 * @param updatePostingSummary
	 * @param insertUnpostedAmt
	 * @param selectAccountDtls
	 * @param selectAccrualSummary
	 * @param selectPostingSummary
	 * @return
	 * @throws LMSException
	 */
	// Changed  method name from 'generateCycleWiseBVTAccountSummaryInfo' to 'generateCycleWiseBVTAccountSummaryInfoForPool'
	public boolean[] generateCycleWiseBVTAccountSummaryInfoForPool(String bvtId,java.util.Date businessDate, String client,
			PreparedStatement insertAccountDtls,PreparedStatement updateAccountDtls,
			PreparedStatement insertAccrualSummary,
			PreparedStatement insertPostingSummary,PreparedStatement updateAccrualSummary,
			PreparedStatement updatePostingSummary, PreparedStatement insertUnpostedAmt,
			PreparedStatement selectAccountDtls ,PreparedStatement selectAccrualSummary ,
			PreparedStatement selectPostingSummary,PreparedStatement updateUnpostedAmt)throws LMSException
	{
			
		if (logger.isDebugEnabled()){
			logger.debug("Entering generateCycleWiseBVTAccountSummaryInfo");
		}
		
		try
		{
			//Changed  method name from 'processCycleWiseBvtTXNCodes' to 'processCycleWiseBvtTXNCodesForPool'
			processCycleWiseBvtTXNCodesForPool(bvtId,businessDate, client, insertAccountDtls,updateAccountDtls, 
					insertAccrualSummary, insertPostingSummary, updateAccrualSummary, 
					updatePostingSummary,insertUnpostedAmt,selectAccountDtls ,selectAccrualSummary ,selectPostingSummary,updateUnpostedAmt);
		}
			
		catch(Exception e)
		{
			logger.fatal("General Exception: ", e);
			
			//ENH_IS1059  Starts
			//throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, e.getMessage());
			throw new LMSException("LMS01000","Error occurred while generating BVTAccount Summary Information for BVT Id "+bvtId+" and Host System "+client+".", e);
			//ENH_IS1059  Ends
		}
		/*
		 * entries will be set true in insertAccountDtls(),populateAccrualSummary() and populatePosting Summary
		 * indicates  corresponding preparedStatement need to be executed in processAccountingEntries() method in 
		 * LotProcessorSBSLBean. 
		 */
		if (logger.isDebugEnabled()){
			logger.debug("Leaving generateCycleWiseBVTAccountSummaryInfo");
		}
		return entries;
	}
	
	/**
	 * This method prepare the select query and call the  populateBvtAccountInfo method  to execute the same.
	 * @param bvtId
	 * @param businessDate
	 * @param client
	 * @param insertAccountDtls
	 * @param updateAccountDtls
	 * @param insertAccrualSummary
	 * @param insertPostingSummary
	 * @param updateAccrualSummary
	 * @param updatePostingSummary
	 * @param insertUnpostedAmt
	 * @param selectAccountDtls
	 * @param selectAccrualSummary
	 * @param selectPostingSummary
	 * @throws LMSException
	 */
	// Changed  method name from 'processCycleWiseBvtTXNCodes' to 'processCycleWiseBvtTXNCodesForPool'
	private void processCycleWiseBvtTXNCodesForPool( String bvtId,Date businessDate, String client, PreparedStatement insertAccountDtls,
			PreparedStatement updateAccountDtls, PreparedStatement insertAccrualSummary, 
			PreparedStatement insertPostingSummary, PreparedStatement updateAccrualSummary, 
			PreparedStatement updatePostingSummary,PreparedStatement insertUnpostedAmt,
			PreparedStatement selectAccountDtls ,PreparedStatement selectAccrualSummary ,
			PreparedStatement selectPostingSummary,PreparedStatement updateUnpostedAmt ) throws LMSException
	{
		boolean allowIntTyp = ExecutionConstants.FLG_INTACC_POST_INTTYP ; //Sandeep changes for ned issue
		boolean flg_newAccountingentry = ExecutionConstants.FlG_NEW_ACCOUNTING_ENTRY_FORMAT; // NEW_ACCOUNTING_ENTRY_FORMAT
		if (logger.isDebugEnabled()){
			//logger.debug("Entering processCycleWiseBvtTXNCodes");
			logger.debug("Entering processCycleWiseBvtTXNCodesForPool");
			logger.debug(" allowIntTyp" + allowIntTyp);
			logger.debug(" flg_newAccountingentry " + flg_newAccountingentry);
		}
		
		StringBuilder Strquery = new StringBuilder(0);
		Strquery.append(" SELECT   TYP_PARTICIPANT, (CASE ")
		.append("              WHEN (SUBSTR (cod_txn, 5, 1) IN ('C', 'B')) ")
		.append("                 THEN amt_cr_interest_diff ")
		.append("              ELSE 0 ")
		.append("           END ")
		.append("          ) amt_calculated, ")
		//NED_ISSUE FIXED STARTS
		/*.append("          currency cod_ccy, dat_business_poolrun busdt, id_pool, flg_realpool, ")
		.append("          cod_txn ||'I' COD_TXN, participant_id, 'CREDIT' txn_type, DAT_POSTING ")
		.append("     FROM opool_txnpool_bvtdiff_int tbl, olm_glmst gl ")
		.append("    WHERE id_bvt = "+bvtId)*/
		.append("          currency cod_ccy, tbl.dat_business_poolrun busdt, tbl.id_pool, tbl.flg_realpool, ")
		//.append("          cod_txn ||'I' COD_TXN, participant_id, 'CREDIT' txn_type, tbl.dat_posting, NVL((hdr.DAT_LASTPOSTING+1),hdr.DAT_start) DAT_POSTING_START, tbl.typ_interest ")//BVT_ISSSUE_01
		.append("          REGEXP_REPLACE(cod_txn, '[B]', 'C', 5, 1) ||'I' COD_TXN, participant_id, 'CREDIT' txn_type, tbl.dat_posting, NVL((hdr.DAT_LASTPOSTING+1),hdr.DAT_start) DAT_POSTING_START, tbl.typ_interest ") //BVT_ISSSUE_01
		.append(" , tbl.id_pool SOURCE_POOL_ID,hdr.flg_acc_accrual_pricing_feed, 0 amt_calculated_central_ccy, 0 RATE_EXRATE_CENTRAL_CCY,hdr.dat_post_settlement ") // NEW_ACCOUNTING_ENTRY_FORMAT//SUP_JPMCIS17438//Reallocate to Self//SUP_ISS_IV_7602
		//SUP_ENH_III_8106 starts
		//.append("     FROM opool_txnpool_bvtdiff_int tbl, olm_glmst gl, opool_txnpoolhdr hdr ");
		.append("     FROM opool_txnpool_bvtdiff_int tbl, opool_txnpoolhdr hdr ");
		//SUP_ENH_III_8106 ends	
	//Sandeep changes for ned issue	
		if(allowIntTyp)
			Strquery.append("     , orate_interest_type int_type ");
		
		Strquery.append("    WHERE tbl.id_bvt = "+bvtId)
		//NED_ISSUE FIXED ENDS
		.append("      AND typ_participant = 'ACCOUNT' ")
		.append("      AND tbl.cod_gl = '"+ client+"' ")
		.append("      AND amt_cr_interest_diff <> 0 ")
		//SUP_ENH_III_8106 starts
		//.append("      AND tbl.cod_gl = gl.cod_gl ")
		//.append("      AND gl.FLG_BACKVALUE_POSTING = '"+ExecutionConstants.YES+"'")
		//SUP_ENH_III_8106  ends
		//NED_ISSUE FIXED STARTS
		.append("      AND tbl.id_pool = hdr.id_pool")
		 .append("      and tbl.NBR_PROCESSID = hdr.NBR_PROCESSID")
		 .append("      and tbl.DAT_BUSINESS_POOLRUN = hdr.DAT_BUSINESS_POOLRUN and tbl.ACCOUNT_ACCRUAL = 'Y' ");//JPMC_620
		 if(allowIntTyp){    //Sandeep changes for ned issue
			 Strquery.append("      AND tbl.typ_interest = int_type.typ_interest(+) ")
			 .append("      AND ('N' = int_type.flg_interestypeas_fee OR int_type.flg_interestypeas_fee IS NULL) ");
			}
		//NED_ISSUE FIXED ENDS
		Strquery.append(" ORDER BY id_pool, participant_id, cod_txn, dat_posting,SOURCE_POOL_ID ");//iRelease_issueNo_61
		if (logger.isDebugEnabled()){
			//logger.debug("SELECT QUERY1 FOR CREDIT IN processCycleWiseBvtTXNCodes :"+ Strquery.toString());
			logger.debug("SELECT QUERY1 FOR CREDIT IN processCycleWiseBvtTXNCodesForPool :"+ Strquery.toString());
		}
		// Changed method name
		//populateCycleWiseBvtAccountInfo(Strquery.toString(),bvtId,businessDate, client, insertAccountDtls,updateAccountDtls, 
				populateCycleWiseBvtAccountInfoForPool(Strquery.toString(),bvtId,businessDate, client, insertAccountDtls,updateAccountDtls,		
				insertAccrualSummary, insertPostingSummary, updateAccrualSummary, updatePostingSummary,
				insertUnpostedAmt,selectAccountDtls ,selectAccrualSummary ,selectPostingSummary,updateUnpostedAmt);
		
		Strquery.replace(0,Strquery.length(),"");
		Strquery.append(" SELECT  TYP_PARTICIPANT, (CASE ")
		 .append("              WHEN (SUBSTR (cod_txn, 5, 1) IN ('D', 'B')) ")
		 .append("                 THEN amt_dr_interest_diff ")
		 .append("              ELSE 0 ")
		 .append("           END ")
		 .append("          ) amt_calculated, ")
		//NED_ISSUE FIXED STARTS
		 /*.append("          currency cod_ccy, dat_business_poolrun busdt, id_pool, flg_realpool, ")
		 .append("          cod_txn ||'I' COD_TXN, participant_id, 'DEBIT' txn_type, DAT_POSTING ")
		 .append("     FROM opool_txnpool_bvtdiff_int tbl, olm_glmst gl ")
		 .append("    WHERE id_bvt = "+bvtId)*/
		 .append("          currency cod_ccy, tbl.dat_business_poolrun busdt, tbl.id_pool, tbl.flg_realpool, ")
		 //.append("          cod_txn ||'I' COD_TXN, participant_id, 'DEBIT' txn_type, tbl.dat_posting, NVL((hdr.DAT_LASTPOSTING+1),hdr.DAT_start) DAT_POSTING_START, tbl.typ_interest ") //BVT_ISSSUE_01
		 .append("          REGEXP_REPLACE(cod_txn, '[B]', 'D', 5, 1) ||'I' COD_TXN, participant_id, 'DEBIT' txn_type, tbl.dat_posting, NVL((hdr.DAT_LASTPOSTING+1),hdr.DAT_start) DAT_POSTING_START, tbl.typ_interest ") //BVT_ISSSUE_01
		 .append(" ,  tbl.id_pool SOURCE_POOL_ID,hdr.flg_acc_accrual_pricing_feed, 0 amt_calculated_central_ccy, 0 RATE_EXRATE_CENTRAL_CCY,hdr.dat_post_settlement  ") // NEW_ACCOUNTING_ENTRY_FORMAT//SUP_JPMCIS17438//Reallocate to Self//SUP_ISS_IV_7602
		 //SUP_ENH_III_8106 starts
		 //.append("     FROM opool_txnpool_bvtdiff_int tbl, olm_glmst gl, opool_txnpoolhdr hdr ");
		 .append("     FROM opool_txnpool_bvtdiff_int tbl,  opool_txnpoolhdr hdr ");
		 //SUP_ENH_III_8106 ends
//Sandeep changes for ned issue		
		if(allowIntTyp)
			Strquery.append("     , orate_interest_type int_type ");
			
			Strquery.append("    WHERE tbl.id_bvt = "+bvtId)
		//NED_ISSUE FIXED ENDS
		 .append("      AND typ_participant = 'ACCOUNT' ")
		 .append("      AND tbl.cod_gl = '"+ client+"' ")
		 .append("      AND amt_dr_interest_diff <> 0 ")
		 //SUP_ENH_III_8106 starts
		 //.append("      AND tbl.cod_gl = gl.cod_gl ")
		 //.append("      AND gl.FLG_BACKVALUE_POSTING = '"+ExecutionConstants.YES+"'")
		//SUP_ENH_III_8106 ends
		//NED_ISSUE FIXED STARTS
		 .append("      AND tbl.id_pool = hdr.id_pool")
		 .append("      and tbl.NBR_PROCESSID = hdr.NBR_PROCESSID")
		 .append("      and tbl.DAT_BUSINESS_POOLRUN = hdr.DAT_BUSINESS_POOLRUN  and tbl.ACCOUNT_ACCRUAL = 'Y' ");//JPMC_620
		 if(allowIntTyp){     //Sandeep changes for ned issue
			 Strquery.append("      AND tbl.typ_interest = int_type.typ_interest(+) ")
			 .append("      AND ('N' = int_type.flg_interestypeas_fee OR int_type.flg_interestypeas_fee IS NULL) ");
		 }
		//NED_ISSUE FIXED ENDS
		Strquery.append(" ORDER BY id_pool, participant_id, cod_txn, dat_posting,SOURCE_POOL_ID ");//iRelease_issueNo_61
		 
		if (logger.isDebugEnabled()){
			//logger.debug("SELECT QUERY2 FOR DEBIT IN processCycleWiseBvtTXNCodes :"+ Strquery.toString());
			logger.debug("SELECT QUERY2 FOR DEBIT IN processCycleWiseBvtTXNCodesForPool :"+ Strquery.toString());
		}
		//populateCycleWiseBvtAccountInfo(Strquery.toString(),bvtId,businessDate, client, insertAccountDtls,updateAccountDtls, 
		populateCycleWiseBvtAccountInfoForPool(Strquery.toString(),bvtId,businessDate, client, insertAccountDtls,updateAccountDtls,
				insertAccrualSummary, insertPostingSummary, updateAccrualSummary, updatePostingSummary,
				insertUnpostedAmt,selectAccountDtls ,selectAccrualSummary,
				selectPostingSummary, updateUnpostedAmt);
			
		Strquery.replace(0,Strquery.length(),"");
		Strquery.append(" SELECT TYP_PARTICIPANT, (CASE ")
		.append("            WHEN (SUBSTR (cod_txn, 5, 1) IN ('C', 'B')) ")
		.append("               THEN amt_cr_diff ")
		.append("            ELSE 0 ")
		.append("         END ")
		.append("        ) amt_calculated, ")
		//NED_ISSUE FIXED STARTS
		/*.append("        dat_business_poolrun busdt, participant_id, id_pool, ")
		.append("          flg_realpool, ")*/
		.append("        tbl.dat_business_poolrun busdt, tbl.participant_id, tbl.id_pool, ")
		.append("          tbl.flg_realpool, ")
		//NED_ISSUE FIXED ENDS
		.append("		(CASE WHEN typ_participant = '"+ExecutionConstants.CENTRAL +"'")
		.append("		 THEN REGEXP_REPLACE(cod_txn, '[B]', 'C', 5, 1) || 'C'") //BVT_ISSSUE_01
		//.append("		 THEN cod_txn || 'C'")
		.append("		 WHEN typ_participant = '"+ExecutionConstants.BANKSHARE+"'")
		//.append(" 		 THEN cod_txn || 'B'")
		.append(" 		 THEN REGEXP_REPLACE(cod_txn, '[B]', 'C', 5, 1) || 'B'") //BVT_ISSSUE_01
		.append("		 WHEN typ_participant = '"+ExecutionConstants.TREASURY+"'")
		//.append("		 THEN cod_txn || 'T'")
		.append("		 THEN REGEXP_REPLACE(cod_txn, '[B]', 'C', 5, 1) || 'T'")  //BVT_ISSSUE_01
		//.append("		 ELSE cod_txn || 'A' END)cod_txn,")//for typ_participant = ACCOUNT
		.append("		 ELSE REGEXP_REPLACE(cod_txn, '[B]', 'C', 5, 1) || 'A' END)cod_txn,");//for typ_participant = ACCOUNT  //BVT_ISSSUE_01
		//NED_ISSUE FIXED STARTS
		/*.append("	  currency cod_ccy, 'CREDIT' txn_type, DAT_POSTING ")
		.append("   FROM opool_txnpool_bvtdiff tbl, olm_glmst gl ")
		.append("  WHERE id_bvt = "+bvtId)*/
		Strquery.append("	  currency cod_ccy, 'CREDIT' txn_type, tbl.dat_posting, NVL((hdr.DAT_LASTPOSTING+1),hdr.DAT_start) DAT_POSTING_START, tbl.typ_interest ");
		if(flg_newAccountingentry)
		{	
			Strquery.append(" , tbl.SOURCE_POOL_ID, null flg_acc_accrual_pricing_feed " );	 // NEW_ACCOUNTING_ENTRY_FORMAT//SUP_JPMCIS17438
		}else
		{
			Strquery.append(" , tbl.id_pool SOURCE_POOL_ID, null flg_acc_accrual_pricing_feed " );	 // NEW_ACCOUNTING_ENTRY_FORMAT//SUP_JPMCIS17438
		}
		
		Strquery.append(" ,( CASE WHEN (SUBSTR (cod_txn, 5, 1) IN ('C', 'B')) THEN nvl(AMT_CR_DIFF_CENTRAL_CCY,0) ELSE 0 END ) amt_calculated_central_ccy, RATE_EXRATE_CENTRAL_CCY,hdr.dat_post_settlement  ");//Reallocate to Self//SUP_ISS_IV_7296//SUP_ISS_IV_7602
		//SUP_ENH_III_8106 starts
		//Strquery.append("   FROM opool_txnpool_bvtdiff tbl, opool_txnpoolhdr hdr, olm_glmst gl ");
		Strquery.append("   FROM opool_txnpool_bvtdiff tbl, opool_txnpoolhdr hdr ");
		//SUP_ENH_III_8106 ends
		if(allowIntTyp)
			Strquery.append("     , orate_interest_type int_type ");
		
		Strquery.append("  WHERE tbl.id_bvt = "+bvtId)
		//NED_ISSUE FIXED ENDS
		.append("    AND tbl.cod_gl = '"+ client+"' ")
		.append("    AND typ_participant <> 'POOL' ")
		.append("    AND amt_cr_diff <> 0 ")
		.append(" AND COD_TXN IS NOT NULL ")
		//SUP_ENH_III_8106 starts
		//.append("      AND tbl.cod_gl = gl.cod_gl ")
		//.append("      AND gl.FLG_BACKVALUE_POSTING = '"+ExecutionConstants.YES+"'")
		//SUP_ENH_III_8106 ends
		//NED_ISSUE FIXED STARTS
		.append("      AND tbl.id_pool = hdr.id_pool")
		.append("      and tbl.NBR_PROCESSID = hdr.NBR_PROCESSID")
		.append("      and tbl.DAT_BUSINESS_POOLRUN = hdr.DAT_BUSINESS_POOLRUN");
		//NED_ISSUE FIXED ENDS
		if(allowIntTyp){   //Sandeep changes for ned issue
			Strquery.append("      AND tbl.typ_interest = int_type.typ_interest(+) ")
			.append("      AND ('N' = int_type.flg_interestypeas_fee OR int_type.flg_interestypeas_fee IS NULL) ");
		}
		Strquery.append(" ORDER BY id_pool, participant_id, cod_txn, dat_posting,SOURCE_POOL_ID ");//iRelease_issueNo_61
		
		if (logger.isDebugEnabled()){
			//logger.debug("SELECT QUERY3 FOR CREDIT IN processCycleWiseBvtTXNCodes :"+ Strquery.toString());
			logger.debug("SELECT QUERY3 FOR CREDIT IN processCycleWiseBvtTXNCodesForPool :"+ Strquery.toString());
		}
		//populateCycleWiseBvtAccountInfo(Strquery.toString(),bvtId,businessDate, client, insertAccountDtls,updateAccountDtls,
		  populateCycleWiseBvtAccountInfoForPool(Strquery.toString(),bvtId,businessDate, client, insertAccountDtls,updateAccountDtls,
				insertAccrualSummary, insertPostingSummary, updateAccrualSummary, updatePostingSummary,
				insertUnpostedAmt,selectAccountDtls ,selectAccrualSummary ,selectPostingSummary,updateUnpostedAmt);
		
		
		
		Strquery.replace(0,Strquery.length(),"");

		Strquery.append(" SELECT  TYP_PARTICIPANT,  (CASE ")
		.append("              WHEN (SUBSTR (cod_txn, 5, 1) IN ('D', 'B')) ")
		.append("                 THEN amt_dr_diff ")
		.append("              ELSE 0 ")
		.append("           END ")
		.append("          ) amt_calculated, ")
		//NED_ISSUE FIXED STARTS
		/*.append("          dat_business_poolrun busdt, bvtdiff.participant_id, id_pool, ")
		.append("          flg_realpool, ")*/
		.append("          bvtdiff.dat_business_poolrun busdt, bvtdiff.participant_id, bvtdiff.id_pool, ")
		.append("          bvtdiff.flg_realpool, ")
		//NED_ISSUE FIXED ENDS
		.append("		(CASE WHEN typ_participant = '"+ExecutionConstants.CENTRAL+"'")
		//.append("		 THEN cod_txn || 'C'")
		.append("		 THEN REGEXP_REPLACE(cod_txn, '[B]', 'D', 5, 1) || 'C'")  //BVT_ISSSUE_01
		.append("		 WHEN typ_participant = '"+ExecutionConstants.BANKSHARE+"'")
		//.append(" 		 THEN cod_txn || 'B'") 
		.append(" 		 THEN REGEXP_REPLACE(cod_txn, '[B]', 'D', 5, 1) || 'B'") //BVT_ISSSUE_01
		.append("		 WHEN typ_participant = '"+ExecutionConstants.TREASURY+"'")
		//.append("		 THEN cod_txn || 'T'")
		.append("		 THEN REGEXP_REPLACE(cod_txn, '[B]', 'D', 5, 1) || 'T'") //BVT_ISSSUE_01
		//.append("		 ELSE cod_txn || 'A' END)cod_txn,")
		.append("		 ELSE REGEXP_REPLACE(cod_txn, '[B]', 'D', 5, 1) || 'A' END)cod_txn,"); //BVT_ISSSUE_01
		//NED_ISSUE FIXED STARTS
		/*.append("	  currency cod_ccy, 'DEBIT' txn_type, DAT_POSTING ")
		.append("     FROM opool_txnpool_bvtdiff bvtdiff, olm_glmst gl ")
		.append("    WHERE id_bvt = "+bvtId)*/
		Strquery.append("	  currency cod_ccy, 'DEBIT' txn_type, bvtdiff.dat_posting, NVL((hdr.DAT_LASTPOSTING+1),hdr.DAT_start) DAT_POSTING_START, bvtdiff.typ_interest ");
		if(flg_newAccountingentry)
		{	
			Strquery.append(" , bvtdiff.SOURCE_POOL_ID, null flg_acc_accrual_pricing_feed " );	 // NEW_ACCOUNTING_ENTRY_FORMAT//SUP_JPMCIS17438
		}else
		{
			Strquery.append(" , bvtdiff.id_pool SOURCE_POOL_ID, null flg_acc_accrual_pricing_feed " );	 // NEW_ACCOUNTING_ENTRY_FORMAT//SUP_JPMCIS17438
		}
		
		Strquery.append(" ,( CASE WHEN (SUBSTR (cod_txn, 5, 1) IN ('D', 'B')) THEN nvl(AMT_DR_DIFF_CENTRAL_CCY,0) ELSE 0 END ) amt_calculated_central_ccy, RATE_EXRATE_CENTRAL_CCY,hdr.dat_post_settlement  ");//Reallocate to Self//SUP_ISS_IV_7296//SUP_ISS_IV_7602
		//SUP_ENH_III_8106 starts
		//Strquery.append("     FROM opool_txnpool_bvtdiff bvtdiff, olm_glmst gl, opool_txnpoolhdr hdr ");
		Strquery.append("     FROM opool_txnpool_bvtdiff bvtdiff, opool_txnpoolhdr hdr ");
		//SUP_ENH_III_8106 ends
		if(allowIntTyp)
			Strquery.append("     , orate_interest_type int_type ");
		
		Strquery.append("    WHERE bvtdiff.id_bvt = "+bvtId);
		//NED_ISSUE FIXED ENDS
		Strquery.append("      AND bvtdiff.cod_gl = '"+ client+"' ")
		.append("      AND typ_participant <> 'POOL' ")
		.append("      AND amt_dr_diff <> 0 ")
		.append("      AND cod_txn IS NOT NULL ")
		//SUP_ENH_III_8106 starts
		//.append("      AND bvtdiff.cod_gl = gl.cod_gl ")
		//.append("      AND gl.FLG_BACKVALUE_POSTING = '"+ExecutionConstants.YES+"'")
		//SUP_ENH_III_8106 ends
		//NED_ISSUE FIXED STARTS
		.append("      AND bvtdiff.id_pool = hdr.id_pool")
		.append("      and bvtdiff.NBR_PROCESSID = hdr.NBR_PROCESSID")
		.append("      and bvtdiff.DAT_BUSINESS_POOLRUN = hdr.DAT_BUSINESS_POOLRUN");
		//NED_ISSUE FIXED ENDS
		if(allowIntTyp){    //Sandeep changes for ned issue 
			Strquery.append("      AND bvtdiff.typ_interest = int_type.typ_interest(+) ")
			.append("      AND ('N' = int_type.flg_interestypeas_fee OR int_type.flg_interestypeas_fee IS NULL) ");
		}
		Strquery.append(" ORDER BY id_pool, participant_id, cod_txn, dat_posting,SOURCE_POOL_ID ");// ISSUE_2416
				
		if (logger.isDebugEnabled()){
			//logger.debug("SELECT QUERY4 FOR DEBIT IN processCycleWiseBvtTXNCodes :"+ Strquery.toString());
			logger.debug("SELECT QUERY4 FOR DEBIT IN processCycleWiseBvtTXNCodesForPool :"+ Strquery.toString());
		}
		
		//populateCycleWiseBvtAccountInfo(Strquery.toString(),bvtId,businessDate, client, insertAccountDtls,updateAccountDtls,
		  populateCycleWiseBvtAccountInfoForPool(Strquery.toString(),bvtId,businessDate, client, insertAccountDtls,updateAccountDtls,
				insertAccrualSummary, insertPostingSummary, updateAccrualSummary, updatePostingSummary,
				insertUnpostedAmt,selectAccountDtls ,selectAccrualSummary ,selectPostingSummary,updateUnpostedAmt);
		
		if (logger.isDebugEnabled()){
			//logger.debug("Leaving processCycleWiseBvtTXNCodes");
			logger.debug("Leaving processCycleWiseBvtTXNCodesForPool");
		}
	}
	//ENH_10.3_154 Ends
	
	/**
	 * This method prepare the select query and call the  populateBvtAccountInfo method  to execute the same.
	 * @param bvtId
	 * @param businessDate
	 * @param client
	 * @param insertAccountDtls
	 * @param updateAccountDtls
	 * @param insertAccrualSummary
	 * @param insertPostingSummary
	 * @param updateAccrualSummary
	 * @param updatePostingSummary
	 * @param insertUnpostedAmt
	 * @param selectAccountDtls
	 * @param selectAccrualSummary
	 * @param selectPostingSummary
	 * @throws LMSException
	 */
	private void processBvtTXNCodes( String bvtId,Date businessDate, String client, PreparedStatement insertAccountDtls,
			PreparedStatement updateAccountDtls, PreparedStatement insertAccrualSummary, 
			PreparedStatement insertPostingSummary, PreparedStatement updateAccrualSummary, 
			PreparedStatement updatePostingSummary,PreparedStatement insertUnpostedAmt,
			PreparedStatement selectAccountDtls ,PreparedStatement selectAccrualSummary ,
			//ENH_IS1297 RETROFIT STARTS
			PreparedStatement selectPostingSummary,PreparedStatement updateUnpostedAmt,PreparedStatement updateUnpostedAmtCentralCcy ) throws LMSException//Reallocate to Self
			//ENH_IS1297 RETROFIT ENDS
	{
		boolean allowIntTyp = ExecutionConstants.FLG_INTACC_POST_INTTYP ; // ENH_10.3_153
		
		boolean flg_newAccountingentry = ExecutionConstants.FlG_NEW_ACCOUNTING_ENTRY_FORMAT; // NEW_ACCOUNTING_ENTRY_FORMAT
		
		if (logger.isDebugEnabled()){
			logger.debug("processBvtTXNCodes entering  ");
			logger.debug("allowIntTyp  "+ allowIntTyp);
			logger.debug("flg_newAccountingentry  "+ flg_newAccountingentry);
		}
		
		
		StringBuilder Strquery = new StringBuilder(0);
		Strquery.append(" SELECT   TYP_PARTICIPANT, (CASE ")
		.append("              WHEN (SUBSTR (cod_txn, 5, 1) IN ('C', 'B')) ")
		.append("                 THEN amt_cr_interest_diff ")
		.append("              ELSE 0 ")
		.append("           END ")
		.append("          ) amt_calculated, ")
		.append("          currency cod_ccy, tbl.dat_business_poolrun busdt, tbl.id_pool, tbl.flg_realpool, ")//SUP_JPMCIS17438
		//ENH_10.3_154 Starts
		//.append("          cod_txn ||'I' COD_TXN, participant_id, 'CREDIT' txn_type ")
				//JPMC_312 ISSUE FIXED START
				.append("          REGEXP_REPLACE(cod_txn, '[B]', 'C', 5, 1) ||'I' COD_TXN, participant_id, 'CREDIT' txn_type, tbl.DAT_POSTING ");//SUP_JPMCIS17438
				//JPMC_312 ISSUE FIXED ENDS
		//ENH_10.3_154 Ends
		
		Strquery.append(", typ_interest ") ; // ENH_10.3_153
		//SUP_JPMCIS17438 STARTS
		Strquery.append(", tbl.id_pool SOURCE_POOL_ID,hdr.flg_acc_accrual_pricing_feed, 0 amt_calculated_central_ccy ") ; //NEW_ACCOUNTING_ENTRY_FORMAT//Reallocate to Self
		Strquery.append("     FROM opool_txnpool_bvtdiff_int tbl,opool_txnpoolhdr hdr ")// This table is like individual interest amounts are populated.
				.append("    WHERE ")		
				.append("    tbl.id_bvt              =hdr.id_bvt ")
				.append("    and tbl.nbr_processid= hdr.nbr_processid ")
				.append("    and tbl.id_pool= hdr.id_pool ")
				.append("    and tbl.dat_business_poolrun= hdr.dat_business_poolrun ")
				.append("    and tbl.id_bvt = " + bvtId)
				.append("      AND typ_participant = 'ACCOUNT' and ACCOUNT_ACCRUAL = 'Y'  ")
				.append("      AND tbl.cod_gl = '"+ client+"' ")
				.append("      AND amt_cr_interest_diff <> 0 ")
		//ENH_10.3_154 Starts
		//.append(" ORDER BY id_pool, participant_id, cod_txn ");
		//.append(" ORDER BY id_pool, participant_id, cod_txn, typ_interest, dat_posting , SOURCE_POOL_ID "); //SIT10.4_BETA2 fix for BVI - typ_interest added
		.append(" ORDER BY tbl.id_pool, participant_id, cod_txn, SOURCE_POOL_ID,typ_interest,tbl.dat_posting "); //JPMC_IS FIXED //ISSUE_2416 Fix
		//SUP_JPMCIS17438 ENDS
		//ENH_10.3_154 Ends
		if (logger.isDebugEnabled()){
			logger.debug("SELECT QUERY1 FOR CREDIT IN processBvtTXNCodes opool_txnpool_bvtdiff_int :"+ Strquery.toString());
		}
		populateBvtAccountInfo(Strquery.toString(),bvtId,businessDate, client, insertAccountDtls,updateAccountDtls, 
				insertAccrualSummary, insertPostingSummary, updateAccrualSummary, updatePostingSummary,
				//ENH_IS1297 RETROFIT STARTS
				insertUnpostedAmt,selectAccountDtls ,selectAccrualSummary ,selectPostingSummary,updateUnpostedAmt,updateUnpostedAmtCentralCcy);//Reallocate to Self
				//ENH_IS1297 RETROFIT ENDS
		
		Strquery.replace(0,Strquery.length(),"");
		Strquery.append(" SELECT  TYP_PARTICIPANT, (CASE ")
		 .append("              WHEN (SUBSTR (cod_txn, 5, 1) IN ('D', 'B')) ")
		 .append("                 THEN amt_dr_interest_diff ")
		 .append("              ELSE 0 ")
		 .append("           END ")
		 .append("          ) amt_calculated, ")
		 .append("          currency cod_ccy, tbl.dat_business_poolrun busdt, tbl.id_pool, tbl.flg_realpool, ")//SUP_JPMCIS17438
 		 //ENH_10.3_154 Starts
		 //.append("          cod_txn ||'I' COD_TXN, participant_id, 'DEBIT' txn_type ")
				//JPMC_312 ISSUE FIXED START
				.append("          REGEXP_REPLACE(cod_txn, '[B]', 'D', 5, 1) ||'I' COD_TXN, participant_id, 'DEBIT' txn_type, tbl.DAT_POSTING ");//SUP_JPMCIS17438
				//JPMC_312 ISSUE FIXED END
		 //ENH_10.3_154 Ends
		
		Strquery.append(", typ_interest ") ; // ENH_10.3_153
		//SUP_JPMCIS17438 STARTS
		Strquery.append(", tbl.id_pool SOURCE_POOL_ID,hdr.flg_acc_accrual_pricing_feed, 0 amt_calculated_central_ccy ") ; // NEW_ACCOUNTING_ENTRY_FORMAT//Reallocate to Self
		Strquery .append("     FROM opool_txnpool_bvtdiff_int tbl,opool_txnpoolhdr hdr ")
		 .append("    WHERE ")
		 .append("    tbl.id_bvt              =hdr.id_bvt ")
		.append("    and tbl.nbr_processid= hdr.nbr_processid ")
		.append("    and tbl.id_pool= hdr.id_pool ")
		.append("    and tbl.dat_business_poolrun= hdr.dat_business_poolrun ")
		.append("    and tbl.id_bvt = " + bvtId)
		 .append("      AND typ_participant = 'ACCOUNT' and ACCOUNT_ACCRUAL = 'Y'")
		 .append("      AND tbl.cod_gl = '"+ client+"' ")
		 .append("      AND amt_dr_interest_diff <> 0 ")
		 //ENH_10.3_154 Starts
		 //.append(" ORDER BY id_pool, participant_id, cod_txn ");
		 // NEW_ACCOUNTING_ENTRY_FORMAT
		 //.append(" ORDER BY id_pool, participant_id, cod_txn, typ_interest, dat_posting , SOURCE_POOL_ID "); //SIT10.4_BETA2 fix for BVI - typ_interest added
		 .append(" ORDER BY tbl.id_pool, participant_id, cod_txn, SOURCE_POOL_ID,typ_interest,tbl.dat_posting "); //JPMC_IS FIXED //ISSUE_2416 Fix
		 //ENH_10.3_154 Ends
		//SUP_JPMCIS17438 ENDS
		if (logger.isDebugEnabled()){
			logger.debug("SELECT QUERY2 FOR DEBIT IN processBvtTXNCodes  opool_txnpool_bvtdiff_int :"+ Strquery.toString());
		}
		populateBvtAccountInfo(Strquery.toString(),bvtId,businessDate, client, insertAccountDtls,updateAccountDtls, 
				insertAccrualSummary, insertPostingSummary, updateAccrualSummary, updatePostingSummary,
				//ENH_IS1297 RETROFIT STARTS
				insertUnpostedAmt,selectAccountDtls ,selectAccrualSummary,
				selectPostingSummary, updateUnpostedAmt,updateUnpostedAmtCentralCcy);//Reallocate to Self
				//ENH_IS1297 RETROFIT STARTS
		
			
		Strquery.replace(0,Strquery.length(),"");
		/* // ENH_10.3_153 Starts */

		/*  ENH_10.3_153
		 *    opool_txnpool_bvtdiff - added source_pool_id, typ_interest as new columns. while getting the 
		 *                            allocated interest amount need to get sum of the interest amount
		 *                            interest type wise irrespective of source_pool_id(who ever assigned)
		 *                            For this the below query changes :
		 */
		
		Strquery.append(" SELECT DISTINCT  TYP_PARTICIPANT,   ") ;
			//I_REL_231 --Start
			/*if(allowIntTyp)
			{*/
				Strquery.append( "       SUM(( CASE ")
				.append("            		    WHEN (SUBSTR (cod_txn, 5, 1) IN ('C', 'B')) ")
				.append("               			THEN amt_cr_diff ")
				.append("            			ELSE 0 ")
				.append("         				END ")
				.append("        			 )) amt_calculated, ")
				.append("        		 dat_business_poolrun busdt, participant_id, id_pool, ") ;
			/*}
			
			else
			{
				Strquery.append( "          ( CASE ")
				.append("            		    WHEN (SUBSTR (cod_txn, 5, 1) IN ('C', 'B')) ")
				.append("               			THEN amt_cr_diff ")
				.append("            			ELSE 0 ")
				.append("         				END ")
				.append("        			 ) amt_calculated, ")
				.append("        		 dat_business_poolrun busdt, participant_id, id_pool, ") ;
			}*/
			//I_REL_231 --End
		/*  ENH_10.3_153 Ends */

//ENH_IS997 starts

		//CCB_SUP_IS760 Starts
		//for typ_participant=CENTRAL,TREASURY AND BANKSHARE append C,T,B in COD_TXN respectively 
		//.append("        flg_realpool,  COD_TXN, currency cod_ccy, 'CREDIT' txn_type ")
				//JPMC_312 ISSUE FIXED START
		Strquery.append("      	flg_realpool, ")
		.append("		(CASE WHEN typ_participant = '"+ExecutionConstants.CENTRAL +"'")
						.append("		 THEN REGEXP_REPLACE(cod_txn, '[B]', 'C', 5, 1) || 'C'")
		.append("		 WHEN typ_participant = '"+ExecutionConstants.BANKSHARE+"'")
						.append(" 		 THEN REGEXP_REPLACE(cod_txn, '[B]', 'C', 5, 1) || 'B'")
		.append("		 WHEN typ_participant = '"+ExecutionConstants.TREASURY+"'")
						.append("		 THEN REGEXP_REPLACE(cod_txn, '[B]', 'C', 5, 1) || 'T'")
						.append("		 ELSE REGEXP_REPLACE(cod_txn, '[B]', 'C', 5, 1) || 'A' END)cod_txn,")// for
				//JPMC_312 ISSUE FIXED END
		//ENH_10.3_154 Starts
		//.append("	  currency cod_ccy, 'CREDIT' txn_type ")
		.append("	  currency cod_ccy, 'CREDIT' txn_type, DAT_POSTING ");
		//ENH_10.3_154 Ends
		//TI_FEE_CHANGES STARTS
		Strquery.append(", diff.typ_interest ") ; // ENH_10.3_153
		
		if(flg_newAccountingentry)
			{ Strquery.append(", diff.SOURCE_POOL_ID , null flg_acc_accrual_pricing_feed") ; }// NEW_ACCOUNTING_ENTRY_FORMAT//SUP_JPMCIS17438
		else
			{ Strquery.append(", id_pool SOURCE_POOL_ID, null flg_acc_accrual_pricing_feed ") ; }//SUP_JPMCIS17438
		//CCB_SUP_IS760 Ends
//ENH_IS997 ends
		Strquery.append(" , sum( CASE WHEN (SUBSTR (cod_txn, 5, 1) IN ('C', 'B')) THEN nvl(AMT_CR_DIFF_CENTRAL_CCY,0) ELSE 0 END ) amt_calculated_central_ccy ");//Reallocate to Self//15.1_fix_defect_346 STARTS ////I_REL_231 
		Strquery.append("   FROM OPOOL_TXNPOOL_BVTDIFF diff");
		if(allowIntTyp)
		{
			Strquery.append(", ORATE_INTEREST_TYPE INT_TYPE ") ;
		}
		Strquery.append("  WHERE id_bvt = "+bvtId)
		.append("    AND cod_gl = '"+ client+"' ")
		.append("    AND typ_participant <> 'POOL' ")
		.append("    AND amt_cr_diff <> 0 ")
		//NO TNX CODE FOR BANK SHARE 
		.append(" 	 AND COD_TXN IS NOT NULL ") ;
		if(allowIntTyp)
		{
			//Strquery.append(" and ('Y' = int_type.flg_only_pool_level or int_type.flg_only_pool_level is null) ") 			
			Strquery.append(" and diff.typ_interest = int_type.typ_interest(+) ") 
			//I_REL_231 --Start
			.append(" and ('N' = int_type.flg_interestypeas_fee or int_type.flg_interestypeas_fee is null) ") ;
			/*Strquery.append(" GROUP BY PARTICIPANT_ID,  TYP_PARTICIPANT,  DAT_BUSINESS_POOLRUN, ID_POOL, FLG_REALPOOL,  ")
			.append(" 		   CURRENCY, COD_TXN, diff.TYP_INTEREST,  DAT_POSTING , SOURCE_POOL_ID") */
			
		}
			Strquery.append(" GROUP BY PARTICIPANT_ID,  TYP_PARTICIPANT,  DAT_BUSINESS_POOLRUN, ID_POOL, FLG_REALPOOL,  ")
			.append(" 		   CURRENCY, COD_TXN, diff.TYP_INTEREST,  DAT_POSTING , SOURCE_POOL_ID") 
			.append("  having (SUM(CASE WHEN (SUBSTR (cod_txn, 5, 1) IN ('C', 'B')) THEN amt_cr_diff Else 0 END ))<>0 ");
		//I_REL_231 --End
		//TI_FEE_CHANGES ENDS
		//ENH_10.3_154 Starts
		//.append(" ORDER BY id_pool, participant_id, cod_txn ");

		Strquery.append(" ORDER BY id_pool, participant_id, cod_txn, SOURCE_POOL_ID,typ_interest, dat_posting "); //JPMC_IS FIXED //ISSUE_2416 Fix
		//ENH_10.3_154 Ends
		
		if (logger.isDebugEnabled()){
			logger.debug("SELECT QUERY3 FOR CREDIT IN processBvtTXNCodes opool_txnpool_bvtdiff :"+ Strquery.toString());
		}
		populateBvtAccountInfo(Strquery.toString(),bvtId,businessDate, client, insertAccountDtls,updateAccountDtls,
				insertAccrualSummary, insertPostingSummary, updateAccrualSummary, updatePostingSummary,
				//ENH_IS1297 RETROFIT STARTS
				insertUnpostedAmt,selectAccountDtls ,selectAccrualSummary ,selectPostingSummary,updateUnpostedAmt,updateUnpostedAmtCentralCcy);//Reallocate to Self
				//ENH_IS1297 RETROFIT ENDS
		
		
		
		Strquery.replace(0,Strquery.length(),"");
		/* // ENH_10.3_153 Starts */

		Strquery.append(" SELECT  DISTINCT TYP_PARTICIPANT, ") ;
			//I_REL_231 --Start
			/*if(allowIntTyp)
			{*/
				Strquery.append("  SUM((CASE   WHEN (SUBSTR (cod_txn, 5, 1) IN ('D', 'B')) ")
						.append("      	          THEN amt_dr_diff ")
						.append("              ELSE 0 ")
						.append("       END ")
						.append("     )) amt_calculated, ")
						.append("  dat_business_poolrun busdt, bvtdiff.participant_id, id_pool, ") ;
			/*}
			else
			{
				Strquery.append("     (CASE   WHEN (SUBSTR (cod_txn, 5, 1) IN ('D', 'B')) ")
						.append("                        THEN amt_dr_diff ")
						.append("              		  ELSE 0 ")
						.append("              END ")
						.append("          	    ) amt_calculated, ")
						.append("  dat_business_poolrun busdt, bvtdiff.participant_id, id_pool, ") ;
			}*/
			//I_REL_231 --End
			/* // ENH_10.3_153 Ends */
//ENH_IS997 starts
		//CCB_SUP_IS760 Starts		
		//for typ_participant=CENTRAL,TREASURY AND BANKSHARE append C,T,B in COD_TXN respectively
		//.append("          flg_realpool, cod_txn ||'A' COD_TXN , currency cod_ccy, 'DEBIT' txn_type ")
				//JPMC_312 ISSUE FIXED START
		Strquery.append("          		 flg_realpool, ")
		.append("		(CASE WHEN typ_participant = '"+ExecutionConstants.CENTRAL+"'")
						.append("		 THEN REGEXP_REPLACE(cod_txn, '[B]', 'D', 5, 1) || 'C'")
		.append("		 WHEN typ_participant = '"+ExecutionConstants.BANKSHARE+"'")
						.append(" 		 THEN REGEXP_REPLACE(cod_txn, '[B]', 'D', 5, 1) || 'B'")
		.append("		 WHEN typ_participant = '"+ExecutionConstants.TREASURY+"'")
						.append("		 THEN REGEXP_REPLACE(cod_txn, '[B]', 'D', 5, 1) || 'T'")
						.append("		 ELSE REGEXP_REPLACE(cod_txn, '[B]', 'D', 5, 1) || 'A' END)cod_txn,")
				//JPMC_312 ISSUE FIXED ENDS
		//ENH_10.3_154 Starts
		//.append("	  currency cod_ccy, 'DEBIT' txn_type ")
		.append("	  currency cod_ccy, 'DEBIT' txn_type, DAT_POSTING ") ;
		//ENH_10.3_154 Ends
		//TI_FEE_CHANGES STARTS
		Strquery.append(", bvtdiff.typ_interest ") ;// ENH_10.3_153
		if(flg_newAccountingentry)
		{ Strquery.append(", bvtdiff.SOURCE_POOL_ID, null flg_acc_accrual_pricing_feed") ; }// NEW_ACCOUNTING_ENTRY_FORMAT//SUP_JPMCIS17438
	else
		{ Strquery.append(", id_pool SOURCE_POOL_ID, null flg_acc_accrual_pricing_feed ") ; }//SUP_JPMCIS17438
		
		//CCB_SUP_IS760 Ends
//ENH_IS997 ends
		Strquery.append(" ,sum( CASE WHEN (SUBSTR (cod_txn, 5, 1) IN ('D', 'B')) THEN nvl(AMT_DR_DIFF_CENTRAL_CCY,0) ELSE 0 END ) amt_calculated_central_ccy ");//Reallocate to Self//15.1_fix_defect_346 STARTS //I_REL_231 
		Strquery.append(" FROM opool_txnpool_bvtdiff bvtdiff ");
		if(allowIntTyp)
		{
			Strquery.append(", ORATE_INTEREST_TYPE INT_TYPE ") ;
		}
		Strquery.append("    WHERE id_bvt = "+bvtId)
		.append("      AND bvtdiff.cod_gl = '"+ client+"' ")
		.append("      AND typ_participant <> 'POOL' ")
		.append("      AND amt_dr_diff <> 0 ")
		.append("      AND cod_txn IS NOT NULL ") ;
		
		if(allowIntTyp)
		{
			//Strquery.append("      and ('Y' = int_type.flg_only_pool_level or int_type.flg_only_pool_level is null) ") 
			Strquery.append("      and bvtdiff.typ_interest = int_type.typ_interest(+)  ") 
			//I_REL_231 --Start
			.append("      and ('N' = int_type.flg_interestypeas_fee or int_type.flg_interestypeas_fee is null) ");
			/*.append(" GROUP BY PARTICIPANT_ID,  TYP_PARTICIPANT,  DAT_BUSINESS_POOLRUN, ID_POOL, FLG_REALPOOL,  ")
		    .append(" 		   CURRENCY, COD_TXN, bvtdiff.TYP_INTEREST, DAT_POSTING , SOURCE_POOL_ID") ;*/// ENH_10.3_153//GROUP_BY_ISS
		}
			Strquery.append(" GROUP BY PARTICIPANT_ID,  TYP_PARTICIPANT,  DAT_BUSINESS_POOLRUN, ID_POOL, FLG_REALPOOL,  ")
		    .append(" 		   CURRENCY, COD_TXN, bvtdiff.TYP_INTEREST, DAT_POSTING , SOURCE_POOL_ID ")
		    .append(" 		    having (SUM(CASE WHEN (SUBSTR (cod_txn, 5, 1) IN ('D', 'B')) THEN amt_dr_diff Else 0 END ))<>0 ");
			//I_REL_231 --End	
		//TI_FEE_CHANGES ENDS
		//ENH_10.3_154 Starts
		//.append(" ORDER BY id_pool, participant_id, cod_txn ");
		Strquery.append(" ORDER BY id_pool, participant_id, cod_txn, SOURCE_POOL_ID,typ_interest, dat_posting"); //JPMC_IS FIXED //ISSUE_2416 Fix
		//ENH_10.3_154 Ends
				
		if (logger.isDebugEnabled()){
			logger.debug("SELECT QUERY4 FOR DEBIT IN processBvtTXNCodes opool_txnpool_bvtdiff :"+ Strquery.toString());
		}
		
		populateBvtAccountInfo(Strquery.toString(),bvtId,businessDate, client, insertAccountDtls,updateAccountDtls,
				insertAccrualSummary, insertPostingSummary, updateAccrualSummary, updatePostingSummary,
				//ENH_IS1297 RETROFIT STARTS
				insertUnpostedAmt,selectAccountDtls ,selectAccrualSummary ,selectPostingSummary,updateUnpostedAmt,updateUnpostedAmtCentralCcy);//Reallocate to Self
				//ENH_IS1297 RETROFIT ENDS
		
		if (logger.isDebugEnabled()){
			logger.debug("Leaving ");
		}
	}
	

	/**This method execute the query and set data in Do Object and call the populateBvtAccountDtls , populateBVTAccuralSummary ,populateBVTPostingSummary
	 * @param strQuery
	 * @param bvtId
	 * @param businessDate
	 * @param client
	 * @param insertAccountDtls
	 * @param updateAccountDtls
	 * @param insertAccrualSummary
	 * @param insertPostingSummary
	 * @param updateAccrualSummary
	 * @param updatePostingSummary
	 * @param insertUnpostedAmt
	 * @param selectAccountDtls
	 * @param selectAccrualSummary
	 * @param selectPostingSummary
	 * @throws LMSException
	 */
	private void populateBvtAccountInfo(String strQuery,String bvtId, Date businessDate, String client, 
			PreparedStatement insertAccountDtls, PreparedStatement updateAccountDtls, 
			PreparedStatement insertAccrualSummary, PreparedStatement insertPostingSummary, 
			PreparedStatement updateAccrualSummary, PreparedStatement updatePostingSummary,
			PreparedStatement insertUnpostedAmt,PreparedStatement selectAccountDtls ,
			//ENH_IS1297 RETROFIT STARTS
			PreparedStatement selectAccrualSummary ,PreparedStatement selectPostingSummary,PreparedStatement updateUnpostedAmt,PreparedStatement updateUnpostedAmtCentralCcy//Reallocate to Self
			//ENH_IS1297 RETROFIT ENDS
		) throws LMSException
	{
		if (logger.isDebugEnabled()){
			logger.debug("Entering ");
		}
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		Statement stmt = null;
		PreparedStatement pStmt = null;
		/** ****** JTEST FIX end **************  on 14DEC09*/
		Connection conn = null;
		ResultSet res = null;
		boolean recordExists = false,firstTime = true;
		AccountInfoDO accountInfoDo = null;
		java.sql.Date businessdate = new java.sql.Date(businessDate.getTime());
		java.util.Date dateList[]= null;
		String txnCode = "";
		String typEntity="";//Intercompany BVT Changes
		try
		{
			try 
			{
				conn = LMSConnectionUtility.getConnection();
			} catch (NamingException e) 
			{
				logger.fatal(" Error occured while getting connection :"+e.getMessage());
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
				//Intellect Liquidity DB Failover Solution starts
				throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE," Error occured while getting connection : ", e);
				//Intellect Liquidity DB Failover Solution ends
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
			}
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			stmt = conn.createStatement();
//			res = stmt.executeQuery(strQuery);
			if(logger.isDebugEnabled()){
			logger.debug("Query " + strQuery) ;
			}
			pStmt = conn.prepareStatement(strQuery);
			res = pStmt.executeQuery();
			/** ****** JTEST FIX end **************  on 14DEC09*/
			accountInfoDo = new AccountInfoDO();
            //SUP_IS1587 Starts
			String flag_bvt_cycle = ExecutionConstants.FLAG_BVT_CYCLE;
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("flag_bvt_cycle:::"+flag_bvt_cycle);
	        boolean flg_newAccountingentry = ExecutionConstants.FlG_NEW_ACCOUNTING_ENTRY_FORMAT;
	        if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
	        	logger.debug("flg_newAccountingentry:::"+flg_newAccountingentry);
	        //SUP_IS1587 Ends	
	        
	        String interestTyp = null ;// ENH_10.3_153
	        // ENH_JPMC_TAXBVT Started
			// Create a HashMap to put the information to call the tax engine for the Inter company loan
			HashMap taxDetailsMap = new HashMap();
			
			// Create a ArrayList to add the information to call the tax engine for the Inter company loan
			ArrayList loanIdList = new ArrayList();

			// Define a boolean to check if Tax Calculator need to call
			boolean calTaxCalculator = false;			
			// ENH_JPMC_TAXBVT Ended
			String strPoolId = null ; // ENH_10.3_153
			String source_PoolId = "NA"; //NEW_ACCOUNTING_ENTRY_FORMAT
	        
			while(res.next())
			{
				txnCode = setTxnCode(res.getString("COD_TXN"),res.getString("TXN_TYPE"));
				if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
					logger.debug("txnCode::: is >>> "+txnCode);
					logger.debug("amt_calculated_central_ccy::: is >>> "+res.getBigDecimal("amt_calculated_central_ccy"));//Reallocate to Self
				}
				recordExists = true;
				//When  frist time control enters into loop ,just populate Do object  
				typEntity=res.getString("TYP_PARTICIPANT");//Intercompany BVT Changes
				//ENH_10.3_154 Starts
				strPoolId =res.getString("ID_POOL");
				 if(!"NONPOOL_ACCT".equals(strPoolId) && !ExecutionConstants.INCOLOAN.equals(typEntity))
				 {
					 source_PoolId =res.getString("SOURCE_POOL_ID"); //NEW_ACCOUNTING_ENTRY_FORMAT
				 }
				//ENH_10.3_154 Ends
				if(!firstTime)
				{
					//ENH_10.3_154 Starts
					if(!strPoolId.equalsIgnoreCase("NONPOOL_ACCT") 
							&& !typEntity.equalsIgnoreCase(InterCoConstants.INCOLOAN)) 
									
					{
						if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y"))
						{
							if(res.getDate("DAT_POSTING").compareTo(businessdate) < 0)
							{
								dateList = (java.util.Date[]) postingDatesCache.get(res.getString("PARTICIPANT_ID"));
								txnCode = txnCode + ExecutionConstants.BVT_POST_CYCLE;
							}
							else
							{
								dateList = (java.util.Date[]) postingDatesCacheCurr.get(res.getString("ID_POOL"));
								txnCode = txnCode + ExecutionConstants.NORMAL_POST_CYCLE;
							}
						}
					}
					//ENH_10.3_154 Ends
//Retro_Mashreq
					//SUPNBAD_4743 STARTS
					if("NONPOOL_ACCT".equals(strPoolId) || typEntity.equalsIgnoreCase(InterCoConstants.INCOLOAN)){  //SUP_ISS_III_7305
						if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y"))
						{
							if(res.getDate("DAT_POSTING").compareTo(businessdate) < 0)
							{
								//dateList = (java.util.Date[]) postingDatesCache.get(res.getString("PARTICIPANT_ID"));
								txnCode = txnCode + ExecutionConstants.BVT_POST_CYCLE;
							}
							else
							{
								//dateList = (java.util.Date[]) postingDatesCacheCurr.get(res.getString("ID_POOL"));
								txnCode = txnCode + ExecutionConstants.NORMAL_POST_CYCLE;
							}
						}
					}
					//SUPNBAD_4743 ENDS
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("FLG_SA_INTEREST_TYPE : "  + ExecutionConstants.FLG_SA_INTEREST_TYPE + "strPoolId" + strPoolId) ;
					if(ExecutionConstants.FLG_SA_INTEREST_TYPE && "NONPOOL_ACCT".equals(strPoolId)) 
					{	
						if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
							logger.debug("strPoolId " + strPoolId + ", typEntity : " + typEntity) ;
					
						interestTyp = res.getString("TYP_INTEREST") ; 
						if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
							logger.debug("interestTyp FLG_SA_INTEREST_TYPE  is : "  + interestTyp + "firstTime" + firstTime) ;
//Retro_Mashreq
						//SUPNBAD_4743 STARTS
						if(interestTyp==null || interestTyp.equalsIgnoreCase("I"))
						{
							// DO NOTHING
						}
						else
						{
							txnCode = txnCode + interestTyp ;
							if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
								logger.debug("interestTyp is FLG_SA_INTEREST_TYPE is ... : "  + interestTyp + "txnCode..." + txnCode) ;
						//	accountInfoDo.setCOD_TXN(txnCode); Nitin
						}
						
						/*if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y"))
						{
							if(res.getDate("DAT_POSTING").compareTo(businessdate) < 0)
							{
								//dateList = (java.util.Date[]) postingDatesCache.get(res.getString("PARTICIPANT_ID"));
								txnCode = txnCode + ExecutionConstants.BVT_POST_CYCLE + interestTyp;
							}
							else
							{
								//dateList = (java.util.Date[]) postingDatesCacheCurr.get(res.getString("ID_POOL"));
								txnCode = txnCode + ExecutionConstants.NORMAL_POST_CYCLE + interestTyp;
							}
						}*/
						//SUPNBAD_4743 STARTS
					}
					else if (!"NONPOOL_ACCT".equals(strPoolId) && !ExecutionConstants.INCOLOAN.equals(typEntity))
					{
						if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
							logger.debug("strPoolId " + strPoolId + ", typEntity : " + typEntity) ;
						if(ExecutionConstants.FLG_INTACC_POST_INTTYP) 
						{	
							interestTyp = res.getString("TYP_INTEREST") ; 
							if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
								logger.debug("interestTyp  >>>>> : "  + interestTyp) ;
							if(interestTyp==null || interestTyp.equalsIgnoreCase("I"))
							{
								// DO NOTHING
							}
							else
							{
								txnCode = txnCode + interestTyp ;
								if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
									logger.debug("interestTyp  >>>>>:::set in  accountInfoDo"  + interestTyp) ;
								//accountInfoDo.setCOD_TXN(txnCode);
							}
						}
					}	
					
					if (logger.isDebugEnabled())
					logger.debug("txnCode "  + txnCode + "accountInfoDo.getCOD_TXN() " + " PARTICIPANT_ID "+res.getString("PARTICIPANT_ID")+ "accountInfoDo.getPARTICIPANT_ID() " + " strPoolId "  + strPoolId +  "accountInfoDo.getID_POOL() "+ accountInfoDo.getID_POOL() + " source_PoolId" + source_PoolId + " SOURCE_POOL_ID() "+ accountInfoDo.getSOURCE_POOL_ID() ) ;
					
					if(txnCode.equalsIgnoreCase(accountInfoDo.getCOD_TXN()) &&
									res.getString("PARTICIPANT_ID").equalsIgnoreCase(accountInfoDo.getPARTICIPANT_ID())
									&& strPoolId.equalsIgnoreCase(accountInfoDo.getID_POOL())
									&& source_PoolId.equalsIgnoreCase(accountInfoDo.getSOURCE_POOL_ID()) //NEW_ACCOUNTING_ENTRY_FORMAT
									)
					{
						if (logger.isDebugEnabled())
						{
							logger.debug("accountInfoDo.getPARTICIPANT_ID()" +accountInfoDo.getPARTICIPANT_ID());
							logger.debug("accountInfoDo.getCOD_TXN()"+accountInfoDo.getCOD_TXN());
							logger.debug("accountInfoDo.getID_POOL()" +accountInfoDo.getID_POOL());
						}
						accountInfoDo.setDAT_BUSINESS(res.getDate("BUSDT"));
						accountInfoDo.setAMT_CALCULATED(res.getBigDecimal("AMT_CALCULATED"));
						accountInfoDo.setAMT_CALCULATED_CENTRAL_CCY(res.getBigDecimal("amt_calculated_central_ccy"));//Reallocate to Self
						if (logger.isDebugEnabled()){
							logger.debug("res.getBigDecimal(AMT_CALCULATED)" +res.getBigDecimal("AMT_CALCULATED"));
							logger.debug("accountInfoDo.getAMT_CALCULATED " +accountInfoDo.getAMT_CALCULATED());
							//Reallocate to Self
							logger.debug("res.getBigDecimal(amt_calculated_central_ccy) " +res.getBigDecimal("amt_calculated_central_ccy"));
							logger.debug("accountInfoDo.getAMT_CALCULATED_CENTRAL_CCY " +accountInfoDo.getAMT_CALCULATED_CENTRAL_CCY());
							//Reallocate to Self
						}
						//Set AMT_CAL_SUM = AMT_CAL_SUM + AMT_CALCULATED
						accountInfoDo.setAMT_CAL_SUM(res.getBigDecimal("AMT_CALCULATED").add(accountInfoDo.getAMT_CAL_SUM()));
						accountInfoDo.setAMT_CAL_SUM_CENTRAL_CCY(res.getBigDecimal("amt_calculated_central_ccy").add(accountInfoDo.getAMT_CAL_SUM_CENTRAL_CCY()));//Reallocate to Self
						//CAll populateBvtAccountDtls()
						if (logger.isDebugEnabled()){
							logger.debug("accountInfoDo.getAMT_CAL_SUM " +accountInfoDo.getAMT_CAL_SUM());
							logger.debug("accountInfoDo.getAMT_CAL_SUM_CENTRAL_CCY " +accountInfoDo.getAMT_CAL_SUM_CENTRAL_CCY());//Reallocate to Self
						}						
						
						populateBvtAccountDtls(businessDate, client, insertAccountDtls, updateAccountDtls,accountInfoDo,selectAccountDtls);
						continue ;
					}
					else
					{
						if (logger.isDebugEnabled()){
							logger.debug("inside else for accural and posting code txn is " +accountInfoDo.getCOD_TXN());
						}						
						//ENH_IS1297 RETROFIT STARTS
						/** ****** JTEST FIX start ************  on 14DEC09*/
//						populateBVTAccuralSummary(businessdate,insertAccrualSummary,updateAccrualSummary,insertUnpostedAmt,accountInfoDo,selectAccrualSummary,updateUnpostedAmt);
						populateBVTAccuralSummary(businessdate,insertAccrualSummary,updateAccrualSummary,insertUnpostedAmt,accountInfoDo,selectAccrualSummary,updateUnpostedAmt,updateUnpostedAmtCentralCcy, conn);//Reallocate to Self
						/** ****** JTEST FIX end **************  on 14DEC09*/
						populateBVTPostingSummary(businessdate,insertPostingSummary,updatePostingSummary,accountInfoDo,selectPostingSummary);
						//ENH_IS1297 RETROFIT ENDS
						
						// ENH_JPMC_TAXBVT Started
						//	flgPostingToBeDone = checkPostingToBeDone(businessdate, accountInfoDo);	
						if(logger.isDebugEnabled())
						{
							logger.debug(" **** Populating TAX LIST for.... ****");
							logger.debug("typEntity :::: " + typEntity);
							logger.debug("businessDate :::: " + businessDate);
							logger.debug("postingDate :::: " + accountInfoDo.getDAT_POSTING().toString());
							//logger.debug("flgPostingToBeDone :::: " + flgPostingToBeDone);
							//logger.debug("partyEndDate :::: " + accountInfoDo.getPARTY_DAT_END().toString());
						}
						
						//if(typEntity.equalsIgnoreCase(InterCoConstants.INCOLOAN) && (businessDate.toString().equalsIgnoreCase(accountInfoDo.getDAT_POSTING().toString())
						//|| accountInfoDo.getPARTY_DAT_END().toString().equalsIgnoreCase(businessDate.toString())) && flgPostingToBeDone)
						if(typEntity.equalsIgnoreCase(InterCoConstants.INCOLOAN) && businessDate.compareTo(accountInfoDo.getDAT_POSTING()) >= 0)
						{					
							calTaxCalculator = true;
							HashMap interestDetailsMap = new HashMap();
							
							// Put the loan id and corresponding interest in the interest Details Map
							interestDetailsMap.put(InterCoConstants.INCOLOAN, accountInfoDo.getPARTICIPANT_ID());
							interestDetailsMap.put(InterCoConstants.AMOUNT, accountInfoDo.getAMT_POSTED());
							interestDetailsMap.put(InterCoConstants.COD_TXN, accountInfoDo.getCOD_TXN());  //ENH_ICLTAX

							// Put the interest details Map in loan Id list ArrayList
							loanIdList.add(interestDetailsMap);
						}
						//	ENH_JPMC_TAXBVT Ended

						
					}
				}
				firstTime = false;
				//Set resultset data  into Data Object
				accountInfoDo.clearObject();
				accountInfoDo.setDAT_CURRENT_BUSINESS(businessdate);
				accountInfoDo.setDAT_BUSINESS(res.getDate("BUSDT"));
				accountInfoDo.setAMT_CALCULATED(res.getBigDecimal("AMT_CALCULATED"));
				accountInfoDo.setAMT_CAL_SUM(res.getBigDecimal("AMT_CALCULATED"));
//Reallocate to Self				
				accountInfoDo.setAMT_CALCULATED_CENTRAL_CCY(res.getBigDecimal("amt_calculated_central_ccy"));
				accountInfoDo.setAMT_CAL_SUM_CENTRAL_CCY(res.getBigDecimal("amt_calculated_central_ccy"));
				//Reallocate to Self
				
				accountInfoDo.setPARTICIPANT_ID(res.getString("PARTICIPANT_ID"));
				accountInfoDo.setID_POOL(res.getString("ID_POOL"));
				accountInfoDo.setFLG_REALPOOL(res.getString("FLG_REALPOOL"));
				//REPLACE LAST CHAR OF  THE TNX CODE BY C OR D
				//ENH_10.3_154 Starts
				txnCode = setTxnCode(res.getString("COD_TXN"),res.getString("TXN_TYPE"));
				//ENH_10.3_154 Ends
				accountInfoDo.setCOD_TXN(txnCode);
				accountInfoDo.setCOD_CCY(res.getString("COD_CCY"));
				accountInfoDo.setTYP_PARTICIPANT(res.getString("TYP_PARTICIPANT"));
						
				accountInfoDo.setFLG_REALPOSTING("NA");
				accountInfoDo.setFLG_POOL_CONSOLIDATED("NA");
				accountInfoDo.setFLG_ACC_CONSOLIDATED("NA");
				
				//Get DAT_FINAL_SETTLEMENT,DAT_VALUE, DAT_POSTING_START ,DAT_POSTING_START from cache
				//ENH_IS1297 Starts...External BVT changes
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("Inside If condition codTransaction Value -->"+accountInfoDo.getCOD_TXN());
	        	String codTxnString = accountInfoDo.getCOD_TXN() ;
	        	//codTxnString = codTxnString.substring((codTxnString.length()-2),(codTxnString.length()-1));
	        	codTxnString = codTxnString.substring(4,5);
	        	if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
	        		logger.debug("codTxnString:::"+codTxnString);
				//ENH_10.3_154 Starts
				//String tmp =res.getString("ID_POOL");
				//ENH_10.3_154 Ends
	        	if(logger.isDebugEnabled())
	        	logger.debug("strPoolId >>>>> "+strPoolId);
				if (strPoolId.equalsIgnoreCase("NONPOOL_ACCT")){
					logger.debug("Entering for Stand Alone");
				//SUP_IS1587 Starts
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("flag_bvt_cycle VALUE =>"+flag_bvt_cycle);
				        if(flag_bvt_cycle.equalsIgnoreCase(ExecutionConstants.YES))
				        {
				        	 logger.debug("Inside If condition codTransaction Value -->"+accountInfoDo.getCOD_TXN());
							//Ned_Retro
				        	//SA_BVT_POSTING_CYCLE STARTS
				        	/*if(codTxnString.equalsIgnoreCase(ExecutionConstants.NORMAL_POST_CYCLE))
				        	{
				        		 logger.debug("codTxnString value is 'N' :::"+codTxnString);
				        		//dateList = (java.util.Date[]) postingDatesCache.get(res.getString("PARTICIPANT_ID"));
				        		dateList = (java.util.Date[]) postingDatesCacheCurr.get(res.getString("PARTICIPANT_ID"));
				        	}
				        	else
				        	{
				        		 logger.debug("codTxnString value is 'B' :::"+codTxnString);
				        		dateList = (java.util.Date[]) postingDatesCache.get(res.getString("PARTICIPANT_ID"));	
				        	}*/
				        	 
				        	 if(res.getDate("DAT_POSTING").compareTo(businessdate) < 0)
				        	 {
								if (logger.isDebugEnabled())// Changes for
								{ // ENH_15.2_LOG_FIXES
									logger.debug("codTxnString value is 'N' :::"
											+ codTxnString);
								}
							dateList = (java.util.Date[]) postingDatesCache
									.get(res.getString("PARTICIPANT_ID"));
							accountInfoDo.setCOD_TXN(txnCode
									+ ExecutionConstants.BVT_POST_CYCLE);
						}
								else
								{
									if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
										logger.debug("codTxnString value is 'B' :::"+codTxnString);
									dateList = (java.util.Date[]) postingDatesCacheCurr.get(res.getString("PARTICIPANT_ID"));
									accountInfoDo.setCOD_TXN(txnCode + ExecutionConstants.NORMAL_POST_CYCLE);
								}
				        	//SA_BVT_POSTING_CYCLE ENDS
				        }
				        else
				        {
				        	if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				        		logger.debug("flag_bvt_cycle VALUE 'N' =>"+flag_bvt_cycle);
				        	dateList = (java.util.Date[]) postingDatesCache.get(res.getString("PARTICIPANT_ID"));
				        }
				    	//SUP_IS1587 Ends
				}
				else{
					//Intercompany BVT Changes start
					if(typEntity.equalsIgnoreCase(ExecutionConstants.INCOLOAN)){
						//SUP_IS1587 BVT Posting Adjustment Starts
						if(flag_bvt_cycle.equalsIgnoreCase("Y")){
							if(res.getDate("DAT_POSTING").compareTo(businessdate) < 0){ //SUP_ISS_III_7305
							//if(codTxnString.equalsIgnoreCase("N")){ //SUP_ISS_III_7305
								if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
									logger.debug("codTxnString value is 'N' :::"+codTxnString);
				        	    dateList = (java.util.Date[]) postingDatesCache.get(res.getString("PARTICIPANT_ID"));//SUP_ISS_III_7305
							    accountInfoDo.setCOD_TXN(txnCode + ExecutionConstants.BVT_POST_CYCLE);//SUP_ISS_III_7305
				        	 }else{		
				        		 logger.debug("postingDatesCache inn>>>>>"+postingDatesCache);
						        dateList = (java.util.Date[]) postingDatesCache.get(res.getString("PARTICIPANT_ID"));
						        accountInfoDo.setCOD_TXN(txnCode + ExecutionConstants.NORMAL_POST_CYCLE);//SUP_ISS_III_7305
						       }
                          //SUP_IS1587 BVT Posting Adjustment ends	
					//Intercompany BVT Changes end
					     }else{
					    	 logger.debug("PARTICIPANT_ID inn>>>>>"+res.getString("PARTICIPANT_ID"));
					    	 dateList = (java.util.Date[]) postingDatesCache.get(res.getString("PARTICIPANT_ID"));
					     }
						if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
							logger.debug("dateList typEntity INCOLOAN " + dateList);
					    	 
					}
					else
					{
						//ENH_10.3_154 Starts
						//dateList = (java.util.Date[]) postingDatesCache.get(res.getString("ID_POOL"));
						if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y"))
						{
							if(res.getDate("DAT_POSTING").compareTo(businessdate) < 0)
							{
								dateList = (java.util.Date[]) postingDatesCache.get(res.getString("PARTICIPANT_ID"));
								accountInfoDo.setCOD_TXN(txnCode + ExecutionConstants.BVT_POST_CYCLE);
							}
							else
							{
								dateList = (java.util.Date[]) postingDatesCacheCurr.get(res.getString("ID_POOL"));
								accountInfoDo.setCOD_TXN(txnCode + ExecutionConstants.NORMAL_POST_CYCLE);
							}
						}
						else
						{
							if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
								logger.debug("dateList 111111>>>>>"+dateList);
								logger.debug("ID_POOL 111111>>>>>"+res.getString("ID_POOL"));
							}
							dateList = (java.util.Date[]) postingDatesCacheCurr.get(res.getString("ID_POOL"));							
							
						}
						//ENH_10.3_154 Ends
					}
				}
				//ENH_IS1297 Ends...BVT Changes
				//Interest and Advantage Seperate Settlement Dates For JPMC
				accountInfoDo.setAccrualPricingInterfaceFeed(res.getString("flg_acc_accrual_pricing_feed"));//SUP_JPMCIS17438
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("getAccrualPricingInterfaceFeed:::="+accountInfoDo.getAccrualPricingInterfaceFeed());
				String accrualPricingInterfaceFeed = accountInfoDo.getAccrualPricingInterfaceFeed();
				//Interest and Advantage Seperate Settlement Dates For JPMC
				
				if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
				logger.debug("dateList >>>>> "+dateList);
				logger.debug("ID_POOL >>>>> "+res.getString("ID_POOL"));
				logger.debug("PARTICIPANT_ID >>>>> "+res.getString("PARTICIPANT_ID"));

				}

				if(accountInfoDo.getCOD_TXN().substring(4,5).equalsIgnoreCase("C"))
				{
					//Interest and Advantage Seperate Settlement Dates For JPMC
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("CR for paticipant opool_postingsummary:"+accountInfoDo.getPARTICIPANT_ID()+"::accountInfoDo.getCOD_TXN().substring(2, 3):"+accountInfoDo.getCOD_TXN().substring(2, 3));
					if("Y".equalsIgnoreCase(ExecutionConstants.FLG_IS_INTEREST_ADVANTAGE_SEPERATE_SETTLEMENT) && accountInfoDo.getCOD_TXN().substring(2, 3).equalsIgnoreCase("I") && "N".equalsIgnoreCase(accrualPricingInterfaceFeed)){
						if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
							logger.debug("setting :"+dateList[2]);
						accountInfoDo.setDAT_FINAL_SETTLEMENT(new java.sql.Date (((java.util.Date )dateList[2]).getTime()));
						accountInfoDo.setDAT_VALUE(new java.sql.Date (((java.util.Date )dateList[2]).getTime()));
					}else{
						//converting util date into sql.Date
						
						accountInfoDo.setDAT_FINAL_SETTLEMENT(new java.sql.Date (((java.util.Date )dateList[3]).getTime()));
						accountInfoDo.setDAT_VALUE(new java.sql.Date (((java.util.Date )dateList[6]).getTime()));
					}
					//Interest and Advantage Seperate Settlement Dates For JPMC
				}
				else
				{
					//Interest and Advantage Seperate Settlement Dates For JPMC
					if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
						logger.debug("dateList 22222>>>>>"+dateList);
						logger.debug("DR for paticipant opool_postingsummary:"+accountInfoDo.getPARTICIPANT_ID()+"::accountInfoDo.getCOD_TXN().substring(2, 3):"+accountInfoDo.getCOD_TXN().substring(2, 3));
					}
					if("Y".equalsIgnoreCase(ExecutionConstants.FLG_IS_INTEREST_ADVANTAGE_SEPERATE_SETTLEMENT) && accountInfoDo.getCOD_TXN().substring(2, 3).equalsIgnoreCase("I") && "N".equalsIgnoreCase(accrualPricingInterfaceFeed)){
						if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
							logger.debug("setting :"+dateList[2]);
						accountInfoDo.setDAT_FINAL_SETTLEMENT(new java.sql.Date (((java.util.Date )dateList[2]).getTime()));
						accountInfoDo.setDAT_VALUE(new java.sql.Date (((java.util.Date )dateList[2]).getTime()));
					}else{
						accountInfoDo.setDAT_FINAL_SETTLEMENT(new java.sql.Date (((java.util.Date )dateList[4]).getTime()));
						accountInfoDo.setDAT_VALUE(new java.sql.Date (((java.util.Date )dateList[7]).getTime()));
					}
					//Interest and Advantage Seperate Settlement Dates For JPMC
				}
				//Set DAT_POSTING
				accountInfoDo.setDAT_POSTING(new java.sql.Date (((java.util.Date )dateList[0]).getTime()));
				
				//Set DAT_POSTING_START 
				accountInfoDo.setDAT_POSTING_START(new java.sql.Date (((java.util.Date )dateList[5]).getTime()));

				//ENH_IS1297 Starts...BVT Changes
				if (strPoolId.equalsIgnoreCase("NONPOOL_ACCT"))
				{
					logger.debug("Entering for Stand Alone");
					dateList = (java.util.Date[]) accrualDatesCache.get(res.getString("PARTICIPANT_ID"));
				}
				else
				{
//					Intercompany BVT Changes start
					if(typEntity.equalsIgnoreCase(ExecutionConstants.INCOLOAN))
					 dateList = (java.util.Date[]) accrualDatesCache.get(res.getString("PARTICIPANT_ID"));
					//Intercompany BVT Changes end
					else	
					dateList = (java.util.Date[])accrualDatesCache.get(res.getString("ID_POOL"));
				}
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("date arraylist: "+dateList[0].getTime()+" , "+dateList[1].getTime()+" and "+dateList[2].getTime());
				//ENH_IS1297 Ends...BVT Changes

				//Get Date_accrual,Adjusted Accrual, Accrual date start from cache	
				//Set Date_accrual
				accountInfoDo.setDAT_ACCRUAL(new java.sql.Date (((java.util.Date )dateList[0]).getTime()));
				//Set Adjusted Accrual
				accountInfoDo.setDAT_ADJUSTEDACCRUAL(new java.sql.Date (((java.util.Date )dateList[1]).getTime()));
				//Set Accrual date start
				accountInfoDo.setDAT_ACCRUAL_START(new java.sql.Date (((java.util.Date )dateList[2]).getTime()));
				
				// ENH_10.3_153 Starts
				if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
					logger.debug("txn Code : " + accountInfoDo.getCOD_TXN()) ;
					logger.debug("FLG_SA_INTEREST_TYPE : "  + ExecutionConstants.FLG_SA_INTEREST_TYPE + "strPoolId" + strPoolId) ;
				}
				if(ExecutionConstants.FLG_SA_INTEREST_TYPE && "NONPOOL_ACCT".equals(strPoolId)) 
				{	
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("strPoolId " + strPoolId + ", typEntity : " + typEntity) ;
				
					interestTyp = res.getString("TYP_INTEREST") ; 
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("interestTyp FLG_SA_INTEREST_TYPE is: "  + interestTyp) ;
					if(interestTyp==null || interestTyp.equalsIgnoreCase("I"))
					{
						// DO NOTHING
					}
					else
					{
						//txnCode = txnCode + interestTyp ;
						//logger.debug("interestTyp is FLG_SA_INTEREST_TYPE final  : "  + interestTyp) ;
						if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
							logger.debug("interestTyp  interestTyp is FLG_SA_INTEREST_TYPE final "  + interestTyp +  "accountInfoDo.getCOD_TXN()" + accountInfoDo.getCOD_TXN()) ;
						accountInfoDo.setCOD_TXN(accountInfoDo.getCOD_TXN() + interestTyp);
					}
				}
				else if (!"NONPOOL_ACCT".equals(strPoolId) && !ExecutionConstants.INCOLOAN.equals(typEntity))
				{
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("strPoolId " + strPoolId + ", typEntity : " + typEntity) ;
					if(ExecutionConstants.FLG_INTACC_POST_INTTYP) 
					{	
						interestTyp = res.getString("TYP_INTEREST") ;
						if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
							logger.debug("interestTyp  >>>>> is : "  + interestTyp) ;
						if(interestTyp==null || interestTyp.equalsIgnoreCase("I"))
						{
							// DO NOTHING
						}
						else
						{
							//txnCode = txnCode + interestTyp ;
							if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
								logger.debug("interestTyp  >>>>>:<<: :final  "  + interestTyp +  "accountInfoDo.getCOD_TXN()" + accountInfoDo.getCOD_TXN()) ;
							//accountInfoDo.setCOD_TXN(txnCode);
							accountInfoDo.setCOD_TXN(accountInfoDo.getCOD_TXN() + interestTyp);
						}
					}
				}
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("txn Code is >>>>>>>> :::: " + accountInfoDo.getCOD_TXN()) ;
				// ENH_10.3_153 Ends
				if(!"NONPOOL_ACCT".equals(strPoolId) && !ExecutionConstants.INCOLOAN.equals(typEntity))
				 {
				accountInfoDo.setSOURCE_POOL_ID(res.getString("SOURCE_POOL_ID")); // NEW_ACCOUNTING_ENTRY_FORMAT
				 }
				else
				{
					accountInfoDo.setSOURCE_POOL_ID("NA"); // NEW_ACCOUNTING_ENTRY_FORMAT
				}
				if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
					logger.debug(" accountInfoDo pass to populateBvtAccountDtls " + accountInfoDo.toString()) ;
					//CAll populateBvtAccountDtls()				
					logger.debug(" accountInfoDo pass to populateBvtAccountDtls DAT_INITIAL_POSTING -->" + res.getDate("DAT_POSTING")) ;
				}
				accountInfoDo.setDAT_INITIAL_POSTING(res.getDate("DAT_POSTING"));//ANZ_ISSUE_5233
				populateBvtAccountDtls(businessDate, client, insertAccountDtls, updateAccountDtls,accountInfoDo,selectAccountDtls);
					
			} // end of while loop
			
			if(!recordExists)
			{
				logger.fatal("BVT QUERY : "+strQuery);
				logger.fatal(" No Transcation Found FOR ABOVE BVT QUERY");        
			}
			else if(recordExists)
			{
				//ENH_IS1297 RETROFIT STARTS
				/** ****** JTEST FIX start ************  on 14DEC09*/
//				populateBVTAccuralSummary(businessdate,insertAccrualSummary,updateAccrualSummary,insertUnpostedAmt,accountInfoDo,selectAccrualSummary,updateUnpostedAmt);
				populateBVTAccuralSummary(businessdate,insertAccrualSummary,updateAccrualSummary,insertUnpostedAmt,accountInfoDo,selectAccrualSummary,updateUnpostedAmt,updateUnpostedAmtCentralCcy, conn);//Reallocate to Self
				/** ****** JTEST FIX end **************  on 14DEC09*/
				//ENH_IS1297 RETROFIT ENDS
				populateBVTPostingSummary(businessdate,insertPostingSummary,updatePostingSummary,accountInfoDo,selectPostingSummary);  
				
				// ENH_JPMC_TAXBVT Started
				//flgPostingToBeDone = checkPostingToBeDone(businessdate, accountInfoDo);
				if(logger.isDebugEnabled())
				{
					logger.debug("*** Populating TAX LIST for... ****");
					logger.debug("typEntity :::: " + typEntity);
					logger.debug("businessDate :::: " + businessDate);
					logger.debug("postingDate :::: " + accountInfoDo.getDAT_POSTING().toString());
					//logger.debug("flgPostingToBeDone :::: " + flgPostingToBeDone);
					//logger.debug("partyEndDate :::: " + accountInfoDo.getPARTY_DAT_END().toString());
				}
				
				//if(typEntity.equalsIgnoreCase(InterCoConstants.INCOLOAN) && (businessDate.toString().equalsIgnoreCase(accountInfoDo.getDAT_POSTING().toString())
				//|| accountInfoDo.getPARTY_DAT_END().toString().equalsIgnoreCase(businessDate.toString())) && flgPostingToBeDone)
				if(typEntity.equalsIgnoreCase(InterCoConstants.INCOLOAN) && businessDate.compareTo(accountInfoDo.getDAT_POSTING()) >= 0)				
				{
					calTaxCalculator = true;
					HashMap interestDetailsMap = new HashMap();
					
					// Put the loan id and corresponding interest in the interest Details Map
					interestDetailsMap.put(InterCoConstants.INCOLOAN, accountInfoDo.getPARTICIPANT_ID());
					interestDetailsMap.put(InterCoConstants.AMOUNT, accountInfoDo.getAMT_POSTED());
					interestDetailsMap.put(InterCoConstants.COD_TXN, accountInfoDo.getCOD_TXN());  //ENH_ICLTAX
					// Put the interest details Map in loan Id list ArrayList
					loanIdList.add(interestDetailsMap);
				}
				//	ENH_JPMC_TAXBVT Ended
				
			}
		
			// ENH_JPMC_TAXBVT Started			
			try {
				if(logger.isDebugEnabled()){	
					logger.debug("calTaxCalculator flag is : " + calTaxCalculator);
				}
				if(typEntity.equalsIgnoreCase(InterCoConstants.INCOLOAN) && calTaxCalculator)
				{
					// Put the event as 'LOAN_INTEREST' IN taxDetails Map
					taxDetailsMap.put(ExecutionConstants.EVENT, InterCoConstants.LOANINTEREST);
					
					// Put the tax type as 'With holding tax' in taxDetails Map
					taxDetailsMap.put(InterCoConstants.TAXTYPE, InterCoConstants.BUSINESSTAX);
					
					// Put the bvt flag as 'YES' in taxDetails Map
					taxDetailsMap.put(InterCoConstants.BVTFLAG, InterCoConstants.YES);
					
					// Put the loanIdlist in taxDetails Map
					taxDetailsMap.put(InterCoConstants.LIST, loanIdList);
					
					/*EJBHomeFactory factory = EJBHomeFactory.getFactory();
					TaxHandlerSLSBHome slsbHome = (TaxHandlerSLSBHome) factory.lookupHome(TaxHandlerSLSBHome.class);
					TaxHandlerSLSB slsb = slsbHome.create();*/
					
					TaxHandlerSLSB slsb = (TaxHandlerSLSB) Ejb3Locator.doLookup("TaxHandlerSLSBBean", TaxHandlerSLSB.class);
					
					// Call the calculateTax() method to calculate the tax 
					if(logger.isDebugEnabled()){
						logger.debug("BVT Tax Details HashMap is : " + taxDetailsMap);
					}
					slsb.calculateTax(taxDetailsMap);					
				}
			} catch (Exception e) 
			{
				logger.fatal("Exception Occured :: ", e );
				throw new LMSException("LMS01000","Error occurred while populating BvtAccount Information for BVT Id "+bvtId+" and Host System "+client+".", e);
			} 
			// ENH_JPMC_TAXBVT Ended

		}
		catch(SQLException ex)
		{
			if(logger.isFatalEnabled())
			{
				logger.fatal("Exception :: ", ex);
			}	
			//ENH_IS1059  Starts
			//throw new LMSException("Error Occured While Executing Query In Method PopulateBvtAccountInfo", ex.getMessage());
			throw new LMSException("LMS01000","Error occurred while populating BvtAccount Information for BVT Id "+bvtId+" and Host System "+client+".", ex);
			//ENH_IS1059  Ends
		}
		finally
		{
			 LMSConnectionUtility.close(res);
			 /** ****** JTEST FIX start ************  on 14DEC09*/
//			 LMSConnectionUtility.close(stmt);
			 LMSConnectionUtility.close(pStmt);
			 /** ****** JTEST FIX end **************  on 14DEC09*/
	         LMSConnectionUtility.close(conn);
		}
		if (logger.isDebugEnabled()){
			logger.debug("Leaving");
		}
	}
	
	//ENH_10.3_154 Starts
	/**This method execute the query and set data in Do Object and call the populateBvtAccountDtls , populateBVTAccuralSummary ,populateBVTPostingSummary
	 * @param strQuery
	 * @param bvtId
	 * @param businessDate
	 * @param client
	 * @param insertAccountDtls
	 * @param updateAccountDtls
	 * @param insertAccrualSummary
	 * @param insertPostingSummary
	 * @param updateAccrualSummary
	 * @param updatePostingSummary
	 * @param insertUnpostedAmt
	 * @param selectAccountDtls
	 * @param selectAccrualSummary
	 * @param selectPostingSummary
	 * @throws LMSException
	 */
	   //Changed Method Name from  'populateCycleWiseBvtAccountInfo' to 'populateCycleWiseBvtAccountInfoForPool'
	  //private void populateCycleWiseBvtAccountInfo(String strQuery,String bvtId, Date businessDate, String client, 
		private void populateCycleWiseBvtAccountInfoForPool(String strQuery,String bvtId, Date businessDate, String client,
			PreparedStatement insertAccountDtls, PreparedStatement updateAccountDtls, 
			PreparedStatement insertAccrualSummary, PreparedStatement insertPostingSummary, 
			PreparedStatement updateAccrualSummary, PreparedStatement updatePostingSummary,
			PreparedStatement insertUnpostedAmt,PreparedStatement selectAccountDtls ,
			PreparedStatement selectAccrualSummary ,PreparedStatement selectPostingSummary,PreparedStatement updateUnpostedAmt
		) throws LMSException
	{
		if (logger.isDebugEnabled()){
			logger.debug("Entering populateCycleWiseBvtAccountInfoForPool");
		}
		PreparedStatement pStmt = null;
		Connection conn = null;
		ResultSet res = null;
		boolean recordExists = false,firstTime = true;
		AccountInfoDO accountInfoDo = null;
		java.sql.Date businessdate = new java.sql.Date(businessDate.getTime());
		java.util.Date dateList[]= null;
		String txnCode = "";
		String typEntity="";
		// Issue 15954 fix starts
		//AE_ON_FINALSETTELMENT_DATE  JPMC starts
		String flg_AE_ON_FinalSettelment_Date = ExecutionConstants.AE_ON_FINALSETTELMENT_DATE;
		if (logger.isDebugEnabled()) {
		 logger.debug("AE_ON_FINALSETTELMENT_DATE ::::: " + flg_AE_ON_FinalSettelment_Date );
		}
		java.util.Date dateList2[]= null;
		java.util.Date final_settlementDate = null;
		
    	//Issue 15954 fix ends
		try
		{
			try 
			{
				conn = LMSConnectionUtility.getConnection();
			} catch (NamingException e) 
			{
				logger.fatal(" Error occured while getting connection :"+e.getMessage());
			}

			pStmt = conn.prepareStatement(strQuery);
			res = pStmt.executeQuery();
			accountInfoDo = new AccountInfoDO();
            String flag_bvt_cycle = ExecutionConstants.FLAG_BVT_CYCLE;
            if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
            	logger.debug("flag_bvt_cycle:::"+flag_bvt_cycle);
	        boolean flg_newAccountingentry = ExecutionConstants.FlG_NEW_ACCOUNTING_ENTRY_FORMAT; // NEW_ACCOUNTING_ENTRY_FORMAT
	        if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
	        	logger.debug("flg_newAccountingentry:::"+flg_newAccountingentry);
	        String source_PoolId = null ;
	        
	        while(res.next())
			{
				txnCode = setTxnCode(res.getString("COD_TXN"),res.getString("TXN_TYPE"));
				recordExists = true;
				typEntity=res.getString("TYP_PARTICIPANT");
				String tmp =res.getString("ID_POOL");
				accountInfoDo.setTYP_INTEREST(res.getString("typ_interest"));
				source_PoolId =res.getString("SOURCE_POOL_ID"); //NEW_ACCOUNTING_ENTRY_FORMAT
				if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
					logger.debug("source_PoolId :::"+source_PoolId); //NEW_ACCOUNTING_ENTRY_FORMAT
					logger.debug("amt_calculated_central_ccy :::"+res.getBigDecimal("amt_calculated_central_ccy"));//Reallocate to Self
				}
				
				//SUP_ISS_IV_7296
				/*if (tmp.equalsIgnoreCase("NONPOOL_ACCT") || typEntity.equalsIgnoreCase(ExecutionConstants.INCOLOAN))
				{
	        		return;
				}*/
				//SUP_ISS_IV_7296
				if(!firstTime)
				{
					if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase(ExecutionConstants.YES))
					{
						if(res.getDate("DAT_POSTING").compareTo(businessdate) < 0)
						{
							dateList = (java.util.Date[]) postingDatesCache.get(res.getString("PARTICIPANT_ID"));
							txnCode = txnCode + ExecutionConstants.BVT_POST_CYCLE;
						}
						else
						{
							dateList = (java.util.Date[]) postingDatesCacheCurr.get(res.getString("ID_POOL"));
							txnCode = txnCode + ExecutionConstants.NORMAL_POST_CYCLE;
						}
					}
					
					if(txnCode.equalsIgnoreCase(accountInfoDo.getCOD_TXN()) 
									&& res.getString("PARTICIPANT_ID").equalsIgnoreCase(accountInfoDo.getPARTICIPANT_ID())
									&& res.getString("ID_POOL").equalsIgnoreCase(accountInfoDo.getID_POOL())
									&& res.getDate("DAT_POSTING").equals(accountInfoDo.getDAT_INITIAL_POSTING())
									&& source_PoolId.equalsIgnoreCase(accountInfoDo.getSOURCE_POOL_ID())) //NEW_ACCOUNTING_ENTRY_FORMAT)
					{
						if (logger.isDebugEnabled())
						{
							logger.debug("accountInfoDo.getPARTICIPANT_ID()" +accountInfoDo.getPARTICIPANT_ID());
							logger.debug("accountInfoDo.getCOD_TXN()"+accountInfoDo.getCOD_TXN());
							logger.debug("accountInfoDo.getID_POOL()" +accountInfoDo.getID_POOL());
							logger.debug("accountInfoDo.getSOURCE_POOL_ID()" +accountInfoDo.getSOURCE_POOL_ID());
						}
						accountInfoDo.setDAT_BUSINESS(res.getDate("BUSDT"));
						accountInfoDo.setAMT_CALCULATED(res.getBigDecimal("AMT_CALCULATED"));
						accountInfoDo.setAMT_CALCULATED_CENTRAL_CCY(res.getBigDecimal("amt_calculated_central_ccy"));//Reallocate to Self
						if (logger.isDebugEnabled()){
							logger.debug("res.getBigDecimal(AMT_CALCULATED)" +res.getBigDecimal("AMT_CALCULATED"));
							logger.debug("accountInfoDo.getAMT_CALCULATED " +accountInfoDo.getAMT_CALCULATED());
							logger.debug("accountInfoDo.getAMT_CALCULATED_CENTRAL_CCY " +accountInfoDo.getAMT_CALCULATED_CENTRAL_CCY());//Reallocate to Self
						}
						//Set AMT_CAL_SUM = AMT_CAL_SUM + AMT_CALCULATED
						accountInfoDo.setAMT_CAL_SUM(res.getBigDecimal("AMT_CALCULATED").add(accountInfoDo.getAMT_CAL_SUM()));
						//Reallocate to Self
						//accountInfoDo.setAMT_CAL_SUM_CENTRAL_CCY(res.getBigDecimal("amt_calculated_central_ccy").add(accountInfoDo.getAMT_CALCULATED_CENTRAL_CCY()));
						accountInfoDo.setAMT_CAL_SUM_CENTRAL_CCY(res.getBigDecimal("amt_calculated_central_ccy").add(accountInfoDo.getAMT_CAL_SUM_CENTRAL_CCY()));
						//Reallocate to Self
						//CAll populateBvtAccountDtls()
						if (logger.isDebugEnabled()){
							logger.debug("accountInfoDo.getAMT_CAL_SUM " +accountInfoDo.getAMT_CAL_SUM());
							logger.debug("accountInfoDo.getAMT_CAL_SUM_CENTRAL_CCY " +accountInfoDo.getAMT_CAL_SUM_CENTRAL_CCY());//Reallocate to Self
						}						
						
						continue ;
					}
					else
					{
						// JPMC_FIX
						//populateCycleWiseBVTAccuralSummary(businessdate,insertAccrualSummary,updateAccrualSummary,insertUnpostedAmt,accountInfoDo,selectAccrualSummary,updateUnpostedAmt, conn);
						  populateCycleWiseBVTAccuralSummaryForPool(businessdate,insertAccrualSummary,updateAccrualSummary,insertUnpostedAmt,accountInfoDo,selectAccrualSummary,updateUnpostedAmt, conn);
						populateCycleWiseBVTPostingSummaryForPool(businessdate,insertPostingSummary,updatePostingSummary,accountInfoDo,selectPostingSummary);

						if(logger.isDebugEnabled())
						{
							logger.debug(" **** Populating TAX LIST for.... ****");
							logger.debug("typEntity :::: " + typEntity);
							logger.debug("businessDate :::: " + businessDate);
							logger.debug("postingDate :::: " + accountInfoDo.getDAT_POSTING().toString());
						}
					}
				}
				firstTime = false;
				//Set resultset data  into Data Object
				accountInfoDo.clearObject();
				accountInfoDo.setDAT_CURRENT_BUSINESS(businessdate);
				accountInfoDo.setDAT_BUSINESS(res.getDate("BUSDT"));
				accountInfoDo.setAMT_CALCULATED(res.getBigDecimal("AMT_CALCULATED"));
				accountInfoDo.setAMT_CAL_SUM(res.getBigDecimal("AMT_CALCULATED"));
//Reallocate to Self				
				accountInfoDo.setAMT_CALCULATED_CENTRAL_CCY(res.getBigDecimal("amt_calculated_central_ccy"));
				accountInfoDo.setAMT_CAL_SUM_CENTRAL_CCY(res.getBigDecimal("amt_calculated_central_ccy"));
				accountInfoDo.setRATE_EXRATE_CENTRAL_CCY(res.getBigDecimal("RATE_EXRATE_CENTRAL_CCY"));
				//Reallocate to Self
				accountInfoDo.setPARTICIPANT_ID(res.getString("PARTICIPANT_ID"));
				accountInfoDo.setID_POOL(res.getString("ID_POOL"));
				accountInfoDo.setFLG_REALPOOL(res.getString("FLG_REALPOOL"));
				//REPLACE LAST CHAR OF  THE TNX CODE BY C OR D
				txnCode = setTxnCode(res.getString("COD_TXN"),res.getString("TXN_TYPE"));
				accountInfoDo.setCOD_TXN(txnCode);
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("txnCode #### " + txnCode);
				accountInfoDo.setCOD_CCY(res.getString("COD_CCY"));
				accountInfoDo.setTYP_PARTICIPANT(res.getString("TYP_PARTICIPANT"));
						
				accountInfoDo.setFLG_REALPOSTING("NA");
				accountInfoDo.setFLG_POOL_CONSOLIDATED("NA");
				accountInfoDo.setFLG_ACC_CONSOLIDATED("NA");
				
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("Inside If condition codTransaction Value -->"+accountInfoDo.getCOD_TXN());
	        	String codTxnString = accountInfoDo.getCOD_TXN() ;
	        	codTxnString = codTxnString.substring(4,5);
	        	if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
	        		logger.debug("codTxnString:::"+codTxnString);
				
        		if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase(ExecutionConstants.YES))
				{
					if(res.getDate("DAT_POSTING").compareTo(businessdate) < 0)
					{
						dateList = (java.util.Date[]) postingDatesCache.get(res.getString("PARTICIPANT_ID"));
						accountInfoDo.setCOD_TXN(txnCode + ExecutionConstants.BVT_POST_CYCLE);
					}
					else
					{
						dateList = (java.util.Date[]) postingDatesCacheCurr.get(res.getString("ID_POOL"));
						accountInfoDo.setCOD_TXN(txnCode + ExecutionConstants.NORMAL_POST_CYCLE);
					}
				}
				else
				{
					//Issue 15954 fix starts (Added Flag and conditions for final settelment date
					if(flg_AE_ON_FinalSettelment_Date.equalsIgnoreCase("Y"))
					{
						//JPMC_SUP 16387 FIXED STARTS 
						if(logger.isDebugEnabled()){
							logger.debug("DAT_POSTING:::"+res.getDate("DAT_POSTING"));
							logger.debug("participant id" + res.getString("PARTICIPANT_ID")); //JPMC_BVT_ISSUE
							logger.debug("postingDatesCachePrev:::"+postingDatesCachePrev);
							logger.debug("postingDatesCacheCurr:::"+postingDatesCacheCurr);
							logger.debug("ID_POOL:::"+res.getString("ID_POOL"));
						}
						//Issue 15954 fix starts
							if(res.getDate("DAT_POSTING").compareTo(businessdate) < 0   && postingDatesCachePrev !=null && postingDatesCachePrev.get(res.getString("ID_POOL"))!=null)
							{
								if(logger.isDebugEnabled())
									logger.debug("Inside previous cycle:::");
								
								dateList2= (java.util.Date[]) postingDatesCachePrev.get(res.getString("ID_POOL"));
								//JPMC_BVT_ISSUE STARTS
								/*if(accountInfoDo.getCOD_TXN().substring(4,5).equalsIgnoreCase("C")){
								
									final_settlementDate = new java.sql.Date (((java.util.Date )dateList2[3]).getTime());
									if(logger.isDebugEnabled())
										logger.debug("final_settlementDate:::" + final_settlementDate);
								
								}else{
								
									final_settlementDate = new java.sql.Date (((java.util.Date )dateList2[4]).getTime());
									if(logger.isDebugEnabled())
										logger.debug("final_settlementDate:::" + final_settlementDate);
								}*/
								final_settlementDate = new java.sql.Date (((java.util.Date )dateList2[2]).getTime());
								//JPMC_BVT_ISSUE ends
								if(logger.isDebugEnabled())
									logger.debug("businessdate:::"+businessdate+",final_settlementDate ::: "+final_settlementDate);
								
								if(businessdate.compareTo(final_settlementDate) <= 0){
									if(logger.isDebugEnabled())
										logger.debug("Inside previous cycle if business date is less than or equal to final settelment date :::");
									
									dateList = (java.util.Date[]) postingDatesCachePrev.get(res.getString("ID_POOL"));
								}
								else{
									if(logger.isDebugEnabled())
										logger.debug("Normal cycle 222222:::");
									
									dateList = (java.util.Date[]) postingDatesCacheCurr.get(res.getString("ID_POOL"));
								}
						}else{
							if(logger.isDebugEnabled())
								logger.debug("Inside next cycle:::");
							
							dateList = (java.util.Date[]) postingDatesCacheCurr.get(res.getString("ID_POOL"));
						}
						
					}else{
						if(logger.isDebugEnabled())
							logger.debug("Inside next cycle:::");
						
						dateList = (java.util.Date[]) postingDatesCacheCurr.get(res.getString("ID_POOL"));
						accountInfoDo.setDAT_POST_SETTLEMENT(new java.sql.Date (((java.util.Date )dateList[2]).getTime()));//SUP_ISS_IV_7602
					}
					//JPMC_SUP 16387 FIXED ENDS
					//Issue 15954 fix ends
				}
				
        		//Interest and Advantage Seperate Settlement Dates For JPMC
        		accountInfoDo.setAccrualPricingInterfaceFeed(res.getString("flg_acc_accrual_pricing_feed"));//SUP_JPMCIS17438
        		if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
        			logger.debug("getAccrualPricingInterfaceFeed:::="+accountInfoDo.getAccrualPricingInterfaceFeed());
        		String accrualPricingInterfaceFeed = accountInfoDo.getAccrualPricingInterfaceFeed();
        		//Interest and Advantage Seperate Settlement Dates For JPMC
				if(accountInfoDo.getCOD_TXN().substring(4,5).equalsIgnoreCase("C"))
				{
					//Interest and Advantage Seperate Settlement Dates For JPMC
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("CR for paticipant:"+accountInfoDo.getPARTICIPANT_ID()+"::accountInfoDo.getCOD_TXN().substring(2, 3):"+accountInfoDo.getCOD_TXN().substring(2, 3));
					if("Y".equalsIgnoreCase(ExecutionConstants.FLG_IS_INTEREST_ADVANTAGE_SEPERATE_SETTLEMENT) && accountInfoDo.getCOD_TXN().substring(2, 3).equalsIgnoreCase("I") && "N".equalsIgnoreCase(accrualPricingInterfaceFeed)){
						if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
							logger.debug("setting :"+dateList[2]);
						accountInfoDo.setDAT_FINAL_SETTLEMENT(new java.sql.Date (((java.util.Date )dateList[2]).getTime()));
						accountInfoDo.setDAT_VALUE(new java.sql.Date (((java.util.Date )dateList[2]).getTime()));
					}else{
						//converting util date into sql.Date
						accountInfoDo.setDAT_FINAL_SETTLEMENT(new java.sql.Date (((java.util.Date )dateList[3]).getTime()));
						accountInfoDo.setDAT_VALUE(new java.sql.Date (((java.util.Date )dateList[6]).getTime()));
					}
					//Interest and Advantage Seperate Settlement Dates For JPMC
				}
				else
				{
					//Interest and Advantage Seperate Settlement Dates For JPMC
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("DR for paticipant:"+accountInfoDo.getPARTICIPANT_ID()+"::accountInfoDo.getCOD_TXN().substring(2, 3):"+accountInfoDo.getCOD_TXN().substring(2, 3));
					if("Y".equalsIgnoreCase(ExecutionConstants.FLG_IS_INTEREST_ADVANTAGE_SEPERATE_SETTLEMENT) && accountInfoDo.getCOD_TXN().substring(2, 3).equalsIgnoreCase("I") && "N".equalsIgnoreCase(accrualPricingInterfaceFeed)){
						if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
							logger.debug("setting :"+dateList[2]);
						accountInfoDo.setDAT_FINAL_SETTLEMENT(new java.sql.Date (((java.util.Date )dateList[2]).getTime()));
						accountInfoDo.setDAT_VALUE(new java.sql.Date (((java.util.Date )dateList[2]).getTime()));
					}else{
						accountInfoDo.setDAT_FINAL_SETTLEMENT(new java.sql.Date (((java.util.Date )dateList[4]).getTime()));
						accountInfoDo.setDAT_VALUE(new java.sql.Date (((java.util.Date )dateList[7]).getTime()));
					}
					//Interest and Advantage Seperate Settlement Dates For JPMC
				}
				//Set DAT_POSTING
				accountInfoDo.setDAT_POSTING(new java.sql.Date (((java.util.Date )dateList[0]).getTime()));
				
				//Set DAT_POSTING_START 
				//NED_ISSUE FIXED STARTS
				//accountInfoDo.setDAT_POSTING_START(new java.sql.Date (((java.util.Date )dateList[5]).getTime()));
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("DAT_POSTING_START >>>>>: "+res.getDate("DAT_POSTING_START"));
				accountInfoDo.setDAT_POSTING_START(res.getDate("DAT_POSTING_START"));
				dateList = (java.util.Date[])accrualDatesCache.get(res.getString("ID_POOL"));
				
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("date arraylist: "+dateList[0]+" , "+dateList[1]+" and "+dateList[2]);
				//NED_ISSUE FIXED ENDS
				
				//Get Date_accrual,Adjusted Accrual, Accrual date start from cache	
				//Set Date_accrual
				accountInfoDo.setDAT_ACCRUAL(new java.sql.Date (((java.util.Date )dateList[0]).getTime()));
				//Set Adjusted Accrual
				accountInfoDo.setDAT_ADJUSTEDACCRUAL(new java.sql.Date (((java.util.Date )dateList[1]).getTime()));
				//Set Accrual date start
				accountInfoDo.setDAT_ACCRUAL_START(new java.sql.Date (((java.util.Date )dateList[2]).getTime()));
				//11.6_Retro_Cycle_3 starts
				//ANZ_Defect_6549_2 starts
				//Set Initial Posting Date for consolidated BVT Adjustments
				
				accountInfoDo.setDAT_INITIAL_POSTING(res.getDate("DAT_POSTING"));
				accountInfoDo.setDAT_POST_SETTLEMENT(res.getDate("dat_post_settlement"));//SUP_ISS_IV_7602
				
				//ANZ_Defect_6549_2 ends
				//11.6_Retro_Cycle_3 ends
				accountInfoDo.setSOURCE_POOL_ID(res.getString("SOURCE_POOL_ID")); // NEW_ACCOUNTING_ENTRY_FORMAT
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("accountInfoDo.setSOURCE_POOL_ID " + accountInfoDo.getSOURCE_POOL_ID()); // NEW_ACCOUNTING_ENTRY_FORMAT
			}
			
			if(!recordExists)
			{
				logger.fatal("BVT QUERY : "+strQuery);
				logger.fatal(" No Transcation Found FOR ABOVE BVT QUERY");        
			}
			if(recordExists)
			{
				// JPMC_FIX Changed method name
				//populateCycleWiseBVTAccuralSummary(businessdate,insertAccrualSummary,updateAccrualSummary,insertUnpostedAmt,accountInfoDo,selectAccrualSummary,updateUnpostedAmt, conn);
				populateCycleWiseBVTAccuralSummaryForPool(businessdate,insertAccrualSummary,updateAccrualSummary,insertUnpostedAmt,accountInfoDo,selectAccrualSummary,updateUnpostedAmt, conn);
				populateCycleWiseBVTPostingSummaryForPool(businessdate,insertPostingSummary,updatePostingSummary,accountInfoDo,selectPostingSummary);  
				
				if(logger.isDebugEnabled())
				{
					logger.debug("*** Populating TAX LIST for... ****");
					logger.debug("typEntity :::: " + typEntity);
					logger.debug("businessDate :::: " + businessDate);
					logger.debug("postingDate :::: " + accountInfoDo.getDAT_POSTING().toString());
				}
				
			}
		}
		catch(SQLException ex)
		{
			//logger.fatal("Exception in populateCycleWiseBvtAccountInfo :: ", ex);
			logger.fatal("Exception in populateCycleWiseBvtAccountInfoForPool :: ", ex);
			throw new LMSException("LMS01000","Error occurred while populating CycleWiseBvtAccount Information for BVT Id "+bvtId+" and Host System "+client+".", ex);
		}
		finally
		{
			 LMSConnectionUtility.close(res);
			 LMSConnectionUtility.close(pStmt);
	         LMSConnectionUtility.close(conn);
		}
		if (logger.isDebugEnabled()){
			//logger.debug("Leaving populateCycleWiseBvtAccountInfo");
			logger.debug("Leaving populateCycleWiseBvtAccountInfoForPool");
		}
	}
	//ENH_10.3_154 Ends
	
	/**
	 * This method checks whether an enrty is there in  AccountDtls and does update or insert operation accordingly
	 * @param businessDate
	 * @param client
	 * @param insertAccountDtls
	 * @param updateAccountDtls
	 * @param accountInfoDo
	 * @param selectAccountDtls
	 * @throws LMSException
	 */
	private void populateBvtAccountDtls(Date businessDate, String client, 
			PreparedStatement insertAccountDtls,PreparedStatement updateAccountDtls,
			AccountInfoDO accountInfoDo,PreparedStatement selectAccountDtls ) throws LMSException 
	{
		if (logger.isDebugEnabled()){
			logger.debug("Entering ");
		}
		
		boolean recordExists = false;
		recordExists = checkRecordInAccountDtls(accountInfoDo,selectAccountDtls);
		if(recordExists == true)
		{
			if (logger.isDebugEnabled()){
				logger.debug(" DATA OBJECT DETAILS recordExists updateAccountDtls true" + accountInfoDo.toString());
			}
			//updateAccountDtls();
			entries[1]=true;
			updateAccountDtls(updateAccountDtls,accountInfoDo);
			
		}
		else
		{
			entries[0]=true;
			if (logger.isDebugEnabled()){
				logger.debug(" DATA OBJECT DETAILS recordExists insertAccountDetails false" + accountInfoDo.toString());
			}
			insertAccountDetails(insertAccountDtls,accountInfoDo);
		}
		
		if (logger.isDebugEnabled()){
			logger.debug("Leaving ");
		}
		
	}

	/**
	 * This method set parameters into updateAccountDtls statement
	 * @param updateAccountDtls
	 * @param accountInfoDo
	 * @throws LMSException
	 */
	private void updateAccountDtls(PreparedStatement updateAccountDtls, AccountInfoDO accountInfoDo)throws LMSException  
	{
		
		if (logger.isDebugEnabled()){
			logger.debug("Entering ");
		}
		try
		{
		updateAccountDtls.setBigDecimal(1,accountInfoDo.getAMT_CALCULATED());
		//ENH_IS1731 Exec audit trailing changes Starts
//Retro_Mashreq
		//updateAccountDtls.setString(2,com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT.format(new java.util.Date()));
		updateAccountDtls.setString(2,REF_DATE_FORMAT.format(new java.util.Date()));
		//SETTING WHRER CLAUSE PLACE HOLDERS
		
		updateAccountDtls.setString(3,accountInfoDo.getPARTICIPANT_ID());
		updateAccountDtls.setString(4,accountInfoDo.getCOD_TXN());
		updateAccountDtls.setDate(5,LMSUtility.truncSqlDate(accountInfoDo.getDAT_CURRENT_BUSINESS()));
		updateAccountDtls.setDate(6,LMSUtility.truncSqlDate(accountInfoDo.getDAT_BUSINESS()));
		updateAccountDtls.setString(7,accountInfoDo.getID_POOL());
		updateAccountDtls.setString(8,accountInfoDo.getTYP_PARTICIPANT());
		updateAccountDtls.setString(9,accountInfoDo.getSOURCE_POOL_ID()); // NEW_ACCOUNTING_ENTRY_FORMAT 
		if (accountInfoDo.getSimId()!=0)
			updateAccountDtls.setLong(10, accountInfoDo.getSimId());
		//ENH_IS1731 Exec audit trailing changes Ends
		updateAccountDtls.addBatch();
		}
		catch(SQLException e)
		{
			logger.fatal("Exception :: " , e);
			//ENH_IS1059  Starts
			//throw new LMSException("Error occured in UpdateAccountDetails", e.getMessage());
			throw new LMSException("LMS01000","Updation of OPOOL_ACCOUNTDTLS table failed  for Transaction Id "+accountInfoDo.getCOD_TXN()+" ,Pool Id :-"+accountInfoDo.getID_POOL()+" ,Paricipant :"+accountInfoDo.getPARTICIPANT_ID()+" and Participant Type :- "+accountInfoDo.getTYP_PARTICIPANT()+".", e);
			//ENH_IS1059  Ends
		}
		if (logger.isDebugEnabled()){
			logger.debug("Leaving ");
		}
		
	}

	/**
	 * This method provides the nextCycleAccrualDates given an array of cod_gl, an array of id_pool, nbr_processId
	 * This helps in calculating the next dat_accrual and dat_adjusted_accrual
	 *
	 * @param conn
	 * @param poolId
	 * @param codGL
	 * @param processId
	 * @param businessDate 
	 * @return
	 */
	public void calculateNextCycleAccrualDates(Connection conn, long processId,
			//ENH_IS1297...External BVT Changes
			//Date businessDate) throws LMSException {
            Date businessDate, long bvtId) throws LMSException {
			//ENH_IS1297 Ends
		
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		Statement stmt = null;
		PreparedStatement pStmt = null;
		/** ****** JTEST FIX end **************  on 14DEC09*/
		ResultSet rSet = null;
		StringBuilder sBuf = new StringBuilder();
		String callMode = "E";
		
		//Do Sanity checks for conn==null,processId=0, businessDate==null
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		SimpleDateFormat formatter = new SimpleDateFormat(LMSConstants.DATE_FORMAT);
//		SimpleDateFormat formatter = new SimpleDateFormat(LMSConstants.DATE_FORMAT,Locale.US);
		/** ****** JTEST FIX end **************  on 14DEC09*/
		/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//		String bDate = formatter.format(businessDate);
		/** ****** FindBugs Fix End ***********  on 14DEC09*/
		
		ArrayList calendarCodes = null;

		try{	
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			stmt = conn.createStatement();
			/** ****** JTEST FIX end **************  on 14DEC09*/
			sBuf.append(" SELECT tmp.calendar, tmp.dat_start, tmp.cod_accr_cycle, tmp.dat_lastaccrual, ");
			sBuf.append(" tmp.dat_lastaccrualcalc, tmp.id_pool, tmp.cod_gl, ");
			sBuf.append(" CASE WHEN typ_accr = 'DAILY' THEN 1 WHEN typ_accr = 'WEEKLY' THEN 2 ");
			sBuf.append(" WHEN typ_accr = 'MONTHLY' THEN 3 WHEN typ_accr = 'YEARLY' THEN 4 ELSE 0 END typ_accr, ");
			sBuf.append(" CASE WHEN opt_accr IS NULL THEN 0 ELSE opt_accr + 0 END opt_accr, ");
			sBuf.append(" CASE WHEN unit1_accr IS NULL THEN -1 ELSE unit1_accr + 0 END unit1_accr, ");
			sBuf.append(" CASE WHEN unit2_accr IS NULL THEN -1 ELSE unit2_accr + 0 END unit2_accr, ");
			sBuf.append(" CASE WHEN cod_non_working_day = 'NEXT' THEN 'N'  ");
			sBuf.append(" WHEN cod_non_working_day = 'PREV' THEN 'P'ELSE 'C' END isholiday  ");
			sBuf.append(" FROM orate_accrual_cycle oac, (SELECT (SELECT cod_calendar ");
			
			//ENH_LIQ_0005 Starts
			//sBuf.append(" FROM orbicash_glmst WHERE cod_gl = hdr.cod_gl) calendar, dat_start, ");
			sBuf.append(" FROM olm_glmst WHERE cod_gl = hdr.cod_gl) calendar, dat_start, ");
			//ENH_LIQ_0005 Ends
			
			sBuf.append(" cod_accr_cycle, dat_lastaccrual, dat_lastaccrualcalc, id_pool, cod_gl ");
			sBuf.append(" FROM opool_poolhdr hdr WHERE id_pool IN  (SELECT DISTINCT id_pool  ");
			
			//ENH_IS1297 Starts...External BVT Changes
			//sBuf.append(" FROM opool_txnpoolhdr h WHERE h.nbr_processid = "+processId+")) tmp ");
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" FROM opool_txnpoolhdr h WHERE h.nbr_processid = "+processId+" and id_bvt = "+bvtId+")) tmp ");
			sBuf.append(" FROM opool_txnpoolhdr h WHERE h.nbr_processid = ? and id_bvt = ?)) tmp ");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			//ENH_IS1297 Ends
			
			sBuf.append(" WHERE oac.cod_accr_cycle = tmp.cod_accr_cycle ");

			if(logger.isDebugEnabled()){
				logger.debug(" calculateNextCycleAccrualDates -> "+sBuf.toString());
			}
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			rSet = stmt.executeQuery(sBuf.toString());
			pStmt = conn.prepareStatement(sBuf.toString());
			pStmt.setLong(1, processId);
			pStmt.setLong(2, bvtId);
			rSet = pStmt.executeQuery();
			/** ****** JTEST FIX end **************  on 14DEC09*/
			
			while(rSet.next()){
			    String calendarCode = rSet.getString(1);
				Date poolStartDate = rSet.getDate(2);
				String frequency = rSet.getString(3);
				Date lastAccrualDate = rSet.getDate(4);
				Date lastCalcAccrualDate = rSet.getDate(5);
				
				String idPool = rSet.getString(6);				
				String hostCode = rSet.getString(7);
				int typeFrequency = rSet.getInt(8);
				int frequencyInfo = rSet.getInt(9);
				int frequencyUnit1 = rSet.getInt(10);
				int frequencyUnit2 = rSet.getInt(11);
				String holidayFlag = rSet.getString(12);
				//set AccrualStartDate = lastAccrualDate + 1
				GregorianCalendar gDate = new GregorianCalendar();
				gDate.setTime(lastAccrualDate);
				gDate.add(Calendar.DATE,1);
				java.util.Date AccrualStartDate = gDate.getTime();
				//java.util.Date incDateUtil =  gDate.getTime();
			//	java.sql.Date AccrualStartDate = new java.sql.Date(incDateUtil.getTime());
				
				if(logger.isInfoEnabled()){
			        logger.info(" Adding information of pool ["+idPool+"] as [calendarCode] = ["+calendarCode+"]," +
			        " [poolStartDate] = ["+poolStartDate+"], [frequency] = ["+frequency+"], [lastAccrualDate] = ["+lastAccrualDate+"]" +
			        ", [lastCalcAccrualDate] = ["+lastCalcAccrualDate+"], [hostCode] = ["+hostCode+"]+" 
			        +"{ Frequency["+typeFrequency+"], FrequencyInfo["+frequencyInfo+"], "
					+"FrequencyUnit1["+frequencyUnit1+"], FrequencyUnit2["+frequencyUnit2+"], "
					+"HolidayFlag["+holidayFlag+"],[AccrualStartDate] = ["+ AccrualStartDate+"]");
			    }
				calendarCodes = new ArrayList();
				calendarCodes.add(calendarCode);

				//declaring a return parameter 
				java.util.Date[] nextActionDates = null;
				if(lastAccrualDate == null || lastCalcAccrualDate == null){
				    callMode = "M";
				}
				if(callMode.equals("E")){
					nextActionDates = 
						ExecutionUtility.getNextActionDate(calendarCodes, lastAccrualDate, lastCalcAccrualDate
										 , typeFrequency, frequencyInfo, frequencyUnit1, frequencyUnit2,null
										 , holidayFlag, businessDate, callMode);//ENH_IS686 changes made by satish for Posting Cycle added one argument null for Posting Calendar
				}
				else{
				    nextActionDates = 
						ExecutionUtility.getNextActionDate(calendarCodes, poolStartDate, businessDate
										 , typeFrequency, frequencyInfo, frequencyUnit1, frequencyUnit2,null
										 , holidayFlag, businessDate, callMode);//ENH_IS686 changes made by satish for Posting Cycle added one argument null
				}
				
				Date dateArray[] = new Date[3];

				
				dateArray[0] = nextActionDates[0];//Date_accrual
				dateArray[1] = nextActionDates[1];//Adjusted Accrual
				dateArray[2] = AccrualStartDate;// Accrual date start
				accrualDatesCache.put(idPool,dateArray);
				if (logger.isDebugEnabled()) {
                    logger.debug(" KEY:["+idPool+"] & VALUE:["+dateArray[0]+", "+dateArray[1]+dateArray[2]+"] added ");                                 
                }
			}
		}
		catch(Exception e){
			logger.fatal("Exception in calculateNextCycleAccrualDates ", e);
			  //ENH_IS1059  Starts
			  //throw new LMSException("101302", e.toString());
			  throw new LMSException("LMS01000","Error occurred while calculating next cycle Accrual dates for Process Id :- "+processId+".", e);
			  //ENH_IS1059  Ends
		}
		finally{
			LMSConnectionUtility.close(rSet);
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			LMSConnectionUtility.close(stmt);
			LMSConnectionUtility.close(pStmt);
			/** ****** JTEST FIX end **************  on 14DEC09*/
		}
    }
	
	// Issue 15954 fix starts......... Added new method for getting previous cycle posting dates
	/**
	 * This method provides the previousCyclePostingDates given an array of cod_gl, an array of id_pool, nbr_processId
	 * This helps in calculating the next dat_posting and dat_adjusted_posting
	 *
	 * @param conn
	 * @param poolId
	 * @param processId
	 * @param businessDate 
	 * @return
	 */
	public void calculatePreviousCyclePostingDates(Connection conn, long processId,
			
			Date businessDate, long bvtId) throws LMSException {
			
		PreparedStatement pStmt = null;
	
		ResultSet rSet = null;
		StringBuffer sBuf = new StringBuffer();
		String callMode = "E";
		GregorianCalendar gDate =null;
		java.util.Date incDateUtil = null;
		java.util.Date PostingStartDate = null;

		java.util.Date firstPostingDate = null;//ENH_IS704 holds firstPostingDate or postingDate after change in postingcycle
		String pstngCycleChng = "N";//ENH_IS704 pstngCycleChng will indicate if posting cycle has changed
		//JTest Changes 11.5.1.Starts
		//ArrayList calendarCodes = null;
		//JTest Changes 11.5.1.Ends

		try{	
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			stmt = conn.createStatement();
			/** ****** JTEST FIX end **************  on 14DEC09*/
			sBuf.append(" SELECT tmp.calendar, tmp.dat_start, tmp.cod_post_cycle, tmp.dat_lastposting, ");
			sBuf.append(" tmp.dat_lastpostingcalc, tmp.id_pool, tmp.cod_gl, ");
			sBuf.append(" CASE WHEN typ_post = 'DAILY' THEN 1 WHEN typ_post = 'WEEKLY' THEN 2 ");
			sBuf.append(" WHEN typ_post = 'MONTHLY' THEN 3 WHEN typ_post = 'YEARLY' THEN 4 WHEN typ_post = 'CALENDAR'THEN 5 ELSE 0 END typ_post, ");//ENH_IS686 For posting calendar frequency = 5
			sBuf.append(" CASE WHEN opt_post IS NULL THEN 0 ELSE opt_post + 0 END opt_post, CASE WHEN unit1_post IS NULL ");
			sBuf.append(" THEN -1 ELSE unit1_post + 0 END unit1_post, CASE WHEN unit2_post IS NULL THEN -1 ELSE unit2_post + 0 ");
			sBuf.append(" END unit2_post, CASE WHEN cod_non_working_day = 'NEXT' THEN 'N' WHEN cod_non_working_day = 'PREV' THEN 'P' ");
			// Li & Fung Changes Start
			//sBuf.append(" ELSE 'C' END isholiday, nbr_settle_days, nbr_delay_days_cr, nbr_delay_days_dr, opc.FLG_SAMEVALDAT,COD_SWEEPCALENDAR ");
			sBuf.append(" ELSE 'C' END isholiday, nbr_settle_days, nbr_delay_days_cr, nbr_delay_days_dr, tmp.FLG_SAMEVALDAT,COD_SWEEPCALENDAR ");
			// Li & Fung Changes End
			//Issue fix for BVT 3043 starts
			//sBuf.append(" ,tmp.DAT_FIRST_POSTING	, tmp.PSTCYCLE_CHNG ");//ENH_IS704 fetch PSTCYCLE_CHNG and DAT_FIRST_POSTING
			sBuf.append(" ,tmp.DAT_FIRST_POSTING	, tmp.PSTCYCLE_CHNG ,FLG_ISWORKINGDAY");
			//Issue fix for BVT 3043 Ends
			//sBuf.append(" FROM orate_posting_cycle opc, ( SELECT (SELECT cod_calendar ");
			  
			// Changes done Rakesh start
			sBuf.append(" FROM orate_posting_cycle opc,  ");
			sBuf.append("  (SELECT DISTINCT(SELECT cod_calendar  FROM olm_glmst  WHERE cod_gl = hdr.cod_gl) calendar, hdr.dat_start, hdr.cod_post_cycle, hdr.dat_lastposting, hdr.dat_lastpostingcalc, hdr.id_pool, ");
			
			// Li & Fung Changes Start
			//sBuf.append(" (Case  when (txn.DAT_BUSINESS_POOLRUN  <= hdr.dat_lastposting)  then hdr.dat_lastposting  else hdr.dat_first_posting  END )dat_first_posting, hdr.pstcycle_chng, hdr.cod_gl ");
			//sBuf.append(" (Case  when (txn.DAT_BUSINESS_POOLRUN  <= hdr.dat_lastposting)  then hdr.dat_lastposting  else hdr.dat_first_posting  END )dat_first_posting, hdr.pstcycle_chng, hdr.cod_gl ,txn.FLG_SAMEVALDAT");
			//JPMC_PERFORMANCE_ISSUE_FIX
			sBuf.append(" NVL(hdr.dat_lastposting,hdr.dat_first_posting) dat_first_posting, hdr.pstcycle_chng, hdr.cod_gl ,txn.FLG_SAMEVALDAT");
			//JPMC_PERFORMANCE_ISSUE_FIX
			// Li & Fung Changes End
			sBuf.append("  FROM opool_poolhdr hdr ,OPOOL_TXNPOOLHDR txn ");
			//sBuf.append(" WHERE hdr.id_pool IN (SELECT DISTINCT id_pool   FROM opool_txnpoolhdr h WHERE h.nbr_processid = ?  AND id_bvt = ? ) AND txn.DAT_BUSINESS_POOLRUN  <= hdr.dat_lastposting AND hdr.id_rootpool = txn.id_rootpool) tmp ");
			//JPMC_PERFORMANCE_ISSUE_FIX
			sBuf.append(" WHERE hdr.id_pool = txn.id_pool AND txn.nbr_processid = ?  AND txn.id_bvt = ? AND txn.DAT_BUSINESS_POOLRUN  <= hdr.dat_lastposting AND hdr.id_rootpool = txn.id_rootpool) tmp ");
			//JPMC_PERFORMANCE_ISSUE_FIX
			sBuf.append(" WHERE opc.cod_post_cycle = tmp.cod_post_cycle ");
			
			
			// Changes done Rakesh ends
			
			/*// Rakesh1111
			
			//ENH_LIQ_0005 Starts
			//sBuf.append(" FROM orbicash_glmst WHERE cod_gl = hdr.cod_gl) calendar, dat_start, ");
			sBuf.append(" FROM olm_glmst WHERE cod_gl = hdr.cod_gl) calendar, dat_start, ");
			//ENH_LIQ_0005 Ends
			
			sBuf.append(" cod_post_cycle, dat_lastposting, dat_lastpostingcalc, id_pool, ");
//ENH_IS997
			sBuf.append(" DAT_FIRST_POSTING	, PSTCYCLE_CHNG, ");//ENH_IS704 fetch PSTCYCLE_CHNG and DAT_FIRST_POSTING 
			
//ENH_IS997
			sBuf.append(" cod_gl FROM opool_poolhdr hdr WHERE id_pool IN  (SELECT DISTINCT id_pool  ");
			
			//ENH_IS1297 Starts...External BVT Changes
			//sBuf.append(" FROM opool_txnpoolhdr h WHERE h.nbr_processid = "+processId+")) tmp WHERE opc.cod_post_cycle = tmp.cod_post_cycle ");
			
//			sBuf.append(" FROM opool_txnpoolhdr h WHERE h.nbr_processid = "+processId+" and id_bvt = "+bvtId+")) tmp WHERE opc.cod_post_cycle = tmp.cod_post_cycle ");
			sBuf.append(" FROM opool_txnpoolhdr h WHERE h.nbr_processid = ? and id_bvt = ?)) tmp WHERE opc.cod_post_cycle = tmp.cod_post_cycle ");
			/** ****** JTEST FIX end **************  on 14DEC09
			 */ // Rakesh22222222
			//ENH_IS1297 Ends
			                       
			if(logger.isDebugEnabled()){
				logger.debug(" calculateNextCyclePostingDates -> "+sBuf.toString());
			}
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			rSet = stmt.executeQuery(sBuf.toString());
			pStmt = conn.prepareStatement(sBuf.toString());
			pStmt.setLong(1, processId);
			pStmt.setLong(2, bvtId);
			rSet = pStmt.executeQuery();
			/** ****** JTEST FIX end **************  on 14DEC09*/
			gDate = new GregorianCalendar();
			while(rSet.next()){
				String idPool = rSet.getString(6);				
			    String calendarCode = rSet.getString(1);
				Date poolStartDate = rSet.getDate(2);
				String frequency = rSet.getString(3);
				Date lastPostingDate = rSet.getDate(4);
				Date lastCalcPostingDate = rSet.getDate(5);
				String hostCode = rSet.getString(7);
				int typeFrequency = rSet.getInt(8);
				int frequencyInfo = rSet.getInt(9);
				int frequencyUnit1 = rSet.getInt(10);
				int frequencyUnit2 = rSet.getInt(11);
				String holidayFlag = rSet.getString(12);
				int nbr_settle_days = rSet.getInt(13);
				int nbr_delay_days_cr = rSet.getInt(14);
				int nbr_delay_days_dr = rSet.getInt(15);

				String Cod_postcalendar = rSet.getString("COD_SWEEPCALENDAR");//ENH_IS686 starts changes made by satish for Posting Cycle added one field Calendar code for Posting Calendar
				String Flg_samevaldat = rSet.getString("FLG_SAMEVALDAT");
				firstPostingDate = rSet.getDate("DAT_FIRST_POSTING");//ENH_IS704 GET THE DAT_FIRST_POSTING
				pstngCycleChng = rSet.getString("PSTCYCLE_CHNG");	//ENH_IS704 fetching  PSTCYCLE_CHNG

				//set PostingStartDate = lastPostingDate + 1 if lastPostingDate is not null
				//or set PostingStartDate = poolStartDate
				boolean isWorkingDay = rSet.getString("FLG_ISWORKINGDAY").equalsIgnoreCase("Y") ? true : false;//changes for BVT Prior cycle Settlement date Calculation 20553
				if(lastPostingDate != null)
				{
					gDate.setTime(lastPostingDate);
					gDate.add(Calendar.DATE,1);
					PostingStartDate=gDate.getTime();
					//incDateUtil =  gDate.getTime();
					//PostingStartDate = new java.sql.Date(incDateUtil.getTime());
				}
				else
				{
					PostingStartDate = poolStartDate;
				}
				if(logger.isInfoEnabled()){
			        logger.info(" Adding information of pool ["+idPool+"] as [calendarCode] = ["+calendarCode+"]," +
			        " [poolStartDate] = ["+poolStartDate+"], [frequency] = ["+frequency+"], [lastPostingDate] = ["+lastPostingDate+"]" +
			        ", [lastCalcPostingDate] = ["+lastCalcPostingDate+"], [hostCode] = ["+hostCode+"]+" 
			        +"{ Frequency["+typeFrequency+"], FrequencyInfo["+frequencyInfo+"], "
					+"FrequencyUnit1["+frequencyUnit1+"], FrequencyUnit2["+frequencyUnit2+"], "
					+"HolidayFlag["+holidayFlag+"]nbr_settle_days["+nbr_settle_days+"], nbr_delay_days_cr["+nbr_delay_days_cr
					+"],nbr_delay_days_dr["+nbr_delay_days_dr+"],Cod_postcalendar["+Cod_postcalendar
					+"],[PostingStartDate]=["+PostingStartDate+"]firstPostingDate["+firstPostingDate+"]pstngCycleChng[" + pstngCycleChng + "]Flg_samevaldat["+Flg_samevaldat+"]");//ENH_IS686 ends
			    }
				//JTest Changes 11.5.1.Starts
				//calendarCodes = new ArrayList();
				//calendarCodes.add(calendarCode);
				//JTest Changes 11.5.1.Ends
 //ENH_IS997 starts
				/**
				 * ENH_IS704 starts
				 * if it is the firstCycle ie lastPostingDate == null || lastCalcPostingDate == null
				 *  || if PSTCYCLE_CHNG is Y..ie if posting cycle has changed then 
				 *   donot call calendar service for the above cases.
				 *   Use DAT_FIRST_POSTING as the Posting Date
				 *   Add settlementDays to get the settlementDate, crDelayDays to get the crDelaydate
				 *   drDelayDays to get the drDelayDate 
				 */
				
				
				java.util.Date[] nextActionDates = null;
				
				/*
				if(lastPostingDate == null || lastCalcPostingDate == null || "Y".equalsIgnoreCase(pstngCycleChng)){
				    callMode = "M";				       				    
				}
//ENH_IS997 ends
				// declaring a return parameter 
				java.util.Date[] nextActionDates = null;
				if(callMode.equals("E")){
					nextActionDates = 
						ExecutionUtility.getNextActionDate(calendarCodes, lastPostingDate, lastCalcPostingDate
										 , typeFrequency, frequencyInfo, frequencyUnit1, frequencyUnit2,Cod_postcalendar
										 , holidayFlag, businessDate, callMode);//ENH_IS686 changes made by satish for Posting Calendar added an argument Cod_postcalendar for Posting Calendar
					java.util.Date tmp_date = nextActionDates[0];
					nextActionDates = null;
					nextActionDates = new java.util.Date[8];
					//set date posting
					nextActionDates[0] = tmp_date;
					tmp_date = null;
					//set date adjustment posting
					nextActionDates[1] = nextActionDates[0];
				}
				else{
                   */
					/* ENH_IS704 
					 * if this is the firstCycle or cycle has changed consider DAT_FIRST_POSTING AS PostingDate
					 * Add settlementDays, creditDelay Days and debitDelay days to get the nextSettlementDate/crDelayDate and DrDelayDate
					 * commented the line below as no need to call calendarService 
					nextActionDates = 
						ExecutionUtility.getNextActionDate(calendarCodes, poolStartDate, businessDate
										 , typeFrequency, frequencyInfo, frequencyUnit1, frequencyUnit2,Cod_postcalendar
										 , holidayFlag, businessDate, callMode);//ENH_IS686 changes made by satish for Posting Calendar added an argument Cod_postcalendar for Posting Calendar
					*/
					nextActionDates = new java.util.Date[8];
					//set date posting
					nextActionDates[0] = firstPostingDate;
					//set date adjustment posting
					nextActionDates[1] = firstPostingDate;									
				
			
			     // }
				/* moved the following into if(callMode.equals("E")){ block as only if calendarService is called , perform the below task 
				java.util.Date tmp_date = nextActionDates[0];
				nextActionDates = null;
				nextActionDates = new java.util.Date[8];
				//set date posting
				nextActionDates[0] = tmp_date;
				tmp_date = null;
				//set date adjustment posting
				nextActionDates[1] = nextActionDates[0];
				*/
				//ENH_IS704 ends
//ENH_IS997 ends
					//JPMC_BVT_ISSUE STARTS 
				/*GregorianCalendar temp = new GregorianCalendar();
				temp.setTime(nextActionDates[0]);
				temp.add(Calendar.DATE, nbr_settle_days);
				//set date post settlement
				nextActionDates[2] = temp.getTime();
				temp = null;*/
				//changes for BVT Prior cycle Settlement date Calculation 20553
				ArrayList<String> calendarCodeList = new ArrayList<String>();
				calendarCodeList.add(calendarCode);
				
				java.util.Date settlementDate_post = ExecutionUtility.getNthWorkingDay(calendarCodeList, nextActionDates[0], holidayFlag.charAt(0),  nbr_settle_days,isWorkingDay);
				java.util.Date crDelayDate_post = ExecutionUtility.getNthWorkingDay(calendarCodeList, settlementDate_post,	holidayFlag.charAt(0), nbr_delay_days_cr,isWorkingDay);
				java.util.Date drDelayDate_post = ExecutionUtility.getNthWorkingDay(calendarCodeList, settlementDate_post,	holidayFlag.charAt(0), nbr_delay_days_dr,isWorkingDay);
				
				// set date post settlement
				/* Commented for 20553
				 * nextActionDates[2] = temp.getTime();*/
				nextActionDates[2] = settlementDate_post;
//				temp = null;
				//set date settlement Cr 
				nextActionDates[3] = crDelayDate_post;
				/* Commented for 20553
				 * nextActionDates[3] = 
					ExecutionUtility.getAnotherWorkingDay((String)calendarCodes.get(0), nextActionDates[0], 
											holidayFlag.charAt(0), 0, 0, nbr_settle_days + nbr_delay_days_cr);*/
				//set date settlement Dr
				nextActionDates[4] = drDelayDate_post;
				/* Commented for 20553
				 * nextActionDates[4] = 
				    ExecutionUtility.getAnotherWorkingDay((String)calendarCodes.get(0), nextActionDates[0], 
											holidayFlag.charAt(0), 0, 0, nbr_settle_days + nbr_delay_days_dr);*/
				//changes for BVT Prior cycle Settlement date Calculation 20553
				// set posting start date
				nextActionDates[5] = PostingStartDate;
				
				if(Flg_samevaldat.equalsIgnoreCase("Y"))
				{
					//set  dat value Cr = date settlement Cr 
					nextActionDates[6]=nextActionDates[3];
					//set  dat value Dr = date settlement Dr
					nextActionDates[7]=nextActionDates[4];
				}
				else
				{
								
					gDate = new GregorianCalendar();
					gDate.setTime(nextActionDates[0]);
					gDate.add(Calendar.DATE,1);
					incDateUtil =  gDate.getTime();
					java.sql.Date ValueDate = new java.sql.Date(incDateUtil.getTime());
					//set  dat value Cr = date posting + 1
			
					nextActionDates[6] = ValueDate;
					//set  dat value Dr = date posting + 1
					nextActionDates[7] = ValueDate;
					
				}

				if (logger.isDebugEnabled()) {
                    logger.debug(" KEY:["+idPool+"] & VALUE:["+nextActionDates[0]+", "+nextActionDates[1]
                                 +","+nextActionDates[2]+","+nextActionDates[3]+" ,"+nextActionDates[4]+","+nextActionDates[5]+","+nextActionDates[6]+","+nextActionDates[7]+"] processed");                                 
                }
				//ENH_10.3_154 Starts
				//postingDatesCache.put(idPool, nextActionDates);
				postingDatesCachePrev.put(idPool, nextActionDates);
				//ENH_10.3_154 Ends
			}
		}
		catch(Exception e){
			logger.fatal("Exception in calculateNextCyclePostingDates ", e);
			  
			  //ENH_IS1059  Starts
			  //throw new LMSException("101302", e.toString());
			  throw new LMSException("LMS01000","Error occurred while calculating next cycle Posting dates for Process Id :- "+processId+".", e);
			  //ENH_IS1059  Ends
		}
		finally{
			LMSConnectionUtility.close(rSet);
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			LMSConnectionUtility.close(stmt);
			LMSConnectionUtility.close(pStmt);
			/** ****** JTEST FIX end **************  on 14DEC09*/
		}		
    }
	
	// Issue 15954 fix .......... ends
	/**
	 * This method provides the nextCyclePostingDates given an array of cod_gl, an array of id_pool, nbr_processId
	 * This helps in calculating the next dat_posting and dat_adjusted_posting
	 *
	 * @param conn
	 * @param poolId
	 * @param processId
	 * @param businessDate 
	 * @return
	 */
	public void calculateNextCyclePostingDates(Connection conn, long processId,
			//ENH_IS1297 Starts...Externla BVT Changes
			//Date businessDate) throws LMSException {
			Date businessDate, long bvtId) throws LMSException {
			//ENH_IS1297 Ends

		/** ****** JTEST FIX start ************  on 14DEC09*/
//		Statement stmt = null;
		PreparedStatement pStmt = null;
		/** ****** JTEST FIX end **************  on 14DEC09*/
		ResultSet rSet = null;
		StringBuilder sBuf = new StringBuilder();
		String callMode = "E";
		GregorianCalendar gDate =null;
		java.util.Date incDateUtil = null;
		java.util.Date PostingStartDate = null;
//ENH_IS997 starts
		java.util.Date firstPostingDate = null;//ENH_IS704 holds firstPostingDate or postingDate after change in postingcycle
		String pstngCycleChng = "N";//ENH_IS704 pstngCycleChng will indicate if posting cycle has changed
//ENH_IS997 ends
		
		//Do Sanity checks for conn==null,processId=0, businessDate==null
		//SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		//String bDate = formatter.format(businessDate);
		
		ArrayList calendarCodes = null;
		//java.util.Date[] postingDates = null;

		try{	
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			stmt = conn.createStatement();
			/** ****** JTEST FIX end **************  on 14DEC09*/
			sBuf.append(" SELECT tmp.calendar, tmp.dat_start, tmp.cod_post_cycle, tmp.dat_lastposting, ");
			sBuf.append(" tmp.dat_lastpostingcalc, tmp.id_pool, tmp.cod_gl, ");
			sBuf.append(" CASE WHEN typ_post = 'DAILY' THEN 1 WHEN typ_post = 'WEEKLY' THEN 2 ");
			sBuf.append(" WHEN typ_post = 'MONTHLY' THEN 3 WHEN typ_post = 'YEARLY' THEN 4 WHEN typ_post = 'CALENDAR'THEN 5 ELSE 0 END typ_post, ");//ENH_IS686 For posting calendar frequency = 5
			sBuf.append(" CASE WHEN opt_post IS NULL THEN 0 ELSE opt_post + 0 END opt_post, CASE WHEN unit1_post IS NULL ");
			sBuf.append(" THEN -1 ELSE unit1_post + 0 END unit1_post, CASE WHEN unit2_post IS NULL THEN -1 ELSE unit2_post + 0 ");
			sBuf.append(" END unit2_post, CASE WHEN cod_non_working_day = 'NEXT' THEN 'N' WHEN cod_non_working_day = 'PREV' THEN 'P' ");
//ENH_IS997
//Retro_Mashreq
			//sBuf.append(" ELSE 'C' END isholiday, nbr_settle_days, nbr_delay_days_cr, nbr_delay_days_dr, opc.FLG_SAMEVALDAT,COD_SWEEPCALENDAR ");
			sBuf.append(" ELSE 'S' END isholiday, nbr_settle_days, nbr_delay_days_cr, nbr_delay_days_dr, opc.FLG_SAMEVALDAT,COD_SWEEPCALENDAR ");//MASHREQ_ISS4711
			sBuf.append(" ,tmp.DAT_FIRST_POSTING	, tmp.PSTCYCLE_CHNG ");//ENH_IS704 fetch PSTCYCLE_CHNG and DAT_FIRST_POSTING
//ENH_IS997
			sBuf.append(" FROM orate_posting_cycle opc, ( SELECT (SELECT cod_calendar ");
			
			//ENH_LIQ_0005 Starts
			//sBuf.append(" FROM orbicash_glmst WHERE cod_gl = hdr.cod_gl) calendar, dat_start, ");
			sBuf.append(" FROM olm_glmst WHERE cod_gl = hdr.cod_gl) calendar, dat_start, ");
			//ENH_LIQ_0005 Ends
			
			sBuf.append(" cod_post_cycle, dat_lastposting, dat_lastpostingcalc, id_pool, ");
//ENH_IS997
			sBuf.append(" DAT_FIRST_POSTING	, PSTCYCLE_CHNG, ");//ENH_IS704 fetch PSTCYCLE_CHNG and DAT_FIRST_POSTING 
//ENH_IS997
			sBuf.append(" cod_gl FROM opool_poolhdr hdr WHERE id_pool IN  (SELECT DISTINCT id_pool  ");
			
			//ENH_IS1297 Starts...External BVT Changes
			//sBuf.append(" FROM opool_txnpoolhdr h WHERE h.nbr_processid = "+processId+")) tmp WHERE opc.cod_post_cycle = tmp.cod_post_cycle ");
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" FROM opool_txnpoolhdr h WHERE h.nbr_processid = "+processId+" and id_bvt = "+bvtId+")) tmp WHERE opc.cod_post_cycle = tmp.cod_post_cycle ");
			sBuf.append(" FROM opool_txnpoolhdr h WHERE h.nbr_processid = ? and id_bvt = ?)) tmp WHERE opc.cod_post_cycle = tmp.cod_post_cycle ");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			//ENH_IS1297 Ends

			if(logger.isDebugEnabled()){
				logger.debug(" calculateNextCyclePostingDates -> "+sBuf.toString());
			}
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			rSet = stmt.executeQuery(sBuf.toString());
			pStmt = conn.prepareStatement(sBuf.toString());
			pStmt.setLong(1, processId);
			pStmt.setLong(2, bvtId);
			rSet = pStmt.executeQuery();
			/** ****** JTEST FIX end **************  on 14DEC09*/
			gDate = new GregorianCalendar();
			while(rSet.next()){
				String idPool = rSet.getString(6);				
			    String calendarCode = rSet.getString(1);
				Date poolStartDate = rSet.getDate(2);
				String frequency = rSet.getString(3);
				Date lastPostingDate = rSet.getDate(4);
				Date lastCalcPostingDate = rSet.getDate(5);
				String hostCode = rSet.getString(7);
				int typeFrequency = rSet.getInt(8);
				int frequencyInfo = rSet.getInt(9);
				int frequencyUnit1 = rSet.getInt(10);
				int frequencyUnit2 = rSet.getInt(11);
				String holidayFlag = rSet.getString(12);
				int nbr_settle_days = rSet.getInt(13);
				int nbr_delay_days_cr = rSet.getInt(14);
				int nbr_delay_days_dr = rSet.getInt(15);
//ENH_IS997
				String Cod_postcalendar = rSet.getString("COD_SWEEPCALENDAR");//ENH_IS686 starts changes made by satish for Posting Cycle added one field Calendar code for Posting Calendar
				String Flg_samevaldat = rSet.getString("FLG_SAMEVALDAT");
				firstPostingDate = rSet.getDate("DAT_FIRST_POSTING");//ENH_IS704 GET THE DAT_FIRST_POSTING
				pstngCycleChng = rSet.getString("PSTCYCLE_CHNG");	//ENH_IS704 fetching  PSTCYCLE_CHNG
//ENH_IS997
				//set PostingStartDate = lastPostingDate + 1 if lastPostingDate is not null
				//or set PostingStartDate = poolStartDate
				if(lastPostingDate != null)
				{
					gDate.setTime(lastPostingDate);
					gDate.add(Calendar.DATE,1);
					PostingStartDate=gDate.getTime();
					//incDateUtil =  gDate.getTime();
					//PostingStartDate = new java.sql.Date(incDateUtil.getTime());
				}
				else
				{
					PostingStartDate = poolStartDate;
				}
				if(logger.isInfoEnabled()){
			        logger.info(" Adding information of pool ["+idPool+"] as [calendarCode] = ["+calendarCode+"]," +
			        " [poolStartDate] = ["+poolStartDate+"], [frequency] = ["+frequency+"], [lastPostingDate] = ["+lastPostingDate+"]" +
			        ", [lastCalcPostingDate] = ["+lastCalcPostingDate+"], [hostCode] = ["+hostCode+"]+" 
			        +"{ Frequency["+typeFrequency+"], FrequencyInfo["+frequencyInfo+"], "
					+"FrequencyUnit1["+frequencyUnit1+"], FrequencyUnit2["+frequencyUnit2+"], "
					+"HolidayFlag["+holidayFlag+"]nbr_settle_days["+nbr_settle_days+"], nbr_delay_days_cr["+nbr_delay_days_cr
					+"],nbr_delay_days_dr["+nbr_delay_days_dr+"],Cod_postcalendar["+Cod_postcalendar
					+"],[PostingStartDate]=["+PostingStartDate+"]firstPostingDate["+firstPostingDate+"]pstngCycleChng[" + pstngCycleChng + "]");//ENH_IS686 ends
			    }
				calendarCodes = new ArrayList();
				calendarCodes.add(calendarCode);
 //ENH_IS997 starts
				/**
				 * ENH_IS704 starts
				 * if it is the firstCycle ie lastPostingDate == null || lastCalcPostingDate == null
				 *  || if PSTCYCLE_CHNG is Y..ie if posting cycle has changed then 
				 *   donot call calendar service for the above cases.
				 *   Use DAT_FIRST_POSTING as the Posting Date
				 *   Add settlementDays to get the settlementDate, crDelayDays to get the crDelaydate
				 *   drDelayDays to get the drDelayDate 
				 */
				 if(lastPostingDate == null || lastCalcPostingDate == null || "Y".equalsIgnoreCase(pstngCycleChng)){
				    callMode = "M";				       				    
				}
//ENH_IS997 ends
				// declaring a return parameter 
				java.util.Date[] nextActionDates = null;
				if(callMode.equals("E")){
					nextActionDates = 
						ExecutionUtility.getNextActionDate(calendarCodes, lastPostingDate, lastCalcPostingDate
										 , typeFrequency, frequencyInfo, frequencyUnit1, frequencyUnit2,Cod_postcalendar
										 , holidayFlag, businessDate, callMode);//ENH_IS686 changes made by satish for Posting Calendar added an argument Cod_postcalendar for Posting Calendar
					java.util.Date tmp_date = nextActionDates[0];
					nextActionDates = null;
					nextActionDates = new java.util.Date[8];
					//set date posting
					nextActionDates[0] = tmp_date;
					tmp_date = null;
					//set date adjustment posting
					nextActionDates[1] = nextActionDates[0];
				}
				else{
//ENH_IS997 starts
					/* ENH_IS704 
					 * if this is the firstCycle or cycle has changed consider DAT_FIRST_POSTING AS PostingDate
					 * Add settlementDays, creditDelay Days and debitDelay days to get the nextSettlementDate/crDelayDate and DrDelayDate
					 * commented the line below as no need to call calendarService 
					nextActionDates = 
						ExecutionUtility.getNextActionDate(calendarCodes, poolStartDate, businessDate
										 , typeFrequency, frequencyInfo, frequencyUnit1, frequencyUnit2,Cod_postcalendar
										 , holidayFlag, businessDate, callMode);//ENH_IS686 changes made by satish for Posting Calendar added an argument Cod_postcalendar for Posting Calendar
					*/
					nextActionDates = new java.util.Date[8];
					//set date posting
					nextActionDates[0] = firstPostingDate;
					//set date adjustment posting
					nextActionDates[1] = firstPostingDate;									
				}
				/* moved the following into if(callMode.equals("E")){ block as only if calendarService is called , perform the below task 
				java.util.Date tmp_date = nextActionDates[0];
				nextActionDates = null;
				nextActionDates = new java.util.Date[8];
				//set date posting
				nextActionDates[0] = tmp_date;
				tmp_date = null;
				//set date adjustment posting
				nextActionDates[1] = nextActionDates[0];
				*/
				//ENH_IS704 ends
//ENH_IS997 ends
				GregorianCalendar temp = new GregorianCalendar();
				temp.setTime(nextActionDates[0]);
				temp.add(Calendar.DATE, nbr_settle_days);
				//set date post settlement
				nextActionDates[2] = temp.getTime();
				temp = null;
				//set date settlement Cr 
				nextActionDates[3] = 
					ExecutionUtility.getAnotherWorkingDay((String)calendarCodes.get(0), nextActionDates[0], 
											holidayFlag.charAt(0), 0, 0, nbr_settle_days + nbr_delay_days_cr);
				//set date settlement Dr
				nextActionDates[4] = 
				    ExecutionUtility.getAnotherWorkingDay((String)calendarCodes.get(0), nextActionDates[0], 
											holidayFlag.charAt(0), 0, 0, nbr_settle_days + nbr_delay_days_dr);
				//set posting start date
				nextActionDates[5] = PostingStartDate;
				
				if(Flg_samevaldat.equalsIgnoreCase("Y"))
				{
					//set  dat value Cr = date settlement Cr 
					nextActionDates[6]=nextActionDates[3];
					//set  dat value Dr = date settlement Dr
					nextActionDates[7]=nextActionDates[4];
				}
				else
				{
								
					gDate = new GregorianCalendar();
					gDate.setTime(nextActionDates[0]);
					gDate.add(Calendar.DATE,1);
					incDateUtil =  gDate.getTime();
					java.sql.Date ValueDate = new java.sql.Date(incDateUtil.getTime());
					//set  dat value Cr = date posting + 1
			
					nextActionDates[6] = ValueDate;
					//set  dat value Dr = date posting + 1
					nextActionDates[7] = ValueDate;
					
				}

				if (logger.isDebugEnabled()) {
                    logger.debug(" KEY:["+idPool+"] & VALUE:["+nextActionDates[0]+", "+nextActionDates[1]
                                 +","+nextActionDates[2]+","+nextActionDates[3]+" ,"+nextActionDates[4]+","+nextActionDates[5]+","+nextActionDates[6]+","+nextActionDates[7]+"] processed");                                 
                }
				//ENH_10.3_154 Starts
				//postingDatesCache.put(idPool, nextActionDates);
				postingDatesCacheCurr.put(idPool, nextActionDates);
				//ENH_10.3_154 Ends
			}
		}
		catch(Exception e){
			logger.fatal("Exception in calculateNextCyclePostingDates ", e);
			  
			  //ENH_IS1059  Starts
			  //throw new LMSException("101302", e.toString());
			  throw new LMSException("LMS01000","Error occurred while calculating next cycle Posting dates for Process Id :- "+processId+".", e);
			  //ENH_IS1059  Ends
		}
		finally{
			LMSConnectionUtility.close(rSet);
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			LMSConnectionUtility.close(stmt);
			LMSConnectionUtility.close(pStmt);
			/** ****** JTEST FIX end **************  on 14DEC09*/
		}		
    }

	//ENH_10.3_154 Starts
	public void calculateNextBVTCyclePostingDates(Connection conn, long processId,
			Date businessDate, long bvtId) throws LMSException {
		PreparedStatement pStmt = null;
		ResultSet rSet = null;
		StringBuilder sBuf = new StringBuilder();
		String callMode = "M";
		GregorianCalendar gDate =null;
		java.util.Date incDateUtil = null;
		java.util.Date PostingStartDate = null;
		ArrayList calendarCodes = null;

		try{	
			//iRelease_Issue_no_148
			/*sBuf.append(" SELECT gl.cod_calendar      ,");
			sBuf.append("  gl.cod_bvt_post_cycle     ,");
			sBuf.append("  tmp.participant_id            ,");
			sBuf.append("  gl.cod_gl             ,");
			sBuf.append("  CASE");
			sBuf.append("    WHEN opc.typ_post = 'DAILY'");
			sBuf.append("    THEN 1");
			sBuf.append("    WHEN opc.typ_post = 'WEEKLY'");
			sBuf.append("    THEN 2");
			sBuf.append("    WHEN opc.typ_post = 'MONTHLY'");
			sBuf.append("    THEN 3");
			sBuf.append("    WHEN opc.typ_post = 'YEARLY'");
			sBuf.append("    THEN 4");
			sBuf.append("    WHEN opc.typ_post = 'CALENDAR'");
			sBuf.append("    THEN 5");
			sBuf.append("    ELSE 0");
			sBuf.append("  END typ_post,");
			sBuf.append("  CASE");
			sBuf.append("    WHEN opc.opt_post IS NULL");
			sBuf.append("    THEN 0");
			sBuf.append("    ELSE opc.opt_post + 0");
			sBuf.append("  END opt_post,");
			sBuf.append("  CASE");
			sBuf.append("    WHEN opc.unit1_post IS NULL");
			sBuf.append("    THEN                -1");
			sBuf.append("    ELSE opc.unit1_post + 0");
			sBuf.append("  END unit1_post,");
			sBuf.append("  CASE");
			sBuf.append("    WHEN opc.unit2_post IS NULL");
			sBuf.append("    THEN                -1");
			sBuf.append("    ELSE opc.unit2_post + 0");
			sBuf.append("  END unit2_post,");
			sBuf.append("  CASE");
			sBuf.append("    WHEN opc.cod_non_working_day = 'NEXT'");
			sBuf.append("    THEN 'N'");
			sBuf.append("    WHEN opc.cod_non_working_day = 'PREV'");
			sBuf.append("    THEN 'P'");
			sBuf.append("    ELSE 'C'");
			sBuf.append("  END isholiday                ,");
			sBuf.append("  opc.nbr_settle_days          ,");
			sBuf.append("  opc.nbr_delay_days_cr        ,");
			sBuf.append("  opc.nbr_delay_days_dr        ,");
			sBuf.append("  opc.FLG_SAMEVALDAT           ,");
			sBuf.append("  opc.COD_SWEEPCALENDAR        , tmp.dat_lastposting");
			sBuf.append("   FROM orate_posting_cycle opc,");
			sBuf.append("  orbicash_accounts acct       ,");
			sBuf.append("  OLM_GLMST GL                 ,");
			sBuf.append("  (SELECT acct_settle_cr participant_id, ");
			sBuf.append("  dat_lastposting ");
			sBuf.append("  FROM opool_poolhdr hdr "); 
			sBuf.append("  WHERE id_pool IN ");
			sBuf.append("  (SELECT DISTINCT id_pool ");
			sBuf.append("     FROM opool_txnpoolhdr h ");
			sBuf.append("    WHERE nbr_processid = "+processId);
			sBuf.append("  AND id_bvt            = "+bvtId);
			sBuf.append("  ) ");
			sBuf.append("    ");
			sBuf.append("    UNION");
			sBuf.append("   ");
			sBuf.append("   SELECT acct_settle_dr participant_id, ");
			sBuf.append("  dat_lastposting ");
			sBuf.append("  FROM opool_poolhdr hdr "); 
			sBuf.append("  WHERE id_pool IN ");
			sBuf.append("  (SELECT DISTINCT id_pool ");
			sBuf.append("     FROM opool_txnpoolhdr h ");
			sBuf.append("    WHERE nbr_processid = "+processId);
			sBuf.append("  AND id_bvt            = "+bvtId);
			sBuf.append("  ) ");
			sBuf.append("    ");
			sBuf.append("    UNION");
			sBuf.append("   ");
			sBuf.append("   SELECT participant_id participant_id,    a.dat_lastposting");
			sBuf.append("     FROM opool_txnpool_partydtls p,    opool_poolhdr a");
			sBuf.append("    WHERE p.nbr_processid = "+processId);
			sBuf.append("  AND p.id_bvt            = "+bvtId);
			sBuf.append("  AND p.typ_participant   = 'ACCOUNT'");
			sBuf.append("  AND a.id_pool           = p.id_pool");
			sBuf.append("  ) tmp");
			sBuf.append("  WHERE tmp.participant_id = acct.nbr_ownaccount");
			sBuf.append(" AND acct.cod_gl            = gl.cod_gl");
			sBuf.append(" AND GL.cod_bvt_post_cycle = opc.cod_post_cycle");*/
			
			
			
			sBuf.append(" SELECT tmp.cod_calendar , tmp.cod_post_cycle , tmp.participant_id ," +
					" tmp.cod_gl , CASE WHEN opc.typ_post = 'DAILY' THEN 1 WHEN opc.typ_post = 'WEEKLY' THEN 2" +
					" WHEN opc.typ_post = 'MONTHLY' THEN 3 WHEN opc.typ_post = 'YEARLY' THEN 4" +
					" WHEN opc.typ_post = 'CALENDAR' THEN 5 ELSE 0 END typ_post, " +
					" CASE WHEN opc.opt_post IS NULL THEN 0 ELSE opc.opt_post + 0 END opt_post, " +
					" CASE WHEN opc.unit1_post IS NULL THEN                -1 ELSE opc.unit1_post + 0 END unit1_post," +
					" CASE WHEN opc.unit2_post IS NULL THEN                -1 ELSE opc.unit2_post + 0 END unit2_post," +
					" CASE WHEN opc.cod_non_working_day = 'NEXT' THEN 'N' " +
					" WHEN opc.cod_non_working_day = 'PREV' THEN 'P' ELSE 'C' END isholiday ," +
					" opc.nbr_settle_days , opc.nbr_delay_days_cr , opc.nbr_delay_days_dr ," +
					" opc.FLG_SAMEVALDAT , opc.COD_SWEEPCALENDAR , tmp.dat_lastposting FROM orate_posting_cycle opc," +
					" orbicash_accounts acct , (SELECT acct_settle_cr participant_id, dat_lastposting," +
					" cod_post_cycle, cod_gl, (SELECT cod_calendar FROM olm_glmst WHERE cod_gl = hdr.cod_gl )" +
					" cod_calendar FROM opool_poolhdr hdr WHERE id_pool IN (SELECT DISTINCT id_pool FROM opool_txnpoolhdr h" +
					" WHERE nbr_processid =  "+processId+
					" AND id_bvt          = "+bvtId+
					" ) UNION SELECT acct_settle_dr participant_id," +
					" dat_lastposting, cod_post_cycle, cod_gl, (SELECT cod_calendar FROM olm_glmst WHERE cod_gl = hdr.cod_gl )" +
					" cod_calendar FROM opool_poolhdr hdr WHERE id_pool IN (SELECT DISTINCT id_pool FROM opool_txnpoolhdr h" +
					" WHERE nbr_processid = "+processId+
					" AND id_bvt          = "+bvtId+
					" ) UNION SELECT participant_id participant_id," +
					" a.dat_lastposting, a.cod_post_cycle, a.cod_gl, (SELECT cod_calendar FROM olm_glmst " +
					" WHERE cod_gl = a.cod_gl ) cod_calendar FROM opool_txnpool_partydtls p, opool_txnpoolhdr a" +
					" WHERE p.nbr_processid = "+processId+
					"	AND p.id_bvt          = "+bvtId+
					"	AND p.typ_participant = 'ACCOUNT' " +
					" AND a.id_pool         = p.id_pool ) tmp WHERE tmp.participant_id = acct.nbr_ownaccount " +
					" AND acct.cod_gl          = tmp.cod_gl AND tmp.cod_post_cycle   = opc.cod_post_cycle");
			//iRelease_Issue_no_148

			if(logger.isDebugEnabled()){
				logger.debug(" calculateNextBVTCyclePostingDates -> "+sBuf.toString());
			}
			pStmt = conn.prepareStatement(sBuf.toString());
			rSet = pStmt.executeQuery();
			gDate = new GregorianCalendar();
			while(rSet.next()){
				logger.debug(" calculateNextBVTCyclePostingDates In While -> ");//iRelease_Issue_no_148
				String participantId = rSet.getString("participant_id");				
			    String calendarCode = rSet.getString("cod_calendar");
				String frequency = rSet.getString("cod_post_cycle");//iRelease_Issue_no_148
				String hostCode = rSet.getString("cod_gl");
				int typeFrequency = rSet.getInt("typ_post");
				int frequencyInfo = rSet.getInt("opt_post");
				int frequencyUnit1 = rSet.getInt("unit1_post");
				int frequencyUnit2 = rSet.getInt("unit2_post");
				String holidayFlag = rSet.getString("isholiday");
				int nbr_settle_days = rSet.getInt("nbr_settle_days");
				int nbr_delay_days_cr = rSet.getInt("nbr_delay_days_cr");
				int nbr_delay_days_dr = rSet.getInt("nbr_delay_days_dr");
				String Cod_postcalendar = rSet.getString("COD_SWEEPCALENDAR");
				String Flg_samevaldat = rSet.getString("FLG_SAMEVALDAT");
				
				Date lastBvtPostingDate = businessDate;
				Date lastBvtCalcPostingDate = businessDate;
				
				if(logger.isInfoEnabled()){
			        logger.info(" Adding information of Account ["+participantId+"] as [calendarCode] = ["+calendarCode+"]," +
			        " [frequency] = ["+frequency+"], [lastPostingDate] = ["+lastBvtPostingDate+"]" +
			        ", [lastCalcPostingDate] = ["+lastBvtCalcPostingDate+"], [hostCode] = ["+hostCode+"]+" 
			        +"{ Frequency["+typeFrequency+"], FrequencyInfo["+frequencyInfo+"], "
					+"FrequencyUnit1["+frequencyUnit1+"], FrequencyUnit2["+frequencyUnit2+"], "
					+"HolidayFlag["+holidayFlag+"]nbr_settle_days["+nbr_settle_days+"], nbr_delay_days_cr["+nbr_delay_days_cr
					+"],nbr_delay_days_dr["+nbr_delay_days_dr+"],Cod_postcalendar["+Cod_postcalendar
					+"],[PostingStartDate]=["+PostingStartDate+"]");
			    }
				calendarCodes = new ArrayList();
				calendarCodes.add(calendarCode);
				
				if(rSet.getDate("dat_lastposting") != null)
				{
					gDate.setTime(rSet.getDate("dat_lastposting"));
					gDate.add(Calendar.DATE,1);
					PostingStartDate=gDate.getTime();
				}
				else
				{
					PostingStartDate = businessDate;
				}
				
				java.util.Date[] nextActionDates = null;
				nextActionDates = 
					ExecutionUtility.getNextActionDate(calendarCodes, lastBvtPostingDate, lastBvtCalcPostingDate
									 , typeFrequency, frequencyInfo, frequencyUnit1, frequencyUnit2,Cod_postcalendar
									 , holidayFlag, businessDate, callMode);
				
				java.util.Date tmp_date = nextActionDates[0];
				nextActionDates = null;
				nextActionDates = new java.util.Date[8];
				//set date posting
				nextActionDates[0] = tmp_date;
				tmp_date = null;
				//set date adjustment posting
				nextActionDates[1] = nextActionDates[0];
				
				GregorianCalendar temp = new GregorianCalendar();
				temp.setTime(nextActionDates[0]);
				temp.add(Calendar.DATE, nbr_settle_days);
				//set date post settlement
				nextActionDates[2] = temp.getTime();
				temp = null;
				//set date settlement Cr 
				nextActionDates[3] = 
					ExecutionUtility.getAnotherWorkingDay((String)calendarCodes.get(0), nextActionDates[0], 
											holidayFlag.charAt(0), 0, 0, nbr_settle_days + nbr_delay_days_cr);
				//set date settlement Dr
				nextActionDates[4] = 
				    ExecutionUtility.getAnotherWorkingDay((String)calendarCodes.get(0), nextActionDates[0], 
											holidayFlag.charAt(0), 0, 0, nbr_settle_days + nbr_delay_days_dr);
				//set posting start date
				nextActionDates[5] = PostingStartDate;
				
				if(Flg_samevaldat.equalsIgnoreCase("Y"))
				{
					//set  dat value Cr = date settlement Cr 
					nextActionDates[6]=nextActionDates[3];
					//set  dat value Dr = date settlement Dr
					nextActionDates[7]=nextActionDates[4];
				}
				else
				{
								
					gDate = new GregorianCalendar();
					gDate.setTime(nextActionDates[0]);
					gDate.add(Calendar.DATE,1);
					incDateUtil =  gDate.getTime();
					java.sql.Date ValueDate = new java.sql.Date(incDateUtil.getTime());
					//set  dat value Cr = date posting + 1
			
					nextActionDates[6] = ValueDate;
					//set  dat value Dr = date posting + 1
					nextActionDates[7] = ValueDate;
					
				}

				if (logger.isDebugEnabled()) {
                    logger.debug(" KEY:["+participantId+"] & VALUE:["+nextActionDates[0]+", "+nextActionDates[1]
                                 +","+nextActionDates[2]+","+nextActionDates[3]+" ,"+nextActionDates[4]+","+nextActionDates[5]+","+nextActionDates[6]+","+nextActionDates[7]+"] processed");                                 
                }
				postingDatesCache.put(participantId, nextActionDates);				
			}
		}
		catch(Exception e)
		{
			logger.fatal("Exception in calculateNextBVTCyclePostingDates ", e);
			throw new LMSException("LMS01000","Error occurred while calculating next BVT cycle Posting dates for Process Id :- "+processId+".", e);
		}
		finally
		{
			LMSConnectionUtility.close(rSet);
			LMSConnectionUtility.close(pStmt);
		}		
    }
	//ENH_10.3_154 Ends
	
	/**
	 * THis method checks the entry in AccountDtls table .
	 * @param accountInfoDo
	 * @param selectAccountDtls
	 * @return
	 * @throws LMSException
	 */
	private boolean checkRecordInAccountDtls(AccountInfoDO accountInfoDo,PreparedStatement selectAccountDtls) throws LMSException
	{
		
		
		ResultSet res = null;
		boolean found = false;
		if (logger.isDebugEnabled())
		{
			logger.debug("Entering ");
			logger.debug(" DATA OBJECT DETAILS checkRecordInAccountDtls " + accountInfoDo.toString());
		}
		
		try
		{
			selectAccountDtls.setString(1,accountInfoDo.getID_POOL());
			selectAccountDtls.setString(2,accountInfoDo.getPARTICIPANT_ID());
			selectAccountDtls.setString(3,accountInfoDo.getCOD_TXN());
			selectAccountDtls.setDate(4,LMSUtility.truncSqlDate(accountInfoDo.getDAT_CURRENT_BUSINESS()));
			selectAccountDtls.setDate(5,LMSUtility.truncSqlDate(accountInfoDo.getDAT_BUSINESS()));
			selectAccountDtls.setString(6,accountInfoDo.getTYP_PARTICIPANT());	
			selectAccountDtls.setString(7,accountInfoDo.getSOURCE_POOL_ID()); // NEW_ACCOUNTING_ENTRY_FORMAT 
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug(" checkRecordInAccountDtls is " + accountInfoDo.getSOURCE_POOL_ID());
			if(accountInfoDo.getSimId()!=0)
				selectAccountDtls.setLong(8, accountInfoDo.getSimId());
			
			res = selectAccountDtls.executeQuery();
			while(res.next())
			{
				found = true;
				if (logger.isDebugEnabled())
				{
					logger.debug("INside while for participant_id  " + accountInfoDo.getPARTICIPANT_ID());
					logger.debug("IN DB AMT_CALCULATED " + res.getDouble("AMT_CALCULATED"));
					logger.debug("IN DO AMT_CALCULATED " + accountInfoDo.getAMT_CALCULATED());
				}
				//set AMT_CALCULATED = AMT_CALCULATED(from Data Object) + AMT_CALCULATED(from Result set)
				accountInfoDo.setAMT_CALCULATED(res.getBigDecimal("AMT_CALCULATED").add(accountInfoDo.getAMT_CALCULATED()));
				
				
			}
			
			
		}
		catch(SQLException e)
		{
			logger.fatal("Exception :: " , e);
			//ENH_IS1059  Starts
			//throw new LMSException("Error occured in checkRecordInAccountDtls", e.toString());
			throw new LMSException("LMS01000","Record already exist in OPOOL_ACCOUNTDTLS for Transaction Id  :- " +accountInfoDo.getCOD_TXN() +" Pool Id  :- "+accountInfoDo.getID_POOL() +" Participant Id :- " +accountInfoDo.getPARTICIPANT_ID() +" and Type Participant Type is :- "+accountInfoDo.getTYP_PARTICIPANT()+".", e.toString());
			//ENH_IS1059  Ends
		}finally
		{
	        LMSConnectionUtility.close(res);
	       	       
		}
		if (logger.isDebugEnabled()){
			logger.debug("Leaving ");
		}
		return found;
		
	}
	/**
	 * This method deals with AccuralSummary table. It finds cycle start or end and does the update or insert operation accodingly
	 * @param businessDate
	 * @param insertAccrualSummary
	 * @param updateAccrualSummary
	 * @param insertUnpostedAmt
	 * @param accountInfoDo
	 * @param selectAccrualSummary
	 * @throws LMSException
	 */
	private void populateBVTAccuralSummary(	Date businessDate ,PreparedStatement insertAccrualSummary, 
			PreparedStatement updateAccrualSummary,PreparedStatement insertUnpostedAmt,
			//ENH_IS1297 RETROFIT STARTS
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			AccountInfoDO accountInfoDo,PreparedStatement selectAccrualSummary,PreparedStatement updateUnpostedAmt) throws LMSException
			AccountInfoDO accountInfoDo,PreparedStatement selectAccrualSummary,PreparedStatement updateUnpostedAmt,PreparedStatement updateUnpostedAmtCentralCcy, Connection conn) throws LMSException//Reallocate to Self
			/** ****** JTEST FIX end **************  on 14DEC09*/
			//ENH_IS1297 RETROFIT ENDS
	{
		if (logger.isDebugEnabled()){
			logger.debug("Entering ");
		}
		boolean cycleStart = true;
		BigDecimal amtAccrued = ZERO;
		BigDecimal amtUnaccrued = ZERO;
		BigDecimal calculatedAmount = ZERO;
		//Reallocate to Self
		BigDecimal calculatedAmountCentralCcy = ZERO;
		BigDecimal amtAccruedCentralCcy = ZERO;
		BigDecimal amtFromUnPostedCentralCcy = ZERO;
		//Reallocate to Self
		//ENH_IS1297 RETROFIT STARTS
		BigDecimal amtFromUnPosted = ZERO;//SUP_QF07
		String key = "";//SUP_QF07
		String codTxn = "";//SUP_QF07
//SIT2DEV Retro -DD start
				//Retro From 9.8 Starts		
		//SUP_QF10 Starts
		BigDecimal amtAccruedPrevious = ZERO;
		BigDecimal amtToBePosted = ZERO;
		//SUP_QF10 Ends
			//Retro from 9.8 Ends		
//SIT2DEV Retro -DD  end
		//ENH_IS1297 RETROFIT ENDS
		//Identify the Accrual cycle start 
		cycleStart  = checkRecordInAccrualSummary(accountInfoDo,selectAccrualSummary);
	
		calculatedAmount = accountInfoDo.getAMT_CAL_SUM();
		calculatedAmountCentralCcy = accountInfoDo.getAMT_CAL_SUM_CENTRAL_CCY();//Reallocate to Self
		amtUnaccrued = accountInfoDo.getAMT_UNACCRUED();
		if (logger.isDebugEnabled()){
			logger.debug("amtUnaccrued"+amtUnaccrued);
		}
		amtAccrued = accountInfoDo.getAMT_ACCRUED();
		amtAccruedPrevious = amtAccrued;//SUP_QF10 //Retro from 9.8
		//Check for Accrual Cycle End	
		//WHAT TO DO IF PARTY END businessDate.toString().equalsIgnoreCase(accountInfoDo.getPARTY_DAT_END().toString())
//		if(businessDate.toString().equalsIgnoreCase(accountInfoDo.getDAT_ACCRUAL().toString()) )  
//		{
		if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
			logger.debug("businessDate : "+businessDate+" || DAT_ACCRUAL : "+accountInfoDo.getDAT_ACCRUAL());
		if(businessDate.compareTo(accountInfoDo.getDAT_ACCRUAL())>=0 )  
		{
			
			
			if (logger.isDebugEnabled()){
				logger.debug("BVT ACCRUAL CYCLE END ");
			}
			//ENH_IS1297 RETROFIT STARTS
			//SUP_QF07
			
			//get unposted amount from cache 
			//SUP_IS764 starts
			//if(cacheAmtUnposted.containsKey(accountInfoDo.getPARTICIPANT_ID()+codTxn))
			codTxn = accountInfoDo.getCOD_TXN();
			//key = accountInfoDo.getID_POOL()+ExecutionConstants.SEPARATOR+accountInfoDo.getPARTICIPANT_ID()+ExecutionConstants.SEPARATOR+codTxn;
			key = accountInfoDo.getID_POOL()+ExecutionConstants.SEPARATOR+accountInfoDo.getPARTICIPANT_ID()+ExecutionConstants.SEPARATOR+codTxn+ExecutionConstants.SEPARATOR+accountInfoDo.getSOURCE_POOL_ID();
			if (logger.isDebugEnabled()){
				logger.debug("key unposted" +key);
			}
			if(cacheAmtUnposted.containsKey(key))
			//SUP_IS764 ends
			{
				if (logger.isDebugEnabled()){
					logger.debug(" unposted");
				}
				//SUP_IS764 starts
				//amtFromUnPosted = (BigDecimal)cacheAmtUnposted.get(accountInfoDo.getPARTICIPANT_ID()+codTxn);
				amtFromUnPosted = (BigDecimal)cacheAmtUnposted.get(key);
				//SUP_IS764 ends
				//also remove the entry from the HashMap as it has already been used
				//SUP_IS764 starts
				//cacheAmtUnposted.remove(accountInfoDo.getPARTICIPANT_ID()+codTxn);
				cacheAmtUnposted.remove(key);
				//SUP_IS764 ends

			}
			if (logger.isDebugEnabled()){
				logger.debug("amtFromUnPosted " +amtFromUnPosted);
			}
			if( amtFromUnPosted.compareTo(ZERO) != 0)
			{
				
				//update unposted amount as used ie set USE_DATE =Current Business Date
				entries[7]=true;
				updateUnpostedAmtToAcctUnpostedTbl(updateUnpostedAmt,accountInfoDo,ExecutionConstants.BVT_ALL);//Carry Forward Changes
			}
			//Reallocate to Self
			if(cacheAmtUnpostedCentralCcy.containsKey(key))
				//SUP_IS764 ends
				{
					if (logger.isDebugEnabled()){
						logger.debug(" unposted");
					}
					//SUP_IS764 starts
					//amtFromUnPosted = (BigDecimal)cacheAmtUnposted.get(accountInfoDo.getPARTICIPANT_ID()+codTxn);
					amtFromUnPostedCentralCcy = (BigDecimal)cacheAmtUnpostedCentralCcy.get(key);
					//SUP_IS764 ends
					//also remove the entry from the HashMap as it has already been used
					//SUP_IS764 starts
					//cacheAmtUnposted.remove(accountInfoDo.getPARTICIPANT_ID()+codTxn);
					cacheAmtUnpostedCentralCcy.remove(key);
					//SUP_IS764 ends

				}
			if (logger.isDebugEnabled()){
				logger.debug("amtFromUnPostedCentralCcy " +amtFromUnPostedCentralCcy);
			}
			if( amtFromUnPostedCentralCcy.compareTo(ZERO) != 0)
			{
				
				//update unposted amount as used ie set USE_DATE =Current Business Date
				entries[8]=true;
				updateUnpostedAmtToAcctUnpostedTbl(updateUnpostedAmtCentralCcy,accountInfoDo,ExecutionConstants.BVT_ALL);//Carry Forward Changes
			}
			//amtAccrued = calculatedAmount + amtUnaccrued + amtFromUnPosted
			//amtAccrued = calculatedAmount.add(amtUnaccrued).add(amtFromUnPosted);
			// SUP_ISS_V_7808 Starts
			//calculatedAmount = calculatedAmount.add(amtFromUnPosted);
			calculatedAmount = calculatedAmount.add(amtFromUnPosted).add(amtUnaccrued);
			// SUP_ISS_V_7808 Ends
			if (logger.isDebugEnabled()){
				logger.debug("calculatedAmount after adding unposted" +calculatedAmount);
			}
			calculatedAmountCentralCcy = calculatedAmountCentralCcy.add(amtFromUnPostedCentralCcy);
			if (logger.isDebugEnabled()){
				logger.debug("calculatedAmountCentralCcy after adding unposted" +calculatedAmountCentralCcy);
			}
			//Reallocate to Self
			//END
			//ENH_IS1297 RETROFIT ENDS
			
			//Apply Caryforward logic to amtAccrued
			
			ExecutionUtility util=new ExecutionUtility();
			BigDecimal[] precision_carryfwd ={ZERO,ZERO};
//SIT2DEV Retro -DD start
			//Retro From 9.8 Start			
			//SUP_QF10 starts
			/*precision_carryfwd = util.getTruncatedAmt(calculatedAmount,ExecutionConstants.ACCRUAL,accountInfoDo.getCOD_CCY());
			//set Truncated amount to amtAccured
			calculatedAmount = precision_carryfwd[0];
			if (logger.isDebugEnabled())
			{
				logger.debug("Truncated amtAccrued" + calculatedAmount);
				logger.debug("Carry Forward Amount " + precision_carryfwd[1]);
			}
			//Add Carry Forward Amount to OPOOL_ACCOUNT_UNPOSTED table
			if( precision_carryfwd[1].compareTo(ZERO) != 0)
			{
				entries[6]=true;
				addAmtToAcctUnpostedTbl(insertUnpostedAmt,precision_carryfwd[1],"BVTAMOUNT_CARRYFORWARD",accountInfoDo,"BVT");
			}*/
			//SUP_QF10 Ends
			
			accountInfoDo.setAMT_UNACCRUED(ZERO);
			//Set Amount Accured = AmtAccrued+calculated Amount
			amtAccrued = amtAccrued.add(calculatedAmount);
			amtAccruedCentralCcy = amtAccruedCentralCcy.add(calculatedAmountCentralCcy);//Reallocate to Self
			//SUP_QF10 starts
			/** ****** JTEST FIX start ************  on 14DEC09*/
			//precision_carryfwd = util.getTruncatedAmt(amtAccrued,ExecutionConstants.ACCRUAL,accountInfoDo.getCOD_CCY());

			precision_carryfwd = util.getTruncatedAmt(amtAccrued,ExecutionConstants.ACCRUAL,accountInfoDo.getCOD_CCY(), conn);
			/** ****** JTEST FIX end **************  on 14DEC09*/
			//set Truncated amount to amtAccured
			amtAccrued = precision_carryfwd[0];
			if (logger.isDebugEnabled())
			{
				logger.debug("Truncated amtAccrued" + amtAccrued);
				logger.debug("Carry Forward Amount " + precision_carryfwd[1]);
			}
			//Add Carry Forward Amount to OPOOL_ACCOUNT_UNPOSTED table
			if( precision_carryfwd[1].compareTo(ZERO) != 0)
			{
				entries[6]=true;
				addAmtToAcctUnpostedTbl(insertUnpostedAmt,precision_carryfwd[1],"BVTAMOUNT_CARRYFORWARD",accountInfoDo,"BVT");
			}
			//SUP_QF10 Ends
			//Reallocate to Self
			precision_carryfwd = util.getTruncatedAmt(amtAccruedCentralCcy,ExecutionConstants.ACCRUAL,accountInfoDo.getCOD_CCY(), conn);
			
			amtAccruedCentralCcy = precision_carryfwd[0];
			if (logger.isDebugEnabled())
			{
				logger.debug("Truncated amtAccruedCentralCcy" + amtAccruedCentralCcy);
				logger.debug("Carry Forward Amount " + precision_carryfwd[1]);
			}
			//Add Carry Forward Amount to OPOOL_ACCOUNT_UNPOSTED table
			if( precision_carryfwd[1].compareTo(ZERO) != 0)
			{
				entries[6]=true;
				addAmtToAcctUnpostedTbl(insertUnpostedAmt,precision_carryfwd[1],"BVTAMOUNT_CARRYFORWARD_CENTRAL_CCY",accountInfoDo,"BVT");
			}
			//Reallocate to Self
			accountInfoDo.setAMT_ACCRUED(amtAccrued);
			//Set Amt to be posted
			
			//SUP_QF10 Starts
			//accountInfoDo.setAMT_TO_BE_POSTED(calculatedAmount);
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("amtAccrued = "+amtAccrued+" amtAccruedPrevious"+amtAccruedPrevious);
			amtToBePosted=amtAccrued.subtract(amtAccruedPrevious);
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("amtAccrued = "+amtAccrued+" amtAccruedPrevious"+amtAccruedPrevious+" amtToBePosted = "+amtToBePosted);
			accountInfoDo.setAMT_TO_BE_POSTED(amtToBePosted);
			accountInfoDo.setAMT_CAL_CENTRAL_CCY(amtAccruedCentralCcy);//Reallocate to Self
			//SUP_QF10 Ends
			//Retro 9.8 Ends			
//SIT2DEV Retro -DD end
			accountInfoDo.setTXT_STATUS(ExecutionConstants.ACCRUAL_GENERATED_STATUS);
			
		}
		else
		{	if (logger.isDebugEnabled()){
				logger.debug("ACCRUAL CYCLE NOT  END ");
			}
			//	Set Amount Accured 
			accountInfoDo.setAMT_ACCRUED(ZERO);
			//Set AmtUnccrued = amtUnaccrued + calcuted amt
			accountInfoDo.setAMT_UNACCRUED(amtUnaccrued.add(calculatedAmount));
			accountInfoDo.setTXT_STATUS(ExecutionConstants.NOACCRUAL_STATUS);
		}
		
		if(cycleStart == false)
		{
			if (logger.isDebugEnabled()){
				logger.debug(" DATA OBJECT DETAILS For BVT : " + accountInfoDo.toString());
			}
			insertAccrualSummary(insertAccrualSummary,accountInfoDo);
			entries[2]=true;
		}
		else
		{
			if (logger.isDebugEnabled()){
				logger.debug(" DATA OBJECT DETAILS " + accountInfoDo.toString());
			}
			updateAccrualSummary(updateAccrualSummary,accountInfoDo);
			entries[3]=true;
		}
		if (logger.isDebugEnabled()){
			logger.debug("Leaving");
		}
	}
	
	
	
	//ENH_10.3_154 Starts
	/**
	 * This method deals with AccuralSummary table. It finds cycle start or end and does the update or insert operation accodingly
	 * @param businessDate
	 * @param insertAccrualSummary
	 * @param updateAccrualSummary
	 * @param insertUnpostedAmt
	 * @param accountInfoDo
	 * @param selectAccrualSummary
	 * @throws LMSException
	 */
	//  JPMC_FIX  - Changed Method Name from 'populateCycleWiseBVTAccuralSummary' to 'populateCycleWiseBVTAccuralSummaryForPool'
	//private void populateCycleWiseBVTAccuralSummary(	Date businessDate ,PreparedStatement insertAccrualSummary, 
	private void populateCycleWiseBVTAccuralSummaryForPool(	Date businessDate ,PreparedStatement insertAccrualSummary, 
			PreparedStatement updateAccrualSummary,PreparedStatement insertUnpostedAmt,
			AccountInfoDO accountInfoDo,PreparedStatement selectAccrualSummary,PreparedStatement updateUnpostedAmt, Connection conn) throws LMSException
	{
		if (logger.isDebugEnabled()){
			//  logger.debug("Entering populateCycleWiseBVTAccuralSummary"); JPMC_FIX
			logger.debug("Entering populateCycleWiseBVTAccuralSummaryForPool");
		}
		boolean cycleStart = true;
		BigDecimal amtAccrued = ZERO;
		BigDecimal amtUnaccrued = ZERO;
		BigDecimal calculatedAmount = ZERO;
		//Reallocate to Self
		BigDecimal calculatedAmountCentralCcy = ZERO;
		BigDecimal amtAccruedCentralCcy = ZERO;
		BigDecimal amtFromUnPostedCentralCcy = ZERO;
		//Reallocate to Self	
		BigDecimal amtFromUnPosted = ZERO;
		String key = "";
		String codTxn = "";
		BigDecimal amtAccruedPrevious = ZERO;
		BigDecimal amtToBePosted = ZERO;

		//Identify the Accrual cycle start 
		cycleStart  = checkRecordInAccrualSummary_BVT(accountInfoDo,selectAccrualSummary);
	
		calculatedAmount = accountInfoDo.getAMT_CAL_SUM();
		calculatedAmountCentralCcy = accountInfoDo.getAMT_CAL_SUM_CENTRAL_CCY();//Reallocate to Self
		amtUnaccrued = accountInfoDo.getAMT_UNACCRUED();
		if (logger.isDebugEnabled()){
			logger.debug("amtUnaccrued"+amtUnaccrued);
		}
		amtAccrued = accountInfoDo.getAMT_ACCRUED();
		amtAccruedPrevious = amtAccrued;
		if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
			logger.debug("businessDate : "+businessDate+" || DAT_ACCRUAL : "+accountInfoDo.getDAT_ACCRUAL());
		if(businessDate.compareTo(accountInfoDo.getDAT_ACCRUAL())>=0 )  
		{
			
			
			if (logger.isDebugEnabled()){
				logger.debug("BVT ACCRUAL CYCLE END ");
			}
			
			//get unposted amount from cache 
			codTxn = accountInfoDo.getCOD_TXN();
			//key = accountInfoDo.getID_POOL()+ExecutionConstants.SEPARATOR+accountInfoDo.getPARTICIPANT_ID()+ExecutionConstants.SEPARATOR+codTxn;
			key = accountInfoDo.getID_POOL()+ExecutionConstants.SEPARATOR+accountInfoDo.getPARTICIPANT_ID()+ExecutionConstants.SEPARATOR+codTxn+ExecutionConstants.SEPARATOR+accountInfoDo.getSOURCE_POOL_ID();
			if (logger.isDebugEnabled()){
				logger.debug("key unposted" +key);
			}
			if(cacheAmtUnpostedtemp.containsKey(key))
			{
				if (logger.isDebugEnabled()){
					logger.debug(" unposted");
				}
				amtFromUnPosted = (BigDecimal)cacheAmtUnpostedtemp.get(key);
				cacheAmtUnpostedtemp.remove(key);
			}

			if (logger.isDebugEnabled()){
				logger.debug("amtFromUnPosted " +amtFromUnPosted);
			}
			// SUP_ISS_V_7808 Starts
			//calculatedAmount = calculatedAmount.add(amtFromUnPosted);
			calculatedAmount = calculatedAmount.add(amtFromUnPosted).add(amtUnaccrued);
			// SUP_ISS_V_7808 Ends
			if (logger.isDebugEnabled()){
				logger.debug("calculatedAmount after adding unposted" +calculatedAmount);
			}
			
			//Reallocate to Self
			
			if(cacheAmtUnpostedCentralCcytemp.containsKey(key))
			{
				if (logger.isDebugEnabled()){
					logger.debug(" unposted");
				}
				amtFromUnPostedCentralCcy = (BigDecimal)cacheAmtUnpostedCentralCcytemp.get(key);
				cacheAmtUnpostedCentralCcytemp.remove(key);
			}

			if (logger.isDebugEnabled()){
				logger.debug("amtFromUnPostedCentralCcy " +amtFromUnPostedCentralCcy);
			}

			calculatedAmountCentralCcy = calculatedAmountCentralCcy.add(amtFromUnPostedCentralCcy);
			if (logger.isDebugEnabled()){
				logger.debug("calculatedAmountCentralCcy after adding unposted" +calculatedAmountCentralCcy);
			}
			//Reallocate to Self
			//Apply Caryforward logic to amtAccrued
			ExecutionUtility util=new ExecutionUtility();
			BigDecimal[] precision_carryfwd ={ZERO,ZERO};

			accountInfoDo.setAMT_UNACCRUED(ZERO);
			//Set Amount Accured = AmtAccrued+calculated Amount
			amtAccrued = amtAccrued.add(calculatedAmount);
			
			//SUP_QF10 starts
			/** ****** JTEST FIX start ************  on 14DEC09*/
			//precision_carryfwd = util.getTruncatedAmt(amtAccrued,ExecutionConstants.ACCRUAL,accountInfoDo.getCOD_CCY());

			precision_carryfwd = util.getTruncatedAmt(amtAccrued,ExecutionConstants.ACCRUAL,accountInfoDo.getCOD_CCY(), conn);
			/** ****** JTEST FIX end **************  on 14DEC09*/
			//set Truncated amount to amtAccured
			amtAccrued = precision_carryfwd[0];
			if (logger.isDebugEnabled())
			{
				logger.debug("Truncated amtAccrued" + amtAccrued);
				logger.debug("Carry Forward Amount " + precision_carryfwd[1]);
			}
			
			//Reallocate to Self
			amtAccruedCentralCcy = amtAccruedCentralCcy.add(calculatedAmountCentralCcy);
			precision_carryfwd = util.getTruncatedAmt(amtAccruedCentralCcy,ExecutionConstants.ACCRUAL,accountInfoDo.getCOD_CCY(), conn);
			
			amtAccruedCentralCcy = precision_carryfwd[0];
			if (logger.isDebugEnabled())
			{
				logger.debug("Truncated amtAccruedCentralCcy" + amtAccruedCentralCcy);
				logger.debug("Carry Forward Amount " + precision_carryfwd[1]);
			}
			//Reallocate to Self
			accountInfoDo.setAMT_ACCRUED(amtAccrued);
			//Set Amt to be posted
			
			//SUP_QF10 Starts
			//accountInfoDo.setAMT_TO_BE_POSTED(calculatedAmount);
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("amtAccrued = "+amtAccrued+" amtAccruedPrevious"+amtAccruedPrevious);
			amtToBePosted=amtAccrued.subtract(amtAccruedPrevious);
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("amtAccrued = "+amtAccrued+" amtAccruedPrevious"+amtAccruedPrevious+" amtToBePosted = "+amtToBePosted);
			accountInfoDo.setAMT_TO_BE_POSTED(amtToBePosted);
			//SUP_QF10 Ends
			//Retro 9.8 Ends			
//SIT2DEV Retro -DD end
			accountInfoDo.setAMT_CAL_CENTRAL_CCY(amtAccruedCentralCcy);//Reallocate to Self
			accountInfoDo.setTXT_STATUS(ExecutionConstants.ACCRUAL_GENERATED_STATUS);
			
		}
		else
		{	if (logger.isDebugEnabled()){
				logger.debug("ACCRUAL CYCLE NOT  END ");
			}
			//	Set Amount Accured 
			accountInfoDo.setAMT_ACCRUED(ZERO);
			//Set AmtUnccrued = amtUnaccrued + calcuted amt
			accountInfoDo.setAMT_UNACCRUED(amtUnaccrued.add(calculatedAmount));
			//accountInfoDo.setAMT_CAL_CENTRAL_CCY(amtUnaccrued.add(calculatedAmountCentralCcy));//Reallocate to Self
			accountInfoDo.setTXT_STATUS(ExecutionConstants.NOACCRUAL_STATUS);
		}
		
		if(!cycleStart)
		{
			if (logger.isDebugEnabled()){
				//logger.debug(" insertAccrualSummary_BVT DATA OBJECT DETAILS " + accountInfoDo.toString());
				logger.debug(" insertAccrualSummary_BVTForPool DATA OBJECT DETAILS " + accountInfoDo.toString());
			}
			//insertAccrualSummary_BVT(insertAccrualSummary,accountInfoDo);
			insertAccrualSummary_BVTForPool(insertAccrualSummary,accountInfoDo);
			entries[2]=true;
		}
		else
		{
			if (logger.isDebugEnabled()){
				//logger.debug(" updateAccrualSummary_BVT DATA OBJECT DETAILS " + accountInfoDo.toString());
				logger.debug(" updateAccrualSummary_BVTForPool DATA OBJECT DETAILS " + accountInfoDo.toString());
			}
			//updateAccrualSummary_BVT(updateAccrualSummary,accountInfoDo);
			updateAccrualSummary_BVTForPool(updateAccrualSummary,accountInfoDo);
			entries[3]=true;
		}
		if (logger.isDebugEnabled()){
			logger.debug("Leaving");
		}
	}
	
	/**
	 * This method checks whether an entry is there in  AccrualSummary_BVT table for particular txn code and sets the some data into Data Object
	 * @param accountInfoDo
	 * @param selectAccrualSummary
	 * @return
	 * @throws LMSException
	 */
	private boolean checkRecordInAccrualSummary_BVT(AccountInfoDO accountInfoDo,PreparedStatement selectAccrualSummary) throws LMSException
	{
		
		
		ResultSet res = null;
		boolean found = false;
		boolean flg_newAccountingentry = ExecutionConstants.FlG_NEW_ACCOUNTING_ENTRY_FORMAT; // NEW_ACCOUNTING_ENTRY_FORMAT
		boolean allowIntTyp = ExecutionConstants.FLG_INTACC_POST_INTTYP ; //Changes done for Unique key exception
		if (logger.isDebugEnabled())
		{
			logger.debug("Entering checkRecordInAccrualSummary_BVT");
			logger.debug(" DATA OBJECT DETAILS checkRecordInAccrualSummary_BVT " + accountInfoDo.toString());
			logger.debug(" flg_newAccountingentry " + flg_newAccountingentry);
			logger.debug(" allowIntTyp " + allowIntTyp);
		}
		
		try
		{
			
			
			String codTxn =null;
			if(allowIntTyp)     //Sandeep changes for ned issue
			{
					if("I".equalsIgnoreCase(accountInfoDo.getTYP_INTEREST()))
						codTxn  = accountInfoDo.getCOD_TXN();
					else
						codTxn = accountInfoDo.getCOD_TXN()+accountInfoDo.getTYP_INTEREST();
			}
			else{
						codTxn = accountInfoDo.getCOD_TXN();
			}
			
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug(" codTxn: " + codTxn);
			selectAccrualSummary.setString(1,accountInfoDo.getPARTICIPANT_ID());
			selectAccrualSummary.setString(2,codTxn);
			selectAccrualSummary.setDate(3,LMSUtility.truncSqlDate(accountInfoDo.getDAT_ACCRUAL_START()));
			selectAccrualSummary.setString(4,accountInfoDo.getID_POOL());
			selectAccrualSummary.setString(5,accountInfoDo.getTYP_PARTICIPANT());
			selectAccrualSummary.setDate(6,LMSUtility.truncSqlDate(accountInfoDo.getDAT_INITIAL_POSTING()));
			//SUP_ISS_III_7774 starts
			if(accountInfoDo.getID_POOL().equalsIgnoreCase("NONPOOL_ACCT")){
				selectAccrualSummary.setString(7,accountInfoDo.getSOURCE_POOL_ID()); //NEW_ACCOUNTING_ENTRY_FORMAT
			}else{
				if(flg_newAccountingentry) 
					selectAccrualSummary.setString(7,accountInfoDo.getSOURCE_POOL_ID()); //NEW_ACCOUNTING_ENTRY_FORMAT
				else
					selectAccrualSummary.setString(7,accountInfoDo.getID_POOL()); //NEW_ACCOUNTING_ENTRY_FORMAT	
			}
			//SUP_ISS_III_7774 ends
			res = selectAccrualSummary.executeQuery();
			while(res.next())
			{
				found = true;
				
				accountInfoDo.setAMT_UNACCRUED(res.getBigDecimal("AMT_UNACCRUED"));
				accountInfoDo.setOLD_DAT_ACCRUAL(res.getDate("DAT_ACCRUAL"));
				accountInfoDo.setOLD_DAT_ADJUSTEDACCRUAL(res.getDate("DAT_ACCRUAL_ADJUSTED"));
				accountInfoDo.setAMT_ACCRUED(res.getBigDecimal("AMT_ACCRUED"));
				if (logger.isDebugEnabled())
				{
					logger.debug("IN DB amtUnaccured " + res.getDouble("AMT_UNACCRUED"));
					logger.debug("IN DB amtUnaccured " + res.getDouble("AMT_ACCRUED"));
				}
			}
			
			
		}
		catch(SQLException e)
		{
			logger.fatal("Exception in checkRecordInAccrualSummary_BVT :: " , e);
			throw new LMSException("LMS01000","Record already exist in OPOOL_ACCRUALSUMMARY_BVT table for Transaction Id "+accountInfoDo.getCOD_TXN()+", Pool Id :-"+accountInfoDo.getID_POOL()+", Participant :- "+accountInfoDo.getPARTICIPANT_ID()+" and Participant Type :"+accountInfoDo.getTYP_PARTICIPANT()+".", e);
		}finally
		{
	        LMSConnectionUtility.close(res);
		}
		if (logger.isDebugEnabled()){
			logger.debug("Leaving checkRecordInAccrualSummary_BVT");
		}
		return found;
		
	}
	
	/**
	 * This method set the parameters into insertAccrualSummary statement 
	 * @param insertAccrualSummary
	 * @param accountInfoDo
	 * @throws LMSException
	 */
	// Changed Method Name
	//private void insertAccrualSummary_BVT(PreparedStatement insertAccrualSummary, AccountInfoDO accountInfoDo) throws LMSException
	private void insertAccrualSummary_BVTForPool(PreparedStatement insertAccrualSummary, AccountInfoDO accountInfoDo) throws LMSException
	{
		if (logger.isDebugEnabled()){
			//logger.debug("Entering insertAccrualSummary_BVT "+ accountInfoDo.getCOD_TXN()+","+accountInfoDo.getTYP_INTEREST());
			logger.debug("Entering insertAccrualSummary_BVTForPool "+ accountInfoDo.getCOD_TXN()+","+accountInfoDo.getTYP_INTEREST());
			
		}
		boolean allowIntTyp = ExecutionConstants.FLG_INTACC_POST_INTTYP ;    //Sandeep changes for ned issue
		String codTxn = null;
		try
		{
			insertAccrualSummary.setString(1,accountInfoDo.getID_POOL());
			insertAccrualSummary.setString(2,accountInfoDo.getPARTICIPANT_ID());
			if(allowIntTyp)     //Sandeep changes for ned issue
			{
				if("I".equalsIgnoreCase(accountInfoDo.getTYP_INTEREST()))
					codTxn = accountInfoDo.getCOD_TXN();
				else
					codTxn = accountInfoDo.getCOD_TXN()+accountInfoDo.getTYP_INTEREST();
			}
			else{
				codTxn = accountInfoDo.getCOD_TXN();
			}
			insertAccrualSummary.setString(3,codTxn);
			insertAccrualSummary.setDate(4,LMSUtility.truncSqlDate(accountInfoDo.getDAT_ACCRUAL_START()));
			insertAccrualSummary.setDate(5,LMSUtility.truncSqlDate(accountInfoDo.getDAT_ACCRUAL()));
			insertAccrualSummary.setDate(6,LMSUtility.truncSqlDate(accountInfoDo.getDAT_ADJUSTEDACCRUAL()));
			insertAccrualSummary.setBigDecimal(7,accountInfoDo.getAMT_UNACCRUED());
			insertAccrualSummary.setBigDecimal(8,accountInfoDo.getAMT_ACCRUED());
			insertAccrualSummary.setDate(9,LMSUtility.truncSqlDate(accountInfoDo.getDAT_FINAL_SETTLEMENT()));
			insertAccrualSummary.setString(10,accountInfoDo.getTXT_STATUS());
			insertAccrualSummary.setDate(11,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING()));
			insertAccrualSummary.setString(12,accountInfoDo.getTYP_PARTICIPANT());	
			// FindBug issue fix starts
			//insertAccrualSummary.setString(13,com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT.format(new java.util.Date()));
			insertAccrualSummary.setString(13,REF_DATE_FORMAT.format(new java.util.Date()));//Harikesh
			// FindBug issue fix ends
			insertAccrualSummary.setDate(14,LMSUtility.truncSqlDate(accountInfoDo.getDAT_CURRENT_BUSINESS()));
			insertAccrualSummary.setDate(15,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING_START()));	
			insertAccrualSummary.setDate(16,LMSUtility.truncSqlDate(accountInfoDo.getDAT_INITIAL_POSTING()));
			insertAccrualSummary.setString(17,accountInfoDo.getSOURCE_POOL_ID()); //NEW_ACCOUNTING_ENTRY_FORMAT
			insertAccrualSummary.addBatch();
		}
		catch(SQLException e )
		{

			//logger.fatal("Exception in insertAccrualSummary_BVT :: " , e);
			logger.fatal("Exception in insertAccrualSummary_BVTForPool :: " , e);
			//ENH_IS1059 Starts  
			//throw new LMSException("Error occured in insertAccrualSummary", e.toString());
			throw new LMSException("LMS01000","Insertion of  accural summary in OPOOL_ACCRUALSUMMARY_BVT table failed for Transaction Id  :- "+accountInfoDo.getCOD_TXN()+", Pool Id  :- "+accountInfoDo.getID_POOL()+",  Participant Id :- "+ accountInfoDo.getPARTICIPANT_ID()+" and Type Participant Type is "+accountInfoDo.getTYP_PARTICIPANT()+".", e);
			//ENH_IS1059  Ends
		}
		if (logger.isDebugEnabled()){
			//logger.debug("Leaving insertAccrualSummary_BVT");
			logger.debug("Leaving insertAccrualSummary_BVTForPool");
		}
	}
	
	/**This method set the parameter into updateAccountSummary statement
	 * @param updateAccountSummary
	 * @param accountInfoDo
	 * @throws LMSException
	 */
	//Changed Method name
	//private void updateAccrualSummary_BVT(PreparedStatement updateAccountSummary, AccountInfoDO accountInfoDo) throws LMSException
	private void updateAccrualSummary_BVTForPool(PreparedStatement updateAccountSummary, AccountInfoDO accountInfoDo) throws LMSException 
	{
		if (logger.isDebugEnabled())
		{
			//logger.debug("Entering updateAccrualSummary_BVT"+ accountInfoDo.getCOD_TXN()+","+accountInfoDo.getTYP_INTEREST());
			logger.debug("Entering updateAccrualSummary_BVTForPool"+ accountInfoDo.getCOD_TXN()+","+accountInfoDo.getTYP_INTEREST());
		}
		boolean allowIntTyp = ExecutionConstants.FLG_INTACC_POST_INTTYP ;    //Sandeep changes for ned issue
		String codTxn = null;
		try
		{
			updateAccountSummary.setBigDecimal(1,accountInfoDo.getAMT_UNACCRUED());
			updateAccountSummary.setBigDecimal(2,accountInfoDo.getAMT_ACCRUED());
			updateAccountSummary.setString(3,accountInfoDo.getTXT_STATUS());
			updateAccountSummary.setDate(4,LMSUtility.truncSqlDate(accountInfoDo.getDAT_ADJUSTEDACCRUAL()));
			updateAccountSummary.setDate(5,LMSUtility.truncSqlDate(accountInfoDo.getDAT_FINAL_SETTLEMENT()));
			updateAccountSummary.setDate(6,LMSUtility.truncSqlDate(accountInfoDo.getDAT_ACCRUAL()));
			// FindBug issue fix starts
			//updateAccountSummary.setString(7,com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT.format(new java.util.Date()));
			updateAccountSummary.setString(7,REF_DATE_FORMAT.format(new java.util.Date()));//Harikesh
			// FindBug issue fix ends
			updateAccountSummary.setDate(8,LMSUtility.truncSqlDate(accountInfoDo.getDAT_CURRENT_BUSINESS()));
			updateAccountSummary.setString(9,accountInfoDo.getPARTICIPANT_ID());
			if(allowIntTyp)          //Sandeep changes for ned issue
			{
				if("I".equalsIgnoreCase(accountInfoDo.getTYP_INTEREST()))
					codTxn = accountInfoDo.getCOD_TXN();
				else
					codTxn = accountInfoDo.getCOD_TXN()+accountInfoDo.getTYP_INTEREST();
			}
			else{
				codTxn = accountInfoDo.getCOD_TXN();
			}
			updateAccountSummary.setString(10,codTxn);
			updateAccountSummary.setDate(11,LMSUtility.truncSqlDate(accountInfoDo.getDAT_ACCRUAL_START()));
			updateAccountSummary.setString(12,accountInfoDo.getID_POOL());
			updateAccountSummary.setString(13,accountInfoDo.getTYP_PARTICIPANT());
			updateAccountSummary.setDate(14,LMSUtility.truncSqlDate(accountInfoDo.getDAT_INITIAL_POSTING()));
			updateAccountSummary.setString(15,accountInfoDo.getSOURCE_POOL_ID());
			updateAccountSummary.addBatch();
		}
		catch(SQLException e)
		{
			//logger.fatal("Exception in updateAccrualSummary_BVT :: " , e);
			logger.fatal("Exception in updateAccrualSummary_BVTForPool :: " , e);
			//ENH_IS1059  Start
			//throw new LMSException("Error occured in updateAccrualSummary", e.toString());
			throw new LMSException("LMS01000","Updation of  accural summary in OPOOL_ACCRUALSUMMARY_BVT table failed for Transaction Id  :-"+accountInfoDo.getCOD_TXN()+" Pool Id  :- "+accountInfoDo.getID_POOL()+ " Participant Id :- "+accountInfoDo.getPARTICIPANT_ID()+" and Type Participant Type is "+accountInfoDo.getTYP_PARTICIPANT()+".", e);
			//ENH_IS1059  Ends
		
		}
		if (logger.isDebugEnabled()){
			//logger.debug("Leaving updateAccrualSummary_BVT");
			logger.debug("Leaving updateAccrualSummary_BVTForPool");
		}
		
	}
	//ENH_10.3_154 Ends
	
	/**
	 * This method deals with PostingSummary table. It finds cycle start or end and does the update or insert operation accodingly
	 * @param businessDate
	 * @param insertPostingSummary
	 * @param updatePostingSummary
	 * @param accountInfoDo
	 * @param selectPostingSummary
	 * @throws LMSException
	 */
	private void populateBVTPostingSummary(	Date businessDate ,PreparedStatement insertPostingSummary, 
			PreparedStatement updatePostingSummary,
			AccountInfoDO accountInfoDo,PreparedStatement selectPostingSummary) throws LMSException 
	{
		if (logger.isDebugEnabled()){
			logger.debug("Entering populateBVTPostingSummary");
		}
		// Issue 15954 fix added flag
		//AE_ON_FINALSETTELMENT_DATE  JPMC starts
		String flg_AE_ON_FinalSettelment_Date = ExecutionConstants.AE_ON_FINALSETTELMENT_DATE;
		if (logger.isDebugEnabled()) {
		 logger.debug("AE_ON_FINALSETTELMENT_DATE ::::: " + flg_AE_ON_FinalSettelment_Date );
		}
		// Issue 15954 fix ends
		
		BigDecimal amtAccuredNotPosted =ZERO;
		BigDecimal amountToBePosted = ZERO;
		BigDecimal postedAmount=ZERO;
		BigDecimal amountToBePostedCentralCcy = ZERO, postedAmountCentralCcy=ZERO;//Reallocate to Self
		boolean cycleStart = true;
		boolean flgPostingToBeDone = false;
		//Identify the Posting cycle start 
		cycleStart  = checkRecordInPostingSummary(accountInfoDo,selectPostingSummary);
		amtAccuredNotPosted = accountInfoDo.getAMT_ACCRUEDNOTPOSTED(); 
		postedAmount =  accountInfoDo.getAMT_POSTED();
		
		amountToBePosted=accountInfoDo.getAMT_TO_BE_POSTED();
		//Reallocate to Self
		amountToBePostedCentralCcy = accountInfoDo.getAMT_CAL_CENTRAL_CCY();
		postedAmountCentralCcy = accountInfoDo.getAMT_POSTED_CENTRAL_CCY();
		//Reallocate to Self
		//Identify Accrual Cycle End || businessDate.toString().equalsIgnoreCase(accountInfoDo.getPARTY_DAT_END().toString())
		if (logger.isDebugEnabled()){
			logger.debug("businessDate " + businessDate.toString());
			logger.debug("DAT_POSTING " + accountInfoDo.getDAT_POSTING().toString());
		}	
		
		//Posting Check for Inter Company : On Loan End Date i.e. on Maturity 
		//interest engine should skip marking posting entry status to 'POSTING_GEN' 
		//as the same would be done during Maturity processing for Inter Company Interest
		//Instead the calculated interest should be added to accrued not posted with 'NO_POSTING' status
		//Commented as this flag was getting used in condition of line no 7002, which was already commented
		//flgPostingToBeDone = checkPostingToBeDone(new java.sql.Date(businessDate.getTime()), accountInfoDo);	

		//condition added as If status is "Posting_gen" in opool_postingsummary_bvt, then it should be same in opool_postingsummary also 
		if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase(ExecutionConstants.YES))
		{
			if (logger.isDebugEnabled()){
				logger.debug("Inside FLAG_BVT_CYCLE Yes");
			}
					//Ned_Retro
			//SA_BVT_POSTING_CYCLE STARTS
			//SUP_ISS_II_7834 starts
			/*if(accountInfoDo.getID_POOL().equalsIgnoreCase("NONPOOL_ACCT")){
				
				if (accountInfoDo.getCOD_TXN().length() > 6) { // SUP_ISS_V_7481 
					if ((accountInfoDo.getCOD_TXN().substring(7, 8).equalsIgnoreCase("B"))
							|| ((accountInfoDo.getCOD_TXN().substring(7, 8).equalsIgnoreCase("N"))
									&& businessDate.compareTo(accountInfoDo.getDAT_POSTING()) > 0)) {
						flgPostingToBeDone = true;
					} else
						flgPostingToBeDone = false;
					//SUP_ISS_V_7481 starts
				} else {
					flgPostingToBeDone = false;
				} ////SUP_ISS_V_7481 End
				// SUP_ISS_III_7305 start
			}else */
			//SUP_ISS_II_7834 ends
			if (ExecutionConstants.INCOLOAN.equals(accountInfoDo.getTYP_PARTICIPANT())) {
				if (accountInfoDo.getCOD_TXN().length() > 6) { // SUP_ISS_V_7481
					if ((accountInfoDo.getCOD_TXN().substring(5, 6).equalsIgnoreCase("B"))
							|| ((accountInfoDo.getCOD_TXN().substring(5, 6).equalsIgnoreCase("N"))
									&& businessDate.compareTo(accountInfoDo.getDAT_POSTING()) > 0)) {
						flgPostingToBeDone = true;
					} else
						flgPostingToBeDone = false;
					// SUP_ISS_V_7481 starts
				} else {
					flgPostingToBeDone = false;
				} //// SUP_ISS_V_7481 End
				// SUP_ISS_III_7305 end
			} else {
				if (accountInfoDo.getCOD_TXN().length() > 6) { // SUP_ISS_V_7481
					if ((accountInfoDo.getCOD_TXN().substring(6, 7).equalsIgnoreCase("B"))
							|| ((accountInfoDo.getCOD_TXN().substring(6, 7).equalsIgnoreCase("N"))
									&& businessDate.compareTo(accountInfoDo.getDAT_POSTING()) > 0)) {
						flgPostingToBeDone = true;
					} else
						flgPostingToBeDone = false;
					// SUP_ISS_V_7481 starts
				} else {
					flgPostingToBeDone = false;
				} //// SUP_ISS_V_7481 End
			}
			
			//SA_BVT_POSTING_CYCLE ENDS
			if (logger.isDebugEnabled()){
				logger.debug("flgPostingToBeDone inside FLAG_BVT_CYCLE Yes : "+flgPostingToBeDone);
			}
			
		}
		else
		{
			if (logger.isDebugEnabled()){
				logger.debug("Inside FLAG_BVT_CYCLE No");
			}
			
			// Issue 15954 fix changed from dat_posting to dat_final_settelment starts
			
			if(flg_AE_ON_FinalSettelment_Date.equalsIgnoreCase("Y")){
				if(businessDate.compareTo(accountInfoDo.getDAT_FINAL_SETTLEMENT()) > 0)
				{
					flgPostingToBeDone = true;
				}
				else
					flgPostingToBeDone = false;	
					
					
			}else{
				if(businessDate.compareTo(accountInfoDo.getDAT_POSTING()) > 0)
				{
					flgPostingToBeDone = true;
				}
				else
					flgPostingToBeDone = false;	
			}
			
			// Issue 15954 fix changed from dat_posting to dat_final_settelment ends
			
			if (logger.isDebugEnabled()){
				logger.debug("flgPostingToBeDone inside FLAG_BVT_CYCLE No : "+flgPostingToBeDone);
			}
		}
		
		//if(businessDate.toString().equalsIgnoreCase(accountInfoDo.getDAT_POSTING().toString()))
		//if((businessDate.toString().equalsIgnoreCase(accountInfoDo.getDAT_POSTING().toString()) || (accountInfoDo.getPARTY_DAT_END() != null && businessDate.toString().equalsIgnoreCase(accountInfoDo.getPARTY_DAT_END().toString()))) && flgPostingToBeDone)
		// ENH_JPMC_TAXBVT Starts
		//As per ned requirement id Date posting is same as business date then update status as NOPOSTING_STATUS  
		//if(businessDate.compareTo(accountInfoDo.getDAT_POSTING()) >= 0) // Issue fix ned
		
		
		//Issue fix ned
		//TI_FEE_CHANGES STARTS
		/*if((accountInfoDo.getCOD_TXN().substring(6,7).equalsIgnoreCase("B")) || 
					((accountInfoDo.getCOD_TXN().substring(6,7).equalsIgnoreCase("N")) && businessDate.compareTo(accountInfoDo.getDAT_POSTING()) > 0))*/
		/*if((accountInfoDo.getCOD_TXN().indexOf("B") > -1) || 
				((accountInfoDo.getCOD_TXN().indexOf("N") > -1) && businessDate.compareTo(accountInfoDo.getDAT_POSTING()) > 0))*/
		// ENH_JPMC_TAXBVT Ends
			//TI_FEE_CHANGES ENDS
		//Reallocate to Self
		postedAmountCentralCcy = amountToBePostedCentralCcy.add(postedAmountCentralCcy);
		if (logger.isDebugEnabled())
		{
			logger.debug("postedAmount: "+postedAmount);
			logger.debug("postedAmountCentralCcy: "+postedAmountCentralCcy);
						
		}
		accountInfoDo.setAMT_POSTED_CENTRAL_CCY(postedAmountCentralCcy);
		//Reallocate to Self
		if(flgPostingToBeDone) // Issue fix ned
		{
			if (logger.isDebugEnabled()){
				logger.debug("POSTING Cycle END ");
			}
			if (logger.isDebugEnabled())
			{
				
				logger.debug("BEFORE amtAccuredNotPosted : "+amtAccuredNotPosted);
				logger.debug("amountToBePosted :  "+amountToBePosted);
				//Reallocate to Self
				logger.debug("amountToBePostedCentralCcy : "+amountToBePostedCentralCcy); //sampada
				logger.debug("postedAmountCentralCcy : "+postedAmountCentralCcy); //sampada	
				//Reallocate to Self
				
			}
			//Set AMT_POSTED
			//postedAmount=amtAccuredNotPosted  + amountToBePosted
			postedAmount = postedAmount.add(amtAccuredNotPosted.add(amountToBePosted));
			if (logger.isDebugEnabled())
			{
				logger.debug("postedAmount: "+postedAmount);
							
			}
			accountInfoDo.setAMT_POSTED(postedAmount);
			//Resetting AMT_ACCRUEDNOTPOSTED to 0
			accountInfoDo.setAMT_ACCRUEDNOTPOSTED(ZERO);
			//status = POST_GEN
			accountInfoDo.setTXT_STATUS(ExecutionConstants.POSTING_GENERATED_STATUS);
			

		}
		else
		{
			if (logger.isDebugEnabled())
			{
				logger.debug("POSTING Cycle NOT END ");
				logger.debug("BEFORE amtAccuredNotPosted : "+amtAccuredNotPosted);
				logger.debug("amountToBePosted :  "+amountToBePosted);
				//Reallocate to Self
				logger.debug("amountToBePostedCentralCcy : "+amountToBePostedCentralCcy); //sampada
				logger.debug("postedAmountCentralCcy : "+postedAmountCentralCcy); //sampada	
				//Reallocate to Self
			}
			//amtAccuredNotPosted = amtAccuredNotPosted  + amountToBePosted;
			amtAccuredNotPosted = amtAccuredNotPosted.add(amountToBePosted);
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("AFETER amtAccuredNotPosted : "+amtAccuredNotPosted);
			accountInfoDo.setAMT_ACCRUEDNOTPOSTED(amtAccuredNotPosted);
			//Resetting AMT_POSTED to 0 
			accountInfoDo.setAMT_POSTED(ZERO);
			//status = NO_ACC
			accountInfoDo.setTXT_STATUS(ExecutionConstants.NOPOSTING_STATUS);

		}
		
		if (logger.isDebugEnabled()){
			logger.debug(" DATA OBJECT DETAILS " + accountInfoDo.toString());
		}
		if(cycleStart==false)
		{
			if (logger.isDebugEnabled()){
				logger.debug(" DATA OBJECT DETAILS populateBVTPostingSummary cycleStart  false" + accountInfoDo.toString());
			}
			//accountInfoDo.setAMT_ACCRUEDNOTPOSTED(0D);
			insertPostingSummary(insertPostingSummary,accountInfoDo);
			entries[4]=true;
		}
		else
		{
			if (logger.isDebugEnabled()){
				logger.debug(" DATA OBJECT DETAILS populateBVTPostingSummary cycleStart true (in else) " + accountInfoDo.toString());
			}
			updatePostingSummary(updatePostingSummary,accountInfoDo);
			entries[5]=true;
		}
		
		
		
		if (logger.isDebugEnabled()){
			logger.debug("Leaving populateBVTPostingSummary");
		}

	}	
	
	//ENH_10.3_154 Starts
	/**
	 * This method deals with PostingSummary table. It finds cycle start or end and does the update or insert operation accodingly
	 * @param businessDate
	 * @param insertPostingSummary
	 * @param updatePostingSummary
	 * @param accountInfoDo
	 * @param selectPostingSummary
	 * @throws LMSException
	 */
	//Changed Method name from 'populateCycleWiseBVTPostingSummary' to 'populateCycleWiseBVTPostingSummaryForPool'
	//private void populateCycleWiseBVTPostingSummary(	Date businessDate ,PreparedStatement insertPostingSummary, 
	  private void populateCycleWiseBVTPostingSummaryForPool(	Date businessDate ,PreparedStatement insertPostingSummary,
			PreparedStatement updatePostingSummary,
			AccountInfoDO accountInfoDo,PreparedStatement selectPostingSummary) throws LMSException 
	{
		if (logger.isDebugEnabled()){
			//logger.debug("Entering populateCycleWiseBVTPostingSummary");
			logger.debug("Entering populateCycleWiseBVTPostingSummaryForPool");
		}
		// Issue 15954 fix added flag
		//AE_ON_FINALSETTELMENT_DATE  JPMC starts
		String flg_AE_ON_FinalSettelment_Date = ExecutionConstants.AE_ON_FINALSETTELMENT_DATE;
		if (logger.isDebugEnabled()) {
		 logger.debug("AE_ON_FINALSETTELMENT_DATE ::::: " + flg_AE_ON_FinalSettelment_Date );
		}
		
		// Issue 15954 fix ends
		
		BigDecimal amtAccuredNotPosted =ZERO;
		BigDecimal amountToBePosted = ZERO;
		BigDecimal postedAmount=ZERO;
		BigDecimal amountToBePostedCentralCcy = ZERO, postedAmountCentralCcy=ZERO;//Reallocate to Self
		boolean cycleStart = true;
		boolean flgPostingToBeDone = false;
		//Identify the Posting cycle start 
		//cycleStart  = checkRecordInPostingSummary_BVT(accountInfoDo,selectPostingSummary, businessDate);
		cycleStart  = checkRecordInPostingSummary_BVTForPool(accountInfoDo,selectPostingSummary, businessDate);
		amtAccuredNotPosted = accountInfoDo.getAMT_ACCRUEDNOTPOSTED(); 
		postedAmount =  accountInfoDo.getAMT_POSTED();
		
		amountToBePosted=accountInfoDo.getAMT_TO_BE_POSTED();
		//Reallocate to Self
		amountToBePostedCentralCcy = accountInfoDo.getAMT_CAL_CENTRAL_CCY();
		postedAmountCentralCcy = accountInfoDo.getAMT_POSTED_CENTRAL_CCY();
		//Reallocate to Self
		if (logger.isDebugEnabled()){
			logger.debug("businessDate " + businessDate.toString());
			logger.debug("DAT_POSTING " + accountInfoDo.getDAT_POSTING().toString());
			logger.debug("accountInfoDo.getCOD_TXN()" + accountInfoDo.getCOD_TXN());
		}	
		
		// if cod_txn is is normal adjustment the 
		// ie - RCIBCI "N" SCI - that is sevent letter of COD_TXN - no_posting 
		// else RCIBCI "B" SCI  that is sevent letter of COD_TXN  is B then POSTING_GEN
		
			
		// AS per ned requirement date_busines is equal to posting then update status as  NOPOSTING_STATUS nitin
		//Ned_issue_70 fix - if condition modified
				
		if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase(ExecutionConstants.YES))
		{
			if (logger.isDebugEnabled()){
				logger.debug("Inside FLAG_BVT_CYCLE Yes");
			}
			//SUP_ISS_II_7834 starts
			/*if (accountInfoDo.getID_POOL().equalsIgnoreCase("NONPOOL_ACCT") || accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.INCOLOAN)){
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("accountInfoDo.getCOD_TXN().substring(7,8)" + accountInfoDo.getCOD_TXN().substring(7,8));
				if((accountInfoDo.getCOD_TXN().substring(7,8).equalsIgnoreCase("B")) || 
						((accountInfoDo.getCOD_TXN().substring(7,8).equalsIgnoreCase("N")) && businessDate.compareTo(accountInfoDo.getDAT_POSTING()) > 0))
				{
					flgPostingToBeDone = true;
				}
				else
					flgPostingToBeDone = false;
			}else{*/
			if((accountInfoDo.getCOD_TXN().substring(6,7).equalsIgnoreCase("B")) || 
					((accountInfoDo.getCOD_TXN().substring(6,7).equalsIgnoreCase("N")) && businessDate.compareTo(accountInfoDo.getDAT_POSTING()) > 0))
			{
				flgPostingToBeDone = true;
			}
			else
				flgPostingToBeDone = false;
			//}
			//SUP_ISS_II_7834 ends
			//SA_BVT_POSTING_CYCLE ENDS
			
			if (logger.isDebugEnabled()){
				logger.debug("flgPostingToBeDone inside FLAG_BVT_CYCLE Yes : "+flgPostingToBeDone);
			}
			
		}
		else
		{
			if (logger.isDebugEnabled()){
				logger.debug("Inside FLAG_BVT_CYCLE No");
			}
			
     // Issue 15954 fix changed from dat_posting to dat_final_settelment starts
			
			if(flg_AE_ON_FinalSettelment_Date.equalsIgnoreCase("Y")){
				if(businessDate.compareTo(accountInfoDo.getDAT_FINAL_SETTLEMENT()) > 0)
				{
					flgPostingToBeDone = true;
				}
				else
					flgPostingToBeDone = false;	
					
					
			}else{
				if(businessDate.compareTo(accountInfoDo.getDAT_POSTING()) > 0)
				{
					flgPostingToBeDone = true;
				}
				else
					flgPostingToBeDone = false;	
			}
			
			// Issue 15954 fix changed from dat_posting to dat_final_settelment ends	
			
			if (logger.isDebugEnabled()){
				logger.debug("flgPostingToBeDone inside FLAG_BVT_CYCLE No : "+flgPostingToBeDone);
			}
		}
		
		//Reallocate to Self
		postedAmountCentralCcy = amountToBePostedCentralCcy.add(postedAmountCentralCcy);
		if (logger.isDebugEnabled())
		{
			logger.debug("postedAmount: "+postedAmount);
			logger.debug("postedAmountCentralCcy: "+postedAmountCentralCcy);
						
		}
		accountInfoDo.setAMT_POSTED_CENTRAL_CCY(postedAmountCentralCcy);
		//Reallocate to Self
		if(flgPostingToBeDone)
		/*if((accountInfoDo.getCOD_TXN().indexOf("B") > -1 ) || 
				((accountInfoDo.getCOD_TXN().indexOf("N") > -1) && businessDate.compareTo(accountInfoDo.getDAT_POSTING()) > 0))*/
			//TI_FEE_CHANGES ENDS	
		//if(businessDate.compareTo(accountInfoDo.getDAT_POSTING()) >= 0) // Issue_fix_ned
		//if(businessDate.compareTo(accountInfoDo.getDAT_POSTING()) > 0)  // Issue_fix_ned
		{
			if (logger.isDebugEnabled()){
				logger.debug("POSTING Cycle END ");
			}
			if (logger.isDebugEnabled())
			{
				logger.debug("BEFORE amtAccuredNotPosted : "+amtAccuredNotPosted);
				logger.debug("amountToBePosted :  "+amountToBePosted);
			}
			//Set AMT_POSTED
			//postedAmount=amtAccuredNotPosted  + amountToBePosted
			postedAmount = postedAmount.add(amtAccuredNotPosted.add(amountToBePosted));
			if (logger.isDebugEnabled())
			{
				logger.debug("postedAmount: "+postedAmount);
			}
			accountInfoDo.setAMT_POSTED(postedAmount);
			//Resetting AMT_ACCRUEDNOTPOSTED to 0
			accountInfoDo.setAMT_ACCRUEDNOTPOSTED(ZERO);
			//status = POST_GEN
			accountInfoDo.setTXT_STATUS(ExecutionConstants.POSTING_GENERATED_STATUS);
		}
		else
		{
			if (logger.isDebugEnabled())
			{
				logger.debug("POSTING Cycle NOT END ");
				logger.debug("BEFORE amtAccuredNotPosted : "+amtAccuredNotPosted);
				logger.debug("amountToBePosted :  "+amountToBePosted);
			
				
			}
			//amtAccuredNotPosted = amtAccuredNotPosted  + amountToBePosted;
			amtAccuredNotPosted = amtAccuredNotPosted.add(amountToBePosted);
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("AFETER amtAccuredNotPosted : "+amtAccuredNotPosted);
			accountInfoDo.setAMT_ACCRUEDNOTPOSTED(amtAccuredNotPosted);
			//Resetting AMT_POSTED to 0 
			accountInfoDo.setAMT_POSTED(ZERO);
			//status = NO_ACC
			accountInfoDo.setTXT_STATUS(ExecutionConstants.NOPOSTING_STATUS);

		}
		
		if (logger.isDebugEnabled()){
			logger.debug(" DATA OBJECT DETAILS " + accountInfoDo.toString());
		}
		if(!cycleStart)
		{
			if (logger.isDebugEnabled()){
				logger.debug("INSERT DATA OBJECT DETAILS " + accountInfoDo.toString());
			}
			//accountInfoDo.setAMT_ACCRUEDNOTPOSTED(0D);
			//insertPostingSummary_BVT(insertPostingSummary,accountInfoDo);
			insertPostingSummary_BVTForPool(insertPostingSummary,accountInfoDo);
			entries[4]=true;
		}
		else
		{
			if (logger.isDebugEnabled()){
				logger.debug("UPDATE DATA OBJECT DETAILS " + accountInfoDo.toString());
			}
			//updatePostingSummary_BVT(updatePostingSummary,accountInfoDo);
			  updatePostingSummary_BVTForPool(updatePostingSummary,accountInfoDo);
			entries[5]=true;
		}
		
		
		
		if (logger.isDebugEnabled()){
			//logger.debug("Leaving populateCycleWiseBVTPostingSummary");
			logger.debug("Leaving populateCycleWiseBVTPostingSummaryForPool");
		}

	}
	
	/**
	 * This method checks whether an entry is there in  PostingSummary table for particular txn code and sets the some data into Data Object
	 * @param accountInfoDo
	 * @param selectPostingSummary
	 * @param businessDate 
	 * @return
	 * @throws LMSException
	 */
	  //Changed Method name
	//private boolean checkRecordInPostingSummary_BVT(AccountInfoDO accountInfoDo,PreparedStatement selectPostingSummary, Date businessDate) throws LMSException
	private boolean checkRecordInPostingSummary_BVTForPool(AccountInfoDO accountInfoDo,PreparedStatement selectPostingSummary, Date businessDate) throws LMSException
	{
		if (logger.isDebugEnabled())
		{
			//logger.debug("Entering checkRecordInPostingSummary_BVT");
			logger.debug("Entering checkRecordInPostingSummary_BVTForPool");
			logger.debug(" DATA OBJECT DETAILS " + accountInfoDo.toString());
		}
		
		ResultSet res = null;
		boolean found = false;
		String flg_AE_ON_FinalSettelment_Date = ExecutionConstants.AE_ON_FINALSETTELMENT_DATE;//SUP_ISS_IV_7602
		try
		{
			if (logger.isDebugEnabled())
			{
				//logger.debug("COD_TXN in checkRecordInPostingSummary_BVT "+accountInfoDo.getCOD_TXN());
				logger.debug("COD_TXN in checkRecordInPostingSummary_BVTForPool "+accountInfoDo.getCOD_TXN());
				logger.debug("getPARTICIPANT_ID >>> "+accountInfoDo.getPARTICIPANT_ID());
				logger.debug("getCOD_TXN()>>>"+accountInfoDo.getCOD_TXN());
				logger.debug("getDAT_POSTING_START()>>>"+accountInfoDo.getDAT_POSTING_START());
				logger.debug("getID_POOL()>>>"+accountInfoDo.getID_POOL());
				logger.debug("getTYP_PARTICIPANT()>>>>"+accountInfoDo.getTYP_PARTICIPANT());
				logger.debug("getDAT_INITIAL_POSTING()>>>"+accountInfoDo.getDAT_INITIAL_POSTING());
				logger.debug("getDAT_POSTING()>>>"+accountInfoDo.getDAT_POSTING());
				logger.debug("getDAT_BUSINESS()>>>"+businessDate+", DAT_POST_SETTLEMENT()>>>"+accountInfoDo.getDAT_POST_SETTLEMENT());//SUP_ISS_IV_7602
			  }
			  //HSBC_PERFORMANCE ISSUE STARTS
			boolean allowIntTyp = false;
			
			if((accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)))
				allowIntTyp = ExecutionConstants.FLG_SA_INTEREST_TYPE;
			else
				allowIntTyp = ExecutionConstants.FLG_INTACC_POST_INTTYP ;
			
			//HSBC_PERFORMANCE ISSUE ENDS
			
			boolean flg_newAccountingentry = ExecutionConstants.FlG_NEW_ACCOUNTING_ENTRY_FORMAT; // NEW_ACCOUNTING_ENTRY_FORMAT
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("flg_newAccountingentry:::"+flg_newAccountingentry); // NEW_ACCOUNTING_ENTRY_FORMAT
			
			String codTxn =null;
			if(allowIntTyp)     //Sandeep changes for ned issue
			{
					if("I".equalsIgnoreCase(accountInfoDo.getTYP_INTEREST()))
						codTxn  = accountInfoDo.getCOD_TXN();
					else
						codTxn = accountInfoDo.getCOD_TXN()+accountInfoDo.getTYP_INTEREST();
			}
			else{
						codTxn = accountInfoDo.getCOD_TXN();
			}
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("codTxn : "+codTxn);
			selectPostingSummary.setString(1,accountInfoDo.getPARTICIPANT_ID());
			selectPostingSummary.setString(2,codTxn);
			selectPostingSummary.setDate(3,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING_START()));
			selectPostingSummary.setString(4,accountInfoDo.getID_POOL());
			selectPostingSummary.setString(5,accountInfoDo.getTYP_PARTICIPANT());
			selectPostingSummary.setDate(6,LMSUtility.truncSqlDate(accountInfoDo.getDAT_INITIAL_POSTING()));
			//Ned_issue_70 fix starts
			selectPostingSummary.setDate(7,LMSUtility.truncSqlDate((java.sql.Date) businessDate)); 
			selectPostingSummary.setString(8, "NO_POSTING" );
			//SUP_ISS_IV_7602 starts
			if(flg_AE_ON_FinalSettelment_Date.equalsIgnoreCase("Y") && !accountInfoDo.getID_POOL().equalsIgnoreCase("NONPOOL_ACCT")){
				selectPostingSummary.setDate(9,(java.sql.Date) businessDate);
				selectPostingSummary.setDate(10,accountInfoDo.getDAT_POST_SETTLEMENT());
				selectPostingSummary.setDate(11,accountInfoDo.getDAT_POSTING_START());
				selectPostingSummary.setDate(12,accountInfoDo.getDAT_POSTING());
				selectPostingSummary.setDate(13,accountInfoDo.getDAT_INITIAL_POSTING());
				selectPostingSummary.setString(14,accountInfoDo.getSOURCE_POOL_ID());
			}else{
				selectPostingSummary.setString(9,accountInfoDo.getSOURCE_POOL_ID()); // NEW_ACCOUNTING_ENTRY_FORMAT
			}
			//SUP_ISS_IV_7602 ends
			//Ned_issue_70 ends
			res = selectPostingSummary.executeQuery();
			while(res.next())
			{
				found = true;
				accountInfoDo.setAMT_ACCRUEDNOTPOSTED(res.getBigDecimal("AMT_ACCRUEDNOTPOSTED"));
				accountInfoDo.setAMT_POSTED(res.getBigDecimal("AMT_POSTED"));
				accountInfoDo.setOLD_DAT_FINAL_SETTLEMENT(res.getDate("DAT_FINAL_SETTLEMENT"));
				accountInfoDo.setOLD_DAT_POSTING(res.getDate("DAT_POSTING"));
				accountInfoDo.setAMT_POSTED_CENTRAL_CCY(res.getBigDecimal("AMT_POSTED_CENTRAL_CCY"));//Reallocate to Self
				if (logger.isDebugEnabled())
				{
				//Reallocate to Self
					logger.debug("IN DB amtAccuredNotPosted " + res.getBigDecimal("AMT_ACCRUEDNOTPOSTED"));
					logger.debug("IN DB AMT_POSTED " + res.getBigDecimal("AMT_POSTED"));
					logger.debug("IN DB AMT_POSTED_CENTRAL_CCY " + res.getBigDecimal("AMT_POSTED_CENTRAL_CCY"));
					//Reallocate to Self
				}
			}
		}
		catch(Exception e)
		{
			//logger.fatal("Exception in checkRecordInPostingSummary_BVT :: " , e);
			logger.fatal("Exception in checkRecordInPostingSummary_BVTForPool :: " , e);
			//ENH_IS1059  Starts
			//throw new LMSException("Error occured in checkRecordInPostigSummary", e.toString());
			throw new LMSException("LMS01000","Record already exist in OPOOL_POSTINGSUMMARY_BVT table for Transaction Id "+accountInfoDo.getCOD_TXN()+", Pool Id :- "+accountInfoDo.getID_POOL()+", Participant :- "+accountInfoDo.getPARTICIPANT_ID()+" and Participant Type :"+accountInfoDo.getTYP_PARTICIPANT()+".", e);
			//ENH_IS1059  Ends
		}finally
		{
		    LMSConnectionUtility.close(res);
	    }
		if (logger.isDebugEnabled()){
			//logger.debug("Leaving checkRecordInPostingSummary_BVT");
			logger.debug("Leaving checkRecordInPostingSummary_BVTForPool");
		}
		return found;
	}
	
	/**
	 * This method set the parameter into insertPostingSummary statement
	 * @param insertPostingSummary
	 * @param accountInfoDo
	 * @throws LMSException
	 */
	//Changed Method Name
	//private void insertPostingSummary_BVT(PreparedStatement insertPostingSummary, AccountInfoDO accountInfoDo) throws LMSException 
	private void insertPostingSummary_BVTForPool(PreparedStatement insertPostingSummary, AccountInfoDO accountInfoDo) throws LMSException 
	{
		if (logger.isDebugEnabled()){
			//logger.debug("Entering insertPostingSummary_BVT >>>"+ accountInfoDo.getCOD_TXN()+","+accountInfoDo.getTYP_INTEREST());
			logger.debug("Entering insertPostingSummary_BVTForPool >>>"+ accountInfoDo.getCOD_TXN()+","+accountInfoDo.getTYP_INTEREST());
		}
//Retro_Mashreq
		//SUPNBAD_4743 STARTS
		boolean allowIntTyp = false;
		if((accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)))
			allowIntTyp = ExecutionConstants.FLG_SA_INTEREST_TYPE;
		else
			allowIntTyp = ExecutionConstants.FLG_INTACC_POST_INTTYP ;           //Sandeep changes for ned issue
		//SUPNBAD_4743 ENDS
		String codTxn = null;
		try
		{
			if (!(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)))
			{
				if(!(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCO)))
				{
					boolean holdreq=chkPendingHoldReq(accountInfoDo);
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("holdreq -- "+holdreq);
					
					if(holdreq)
					{
					logger.debug("Hold req. is present in populate posting summary");
					 accountInfoDo.setTXT_STATUS("HOLD");
					}
				} 
			} 
			
			insertPostingSummary.setString(1,accountInfoDo.getID_POOL());
			insertPostingSummary.setString(2,accountInfoDo.getPARTICIPANT_ID());
			if(allowIntTyp)      //Sandeep changes for ned issue
			{
				if("I".equalsIgnoreCase(accountInfoDo.getTYP_INTEREST()))
					codTxn = accountInfoDo.getCOD_TXN();
				else
					codTxn = accountInfoDo.getCOD_TXN()+accountInfoDo.getTYP_INTEREST();
			}
			else{
				codTxn = accountInfoDo.getCOD_TXN();
			}
			insertPostingSummary.setString(3,codTxn);
			insertPostingSummary.setDate(4,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING_START()));
			insertPostingSummary.setDate(5,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING()));
			insertPostingSummary.setDate(6,LMSUtility.truncSqlDate(accountInfoDo.getDAT_FINAL_SETTLEMENT()));
			
			insertPostingSummary.setBigDecimal(7,accountInfoDo.getAMT_ACCRUEDNOTPOSTED());
			insertPostingSummary.setBigDecimal(8,accountInfoDo.getAMT_POSTED());
			insertPostingSummary.setString(9,accountInfoDo.getTXT_STATUS());
			insertPostingSummary.setDate(10,LMSUtility.truncSqlDate(accountInfoDo.getDAT_VALUE()));
			insertPostingSummary.setString(11,accountInfoDo.getTYP_PARTICIPANT());
//Retro_Mashreq
			insertPostingSummary.setString(12,com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT.format(new java.util.Date()));
			insertPostingSummary.setDate(13,LMSUtility.truncSqlDate(accountInfoDo.getDAT_CURRENT_BUSINESS()));
			insertPostingSummary.setDate(14,LMSUtility.truncSqlDate(accountInfoDo.getDAT_INITIAL_POSTING()));
			insertPostingSummary.setString(15,accountInfoDo.getSOURCE_POOL_ID());
			insertPostingSummary.setBigDecimal(16,accountInfoDo.getAMT_POSTED_CENTRAL_CCY());//Reallocate to Self
			insertPostingSummary.addBatch(); 
			}
		catch(SQLException e)
		{
			//logger.fatal("Exception in insertPostingSummary_BVT :: " , e);
			logger.fatal("Exception in insertPostingSummary_BVTForPool :: " , e);
			throw new LMSException("LMS01000","Insertion of  posting summary in OPOOL_POSTINGSUMMARY_BVT table failed for Transaction Id  :- "+accountInfoDo.getCOD_TXN()+", Pool Id  :- "+accountInfoDo.getID_POOL()+", Participant Id :- "+ accountInfoDo.getPARTICIPANT_ID()+" and Type Participant Type is "+accountInfoDo.getTYP_PARTICIPANT()+".", e);
		}
		
		if (logger.isDebugEnabled()){
			//logger.debug("Leaving insertPostingSummary_BVT");
			logger.debug("Leaving insertPostingSummary_BVTForPool");
		}
	}
	
	/**This method set the parameter into updatePostingSummary statement
	 * @param updatePostingSummary
	 * @param accountInfoDo
	 * @throws LMSException
	 */
	// Changed Method name
	//private void updatePostingSummary_BVT(PreparedStatement updatePostingSummary, AccountInfoDO accountInfoDo) throws LMSException {
	  private void updatePostingSummary_BVTForPool(PreparedStatement updatePostingSummary, AccountInfoDO accountInfoDo) throws LMSException {
		if (logger.isDebugEnabled()){
			//logger.debug("Entering updatePostingSummary_BVT>>>"+ accountInfoDo.getCOD_TXN()+","+accountInfoDo.getTYP_INTEREST());
			  logger.debug("Entering updatePostingSummary_BVTForPool>>>"+ accountInfoDo.getCOD_TXN()+","+accountInfoDo.getTYP_INTEREST());
		}
//Retro_Mashreq
		//SUPNBAD_4743 STARTS
		boolean allowIntTyp = false;
		
		if((accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)))
			allowIntTyp = ExecutionConstants.FLG_SA_INTEREST_TYPE;
		else
			allowIntTyp = ExecutionConstants.FLG_INTACC_POST_INTTYP ;
		
		//SUPNBAD_4743 ENDS
		
		String codTxn = null;
		try
		{
			if (!(accountInfoDo.getID_POOL().equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT)))
			{
				logger.debug("calling function chkHoldReq() ");
				if(!(accountInfoDo.getTYP_PARTICIPANT().equalsIgnoreCase(ExecutionConstants.INCO)))
				{
					boolean holdreq=chkPendingHoldReq(accountInfoDo);
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("holdreq -- "+holdreq);
					
					if(holdreq)
					{
						logger.debug("Hold req. is present in updatePostingSummary");
						accountInfoDo.setTXT_STATUS("HOLD");
					}
				}
			}
			
			updatePostingSummary.setBigDecimal(1,accountInfoDo.getAMT_ACCRUEDNOTPOSTED());
			updatePostingSummary.setBigDecimal(2,accountInfoDo.getAMT_POSTED());
			updatePostingSummary.setString(3,accountInfoDo.getTXT_STATUS());
			updatePostingSummary.setDate(4,LMSUtility.truncSqlDate(accountInfoDo.getDAT_VALUE()));
			updatePostingSummary.setDate(5,LMSUtility.truncSqlDate(accountInfoDo.getDAT_FINAL_SETTLEMENT()));
			updatePostingSummary.setDate(6,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING()));
			// FindBug issue fix starts
			//updatePostingSummary.setString(7,com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT.format(new java.util.Date()));
			updatePostingSummary.setString(7,REF_DATE_FORMAT.format(new java.util.Date()));//Harikesh
			// FindBug issue fix ends
			updatePostingSummary.setDate(8,LMSUtility.truncSqlDate(accountInfoDo.getDAT_CURRENT_BUSINESS()));
			//SETTING WHRER CLAUSE PLACE HOLDERS
			//Reallocate to Self
			updatePostingSummary.setBigDecimal(9,accountInfoDo.getAMT_POSTED_CENTRAL_CCY());
			updatePostingSummary.setString(10,accountInfoDo.getPARTICIPANT_ID());
			//Reallocate to Self
			if(allowIntTyp)         //Sandeep changes for ned issue
			{
				if("I".equalsIgnoreCase(accountInfoDo.getTYP_INTEREST()))
					codTxn = accountInfoDo.getCOD_TXN();
				else
					codTxn = accountInfoDo.getCOD_TXN()+accountInfoDo.getTYP_INTEREST();
			}
			else{
				codTxn = accountInfoDo.getCOD_TXN();
			}
			//Reallocate to Self
			updatePostingSummary.setString(11,codTxn);
			updatePostingSummary.setDate(12,LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING_START()));
			updatePostingSummary.setString(13,accountInfoDo.getID_POOL());
			updatePostingSummary.setString(14,accountInfoDo.getTYP_PARTICIPANT());
			updatePostingSummary.setDate(15,LMSUtility.truncSqlDate(accountInfoDo.getDAT_INITIAL_POSTING()));
			updatePostingSummary.setDate(16,LMSUtility.truncSqlDate(accountInfoDo.getDAT_CURRENT_BUSINESS())); //Ned issue 70
			updatePostingSummary.setString(17,accountInfoDo.getSOURCE_POOL_ID());
			//Reallocate to Self
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("updatePostingSummary_BVTForPool  " + accountInfoDo.getSOURCE_POOL_ID());
			updatePostingSummary.addBatch();
		}
		catch(SQLException e)
		{
			//logger.fatal("Exception in updatePostingSummary_BVT :: " , e);
			logger.fatal("Exception in updatePostingSummary_BVTForPool :: " , e);
			throw new LMSException("LMS01000","Updation of  posting summary in OPOOL_POSTINGSUMMARY_BVT table failed for Transaction Id  :- "+accountInfoDo.getCOD_TXN()+" Pool Id  :- "+accountInfoDo.getID_POOL() +" Participant Id :- "+accountInfoDo.getPARTICIPANT_ID()+" and Type Participant Type is "+accountInfoDo.getTYP_PARTICIPANT()+".", e);
		}
		
		if (logger.isDebugEnabled()){
			//logger.debug("Leaving updatePostingSummary_BVT");
			logger.debug("Leaving updatePostingSummary_BVTForPool");
		}
		
	}
	//ENH_10.3_154 Ends
	
	/**
	 * This method is to manipulate the codTxn as per type 
	 * @param codTxn
	 * @param type
	 * @return
	 */
	public  String  setTxnCode(String codTxn , String type)
    {
  	  if (logger.isDebugEnabled()){
 			logger.debug("Entering");
  	  }

  	  String finalCodeTxn = null;  	 
  	  StringBuilder sbufCodeTxn = new StringBuilder(0);
  	  sbufCodeTxn.append(codTxn);
  	  
  	  if (type.equalsIgnoreCase(ExecutionConstants.ACCRUAL))
  	  { 
  		 finalCodeTxn = sbufCodeTxn.replace(1,2,"A").toString();    		  
  	  }
  		    	   
  	  if(type.equalsIgnoreCase(ExecutionConstants.POSTING))
  	  {
  	     finalCodeTxn = sbufCodeTxn.replace(1,2,"P").toString();    		
  	  }
  	  if(type.equalsIgnoreCase("BVT"))
  	  {
  		 finalCodeTxn = sbufCodeTxn.replace(3,4,"N").toString();    
  	  }
  	  if(type.equalsIgnoreCase("CREDIT"))
 	  {
 		 finalCodeTxn = sbufCodeTxn.replace(4,5,"C").toString();    
 	  }
  	  if(type.equalsIgnoreCase("DEBIT"))
	  {
		 finalCodeTxn = sbufCodeTxn.replace(4,5,"D").toString();    
	  }
  	  if (logger.isDebugEnabled())
  	  {
  		   logger.debug("finalCodeTxn"+finalCodeTxn);
  		   logger.debug("Leaving");
  	  }
 			return finalCodeTxn;
    }
	
	/**
	 * This method provides the nextCyclePostingDates given cod_gl, id_pool, nbr_processId
	 * If the bvt came just after the day of dat_posting, this helps in calculating the next_debit_delay
	 * and next_credit_delay.
	 *
	 * @param conn
	 * @param poolId
	 * @param codGL
	 * @param processId
    * @param businessDate
	 * @return
	 */
	
	public static  Date[]calculateNextCyclePostingDates(Connection conn, String poolId, String codGL, long processId, Date businessDate)
           throws LMSException {
		/** ****** JTEST Fix Start ***********  on 14DEC09*/
//		Statement stmt = null;
		PreparedStatement pStmt = null;
		/** ****** JTEST Fix End ***********  on 14DEC09*/
		ResultSet rSet = null;
		StringBuilder sBuf = new StringBuilder();
		String callMode = "E";
		//Do Sanity checks for conn==null, poolId==null, codGL==null, processId=0, businessDate==null
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		SimpleDateFormat formatter = new SimpleDateFormat(LMSConstants.DATE_FORMAT);
//		SimpleDateFormat formatter = new SimpleDateFormat(LMSConstants.DATE_FORMAT,Locale.US);
		/** ****** JTEST FIX end **************  on 14DEC09*/
		/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//		String bDate = formatter.format(businessDate);
		/** ****** FindBugs Fix End ***********  on 14DEC09*/
		java.util.Date[] postingDates = new java.util.Date[8];//MASHREQ_BVT_ISSUE

		try{	
			/** ****** JTEST Fix Start ***********  on 14DEC09*/
//			stmt = conn.createStatement();
			/** ****** JTEST Fix End ***********  on 14DEC09*/
			/*sBuf.append(" SELECT DISTINCT (SELECT cod_calendar ");
			sBuf.append(" FROM orbicash_glmst "); 
			sBuf.append(" WHERE cod_gl = '"+codGL+"') calendar, dat_start, cod_post_cycle, ");
			//sBuf.append(" dat_lastposting, dat_lastpostingcalc ");
			sBuf.append(" dat_posting, dat_adjustedposting ");
			sBuf.append(" FROM opool_txnpoolhdr ");
			sBuf.append(" WHERE cod_gl = '"+codGL+"' AND nbr_processid = "+processId);
			sBuf.append(" AND id_pool = '"+poolId+"' ");
			sBuf.append(" ORDER BY DAT_POSTING DESC ");*/
			/** ****** FindBugs Fix Start ***********  on 14DEC09*/
			sBuf.append(" SELECT DISTINCT (SELECT cod_calendar FROM OLM_GLMST WHERE cod_gl = ?) calendar,");
			sBuf.append(" dat_start, POST_CYCLE.cod_post_cycle,dat_posting,dat_adjustedposting,PSTCYCLE_CHNG, ");
			sBuf.append(" DAT_FIRST_POSTING,POST_CYCLE.NBR_SETTLE_DAYS,POST_CYCLE.NBR_DELAY_DAYS_CR,");
			sBuf.append(" POST_CYCLE.NBR_DELAY_DAYS_DR,POST_CYCLE.COD_NON_WORKING_DAY,hdr.dat_business_poolrun");
			sBuf.append(" FROM OPOOL_TXNPOOLHDR hdr,ORATE_POSTING_CYCLE POST_CYCLE");
			sBuf.append(" WHERE nbr_processid = ? AND id_pool = ? AND hdr.cod_post_cycle = POST_CYCLE.cod_post_cycle");
			sBuf.append(" ORDER BY hdr.dat_business_poolrun DESC ");
			//SUP_IS2398 ends						
			if(logger.isDebugEnabled()){
				logger.debug(" calculateNextCyclePostingDates -> "+sBuf.toString());
			}
			//rSet = stmt.executeQuery(sBuf.toString());
			pStmt = conn.prepareStatement(sBuf.toString());
			pStmt.setString(1, codGL);
			pStmt.setLong(2, processId);
			pStmt.setString(3, poolId);
			rSet = pStmt.executeQuery();
			/** ****** JTEST Fix End ***********  on 14DEC09*/
			if(rSet!= null && rSet.next()){
			//SUP_IS2398 starts
				String calendarCode = rSet.getString("calendar");
				Date poolStartDate = rSet.getDate("dat_start");
				String frequency = rSet.getString("cod_post_cycle");
				Date postingDate = rSet.getDate("dat_posting");
				Date lastCalcPostingDate = rSet.getDate("dat_adjustedposting");
				String postCycleChg = rSet.getString("PSTCYCLE_CHNG");
				Date datFirstPost = rSet.getDate("DAT_FIRST_POSTING");
				int settlementDay = rSet.getInt("NBR_SETTLE_DAYS");
				int crDelay = rSet.getInt("NBR_DELAY_DAYS_CR");
				int drDelay = rSet.getInt("NBR_DELAY_DAYS_DR");
				String codNonWorkDay = rSet.getString("COD_NON_WORKING_DAY");
				
				if (postingDate == null || lastCalcPostingDate == null || ("Y").equalsIgnoreCase(postCycleChg)) {
					//FindBug issue fix on 13MARCH2012 start
					if(postingDates == null){
						postingDates = new java.util.Date[5];
					}
					//FindBug issue fix on 13MARCH2012 end					
					postingDates[0]=datFirstPost;
					postingDates[1]=postingDate;
					postingDates[2]=lastCalcPostingDate;
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug(" poolStartDate -> "+poolStartDate +" lastPostingDate-> " +postingDate);
					postingDates[4] = ExecutionUtility.getAnotherWorkingDay(calendarCode, postingDate, 
							codNonWorkDay.charAt(0), 0, 0, settlementDay+drDelay);
					postingDates[3] = ExecutionUtility.getAnotherWorkingDay(calendarCode, postingDate, 
							codNonWorkDay.charAt(0), 0, 0, settlementDay+crDelay);
					
							if(logger.isDebugEnabled()){
							logger.debug("settlementDate_post:"+postingDates[4]+"crDelayDate_post:"+postingDates[5]);
							}
					
				}else{
	//SUP_IS2398 ends				
				//TODO Invoke executionUtility method
//				if (lastPostingDate == null || lastCalcPostingDate == null) {
//					lastPostingDate = poolStartDate;
//					lastCalcPostingDate = poolStartDate;
//					callMode = "M";
//				} 
//				else{
					callMode = "E";
//				}
				// validateInputParameters is called in ExecutionUtility's getNextPostingCycleDate
				postingDates = ExecutionUtility.getNextPostingCycleDate(conn,frequency,postingDate,lastCalcPostingDate,calendarCode,businessDate,callMode);
//				java.util.Date actualDate_post=postingDates[0];
//				java.util.Date adjustedDate_post=postingDates[1];
//				java.util.Date settlementDate_post=postingDates[2];
//				java.util.Date crDelayDate_post=postingDates[3];
//				java.util.Date drDelayDate_post=postingDates[4];
			}
			}//SUP_IS2398
//SIT2DEV Retro -DD end
			else{
				//ENH_IS1059  Starts
				//throw new LMSException("2001010 "," Unable to calculateNextCyclePostingDates ");
				throw new LMSException("LMS01000","Error occurred while calculating next cycle Posting dates for Process Id :- "+ processId + " host System :- "+codGL+" and Pool Id :- "+poolId+".");
				//ENH_IS1059  Ends
			}
		}
		catch(Exception e){
			logger.fatal("Exception in calculateNextCyclePostingDates ", e);
			  throw new LMSException("101302", e.toString());
		}
		finally{
			LMSConnectionUtility.close(rSet);
			/** ****** JTEST Fix Start ***********  on 14DEC09*/
//			LMSConnectionUtility.close(stmt);
			LMSConnectionUtility.close(pStmt);
			/** ****** JTEST Fix End ***********  on 14DEC09*/
		}
		return postingDates;
	}
	
	//ENH_IS1297 Starts...BVT Changes
	/**
	 * @param conn
	 * @param processId
	 * @param businessDate
	 * @throws LMSException
	 * 
	 * This method calculates next accrual cycle dates for BVT
	 * 
	 */
	public void sABVTcalculateNextCycleAccrualDates (Connection conn, long processId, Date businessDate) throws LMSException {
		PreparedStatement pstmt = null;
		ResultSet rst  = null;
		String callMode = "E";
		
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		SimpleDateFormat formatter = new SimpleDateFormat(LMSConstants.DATE_FORMAT);
//		SimpleDateFormat formatter = new SimpleDateFormat(LMSConstants.DATE_FORMAT,Locale.US);
		/** ****** JTEST FIX end **************  on 14DEC09*/
		/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//		String bDate = formatter.format(businessDate);
		/** ****** FindBugs Fix End ***********  on 14DEC09*/
		ArrayList calendarCodes = null;

		try {
			
			setSqlProperties();
			
			String CalculateBVTNextAccrCycle = (String)interestCalculatorSql.get("CalculateBVTNextAccrCycle");
			pstmt = conn.prepareStatement(CalculateBVTNextAccrCycle);
			pstmt.setLong(1,processId);
			pstmt.setString(2,ExecutionConstants.ACCOUNT);
			pstmt.setDate(3,LMSUtility.truncSqlDate((java.sql.Date) businessDate));//HSBC_IN21607119
			pstmt.setString(4,ExecutionConstants.ACTIVE);
			pstmt.setString(5,ExecutionConstants.ACCOUNT);
			
			rst = pstmt.executeQuery();
			
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
			logger.debug("Query for fetching Accrual information for BVT is "+CalculateBVTNextAccrCycle);
			
			while (rst.next()) {
				String calendarCode = rst.getString(1);
				Date AccountStartDate = rst.getDate(2);
				String frequency = rst.getString(3);
				Date lastAccrualDate = rst.getDate(4);
				String codEntity = rst.getString(5);				
				String hostCode = rst.getString(6);
				int typeFrequency = rst.getInt(7);
				int frequencyInfo = rst.getInt(8);
				int frequencyUnit1 = rst.getInt(9);
				int frequencyUnit2 = rst.getInt(10);
				String holidayFlag = rst.getString(11);
				
				Date lastCalcAccrualDate = null;
				java.util.Date AccrualStartDate = null;
				if(lastAccrualDate != null ){
					GregorianCalendar gDate = new GregorianCalendar();
					gDate.setTime(lastAccrualDate);
					gDate.add(Calendar.DATE,1);
					AccrualStartDate = gDate.getTime();
					
					lastCalcAccrualDate = lastAccrualDate;
				}
				//ENH_IS1297 Starts
				else {
					AccrualStartDate = AccountStartDate;
				}
				//ENH_IS1297 Ends
				
				//ENH_IS1297 Starts...External BVT Changes
				GregorianCalendar bsDate = new GregorianCalendar();
				bsDate.setTime(businessDate);
				bsDate.add(Calendar.DATE,-1);
				Date prevBsDate = bsDate.getTime();
				//ENH_IS1297 Ends
				
				if(logger.isInfoEnabled()){
			        logger.info(" Adding information of pool ["+codEntity+"] as [calendarCode] = ["+calendarCode+"]," +
			        " [poolStartDate] = ["+AccountStartDate+"], [frequency] = ["+frequency+"], [lastAccrualDate] = ["+lastAccrualDate+"]" +
			        ", [lastCalcAccrualDate] = ["+lastCalcAccrualDate+"], [hostCode] = ["+hostCode+"]+" 
			        +"{ Frequency["+typeFrequency+"], FrequencyInfo["+frequencyInfo+"], "
					+"FrequencyUnit1["+frequencyUnit1+"], FrequencyUnit2["+frequencyUnit2+"], "
					+"HolidayFlag["+holidayFlag+"],[AccrualStartDate] = ["+ AccrualStartDate+"]"+"Prev Bs Date = ["+prevBsDate+"]");//ENH_IS1297...External BVT Changes
			    }
				calendarCodes = new ArrayList();
				calendarCodes.add(calendarCode);

				//declaring a return parameter 
				java.util.Date[] nextActionDates = null;
				
				//ENH_IS1297 Starts...External BVT Changes
				//if(lastAccrualDate == null ){
				if(lastAccrualDate == null || (prevBsDate.compareTo(lastAccrualDate) > 0)){
				//ENH_IS1297 Ends
					
				    callMode = "M";
				    lastCalcAccrualDate = businessDate;//ENH_IS1297 External BVT Changes
				}
				if(callMode.equals("E")){
					nextActionDates = 
						ExecutionUtility.getNextActionDate(calendarCodes, lastAccrualDate, lastCalcAccrualDate
										 , typeFrequency, frequencyInfo, frequencyUnit1, frequencyUnit2,null
										 , holidayFlag, businessDate, callMode);
				}
				else{
				    nextActionDates = 
						
				    	//ENH_IS1297 Starts...External BVT Changes
				    	//ExecutionUtility.getNextActionDate(calendarCodes, AccountStartDate, AccountStartDate
				    	ExecutionUtility.getNextActionDate(calendarCodes, AccountStartDate, businessDate
				    	//ENH_IS1297 Ends
										 , typeFrequency, frequencyInfo, frequencyUnit1, frequencyUnit2,null
										 , holidayFlag, businessDate, callMode);
				}
				
				Date dateArray[] = new Date[3];

				
				dateArray[0] = nextActionDates[0];//Date_accrual
				dateArray[1] = nextActionDates[1];//Adjusted Accrual
				dateArray[2] = AccrualStartDate;// Accrual date start
				accrualDatesCache.put(codEntity,dateArray);
				if (logger.isDebugEnabled()) {
                    logger.debug(" KEY:["+codEntity+"] & VALUE:["+dateArray[0]+", "+dateArray[1]+dateArray[2]+"] added ");                                 
                }
			}
		}
		catch (Exception e){
			throw new LMSException("LMS01000","Error occurred while calculating next cycle Accrual dates for Process Id :- "+ processId );
		}
		finally {
			LMSConnectionUtility.close(rst);
			LMSConnectionUtility.close(pstmt);
		}
	}
	
//	ENH_IS1297 Starts...BVT Changes
	/**
	 * @param conn
	 * @param processId
	 * @param businessDate
	 * @throws LMSException
	 * 
	 * This method calculates next accrual cycle dates for BVT for Intercompamy
	 * 
	 */
	public void incoBVTcalculateNextCycleAccrualDates (Connection conn, long processId, Date businessDate,Date startDate) throws LMSException {
		PreparedStatement pstmt = null;
		ResultSet rst  = null;
		String callMode = "E";
		if (logger.isDebugEnabled())
			logger.debug("Entering incoBVTcalculateNextCycleAccrualDates");
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		SimpleDateFormat formatter = new SimpleDateFormat(LMSConstants.DATE_FORMAT);
//		SimpleDateFormat formatter = new SimpleDateFormat(LMSConstants.DATE_FORMAT, Locale.US);
		/** ****** JTEST FIX end **************  on 14DEC09*/
		/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//		String bDate = formatter.format(businessDate);
		/** ****** FindBugs Fix End ***********  on 14DEC09*/
		ArrayList calendarCodes = null;

		try {
			
			setSqlProperties();
			
			String calculateInCoBVTNextAccrCycle = (String)interestCalculatorSql.get("calculateInCoBVTNextAccrCycle");
			pstmt = conn.prepareStatement(calculateInCoBVTNextAccrCycle);
			pstmt.setLong(1,processId);
			pstmt.setString(2,ExecutionConstants.INCOLOAN);
			
			rst = pstmt.executeQuery();
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("Query for fetching Accrual information for BVT is "+calculateInCoBVTNextAccrCycle);
			
			while (rst.next()) {
				String calendarCode = rst.getString(1);
				Date incoStartDate = rst.getDate(2);
				String frequency = rst.getString(3);
				Date lastAccrualDate = rst.getDate(4);
				String codEntity = rst.getString(5);				
				String hostCode = rst.getString(6);
				int typeFrequency = rst.getInt(7);
				int frequencyInfo = rst.getInt(8);
				int frequencyUnit1 = rst.getInt(9);
				int frequencyUnit2 = rst.getInt(10);
				String holidayFlag = rst.getString(11);
				
				Date lastCalcAccrualDate = null;
				java.util.Date AccrualStartDate = null;
				if(lastAccrualDate != null ){
					GregorianCalendar gDate = new GregorianCalendar();
					gDate.setTime(lastAccrualDate);
					gDate.add(Calendar.DATE,1);
					//added by aniruddh for AccrualStartDate
					AccrualStartDate = startDate; //ENH_IS1913
					
					lastCalcAccrualDate = lastAccrualDate;
				}
				//ENH_IS1297 Starts
				else {
					AccrualStartDate = incoStartDate;
				}
				//ENH_IS1297 Ends
				
				//ENH_IS1297 Starts...External BVT Changes
				GregorianCalendar bsDate = new GregorianCalendar();
				bsDate.setTime(businessDate);
				bsDate.add(Calendar.DATE,-1);
				Date prevBsDate = bsDate.getTime();
				//ENH_IS1297 Ends

				
				
				if(logger.isInfoEnabled()){
			        logger.info(" Adding information of Intercompany Loans ["+codEntity+"] as [calendarCode] = ["+calendarCode+"]," +
			        " [incoStartDate] = ["+incoStartDate+"], [frequency] = ["+frequency+"], [lastAccrualDate] = ["+lastAccrualDate+"]" +
			        ", [lastCalcAccrualDate] = ["+lastCalcAccrualDate+"], [hostCode] = ["+hostCode+"]+" 
			        +"{ Frequency["+typeFrequency+"], FrequencyInfo["+frequencyInfo+"], "
					+"FrequencyUnit1["+frequencyUnit1+"], FrequencyUnit2["+frequencyUnit2+"], "
					+"HolidayFlag["+holidayFlag+"],[AccrualStartDate] = ["+ AccrualStartDate+"]"+"Prev Bs Date = ["+prevBsDate+"]");//ENH_IS1297...External BVT Changes
			    }
				calendarCodes = new ArrayList();
				calendarCodes.add(calendarCode);

				//declaring a return parameter 
				java.util.Date[] nextActionDates = null;
				
				if(lastAccrualDate == null || (prevBsDate.compareTo(lastAccrualDate) > 0)){
				    callMode = "M";
				    lastCalcAccrualDate = businessDate;
				}
				if(callMode.equals("E")){
					nextActionDates = 
						ExecutionUtility.getNextActionDate(calendarCodes, lastAccrualDate, lastCalcAccrualDate
										 , typeFrequency, frequencyInfo, frequencyUnit1, frequencyUnit2,null
										 , holidayFlag, businessDate, callMode);
				}
				else{
				    nextActionDates = 
						ExecutionUtility.getNextActionDate(calendarCodes, incoStartDate, businessDate
										 , typeFrequency, frequencyInfo, frequencyUnit1, frequencyUnit2,null
										 , holidayFlag, businessDate, callMode);
				}
				
				Date dateArray[] = new Date[3];

				
				dateArray[0] = nextActionDates[0];//Date_accrual
				dateArray[1] = nextActionDates[1];//Adjusted Accrual
				dateArray[2] = AccrualStartDate;// Accrual date start
				accrualDatesCache.put(codEntity,dateArray);
				if (logger.isDebugEnabled()) {
                    logger.debug(" KEY:["+codEntity+"] & VALUE:["+dateArray[0]+", "+dateArray[1]+dateArray[2]+"] added ");                                 
                }
			}
		}
		catch (Exception e){
			throw new LMSException("LMS01000","Error occurred while calculating next cycle Accrual dates for Process Id :- "+ processId );
		}
		finally {
			LMSConnectionUtility.close(rst);
			LMSConnectionUtility.close(pstmt);
		}
		if (logger.isDebugEnabled())
			logger.debug("Leaving incoBVTcalculateNextCycleAccrualDates");
	}
	
	/**
	 * @param conn
	 * @param processId
	 * @param businessDate
	 * @throws LMSException
	 * 
	 * This method calculates posting cycle dates for BVT
	 * 
	 */
	 // SA_POSTING_BVT_CHANGE 
	public void sABVTcalculateNextCyclePostingDates (Connection conn, long processId, Date businessDate, String cycleMode) throws LMSException {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		//String callMode = "E";
		String callMode = "M";//SA_BVT_POSTING_CYCLE
		GregorianCalendar gDate = new GregorianCalendar();
		/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//		GregorianCalendar lastPostDate = new GregorianCalendar();
		/** ****** FindBugs Fix End ***********  on 14DEC09*/
		java.util.Date incDateUtil = null;
		java.util.Date postingStartDate = null;
		java.util.Date firstPostingDate = null;
		String pstngCycleChng = "N";
		ArrayList calendarCodes = null;
		Date lastCalcPostingDate = null;
		try {
		
		// SA_POSTING_BVT_CHANGE Start
			String calculateBVTNextPostingCycle = null;
			if(cycleMode.equalsIgnoreCase("NORMAL"))
			{ logger.debug("Query normal");
				calculateBVTNextPostingCycle = (String)interestCalculatorSql.get("CalculateBVTNextPostingCycleNormal"); // new
			}
			else
			{	
				calculateBVTNextPostingCycle = (String)interestCalculatorSql.get("CalculateBVTNextPostingCycle"); /// old query
			}
			// SA_POSTING_BVT_CHANGE end
			pstmt = conn.prepareStatement(calculateBVTNextPostingCycle);
			pstmt.setLong(1,processId);
			pstmt.setString(2,ExecutionConstants.ACCOUNT);
			pstmt.setString(3,ExecutionConstants.ACTIVE);
			pstmt.setString(4,ExecutionConstants.ACCOUNT);
			
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("Query for fetching Accrual information for BVT is "+ calculateBVTNextPostingCycle);
			
			rst = pstmt.executeQuery();
			//SUP_IS1587 Starts //ENH_IS1913 starts
			String flag_bvt_cycle = ExecutionConstants.FLAG_BVT_CYCLE;
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("flag_bvt_cycle:::"+flag_bvt_cycle);
	        //SUP_IS1587 Etarts //ENH_IS1913 ends
			while (rst.next()) {
				String calendarCode = rst.getString(1);
				Date accountStartDate = rst.getDate(2);
				String frequency = rst.getString(3);
				Date lastPostingDate = rst.getDate(4);
				String codEntity = rst.getString(5);
				String hostCode = rst.getString(6);
				int typeFrequency = rst.getInt(7);
				int frequencyInfo = rst.getInt(8);
				int frequencyUnit1 = rst.getInt(9);
				int frequencyUnit2 = rst.getInt(10);
				String holidayFlag = rst.getString(11);
				int nbr_settle_days = rst.getInt(12);
				int nbr_delay_days_cr = rst.getInt(13);
				int nbr_delay_days_dr = rst.getInt(14);
				String Flg_samevaldat = rst.getString(15);
				String Cod_postcalendar = rst.getString(16);
				firstPostingDate = rst.getDate(17);
				pstngCycleChng = rst.getString(18);
				//SA_BVT_POSTING_CYCLE STARTS
				Date lastBvtPostingDate = businessDate;
				Date lastBvtCalcPostingDate = businessDate;
				//SA_BVT_POSTING_CYCLE ENDS
				
				if(lastPostingDate != null)
				{
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("Last Posting date is : "+lastPostingDate);
					gDate.setTime(lastPostingDate);
					gDate.add(Calendar.DATE,1);
					postingStartDate=gDate.getTime();
					lastCalcPostingDate = lastPostingDate;
				}
				else
				{
					postingStartDate = accountStartDate;
					lastCalcPostingDate = accountStartDate;
				}
				if(logger.isInfoEnabled()){
					logger.info(" Adding information of account ["+codEntity+"] as [calendarCode] = ["+calendarCode+"]," +
							" [poolStartDate] = ["+accountStartDate+"], [frequency] = ["+frequency+"], [lastPostingDate] = ["+lastPostingDate+"]" +
							", [lastCalcPostingDate] = ["+lastCalcPostingDate+"], [hostCode] = ["+hostCode+"]+" 
							+"{ Frequency["+typeFrequency+"], FrequencyInfo["+frequencyInfo+"], "
							+"FrequencyUnit1["+frequencyUnit1+"], FrequencyUnit2["+frequencyUnit2+"], "
							+"HolidayFlag["+holidayFlag+"]nbr_settle_days["+nbr_settle_days+"], nbr_delay_days_cr["+nbr_delay_days_cr
							+"],nbr_delay_days_dr["+nbr_delay_days_dr+"],Cod_postcalendar["+Cod_postcalendar
							+"],[PostingStartDate]=["+postingStartDate+"]firstPostingDate["+firstPostingDate+"]pstngCycleChng[" + pstngCycleChng + "]");//ENH_IS686 ends
				}
				calendarCodes = new ArrayList();
				calendarCodes.add(calendarCode);
				
				//if it is the firstCycle ie lastPostingDate == null || lastCalcPostingDate == null
				//|| if PSTCYCLE_CHNG is Y..ie if posting cycle has changed then 
				//donot call calendar service for the above cases.
				//Use DAT_FIRST_POSTING as the Posting Date
				//Add settlementDays to get the settlementDate, crDelayDays to get the crDelaydate
				//drDelayDays to get the drDelayDate 
				
				if(lastPostingDate == null || "Y".equalsIgnoreCase(pstngCycleChng)){
					callMode = "M";				       				    
				}
				java.util.Date[] nextActionDates = null;
				if(callMode.equals("E")){
					nextActionDates = 
						//ExecutionUtility.getNextActionDate(calendarCodes, lastPostingDate, lastCalcPostingDate
						ExecutionUtility.getNextActionDate(calendarCodes, lastBvtPostingDate, lastBvtCalcPostingDate//SA_BVT_POSTING_CYCLE
								, typeFrequency, frequencyInfo, frequencyUnit1, frequencyUnit2,Cod_postcalendar
								, holidayFlag, businessDate, callMode);//ENH_IS686 changes made by satish for Posting Calendar added an argument Cod_postcalendar for Posting Calendar
					java.util.Date tmp_date = nextActionDates[0];
					nextActionDates = null;
					nextActionDates = new java.util.Date[8];
					//set date posting
					nextActionDates[0] = tmp_date;
					tmp_date = null;
					//set date adjustment posting
					nextActionDates[1] = nextActionDates[0];
					/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//					logger.debug("tmp date is : ["+tmp_date+"]");
					/** ****** FindBugs Fix End ***********  on 14DEC09*/
				}
				else{
					/* 
					 * if this is the firstCycle or cycle has changed consider DAT_FIRST_POSTING AS PostingDate
					 * Add settlementDays, creditDelay Days and debitDelay days to get the nextSettlementDate/crDelayDate and DrDelayDate
					 * commented the line below as no need to call calendarService 
					 */
					nextActionDates = new java.util.Date[8];
					//set date posting
					nextActionDates[0] = firstPostingDate;
					//set date adjustment posting
					nextActionDates[1] = firstPostingDate;
					
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("Date posting is : ["+nextActionDates[0]+"]");
					
					nextActionDates = 
						//ExecutionUtility.getNextPostingCycleDate(conn, frequency, firstPostingDate, firstPostingDate, Cod_postcalendar, businessDate, callMode);//SA_BVT_POSTING_CYCLE
						ExecutionUtility.getNextPostingCycleDate(conn, frequency, lastBvtPostingDate, lastBvtCalcPostingDate, Cod_postcalendar, businessDate, callMode);
					
					java.util.Date tmp_date = nextActionDates[0];
					nextActionDates[1] = nextActionDates[0];
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("changed tmp date is : ["+tmp_date+"]");
					//Changes Done For BVT Ends
					nextActionDates = new java.util.Date[8];
					nextActionDates[0] = tmp_date; 
					nextActionDates[1] = nextActionDates[0];
					//Changes Done For BVT Ends
				}
				//ENH_IS1297 Starts...External BVT Changes
				while (nextActionDates[0].compareTo(businessDate)<0) {
					nextActionDates = ExecutionUtility.getNextActionDate(calendarCodes, nextActionDates[0], nextActionDates[1] 
					, typeFrequency, frequencyInfo, frequencyUnit1, frequencyUnit2,Cod_postcalendar
					, holidayFlag, businessDate, "E");
					nextActionDates[1] = nextActionDates[0];
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("dat_posting is : "+nextActionDates[0]);
					java.util.Date tmp_date = nextActionDates[0];
					nextActionDates = null;
					nextActionDates = new java.util.Date[8];
					//set date posting
					nextActionDates[0] = tmp_date;
					tmp_date = null;
					//set date adjustment posting
					nextActionDates[1] = nextActionDates[0];
					/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//					logger.debug("tmp date is : ["+tmp_date+"]");
					/** ****** FindBugs Fix End ***********  on 14DEC09*/
				}
				//ENH_IS1297 Ends
				GregorianCalendar temp = new GregorianCalendar();
				temp.setTime(nextActionDates[0]);
				temp.add(Calendar.DATE, nbr_settle_days);
				//set date post settlement
				nextActionDates[2] = temp.getTime();
				temp = null;
				//set date settlement Cr 
				nextActionDates[3] = 
					ExecutionUtility.getAnotherWorkingDay((String)calendarCodes.get(0), nextActionDates[0], 
							holidayFlag.charAt(0), 0, 0, nbr_settle_days + nbr_delay_days_cr);
				//set date settlement Dr
				nextActionDates[4] = 
					ExecutionUtility.getAnotherWorkingDay((String)calendarCodes.get(0), nextActionDates[0], 
							holidayFlag.charAt(0), 0, 0, nbr_settle_days + nbr_delay_days_dr);
				//set posting start date
				nextActionDates[5] = postingStartDate;
				
				if(Flg_samevaldat.equalsIgnoreCase("Y"))
				{
					//set  dat value Cr = date settlement Cr 
					nextActionDates[6]=nextActionDates[3];
					//set  dat value Dr = date settlement Dr
					nextActionDates[7]=nextActionDates[4];
				}
				else
				{
					
					gDate = new GregorianCalendar();
					gDate.setTime(nextActionDates[0]);
					gDate.add(Calendar.DATE,1);
					incDateUtil =  gDate.getTime();
					java.sql.Date valueDate = new java.sql.Date(incDateUtil.getTime());
					//set  dat value Cr = date posting + 1
					
					nextActionDates[6] = valueDate;
					//set  dat value Dr = date posting + 1
					nextActionDates[7] = valueDate;
					
				}
				
				if (logger.isDebugEnabled()) {
					logger.debug(" KEY:["+codEntity+"] & VALUE:["+nextActionDates[0]+", "+nextActionDates[1]+","+nextActionDates[2]+","+nextActionDates[3]+" ,"+nextActionDates[4]+","+nextActionDates[5]+","+nextActionDates[6]+","+nextActionDates[7]+"] processed");                                 
				}
				postingDatesCache.put(codEntity, nextActionDates);				
			}
			//SUP_IS1587 Start //ENH_IS1913 starts
			//if(flag_bvt_cycle.equalsIgnoreCase("Y")){
				 logger.debug("Before Callint the sABVTcalculateNextCyclePostingDatesCurr function flag_bvt_cycle:::"+flag_bvt_cycle);
				sABVTcalculateNextCyclePostingDatesCurr(conn,processId,businessDate);
			//}
			//SUP_IS1587 End //ENH_IS1913 ends
		}
		catch(Exception e) {
			throw new LMSException ("LMS01000","Error occurred while calculating next cycle Accrual dates for Process Id :- ",e);
		}
		finally {
			LMSConnectionUtility.close(rst);
			LMSConnectionUtility.close(pstmt);
		}
	}
	//SUP_IS1587 Starts //ENH_IS1913 starts
	private void sABVTcalculateNextCyclePostingDatesCurr(Connection conn, long processId, Date businessDate) throws LMSException {
		
		logger.debug("Entering in sABVTcalculateNextCyclePostingDatesCurr");
		
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		String callMode = "E";
		GregorianCalendar gDate = new GregorianCalendar();
		/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//		GregorianCalendar lastPostDate = new GregorianCalendar();
		/** ****** FindBugs Fix End ***********  on 14DEC09*/
		java.util.Date incDateUtil = null;
		java.util.Date postingStartDate = null;
		java.util.Date firstPostingDate = null;
		String pstngCycleChng = "N";
		ArrayList calendarCodes = null;
		Date lastCalcPostingDate = null;
		try {
			String CalculateBVTNextPostingCurrCycle = (String)interestCalculatorSql.get("CalculateBVTNextPostingCurrCycle");
			pstmt = conn.prepareStatement(CalculateBVTNextPostingCurrCycle);
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("ProcessId -->"+processId);
			pstmt.setLong(1,processId);
			pstmt.setString(2,ExecutionConstants.ACCOUNT);
			pstmt.setString(3,ExecutionConstants.ACTIVE);
			pstmt.setString(4,ExecutionConstants.ACCOUNT);
			
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("Query for fetching information for BVT is "+ CalculateBVTNextPostingCurrCycle);
			
			rst = pstmt.executeQuery();
			while (rst.next()) {
				String calendarCode = rst.getString(1);
				Date accountStartDate = rst.getDate(2);
				String frequency = rst.getString(3);
				Date lastPostingDate = rst.getDate(4);
				String codEntity = rst.getString(5);
				String hostCode = rst.getString(6);
				int typeFrequency = rst.getInt(7);
				int frequencyInfo = rst.getInt(8);
				int frequencyUnit1 = rst.getInt(9);
				int frequencyUnit2 = rst.getInt(10);
				String holidayFlag = rst.getString(11);
				int nbr_settle_days = rst.getInt(12);
				int nbr_delay_days_cr = rst.getInt(13);
				int nbr_delay_days_dr = rst.getInt(14);
				String Flg_samevaldat = rst.getString(15);
				String Cod_postcalendar = rst.getString(16);
				firstPostingDate = rst.getDate(17);
				pstngCycleChng = rst.getString(18);
				
				if(lastPostingDate != null)
				{
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("Last Posting date is : "+lastPostingDate);
					gDate.setTime(lastPostingDate);
					gDate.add(Calendar.DATE,1);
					postingStartDate=gDate.getTime();
					lastCalcPostingDate = lastPostingDate;
				}
				else
				{
					postingStartDate = accountStartDate;
					lastCalcPostingDate = accountStartDate;
				}
				if(logger.isInfoEnabled()){
					logger.info(" Adding information of account ["+codEntity+"] as [calendarCode] = ["+calendarCode+"]," +
							" [poolStartDate] = ["+accountStartDate+"], [frequency] = ["+frequency+"], [lastPostingDate] = ["+lastPostingDate+"]" +
							", [lastCalcPostingDate] = ["+lastCalcPostingDate+"], [hostCode] = ["+hostCode+"]+" 
							+"{ Frequency["+typeFrequency+"], FrequencyInfo["+frequencyInfo+"], "
							+"FrequencyUnit1["+frequencyUnit1+"], FrequencyUnit2["+frequencyUnit2+"], "
							+"HolidayFlag["+holidayFlag+"]nbr_settle_days["+nbr_settle_days+"], nbr_delay_days_cr["+nbr_delay_days_cr
							+"],nbr_delay_days_dr["+nbr_delay_days_dr+"],Cod_postcalendar["+Cod_postcalendar
							+"],[PostingStartDate]=["+postingStartDate+"]firstPostingDate["+firstPostingDate+"]pstngCycleChng[" + pstngCycleChng + "]");//ENH_IS686 ends
				}
				calendarCodes = new ArrayList();
				calendarCodes.add(calendarCode);
				
				//if it is the firstCycle ie lastPostingDate == null || lastCalcPostingDate == null
				//|| if PSTCYCLE_CHNG is Y..ie if posting cycle has changed then 
				//donot call calendar service for the above cases.
				//Use DAT_FIRST_POSTING as the Posting Date
				//Add settlementDays to get the settlementDate, crDelayDays to get the crDelaydate
				//drDelayDays to get the drDelayDate 
				
				if(lastPostingDate == null || "Y".equalsIgnoreCase(pstngCycleChng)){
					callMode = "M";				       				    
				}
				java.util.Date[] nextActionDates = null;
				if(callMode.equals("E")){
					nextActionDates = 
						ExecutionUtility.getNextActionDate(calendarCodes, lastPostingDate, lastCalcPostingDate
								, typeFrequency, frequencyInfo, frequencyUnit1, frequencyUnit2,Cod_postcalendar
								, holidayFlag, businessDate, callMode);//ENH_IS686 changes made by satish for Posting Calendar added an argument Cod_postcalendar for Posting Calendar
					java.util.Date tmp_date = nextActionDates[0];
					nextActionDates = null;
					nextActionDates = new java.util.Date[8];
					//set date posting
					nextActionDates[0] = tmp_date;
					tmp_date = null;
					//set date adjustment posting
					nextActionDates[1] = nextActionDates[0];
					/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//					logger.debug("tmp date is : ["+tmp_date+"]");
					/** ****** FindBugs Fix End ***********  on 14DEC09*/
				}
				else{
					/* 
					 * if this is the firstCycle or cycle has changed consider DAT_FIRST_POSTING AS PostingDate
					 * Add settlementDays, creditDelay Days and debitDelay days to get the nextSettlementDate/crDelayDate and DrDelayDate
					 * commented the line below as no need to call calendarService 
					 */
					nextActionDates = new java.util.Date[8];
					//set date posting
					nextActionDates[0] = firstPostingDate;
					//set date adjustment posting
					nextActionDates[1] = firstPostingDate;
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("Date posting is : ["+nextActionDates[0]+"]");
					
					nextActionDates = 
						ExecutionUtility.getNextPostingCycleDate(conn, frequency, firstPostingDate, firstPostingDate, Cod_postcalendar, businessDate, callMode);
					
					java.util.Date tmp_date = nextActionDates[0];
					nextActionDates[1] = nextActionDates[0];
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("changed tmp date is : ["+tmp_date+"]");
					//Changes Done For BVT Ends
					nextActionDates = new java.util.Date[8];
					nextActionDates[0] = tmp_date; 
					nextActionDates[1] = nextActionDates[0];
					//Changes Done For BVT Ends
				}
				//ENH_IS1297 Starts...External BVT Changes
				while (nextActionDates[0].compareTo(businessDate)<0) {
					nextActionDates = ExecutionUtility.getNextActionDate(calendarCodes, nextActionDates[0], nextActionDates[1] 
					, typeFrequency, frequencyInfo, frequencyUnit1, frequencyUnit2,Cod_postcalendar
					, holidayFlag, businessDate, "E");
					nextActionDates[1] = nextActionDates[0];
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("dat_posting is : "+nextActionDates[0]);
					java.util.Date tmp_date = nextActionDates[0];
					nextActionDates = null;
					nextActionDates = new java.util.Date[8];
					//set date posting
					nextActionDates[0] = tmp_date;
					tmp_date = null;
					//set date adjustment posting
					nextActionDates[1] = nextActionDates[0];
					/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//					logger.debug("tmp date is : ["+tmp_date+"]");
					/** ****** FindBugs Fix End ***********  on 14DEC09*/
				}
				//ENH_IS1297 Ends
				GregorianCalendar temp = new GregorianCalendar();
				temp.setTime(nextActionDates[0]);
				temp.add(Calendar.DATE, nbr_settle_days);
				//set date post settlement
				nextActionDates[2] = temp.getTime();
				temp = null;
				//set date settlement Cr 
				nextActionDates[3] = 
					ExecutionUtility.getAnotherWorkingDay((String)calendarCodes.get(0), nextActionDates[0], 
							holidayFlag.charAt(0), 0, 0, nbr_settle_days + nbr_delay_days_cr);
				//set date settlement Dr
				nextActionDates[4] = 
					ExecutionUtility.getAnotherWorkingDay((String)calendarCodes.get(0), nextActionDates[0], 
							holidayFlag.charAt(0), 0, 0, nbr_settle_days + nbr_delay_days_dr);
				//set posting start date
				nextActionDates[5] = postingStartDate;
				
				if(Flg_samevaldat.equalsIgnoreCase("Y"))
				{
					//set  dat value Cr = date settlement Cr 
					nextActionDates[6]=nextActionDates[3];
					//set  dat value Dr = date settlement Dr
					nextActionDates[7]=nextActionDates[4];
				}
				else
				{
					
					gDate = new GregorianCalendar();
					gDate.setTime(nextActionDates[0]);
					gDate.add(Calendar.DATE,1);
					incDateUtil =  gDate.getTime();
					java.sql.Date valueDate = new java.sql.Date(incDateUtil.getTime());
					//set  dat value Cr = date posting + 1
					
					nextActionDates[6] = valueDate;
					//set  dat value Dr = date posting + 1
					nextActionDates[7] = valueDate;
					
				}
				
				if (logger.isDebugEnabled()) {
					logger.debug(" KEY:["+codEntity+"] & VALUE:["+nextActionDates[0]+", "+nextActionDates[1]+","+nextActionDates[2]+","+nextActionDates[3]+" ,"+nextActionDates[4]+","+nextActionDates[5]+","+nextActionDates[6]+","+nextActionDates[7]+"] processed");                                 
				}
				postingDatesCacheCurr.put(codEntity, nextActionDates);				
			}
		}
		catch(Exception e) {
			throw new LMSException ("LMS01000","Error occurred while calculating next cycle Accrual dates for Process Id :- ",e);
		}
		finally {
			LMSConnectionUtility.close(rst);
			LMSConnectionUtility.close(pstmt);
		}
	}
	//SUP_IS1587 END //ENH_IS1913 ends
	
	/**
	 * @param conn
	 * @param processId
	 * @param businessDate
	 * @throws LMSException
	 * 
	 * This method calculates posting cycle dates for BVT for Intercompany
	 * 
	 */
	public void incoBVTcalculateNextCyclePostingDates (Connection conn, long processId, Date businessDate,Date startDate) throws LMSException {
		if (logger.isDebugEnabled())
			logger.debug("Entering incoBVTcalculateNextCyclePostingDates");
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		String callMode = "E";
		GregorianCalendar gDate = new GregorianCalendar();
		/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//		GregorianCalendar lastPostDate = new GregorianCalendar();
		/** ****** FindBugs Fix End ***********  on 14DEC09*/
		java.util.Date incDateUtil = null;
		java.util.Date postingStartDate = null;
		java.util.Date firstPostingDate = null;
		String pstngCycleChng = "N";
		ArrayList calendarCodes = null;
		Date lastCalcPostingDate = null;
		String flag_bvt_cycle = ExecutionConstants.FLAG_BVT_CYCLE;
		String calculateInCoBVTNextPostingCycle = null;
		try {
		//SUP_ISS_III_7305 start
			// SUP_ISS_V_7481 UnComment Start
			if(flag_bvt_cycle.equalsIgnoreCase(ExecutionConstants.YES))
				calculateInCoBVTNextPostingCycle = (String)interestCalculatorSql.get("calculateInCoBVTNextPostingCycle");
			else
				//SUP_ISS_V_7481 UnComment End
			//SUP_ISS_III_7305 end
				calculateInCoBVTNextPostingCycle = (String)interestCalculatorSql.get("calculateInCoBVTNextPostingCycleNonBVT");
			pstmt = conn.prepareStatement(calculateInCoBVTNextPostingCycle);
			pstmt.setLong(1,processId);
			pstmt.setString(2,ExecutionConstants.INCOLOAN);
			pstmt.setDate(3,LMSUtility.truncSqlDate(new java.sql.Date(businessDate.getTime())));
			//SA_BVT_POSTING_CYCLE STARTS
//Retro_Mashreq
			//ANZ_BVT_FIX starts
			/*if(flag_bvt_cycle.equalsIgnoreCase(ExecutionConstants.YES))//SIT_ISSUE_BVT
			{*/
				pstmt.setString(4,"CYCLE");
				pstmt.setDate(5,LMSUtility.truncSqlDate(new java.sql.Date(businessDate.getTime())));
			//}
				//ANZ_BVT_FIX ends
			//SA_BVT_POSTING_CYCLE ENDS
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("Query for fetching Posting information for BVT is "+ calculateInCoBVTNextPostingCycle);
			
			rst = pstmt.executeQuery();
			while (rst.next()) {
				String calendarCode = rst.getString(1);
				Date incoStartDate = rst.getDate(2);
				String frequency = rst.getString(3);
				Date lastPostingDate = rst.getDate(4);
				String codEntity = rst.getString(5);
				String hostCode = rst.getString(6);
				int typeFrequency = rst.getInt(7);
				int frequencyInfo = rst.getInt(8);
				int frequencyUnit1 = rst.getInt(9);
				int frequencyUnit2 = rst.getInt(10);
				String holidayFlag = rst.getString(11);
				int nbr_settle_days = rst.getInt(12);
				int nbr_delay_days_cr = rst.getInt(13);
				int nbr_delay_days_dr = rst.getInt(14);
				String Flg_samevaldat = rst.getString(15);
				String Cod_postcalendar = rst.getString(16);
				firstPostingDate = rst.getDate(17);
				pstngCycleChng = rst.getString(18);
				
				if(lastPostingDate != null)
				{
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("Last Posting date is : "+lastPostingDate);
					gDate.setTime(lastPostingDate);
					gDate.add(Calendar.DATE,1);
					postingStartDate=gDate.getTime();
					lastCalcPostingDate = lastPostingDate;
				}
				else
				{
				   //RBS_236 starts
					//postingStartDate = incoStartDate;
					
					/**
					 * Issue was for bvt transactions which are being posted for
					 * the first time. Last posting date is null as a result it
					 * was taking businessdate .As a result it was not picked
					 * for updating settlement date.It is initialized to incostart date
					 **/
					
					//SUP_ISS_V_7481 Start
					// Instead of picking ICL Start Date, pick BVT Cycle Start Date
					 
					postingStartDate = startDate;
					//postingStartDate = incoStartDate;
					//SUP_ISS_V_7481 End 
				//	postingStartDate = businessDate;
					//RBS_236 ends
					lastCalcPostingDate = incoStartDate;
				}
				if(logger.isInfoEnabled()){
					logger.info(" Adding information of Intercompany Loan ["+codEntity+"] as [calendarCode] = ["+calendarCode+"]," +
							" [incoStartDate] = ["+incoStartDate+"], [frequency] = ["+frequency+"], [lastPostingDate] = ["+lastPostingDate+"]" +
							", [lastCalcPostingDate] = ["+lastCalcPostingDate+"], [hostCode] = ["+hostCode+"]+" 
							+"{ Frequency["+typeFrequency+"], FrequencyInfo["+frequencyInfo+"], "
							+"FrequencyUnit1["+frequencyUnit1+"], FrequencyUnit2["+frequencyUnit2+"], "
							+"HolidayFlag["+holidayFlag+"]nbr_settle_days["+nbr_settle_days+"], nbr_delay_days_cr["+nbr_delay_days_cr
							+"],nbr_delay_days_dr["+nbr_delay_days_dr+"],Cod_postcalendar["+Cod_postcalendar
							+"],[PostingStartDate]=["+postingStartDate+"]firstPostingDate["+firstPostingDate+"]pstngCycleChng[" + pstngCycleChng + "]");//ENH_IS686 ends
				}
				calendarCodes = new ArrayList();
				calendarCodes.add(calendarCode);
				
				//if it is the firstCycle ie lastPostingDate == null || lastCalcPostingDate == null
				//|| if PSTCYCLE_CHNG is Y..ie if posting cycle has changed then 
				//donot call calendar service for the above cases.
				//Use DAT_FIRST_POSTING as the Posting Date
				//Add settlementDays to get the settlementDate, crDelayDays to get the crDelaydate
				//drDelayDays to get the drDelayDate 
				
				if(lastPostingDate == null || "Y".equalsIgnoreCase(pstngCycleChng)){
					callMode = "M";				       				    
				}
				java.util.Date[] nextActionDates = null;
				if(callMode.equals("E")){
					if (frequency!= null) {
						nextActionDates = 
							ExecutionUtility.getNextActionDate(calendarCodes, lastPostingDate, lastCalcPostingDate
									, typeFrequency, frequencyInfo, frequencyUnit1, frequencyUnit2,Cod_postcalendar
									, holidayFlag, businessDate, callMode);//ENH_IS686 changes made by satish for Posting Calendar added an argument Cod_postcalendar for Posting Calendar
						java.util.Date tmp_date = nextActionDates[0];
						nextActionDates = null;
						nextActionDates = new java.util.Date[8];
						//set date posting
						nextActionDates[0] = tmp_date;
						tmp_date = null;
						//set date adjustment posting
						nextActionDates[1] = nextActionDates[0];
					}
					else {
						nextActionDates = new java.util.Date[8];
						//set date posting
						nextActionDates[0] = firstPostingDate;
						//set date adjustment posting
						nextActionDates[1] = nextActionDates[0];
					}
				}
				else{
					/* 
					 * if this is the firstCycle or cycle has changed consider DAT_FIRST_POSTING AS PostingDate
					 * Add settlementDays, creditDelay Days and debitDelay days to get the nextSettlementDate/crDelayDate and DrDelayDate
					 * commented the line below as no need to call calendarService 
					 */
					nextActionDates = new java.util.Date[8];
					//set date posting
					nextActionDates[0] = firstPostingDate;
					//set date adjustment posting
					nextActionDates[1] = firstPostingDate;	
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("Date posting is : ["+nextActionDates[0]+"]");
					
					if(frequency != null)
					nextActionDates = ExecutionUtility.getNextPostingCycleDate(conn, frequency, firstPostingDate, firstPostingDate, Cod_postcalendar, businessDate, callMode);
					
					java.util.Date tmp_date = nextActionDates[0];
					nextActionDates[1] = nextActionDates[0];
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("changed tmp date is : ["+tmp_date+"]");
					//Changes Done For BVT Ends
					nextActionDates = new java.util.Date[8];
					nextActionDates[0] = tmp_date; 
					nextActionDates[1] = nextActionDates[0];
					//Changes Done For BVT Ends


				}
				//ENH_IS1297 Starts...External BVT Changes
				while (nextActionDates[0].compareTo(businessDate)<0) {
					nextActionDates = ExecutionUtility.getNextActionDate(calendarCodes, nextActionDates[0], nextActionDates[1] 
					, typeFrequency, frequencyInfo, frequencyUnit1, frequencyUnit2,Cod_postcalendar
					, holidayFlag, businessDate, "E");
					nextActionDates[1] = nextActionDates[0];
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("dat_posting is : "+nextActionDates[0]);
					java.util.Date tmp_date = nextActionDates[0];
					nextActionDates = null;
					nextActionDates = new java.util.Date[8];
					//set date posting
					nextActionDates[0] = tmp_date;
					tmp_date = null;
					//set date adjustment posting
					nextActionDates[1] = nextActionDates[0];
					/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//					logger.debug("tmp date is : ["+tmp_date+"]");
					/** ****** FindBugs Fix End ***********  on 14DEC09*/
				}
				//ENH_IS1297 Ends

				GregorianCalendar temp = new GregorianCalendar();
				temp.setTime(nextActionDates[0]);
				temp.add(Calendar.DATE, nbr_settle_days);
				//set date post settlement
				nextActionDates[2] = temp.getTime();
				temp = null;
				//set date settlement Cr 
				nextActionDates[3] = 
					ExecutionUtility.getAnotherWorkingDay((String)calendarCodes.get(0), nextActionDates[0], 
							holidayFlag.charAt(0), 0, 0, nbr_settle_days + nbr_delay_days_cr);
				//set date settlement Dr
				nextActionDates[4] = 
					ExecutionUtility.getAnotherWorkingDay((String)calendarCodes.get(0), nextActionDates[0], 
							holidayFlag.charAt(0), 0, 0, nbr_settle_days + nbr_delay_days_dr);
				//set posting start date
				nextActionDates[5] = postingStartDate;
				
				if(Flg_samevaldat.equalsIgnoreCase("Y"))
				{
					//set  dat value Cr = date settlement Cr 
					nextActionDates[6]=nextActionDates[3];
					//set  dat value Dr = date settlement Dr
					nextActionDates[7]=nextActionDates[4];
				}
				else
				{
					
					gDate = new GregorianCalendar();
					gDate.setTime(nextActionDates[0]);
					gDate.add(Calendar.DATE,1);
					incDateUtil =  gDate.getTime();
					java.sql.Date valueDate = new java.sql.Date(incDateUtil.getTime());
					//set  dat value Cr = date posting + 1
					
					nextActionDates[6] = valueDate;
					//set  dat value Dr = date posting + 1
					nextActionDates[7] = valueDate;
					
				}
				
				if (logger.isDebugEnabled()) {
					logger.debug(" KEY:["+codEntity+"] & VALUE:["+nextActionDates[0]+", "+nextActionDates[1]+","+nextActionDates[2]+","+nextActionDates[3]+" ,"+nextActionDates[4]+","+nextActionDates[5]+","+nextActionDates[6]+","+nextActionDates[7]+"] processed");                                 
				}
				postingDatesCache.put(codEntity, nextActionDates);				
			}
			//SUP_IS1587 Start
			//SUP_ISS_III_7305 start
			/*if(flag_bvt_cycle.equalsIgnoreCase("Y")){
				this.incoBVTcalculateNextCyclePostingDatesCurr(conn,processId,businessDate,startDate);
			}*/
		//SUP_ISS_III_7305 end
			//SUP_IS1587 End
		}
		catch(Exception e) {
			throw new LMSException ("LMS01000","Error occurred while calculating next cycle Posting dates for Process Id :- ",e);
		}
		finally {
			LMSConnectionUtility.close(rst);
			LMSConnectionUtility.close(pstmt);
		}
		if (logger.isDebugEnabled())
			logger.debug("Entering incoBVTcalculateNextCyclePostingDates");
	}
	//SUP_ISS_III_7305 start
	/**
	 * @param conn
	 * @param processId
	 * @param businessDate
	 * @throws LMSException
	 * 
	 * This method calculates posting cycle dates for BVT for Intercompany
	 * 
	 */
	public void incoBVTcalculateNextCyclePostingDatesForBVT (Connection conn, long processId, Date businessDate,Date startDate) throws LMSException {
		if (logger.isDebugEnabled())
			logger.debug("Entering incoBVTcalculateNextCyclePostingDates");
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		String callMode = "E";
		GregorianCalendar gDate = new GregorianCalendar();
		/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//		GregorianCalendar lastPostDate = new GregorianCalendar();
		/** ****** FindBugs Fix End ***********  on 14DEC09*/
		java.util.Date incDateUtil = null;
		java.util.Date postingStartDate = null;
		java.util.Date firstPostingDate = null;
		String pstngCycleChng = "N";
		ArrayList calendarCodes = null;
		Date lastCalcPostingDate = null;
		String flag_bvt_cycle = ExecutionConstants.FLAG_BVT_CYCLE;
		String calculateInCoBVTNextPostingCycle = null;
		try {
			calculateInCoBVTNextPostingCycle = (String)interestCalculatorSql.get("calculateInCoBVTNextPostingCycle");
			pstmt = conn.prepareStatement(calculateInCoBVTNextPostingCycle);
			pstmt.setLong(1,processId);
			pstmt.setString(2,ExecutionConstants.INCOLOAN);
			pstmt.setDate(3,LMSUtility.truncSqlDate(new java.sql.Date(businessDate.getTime())));
			//SA_BVT_POSTING_CYCLE STARTS
//Retro_Mashreq
			//ANZ_BVT_FIX starts
			/*if(flag_bvt_cycle.equalsIgnoreCase(ExecutionConstants.YES))//SIT_ISSUE_BVT
			{*/
				pstmt.setString(4,"CYCLE");
				pstmt.setDate(5,LMSUtility.truncSqlDate(new java.sql.Date(businessDate.getTime())));
			//}
				//ANZ_BVT_FIX ends
			//SA_BVT_POSTING_CYCLE ENDS
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("Query for fetching Posting information for BVT is "+ calculateInCoBVTNextPostingCycle);
			
			rst = pstmt.executeQuery();
			while (rst.next()) {
				String calendarCode = rst.getString(1);
				Date incoStartDate = rst.getDate(2);
				String frequency = rst.getString(3);
				Date lastPostingDate = rst.getDate(4);
				String codEntity = rst.getString(5);
				String hostCode = rst.getString(6);
				int typeFrequency = rst.getInt(7);
				int frequencyInfo = rst.getInt(8);
				int frequencyUnit1 = rst.getInt(9);
				int frequencyUnit2 = rst.getInt(10);
				String holidayFlag = rst.getString(11);
				int nbr_settle_days = rst.getInt(12);
				int nbr_delay_days_cr = rst.getInt(13);
				int nbr_delay_days_dr = rst.getInt(14);
				String Flg_samevaldat = rst.getString(15);
				String Cod_postcalendar = rst.getString(16);
				firstPostingDate = rst.getDate(17);
				pstngCycleChng = rst.getString(18);
				Date lastBvtPostingDate = businessDate;
				Date lastBvtCalcPostingDate = businessDate;
				
				if(lastPostingDate != null)
				{
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("Last Posting date is : "+lastPostingDate);
					gDate.setTime(lastPostingDate);
					gDate.add(Calendar.DATE,1);
					postingStartDate=gDate.getTime();
					lastCalcPostingDate = lastPostingDate;
				}
				else
				{
				   //RBS_236 starts
					//postingStartDate = incoStartDate;
					
					/**
					 * Issue was for bvt transactions which are being posted for
					 * the first time. Last posting date is null as a result it
					 * was taking businessdate .As a result it was not picked
					 * for updating settlement date.It is initialized to incostart date
					 **/
					//postingStartDate = incoStartDate;
					postingStartDate = businessDate;
					//RBS_236 ends
					//lastCalcPostingDate = incoStartDate;
				}
				if(logger.isInfoEnabled()){
					logger.info(" Adding information of Intercompany Loan ["+codEntity+"] as [calendarCode] = ["+calendarCode+"]," +
							" [incoStartDate] = ["+incoStartDate+"], [frequency] = ["+frequency+"], [lastBvtPostingDate] = ["+lastBvtPostingDate+"]" +
							", [lastBvtCalcPostingDate] = ["+lastBvtCalcPostingDate+"], [hostCode] = ["+hostCode+"]+" 
							+"{ Frequency["+typeFrequency+"], FrequencyInfo["+frequencyInfo+"], "
							+"FrequencyUnit1["+frequencyUnit1+"], FrequencyUnit2["+frequencyUnit2+"], "
							+"HolidayFlag["+holidayFlag+"]nbr_settle_days["+nbr_settle_days+"], nbr_delay_days_cr["+nbr_delay_days_cr
							+"],nbr_delay_days_dr["+nbr_delay_days_dr+"],Cod_postcalendar["+Cod_postcalendar
							+"],[PostingStartDate]=["+postingStartDate+"]firstPostingDate["+firstPostingDate+"]pstngCycleChng[" + pstngCycleChng + "]");//ENH_IS686 ends
				}
				calendarCodes = new ArrayList();
				calendarCodes.add(calendarCode);
				
				//if it is the firstCycle ie lastPostingDate == null || lastCalcPostingDate == null
				//|| if PSTCYCLE_CHNG is Y..ie if posting cycle has changed then 
				//donot call calendar service for the above cases.
				//Use DAT_FIRST_POSTING as the Posting Date
				//Add settlementDays to get the settlementDate, crDelayDays to get the crDelaydate
				//drDelayDays to get the drDelayDate 
				
				/*if(lastPostingDate == null || "Y".equalsIgnoreCase(pstngCycleChng)){
					callMode = "M";				       				    
				}*/
				java.util.Date[] nextActionDates = null;
						nextActionDates = 
							ExecutionUtility.getNextActionDate(calendarCodes, lastBvtPostingDate, lastBvtCalcPostingDate
									, typeFrequency, frequencyInfo, frequencyUnit1, frequencyUnit2,Cod_postcalendar
									, holidayFlag, businessDate, callMode);//ENH_IS686 changes made by satish for Posting Calendar added an argument Cod_postcalendar for Posting Calendar
						java.util.Date tmp_date = nextActionDates[0];
						nextActionDates = null;
						nextActionDates = new java.util.Date[8];
						//set date posting
						nextActionDates[0] = tmp_date;
						tmp_date = null;
						//set date adjustment posting
						nextActionDates[1] = nextActionDates[0];
					
				//ENH_IS1297 Starts...External BVT Changes
				while (nextActionDates[0].compareTo(businessDate)<0) {
					nextActionDates = ExecutionUtility.getNextActionDate(calendarCodes, nextActionDates[0], nextActionDates[1] 
					, typeFrequency, frequencyInfo, frequencyUnit1, frequencyUnit2,Cod_postcalendar
					, holidayFlag, businessDate, "E");
					nextActionDates[1] = nextActionDates[0];
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("dat_posting is : "+nextActionDates[0]);
					 tmp_date = nextActionDates[0];
					nextActionDates = null;
					nextActionDates = new java.util.Date[8];
					//set date posting
					nextActionDates[0] = tmp_date;
					tmp_date = null;
					//set date adjustment posting
					nextActionDates[1] = nextActionDates[0];
					/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//					logger.debug("tmp date is : ["+tmp_date+"]");
					/** ****** FindBugs Fix End ***********  on 14DEC09*/
				}
				//ENH_IS1297 Ends

				GregorianCalendar temp = new GregorianCalendar();
				temp.setTime(nextActionDates[0]);
				temp.add(Calendar.DATE, nbr_settle_days);
				//set date post settlement
				nextActionDates[2] = temp.getTime();
				temp = null;
				//set date settlement Cr 
				nextActionDates[3] = 
					ExecutionUtility.getAnotherWorkingDay((String)calendarCodes.get(0), nextActionDates[0], 
							holidayFlag.charAt(0), 0, 0, nbr_settle_days + nbr_delay_days_cr);
				//set date settlement Dr
				nextActionDates[4] = 
					ExecutionUtility.getAnotherWorkingDay((String)calendarCodes.get(0), nextActionDates[0], 
							holidayFlag.charAt(0), 0, 0, nbr_settle_days + nbr_delay_days_dr);
				//set posting start date
				nextActionDates[5] = postingStartDate;
				
				if(Flg_samevaldat.equalsIgnoreCase("Y"))
				{
					//set  dat value Cr = date settlement Cr 
					nextActionDates[6]=nextActionDates[3];
					//set  dat value Dr = date settlement Dr
					nextActionDates[7]=nextActionDates[4];
				}
				else
				{
					
					gDate = new GregorianCalendar();
					gDate.setTime(nextActionDates[0]);
					gDate.add(Calendar.DATE,1);
					incDateUtil =  gDate.getTime();
					java.sql.Date valueDate = new java.sql.Date(incDateUtil.getTime());
					//set  dat value Cr = date posting + 1
					
					nextActionDates[6] = valueDate;
					//set  dat value Dr = date posting + 1
					nextActionDates[7] = valueDate;
					
				}
				
				if (logger.isDebugEnabled()) {
					logger.debug(" KEY:["+codEntity+"] & VALUE:["+nextActionDates[0]+", "+nextActionDates[1]+","+nextActionDates[2]+","+nextActionDates[3]+" ,"+nextActionDates[4]+","+nextActionDates[5]+","+nextActionDates[6]+","+nextActionDates[7]+"] processed");                                 
				}
				postingDatesCache.put(codEntity, nextActionDates);				
			}
			
				this.incoBVTcalculateNextCyclePostingDatesCurr(conn,processId,businessDate,startDate);
			
		
			//SUP_IS1587 End
		}
		catch(Exception e) {
			throw new LMSException ("LMS01000","Error occurred while calculating next cycle Posting dates for Process Id :- ",e);
		}
		finally {
			LMSConnectionUtility.close(rst);
			LMSConnectionUtility.close(pstmt);
		}
		if (logger.isDebugEnabled())
			logger.debug("Entering incoBVTcalculateNextCyclePostingDates");
	}
	//SUP_ISS_III_7305 end
	//SUP_IS1587 BVT Posting Adjustment Starts
	public void incoBVTcalculateNextCyclePostingDatesCurr  (Connection conn, long processId, Date businessDate,Date startDate) throws LMSException {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		String callMode = "E";
		GregorianCalendar gDate = new GregorianCalendar();
		/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//		GregorianCalendar lastPostDate = new GregorianCalendar();
		/** ****** FindBugs Fix End ***********  on 14DEC09*/
		java.util.Date incDateUtil = null;
		java.util.Date postingStartDate = null;
		java.util.Date firstPostingDate = null;
		String pstngCycleChng = "N";
		ArrayList calendarCodes = null;
		Date lastCalcPostingDate = null;
		try {
		//SUP_ISS_III_7305 start
			//String calculateInCoBVTNormalNextPostingCycle = (String)interestCalculatorSql.get("calculateInCoBVTNormalNextPostingCycle");calculateInCoBVTNextPostingCycleNonBVT
			String calculateInCoBVTNormalNextPostingCycle = (String)interestCalculatorSql.get("calculateInCoBVTNextPostingCycleNonBVT");
			pstmt = conn.prepareStatement(calculateInCoBVTNormalNextPostingCycle);
			/*pstmt.setLong(1,processId);
			pstmt.setString(2,ExecutionConstants.INCO);*/
			pstmt.setLong(1,processId);
			pstmt.setString(2,ExecutionConstants.INCOLOAN);
			pstmt.setDate(3,LMSUtility.truncSqlDate(new java.sql.Date(businessDate.getTime())));
			pstmt.setString(4,"CYCLE");
			pstmt.setDate(5,LMSUtility.truncSqlDate(new java.sql.Date(businessDate.getTime())));
				//SUP_ISS_III_7305 end
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("Query for fetching Posting information for Current posting cycle in BVT is "+ calculateInCoBVTNormalNextPostingCycle);
			
			rst = pstmt.executeQuery();
			while (rst.next()) {
				String calendarCode = rst.getString(1);
				Date incoStartDate = rst.getDate(2);
				String frequency = rst.getString(3);
				Date lastPostingDate = rst.getDate(4);
				String codEntity = rst.getString(5);
				String hostCode = rst.getString(6);
				int typeFrequency = rst.getInt(7);
				int frequencyInfo = rst.getInt(8);
				int frequencyUnit1 = rst.getInt(9);
				int frequencyUnit2 = rst.getInt(10);
				String holidayFlag = rst.getString(11);
				int nbr_settle_days = rst.getInt(12);
				int nbr_delay_days_cr = rst.getInt(13);
				int nbr_delay_days_dr = rst.getInt(14);
				String Flg_samevaldat = rst.getString(15);
				String Cod_postcalendar = rst.getString(16);
				firstPostingDate = rst.getDate(17);
				pstngCycleChng = rst.getString(18);
				
				if(lastPostingDate != null)
				{
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("Last Posting date is : "+lastPostingDate);
					gDate.setTime(lastPostingDate);
					gDate.add(Calendar.DATE,1);
					postingStartDate=gDate.getTime();
					lastCalcPostingDate = lastPostingDate;
				}
				else
				{
					//postingStartDate = incoStartDate;
					postingStartDate = startDate;
					lastCalcPostingDate = incoStartDate;
				}
				if(logger.isInfoEnabled()){
					logger.info(" Adding information of Intercompany Loan ["+codEntity+"] as [calendarCode] = ["+calendarCode+"]," +
							" [incoStartDate] = ["+incoStartDate+"], [frequency] = ["+frequency+"], [lastPostingDate] = ["+lastPostingDate+"]" +
							", [lastCalcPostingDate] = ["+lastCalcPostingDate+"], [hostCode] = ["+hostCode+"]+" 
							+"{ Frequency["+typeFrequency+"], FrequencyInfo["+frequencyInfo+"], "
							+"FrequencyUnit1["+frequencyUnit1+"], FrequencyUnit2["+frequencyUnit2+"], "
							+"HolidayFlag["+holidayFlag+"]nbr_settle_days["+nbr_settle_days+"], nbr_delay_days_cr["+nbr_delay_days_cr
							+"],nbr_delay_days_dr["+nbr_delay_days_dr+"],Cod_postcalendar["+Cod_postcalendar
							+"],[PostingStartDate]=["+postingStartDate+"]firstPostingDate["+firstPostingDate+"]pstngCycleChng[" + pstngCycleChng + "]");//ENH_IS686 ends
				}
				calendarCodes = new ArrayList();
				calendarCodes.add(calendarCode);
				
				//if it is the firstCycle ie lastPostingDate == null || lastCalcPostingDate == null
				//|| if PSTCYCLE_CHNG is Y..ie if posting cycle has changed then 
				//donot call calendar service for the above cases.
				//Use DAT_FIRST_POSTING as the Posting Date
				//Add settlementDays to get the settlementDate, crDelayDays to get the crDelaydate
				//drDelayDays to get the drDelayDate 
				
				if(lastPostingDate == null || "Y".equalsIgnoreCase(pstngCycleChng)){
					callMode = "M";				       				    
				}
				java.util.Date[] nextActionDates = null;
				if(callMode.equals("E")){
					nextActionDates = 
						ExecutionUtility.getNextActionDate(calendarCodes, lastPostingDate, lastCalcPostingDate
								, typeFrequency, frequencyInfo, frequencyUnit1, frequencyUnit2,Cod_postcalendar
								, holidayFlag, businessDate, callMode);//ENH_IS686 changes made by satish for Posting Calendar added an argument Cod_postcalendar for Posting Calendar
					java.util.Date tmp_date = nextActionDates[0];
					nextActionDates = null;
					nextActionDates = new java.util.Date[8];
					//set date posting
					nextActionDates[0] = tmp_date;
					tmp_date = null;
					//set date adjustment posting
					nextActionDates[1] = nextActionDates[0];
					
					if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
						logger.debug("Date posting 111111111 : ["+nextActionDates[0]+"]");
						logger.debug("Date posting 111111111: ["+nextActionDates[1]+"]");
						logger.debug("Date posting 111111111 : ["+nextActionDates[2]+"]");
						logger.debug("Date posting 111111111 : ["+nextActionDates[3]+"]");
						logger.debug("Date posting 111111111 : ["+nextActionDates[4]+"]");
					}
				}
				else{
					/* 
					 * if this is the firstCycle or cycle has changed consider DAT_FIRST_POSTING AS PostingDate
					 * Add settlementDays, creditDelay Days and debitDelay days to get the nextSettlementDate/crDelayDate and DrDelayDate
					 * commented the line below as no need to call calendarService 
					 */
					nextActionDates = new java.util.Date[8];
					//set date posting
					nextActionDates[0] = firstPostingDate;
					//set date adjustment posting
					nextActionDates[1] = firstPostingDate;	
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("Date posting is : ["+nextActionDates[0]+"]");
					
					nextActionDates = 
						ExecutionUtility.getNextPostingCycleDate(conn, frequency, firstPostingDate, firstPostingDate, Cod_postcalendar, businessDate, callMode);
					
					java.util.Date tmp_date = nextActionDates[0];
					nextActionDates[1] = nextActionDates[0];
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("changed tmp date is : ["+tmp_date+"]");
					//Changes Done For BVT Ends
					nextActionDates = new java.util.Date[8];
					nextActionDates[0] = tmp_date; 
					nextActionDates[1] = nextActionDates[0];
					//Changes Done For BVT Ends
					if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
						logger.debug("Date posting 222222222: ["+nextActionDates[0]+"]");
						logger.debug("Date posting 222222222: ["+nextActionDates[1]+"]");
						logger.debug("Date posting 222222222: ["+nextActionDates[2]+"]");
						logger.debug("Date posting 222222222: ["+nextActionDates[3]+"]");
						logger.debug("Date posting 222222222: ["+nextActionDates[4]+"]");
					}

				}
				//ENH_IS1297 Starts...External BVT Changes
				while (nextActionDates[0].compareTo(businessDate)<0) {
					nextActionDates = ExecutionUtility.getNextActionDate(calendarCodes, nextActionDates[0], nextActionDates[1] 
					, typeFrequency, frequencyInfo, frequencyUnit1, frequencyUnit2,Cod_postcalendar
					, holidayFlag, businessDate, "E");
					nextActionDates[1] = nextActionDates[0];
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("dat_posting is : "+nextActionDates[0]);
					java.util.Date tmp_date = nextActionDates[0];
					nextActionDates = null;
					nextActionDates = new java.util.Date[8];
					//set date posting
					nextActionDates[0] = tmp_date;
					tmp_date = null;
					//set date adjustment posting
					nextActionDates[1] = nextActionDates[0];
					/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//					logger.debug("tmp date is : ["+tmp_date+"]");
					/** ****** FindBugs Fix End ***********  on 14DEC09*/
				}
				//ENH_IS1297 Ends

				GregorianCalendar temp = new GregorianCalendar();
				temp.setTime(nextActionDates[0]);
				temp.add(Calendar.DATE, nbr_settle_days);
				//set date post settlement
				nextActionDates[2] = temp.getTime();
				temp = null;
				//set date settlement Cr 
				nextActionDates[3] = 
					ExecutionUtility.getAnotherWorkingDay((String)calendarCodes.get(0), nextActionDates[0], 
							holidayFlag.charAt(0), 0, 0, nbr_settle_days + nbr_delay_days_cr);
				//set date settlement Dr
				nextActionDates[4] = 
					ExecutionUtility.getAnotherWorkingDay((String)calendarCodes.get(0), nextActionDates[0], 
							holidayFlag.charAt(0), 0, 0, nbr_settle_days + nbr_delay_days_dr);
				//set posting start date
				nextActionDates[5] = postingStartDate;
				
				if(Flg_samevaldat.equalsIgnoreCase("Y"))
				{
					//set  dat value Cr = date settlement Cr 
					nextActionDates[6]=nextActionDates[3];
					//set  dat value Dr = date settlement Dr
					nextActionDates[7]=nextActionDates[4];
				}
				else
				{
					
					gDate = new GregorianCalendar();
					gDate.setTime(nextActionDates[0]);
					gDate.add(Calendar.DATE,1);
					incDateUtil =  gDate.getTime();
					java.sql.Date valueDate = new java.sql.Date(incDateUtil.getTime());
					//set  dat value Cr = date posting + 1
					
					nextActionDates[6] = valueDate;
					//set  dat value Dr = date posting + 1
					nextActionDates[7] = valueDate;
					
				}
				
				if (logger.isDebugEnabled()) {
					logger.debug(" KEY:["+codEntity+"] & VALUE:["+nextActionDates[0]+", "+nextActionDates[1]+","+nextActionDates[2]+","+nextActionDates[3]+" ,"+nextActionDates[4]+","+nextActionDates[5]+","+nextActionDates[6]+","+nextActionDates[7]+"] processed");                                 
				}
				postingDatesCacheCurr.put(codEntity, nextActionDates);				
			}
		}
		catch(Exception e) {
			throw new LMSException ("LMS01000","Error occurred while calculating next cycle Posting dates for Process Id :- ",e);
		}
		finally {
			LMSConnectionUtility.close(rst);
			LMSConnectionUtility.close(pstmt);
		}
	}
	//SUP_IS1587 BVT Posting Adjustment ends
	public boolean[] generatesABVTAccountSummaryInfo(long processId, long bvtId,java.util.Date businessDate, String client,
			PreparedStatement insertAccountDtls,PreparedStatement updateAccountDtls,
			PreparedStatement insertAccrualSummary,
			PreparedStatement insertPostingSummary,PreparedStatement updateAccrualSummary,
			PreparedStatement updatePostingSummary, PreparedStatement insertUnpostedAmt,
			PreparedStatement selectAccountDtls ,PreparedStatement selectAccrualSummary ,
			PreparedStatement selectPostingSummary,PreparedStatement updateUnpostedAmt)throws LMSException{
		
		String BVT_ACCRUAL = ExecutionConstants.BVT_TXN;
		long idBVT = bvtId;
		try
		{
			//Intercompany Changes start
			//getCacheUnpostedAmount(BVT_ACCRUAL,idBVT);
			getCacheUnpostedAmount(ExecutionConstants.INTEREST_CALC,BVT_ACCRUAL,idBVT);
			//Intercompany Changes end
			
			processsABvtTXNCodes(processId,bvtId,businessDate, client, insertAccountDtls,updateAccountDtls, insertAccrualSummary, insertPostingSummary, updateAccrualSummary,updatePostingSummary,insertUnpostedAmt,selectAccountDtls ,selectAccrualSummary ,selectPostingSummary,updateUnpostedAmt);
		}
		
		catch(Exception e)
		{
			throw new LMSException ("LMS01000","Error occurred while calculating next cycle Accrual dates for BVT Id :- ",e);
		}
		return entries;
	}
	
	private void processsABvtTXNCodes(long processId, long bvtId,Date businessDate, String host, PreparedStatement insertAccountDtls,
			PreparedStatement updateAccountDtls, PreparedStatement insertAccrualSummary, 
			PreparedStatement insertPostingSummary, PreparedStatement updateAccrualSummary, 
			PreparedStatement updatePostingSummary,PreparedStatement insertUnpostedAmt,
			PreparedStatement selectAccountDtls ,PreparedStatement selectAccrualSummary ,
			PreparedStatement selectPostingSummary,PreparedStatement updateUnpostedAmt ) throws LMSException
	{
		String idBVT = "0";
		String flag_bvt_cycle = ExecutionConstants.FLAG_BVT_CYCLE; //HSBC_BUG_FIX
		StringBuilder sbuf = new StringBuilder();
		try {
				
			/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//			idBVT = new Long(bvtId).toString();
			idBVT = Long.valueOf(bvtId).toString();
			/** ****** FindBugs Fix End ***********  on 14DEC09*/
			
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("BVT ID is : "+idBVT);
			
			/*String FetchBVTCrAMT = (String)interestCalculatorSql.get("FetchBVTCrAMT");
			pstmt = conn.prepareStatement(FetchBVTCrAMT);
			pstmt.setLong(1,processId);
			pstmt.setLong(2,bvtId);
			pstmt.setString(3,ExecutionConstants.ACCOUNT);
			pstmt.setString(4,host);*/
			
			sbuf.setLength(0);
			sbuf.append("SELECT   typ_entity,(CASE ");
		//SUP_IS1587 START //ENH_IS1913 starts
			//HSBC_BUG_FIX starts
			//SUP_ISS_II_7834 starts
			//if("N".equalsIgnoreCase(flag_bvt_cycle)){
				sbuf.append("WHEN (SUBSTR (cod_txn, 5, 1) IN ('C', 'B')) ");
			/*}else{
				sbuf.append("WHEN (SUBSTR (cod_txn, 6, 1) IN ('C', 'B')) ");
			}*/
			//SUP_ISS_II_7834 ends
		 //HSBC_BUG_FIX ends
		//SUP_IS1587 END //ENH_IS1913 ends
			sbuf.append("THEN amt_cr_interest_diff ELSE 0 END ");
			sbuf.append(") amt_calculated, ");
			//SUP_ISS_II_7834 starts
			sbuf.append("currency cod_ccy, dat_business busdt, ");
			if(ExecutionConstants.FLG_SA_INTEREST_TYPE)
				sbuf.append(" SUBSTR(COD_TXN, 1, 5) || 'I' cod_txn, ");
			else	
				sbuf.append(" cod_txn || 'I' cod_txn, ");
			//SUP_ISS_II_7834 ends
			sbuf.append("cod_entity participant_id, 'CREDIT' txn_type, 'NONPOOL_ACCT' id_pool,'N'FLG_REALPOOL, typ_entity TYP_PARTICIPANT ");
			// StandAlone Issue fix Start 
			sbuf.append(",TYP_INTEREST, DAT_POSTING, null flg_acc_accrual_pricing_feed, 0 amt_calculated_central_ccy   "); //SA_BVT_POSTING_CYCLE//SUP_JPMCIS17438//Reallocate to Self
			sbuf.append("FROM olm_txnaccount_bvtdiff_int tbl ");
			sbuf.append("WHERE nbr_processid = "+processId+" AND id_bvt = "+idBVT+" AND typ_entity = '"+ExecutionConstants.ACCOUNT+"' ");
			//sbuf.append("AND cod_gl = '"+host+"' AND amt_cr_interest_diff <> 0 ORDER BY cod_entity, cod_txn ");
			// StandAlone Issue fix END
			sbuf.append("AND cod_gl = '"+host+"' AND amt_cr_interest_diff <> 0 ORDER BY cod_entity, cod_txn , typ_interest, busdt"); //SA_BVT_POSTING_CYCLE
			
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("Query for fetching BVT CR Amount is : "+sbuf.toString());
			
			populateBvtAccountInfo(sbuf.toString(),idBVT,businessDate, host, insertAccountDtls,updateAccountDtls, 
					insertAccrualSummary, insertPostingSummary, updateAccrualSummary, updatePostingSummary,
					insertUnpostedAmt,selectAccountDtls ,selectAccrualSummary ,selectPostingSummary,updateUnpostedAmt, null);//Reallocate to Self
						
			/*String FetchBVTDrAMT = (String)interestCalculatorSql.get("FetchBVTDrAMT");
			pstmt = conn.prepareStatement(FetchBVTDrAMT);
			pstmt.setLong(1,processId);
			pstmt.setLong(2,bvtId);
			pstmt.setString(3,ExecutionConstants.ACCOUNT);
			pstmt.setString(4,host);*/
			
			sbuf.setLength(0);
				//SUP_IS1587 START //ENH_IS1913 starts
		//HSBC_BUG_FIX starts
		//SUP_ISS_II_7834 starts
			//if("N".equalsIgnoreCase(flag_bvt_cycle)){
				sbuf.append("SELECT   typ_entity, (CASE WHEN (SUBSTR (cod_txn, 5, 1) IN ('D', 'B')) ");
			/*}else{
				sbuf.append("SELECT   typ_entity, (CASE WHEN (SUBSTR (cod_txn, 6, 1) IN ('D', 'B')) ");
			}*/
			//SUP_ISS_II_7834 ends
		//HSBC_BUG_FIX ends
			//SUP_IS1587 END //ENH_IS1913 ends


			sbuf.append("THEN amt_dr_interest_diff ELSE 0 END ");
			sbuf.append(") amt_calculated, ");
			//SUP_ISS_II_7834 starts
			//sbuf.append("currency cod_ccy, dat_business busdt, cod_txn || 'I' cod_txn, ");
			sbuf.append("currency cod_ccy, dat_business busdt, ");
			if(ExecutionConstants.FLG_SA_INTEREST_TYPE)
				sbuf.append(" SUBSTR(COD_TXN, 1, 5) || 'I' cod_txn, ");
			else	
				sbuf.append(" cod_txn || 'I' cod_txn, ");
			//SUP_ISS_II_7834 ends
			sbuf.append("cod_entity participant_id, 'DEBIT' txn_type, 'NONPOOL_ACCT' id_pool,'N'FLG_REALPOOL, typ_entity TYP_PARTICIPANT ");
			// StandAlone Issue fix
			sbuf.append(",TYP_INTEREST,DAT_POSTING, null flg_acc_accrual_pricing_feed, 0 amt_calculated_central_ccy   ");//SA_BVT_POSTING_CYCLE//SUP_JPMCIS17438//Reallocate to Self
			sbuf.append("FROM olm_txnaccount_bvtdiff_int tbl ");
			sbuf.append("WHERE nbr_processid = "+processId+" AND id_bvt = "+idBVT+" AND typ_entity = '"+ExecutionConstants.ACCOUNT+"' ");
			//sbuf.append("AND cod_gl = '"+host+"' AND amt_dr_interest_diff <> 0 ORDER BY cod_entity, cod_txn ");
			sbuf.append("AND cod_gl = '"+host+"' AND amt_dr_interest_diff <> 0 ORDER BY cod_entity, cod_txn ,typ_interest, busdt"); //SA_BVT_POSTING_CYCLE
			// StandAlone Issue fix end
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("Query for fetching BVT DR Amount is : "+sbuf.toString());
			
			populateBvtAccountInfo(sbuf.toString(),idBVT,businessDate, host, insertAccountDtls,updateAccountDtls, 
					insertAccrualSummary, insertPostingSummary, updateAccrualSummary, updatePostingSummary,
					insertUnpostedAmt,selectAccountDtls ,selectAccrualSummary ,selectPostingSummary,updateUnpostedAmt, null);//Reallocate to Self
			
		}
		/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//		catch(Exception e) {
		catch(LMSException e) {
		/** ****** FindBugs Fix End ***********  on 14DEC09*/
			throw new LMSException ("LMS01000","Error occurred while fetching Cr / DR BVT amount for BVT Id :- "+ idBVT+" and host = ["+host+"]",e);
		}
	}
	
	/**
	 * 
	 * @param processId
	 * @param bvtId
	 * @param businessDate
	 * @param client
	 * @param insertAccountDtls
	 * @param updateAccountDtls
	 * @param insertAccrualSummary
	 * @param insertPostingSummary
	 * @param updateAccrualSummary
	 * @param updatePostingSummary
	 * @param insertUnpostedAmt
	 * @param selectAccountDtls
	 * @param selectAccrualSummary
	 * @param selectPostingSummary
	 * @param updateUnpostedAmt
	 * @return
	 * @throws LMSException
	 */
	public boolean[] generateInCoBVTAccountSummaryInfo(long processId, long bvtId,java.util.Date businessDate, String client,
			PreparedStatement insertInCoDtls,PreparedStatement updateInCoDtls,
			PreparedStatement insertAccrualSummary,
			PreparedStatement insertPostingSummary,PreparedStatement updateAccrualSummary,
			PreparedStatement updatePostingSummary, PreparedStatement insertUnpostedAmt,
			PreparedStatement selectInCoDtls ,PreparedStatement selectAccrualSummary ,
			PreparedStatement selectPostingSummary,PreparedStatement updateUnpostedAmt)throws LMSException{
		
		String BVT_ACCRUAL = ExecutionConstants.BVT_TXN;
		long idBVT = bvtId;
		try
		{
			getCacheUnpostedAmount(ExecutionConstants.INCO_INTEREST_CALC,BVT_ACCRUAL,idBVT);
			
			processInCoBvtTXNCodes(processId,bvtId,businessDate, client, insertInCoDtls,updateInCoDtls, insertAccrualSummary, insertPostingSummary, updateAccrualSummary,updatePostingSummary,insertUnpostedAmt,selectInCoDtls ,selectAccrualSummary ,selectPostingSummary,updateUnpostedAmt);
		}
		
		catch(Exception e)
		{
			throw new LMSException ("LMS01000","Error occurred while calculating next cycle Accrual dates for BVT Id :- ",e);
		}
		return entries;
	}
	/**
	 * 
	 * @param processId
	 * @param bvtId
	 * @param businessDate
	 * @param host
	 * @param insertAccountDtls
	 * @param updateAccountDtls
	 * @param insertAccrualSummary
	 * @param insertPostingSummary
	 * @param updateAccrualSummary
	 * @param updatePostingSummary
	 * @param insertUnpostedAmt
	 * @param selectAccountDtls
	 * @param selectAccrualSummary
	 * @param selectPostingSummary
	 * @param updateUnpostedAmt
	 * @throws LMSException
	 */
	private void processInCoBvtTXNCodes(long processId, long bvtId,Date businessDate, String host, PreparedStatement insertInCoDtls,
			PreparedStatement updateInCoDtls, PreparedStatement insertAccrualSummary, 
			PreparedStatement insertPostingSummary, PreparedStatement updateAccrualSummary, 
			PreparedStatement updatePostingSummary,PreparedStatement insertUnpostedAmt,
			PreparedStatement selectInCoDtls ,PreparedStatement selectAccrualSummary ,
			PreparedStatement selectPostingSummary,PreparedStatement updateUnpostedAmt ) throws LMSException
	{
		String idBVT = "0";
		
		StringBuilder sbuf = new StringBuilder();
		try {
				
			/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//			idBVT = new Long(bvtId).toString();
			idBVT = Long.valueOf(bvtId).toString();
			/** ****** FindBugs Fix End ***********  on 14DEC09*/
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("BVT ID is : "+idBVT);
			
			sbuf.setLength(0);
			sbuf.append("SELECT tbl.typ_entity,(CASE WHEN (SUBSTR (tbl.cod_txn, 5, 1) IN ('C', 'B')) ");
			sbuf.append(" THEN tbl.amt_cr_interest_diff ELSE 0 END) amt_calculated, ");
			sbuf.append(" tbl.currency cod_ccy, tbl.dat_business busdt, ");
			sbuf.append(" tbl.cod_txn || 'I' cod_txn, tbl.cod_entity participant_id, ");
			sbuf.append(" 'CREDIT' txn_type, incoloan.cod_incoid id_pool, 'N' flg_realpool, ");
			sbuf.append(" tbl.typ_entity typ_participant,DAT_POSTING, null flg_acc_accrual_pricing_feed, 0 amt_calculated_central_ccy  "); //ANZ_BVI_ISSUE_1//SUP_JPMCIS17438//Reallocate to Self
			sbuf.append(" FROM OLM_TXNACCOUNT_BVTDIFF_INT tbl, OLM_INCO_LOAN_DETAILS incoloan ");
			sbuf.append(" WHERE tbl.cod_entity = incoloan.cod_incoloanid ");
			sbuf.append(" AND nbr_processid = "+processId+" AND id_bvt = "+idBVT+"");
			sbuf.append(" AND typ_entity = '"+ExecutionConstants.INCOLOAN+"'");
			sbuf.append(" AND cod_gl = '"+host+"' AND amt_cr_interest_diff <> 0 ");
			//sbuf.append(" ORDER BY cod_entity, cod_txn");
			sbuf.append(" ORDER BY ");			//SUP_ISS_III_7305
			sbuf.append(" cod_entity, tbl.typ_entity ,tbl.dat_business , tbl.cod_txn, ");		//SUP_ISS_III_7305
			sbuf.append(" tbl.dat_posting "); //SUP_ISS_III_7305
			
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("Query for fetching BVT CR Amount is : "+sbuf.toString());
			
			populateBvtAccountInfo(sbuf.toString(),idBVT,businessDate, host, insertInCoDtls,updateInCoDtls, 
					insertAccrualSummary, insertPostingSummary, updateAccrualSummary, updatePostingSummary,
					insertUnpostedAmt,selectInCoDtls ,selectAccrualSummary ,selectPostingSummary,updateUnpostedAmt, null);//Reallocate to Self
						
			
			sbuf.setLength(0);
			sbuf.append("SELECT   tbl.typ_entity,(CASE WHEN (SUBSTR (tbl.cod_txn, 5, 1) IN ('D', 'B')) ");
	        sbuf.append(" THEN tbl.amt_dr_interest_diff ELSE 0 END) amt_calculated, ");
			sbuf.append(" tbl.currency cod_ccy, tbl.dat_business busdt,tbl.cod_txn || 'I' cod_txn,");
			sbuf.append(" tbl.cod_entity participant_id,'DEBIT' txn_type, incoloan.cod_incoid id_pool,");
			//sbuf.append(" 'N' flg_realpool,tbl.typ_entity typ_participant ");
			sbuf.append(" 'N' flg_realpool,tbl.typ_entity typ_participant , DAT_POSTING  , null flg_acc_accrual_pricing_feed, 0 amt_calculated_central_ccy  "); //ANZ_BVI_ISSUE_1//SUP_JPMCIS17438//Reallocate to Self
			sbuf.append(" FROM OLM_TXNACCOUNT_BVTDIFF_INT tbl, OLM_INCO_LOAN_DETAILS incoloan ");
			sbuf.append(" WHERE tbl.cod_entity = incoloan.cod_incoloanid" );
			sbuf.append(" AND tbl.nbr_processid = "+processId+" AND tbl.id_bvt = "+idBVT+"");
			sbuf.append(" AND tbl.typ_entity = '"+ExecutionConstants.INCOLOAN+"' AND tbl.cod_gl = '"+host+"'");
			sbuf.append(" AND tbl.amt_dr_interest_diff <> 0	");
			//sbuf.append(" ORDER BY tbl.cod_entity, tbl.cod_txn ");
			sbuf.append(" ORDER BY ");			//SUP_ISS_III_7305
			sbuf.append(" cod_entity, tbl.typ_entity ,tbl.dat_business , tbl.cod_txn, ");		//SUP_ISS_III_7305
			sbuf.append(" tbl.dat_posting "); //SUP_ISS_III_7305
	
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
			logger.debug("Query for fetching BVT DR Amount is : "+sbuf.toString());
			
			populateBvtAccountInfo(sbuf.toString(),idBVT,businessDate, host, insertInCoDtls,updateInCoDtls, 
					insertAccrualSummary, insertPostingSummary, updateAccrualSummary, updatePostingSummary,
					insertUnpostedAmt,selectInCoDtls ,selectAccrualSummary ,selectPostingSummary,updateUnpostedAmt,null);//Reallocate to Self
			
		}
		/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//		catch(Exception e) {
		catch(LMSException e) {
		/** ****** FindBugs Fix End ***********  on 14DEC09*/
			throw new LMSException ("LMS01000","Error occurred while fetching Cr / DR BVT amount for BVT Id :- "+ idBVT+" and host = ["+host+"]",e);
		}
	}
	
	private static void setSqlProperties() throws LMSException 
	{
		try 
		{
			interestCalculatorSql = LMSConnectionUtility.getProperties("InterestCalculatorSql.properties");
		}
		catch (Exception e) 
		{
			logger.debug("Sql property file can not be loded:",e);
			throw new LMSException ("1000","Problem while loading the Sql properties file",e);
		}
	}
	//ENH_IS1297 Ends...BVT Ends

	/**
	 * @param accountInfoDO
	 * @return
	 * @throws LMSException
	 * 
	 * 	Returns false if the Loan End Date is Current Business Date. 
	 *  On Loan end date the maturity engine will handle the posting and mark the status as 'POSTING_GEN'
	 */
	private boolean checkPostingToBeDone(java.sql.Date businessDate, AccountInfoDO accountInfoDO) throws LMSException 
	{
		boolean flgReturn = true;
		try 
		{
			if(accountInfoDO.getTYP_PARTICIPANT().equals(ExecutionConstants.INCOLOAN))
			{
				if(accountInfoDO.getPARTY_DAT_END() != null)
				{
					if(businessDate.toString().equalsIgnoreCase(accountInfoDO.getPARTY_DAT_END().toString()))
					{
						flgReturn = false;	
					}
				}
			}
		} 
		catch (Exception e) 
		{
			logger.debug("checkPostingToBeDone", e);
			throw new LMSException("LMS01000", " checkPostingToBeDone ", e);
		}
		return flgReturn;

	}
	// ENH_10.3_153 Starts
	/**
	 *  This method frame the query to get the required data from opool_txnpool_allocationdtls table.
	 *  
	 * @return
	 */
	private static String getTxnPoolAllocationDtls(String typ_participant) //Retro from JPMC LIQ_REL_PROD_9.11_016_E - SIT
	{
		boolean flg_newAccountingentry = ExecutionConstants.FlG_NEW_ACCOUNTING_ENTRY_FORMAT;
		StringBuilder lQuery = new StringBuilder() ;
		lQuery.append(" SELECT   dtls.nbr_processid, dtls.id_pool, dtls.typ_settle, dtls.flg_real, ")
		      .append("                  dtls.flg_allocation, dtls.cod_ccy_account, dtls.dat_business_poolrun, ")
		      .append("                  dtls.participant_id, dtls.cod_gl, dtls.cod_gl partycod_gl, ")
		      .append("                  dtls.typ_participant, dtls.cod_txn, dtls.cod_ccy_account cod_ccy,")
		      .append("                 SUM(CASE  WHEN (SUBSTR (dtls.cod_txn, 5, 1) = 'D') ")
		      .append("                           THEN eqv_amt_allocation * -1 ELSE eqv_amt_allocation END ")
		      //.append("                    ) amt_calculated, dtls.typ_interest,id_bvt, eqv_amt_allocation ")//JPMC RETRO ISSUE FIXED
//Retro_Mashreq
		      //.append("                    ) amt_calculated, dtls.typ_interest,id_bvt ") //Sanity_V10.5
		      //.append("                    ) amt_calculated, dtls.typ_interest,id_bvt,dtls.narration ") //Sanity_V10.5 //MASHREQ_4647
		      .append("                    ) amt_calculated, dtls.typ_interest,id_bvt,dtls.narration "); //Sanity_V10.5 //MASHREQ_4647
			  // NEW_ACCOUNTING_ENTRY_FORMAT 
		     if(flg_newAccountingentry)
		     { 
		    	 lQuery.append(" , SOURCE_POOL_ID , AMT_ALLOCATION_CENTRAL_CCY, RATE_EXRATE_CENTRAL_CCY ");
		     }else
		     {
		    	 lQuery.append(" , dtls.id_pool SOURCE_POOL_ID , AMT_ALLOCATION_CENTRAL_CCY, RATE_EXRATE_CENTRAL_CCY ");//Reallocate to Self
		     }
			//TI_FEE_CHANGES STARTS
		     lQuery.append("       FROM opool_txnpool_allocationdtls dtls, ORATE_INTEREST_TYPE INT_TYPE      ")
		      .append("      WHERE dtls.typ_participant = '"+typ_participant+"' AND dtls.id_bvt = 0 ") //Retro from JPMC LIQ_REL_PROD_9.11_016_E - SIT
		      //.append("      and ('Y' = int_type.flg_only_pool_level or int_type.flg_only_pool_level is null) ")     //Sandeep changes for ned issue
		      .append("      and dtls.typ_interest = int_type.typ_interest(+) ")
		      .append("      and ('N' = int_type.flg_interestypeas_fee or int_type.flg_interestypeas_fee is null) ")
			//TI_FEE_CHANGES ENDS
		      .append("   GROUP BY dtls.nbr_processid, dtls.id_pool, dtls.typ_settle, ")
		      .append("            dtls.flg_real, dtls.flg_allocation, dtls.cod_ccy_account, ")
		      .append("            dtls.dat_business_poolrun, dtls.participant_id, dtls.cod_gl,")
		      //.append("            dtls.typ_participant,dtls.cod_txn,dtls.typ_interest,dtls.id_bvt,dtls.eqv_amt_allocation ") ;//JPMC RETRO ISSUE FIXED
		      .append("            dtls.typ_participant,dtls.cod_txn,dtls.typ_interest,dtls.id_bvt ,dtls.narration ");   //Sanity_V10.5
		      // NEW_ACCOUNTING_ENTRY_FORMAT
		      if(flg_newAccountingentry)
		     { 
		    	  lQuery.append(" , SOURCE_POOL_ID, AMT_ALLOCATION_CENTRAL_CCY, RATE_EXRATE_CENTRAL_CCY ");//Reallocate to Self
		     }else
		     {
		    	 lQuery.append(" , dtls.id_pool, AMT_ALLOCATION_CENTRAL_CCY, RATE_EXRATE_CENTRAL_CCY ");//Reallocate to Self
		    	 
		     }
			  	
		
		if(logger.isDebugEnabled())
			logger.debug("Allocation Details Query " + lQuery) ;
				      
		return lQuery.toString() ;		
	}
 // ENH_10.3_153 Ends
	//SA_BVT_POSTING_CYCLE STARTS
	/** This method call the processCycleWiseBvtTXNCodes method
	 * @param bvtId
	 * @param businessDate
	 * @param client
	 * @param insertAccountDtls
	 * @param updateAccountDtls
	 * @param insertAccrualSummary
	 * @param insertPostingSummary
	 * @param updateAccrualSummary
	 * @param updatePostingSummary
	 * @param insertUnpostedAmt
	 * @param selectAccountDtls
	 * @param selectAccrualSummary
	 * @param selectPostingSummary
	 * @return
	 * @throws LMSException
	 */
	public boolean[] generateCycleWiseBVTAccountSummaryInfoForSA(long processId,long bvtId,java.util.Date businessDate, String client,
			PreparedStatement insertAccountDtls,PreparedStatement updateAccountDtls,
			PreparedStatement insertAccrualSummary,
			PreparedStatement insertPostingSummary,PreparedStatement updateAccrualSummary,
			PreparedStatement updatePostingSummary, PreparedStatement insertUnpostedAmt,
			PreparedStatement selectAccountDtls ,PreparedStatement selectAccrualSummary ,
			PreparedStatement selectPostingSummary,PreparedStatement updateUnpostedAmt)throws LMSException
	{
			
		if (logger.isDebugEnabled()){
			logger.debug("Entering generateCycleWiseBVTAccountSummaryInfo");
		}
		
		try
		{
			processSACycleWiseBvtTXNCodes(processId, bvtId,businessDate, client, insertAccountDtls,updateAccountDtls, 
					insertAccrualSummary, insertPostingSummary, updateAccrualSummary, 
					updatePostingSummary,insertUnpostedAmt,selectAccountDtls ,selectAccrualSummary ,selectPostingSummary,updateUnpostedAmt);
			
			
			
			
		}
			
		catch(Exception e)
		{
			logger.fatal("General Exception: ", e);
			throw new LMSException("LMS01000","Error occurred while generating BVTAccount Summary Information for BVT Id "+bvtId+" and Host System "+client+".", e);
		}
		if (logger.isDebugEnabled()){
			logger.debug("Leaving generateCycleWiseBVTAccountSummaryInfo");
		}
		return entries;
	}
	
	
	private void processSACycleWiseBvtTXNCodes(long processId, long bvtId,Date businessDate, String host, PreparedStatement insertAccountDtls,
			PreparedStatement updateAccountDtls, PreparedStatement insertAccrualSummary, 
			PreparedStatement insertPostingSummary, PreparedStatement updateAccrualSummary, 
			PreparedStatement updatePostingSummary,PreparedStatement insertUnpostedAmt,
			PreparedStatement selectAccountDtls ,PreparedStatement selectAccrualSummary ,
			PreparedStatement selectPostingSummary,PreparedStatement updateUnpostedAmt ) throws LMSException
	{
		String idBVT = "0";
		String flag_bvt_cycle = ExecutionConstants.FLAG_BVT_CYCLE; 
		StringBuffer sbuf = new StringBuffer();
		try {
				
			idBVT = Long.valueOf(bvtId).toString();
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("BVT ID is : "+idBVT);
			
			sbuf.append("SELECT   tbl.TYP_ENTITY, ");
			sbuf.append("         (CASE ");
			//SUP_ISS_II_7834 starts
			//if("N".equalsIgnoreCase(flag_bvt_cycle)){
				sbuf.append("WHEN (SUBSTR (TBL.cod_txn, 5, 1) IN ('C', 'B')) ");
			/*}else{
				sbuf.append("WHEN (SUBSTR (TBL.cod_txn, 6, 1) IN ('C', 'B')) ");
			}*/
			//SUP_ISS_II_7834 ends
			sbuf.append("                THEN TBL.AMT_CR_INTEREST_DIFF ");
			sbuf.append("             ELSE 0 ");
			sbuf.append("          END ");
			sbuf.append("         ) AMT_CALCULATED, ");
			//SUP_ISS_II_7834 starts
			//sbuf.append("         CURRENCY COD_CCY, TBL.DAT_BUSINESS BUSDT, TBL.COD_TXN || 'I' COD_TXN, ");
			sbuf.append("CURRENCY COD_CCY, TBL.DAT_BUSINESS BUSDT, ");
			if(ExecutionConstants.FLG_SA_INTEREST_TYPE)
				sbuf.append(" SUBSTR(TBL.COD_TXN, 1, 5) || 'I' cod_txn, ");
			else	
				sbuf.append(" TBL.COD_TXN || 'I' cod_txn, ");
			//SUP_ISS_II_7834 ends
			sbuf.append("         TBL.COD_ENTITY PARTICIPANT_ID, 'CREDIT' TXN_TYPE, 'NONPOOL_ACCT' ID_POOL, ");
			sbuf.append("         'N' FLG_REALPOOL, TBL.TYP_ENTITY TYP_PARTICIPANT, TBL.TYP_INTEREST, ");
			sbuf.append("         TBL.DAT_POSTING, NVL(DAT_LASTPOSTING+1,DAT_START) DAT_POSTING_START "); 
			sbuf.append("    FROM OLM_TXNACCOUNT_BVTDIFF_INT TBL, ");
			sbuf.append("    OLM_TXNACCOUNT_INTERESTINFO INTINFO ");
			sbuf.append("   , OLM_GLMST glmst");      //SA_POSTING_BVT_CHANGE 
			sbuf.append("   WHERE TBL.NBR_PROCESSID = "+processId+" ");
			sbuf.append("     AND TBL.ID_BVT = "+idBVT+" ");
			sbuf.append("     AND TBL.TYP_ENTITY = '"+ExecutionConstants.ACCOUNT+"' ");
			sbuf.append("      AND TBL.cod_gl = INTINFO.cod_gl ");							//SA_POSTING_BVT_CHANGE
			sbuf.append("      AND glmst.cod_gl = INTINFO.cod_gl ");						//SA_POSTING_BVT_CHANGE
			sbuf.append("      AND glmst.FLG_BACKVALUE_POSTING = '"+ExecutionConstants.YES+"'"); // SA_POSTING_BVT_CHANGE
			sbuf.append("     AND TBL.COD_GL = '"+host+"' ");
			sbuf.append("     AND TBL.AMT_CR_INTEREST_DIFF <> 0 ");
			sbuf.append("     AND TBL.NBR_PROCESSID = INTINFO.NBR_PROCESSID ");
			sbuf.append("     AND TBL.COD_ENTITY=INTINFO.COD_ENTITY ");
			sbuf.append("     AND TBL.TYP_ENTITY = INTINFO.TYP_ENTITY ");
			sbuf.append("     AND TBL.TYP_INTEREST = INTINFO.TYP_INTEREST ");
			sbuf.append("     AND TBL.DAT_BUSINESS = INTINFO.DAT_BUSINESS ");
			sbuf.append("ORDER BY TBL.COD_ENTITY, TBL.COD_TXN, TBL.TYP_INTEREST, BUSDT ");
					
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("Query for fetching BVT CR Amount is : "+sbuf.toString()); 
			
			populateCycleWiseBvtAccountInfoSA(sbuf.toString(),idBVT,businessDate, host, insertAccountDtls,updateAccountDtls, 
					insertAccrualSummary, insertPostingSummary, updateAccrualSummary, updatePostingSummary,
					insertUnpostedAmt,selectAccountDtls ,selectAccrualSummary ,selectPostingSummary,updateUnpostedAmt);
						
			
			
			sbuf.setLength(0);
			
			sbuf.append("SELECT   tbl.typ_entity, ");
			sbuf.append("         (CASE ");
			//SUP_ISS_II_7834 starts
			//if("N".equalsIgnoreCase(flag_bvt_cycle)){
				sbuf.append(" WHEN (SUBSTR (tbl.cod_txn, 5, 1) IN ('D', 'B')) ");
			/*}else{
				sbuf.append(" WHEN (SUBSTR (tbl.cod_txn, 6, 1) IN ('D', 'B')) ");
			}*/
			//SUP_ISS_II_7834 ends
			sbuf.append("                THEN amt_dr_interest_diff ");
			sbuf.append("             ELSE 0 ");
			sbuf.append("          END ");
			sbuf.append("         ) amt_calculated, ");
			//SUP_ISS_II_7834 starts
			//sbuf.append("         currency cod_ccy, tbl.dat_business busdt, tbl.cod_txn || 'I' cod_txn, ");
			sbuf.append("CURRENCY COD_CCY, TBL.DAT_BUSINESS BUSDT, ");
			if(ExecutionConstants.FLG_SA_INTEREST_TYPE)
				sbuf.append(" SUBSTR(TBL.COD_TXN, 1, 5) || 'I' cod_txn, ");
			else	
				sbuf.append(" TBL.COD_TXN || 'I' cod_txn, ");
			//SUP_ISS_II_7834 ends
			sbuf.append("         tbl.cod_entity participant_id, 'DEBIT' txn_type, 'NONPOOL_ACCT' id_pool, ");
			sbuf.append("         'N' flg_realpool, tbl.typ_entity typ_participant, tbl.typ_interest, ");
			sbuf.append("         tbl.dat_posting, NVL(DAT_LASTPOSTING+1,DAT_START) DAT_POSTING_START ");
			sbuf.append("    FROM olm_txnaccount_bvtdiff_int tbl, ");
			sbuf.append("    OLM_TXNACCOUNT_INTERESTINFO INTINFO ");
			sbuf.append("   , OLM_GLMST glmst"); 		//SA_POSTING_BVT_CHANGE
			sbuf.append("   WHERE tbl.nbr_processid = "+processId+" ");
			sbuf.append("     AND tbl.id_bvt = "+idBVT+" ");
			sbuf.append("     AND tbl.typ_entity = '"+ExecutionConstants.ACCOUNT+"' ");
			sbuf.append("      AND TBL.cod_gl = INTINFO.cod_gl ");							// SA_POSTING_BVT_CHANGE
			sbuf.append("      AND glmst.cod_gl = INTINFO.cod_gl ");						//SA_POSTING_BVT_CHANGE
			sbuf.append("      AND glmst.FLG_BACKVALUE_POSTING = '"+ExecutionConstants.YES+"'"); // SA_POSTING_BVT_CHANGE
			sbuf.append("     AND tbl.cod_gl = '"+host+"' ");
			sbuf.append("     AND amt_dr_interest_diff <> 0 ");
			sbuf.append("     AND tbl.nbr_processid = INTINFO.nbr_processid ");
			sbuf.append("     AND tbl.COD_ENTITY=INTINFO.COD_ENTITY ");
			sbuf.append("     AND tbl.TYP_ENTITY = INTINFO.TYP_ENTITY ");
			sbuf.append("     AND tbl.TYP_INTEREST = INTINFO.TYP_INTEREST ");
			sbuf.append("     AND tbl.DAT_BUSINESS = INTINFO.DAT_BUSINESS ");
			sbuf.append("ORDER BY tbl.cod_entity, cod_txn, typ_interest, busdt ");
			
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("Query for fetching BVT DR Amount is : "+sbuf.toString());
			
			populateCycleWiseBvtAccountInfoSA(sbuf.toString(),idBVT,businessDate, host, insertAccountDtls,updateAccountDtls, 
					insertAccrualSummary, insertPostingSummary, updateAccrualSummary, updatePostingSummary,
					insertUnpostedAmt,selectAccountDtls ,selectAccrualSummary ,selectPostingSummary,updateUnpostedAmt);
			
		}
		catch(LMSException e) {
			throw new LMSException ("LMS01000","Error occurred while fetching Cr / DR BVT amount for BVT Id :- "+ idBVT+" and host = ["+host+"]",e);
		}
	}
	
	/**This method execute the query and set data in Do Object and call the populateBvtAccountDtls , populateBVTAccuralSummary ,populateBVTPostingSummary
	 * @param strQuery
	 * @param bvtId
	 * @param businessDate
	 * @param client
	 * @param insertAccountDtls
	 * @param updateAccountDtls
	 * @param insertAccrualSummary
	 * @param insertPostingSummary
	 * @param updateAccrualSummary
	 * @param updatePostingSummary
	 * @param insertUnpostedAmt
	 * @param selectAccountDtls
	 * @param selectAccrualSummary
	 * @param selectPostingSummary
	 * @throws LMSException
	 */
	private void populateCycleWiseBvtAccountInfoSA(String strQuery,String bvtId, Date businessDate, String client, 
			PreparedStatement insertAccountDtls, PreparedStatement updateAccountDtls, 
			PreparedStatement insertAccrualSummary, PreparedStatement insertPostingSummary, 
			PreparedStatement updateAccrualSummary, PreparedStatement updatePostingSummary,
			PreparedStatement insertUnpostedAmt,PreparedStatement selectAccountDtls ,
			PreparedStatement selectAccrualSummary ,PreparedStatement selectPostingSummary,PreparedStatement updateUnpostedAmt
		) throws LMSException
	{
		if (logger.isDebugEnabled()){
			logger.debug("Entering ");
		}
		PreparedStatement pStmt = null;
		Connection conn = null;
		ResultSet res = null;
		boolean recordExists = false,firstTime = true;
		AccountInfoDO accountInfoDo = null;
		java.sql.Date businessdate = new java.sql.Date(businessDate.getTime());
		java.util.Date dateList[]= null;
		String txnCode = "";
		String typEntity="";
		try
		{
			try 
			{
				conn = LMSConnectionUtility.getConnection();
			} catch (NamingException e) 
			{
				logger.fatal(" Error occured while getting connection :"+e.getMessage());
			}

			pStmt = conn.prepareStatement(strQuery);
			res = pStmt.executeQuery();
			accountInfoDo = new AccountInfoDO();
            String flag_bvt_cycle = ExecutionConstants.FLAG_BVT_CYCLE;
            if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
            	logger.debug("flag_bvt_cycle:::"+flag_bvt_cycle);
	        
	        while(res.next())
			{
				txnCode = setTxnCode(res.getString("COD_TXN"),res.getString("TXN_TYPE"));
				recordExists = true;
				typEntity=res.getString("TYP_PARTICIPANT");
				String tmp =res.getString("ID_POOL");
				accountInfoDo.setTYP_INTEREST(res.getString("typ_interest"));
				
				if(!firstTime)
				{
					if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase(ExecutionConstants.YES))
					{
						if(res.getDate("DAT_POSTING").compareTo(businessdate) < 0)
						{
							dateList = (java.util.Date[]) postingDatesCache.get(res.getString("PARTICIPANT_ID"));
							if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
								logger.debug("1ND inside if::: dateList "+dateList);
								logger.debug("date arraylist: "+dateList[0]+" , "+dateList[3]+" and "+dateList[6]);
							}
							txnCode = txnCode + ExecutionConstants.BVT_POST_CYCLE;
						}
						else
						{
							dateList = (java.util.Date[]) postingDatesCacheCurr.get(res.getString("PARTICIPANT_ID"));
							if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
								logger.debug("1ND inside else::: dateList "+dateList);
								logger.debug("date arraylist: "+dateList[0]+" , "+dateList[3]+" and "+dateList[6]);
							}
							txnCode = txnCode + ExecutionConstants.NORMAL_POST_CYCLE;
						}
					}
					
					if(txnCode.equalsIgnoreCase(accountInfoDo.getCOD_TXN()) 
									&& res.getString("PARTICIPANT_ID").equalsIgnoreCase(accountInfoDo.getPARTICIPANT_ID())
									&& res.getString("ID_POOL").equalsIgnoreCase(accountInfoDo.getID_POOL())
									&& res.getDate("DAT_POSTING").equals(accountInfoDo.getDAT_INITIAL_POSTING()))
					{
						if (logger.isDebugEnabled())
						{
							logger.debug("accountInfoDo.getPARTICIPANT_ID()" +accountInfoDo.getPARTICIPANT_ID());
							logger.debug("accountInfoDo.getCOD_TXN()"+accountInfoDo.getCOD_TXN());
							logger.debug("accountInfoDo.getID_POOL()" +accountInfoDo.getID_POOL());
						}
						accountInfoDo.setDAT_BUSINESS(res.getDate("BUSDT"));
						accountInfoDo.setAMT_CALCULATED(res.getBigDecimal("AMT_CALCULATED"));
						if (logger.isDebugEnabled()){
							logger.debug("res.getBigDecimal(AMT_CALCULATED)" +res.getBigDecimal("AMT_CALCULATED"));
							logger.debug("accountInfoDo.getAMT_CALCULATED " +accountInfoDo.getAMT_CALCULATED());
						}
						//Set AMT_CAL_SUM = AMT_CAL_SUM + AMT_CALCULATED
						accountInfoDo.setAMT_CAL_SUM(res.getBigDecimal("AMT_CALCULATED").add(accountInfoDo.getAMT_CAL_SUM()));
						//CAll populateBvtAccountDtls()
						if (logger.isDebugEnabled()){
							logger.debug("accountInfoDo.getAMT_CAL_SUM " +accountInfoDo.getAMT_CAL_SUM());
						}						
						
						continue ;
					}
					else
					{
						populateCycleWiseBVTAccuralSummaryForPool(businessdate,insertAccrualSummary,updateAccrualSummary,insertUnpostedAmt,accountInfoDo,selectAccrualSummary,updateUnpostedAmt, conn);
						populateCycleWiseBVTPostingSummaryForPool(businessdate,insertPostingSummary,updatePostingSummary,accountInfoDo,selectPostingSummary);

						if(logger.isDebugEnabled())
						{
							logger.debug(" **** Populating TAX LIST for.... ****");
							logger.debug("typEntity :::: " + typEntity);
							logger.debug("businessDate :::: " + businessDate);
							logger.debug("postingDate :::: " + accountInfoDo.getDAT_POSTING().toString());
						}
					}
				}
				firstTime = false;
				//Set resultset data  into Data Object
				accountInfoDo.clearObject();
				accountInfoDo.setDAT_CURRENT_BUSINESS(businessdate);
				accountInfoDo.setDAT_BUSINESS(res.getDate("BUSDT"));
				accountInfoDo.setAMT_CALCULATED(res.getBigDecimal("AMT_CALCULATED"));
				accountInfoDo.setAMT_CAL_SUM(res.getBigDecimal("AMT_CALCULATED"));
				accountInfoDo.setPARTICIPANT_ID(res.getString("PARTICIPANT_ID"));
				accountInfoDo.setID_POOL(res.getString("ID_POOL"));
				accountInfoDo.setFLG_REALPOOL(res.getString("FLG_REALPOOL"));
				//REPLACE LAST CHAR OF  THE TNX CODE BY C OR D
				txnCode = setTxnCode(res.getString("COD_TXN"),res.getString("TXN_TYPE"));
				accountInfoDo.setCOD_TXN(txnCode);
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("txnCode #### " + txnCode);
				accountInfoDo.setCOD_CCY(res.getString("COD_CCY"));
				accountInfoDo.setTYP_PARTICIPANT(res.getString("TYP_PARTICIPANT"));
						
				accountInfoDo.setFLG_REALPOSTING("NA");
				accountInfoDo.setFLG_POOL_CONSOLIDATED("NA");
				accountInfoDo.setFLG_ACC_CONSOLIDATED("NA");
				
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("Inside If condition codTransaction Value -->"+accountInfoDo.getCOD_TXN());
	        	String codTxnString = accountInfoDo.getCOD_TXN() ;
	        	codTxnString = codTxnString.substring(4,5);
	        	if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
	        		logger.debug("codTxnString:::"+codTxnString);
				
        		if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase(ExecutionConstants.YES))
				{
					if(res.getDate("DAT_POSTING").compareTo(businessdate) < 0)
					{
						dateList = (java.util.Date[]) postingDatesCache.get(res.getString("PARTICIPANT_ID"));
						accountInfoDo.setCOD_TXN(txnCode + ExecutionConstants.BVT_POST_CYCLE);
						if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
							logger.debug("2ND inside if::: dateList "+dateList);
							logger.debug("date arraylist: "+dateList[0]+" , "+dateList[3]+" and "+dateList[6]);
						}
					}
					else
					{
						dateList = (java.util.Date[]) postingDatesCacheCurr.get(res.getString("PARTICIPANT_ID"));
						accountInfoDo.setCOD_TXN(txnCode + ExecutionConstants.NORMAL_POST_CYCLE);
						if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
							logger.debug("2ND inside ELSE::: dateList "+dateList);
							logger.debug("date arraylist: "+dateList[0]+" , "+dateList[3]+" and "+dateList[6]);
						}
					}
				}
				else
				{
					//dateList = (java.util.Date[]) postingDatesCacheCurr.get(res.getString("PARTICIPANT_ID"));
					dateList = (java.util.Date[]) postingDatesCache.get(res.getString("PARTICIPANT_ID"));
				}
				
				if(accountInfoDo.getCOD_TXN().substring(4,5).equalsIgnoreCase("C"))
				{
			 		//converting util date into sql.Date
					accountInfoDo.setDAT_FINAL_SETTLEMENT(new java.sql.Date (((java.util.Date )dateList[3]).getTime()));
					accountInfoDo.setDAT_VALUE(new java.sql.Date (((java.util.Date )dateList[6]).getTime()));
				}
				else
				{
					accountInfoDo.setDAT_FINAL_SETTLEMENT(new java.sql.Date (((java.util.Date )dateList[4]).getTime()));
					accountInfoDo.setDAT_VALUE(new java.sql.Date (((java.util.Date )dateList[7]).getTime()));
				}
				//Set DAT_POSTING
				accountInfoDo.setDAT_POSTING(new java.sql.Date (((java.util.Date )dateList[0]).getTime()));
				
				//Set DAT_POSTING_START 
				//NED_ISSUE FIXED STARTS
				//accountInfoDo.setDAT_POSTING_START(new java.sql.Date (((java.util.Date )dateList[5]).getTime()));
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("DAT_POSTING_START >>>>>: "+res.getDate("DAT_POSTING_START"));
				accountInfoDo.setDAT_POSTING_START(res.getDate("DAT_POSTING_START"));
				dateList = (java.util.Date[])accrualDatesCache.get(res.getString("PARTICIPANT_ID"));
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("date arraylist: "+dateList[0]+" , "+dateList[1]+" and "+dateList[2]);
				//NED_ISSUE FIXED ENDS
				
				//Get Date_accrual,Adjusted Accrual, Accrual date start from cache	
				//Set Date_accrual
				accountInfoDo.setDAT_ACCRUAL(new java.sql.Date (((java.util.Date )dateList[0]).getTime()));
				//Set Adjusted Accrual
				accountInfoDo.setDAT_ADJUSTEDACCRUAL(new java.sql.Date (((java.util.Date )dateList[1]).getTime()));
				//Set Accrual date start
				accountInfoDo.setDAT_ACCRUAL_START(new java.sql.Date (((java.util.Date )dateList[2]).getTime()));
				
				//Set Initial Posting Date for consolidated BVT Adjustments
				accountInfoDo.setDAT_INITIAL_POSTING(res.getDate("DAT_POSTING"));
				
			}
			
			if(!recordExists)
			{
				logger.fatal("BVT QUERY : "+strQuery);
				logger.fatal(" No Transcation Found FOR ABOVE BVT QUERY");        
			}
			if(recordExists)
			{
				populateCycleWiseBVTAccuralSummaryForPool(businessdate,insertAccrualSummary,updateAccrualSummary,insertUnpostedAmt,accountInfoDo,selectAccrualSummary,updateUnpostedAmt, conn);
				populateCycleWiseBVTPostingSummaryForPool(businessdate,insertPostingSummary,updatePostingSummary,accountInfoDo,selectPostingSummary);  
				
				if(logger.isDebugEnabled())
				{
					logger.debug("*** Populating TAX LIST for... ****");
					logger.debug("typEntity :::: " + typEntity);
					logger.debug("businessDate :::: " + businessDate);
					logger.debug("postingDate :::: " + accountInfoDo.getDAT_POSTING().toString());
				}
				
			}
		}
		catch(SQLException ex)
		{
			logger.fatal("Exception in populateCycleWiseBvtAccountInfo :: ", ex);
			throw new LMSException("LMS01000","Error occurred while populating CycleWiseBvtAccount Information for BVT Id "+bvtId+" and Host System "+client+".", ex);
		}
		finally
		{
			 LMSConnectionUtility.close(res);
			 LMSConnectionUtility.close(pStmt);
	         LMSConnectionUtility.close(conn);
		}
		if (logger.isDebugEnabled()){
			logger.debug("Leaving populateCycleWiseBvtAccountInfo");
		}
	}
	//SA_BVT_POSTING_CYCLE ENDS
	// ENH_BVT_PARTY_DEL starts
	public boolean[] insertAccDelReversalEntries(String nbr_processid,String bvtId,java.util.Date businessDate, String client,
			PreparedStatement insertAccountDtls,PreparedStatement updateAccountDtls,
			PreparedStatement insertAccrualSummary,
			PreparedStatement insertPostingSummary,PreparedStatement updateAccrualSummary,
			PreparedStatement updatePostingSummary, PreparedStatement insertUnpostedAmt,
			PreparedStatement selectAccountDtls ,PreparedStatement selectAccrualSummary ,
			PreparedStatement selectPostingSummary,PreparedStatement updateUnpostedAmt,PreparedStatement updateUnpostedAmtCentralCcy)throws LMSException{//Reallocate to Self
		StringBuilder Strquery = new StringBuilder(0);

		Strquery.append("SELECT 'Y' FLG_ACC, ACC.TYP_PARTICIPANT,SUM( (-1*AMT_ACCRUED) ) amt_calculated,")
		.append(" DTLS.cod_ccy cod_ccy, acc.ID_POOL, COD_TXN, acc.NBR_OWNACCOUNT participant_id, DAT_POSTING ,")
		.append(" SOURCE_POOL_ID,FLG_POSTCYCLE,DAT_POSTING_START,DAT_FINAL_SETTLEMENT,BVT.DAT_VALUE ")
		.append(" FROM OPOOL_ACCRUALSUMMARY ACC , OPOOL_BVT_PARTICIPANTDEL_DTLS DTLS,OPOOL_BVT_PARTICIPANTDEL BVT ")
		.append(" WHERE BVT.ID_DEL = DTLS.ID_DEL AND BVT.ID_BVT = "+bvtId+" AND ACC.ID_POOL = DTLS.ID_POOL ")
		.append(" AND ACC.DAT_ACCRUAL<=BVT.DAT_BUSINESS AND ACC.DAT_ACCRUAL>=BVT.DAT_VALUE ")
		.append(" AND  NOT EXISTS (SELECT DISTINCT POST.TXT_STATUS FROM OPOOL_POSTINGSUMMARY POST WHERE POST.NBR_OWNACCOUNT = ACC.NBR_OWNACCOUNT AND POST.COD_TXN = ACC.COD_TXN AND POST.ID_POOL = ACC.ID_POOL AND ACC.DAT_ACCRUAL >= POST.DAT_POSTING_START AND ACC.DAT_ACCRUAL <= POST.DAT_POSTING AND ACC.DAT_POSTING = POST.DAT_POSTING AND POST.TXT_STATUS = 'POSTING_GEN'  ) ")
		.append(" AND ACC.TYP_PARTICIPANT = 'ACCOUNT' AND ACC.NBR_OWNACCOUNT = DTLS.participant_id group by NBR_OWNACCOUNT,cod_ccy,TYP_PARTICIPANT,COD_TXN,SOURCE_POOL_ID,acc.ID_POOL,DAT_POSTING,FLG_POSTCYCLE,DAT_POSTING_START,DAT_FINAL_SETTLEMENT,DAT_VALUE,DTLS.NBR_OWNACCOUNT_DRSETTL,DTLS.NBR_OWNACCOUNT_CRSETTL ")
		.append(" UNION ")
		.append(" SELECT 'N' FLG_ACC,ALLC.TYP_PARTICIPANT, SUM ( (-1 * ALLC.EQV_AMT_ALLOCATION) ) AMT_CALCULATED, ")
		.append(" DTLS.COD_CCY COD_CCY, ALLC.ID_POOL, POST.COD_TXN, ")
		.append(" ALLC.PARTICIPANT_ID PARTICIPANT_ID, POST.DAT_POSTING, ALLC.SOURCE_POOL_ID, ")
		.append(" FLG_POSTCYCLE, POST.DAT_POSTING_START, POST.DAT_FINAL_SETTLEMENT,")
		.append(" BVT.DAT_VALUE ")
		.append(" FROM OPOOL_TXNPOOL_ALLOCDTLS_TMP ALLC ,OPOOL_POSTINGSUMMARY POST,OPOOL_BVT_PARTICIPANTDEL_DTLS DTLS,OPOOL_BVT_PARTICIPANTDEL BVT ")
		.append(" WHERE BVT.ID_DEL = DTLS.ID_DEL AND BVT.ID_BVT = "+bvtId+" AND ALLC.ID_POOL = DTLS.ID_POOL AND ALLC.NEW_PROCESSID = "+nbr_processid)
		.append(" AND ALLC.DAT_BUSINESS_POOLRUN <= BVT.DAT_BUSINESS AND ALLC.DAT_BUSINESS_POOLRUN >= BVT.DAT_VALUE AND ALLC.TYP_PARTICIPANT = 'ACCOUNT' ")
		.append(" AND ALLC.PARTICIPANT_ID = DTLS.PARTICIPANT_ID AND POST.DAT_POSTING_START <= ALLC.DAT_BUSINESS_POOLRUN AND POST.DAT_POSTING >= ALLC.DAT_BUSINESS_POOLRUN ")
		.append(" AND ALLC.PARTICIPANT_ID = POST.NBR_OWNACCOUNT AND ALLC.COD_TXN = SUBSTR (POST.COD_TXN, 1, 5) AND SUBSTR (POST.COD_TXN, 6, 1) = 'A'")
		.append(" AND NOT EXISTS (SELECT * FROM OPOOL_ACCRUALSUMMARY ACC WHERE ACC.ID_POOL = DTLS.ID_POOL AND ACC.DAT_ACCRUAL <= BVT.DAT_BUSINESS AND ACC.DAT_ACCRUAL >= BVT.DAT_VALUE AND ACC.TYP_PARTICIPANT = 'ACCOUNT' AND ACC.NBR_OWNACCOUNT = DTLS.PARTICIPANT_ID) ")
		.append(" GROUP BY ALLC.PARTICIPANT_ID, COD_CCY, ALLC.TYP_PARTICIPANT, POST.COD_TXN, ALLC.SOURCE_POOL_ID, ALLC.ID_POOL, POST.DAT_POSTING, FLG_POSTCYCLE, DAT_POSTING_START, DAT_FINAL_SETTLEMENT, BVT.DAT_VALUE ")
		.append(" ORDER BY id_pool, participant_id, cod_txn, SOURCE_POOL_ID,dat_posting ");
		if (logger.isDebugEnabled()){
			logger.debug("SELECT QUERY FOR reversal entries :"+ Strquery.toString());
		}

		populateBvtAccountDeletionInfo(Strquery.toString(),bvtId,businessDate, client, insertAccountDtls,updateAccountDtls, 
				insertAccrualSummary, insertPostingSummary, updateAccrualSummary, updatePostingSummary,
				insertUnpostedAmt,selectAccountDtls ,selectAccrualSummary ,selectPostingSummary,updateUnpostedAmt,updateUnpostedAmtCentralCcy);//Reallocate to Self
		return entries;
	}
	private void populateBvtAccountDeletionInfo(String strQuery,String bvtId, Date businessDate, String client, 
			PreparedStatement insertAccountDtls, PreparedStatement updateAccountDtls, 
			PreparedStatement insertAccrualSummary, PreparedStatement insertPostingSummary, 
			PreparedStatement updateAccrualSummary, PreparedStatement updatePostingSummary,
			PreparedStatement insertUnpostedAmt,PreparedStatement selectAccountDtls ,
			PreparedStatement selectAccrualSummary ,PreparedStatement selectPostingSummary,PreparedStatement updateUnpostedAmt,PreparedStatement updateUnpostedAmtCentralCcy//Reallocate to Self
		) throws LMSException
	{
		if (logger.isDebugEnabled()){
			logger.debug("Entering ");
		}
		
		PreparedStatement pStmt = null;
		ExecutionUtility util=new ExecutionUtility();
		Connection conn = null;
		ResultSet res = null;
		boolean recordExists = false,firstTime = true;
		AccountInfoDO accountInfoDo = null;
		java.sql.Date businessdate = new java.sql.Date(businessDate.getTime());
		java.util.Date dateList[]= null;
		String txnCode = "";
		String typEntity="";
		try
		{
			try 
			{
				conn = LMSConnectionUtility.getConnection();
			} catch (NamingException e) 
			{
				if(logger.isFatalEnabled())
					logger.fatal(" Error occured while getting connection :"+e.getMessage());

				throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE," Error occured while getting connection : ", e);
			}
			
			if(logger.isDebugEnabled()){
				logger.debug("Query " + strQuery) ;
			}
			
			pStmt = conn.prepareStatement(strQuery);
			res = pStmt.executeQuery();
			accountInfoDo = new AccountInfoDO();
			String flag_bvt_cycle = ExecutionConstants.FLAG_BVT_CYCLE;
			if(logger.isDebugEnabled())
				logger.debug("flag_bvt_cycle:::"+flag_bvt_cycle);
	        boolean flg_newAccountingentry = ExecutionConstants.FlG_NEW_ACCOUNTING_ENTRY_FORMAT;
	        if(logger.isDebugEnabled())
	        	logger.debug("flg_newAccountingentry:::"+flg_newAccountingentry);
	        String interestTyp = null ;// ENH_10.3_153
			String strPoolId = null ;
			String source_PoolId = null ; 
	        
			while(res.next())
			{
				//txnCode = setTxnCode(res.getString("COD_TXN"),res.getString("TXN_TYPE"));
				if(logger.isDebugEnabled())
					logger.debug("txnCode::: is >>> "+txnCode);
				recordExists = true;
				//When  frist time control enters into loop ,just populate Do object  
				typEntity=res.getString("TYP_PARTICIPANT");
				txnCode = res.getString("COD_TXN");
				strPoolId =res.getString("ID_POOL");
				source_PoolId =res.getString("SOURCE_POOL_ID"); 

				if(!firstTime)
				{

					if(!strPoolId.equalsIgnoreCase("NONPOOL_ACCT") 
							&& !typEntity.equalsIgnoreCase(InterCoConstants.INCOLOAN)) 
									
					{
						if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y"))
						{
							if(res.getDate("DAT_POSTING").compareTo(businessdate) < 0)
							{
								dateList = (java.util.Date[]) postingDatesCache.get(res.getString("PARTICIPANT_ID"));
								if(accountInfoDo.getCOD_TXN().length()<7)	
									txnCode = txnCode + ExecutionConstants.BVT_POST_CYCLE;
							}
							else
							{
								dateList = (java.util.Date[]) postingDatesCacheCurr.get(res.getString("ID_POOL"));
								if(accountInfoDo.getCOD_TXN().length()<7)	
									txnCode = txnCode + ExecutionConstants.NORMAL_POST_CYCLE;
							}                  
						}
					}

					if(logger.isDebugEnabled())
						logger.debug("FLG_SA_INTEREST_TYPE : "  + ExecutionConstants.FLG_SA_INTEREST_TYPE + "strPoolId" + strPoolId) ;
					
					
					
					if (logger.isDebugEnabled())
						logger.debug("txnCode "  + txnCode + "accountInfoDo.getCOD_TXN() " + " PARTICIPANT_ID "+res.getString("PARTICIPANT_ID")+ "accountInfoDo.getPARTICIPANT_ID() " + " strPoolId "  + strPoolId +  "accountInfoDo.getID_POOL() "+ accountInfoDo.getID_POOL() + " source_PoolId" + source_PoolId + " SOURCE_POOL_ID() "+ accountInfoDo.getSOURCE_POOL_ID() ) ;
					
					if(txnCode.equalsIgnoreCase(accountInfoDo.getCOD_TXN()) &&
									res.getString("PARTICIPANT_ID").equalsIgnoreCase(accountInfoDo.getPARTICIPANT_ID())
									&& strPoolId.equalsIgnoreCase(accountInfoDo.getID_POOL())
									&& source_PoolId.equalsIgnoreCase(accountInfoDo.getSOURCE_POOL_ID()) //NEW_ACCOUNTING_ENTRY_FORMAT
									)
					{
						if (logger.isDebugEnabled())
						{
							logger.debug("accountInfoDo.getPARTICIPANT_ID()" +accountInfoDo.getPARTICIPANT_ID());
							logger.debug("accountInfoDo.getCOD_TXN()"+accountInfoDo.getCOD_TXN());
							logger.debug("accountInfoDo.getID_POOL()" +accountInfoDo.getID_POOL());
						}
						accountInfoDo.setDAT_BUSINESS(businessdate);
						BigDecimal amtCalc = res.getBigDecimal("AMT_CALCULATED");
						if(res.getString("FLG_ACC")!=null && res.getString("FLG_ACC").equals("N")){
							BigDecimal[] precision_carryfwd  = util.getTruncatedAmt(amtCalc,ExecutionConstants.ROUNDOFF_ACCRUAL_POSTING,res.getString("COD_CCY"), conn);
							amtCalc = precision_carryfwd[0];
						}
						accountInfoDo.setAMT_CALCULATED(amtCalc);
						if (logger.isDebugEnabled()){
							logger.debug("res.getBigDecimal(AMT_CALCULATED)" +res.getBigDecimal("AMT_CALCULATED"));
							logger.debug("accountInfoDo.getAMT_CALCULATED " +accountInfoDo.getAMT_CALCULATED());
						}
						//Set AMT_CAL_SUM = AMT_CAL_SUM + AMT_CALCULATED
						accountInfoDo.setAMT_CAL_SUM(amtCalc.add(accountInfoDo.getAMT_CAL_SUM()));
						//CAll populateBvtAccountDtls()
						if (logger.isDebugEnabled()){
							logger.debug("accountInfoDo.getAMT_CAL_SUM " +accountInfoDo.getAMT_CAL_SUM());
						}						
						
						populateBvtAccountDtls(businessDate, client, insertAccountDtls, updateAccountDtls,accountInfoDo,selectAccountDtls);
						continue ;
					}
					else
					{
						if (logger.isDebugEnabled()){
							logger.debug("inside else for accural and posting code txn is " +accountInfoDo.getCOD_TXN());
						}						

						populateBVTAccuralSummary(businessdate,insertAccrualSummary,updateAccrualSummary,insertUnpostedAmt,accountInfoDo,selectAccrualSummary,updateUnpostedAmt, updateUnpostedAmtCentralCcy, conn);//Reallocate to Self

						populateBVTPostingSummary(businessdate,insertPostingSummary,updatePostingSummary,accountInfoDo,selectPostingSummary);
						
						
						

						
					}
				}
				firstTime = false;
				//Set resultset data  into Data Object
				accountInfoDo.clearObject();
				accountInfoDo.setDAT_CURRENT_BUSINESS(businessdate);
				accountInfoDo.setDAT_BUSINESS(businessdate);
				BigDecimal amtCalc = res.getBigDecimal("AMT_CALCULATED");
				if(res.getString("FLG_ACC")!=null && res.getString("FLG_ACC").equals("N")){
					BigDecimal[] precision_carryfwd  = util.getTruncatedAmt(amtCalc,ExecutionConstants.ROUNDOFF_ACCRUAL_POSTING,res.getString("COD_CCY"), conn);
					amtCalc = precision_carryfwd[0];
				}
				accountInfoDo.setAMT_CALCULATED(amtCalc);
				accountInfoDo.setAMT_CAL_SUM(amtCalc);
				accountInfoDo.setPARTICIPANT_ID(res.getString("PARTICIPANT_ID"));
				accountInfoDo.setID_POOL(res.getString("ID_POOL"));
				//accountInfoDo.setFLG_REALPOOL(res.getString("FLG_REALPOOL")); // TODO see if used
				//REPLACE LAST CHAR OF  THE TNX CODE BY C OR D

				txnCode = res.getString("COD_TXN");

				accountInfoDo.setCOD_TXN(txnCode);
				accountInfoDo.setCOD_CCY(res.getString("COD_CCY"));
				accountInfoDo.setTYP_PARTICIPANT(res.getString("TYP_PARTICIPANT"));
						
				accountInfoDo.setFLG_REALPOSTING("NA");
				accountInfoDo.setFLG_POOL_CONSOLIDATED("NA");
				accountInfoDo.setFLG_ACC_CONSOLIDATED("NA");

				String flgPosting= res.getString("FLG_POSTCYCLE");
				
				if(logger.isDebugEnabled())
					logger.debug(" back valued deletion with flg_postcycle >> "+ flgPosting);				
				
				if(flgPosting ==null)
					flgPosting = "N";
				accountInfoDo.setFLG_POSTCYCLE(flgPosting);

				//Get DAT_FINAL_SETTLEMENT,DAT_VALUE, DAT_POSTING_START ,DAT_POSTING_START from cache
				if(logger.isDebugEnabled())
					logger.debug("Inside If condition codTransaction Value -->"+accountInfoDo.getCOD_TXN());
	        	String codTxnString = accountInfoDo.getCOD_TXN() ;

	        	codTxnString = codTxnString.substring(4,5);
	        	if(logger.isDebugEnabled())
	        		logger.debug("codTxnString:::"+codTxnString);
				
	        	if(flgPosting.equals("Y") ){
		
					if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y"))
					{
						if(res.getDate("DAT_POSTING").compareTo(businessdate) < 0)
						{
							dateList = (java.util.Date[]) postingDatesCache.get(res.getString("PARTICIPANT_ID"));
							if(accountInfoDo.getCOD_TXN().length()<7)
								accountInfoDo.setCOD_TXN(txnCode + ExecutionConstants.BVT_POST_CYCLE);
						}
						else
						{
							dateList = (java.util.Date[]) postingDatesCacheCurr.get(res.getString("ID_POOL"));
							if(accountInfoDo.getCOD_TXN().length()<7)	
								accountInfoDo.setCOD_TXN(txnCode + ExecutionConstants.NORMAL_POST_CYCLE);
						}
					}
					else
					{
						dateList = (java.util.Date[]) postingDatesCacheCurr.get(res.getString("ID_POOL"));
					}
					
					accountInfoDo.setDAT_FINAL_SETTLEMENT(res.getDate("DAT_FINAL_SETTLEMENT"));
					accountInfoDo.setDAT_VALUE(res.getDate("DAT_VALUE"));
					
					//Set DAT_POSTING
					//accountInfoDo.setDAT_POSTING(new java.sql.Date (((java.util.Date )dateList[0]).getTime()));
					accountInfoDo.setDAT_POSTING(res.getDate("DAT_POSTING"));
					//Set DAT_POSTING_START 
					//accountInfoDo.setDAT_POSTING_START(new java.sql.Date (((java.util.Date )dateList[5]).getTime()));
					accountInfoDo.setDAT_POSTING_START(res.getDate("DAT_POSTING_START"));
	
					
						dateList = (java.util.Date[])accrualDatesCache.get(res.getString("ID_POOL"));
					
					if(logger.isDebugEnabled())
						logger.debug("date arraylist: "+dateList[0].getTime()+" , "+dateList[1].getTime()+" and "+dateList[2].getTime());
	
	
					//Get Date_accrual,Adjusted Accrual, Accrual date start from cache	
					//Set Date_accrual
					accountInfoDo.setDAT_ACCRUAL(new java.sql.Date (((java.util.Date )dateList[0]).getTime()));
					//Set Adjusted Accrual
					accountInfoDo.setDAT_ADJUSTEDACCRUAL(new java.sql.Date (((java.util.Date )dateList[1]).getTime()));
					//Set Accrual date start
					accountInfoDo.setDAT_ACCRUAL_START(new java.sql.Date (((java.util.Date )dateList[2]).getTime()));
	        	}
	        	else{
	        		if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y"))
					{
						if(accountInfoDo.getCOD_TXN().length()<7)	
							accountInfoDo.setCOD_TXN(txnCode + ExecutionConstants.NORMAL_POST_CYCLE);
					}
	        		
	        		accountInfoDo.setDAT_FINAL_SETTLEMENT(businessdate);
					accountInfoDo.setDAT_VALUE(res.getDate("DAT_VALUE"));
					accountInfoDo.setDAT_POSTING(businessdate);
					
					//dateList = (java.util.Date[]) postingDatesCacheCurr.get(res.getString("ID_POOL"));
					
					accountInfoDo.setDAT_POSTING_START(res.getDate("DAT_POSTING_START"));
					accountInfoDo.setDAT_ACCRUAL(businessdate);
					dateList = (java.util.Date[])accrualDatesCache.get(res.getString("ID_POOL"));
					accountInfoDo.setDAT_ADJUSTEDACCRUAL(businessdate);
					accountInfoDo.setDAT_ACCRUAL_START(new java.sql.Date (((java.util.Date )dateList[2]).getTime()));
	        	}

				if(logger.isDebugEnabled()){
					logger.debug("txn Code : " + accountInfoDo.getCOD_TXN()) ;
					logger.debug("FLG_SA_INTEREST_TYPE : "  + ExecutionConstants.FLG_SA_INTEREST_TYPE + "strPoolId" + strPoolId) ;
				}
				
				
				
				if(logger.isDebugEnabled())
					logger.debug("txn Code is >>>>>>>> :::: " + accountInfoDo.getCOD_TXN()) ;
				
				
				accountInfoDo.setSOURCE_POOL_ID(res.getString("SOURCE_POOL_ID")); 
				
				if(logger.isDebugEnabled())
					logger.debug(" accountInfoDo pass to populateBvtAccountDtls " + accountInfoDo.toString()) ;
				//CAll populateBvtAccountDtls()				
				populateBvtAccountDtls(businessDate, client, insertAccountDtls, updateAccountDtls,accountInfoDo,selectAccountDtls);
					
			} // end of while loop
			
			if(!recordExists && logger.isFatalEnabled())
			{
				logger.fatal("BVT QUERY : "+strQuery);
				logger.fatal(" No Transcation Found FOR ABOVE BVT QUERY");        
			}
			else if(recordExists)
			{
				populateBVTAccuralSummary(businessdate,insertAccrualSummary,updateAccrualSummary,insertUnpostedAmt,accountInfoDo,selectAccrualSummary,updateUnpostedAmt, updateUnpostedAmtCentralCcy, conn);//Reallocate to Self
				
				
				populateBVTPostingSummary(businessdate,insertPostingSummary,updatePostingSummary,accountInfoDo,selectPostingSummary);  
			}
		
			

		}
		catch(SQLException ex)
		{
			if(logger.isFatalEnabled())
			{
				logger.fatal("Exception :: ", ex);
			}	
			
			
			throw new LMSException("LMS01000","Error occurred while populating BvtAccount Information for BVT Id "+bvtId+" and Host System "+client+".", ex);
			
		}
		finally
		{
			 LMSConnectionUtility.close(res);
			 
			 LMSConnectionUtility.close(pStmt);

	         LMSConnectionUtility.close(conn);
		}
		if (logger.isDebugEnabled()){
			logger.debug("Leaving");
		}
	}
	// ENH_BVT_PARTY_DEL ends
	//SUP_ISS_III_7305 start
	public boolean[] generateCycleWiseBVTAccountSummaryInfoForICL(
			String bvtId, Date businessDate, String hostId,
			PreparedStatement insertAccountDtls,
			PreparedStatement updateAccountDtls,
			PreparedStatement insertAccrualSummary,
			PreparedStatement insertPostingSummary,
			PreparedStatement updateAccrualSummary,
			PreparedStatement updatePostingSummary,
			PreparedStatement insertUnpostedAmt,
			PreparedStatement selectAccountDtls,
			PreparedStatement selectAccrualSummary,
			PreparedStatement selectPostingSummary,
			PreparedStatement updateUnpostedAmt)throws LMSException {
		if (logger.isDebugEnabled()) {
			logger.debug("Entering generateCycleWiseBVTAccountSummaryInfoForICL");
		}

		try {
			processCycleWiseBvtTXNCodesForICL(bvtId, businessDate, hostId,
					insertAccountDtls, updateAccountDtls, insertAccrualSummary,
					insertPostingSummary, updateAccrualSummary,
					updatePostingSummary, insertUnpostedAmt, selectAccountDtls,
					selectAccrualSummary, selectPostingSummary,
					updateUnpostedAmt);
		}

		catch (Exception e) {
			logger.fatal("General Exception: ", e);

			// ENH_IS1059 Starts
			// throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE,
			// e.getMessage());
			throw new LMSException("LMS01000",
					"Error occurred while generating BVTAccount Summary Information for BVT Id "
							+ bvtId + " and Host System " + hostId + ".", e);
			// ENH_IS1059 Ends
		}
		/*
		 * entries will be set true in
		 * insertAccountDtls(),populateAccrualSummary() and populatePosting
		 * Summary indicates corresponding preparedStatement need to be executed
		 * in processAccountingEntries() method in LotProcessorSBSLBean.
		 */
		if (logger.isDebugEnabled()) {
			logger.debug("Leaving generateCycleWiseBVTAccountSummaryInfoForICL");
		}
		return entries;
	}
	//SUP_ISS_III_7305 end
	//SUP_ISS_III_7305 start
	private void processCycleWiseBvtTXNCodesForICL(String bvtId,
			Date businessDate, String hostId,
			PreparedStatement insertAccountDtls,
			PreparedStatement updateAccountDtls,
			PreparedStatement insertAccrualSummary,
			PreparedStatement insertPostingSummary,
			PreparedStatement updateAccrualSummary,
			PreparedStatement updatePostingSummary,
			PreparedStatement insertUnpostedAmt,
			PreparedStatement selectAccountDtls,
			PreparedStatement selectAccrualSummary,
			PreparedStatement selectPostingSummary,
			PreparedStatement updateUnpostedAmt)throws LMSException {
		if (logger.isDebugEnabled()) {
			logger.debug("Entering processCycleWiseBvtTXNCodesForICL");
		}
		boolean allowIntTyp = false; // Sandeep changes for ned issue
		StringBuffer Strquery = new StringBuffer(0);
		Strquery.append(" SELECT tbl.typ_entity, ");
		Strquery.append(" ( CASE ");
		Strquery.append(" WHEN (SUBSTR (hdr.cod_txn, 5, 1) IN ('C', 'B')) ");
		Strquery.append(" THEN amt_cr_interest_diff ");
		Strquery.append(" 		ELSE 0 ");
		Strquery.append(" END ) amt_calculated          , ");
		Strquery.append(" 		currency cod_ccy              , ");
		Strquery.append(" tbl.dat_business busdt, ");
		Strquery.append(" hdr.cod_txn ");
		Strquery
				.append(" ||'I' COD_TXN                                               , ");
		Strquery
				.append(" tbl.cod_entity participant_id, incoloan.cod_incoid id_pool  , ");
		Strquery
				.append(" 'CREDIT' txn_type, 'N' FLG_REALPOOL                         , ");
		Strquery
				.append(" tbl.dat_posting                                             , ");
		Strquery
				.append(" NVL((hdr.DAT_LASTPOSTING+1),hdr.DAT_start) DAT_POSTING_START, ");
		Strquery.append(" tbl.typ_interest, incoloan.dat_loanend dat_loanend ");
		Strquery.append(" ,postingcycle.flg_samevaldat flg_samevaldat,hdr.dat_post_creditdelay dat_post_creditdelay,hdr.dat_post_debitdelay dat_post_debitdelay");
		Strquery.append(" FROM olm_txnaccount_bvtdiff_int tbl, ");
		Strquery.append(" olm_glmst gl                       , ");
		Strquery.append(" olm_txnaccount_interestinfo hdr , olm_inco_loan_details incoloan");
		Strquery.append(" ,orate_posting_cycle postingcycle ");
		Strquery.append(" WHERE tbl.id_bvt           = " + bvtId);
		Strquery.append(" and postingcycle.cod_post_cycle=hdr.cod_def_post_cycle ");
		Strquery.append(" AND hdr.typ_entity          = 'INCOLOAN' ");
		Strquery.append(" and hdr.typ_entity = tbl.typ_entity ");
		Strquery.append(" and hdr.typ_interest = tbl.typ_interest ");
		Strquery.append(" AND tbl.cod_gl               = '" + hostId + "' ");
		Strquery.append(" AND amt_cr_interest_diff    <> 0 ");
		Strquery.append(" AND tbl.cod_gl               = gl.cod_gl ");
		Strquery.append(" AND tbl.cod_entity              = hdr.cod_entity ");
		Strquery.append(" AND tbl.NBR_PROCESSID        = hdr.NBR_PROCESSID ");
		Strquery.append(" AND tbl.DAT_BUSINESS = hdr.DAT_BUSINESS ");
		Strquery.append(" and incoloan.cod_incoloanid = hdr.cod_entity ");
		Strquery.append(" ORDER BY tbl.cod_entity,       ");
		Strquery.append(" tbl.cod_txn, ");
		Strquery.append(" tbl.dat_posting, tbl.typ_entity ,tbl.dat_business ");

		if (logger.isDebugEnabled()) {
			logger
					.debug("SELECT QUERY1 FOR CREDIT IN processCycleWiseBvtTXNCodes :"
							+ Strquery.toString());
		}
		populateCycleWiseBvtAccountInfoForICL(Strquery.toString(), bvtId,
				businessDate, hostId, insertAccountDtls, updateAccountDtls,
				insertAccrualSummary, insertPostingSummary,
				updateAccrualSummary, updatePostingSummary, insertUnpostedAmt,
				selectAccountDtls, selectAccrualSummary, selectPostingSummary,
				updateUnpostedAmt);

		Strquery.replace(0, Strquery.length(), "");

		Strquery.append(" SELECT tbl.typ_entity, ");
		Strquery.append(" ( CASE ");
		Strquery.append(" WHEN (SUBSTR (hdr.cod_txn, 5, 1) IN ('D', 'B')) ");
		Strquery.append(" THEN amt_dr_interest_diff ");
		Strquery.append(" 		ELSE 0 ");
		Strquery.append(" END ) amt_calculated          , ");
		Strquery.append(" 		currency cod_ccy              , ");
		Strquery.append(" tbl.dat_business busdt, ");
		Strquery.append(" hdr.cod_txn ");
		Strquery
				.append(" ||'I' COD_TXN                                               , ");
		Strquery
				.append(" tbl.cod_entity participant_id, incoloan.cod_incoid id_pool  , ");
		Strquery
				.append(" 'DEBIT' txn_type, 'N' FLG_REALPOOL                          , ");
		Strquery
				.append(" tbl.dat_posting                                             , ");
		Strquery
				.append(" NVL((hdr.DAT_LASTPOSTING+1),hdr.DAT_start) DAT_POSTING_START, ");
		Strquery
				.append(" tbl.typ_interest, incoloan.dat_loanend dat_loanend  ");
		Strquery.append(" ,postingcycle.flg_samevaldat flg_samevaldat,hdr.dat_post_creditdelay dat_post_creditdelay,hdr.dat_post_debitdelay dat_post_debitdelay");
		Strquery.append(" FROM olm_txnaccount_bvtdiff_int tbl, ");
		Strquery.append(" olm_glmst gl                       , ");
		Strquery.append(" olm_txnaccount_interestinfo hdr  , olm_inco_loan_details incoloan");
		Strquery.append(" ,orate_posting_cycle postingcycle ");
		Strquery.append(" WHERE tbl.id_bvt           = " + bvtId);
		Strquery.append(" and postingcycle.cod_post_cycle=hdr.cod_def_post_cycle ");
		Strquery.append(" AND hdr.typ_entity          = 'INCOLOAN' ");
		Strquery.append(" and hdr.typ_entity = tbl.typ_entity ");
		Strquery.append(" and hdr.typ_interest = tbl.typ_interest ");
		Strquery.append(" AND tbl.cod_gl               = '" + hostId + "' ");
		Strquery.append(" AND amt_dr_interest_diff    <> 0 ");
		Strquery.append(" AND tbl.cod_gl               = gl.cod_gl ");
		Strquery.append(" AND tbl.cod_entity              = hdr.cod_entity ");
		Strquery.append(" AND tbl.NBR_PROCESSID        = hdr.NBR_PROCESSID ");
		Strquery.append(" AND tbl.DAT_BUSINESS = hdr.DAT_BUSINESS ");
		Strquery.append(" and incoloan.cod_incoloanid = hdr.cod_entity ");
		Strquery.append(" ORDER BY tbl.cod_entity,        ");
		Strquery.append(" tbl.cod_txn,  ");
		Strquery.append(" tbl.dat_posting, tbl.typ_entity,tbl.dat_business  ");

		if (logger.isDebugEnabled()) {
			logger
					.debug("SELECT QUERY2 FOR DEBIT IN processCycleWiseBvtTXNCodes :"
							+ Strquery.toString());
		}
		populateCycleWiseBvtAccountInfoForICL(Strquery.toString(), bvtId,
				businessDate, hostId, insertAccountDtls, updateAccountDtls,
				insertAccrualSummary, insertPostingSummary,
				updateAccrualSummary, updatePostingSummary, insertUnpostedAmt,
				selectAccountDtls, selectAccrualSummary, selectPostingSummary,
				updateUnpostedAmt);

		if (logger.isDebugEnabled()) {
			logger.debug("Leaving processCycleWiseBvtTXNCodesForICL");
		}
		
	}
	//SUP_ISS_III_7305 end
	//SUP_ISS_III_7305 start
	private void populateCycleWiseBvtAccountInfoForICL(String strQuery, String bvtId,
			Date businessDate, String hostId,
			PreparedStatement insertAccountDtls,
			PreparedStatement updateAccountDtls,
			PreparedStatement insertAccrualSummary,
			PreparedStatement insertPostingSummary,
			PreparedStatement updateAccrualSummary,
			PreparedStatement updatePostingSummary,
			PreparedStatement insertUnpostedAmt,
			PreparedStatement selectAccountDtls,
			PreparedStatement selectAccrualSummary,
			PreparedStatement selectPostingSummary,
			PreparedStatement updateUnpostedAmt)throws LMSException {
		if (logger.isDebugEnabled()) {
			logger.debug("Entering populateCycleWiseBvtAccountInfoForICL");
		}
		PreparedStatement pStmt = null;
		Connection conn = null;
		ResultSet res = null;
		boolean recordExists = false, firstTime = true;
		boolean isCurrCycle = false;
		AccountInfoDO accountInfoDo = null;
		java.sql.Date businessdate = new java.sql.Date(businessDate.getTime());
		java.util.Date dateList[] = null;
		String txnCode = "";
		String typEntity = "";
		BigDecimal amtPostedDec = ZERO;

		boolean isupdt = false;
		try {
			try {
				conn = LMSConnectionUtility.getConnection();
			} catch (NamingException e) {
				logger.fatal(" Error occured while getting connection :"+ e.getMessage());
			}
			pStmt = conn.prepareStatement(strQuery);
			res = pStmt.executeQuery();
			accountInfoDo = new AccountInfoDO();
			String flag_bvt_cycle = ExecutionConstants.FLAG_BVT_CYCLE;
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("flag_bvt_cycle:::" + flag_bvt_cycle);

			while (res.next()) {

				isCurrCycle = false;

				txnCode = setTxnCode(res.getString("COD_TXN"), res.getString("TXN_TYPE"));
				recordExists = true;
				typEntity = res.getString("TYP_ENTITY");
				accountInfoDo.setTYP_INTEREST(res.getString("typ_interest"));

				if (!firstTime) {
					if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
						logger.debug("businessdate :: " + businessdate );
						logger.debug("res.getDate(DAT_POSTING) :: " + res.getDate("DAT_POSTING") );
					}
					if (ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase(ExecutionConstants.YES)) {
						logger.debug("inside 1st if");
						if (res.getDate("DAT_POSTING").compareTo(businessdate) < 0) {
							dateList = (java.util.Date[]) postingDatesCache.get(res.getString("PARTICIPANT_ID"));
							txnCode = txnCode + ExecutionConstants.BVT_POST_CYCLE;
						} else {
							dateList = (java.util.Date[]) postingDatesCacheCurr.get(res.getString("PARTICIPANT_ID"));
							txnCode = txnCode + ExecutionConstants.NORMAL_POST_CYCLE;
						}
						int len = dateList.length;
						logger.debug("****~~~~~~~~~~~~~~~~~~~~~~~~~****");
						for(int i = 0; i < len; i++){
							if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
								logger.debug("dateList["+i+"] :: " + dateList[i]);
						}
						logger.debug("****~~~~~~~~~~~~~~~~~~~~~~~~~****");
					}
					if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
						logger.debug("txnCode :: " + txnCode);
						logger.debug("accountInfoDo.getCOD_TXN() :: " + accountInfoDo.getCOD_TXN());
						logger.debug("res.getString(PARTICIPANT_ID) :: " + res.getString("PARTICIPANT_ID"));
						logger.debug("accountInfoDo.getPARTICIPANT_ID() :: " + accountInfoDo.getPARTICIPANT_ID());
						logger.debug("res.getDate(DAT_POSTING) :: " + res.getDate("DAT_POSTING"));
						logger.debug("accountInfoDo.getDAT_INITIAL_POSTING() :: " + accountInfoDo.getDAT_INITIAL_POSTING());
					}
					
					if (res.getString("PARTICIPANT_ID").equalsIgnoreCase(accountInfoDo.getPARTICIPANT_ID())
							&& txnCode.equalsIgnoreCase(accountInfoDo.getCOD_TXN())
							&& res.getDate("DAT_POSTING").equals(accountInfoDo.getDAT_INITIAL_POSTING())) {
						if (logger.isDebugEnabled()) {
							logger.debug("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
							logger.debug("accountInfoDo.getPARTICIPANT_ID()" + accountInfoDo.getPARTICIPANT_ID());
							logger.debug("accountInfoDo.getCOD_TXN()" + accountInfoDo.getCOD_TXN());
							logger.debug("accountInfoDo.getID_POOL()" + accountInfoDo.getID_POOL());
						}
						accountInfoDo.setDAT_BUSINESS(res.getDate("BUSDT"));
						if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
							logger.debug("res.getDate(BUSDT):: " + res.getDate("BUSDT"));
							//logger.debug("value of cal sum1 for the second time before setting::::"+ accountInfoDo.getAMT_CAL_SUM1());
							logger.debug("value of res.getBigDecimalAMT_CALCULATED for the second time before setting::::"+ res.getBigDecimal("AMT_CALCULATED"));
						}
						// Defect#16170 Starts
						//accountInfoDo.setAMT_CAL_SUM1(res.getBigDecimal("AMT_CALCULATED").add(accountInfoDo.getAMT_CAL_SUM1()));
						//logger.debug("value of cal sum1 for the second time after setting::::"+ accountInfoDo.getAMT_CAL_SUM1());
						// Defect#16170 Ends
						accountInfoDo.setAMT_CALCULATED(res.getBigDecimal("AMT_CALCULATED"));
						if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
							logger.debug("accountInfoDo.getAMT_CAL_SUM()::::" + accountInfoDo.getAMT_CAL_SUM());
							logger.debug("accountInfoDo.getAMT_CALCULATED()::::" + accountInfoDo.getAMT_CALCULATED());
						}
						if (logger.isDebugEnabled()) {
							logger.debug("res.getBigDecimal(AMT_CALCULATED)" + res.getBigDecimal("AMT_CALCULATED"));
							logger.debug("accountInfoDo.getAMT_CALCULATED " + accountInfoDo.getAMT_CALCULATED());
						}
						// Set AMT_CAL_SUM = AMT_CAL_SUM + AMT_CALCULATED
						
						//if(accountInfoDo.getDAT_INITIAL_POSTING().compareTo(accountInfoDo.getDAT_POSTING())==0){
						// isCurrCycle=true; 
						// logger.debug("EnteringisCurrCycle:::"+isCurrCycle); 
						// }
						//if (!isCurrCycle){
						 accountInfoDo.setAMT_CAL_SUM(res.getBigDecimal("AMT_CALCULATED").add(accountInfoDo.getAMT_CAL_SUM())); //}
						 
						// CAll populateBvtAccountDtls()
						 if (logger.isDebugEnabled()) {//Changes for ENH_15.2_LOG_FIXES
							 logger.debug("accountInfoDo.getAMT_CAL_SUM " + accountInfoDo.getAMT_CAL_SUM());


							 logger.debug("accountInfoDo.getDAT_INITIAL_POSTING()"+ accountInfoDo.getDAT_INITIAL_POSTING());
							 logger.debug("accountInfoDo.getDAT_POSTING()"+ accountInfoDo.getDAT_POSTING());
							 // if(accountInfoDo.getDAT_INITIAL_POSTING().after(accountInfoDo.getDAT_POSTING())){

							 logger.debug("Inserting into accrual summary for current cycle :: "+ isCurrCycle);
							 // Defect#16170 Starts isupdt variable is passed to keep track of cycle change
							 logger.debug("******************start1*******************");
							 //populateCycleWiseBVTAccuralSummaryForICL(businessdate,insertAccrualSummary, updateAccrualSummary,insertUnpostedAmt, accountInfoDo,selectAccrualSummary, updateUnpostedAmt, conn,isupdt);
							 logger.debug("*******************end1******************");
						 }
						continue;
					} else {
						isupdt = false;
						 //accountInfoDo.setAMT_CAL_SUM1(res.getBigDecimal("AMT_CALCULATED"));
						// Defect#16170 Starts isupdt variable is passed to keep track of cycle change
						logger.debug("******************start2*******************");
						populateCycleWiseBVTAccuralSummaryForICL(businessdate,insertAccrualSummary, updateAccrualSummary,insertUnpostedAmt, accountInfoDo,selectAccrualSummary, updateUnpostedAmt, conn,isupdt);
						populateCycleWiseBVTPostingSummaryForICL(businessdate,insertPostingSummary, updatePostingSummary,accountInfoDo, selectPostingSummary);
						logger.debug("*******************end2******************");
						if (logger.isDebugEnabled()) {
							logger.debug(" **** Populating TAX LIST for.... ****");
							logger.debug("typEntity :::: " + typEntity);
							logger.debug("businessDate :::: " + businessDate);
							logger.debug("postingDate :::: "+ accountInfoDo.getDAT_POSTING().toString());
						}
					}
				}
				firstTime = false;
				// Set resultset data into Data Object
				accountInfoDo.clearObject();
				accountInfoDo.setDAT_CURRENT_BUSINESS(businessdate);
				accountInfoDo.setDAT_BUSINESS(res.getDate("BUSDT"));
				accountInfoDo.setAMT_CALCULATED(res.getBigDecimal("AMT_CALCULATED"));
				accountInfoDo.setAMT_CAL_SUM(res.getBigDecimal("AMT_CALCULATED"));
				accountInfoDo.setCOD_CCY(res.getString("COD_CCY"));
				if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
					logger.debug("accountInfoDo.setDAT_CURRENT_BUSINESS :: " + accountInfoDo.getDAT_CURRENT_BUSINESS());
					logger.debug("accountInfoDo.setDAT_BUSINESS :: " + accountInfoDo.getDAT_BUSINESS());
					logger.debug("accountInfoDo.setAMT_CALCULATED :: " + accountInfoDo.getAMT_CALCULATED());
					logger.debug("accountInfoDo.setAMT_CAL_SUM :: " + accountInfoDo.getAMT_CAL_SUM());
				}
				// Defect#16170 Starts 
				if (!isupdt) {
                 //JPMC_312 Postin Adjustment Start
					accountInfoDo.setAMT_TO_BE_POSTED(ZERO);
					//JPMC_312 Postin Adjustment  end 
					isupdt = true;
				}
				
				//logger.debug("value of cal sum1 for the first time before setting::::"+ accountInfoDo.getAMT_CAL_SUM1());
				
				//logger.debug("value of cal sum1 for the first time after setting::::"+ accountInfoDo.getAMT_CAL_SUM1());
				// Defect#16170 Ends 
				
				accountInfoDo.setPARTICIPANT_ID(res.getString("PARTICIPANT_ID"));
				accountInfoDo.setID_POOL(res.getString("ID_POOL"));
				accountInfoDo.setFLG_REALPOOL(res.getString("FLG_REALPOOL"));
				// REPLACE LAST CHAR OF THE TNX CODE BY C OR D
				txnCode = setTxnCode(res.getString("COD_TXN"), res.getString("TXN_TYPE"));
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("txnCode #### " + txnCode);
				accountInfoDo.setCOD_TXN(txnCode);

				accountInfoDo.setTYP_PARTICIPANT(res.getString("TYP_ENTITY"));

				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("Inside If condition codTransaction Value -->" + accountInfoDo.getCOD_TXN());

				String codTxnString = accountInfoDo.getCOD_TXN();
				codTxnString = codTxnString.substring(4, 5);
				if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
					logger.debug("codTxnString:::" + codTxnString);
					logger.debug("participant Id :::" + res.getString("PARTICIPANT_ID"));
					logger.debug(" Posting date cache :: " + postingDatesCache);
				}

				if (ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase(ExecutionConstants.YES)) {
					logger.debug("inside if");
					if (res.getDate("DAT_POSTING").compareTo(businessdate) < 0) {
						dateList = (java.util.Date[]) postingDatesCache.get(res.getString("PARTICIPANT_ID"));
						accountInfoDo.setCOD_TXN(txnCode + ExecutionConstants.BVT_POST_CYCLE);
					} else {
						dateList = (java.util.Date[]) postingDatesCacheCurr.get(res.getString("PARTICIPANT_ID"));
						accountInfoDo.setCOD_TXN(txnCode + ExecutionConstants.NORMAL_POST_CYCLE);
					}
					int len = dateList.length;
					logger.debug("****~~~~~~~~~~~~~~~~~~~~~~~~~****");
					for(int i = 0; i < len; i++){
						if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
							logger.debug("dateList["+i+"] :: " + dateList[i]);
					}
					logger.debug("****~~~~~~~~~~~~~~~~~~~~~~~~~****");
				} else {
					if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
						logger.debug("inside else");
						logger.debug("dateList@@@$$$$" + postingDatesCache.get(res.getString("PARTICIPANT_ID")));
					}
					dateList = (java.util.Date[]) postingDatesCache.get(res.getString("PARTICIPANT_ID"));
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("dateList@@@" + dateList);
					int len = dateList.length;
					logger.debug("****~~~~~~~~~~~~~~~~~~~~~~~~~****");
					for(int i = 0; i < len; i++){
						if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
							logger.debug("dateList["+i+"] :: " + dateList[i]);
					}
					logger.debug("****~~~~~~~~~~~~~~~~~~~~~~~~~****");
				}
				if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
					logger.debug("dat_post_creditdelay :: " +res.getString("dat_post_creditdelay"));
					logger.debug("dat_post_debitdelay :: " +res.getString("dat_post_debitdelay"));
				}
				if (accountInfoDo.getCOD_TXN().substring(4, 5).equalsIgnoreCase("C")) {
					if(logger.isInfoEnabled()){//Changes for ENH_15.2_LOG_FIXES
						logger.info("dateList[3]).getTime()"+ dateList[3].getTime());
						logger.info("dateList[6]).getTime()"+ dateList[6].getTime());
					}
					// converting util date into sql.Date
					accountInfoDo.setDAT_FINAL_SETTLEMENT(new java.sql.Date(((java.util.Date) dateList[3]).getTime()));
					accountInfoDo.setDAT_VALUE(new java.sql.Date(((java.util.Date) dateList[6]).getTime()));
					accountInfoDo.setDAT_INITIAL_VALUE(res.getDate("dat_post_creditdelay"));
					if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
						logger.debug("accountInfoDo.setDAT_FINAL_SETTLEMENT :: " +accountInfoDo.getDAT_FINAL_SETTLEMENT());
						logger.debug("accountInfoDo.setDAT_VALUE :: " +accountInfoDo.getDAT_VALUE());
					}
				} else {
					accountInfoDo.setDAT_FINAL_SETTLEMENT(new java.sql.Date(((java.util.Date) dateList[4]).getTime()));
					accountInfoDo.setDAT_VALUE(new java.sql.Date(((java.util.Date) dateList[7]).getTime()));
					accountInfoDo.setDAT_INITIAL_VALUE(res.getDate("dat_post_debitdelay"));
					if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
						logger.debug("accountInfoDo.setDAT_FINAL_SETTLEMENT :: " +accountInfoDo.getDAT_FINAL_SETTLEMENT());
						logger.debug("accountInfoDo.setDAT_VALUE :: " +accountInfoDo.getDAT_VALUE());
					}
				}
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("flg_samevaldat :: " +res.getString("flg_samevaldat"));
				if(res.getString("flg_samevaldat").equalsIgnoreCase(ExecutionConstants.NO)){
					GregorianCalendar temp = new GregorianCalendar();
					temp.setTime(res.getDate("DAT_POSTING"));
					temp.add(Calendar.DATE, 1);
					accountInfoDo.setDAT_INITIAL_VALUE(new java.sql.Date(temp.getTime().getTime()));
				}
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("accountInfoDo.setDAT_VALUE :: " +accountInfoDo.getDAT_VALUE());
				// Set DAT_POSTING
				if(logger.isInfoEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.info("dateList[6]" + dateList[6].getTime());
				accountInfoDo.setDAT_POSTING(new java.sql.Date(((java.util.Date) dateList[0]).getTime()));
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("accountInfoDo.setDAT_POSTING :: " + accountInfoDo.getDAT_POSTING());
				// Set DAT_POSTING_START
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("DAT_POSTING_START >>>>>: "+ res.getDate("DAT_POSTING_START"));
				accountInfoDo.setDAT_POSTING_START(res.getDate("DAT_POSTING_START"));
				dateList = (java.util.Date[]) accrualDatesCache.get(res.getString("PARTICIPANT_ID"));
				
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("date arraylist: " + dateList[0] + " , " + dateList[1] + " and " + dateList[2]);
				// NED_ISSUE FIXED ENDS

				// Get Date_accrual,Adjusted Accrual, Accrual date start from
				// cache
				// Set Date_accrual
				accountInfoDo.setDAT_ACCRUAL(new java.sql.Date(((java.util.Date) dateList[0]).getTime()));
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("accountInfoDo.setDAT_ACCRUAL :: " + accountInfoDo.getDAT_ACCRUAL());
				// Set Adjusted Accrual
				accountInfoDo.setDAT_ADJUSTEDACCRUAL(new java.sql.Date(((java.util.Date) dateList[1]).getTime()));
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("accountInfoDo.setDAT_ADJUSTEDACCRUAL :: " + accountInfoDo.getDAT_ADJUSTEDACCRUAL());
				// Set Accrual date start
				accountInfoDo.setDAT_ACCRUAL_START(res.getDate("BUSDT"));
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("accountInfoDo.setDAT_ACCRUAL_START :: " + accountInfoDo.getDAT_ACCRUAL_START());
				// Set Initial Posting Date for consolidated BVT Adjustments
				accountInfoDo.setDAT_INITIAL_POSTING(res.getDate("DAT_POSTING"));
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("accountInfoDo.setDAT_INITIAL_POSTING :: " + accountInfoDo.getDAT_INITIAL_POSTING());
				accountInfoDo.setPARTY_DAT_END(res.getDate("dat_loanend"));
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("accountInfoDo.setPARTY_DAT_END :: " + accountInfoDo.getPARTY_DAT_END() );
			}

			if (!recordExists) {
				logger.fatal("BVT QUERY : " + strQuery);
				logger.fatal(" No Transcation Found FOR ABOVE BVT QUERY");
			}
			if (recordExists) {
				if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
					logger.debug("----------------------------------");
					logger.debug("isCurrCycle :: " + isCurrCycle);
				}
				// Defect#16170 Starts isupdt variable is passed to keep track of cycle change
				//populateCycleWiseBVTAccuralSummaryForICL(businessdate,insertAccrualSummary, updateAccrualSummary,insertUnpostedAmt, accountInfoDo, selectAccrualSummary,updateUnpostedAmt, conn,isupdt);
				//populateCycleWiseBVTPostingSummaryForICL(businessdate,insertPostingSummary, updatePostingSummary,accountInfoDo, selectPostingSummary);
				populateCycleWiseBVTAccuralSummaryForICL(businessdate,insertAccrualSummary,updateAccrualSummary,insertUnpostedAmt,accountInfoDo,selectAccrualSummary,updateUnpostedAmt, conn,isupdt);
				populateCycleWiseBVTPostingSummaryForICL(businessdate,insertPostingSummary,updatePostingSummary,accountInfoDo,selectPostingSummary);  
				
				if (logger.isDebugEnabled()) {
					logger.debug("*** Populating TAX LIST for2... ****");
					logger.debug("typEntity :::: " + typEntity);
					logger.debug("businessDate :::: " + businessDate);
					logger.debug("postingDate :::: " + accountInfoDo.getDAT_POSTING().toString());
				}

			}

		} catch (Exception ex) {
			logger.fatal("Exception in populateCycleWiseBvtAccountInfo :: ",
							ex);
			throw new LMSException("LMS01000","Error occurred while populating CycleWiseBvtAccount Information for BVT Id "+ bvtId + " and Host System " + hostId + ".", ex);

		} finally {
			LMSConnectionUtility.close(res);
			LMSConnectionUtility.close(pStmt);
			LMSConnectionUtility.close(conn);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("Leaving populateCycleWiseBvtAccountInfoForICL");
		}
	}
	//SUP_ISS_III_7305 end
	//SUP_ISS_III_7305 start
	private void populateCycleWiseBVTPostingSummaryForICL(java.sql.Date businessDate,
			PreparedStatement insertPostingSummary,
			PreparedStatement updatePostingSummary,
			AccountInfoDO accountInfoDo, PreparedStatement selectPostingSummary)throws LMSException {
		if (logger.isDebugEnabled()) {
			logger.debug("Entering populateCycleWiseBVTPostingSummaryForICL");
		}

		BigDecimal amtAccuredNotPosted = ZERO;
		BigDecimal amountToBePosted = ZERO;
		BigDecimal postedAmount = ZERO;
		boolean cycleStart = true;
		boolean flgPostingToBeDone = false;
		// Identify the Posting cycle start
		cycleStart = checkRecordInPostingSummary_BVTForICL(accountInfoDo,
				selectPostingSummary, businessDate);
		amtAccuredNotPosted = accountInfoDo.getAMT_ACCRUEDNOTPOSTED();
		postedAmount = accountInfoDo.getAMT_POSTED();

		amountToBePosted = accountInfoDo.getAMT_TO_BE_POSTED();

		if (logger.isDebugEnabled()) {
			logger.debug("businessDate " + businessDate.toString());
			logger.debug("DAT_POSTING "
					+ accountInfoDo.getDAT_POSTING().toString());
			logger.debug("accountInfoDo.getCOD_TXN()"
					+ accountInfoDo.getCOD_TXN());
		}

		// if cod_txn is is normal adjustment the
		// ie - RCIBCI "N" SCI - that is sevent letter of COD_TXN - no_posting
		// else RCIBCI "B" SCI that is sevent letter of COD_TXN is B then
		// POSTING_GEN

		// AS per ned requirement date_busines is equal to posting then update
		// status as NOPOSTING_STATUS nitin
		// Ned_issue_70 fix - if condition modified

		if (ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase(ExecutionConstants.YES)) {
			if (logger.isDebugEnabled()) {
				logger.debug("Inside FLAG_BVT_CYCLE Yes");
			}

			if ((accountInfoDo.getCOD_TXN().substring(6, 7).equalsIgnoreCase("B")) || 
					((accountInfoDo.getCOD_TXN().substring(6, 7).equalsIgnoreCase("N")) && 
							businessDate.compareTo(accountInfoDo.getDAT_POSTING()) > 0)) {
				flgPostingToBeDone = true;
			} else
				flgPostingToBeDone = false;

			if (logger.isDebugEnabled()) {
				logger.debug("flgPostingToBeDone inside FLAG_BVT_CYCLE Yes : "
						+ flgPostingToBeDone);
			}

		} else {
			if (logger.isDebugEnabled()) {
				logger.debug("Inside FLAG_BVT_CYCLE No");
			}

			if (businessDate.compareTo(accountInfoDo.getDAT_POSTING()) > 0) {
				flgPostingToBeDone = true;
			} else
				flgPostingToBeDone = false;

			if (logger.isDebugEnabled()) {
				logger.debug("flgPostingToBeDone inside FLAG_BVT_CYCLE No : "
						+ flgPostingToBeDone);
			}
		}

		flgPostingToBeDone = checkPostingToBeDone(new java.sql.Date(
				businessDate.getTime()), accountInfoDo);
		if (flgPostingToBeDone
				&& businessDate.compareTo(accountInfoDo.getDAT_POSTING()) > 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("POSTING Cycle END ");
			}
			if (logger.isDebugEnabled()) {
				logger.debug("BEFORE amtAccuredNotPosted : "
						+ amtAccuredNotPosted);
				logger.debug("amountToBePosted :  " + amountToBePosted);
			}
			// Set AMT_POSTED
			// postedAmount=amtAccuredNotPosted + amountToBePosted
			postedAmount = postedAmount.add(amtAccuredNotPosted
					.add(amountToBePosted));
			if (logger.isDebugEnabled()) {
				logger.debug("postedAmount: " + postedAmount);
			}
			accountInfoDo.setAMT_POSTED(postedAmount);
			// Resetting AMT_ACCRUEDNOTPOSTED to 0
			accountInfoDo.setAMT_ACCRUEDNOTPOSTED(ZERO);
			// status = POST_GEN
			accountInfoDo
					.setTXT_STATUS(ExecutionConstants.POSTING_GENERATED_STATUS);
		} else {
			if (logger.isDebugEnabled()) {
				logger.debug("POSTING Cycle NOT END ");
				logger.debug("BEFORE amtAccuredNotPosted : "
						+ amtAccuredNotPosted);
				logger.debug("amountToBePosted :  " + amountToBePosted);

			}
			// amtAccuredNotPosted = amtAccuredNotPosted + amountToBePosted;
			amtAccuredNotPosted = amtAccuredNotPosted.add(amountToBePosted);
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("AFETER amtAccuredNotPosted : " + amtAccuredNotPosted);
			accountInfoDo.setAMT_ACCRUEDNOTPOSTED(amtAccuredNotPosted);
			// Resetting AMT_POSTED to 0
			accountInfoDo.setAMT_POSTED(ZERO);
			// status = NO_ACC
			accountInfoDo.setTXT_STATUS(ExecutionConstants.NOPOSTING_STATUS);

		}

		if (logger.isDebugEnabled()) {
			logger.debug(" DATA OBJECT DETAILS " + accountInfoDo.toString());
		}
		if (!cycleStart) {
			if (logger.isDebugEnabled()) {
				logger.debug("INSERT DATA OBJECT DETAILS "
						+ accountInfoDo.toString());
			}
			// accountInfoDo.setAMT_ACCRUEDNOTPOSTED(0D);
			insertPostingSummary_BVTForICL(insertPostingSummary, accountInfoDo);
			entries[4] = true;
		} else {
			if (logger.isDebugEnabled()) {
				logger.debug("UPDATE DATA OBJECT DETAILS "
						+ accountInfoDo.toString());
			}
			updatePostingSummary_BVTForICL(updatePostingSummary, accountInfoDo);
			entries[5] = true;
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Leaving populateCycleWiseBVTPostingSummaryForICL");
		}
		
	}
	//SUP_ISS_III_7305 end
	//SUP_ISS_III_7305 start
	private boolean checkRecordInPostingSummary_BVTForICL(
			AccountInfoDO accountInfoDo,
			PreparedStatement selectPostingSummary, java.sql.Date businessDate) throws LMSException{

		if (logger.isDebugEnabled()) {
			logger.debug("Entering checkRecordInPostingSummary_BVTForICL");
			logger.debug(" DATA OBJECT DETAILS " + accountInfoDo.toString());
		}

		ResultSet res = null;
		boolean found = false;
		try {
			if (logger.isDebugEnabled()) {
				logger.debug("COD_TXN in checkRecordInPostingSummary_BVT "
						+ accountInfoDo.getCOD_TXN());
				logger.debug("getPARTICIPANT_ID >>> "
						+ accountInfoDo.getPARTICIPANT_ID());
				logger.debug("getCOD_TXN()>>>" + accountInfoDo.getCOD_TXN());
				logger.debug("getDAT_POSTING_START()>>>"
						+ accountInfoDo.getDAT_POSTING_START());
				logger.debug("getID_POOL()>>>" + accountInfoDo.getID_POOL());
				logger.debug("getTYP_PARTICIPANT()>>>>"
						+ accountInfoDo.getTYP_PARTICIPANT());
				logger.debug("getDAT_INITIAL_POSTING()>>>"
						+ accountInfoDo.getDAT_INITIAL_POSTING());
				logger.debug("getDAT_POSTING()>>>"
						+ accountInfoDo.getDAT_POSTING());
				logger.debug("getDAT_BUSINESS()>>>" + businessDate);
			}
			boolean allowIntTyp = ExecutionConstants.FLG_INTACC_POST_INTTYP; // Changes
																				// done
																				// for
																				// Unique
																				// key
																				// exception

			String codTxn = null;
			if (allowIntTyp) // Sandeep changes for ned issue
			{
				if ("I".equalsIgnoreCase(accountInfoDo.getTYP_INTEREST()))
					codTxn = accountInfoDo.getCOD_TXN();
				else
					codTxn = accountInfoDo.getCOD_TXN()
							+ accountInfoDo.getTYP_INTEREST();
			} else {
				codTxn = accountInfoDo.getCOD_TXN();
			}
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("codTxn : " + codTxn);
			selectPostingSummary
					.setString(1, accountInfoDo.getPARTICIPANT_ID());
			selectPostingSummary.setString(2, codTxn);
			selectPostingSummary.setDate(3, LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING_START()));
			selectPostingSummary.setString(4, accountInfoDo.getID_POOL());
			selectPostingSummary.setString(5, accountInfoDo
					.getTYP_PARTICIPANT());
			selectPostingSummary.setDate(6, LMSUtility.truncSqlDate(accountInfoDo.getDAT_INITIAL_POSTING()));
			// Ned_issue_70 fix starts
			selectPostingSummary.setDate(7,LMSUtility.truncSqlDate( (java.sql.Date) businessDate));
			selectPostingSummary.setString(8, "NO_POSTING");
			// Ned_issue_70 ends
			res = selectPostingSummary.executeQuery();
			while (res.next()) {
				found = true;
				accountInfoDo.setAMT_ACCRUEDNOTPOSTED(res
						.getBigDecimal("AMT_ACCRUEDNOTPOSTED"));
				accountInfoDo.setAMT_POSTED(res.getBigDecimal("AMT_POSTED"));
				accountInfoDo.setOLD_DAT_FINAL_SETTLEMENT(res
						.getDate("DAT_FINAL_SETTLEMENT"));
				accountInfoDo.setOLD_DAT_POSTING(res.getDate("DAT_POSTING"));

				if (logger.isDebugEnabled()) {
					logger.debug("IN DB amtAccuredNotPosted "
							+ res.getDouble("AMT_ACCRUEDNOTPOSTED"));
					logger.debug("IN DB AMT_POSTED "
							+ res.getDouble("AMT_POSTED"));
				}
			}
		} catch (Exception e) {
			logger.fatal("Exception in checkRecordInPostingSummary_BVTForICL :: ", e);
			// ENH_IS1059 Starts
			// throw new LMSException("Error occured in
			// checkRecordInPostigSummary", e.toString());
			throw new LMSException("LMS01000",
					"Record already exist in OPOOL_POSTINGSUMMARY_BVT table for Transaction Id "
							+ accountInfoDo.getCOD_TXN() + ", Pool Id :- "
							+ accountInfoDo.getID_POOL() + ", Participant :- "
							+ accountInfoDo.getPARTICIPANT_ID()
							+ " and Participant Type :"
							+ accountInfoDo.getTYP_PARTICIPANT() + ".", e);
			// ENH_IS1059 Ends
		} finally {
			LMSConnectionUtility.close(res);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("Leaving checkRecordInPostingSummary_BVTForICL");
		}
		return found;
	

	}
	//SUP_ISS_III_7305 end
	//SUP_ISS_III_7305 start
	private void updatePostingSummary_BVTForICL(
			PreparedStatement updatePostingSummary, AccountInfoDO accountInfoDo) throws LMSException {

		if (logger.isDebugEnabled()) {
			logger.debug("Entering updatePostingSummary_BVTForICL>>>"
					+ accountInfoDo.getCOD_TXN() + ","
					+ accountInfoDo.getTYP_INTEREST());
		}
		boolean allowIntTyp = ExecutionConstants.FLG_INTACC_POST_INTTYP; //Sandeep changes for ned issue
		String codTxn = null;
		try {
			if (!(accountInfoDo.getID_POOL()
					.equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT))) {
				logger.debug("calling function chkHoldReq() ");
				if (!(accountInfoDo.getTYP_PARTICIPANT()
						.equalsIgnoreCase(ExecutionConstants.INCO))) {
					boolean holdreq = chkPendingHoldReq(accountInfoDo);
					if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
					logger.debug("holdreq -- " + holdreq);
					}
					if (holdreq) {
						logger
								.debug("Hold req. is present in updatePostingSummary");
						accountInfoDo.setTXT_STATUS("HOLD");
					}
				}
			}

			updatePostingSummary.setBigDecimal(1, accountInfoDo
					.getAMT_ACCRUEDNOTPOSTED());
			updatePostingSummary
					.setBigDecimal(2, accountInfoDo.getAMT_POSTED());
			updatePostingSummary.setString(3, accountInfoDo.getTXT_STATUS());
			updatePostingSummary.setDate(4,LMSUtility.truncSqlDate( accountInfoDo.getDAT_VALUE()));
			updatePostingSummary.setDate(5, LMSUtility.truncSqlDate(accountInfoDo.getDAT_FINAL_SETTLEMENT()));
			updatePostingSummary.setDate(6,LMSUtility.truncSqlDate( accountInfoDo.getDAT_POSTING()));
			updatePostingSummary
					.setString(
							7,
							com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT
									.format(new java.util.Date()));
			updatePostingSummary.setDate(8, LMSUtility.truncSqlDate(accountInfoDo.getDAT_CURRENT_BUSINESS()));
			//SETTING WHRER CLAUSE PLACE HOLDERS
			updatePostingSummary.setDate(9,LMSUtility.truncSqlDate( accountInfoDo.getDAT_INITIAL_VALUE()));
			updatePostingSummary
					.setString(10, accountInfoDo.getPARTICIPANT_ID());
			if (allowIntTyp) //Sandeep changes for ned issue
			{
				if ("I".equalsIgnoreCase(accountInfoDo.getTYP_INTEREST()))
					codTxn = accountInfoDo.getCOD_TXN();
				else
					codTxn = accountInfoDo.getCOD_TXN()
							+ accountInfoDo.getTYP_INTEREST();
			} else {
				codTxn = accountInfoDo.getCOD_TXN();
			}

			updatePostingSummary.setString(11, codTxn);
			updatePostingSummary.setDate(12, LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING_START()));
			updatePostingSummary.setString(13, accountInfoDo.getID_POOL());
			updatePostingSummary.setString(14, accountInfoDo.getTYP_PARTICIPANT());
			updatePostingSummary.setDate(15, LMSUtility.truncSqlDate(accountInfoDo.getDAT_INITIAL_POSTING()));
			updatePostingSummary.setDate(16, LMSUtility.truncSqlDate(accountInfoDo.getDAT_CURRENT_BUSINESS()));
			updatePostingSummary.setString(17, "NO_POSTING");
			updatePostingSummary.addBatch();
		} catch (SQLException e) {
			logger.fatal("Exception in updatePostingSummary_BVTForICL :: ", e);
			throw new LMSException(
					"LMS01000",
					"Updation of  posting summary in OPOOL_POSTINGSUMMARY_BVT table failed for Transaction Id  :- "
							+ accountInfoDo.getCOD_TXN()
							+ " Pool Id  :- "
							+ accountInfoDo.getID_POOL()
							+ " Participant Id :- "
							+ accountInfoDo.getPARTICIPANT_ID()
							+ " and Type Participant Type is "
							+ accountInfoDo.getTYP_PARTICIPANT() + ".", e);
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Leaving updatePostingSummary_BVTForICL");
		}

	
		
	}
	//SUP_ISS_III_7305 end
	//SUP_ISS_III_7305 start
	private void insertPostingSummary_BVTForICL(
			PreparedStatement insertPostingSummary, AccountInfoDO accountInfoDo) throws LMSException{

		if (logger.isDebugEnabled()) {
			logger.debug("Entering insertPostingSummary_BVTForICL >>>"
					+ accountInfoDo.getCOD_TXN() + ","
					+ accountInfoDo.getTYP_INTEREST());
		}
		boolean allowIntTyp = ExecutionConstants.FLG_INTACC_POST_INTTYP; //Sandeep changes for ned issue
		String codTxn = null;
		try {
			if (!(accountInfoDo.getID_POOL()
					.equalsIgnoreCase(ExecutionConstants.NONPOOL_ACCT))) {
				if (!(accountInfoDo.getTYP_PARTICIPANT()
						.equalsIgnoreCase(ExecutionConstants.INCO))) {
					boolean holdreq = chkPendingHoldReq(accountInfoDo);
					if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
						logger.debug("holdreq -- " + holdreq);
					if (holdreq) {
						logger
								.debug("Hold req. is present in populate posting summary");
						accountInfoDo.setTXT_STATUS("HOLD");
					}
				}
			}

			insertPostingSummary.setString(1, accountInfoDo.getID_POOL());
			insertPostingSummary
					.setString(2, accountInfoDo.getPARTICIPANT_ID());
			if (allowIntTyp) //Sandeep changes for ned issue
			{
				if ("I".equalsIgnoreCase(accountInfoDo.getTYP_INTEREST()))
					codTxn = accountInfoDo.getCOD_TXN();
				else
					codTxn = accountInfoDo.getCOD_TXN()
							+ accountInfoDo.getTYP_INTEREST();
			} else {
				codTxn = accountInfoDo.getCOD_TXN();
			}
			insertPostingSummary.setString(3, codTxn);
			insertPostingSummary.setDate(4, LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING_START()));
			insertPostingSummary.setDate(5,LMSUtility.truncSqlDate( accountInfoDo.getDAT_POSTING()));
			insertPostingSummary.setDate(6, LMSUtility.truncSqlDate(accountInfoDo.getDAT_FINAL_SETTLEMENT()));

			insertPostingSummary.setBigDecimal(7, accountInfoDo
					.getAMT_ACCRUEDNOTPOSTED());
			insertPostingSummary
					.setBigDecimal(8, accountInfoDo.getAMT_POSTED());
			insertPostingSummary.setString(9, accountInfoDo.getTXT_STATUS());
			insertPostingSummary.setDate(10,LMSUtility.truncSqlDate( accountInfoDo.getDAT_VALUE()));
			insertPostingSummary.setString(11, accountInfoDo
					.getTYP_PARTICIPANT());
			insertPostingSummary
					.setString(
							12,
							com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT
									.format(new java.util.Date()));
			insertPostingSummary.setDate(13, LMSUtility.truncSqlDate(accountInfoDo.getDAT_CURRENT_BUSINESS()));
			insertPostingSummary.setDate(14, LMSUtility.truncSqlDate(accountInfoDo.getDAT_INITIAL_POSTING()));
			insertPostingSummary.setDate(15, LMSUtility.truncSqlDate(accountInfoDo.getDAT_INITIAL_VALUE()));
			insertPostingSummary.addBatch();
		} catch (SQLException e) {
			logger.fatal("Exception in insertPostingSummary_BVTForICL :: ", e);
			throw new LMSException(
					"LMS01000",
					"Insertion of  posting summary in OPOOL_POSTINGSUMMARY_BVT table failed for Transaction Id  :- "
							+ accountInfoDo.getCOD_TXN()
							+ ", Pool Id  :- "
							+ accountInfoDo.getID_POOL()
							+ ", Participant Id :- "
							+ accountInfoDo.getPARTICIPANT_ID()
							+ " and Type Participant Type is "
							+ accountInfoDo.getTYP_PARTICIPANT() + ".", e);
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Leaving insertPostingSummary_BVTForICL");
		}
	
		
	}
	//SUP_ISS_III_7305 end
	//SUP_ISS_III_7305 start
	private void populateCycleWiseBVTAccuralSummaryForICL(java.sql.Date businessDate,
			PreparedStatement insertAccrualSummary,
			PreparedStatement updateAccrualSummary,
			PreparedStatement insertUnpostedAmt, AccountInfoDO accountInfoDo,
			PreparedStatement selectAccrualSummary,
			PreparedStatement updateUnpostedAmt, Connection conn, boolean isupdt)throws LMSException {
		if (logger.isDebugEnabled()) {
			logger.debug("Entering populateCycleWiseBVTAccuralSummaryForICL");
		}
		boolean cycleStart = true;
		boolean isCurrCycle = false;

		if (accountInfoDo.getDAT_INITIAL_POSTING().compareTo(
				accountInfoDo.getDAT_POSTING()) == 0) {
			isCurrCycle = true;
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("Entering isCurrCycle:::" + isCurrCycle);
		}

		BigDecimal amtAccrued = ZERO;
		BigDecimal amt_CarryFwd = ZERO; // LS212_Defect_15055
		BigDecimal amtUnaccrued = ZERO;
		BigDecimal calculatedAmount = ZERO;

		BigDecimal amtFromUnPosted = ZERO;
		String key = "";
		String codTxn = "";
		BigDecimal amtAccruedPrevious = ZERO;
		BigDecimal amtToBePosted = ZERO;
		BigDecimal amtPostedDec = ZERO;

		// Identify the Accrual cycle start
		cycleStart = checkRecordInAccrualSummary_BVTForICL(accountInfoDo,
				selectAccrualSummary, businessDate);

		calculatedAmount = accountInfoDo.getAMT_CAL_SUM();
		amtUnaccrued = accountInfoDo.getAMT_UNACCRUED();
		if (logger.isDebugEnabled()) {
			logger.debug("amtUnaccrued" + amtUnaccrued);
		}
		amtAccrued = accountInfoDo.getAMT_ACCRUED();
		amtAccruedPrevious = amtAccrued;
        
		if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
			logger.debug("businessDate : " + businessDate + " || DAT_ACCRUAL : "
					+ accountInfoDo.getDAT_ACCRUAL());
		if (businessDate.compareTo(accountInfoDo.getDAT_ACCRUAL()) >= 0) {

			if (logger.isDebugEnabled()) {
				logger.debug("BVT ACCRUAL CYCLE END ");
			}

			// get unposted amount from cache
			codTxn = accountInfoDo.getCOD_TXN();
			key = accountInfoDo.getID_POOL() + ExecutionConstants.SEPARATOR
					+ accountInfoDo.getPARTICIPANT_ID()
					+ ExecutionConstants.SEPARATOR + codTxn;

			if (logger.isDebugEnabled()) {
				logger.debug("key unposted" + key);
			}
			if (cacheAmtUnpostedtemp.containsKey(key)) {
				if (logger.isDebugEnabled()) {
					logger.debug(" unposted");
				}
				amtFromUnPosted = (BigDecimal) cacheAmtUnpostedtemp.get(key);
				cacheAmtUnpostedtemp.remove(key);
			}

			if (logger.isDebugEnabled()) {
				logger.debug("amtFromUnPosted " + amtFromUnPosted);
			}

			calculatedAmount = calculatedAmount.add(amtFromUnPosted);
			if (logger.isDebugEnabled()) {
				logger.debug("calculatedAmount after adding unposted"
						+ calculatedAmount);
			}

			// Apply Caryforward logic to amtAccrued
			ExecutionUtility util = new ExecutionUtility();
			BigDecimal[] precision_carryfwd = { ZERO, ZERO };

			accountInfoDo.setAMT_UNACCRUED(ZERO);
			// Set Amount Accured = AmtAccrued+calculated Amount
			amtAccrued = amtAccrued.add(calculatedAmount);

			precision_carryfwd = util.getTruncatedAmt(amtAccrued,
					ExecutionConstants.ACCRUAL, accountInfoDo.getCOD_CCY());
			// precision_carryfwd=util.getTruncatedAmt(amtAccrued,"INTEREST",accountInfoDo.getCOD_CCY());

			amtAccrued = precision_carryfwd[0];
			amt_CarryFwd = precision_carryfwd[1]; // LS212_Defect_15055
			if (logger.isDebugEnabled()) {
				logger.debug("Truncated amtAccrued" + amtAccrued);
				logger.debug("Carry Forward Amount " + precision_carryfwd[1]);
			}

			//JPMC_312 Postin Adjustment Start
			if(logger.isDebugEnabled()){//Changes for ENH_15.2_LOG_FIXES
				logger.debug("amtToBePosted :::::changes done for subtract0  :" +accountInfoDo.getAMT_TO_BE_POSTED());
				logger.debug("amtToBePosted :::::changes done for subtract1  :" +amtAccrued.subtract(amtAccruedPrevious));
			}
			amtPostedDec = accountInfoDo.getAMT_TO_BE_POSTED().add(amtAccrued.subtract(amtAccruedPrevious));
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("amtToBePosted :::::changes done for subtract2  :" + amtPostedDec);
			//JPMC_312 Postin Adjustment END
			// LS212_Defect_15055 start
			cacheAmtUnpostedtemp.put(key, amt_CarryFwd);
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("cacheAmtUnpostedtemp:::" + cacheAmtUnpostedtemp);

			// LS212_Defect_15055 end
			accountInfoDo.setAMT_ACCRUED(amtAccrued);
			// Set Amt to be posted
            
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("amtAccrued = " + amtAccrued + " amtAccruedPrevious"
						+ amtAccruedPrevious);
						/*
			if (!isupdt) {
				amtPostedDec = ZERO;
				isupdt = true;
				amtPostedDec = amtPostedDec.add(accountInfoDo.getAMT_ACCRUED());
				logger.debug("amtToBePosted :::::" + amtPostedDec);
				
			}
			else{
				amtPostedDec = accountInfoDo.getAMT_TO_BE_POSTED().add(accountInfoDo.getAMT_ACCRUED());
				logger.debug("amtToBePosted :::::" + amtPostedDec);
				
			}
			
						
			/*BigDecimal[] precision_carryfwd1 = { ZERO, ZERO };
			amtToBePosted = accountInfoDo.getAMT_CAL_SUM1();
			logger.debug("amtToBePosted :::::" + amtToBePosted);
			precision_carryfwd1 = util.getTruncatedAmt(amtToBePosted,
					ExecutionConstants.ACCRUAL, accountInfoDo.getCOD_CCY());
			amtToBePosted = precision_carryfwd1[0];
			logger.debug("amtToBePosted :::::" + amtToBePosted);
			if (logger.isDebugEnabled()) {
				logger.debug("Truncated amtToBePosted" + amtToBePosted);
				logger.debug("Carry Forward amtToBePosted "
						+ precision_carryfwd1[1]);

			}*/
	
			// Defect#16170 Ends 
			//JPMC_312 Postin Adjustment end
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("amtAccrued = " + amtAccrued + " amtAccruedPrevious"
						+ amtAccruedPrevious + " amtToBePosted = " + amtPostedDec);
			accountInfoDo.setAMT_TO_BE_POSTED(amtPostedDec);
			accountInfoDo
					.setTXT_STATUS(ExecutionConstants.ACCRUAL_GENERATED_STATUS);

		} else {
			if (logger.isDebugEnabled()) {
				logger.debug("ACCRUAL CYCLE NOT  END ");
			}
			// Set Amount Accured
			accountInfoDo.setAMT_ACCRUED(ZERO);
			// Set AmtUnccrued = amtUnaccrued + calcuted amt
			accountInfoDo.setAMT_UNACCRUED(amtUnaccrued.add(calculatedAmount));
			accountInfoDo.setTXT_STATUS(ExecutionConstants.NOACCRUAL_STATUS);
		}
		if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
			logger.debug(" cycleStart:::: " + cycleStart + "isCurrCycle ::::"
					+ isCurrCycle);
		if (!cycleStart) {
			if (logger.isDebugEnabled()) {
				logger.debug(" insertAccrualSummary_BVT DATA OBJECT DETAILS "
						+ accountInfoDo.toString());
			}
			insertAccrualSummary_BVTForICL(insertAccrualSummary, accountInfoDo);
			entries[2] = true;
		} else {
			if (logger.isDebugEnabled()) {
				logger.debug(" updateAccrualSummary_BVT DATA OBJECT DETAILS "
						+ accountInfoDo.toString());
			}
			updateAccrualSummary_BVTForICL(updateAccrualSummary, accountInfoDo);
			entries[3] = true;
		}
		if (logger.isDebugEnabled()) {
			logger.debug("Leaving populateCycleWiseBVTAccuralSummaryForICL");
		}
		
	}
	//SUP_ISS_III_7305 end
	//SUP_ISS_III_7305 start
	private boolean checkRecordInAccrualSummary_BVTForICL(
			AccountInfoDO accountInfoDo,
			PreparedStatement selectAccrualSummary, java.sql.Date businessDate)throws LMSException {


		ResultSet res = null;
		boolean found = false;
		if (logger.isDebugEnabled()) {
			logger.debug("Entering checkRecordInAccrualSummary_BVTForICL");
			logger.debug(" DATA OBJECT DETAILS " + accountInfoDo.toString());
		}

		try {
			boolean allowIntTyp = ExecutionConstants.FLG_SA_INTEREST_TYPE;  // Changes done for Unique key
			// exception

			String codTxn = null;
			if (allowIntTyp) // Sandeep changes for ned issue
			{
				if ("I".equalsIgnoreCase(accountInfoDo.getTYP_INTEREST()))
					codTxn = accountInfoDo.getCOD_TXN();
				else
					codTxn = accountInfoDo.getCOD_TXN()
							+ accountInfoDo.getTYP_INTEREST();
			} else {
				codTxn = accountInfoDo.getCOD_TXN();
			}

			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug(" codTxn: " + codTxn);
			selectAccrualSummary
					.setString(1, accountInfoDo.getPARTICIPANT_ID());
			selectAccrualSummary.setString(2, codTxn);
			selectAccrualSummary.setDate(3, LMSUtility.truncSqlDate(accountInfoDo.getDAT_ACCRUAL_START()));
			selectAccrualSummary.setString(4, accountInfoDo.getID_POOL());
			selectAccrualSummary.setString(5, accountInfoDo.getTYP_PARTICIPANT());
			selectAccrualSummary.setDate(6, LMSUtility.truncSqlDate(accountInfoDo.getDAT_INITIAL_POSTING()));
			//selectAccrualSummary.setDate(7,LMSUtility.truncSqlDate( (java.sql.Date) businessDate));

			res = selectAccrualSummary.executeQuery();
			while (res.next()) {
				found = true;

				accountInfoDo.setAMT_UNACCRUED(res
						.getBigDecimal("AMT_UNACCRUED"));
				accountInfoDo.setOLD_DAT_ACCRUAL(res.getDate("DAT_ACCRUAL"));
				accountInfoDo.setOLD_DAT_ADJUSTEDACCRUAL(res
						.getDate("DAT_ACCRUAL_ADJUSTED"));
				accountInfoDo.setAMT_ACCRUED(res.getBigDecimal("AMT_ACCRUED"));
				if (logger.isDebugEnabled()) {
					logger.debug("IN DB amtUnaccured "
							+ res.getDouble("AMT_UNACCRUED"));
					logger.debug("IN DB amtUnaccured "
							+ res.getDouble("AMT_ACCRUED"));
				}
			}

		} catch (SQLException e) {
			logger.fatal("Exception in checkRecordInAccrualSummary_BVTForICL :: ", e);
			throw new LMSException("LMS01000","Record already exist in OPOOL_ACCRUALSUMMARY_BVT table for Transaction Id "+accountInfoDo.getCOD_TXN()+", Pool Id :-"+accountInfoDo.getID_POOL()+", Participant :- "+accountInfoDo.getPARTICIPANT_ID()+" and Participant Type :"+accountInfoDo.getTYP_PARTICIPANT()+".", e);
		}finally
		{
	        LMSConnectionUtility.close(res);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("Leaving checkRecordInAccrualSummary_BVTForICL");
		}
		return found;

	
	}
	//SUP_ISS_III_7305 end
	//SUP_ISS_III_7305 start
	private void updateAccrualSummary_BVTForICL(
			PreparedStatement updateAccountSummary, AccountInfoDO accountInfoDo) throws LMSException{
		if (logger.isDebugEnabled()) {
			logger.debug("Entering updateAccrualSummary_BVTForICL"
					+ accountInfoDo.getCOD_TXN() + ","
					+ accountInfoDo.getTYP_INTEREST());
		}
		boolean allowIntTyp = ExecutionConstants.FLG_SA_INTEREST_TYPE; // Sandeep changes for ned issue; 
		String codTxn = null;
		try {
			updateAccountSummary.setBigDecimal(1, accountInfoDo.getAMT_UNACCRUED());
			updateAccountSummary.setBigDecimal(2, accountInfoDo.getAMT_ACCRUED());
			updateAccountSummary.setString(3, accountInfoDo.getTXT_STATUS());
			updateAccountSummary.setDate(4,LMSUtility.truncSqlDate( accountInfoDo.getDAT_ADJUSTEDACCRUAL()));
			updateAccountSummary.setDate(5,LMSUtility.truncSqlDate( accountInfoDo.getDAT_FINAL_SETTLEMENT()));
			updateAccountSummary.setDate(6,LMSUtility.truncSqlDate( accountInfoDo.getDAT_ACCRUAL()));
			updateAccountSummary.setString(7,com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT.format(new java.util.Date()));
			updateAccountSummary.setDate(8,LMSUtility.truncSqlDate( accountInfoDo.getDAT_CURRENT_BUSINESS()));
			updateAccountSummary.setDate(9,LMSUtility.truncSqlDate( accountInfoDo.getDAT_INITIAL_VALUE()));
			updateAccountSummary.setString(10, accountInfoDo.getPARTICIPANT_ID());
			if (allowIntTyp) // Sandeep changes for ned issue
			{
				if ("I".equalsIgnoreCase(accountInfoDo.getTYP_INTEREST()))
					codTxn = accountInfoDo.getCOD_TXN();
				else
					codTxn = accountInfoDo.getCOD_TXN()+ accountInfoDo.getTYP_INTEREST();
			} else {
				codTxn = accountInfoDo.getCOD_TXN();
			}
			updateAccountSummary.setString(11, codTxn);
			updateAccountSummary.setDate(12,LMSUtility.truncSqlDate( accountInfoDo.getDAT_ACCRUAL_START()));
			updateAccountSummary.setString(13, accountInfoDo.getID_POOL());
			updateAccountSummary.setString(14, accountInfoDo.getTYP_PARTICIPANT());
			updateAccountSummary.setDate(15,LMSUtility.truncSqlDate( accountInfoDo.getDAT_INITIAL_POSTING()));
			//updateAccountSummary.setDate(15,LMSUtility.truncSqlDate( accountInfoDo.getDAT_CURRENT_BUSINESS()));
			updateAccountSummary.addBatch();
		} catch (SQLException e) {
			logger.fatal("Exception in updateAccrualSummary_BVT :: ", e);
			// ENH_IS1059 Start
			// throw new LMSException("Error occured in updateAccrualSummary",
			// e.toString());
			throw new LMSException(
					"LMS01000",
					"Updation of  accural summary in OPOOL_ACCRUALSUMMARY_BVT table failed for Transaction Id  :-"
							+ accountInfoDo.getCOD_TXN()
							+ " Pool Id  :- "
							+ accountInfoDo.getID_POOL()
							+ " Participant Id :- "
							+ accountInfoDo.getPARTICIPANT_ID()
							+ " and Type Participant Type is "
							+ accountInfoDo.getTYP_PARTICIPANT() + ".", e);
			// ENH_IS1059 Ends

		}
		if (logger.isDebugEnabled()) {
			logger.debug("Leaving updateAccrualSummary_BVTForICL");
		}
		
	}
	//SUP_ISS_III_7305 end

	//SUP_ISS_III_7305 start
	private void insertAccrualSummary_BVTForICL(
			PreparedStatement insertAccrualSummary, AccountInfoDO accountInfoDo)throws LMSException {
		if (logger.isDebugEnabled()) {
			logger.debug("Entering insertAccrualSummary_BVTForICL "
					+ accountInfoDo.getCOD_TXN() + ","
					+ accountInfoDo.getTYP_INTEREST());

		}
		boolean allowIntTyp = ExecutionConstants.FLG_SA_INTEREST_TYPE; // Sandeep changes for ned issue
		String codTxn = null;
		try {
			insertAccrualSummary.setString(1, accountInfoDo.getID_POOL());
			insertAccrualSummary
					.setString(2, accountInfoDo.getPARTICIPANT_ID());
			if (allowIntTyp) // Sandeep changes for ned issue
			{
				if ("I".equalsIgnoreCase(accountInfoDo.getTYP_INTEREST()))
					codTxn = accountInfoDo.getCOD_TXN();
				else
					codTxn = accountInfoDo.getCOD_TXN()
							+ accountInfoDo.getTYP_INTEREST();
			} else {
				codTxn = accountInfoDo.getCOD_TXN();
			}
			insertAccrualSummary.setString(3, codTxn);
			insertAccrualSummary.setDate(4, LMSUtility.truncSqlDate(accountInfoDo.getDAT_ACCRUAL_START()));
			insertAccrualSummary.setDate(5,LMSUtility.truncSqlDate( accountInfoDo.getDAT_ACCRUAL()));
			insertAccrualSummary.setDate(6, LMSUtility.truncSqlDate(accountInfoDo.getDAT_ADJUSTEDACCRUAL()));
			insertAccrualSummary.setBigDecimal(7, accountInfoDo
					.getAMT_UNACCRUED());
			insertAccrualSummary.setBigDecimal(8, accountInfoDo
					.getAMT_ACCRUED());
			insertAccrualSummary.setDate(9, LMSUtility.truncSqlDate(accountInfoDo.getDAT_FINAL_SETTLEMENT()));
			insertAccrualSummary.setString(10, accountInfoDo.getTXT_STATUS());
			insertAccrualSummary.setDate(11,LMSUtility.truncSqlDate( accountInfoDo.getDAT_POSTING()));
			insertAccrualSummary.setString(12, accountInfoDo
					.getTYP_PARTICIPANT());
			insertAccrualSummary
					.setString(
							13,
							com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants.REF_DATE_FORMAT
									.format(new java.util.Date()));
			insertAccrualSummary.setDate(14, LMSUtility.truncSqlDate(accountInfoDo.getDAT_CURRENT_BUSINESS()));
			insertAccrualSummary.setDate(15, LMSUtility.truncSqlDate(accountInfoDo.getDAT_POSTING_START()));
			insertAccrualSummary.setDate(16, LMSUtility.truncSqlDate(accountInfoDo.getDAT_INITIAL_POSTING()));
			insertAccrualSummary.setDate(17, LMSUtility.truncSqlDate(accountInfoDo.getDAT_INITIAL_VALUE()));
			insertAccrualSummary.addBatch();
		} catch (SQLException e) {

			logger.fatal("Exception in insertAccrualSummary_BVTForICL :: ", e);
			throw new LMSException(
					"LMS01000",
					"Insertion of  accural summary in OPOOL_ACCRUALSUMMARY_BVT table failed for Transaction Id  :- "
							+ accountInfoDo.getCOD_TXN()
							+ ", Pool Id  :- "
							+ accountInfoDo.getID_POOL()
							+ ",  Participant Id :- "
							+ accountInfoDo.getPARTICIPANT_ID()
							+ " and Type Participant Type is "
							+ accountInfoDo.getTYP_PARTICIPANT() + ".", e);
			// ENH_IS1059 Ends
		}
		if (logger.isDebugEnabled()) {
			logger.debug("Leaving insertAccrualSummary_BVTForICL");
		}
		
	}
	//SUP_ISS_III_7305 end


//LMS_45_PERF_CERTIFICATION_PF24 start

/**
 * This Method populates Accrual Summary / Posting Summary /Account Details required for PROCESSING Accounting Entries
 * @param lCachedRowSet
 * @param simId 
 * @param eventId 
 * @param b 
 * @throws LMSException 
 * @throws SQLException 
 */
public void populateDetailsForAccountInfo(Connection conn, CachedRowSet lCachedRowSet, int eventId, long simId, boolean source, HashMap<String,AccrualInfoDO> accuralInfoMap,HashMap<String,PostingInfoDO> postingInfoMap,HashMap<String,AccountDtlsInfoDO> accountDtlsInfoMap) throws LMSException  {

	
	if (logger.isDebugEnabled()) {
		logger.debug("Entering populateDetailsForAccountInfo");
	}
	
	

	if (logger.isDebugEnabled()) {
		logger.debug("eventId>>>"+ eventId);
		logger.debug("simId>>>"+ simId);
		logger.debug("source>>>"+ source);
	}
	
	setSqlProperties();

	
	String idPool = null;
	String typeParticipant = null;
	String sourcePoolId = null;
	String participantId = null;
	String cod_txn=null;
	GregorianCalendar gDate = new GregorianCalendar();
	java.util.Date incDateUtil = null;
	java.sql.Date incDateSql = null;
	
	
	PreparedStatement accurualTempInsert = null;
	PreparedStatement postingTempInsert = null;
	PreparedStatement accountDetailsTempInsert = null;

	
	try{
		
		String accurualTempInsertSimQuery = interestCalculatorSql.getProperty("accurualTempInsertSim");
		String postingTempInsertSimQuery = interestCalculatorSql.getProperty("postingTempInsertSim");
		String accountDetailsTempInsertSimQuery = interestCalculatorSql.getProperty("accountDetailsTempInsertSim");

		String accurualTempInsertQuery = interestCalculatorSql.getProperty("accurualTempInsert");
		String postingTempInsertQuery = interestCalculatorSql.getProperty("postingTempInsert");
		String accountDetailsTempInsertQuery = interestCalculatorSql.getProperty("accountDetailsTempInsert");

		
		if(simId !=0){
		accurualTempInsert = conn.prepareStatement(accurualTempInsertSimQuery);
		postingTempInsert = conn.prepareStatement(postingTempInsertSimQuery);
		accountDetailsTempInsert = conn.prepareStatement(accountDetailsTempInsertSimQuery);
		}else{
		accurualTempInsert = conn.prepareStatement(accurualTempInsertQuery);
		postingTempInsert = conn.prepareStatement(postingTempInsertQuery);
		accountDetailsTempInsert = conn.prepareStatement(accountDetailsTempInsertQuery);
		}
		
		
		AccrualInfoDO accuralInfoDO = new AccrualInfoDO();
    	PostingInfoDO postingInfoDO = new PostingInfoDO();
    	AccountDtlsInfoDO accountDtlsInfoDO = new AccountDtlsInfoDO();
    	
    	
    	
        while(lCachedRowSet.next()){
    	
        	accuralInfoDO.clearObject();
        	postingInfoDO.clearObject();
        	accountDtlsInfoDO.clearObject();

        	 idPool = lCachedRowSet.getString("ID_POOL");
        	 typeParticipant = lCachedRowSet.getString("TYP_PARTICIPANT");
        	 participantId = lCachedRowSet.getString("PARTICIPANT_ID");

        	 if(source){
        		
        		 sourcePoolId = lCachedRowSet.getString("SOURCE_POOL_ID");
        		 accuralInfoDO.setSOURCE_POOL_ID(sourcePoolId);
            	 postingInfoDO.setSOURCE_POOL_ID(sourcePoolId);
            	 accountDtlsInfoDO.setSOURCE_POOL_ID(sourcePoolId);
        		 
        		 
            	 if(lCachedRowSet.getDate("DAT_LASTACCRUAL")==null)
 				{
 					//set DAT_ACCRUAL_START = DATE_POOL_START
 					if(simId !=0)
 					{
 						accuralInfoDO.setDAT_ACCRUAL_START(lCachedRowSet.getDate("DAT_ADJUSTEDACCRUAL"));//should be last Acc date+1
 					}
 					else
 					{
 						accuralInfoDO.setDAT_ACCRUAL_START(lCachedRowSet.getDate("DAT_START"));//should be last Acc date+1
 					}
 				}
 				else
 				{
 					//set DAT_ACCRUAL_START = DAT_LASTACCRUAL + 1 
 					gDate.setTime(lCachedRowSet.getDate("DAT_LASTACCRUAL"));
 					gDate.add(Calendar.DATE,1);
 					incDateUtil =  gDate.getTime();
 					incDateSql = new java.sql.Date(incDateUtil.getTime());
 					accuralInfoDO.setDAT_ACCRUAL_START(incDateSql);
 					
 				}
            	 
            	 if(ExecutionConstants.FLAG_BVT_CYCLE.equalsIgnoreCase("Y"))
 					cod_txn = lCachedRowSet.getString("COD_TXN")+ExecutionConstants.NORMAL_POST_CYCLE;
 				else
 					cod_txn = lCachedRowSet.getString("COD_TXN");
            	 
            	 String interestType = null;
            	 
            	 if(!ExecutionConstants.FLG_INTACC_POST_INTTYP) // If it is 'N'
 				{
 					interestType = Constants.EMPTY_STRING ;
 				}
 				else
 				{
 					
 					interestType = lCachedRowSet.getString("TYP_INTEREST") ;
 					
 					if(interestType == null || interestType.equals(AllocationConstants.DEFAULT))
 					{
 						interestType = Constants.EMPTY_STRING ;
 					}
 				}
 				
            		accuralInfoDO.setCOD_TXN(cod_txn + interestType); 
     				postingInfoDO.setCOD_TXN(cod_txn + interestType); 
     				accountDtlsInfoDO.setCOD_TXN(cod_txn + interestType); 
        	 }else{
        		 
        		 if(lCachedRowSet.getDate("DAT_LASTACCRUAL")==null)
 				{
 					//set DAT_ACCRUAL_START = DATE_POOL_START
 					if(simId !=0)
 					{
 						accuralInfoDO.setDAT_ACCRUAL_START(lCachedRowSet.getDate("DAT_ADJUSTEDACCRUAL"));//should be last Acc date+1
 					}
 					else
 					{
 						accuralInfoDO.setDAT_ACCRUAL_START(lCachedRowSet.getDate("DAT_START"));//should be last Acc date+1
 					}
 				}
 				else
 				{
 					//set DAT_ACCRUAL_START = DAT_LASTACCRUAL + 1 
 					gDate.setTime(lCachedRowSet.getDate("DAT_LASTACCRUAL"));
 					if(eventId!=ExecutionConstants.MANUAL_ADJUSTMENTS  && (simId == 0 || (simId != 0 && eventId != ExecutionConstants.INTEREST_CALC))){
 						gDate.add(Calendar.DATE,1);
 					}
 					incDateUtil =  gDate.getTime();
 					incDateSql = new java.sql.Date(incDateUtil.getTime());
 					accuralInfoDO.setDAT_ACCRUAL_START(incDateSql);
 					
 				}
        		 
 				
 				accuralInfoDO.setCOD_TXN(lCachedRowSet.getString("COD_TXN")); 
 				postingInfoDO.setCOD_TXN(lCachedRowSet.getString("COD_TXN")); 
 				accountDtlsInfoDO.setCOD_TXN(lCachedRowSet.getString("COD_TXN")); 

        		 
        	 }
        	 
        	 
        	   if(lCachedRowSet.getDate("DAT_LASTPOSTING") == null)
				{
					//set DAT_POSTING_START = DATE_POOL_START
					postingInfoDO.setDAT_POSTING_START(lCachedRowSet.getDate("DAT_START"));//should be last Posting +1
				}
				else
				{
					//set DAT_POSTING_START = DAT_LASTPOSTING + 1 
					gDate.setTime(lCachedRowSet.getDate("DAT_LASTPOSTING"));
					gDate.add(Calendar.DATE,1);
					incDateUtil =  gDate.getTime();
					incDateSql = new java.sql.Date(incDateUtil.getTime());
					postingInfoDO.setDAT_POSTING_START(incDateSql);
				}
				
        	 
        	 accuralInfoDO.setID_POOL(idPool);
        	 postingInfoDO.setID_POOL(idPool);
        	 accountDtlsInfoDO.setID_POOL(idPool);

        	
        	 accuralInfoDO.setTYP_PARTICIPANT(typeParticipant);
        	 postingInfoDO.setTYP_PARTICIPANT(typeParticipant);
        	 accountDtlsInfoDO.setTYP_PARTICIPANT(typeParticipant);
        	 
        	 accuralInfoDO.setPARTICIPANT_ID(participantId);
        	 postingInfoDO.setPARTICIPANT_ID(participantId);
        	 accountDtlsInfoDO.setPARTICIPANT_ID(participantId);


        	accountDtlsInfoDO.setDAT_BUSINESS(lCachedRowSet.getDate("BUSDT"));
        	accountDtlsInfoDO.setDAT_CURRENT_BUSINESS(lCachedRowSet.getDate("BUSDT"));

        	
        	if (logger.isDebugEnabled()) {
        		logger.debug("accuralInfoDO "  + accuralInfoDO);
        		logger.debug("postingInfoDO "  + postingInfoDO);
        		logger.debug("accountDtlsInfoDO "  + accountDtlsInfoDO);
        	}
        	
        	
        	
        	createAccrualTempTableBatch(accurualTempInsert,accuralInfoDO,simId);
        	createPostingTempTableBatch(postingTempInsert,postingInfoDO,simId);
        	createAccountDtlsTempTableBatch(accountDetailsTempInsert,accountDtlsInfoDO,simId);
        	
        }
        
        accurualTempInsert.executeBatch();
        postingTempInsert.executeBatch();
        accountDetailsTempInsert.executeBatch();

		
        populateAccrualInfoMap(conn,accuralInfoMap,simId);
        
        if (logger.isDebugEnabled()) {
    		logger.debug("accuralInfoMap :: "  + accuralInfoMap);
    	}
        
        
        populatePostingInfoMap(conn,postingInfoMap,simId);
        
        if (logger.isDebugEnabled()) {
    		logger.debug("postingInfoMap :: "  + postingInfoMap);
    	}
        
        populateAccountDtlsInfoMap(conn,accountDtlsInfoMap,simId);

    	
    	if (logger.isDebugEnabled()) {
    		logger.debug("accountDtlsInfoMap ::"  + accountDtlsInfoMap);
    	}
    	
    	
    	
        lCachedRowSet.beforeFirst();
        
        
	}catch(SQLException e){
			
		logger.fatal("Exception in populateDetailsForAccountInfo :: ", e);
		throw new LMSException(
				"LMS01000",
				"Exception in populateDetailsForAccountInfo", e);
	}finally{
		
		LMSConnectionUtility.close(accurualTempInsert);
		LMSConnectionUtility.close(postingTempInsert);
		LMSConnectionUtility.close(accountDetailsTempInsert);

	}
	
	if (logger.isDebugEnabled()) {
		logger.debug("Leaving populateDetailsForAccountInfo");
	}		
			
	
	}

/**
 * Populates Required Account Dtls Data For Processing accounting entries and  Store it in memory map.
 * @param conn
 * @param accountDtlsInfoMap
 * @param simId
 * @throws LMSException 
 */
private void populateAccountDtlsInfoMap(Connection conn,
		HashMap<String, AccountDtlsInfoDO> accountDtlsInfoMap, long simId) throws LMSException {

	PreparedStatement prep = null;
	ResultSet resultSet = null;
	String query = new String();
	try {
		if (simId != 0) {
			query=interestCalculatorSql.getProperty("populateAccountDtlsInfoMapSim");
		} else {

			query=interestCalculatorSql.getProperty("populateAccountDtlsInfoMap");
		}

		prep = conn.prepareStatement(query.toString());
		prep.setFetchSize(1000);
		resultSet = prep.executeQuery();
	
		while (resultSet.next()) {

			AccountDtlsInfoDO accountDtlsInfoDo = new AccountDtlsInfoDO();
			String key = resultSet.getString("NBR_OWNACCOUNT")
					+ "|"
					+ resultSet.getString("ID_POOL")
					+ "|"
					+ resultSet.getString("TYP_PARTICIPANT")
					+ "|"
					+ resultSet.getString("SOURCE_POOL_ID")+"|"
					+ resultSet.getString("COD_TXN")+"|"
					+ resultSet.getDate("DAT_BUSINESS").toString();

			accountDtlsInfoDo.setPARTICIPANT_ID(resultSet
					.getString("NBR_OWNACCOUNT"));
			accountDtlsInfoDo.setTYP_PARTICIPANT(resultSet
					.getString("TYP_PARTICIPANT"));
			accountDtlsInfoDo.setCOD_TXN(resultSet.getString("COD_TXN"));
			accountDtlsInfoDo.setSOURCE_POOL_ID(resultSet
					.getString("SOURCE_POOL_ID"));
			accountDtlsInfoDo.setAMT_CALCULATED(resultSet.getBigDecimal("AMT_CALCULATED"));

			accountDtlsInfoMap.put(key, accountDtlsInfoDo);

		}

	} catch (Exception e) {
		logger.fatal("Exception in populateAccountDtlsInfoMap :: ", e);
		throw new LMSException(
				"LMS01000",
				"Exception in populateAccountDtlsInfoMap", e);
	}finally{
		LMSConnectionUtility.close(resultSet);
		LMSConnectionUtility.close(prep);
	}
	
}


/**
 * Create Preapared Statment which will insert data in Temp Table.
 * @param accountDetailsTempInsert
 * @param accountDtlsInfoDO
 * @throws LMSException 
 */
private void createAccountDtlsTempTableBatch(
		PreparedStatement accountDetailsTempInsert,
		AccountDtlsInfoDO accountDtlsInfoDO,long simId) throws LMSException {


int count = 1;
try{	
		accountDetailsTempInsert.setString(count++, accountDtlsInfoDO.getID_POOL());
		accountDetailsTempInsert.setString(count++, accountDtlsInfoDO.getPARTICIPANT_ID());
		accountDetailsTempInsert.setString(count++, accountDtlsInfoDO.getTYP_PARTICIPANT());
		accountDetailsTempInsert.setString(count++, accountDtlsInfoDO.getSOURCE_POOL_ID());
		accountDetailsTempInsert.setDate(count++, accountDtlsInfoDO.getDAT_BUSINESS());
		accountDetailsTempInsert.setDate(count++, accountDtlsInfoDO.getDAT_CURRENT_BUSINESS());
		accountDetailsTempInsert.setString(count++, accountDtlsInfoDO.getCOD_TXN());
		
		if(simId!=0)
			accountDetailsTempInsert .setLong(count++, simId);
		
		accountDetailsTempInsert.addBatch();
		
	}catch(SQLException e){
			
		logger.fatal("Exception in createAccountDtlsTempTableBatch :: ", e);
		throw new LMSException(
				"LMS01000",
				"Exception in createAccountDtlsTempTableBatch", e);	
	}
}

/**
 * Create Preapared Statment which will insert data in Temp Table.
 * @param postingTempInsert
 * @param postingInfoDO
 * @throws LMSException 
 */
private void createPostingTempTableBatch(PreparedStatement postingTempInsert,
		PostingInfoDO postingInfoDO,long simId) throws LMSException {
	
	int count = 1;
	try{
			
			
		postingTempInsert.setString(count++, postingInfoDO.getID_POOL());
		postingTempInsert.setString(count++, postingInfoDO.getPARTICIPANT_ID());
		postingTempInsert.setString(count++, postingInfoDO.getTYP_PARTICIPANT());
		postingTempInsert.setString(count++, postingInfoDO.getSOURCE_POOL_ID());
		postingTempInsert.setDate(count++, postingInfoDO.getDAT_POSTING_START());
		postingTempInsert.setString(count++, postingInfoDO.getCOD_TXN());
		
		if(simId!=0)
			postingTempInsert .setLong(count++, simId);
		
		postingTempInsert.addBatch();
		
	}catch(SQLException e){
			
		logger.fatal("Exception in createPostingTempTableBatch :: ", e);
		throw new LMSException(
				"LMS01000",
				"Exception in createPostingTempTableBatch", e);	
	}
	
}


/**
 * Create Preapared Statment which will insert data in Temp Table.
 * @param accurualTempInsert
 * @param accuralInfoDO
 * @param simId 
 * @throws LMSException 
 */
private void createAccrualTempTableBatch(PreparedStatement accurualTempInsert,
		AccrualInfoDO accuralInfoDO, long simId) throws LMSException {

int count = 1;
try{
		
		
	accurualTempInsert.setString(count++, accuralInfoDO.getID_POOL());
	accurualTempInsert.setString(count++, accuralInfoDO.getPARTICIPANT_ID());
	accurualTempInsert.setString(count++, accuralInfoDO.getTYP_PARTICIPANT());
	accurualTempInsert.setString(count++, accuralInfoDO.getSOURCE_POOL_ID());
	accurualTempInsert.setDate(count++, accuralInfoDO.getDAT_ACCRUAL_START());
	accurualTempInsert.setString(count++, accuralInfoDO.getCOD_TXN());
	
	if(simId!=0)
	accurualTempInsert.setLong(count++, simId);
	
	
	accurualTempInsert.addBatch();
	
}catch(SQLException e){
	logger.fatal("Exception in createAccrualTempTableBatch :: ", e);
	throw new LMSException(
			"LMS01000",
			"Exception in createAccrualTempTableBatch", e);	
		
}

}




/**
 * Populates Required Posting Data For Processing accounting entries aND Store it in memory map.
 * @param conn
 * @param postingInfoDOListtemp
 * @param postingInfoMap
 * @param simId
 * @throws LMSException 
 */
	private void populatePostingInfoMap(Connection conn,
			HashMap<String, PostingInfoDO> postingInfoMap, long simId) throws LMSException {

		PreparedStatement prep = null;
		ResultSet resultSet = null;
		String query = new String();
		try {
			if (simId != 0) {
				query=interestCalculatorSql.getProperty("populatePostingInfoMapSim");
			} else {

				query=interestCalculatorSql.getProperty("populatePostingInfoMap");
			}

			prep = conn.prepareStatement(query);
			prep.setFetchSize(1000);

			resultSet = prep.executeQuery();

			while (resultSet.next()) {

				PostingInfoDO pstingInfoDo = new PostingInfoDO();
				String key = resultSet.getString("NBR_OWNACCOUNT")
						+ "|"
						+ resultSet.getString("ID_POOL")
						+ "|"
						+ resultSet.getString("TYP_PARTICIPANT")
						+ "|"
						+ resultSet.getString("SOURCE_POOL_ID")+"|"
						+ resultSet.getString("COD_TXN")+"|"
						+ resultSet.getDate("DAT_POSTING_START").toString();
										

				pstingInfoDo.setPARTICIPANT_ID(resultSet
						.getString("NBR_OWNACCOUNT"));
				pstingInfoDo.setTYP_PARTICIPANT(resultSet
						.getString("TYP_PARTICIPANT"));
				pstingInfoDo.setCOD_TXN(resultSet.getString("COD_TXN"));
				pstingInfoDo.setSOURCE_POOL_ID(resultSet
						.getString("SOURCE_POOL_ID"));
				pstingInfoDo.setDAT_POSTING_START(resultSet
						.getDate("DAT_POSTING_START"));
				pstingInfoDo.setAMT_ACCRUEDNOTPOSTED(resultSet
						.getBigDecimal("AMT_ACCRUEDNOTPOSTED"));
				pstingInfoDo.setAMT_POSTED(resultSet
						.getBigDecimal("AMT_POSTED"));
				pstingInfoDo.setAMT_POSTED_CENTRAL_CCY(resultSet
						.getBigDecimal("AMT_POSTED_CENTRAL_CCY"));
				pstingInfoDo.setOLD_DAT_FINAL_SETTLEMENT(resultSet
						.getDate("DAT_FINAL_SETTLEMENT"));
				pstingInfoDo.setOLD_DAT_POSTING(resultSet
						.getDate("DAT_POSTING"));

				postingInfoMap.put(key, pstingInfoDo);

			}

		} catch (SQLException e) {
			logger.fatal("Exception in populatePostingInfoMap :: ", e);
			throw new LMSException(
					"LMS01000",
					"Exception in populatePostingInfoMap", e);	
		}finally{
			LMSConnectionUtility.close(resultSet);
			LMSConnectionUtility.close(prep);
		}

	
}


/**
 * Populates Required Accrual Data For Processing accounting entries aND Store it in memory map.
 * @param conn
 * @param accuralInfoDOList
 * @param accuralInfoMap
 * @param simId 
 * @throws LMSException 
 */
private void populateAccrualInfoMap(Connection conn,
		HashMap<String, AccrualInfoDO> accuralInfoMap, long simId) throws LMSException {
	
	PreparedStatement prep = null;
	ResultSet resultSet = null;
	String query = new String();
	try {
		if (simId != 0) {
			query=interestCalculatorSql.getProperty("populateAccrualInfoMapSim");
		} else {

			query=interestCalculatorSql.getProperty("populateAccrualInfoMap");
		}
	   
		prep = conn.prepareStatement(query);
		prep.setFetchSize(1000);
		resultSet = prep.executeQuery();
		
		while(resultSet.next()){
		
		AccrualInfoDO accrualInfoDo = new AccrualInfoDO();
        String key = resultSet.getString("NBR_OWNACCOUNT")+"|"+resultSet.getString("ID_POOL")
        		    +"|"+resultSet.getString("TYP_PARTICIPANT")+"|"+resultSet.getString("SOURCE_POOL_ID")+"|"
        		    +resultSet.getString("COD_TXN")+"|"+resultSet.getDate("DAT_ACCRUAL_START").toString();
		
        accrualInfoDo.setPARTICIPANT_ID(resultSet.getString("NBR_OWNACCOUNT"));
        accrualInfoDo.setTYP_PARTICIPANT(resultSet.getString("TYP_PARTICIPANT"));
        accrualInfoDo.setCOD_TXN(resultSet.getString("COD_TXN"));
        accrualInfoDo.setSOURCE_POOL_ID(resultSet.getString("SOURCE_POOL_ID"));
        accrualInfoDo.setAMT_ACCRUED(resultSet.getBigDecimal("AMT_ACCRUED"));
        accrualInfoDo.setAMT_UNACCRUED(resultSet.getBigDecimal("aMT_UNACCRUED"));
        accrualInfoDo.setDAT_ACCRUAL_START(resultSet.getDate("DAT_ACCRUAL_START"));
        accrualInfoDo.setOLD_DAT_ACCRUAL(resultSet.getDate("DAT_ACCRUAL"));
        accrualInfoDo.setOLD_DAT_ADJUSTEDACCRUAL(resultSet.getDate("DAT_ACCRUAL_ADJUSTED"));
        accuralInfoMap.put(key, accrualInfoDo);
			
		}
	}catch(SQLException e){
		logger.fatal("Exception in populateAccrualInfoMap :: ", e);
		throw new LMSException(
				"LMS01000",
				"Exception in populateAccrualInfoMap", e);	
	}finally{
		LMSConnectionUtility.close(resultSet);
		LMSConnectionUtility.close(prep);

	}
		
	 
	
	
}

/**
 * This Method will check in Record Exists in Accural Summary.
 * @param accountInfoDo
 * @param accuralInfoMap
 * @return
 */
private boolean checkRecordInAccrualSummary(AccountInfoDO accountInfoDo,
		HashMap<String, AccrualInfoDO> accuralInfoMap) {

	boolean found = false;
	
	
	String key =  accountInfoDo.getPARTICIPANT_ID()+"|"+accountInfoDo.getID_POOL()+"|"+accountInfoDo.getTYP_PARTICIPANT()+"|"+accountInfoDo.getSOURCE_POOL_ID()+"|"+
			accountInfoDo.getCOD_TXN()+"|"+accountInfoDo.getDAT_ACCRUAL_START().toString();	
	
	if (logger.isDebugEnabled())
	{
		logger.debug("Key  " + key);
		logger.debug("accuralInfoMap " + accuralInfoMap);
	}
	
	
	
	AccrualInfoDO accrualInfoDO = accuralInfoMap.get(key);
	
	if(accrualInfoDO!=null){
		
		found = true;
		
		accountInfoDo.setAMT_UNACCRUED(accrualInfoDO.getAMT_UNACCRUED());
		accountInfoDo.setOLD_DAT_ACCRUAL(accrualInfoDO.getOLD_DAT_ACCRUAL());
		accountInfoDo.setOLD_DAT_ADJUSTEDACCRUAL(accrualInfoDO.getOLD_DAT_ADJUSTEDACCRUAL());
		//FOR BVT AMT_ACCRUED is needed
		accountInfoDo.setAMT_ACCRUED(accrualInfoDO.getAMT_ACCRUED());
		if (logger.isDebugEnabled())
		{
			logger.debug("IN DB amtUnaccured " + accrualInfoDO.getAMT_UNACCRUED());
			logger.debug("IN DB accured " + accrualInfoDO.getAMT_ACCRUED());
		}
	}
	
	
	
	return found;
}


/**
 * This Method will check in Record Exists in Accural Summary.
 * @param accountInfoDo
 * @param postingInfoMap
 * @return
 */
private boolean checkRecordInPostingSummary(AccountInfoDO accountInfoDo,
		HashMap<String, PostingInfoDO> postingInfoMap) throws LMSException {
	
   boolean found = false;
	
	//added by BOI issue 145
	PreparedStatement selectTypRepay=null;
	Connection conn = null;
	ResultSet res = null;
	//end by BOI issue 145	
	String key =  accountInfoDo.getPARTICIPANT_ID()+"|"+accountInfoDo.getID_POOL()+"|"+accountInfoDo.getTYP_PARTICIPANT()+"|"+accountInfoDo.getSOURCE_POOL_ID()+"|"+
			accountInfoDo.getCOD_TXN()+"|"+accountInfoDo.getDAT_POSTING_START().toString();
	
	if (logger.isDebugEnabled())
	{
		logger.debug("Key  " + key);
		logger.debug("accuralInfoMap " + postingInfoMap);
	}
	
	
	
	PostingInfoDO postingInfo = postingInfoMap.get(key);
	
	if(postingInfo!=null){
		
		found = true;

		accountInfoDo.setAMT_ACCRUEDNOTPOSTED(postingInfo.getAMT_ACCRUEDNOTPOSTED());
		accountInfoDo.setAMT_POSTED(postingInfo.getAMT_POSTED());
		accountInfoDo.setOLD_DAT_FINAL_SETTLEMENT(postingInfo.getOLD_DAT_FINAL_SETTLEMENT());
		
		accountInfoDo.setOLD_DAT_POSTING(postingInfo.getOLD_DAT_POSTING());
		accountInfoDo.setAMT_POSTED_CENTRAL_CCY(postingInfo.getAMT_POSTED_CENTRAL_CCY());//Reallocate to Self
		
		if (logger.isDebugEnabled())
		{
			logger.debug("IN DB amtAccuredNotPosted " + postingInfo.getAMT_ACCRUEDNOTPOSTED());
			logger.debug("IN DB AMT_POSTED " + postingInfo.getAMT_POSTED());
			logger.debug("IN DB AMT_POSTED_CENTRAL_CCY " + postingInfo.getAMT_POSTED_CENTRAL_CCY());//Reallocate to Self
		}
	}
	
	//added by BOI issue 145
	try{
	conn = LMSConnectionUtility.getConnection(); 
	String sqlString = "select TYP_REPAYMENT from OLM_INCO_DEFINITION where COD_INCOID =?";
	selectTypRepay= conn.prepareStatement(sqlString);
	if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
		logger.debug("RPI case accountInfoDo.getID_POOL() "+accountInfoDo.getID_POOL());
	selectTypRepay.setString(1, accountInfoDo.getID_POOL());
	LMSConnectionUtility.close(res); //15.1.1_Resource_Leaks_Fix
	res =null;
	res=selectTypRepay.executeQuery();
	String typ_repayment="";
	if(res.next()){
		typ_repayment=res.getString("TYP_REPAYMENT");
	}
	accountInfoDo.setTYP_REPAYMENT(typ_repayment);
	//end by BOI issue 145
	}catch(Exception e)
	{
		logger.fatal("Exception :: " , e);
		//ENH_IS1059  Starts
		//throw new LMSException("Error occured in checkRecordInPostigSummary", e.toString());
		throw new LMSException("LMS01000","Record already exist in OPOOL_POSTINGSUMMARY table for Transaction Id "+accountInfoDo.getCOD_TXN()+", Pool Id :- "+accountInfoDo.getID_POOL()+", Participant :- "+accountInfoDo.getPARTICIPANT_ID()+" and Participant Type :"+accountInfoDo.getTYP_PARTICIPANT()+".", e);
		//ENH_IS1059  Ends
	}finally
	{
	    LMSConnectionUtility.close(res);
	    //added by BOI issue 145
	    LMSConnectionUtility.close(selectTypRepay);
	    LMSConnectionUtility.close(conn);
	    
	    //end by BOI issue 145
    }
	if (logger.isDebugEnabled()){
		logger.debug("Leaving ");
	}
	
	return found;
}


/**
 * This Method will check in Record Exists in Accural Summary.
 * @param accountInfoDo
 * @param accountDtlsInfoMap
 * @return
 */
private boolean checkRecordInAccountDtls(AccountInfoDO accountInfoDo,
		HashMap<String, AccountDtlsInfoDO> accountDtlsInfoMap) {

	
	
  boolean found = false;
	
	
	String key =  accountInfoDo.getPARTICIPANT_ID()+"|"+accountInfoDo.getID_POOL()+"|"+accountInfoDo.getTYP_PARTICIPANT()+"|"+accountInfoDo.getSOURCE_POOL_ID()+"|"+
			accountInfoDo.getCOD_TXN()+"|"+accountInfoDo.getDAT_BUSINESS().toString();	
	
	if (logger.isDebugEnabled())
	{
		logger.debug("Key  " + key);
		logger.debug("accuralInfoMap " + accountDtlsInfoMap);
	}
	
	
	
	AccountDtlsInfoDO accountDtlsInfoDO = accountDtlsInfoMap.get(key);
	
	if(accountDtlsInfoDO!=null){
		
	found = true;
	if (logger.isDebugEnabled())
	{
		logger.debug("INside while for participant_id  " + accountInfoDo.getPARTICIPANT_ID());
		logger.debug("IN DB AMT_CALCULATED " + accountDtlsInfoDO.getAMT_CALCULATED());
		logger.debug("IN DO AMT_CALCULATED " + accountInfoDo.getAMT_CALCULATED());
	}
	accountInfoDo.setAMT_CALCULATED(accountDtlsInfoDO.getAMT_CALCULATED().add(accountInfoDo.getAMT_CALCULATED()));

	}
	//set AMT_CALCULATED = AMT_CALCULATED(from Data Object) + AMT_CALCULATED(from Result set)
	
	
	
	
	return found;
}
//LMS_45_PERF_CERTIFICATION_PF24 end

}


