
/**
 *     These materials are confidential and proprietary to Intellect Design Arena Ltd.
 *  and no part of these materials should be reproduced, published, transmitted or
 *  distributed in any form or by any means, electronic, mechanical, photocopying,
 *  recording or otherwise, or stored in any information storage or retrieval system
 *  of any nature nor should the materials be disclosed to third parties or used in
     *  any other manner for which this is not authorized, without the prior express
 *  written authorization of Intellect Design Arena Ltd.
 *
 * <p>Title       : Intellect Liquidity</p>
 * <p>Description :This class holds the data for the Accounting entry Leg 
 *                  
 * <p>SCF NO      :   </p>
 * <p>Copyright   : Copyright 2016 Intellect Design Arena Limited. All rights reserved.</p>
 * <p>Company     : Intellect Design Arena Limited</p>
 * <p>Date of Creation : 23-May-2005</p>
 * <p>Source      : AELegData.java </p>
 * <p>Package     : com.intellectdesign.cash.pooling.execution.accountingentry  </p>
 * 
 * @version 1.0
 * <p>------------------------------------------------------------------------------------</p>
 * <p>MODIFICATION HISTORY:</p>
 * <p>------------------------------------------------------------------------------------</p>
 * <p>SERIAL      AUTHOR		    DATE		SCF	DESCRIPTION</p>
 * <p>------------------------------------------------------------------------------------</p>
 * <p>1   Prateek Aole			11/05/2012		 		        		JTest Fixes</p>
 * <p>2   Sandeep M.			24/09/2014		 		        		SUP_IS173</p>
 */
package com.intellectdesign.cash.accountingentry;

import java.util.GregorianCalendar;

import com.intellectdesign.exception.LMSException;
import com.polaris.intellect.commons.logging.Log;
import com.polaris.intellect.commons.logging.LogFactory;

public class AELegData {
	private static Log logger = LogFactory.getLog(AELegData.class.getName());
	
//	Declaring the variables
	  /**
	   * This indicates the process ID.
	   */
	  protected long nbr_processid;

	  /**
	   * This indicates the accounting entry ID.
	   */
	  protected long nbr_acctentryid;

	  /**
	   * This indicates the instruction ID.
	   */
	  protected long nbr_instrtxnid;

	  /**
	   * This indicates the leg ID.
	   */
	  protected long nbr_legid;

	  /**
	   * this variable stores the own account Number for the 'Legs'.
	   */
	  protected String nbr_ownaccount;

	  /**
	   * this variable is used for entry type of legs
	  **/
	  
	  
	  protected String flg_entrytype;

	  /**
	   * this variable stores the core account Number for the 'Legs'.
	   */
	  protected String nbr_coreaccount;

	  /**
	   * This flag indicates the account type
	   */
	  protected String flg_accttype;

	  /**
	   * This flag indicates whether there is a mirror entry or not
	   */
	  protected String flg_mirrorentry;

	  /**
	   * indicates the branch code
	   */
	  protected String cod_branch;

	  /**
	   * indicates the BIC code
	   */
	  protected String cod_bic;

	  /**
	   * indicates the GL code
	   */
	  protected String cod_gl;

	  /**
	   * indiacates the currency code
	   */
	  protected String cod_ccy;

	  /**
	   * indicates the Debit Credit Type for each & all the legs
	   */
	  protected String flg_drcr;

	  /**
	   * indiactes the transaction amount
	   */
	  protected double amt_txn;

	  /**
	   * indicates the transaction code
	   */
	  protected String cod_txn;

	  /**
	   * the below four variables indicate the narration of the account
	   */
	  protected String txt_narration1;

	  protected String txt_narration2;

	  protected String txt_narration3;

	  protected String txt_narration4;

	  /**
	   * the below five variables indicate the extra information of the account
	   */
	  protected String txt_extinfo1;

	  protected String txt_extinfo2;

	  protected String txt_extinfo3;

	  protected String txt_extinfo4;

	  protected String txt_extinfo5;
	
	
	protected String txt_extinfo6;

	protected String txt_extinfo7;

	protected String txt_extinfo8;

	protected String txt_extinfo9;

	protected String txt_extinfo10;

	  /**
	   * indicates the system date
	   */
	  protected GregorianCalendar dat_system;

	  /**
	   * indicates the sent date
	   */
	  protected GregorianCalendar dat_sent;

	  /**
	   * indicates the business date
	   */
	  protected GregorianCalendar dat_business;

	  /**
	   * indicates the status text
	   */
	  protected String txt_status;

	  /**
	   * indicates the code of the reson
	   */
	  protected String cod_reason;

	  /**
	     * This indicates the accounting entry ID.
	     */
	  protected long nbr_lotid;

	  /**
	   * indicates the text of the failure
	   */
	  protected String txt_failure;
	  protected String txt_ibanacct;
	  protected String cod_bnkglent;
	  protected String flg_forcedr;
	  protected String txt_hostsystemref;
	  // ENH0004 starts
	  protected String flg_srctgtTyp;
	  // ENH0004 ends
	protected String nbr_ownsettle;

	protected String nbr_coresettle;
	
	protected GregorianCalendar dat_posting;
	
	protected GregorianCalendar dat_final_settlement;
	
	protected GregorianCalendar dat_value;

	protected String flg_status1;// flag status will have Accrual/Posting A/P
	protected String flg_txn; // JPMC Issue 3249
	//SUP_IS173 starts
	protected String cod_incoid;
	protected String cod_agreement;
	//SUP_IS173 ends
	  public String toString() {
	    StringBuilder sBuffer = new StringBuilder("");
	    sBuffer.append("nbr_processid : " + nbr_processid + "nbr_instrtxnid: " +
	                   nbr_instrtxnid + "nbr_legid: " + nbr_legid +
	                   " nbr_ownaccount: " + nbr_ownaccount + "nbr_coreaccount: " +
	                   nbr_coreaccount + " flg_accttype: " + flg_accttype +
	                   " flg_mirrorentry : " + flg_mirrorentry + "cod_branch : " +
	                   cod_branch + " cod_bic : " + cod_bic +
	                   " cod_gl : " + cod_gl + " cod_ccy : " + cod_ccy +
	                   " flg_drcr : " + flg_drcr +
	                   " amt_txn : " + amt_txn + " cod_txn  : " + cod_txn +
	                   " txt_narration1  : " + txt_narration1 +
	                   " txt_narration2 : " + txt_narration2 + " txt_narration3 : " +
	                   txt_narration3 + " txt_narration4 : " + txt_narration4 +
	                   " txt_extinfo1 : " + txt_extinfo1 + " txt_extinfo2 : " +
	                   txt_extinfo2 + " txt_extinfo3 : " + txt_extinfo3 +
	                   " txt_extinfo4 : " + txt_extinfo4 + " txt_extinfo5 : " +
	                   txt_extinfo5 + "dat_system : " + dat_system +
	                   " dat_sent : " + dat_sent + " txt_status : " + txt_status +
	                   " cod_reason : " + cod_reason + " txt_failure : " +
	                   txt_failure);

	    return sBuffer.toString();
	  }

	  public boolean checkMandatoryFields() throws LMSException {

	    boolean mandatFieldsPresent = false;
	    try {

	      if (this.nbr_processid == 0) {
	        logger.fatal(
	            "One of the field necessary to initiate the processing of Acct-Entry is absent");
	      }
	      else {
	        mandatFieldsPresent = true;
	      }
	    }
	    catch (Exception e) {
	      logger.fatal(
	          "General Exception occured while creating the AE leg data object", e);
	      throw new LMSException("99025", e.toString());
	    }
	    return mandatFieldsPresent;
	  }

	  /**
	   * @return
	   */
	  public double getAmt_txn() {
	    return amt_txn;
	  }

	  /**
	   * @return
	   */
	  public String getCod_branch() {
	    return cod_branch;
	  }

	  /**
	   * @return
	   */
	  public String getCod_ccy() {
	    return cod_ccy;
	  }

	  /**
	   * @return
	   */
	  public String getCod_gl() {
	    return cod_gl;
	  }

	  /**
	   * @return
	   */
	  public String getCod_reason() {
	    return cod_reason;
	  }

	  /**
	   * @return
	   */
	  public String getCod_txn() {

	    return cod_txn;
	  }

	  /**
	   * @return
	   */
	  public GregorianCalendar getDat_sent() {
	    return dat_sent;
	  }

	  /**
	   * @return
	   */
	  public GregorianCalendar getDat_system() {
	    return dat_system;
	  }

	  /**
	   * @return
	   */
	  public String getFlg_accttype() {
	    return flg_accttype;
	  }

	  /**
	   * @return
	   */
	  public String getFlg_drcr() {
	    return flg_drcr;
	  }

	  /**
	   * @return
	   */
	  public String getFlg_mirrorentry() {
	    return flg_mirrorentry;
	  }

	  /**
	   * @return
	   */
	  public String getNbr_coreaccount() {
	    return nbr_coreaccount;
	  }

	  /**
	   * @return
	   */
	  public long getNbr_legid() {
	    return nbr_legid;
	  }

	  /**
	   * @return
	   */
	  public long getNbr_instrtxnid() {
	    return nbr_instrtxnid;
	  }

	  /**
	   * @return
	   */
	  public String getNbr_ownaccount() {
	    return nbr_ownaccount;
	  }

	  /**
	   * @return
	   */
	  public long getNbrProcessID() {
	    return nbr_processid;
	  }

	  /**
	   * @return
	   */
	  public String getCod_bic() {
	    return cod_bic;
	  }

	  /**
	   * @return
	   */
	  public String getTxt_extinfo1() {
	    return txt_extinfo1;
	  }

	  /**
	   * @return
	   */
	  public String getTxt_extinfo2() {
	    return txt_extinfo2;
	  }

	  /**
	   * @return
	   */
	  public String getTxt_extinfo3() {
	    return txt_extinfo3;
	  }

	  /**
	   * @return
	   */
	  public String getTxt_extinfo4() {
	    return txt_extinfo4;
	  }

	  /**
	   * @return
	   */
	  public String getTxt_extinfo5() {
	    return txt_extinfo5;
	  }

	  /**
	   * @return
	   */
	  public String getTxt_failure() {
	    return txt_failure;
	  }

	  /**
	   * @return
	   */
	  public String getTxt_narration1() {
	    return txt_narration1;
	  }

	  /**
	   * @return
	   */
	  public String getTxt_narration2() {
	    return txt_narration2;
	  }

	  /**
	   * @return
	   */
	  public String getTxt_narration3() {
	    return txt_narration3;
	  }

	  /**
	   * @return
	   */
	  public String getTxt_narration4() {
	    return txt_narration4;
	  }

	  /**
	   * @return
	   */
	  public String getTxt_status() {
	    return txt_status;
	  }

	  /**
	   * @param f
	   */
	// JPMC Issue 3249 starts
	
	public String getFlg_txn() {
		return flg_txn;
	}
	public void setFlg_txn(String flg_txn) {
		this.flg_txn = flg_txn;
	}
	
	// JPMC Issue 3249 ends
	  public void setAmt_txn(double amt_txn) {
	    this.amt_txn = amt_txn;
	  }

	  /**
	   * @param string
	   */
	  public void setCod_branch(String cod_branch) {

	    if (cod_branch != null) {
	      this.cod_branch = cod_branch;
	    }
	    else {
	      logger.fatal(" This value should not be null");
	    }

	  }

	  /**
	   * @param string
	   */
	  public void setCod_ccy(String cod_ccy) {
	    if (cod_ccy != null) {
	      this.cod_ccy = cod_ccy;
	    }
	    else {
	    	logger.fatal(" This value should not be null");
	    }

	  }

	  /**
	   * @param string
	   */
	  public void setCod_gl(String cod_gl) {
	    if (cod_gl != null) {
	      this.cod_gl = cod_gl;
	    }
	    else {
	    	logger.fatal(" This value should not be null");
	    }
	  }

	  /**
	   * @param string
	   */
	  public void setCod_reason(String cod_reason) {
	    this.cod_reason = cod_reason;
	  }

	  /**
	   * @param string
	   */
	  public void setCod_txn(String cod_txn) {
	    this.cod_txn = cod_txn;
	  }

	  /**
	   * @param date
	   */
	  public void setDat_sent(GregorianCalendar dat_sent) {
	    if (dat_sent != null) {
	      this.dat_sent = dat_sent;
	    }
	    else {
	      logger.fatal(" This value should not be null");
	    }
	  }

	  /**
	   * @param date
	   */
	  public void setDat_system(GregorianCalendar dat_system) {
	    if (dat_system != null) {
	      this.dat_system = dat_system;
	    }
	    else {
	      logger.fatal(" This value should not be null");
	    }
	  }

	  /**
	   * @param string
	   */
	  public void setFlg_accttype(String flg_accttype) {
	    if (flg_accttype != null) {
	      this.flg_accttype = flg_accttype;
	    }
	    else {
	      logger.fatal(" This value should not be null");
	    }
	  }

	  /**
	   * @param string
	   */
	  public void setFlg_drcr(String flg_drcr) {
	    if (flg_drcr != null) {
	      this.flg_drcr = flg_drcr;
	    }
	    else {
	      logger.fatal(" This value should not be null");
	    }
	  }

	  /**
	   * @param string
	   */
	  public void setFlg_mirrorentry(String flg_mirrorentry) {
	    if (flg_mirrorentry != null) {
	      this.flg_mirrorentry = flg_mirrorentry;
	    }
	    else {
	      this.flg_mirrorentry = "n";
	    }
	  }

	  /**
	   * @param string
	   */
	  public void setNbr_coreaccount(String nbr_coreaccount) {
	    if (nbr_coreaccount != null) {
	      this.nbr_coreaccount = nbr_coreaccount;
	    }
	    else {
	      logger.fatal(" This value should not be null");
	    }
	  }

	  /**
	   * @param l
	   */
	  public void setNbr_instrlegid(long nbr_legid) {
	    if (nbr_legid != 0) {
	      this.nbr_legid = nbr_legid;
	    }
	    else {
	      logger.fatal(" This value should not be null");
	    }
	  }

	  /**
	   * @param l
	   */
	  public void setNbr_instrtxnid(long nbr_instrtxnid) {
	    if (nbr_instrtxnid != 0) {
	      this.nbr_instrtxnid = nbr_instrtxnid;
	    }
	    else {
	      logger.fatal(" This value should not be null");
	    }
	  }

	  /**
	   * @param string
	   */
	  public void setNbr_ownaccount(String nbr_ownaccount) {
	    if (nbr_ownaccount != null) {
	      this.nbr_ownaccount = nbr_ownaccount;
	    }
	    else {
	      logger.fatal(" This value should not be null");
	    }
	  }

	  /**
	   * @param l
	   */
	  public void setNbr_processid(long nbr_processid) {
	    if (nbr_processid != 0) {
	      this.nbr_processid = nbr_processid;
	    }
	    else {
	      logger.fatal(" This value should not be null");
	    }
	  }

	  /**
	   * @param string
	   */
	  public void setCod_bic(String cod_bic) {
	    this.cod_bic = cod_bic;
	  }

	  /**
	   * @param string
	   */
	  public void setTxt_extinfo1(String txt_extinfo1) {
	    this.txt_extinfo1 = txt_extinfo1;
	  }

	  /**
	   * @param string
	   */
	  public void setTxt_extinfo2(String txt_extinfo2) {
	    this.txt_extinfo2 = txt_extinfo2;
	  }

	  /**
	   * @param string
	   */
	  public void setTxt_extinfo3(String txt_extinfo3) {
	    this.txt_extinfo3 = txt_extinfo3;
	  }

	  /**
	   * @param string
	   */
	  public void setTxt_extinfo4(String txt_extinfo4) {
	    this.txt_extinfo4 = txt_extinfo4;
	  }

	  /**
	   * @param string
	   */
	  public void setTxt_extinfo5(String txt_extinfo5) {
	    this.txt_extinfo5 = txt_extinfo5;
	  }

	  /**
	   * @param string
	   */
	  public void setTxt_failure(String txt_failure) {
	    this.txt_failure = txt_failure;
	  }

	  /**
	   * @param string
	   */
	  public void setTxt_narration1(String txt_narration1) {
	    this.txt_narration1 = txt_narration1;
	  }

	  /**
	   * @param string
	   */
	  public void setTxt_narration2(String txt_narration2) {
	    this.txt_narration2 = txt_narration2;
	  }

	  /**
	   * @param string
	   */
	  public void setTxt_narration3(String txt_narration3) {
	    this.txt_narration3 = txt_narration3;
	  }

	  /**
	   * @param string
	   */
	  public void setTxt_narration4(String txt_narration4) {
	    this.txt_narration4 = txt_narration4;
	  }

	  /**
	   * @param string
	   */
	  public void setTxt_status(String txt_status) {
	    this.txt_status = txt_status;
	  }

	  /**
	   * @return
	   */
	  public long getNbr_acctentryid() {
	    return nbr_acctentryid;
	  }

	  /**
	   * @param l
	   */
	  public void setNbr_acctentryid(long nbr_acctentryid) {

	    if (nbr_acctentryid != 0) {
	      this.nbr_acctentryid = nbr_acctentryid;
	    }
	    else {
	      logger.fatal(" This value should not be null");
	    }

	  }

	  /**
	   * @return
	   */
	  public String getCod_bnkglent() {
	    return cod_bnkglent;
	  }

	  /**
	   * @return
	   */
	  public String getTxt_ibanacct() {
	    return txt_ibanacct;
	  }

	  /**
	   * @param string
	   */
	  public void setCod_bnkglent(String cod_bnkglent) {
	    if (cod_bnkglent != null) {
	      this.cod_bnkglent = cod_bnkglent;
	    }
	    else {
	      logger.fatal(" This value should not be null");
	    }

	  }

	  /**
	   * @param string
	   */
	  public void setTxt_ibanacct(String txt_ibanacct) {
	    if (txt_ibanacct != null) {
	      this.txt_ibanacct = txt_ibanacct;
	    }
	    else {
	      logger.fatal(" This value should not be null");
	    }

	  }

	  /**
	   * @return
	   */
	  public GregorianCalendar getDat_business() {
	    return dat_business;
	  }

	  /**
	   * @param calendar
	   */
	  public void setDat_business(GregorianCalendar calendar) {
	    dat_business = calendar;
	  }

	  /**
	   * @return
	   */
	  public String getFlg_forcedr() {
	    return flg_forcedr;
	  }

	  /**
	   * @return
	   */
	  public String getTxt_hostsystemref() {
	    return txt_hostsystemref;
	  }

	  /**
	   * @param string
	   */
	  public void setFlg_forcedr(String flg_forcedr) {
	    if (flg_forcedr != null) {
	      this.flg_forcedr = flg_forcedr;
	    }
	    else {
	      logger.fatal("This value cannot be null");
	    }

	  }

	  /**
	   * @param string
	   */
	  public void setTxt_hostsystemref(String txt_hostsystemref) {
	    this.txt_hostsystemref = txt_hostsystemref;
	  }
	  public long getNbr_lotid() {
	    return nbr_lotid;
	  }
	  public void setNbr_lotid(long nbr_lotid) {
	    this.nbr_lotid = nbr_lotid;
	  }
	/**
	 * 
	 * @return
	 */
	public String getFlg_srctgtTyp() {
		return flg_srctgtTyp;
		}
		public void setFlg_srctgtTyp(String flg_srctgtTyp) {
		  this.flg_srctgtTyp = flg_srctgtTyp;
		}
	
	
	/**
	 * @return
	 */
	public String getFlg_status1() {
		return flg_status1;
	}

	/**
	 * @param string
	 */
	public void setFlg_status1(String string) {
		flg_status1 = string;
	}
	/**
	 * @return
	 */
	public String getNbr_ownsettle() {
		return nbr_ownsettle;
	}
	/**
	 * @param string
	 */
	public void setNbr_ownsettle(String nbr_ownsettle) {
		this.nbr_ownsettle = nbr_ownsettle;
	}
	/**
	 * @return
	 */
	public String getNbr_coresettle() {
		return nbr_coresettle;
	}
	/**
	 * @param string
	 */
	public void setNbr_coresettle(String nbr_coresettle) {
		this.nbr_coresettle = nbr_coresettle;
	}
	/**
	 * @return
	 */
	public GregorianCalendar getDat_posting() {
		return dat_posting;
	}
	/**
	 * @param string
	 */
	public void setDat_posting(GregorianCalendar dat_posting) {
		this.dat_posting = dat_posting;
	}
	/**
	 * @return
	 */
	public GregorianCalendar getDat_final_settlement() {
		return dat_final_settlement;
	}
	/**
	 * @param string
	 */
	public void setDat_final_settlement(GregorianCalendar dat_final_settlement) {
		this.dat_final_settlement = dat_final_settlement;
	}
	/**
	 * @return
	 */
	public GregorianCalendar getDat_value() {
		return dat_value;
	}
	/**
	 * @param string
	 */
	public void setDat_value(GregorianCalendar dat_value) {
		this.dat_value = dat_value;
	}
	/**
	 * @return
	 */
	public String getTxt_extinfo6() {
		return txt_extinfo6;
	}
	/**
	 * @param string
	 */
	public void setTxt_extinfo6(String txt_extinfo6) {
		this.txt_extinfo6 = txt_extinfo6;
	}
	/**
	 * @return
	 */
	public String getTxt_extinfo7() {
		return txt_extinfo7;
	}
	/**
	 * @param string
	 */
	public void setTxt_extinfo7(String txt_extinfo7) {
		this.txt_extinfo7 = txt_extinfo7;
	}
	/**
	 * @return
	 */
	public String getTxt_extinfo8() {
		return txt_extinfo8;
	}
	/**
	 * @param string
	 */
	public void setTxt_extinfo8(String txt_extinfo8) {
		this.txt_extinfo8 = txt_extinfo8;
	}
	/**
	 * @return
	 */
	public String getTxt_extinfo9() {
		return txt_extinfo9;
	}
	/**
	 * @param string
	 */
	public void setTxt_extinfo9(String txt_extinfo9) {
		this.txt_extinfo9 = txt_extinfo9;
	}
	/**
	 * @return
	 */
	public String getTxt_extinfo10() {
		return txt_extinfo10;
	}
	/**
	 * @param string
	 */
	public void setTxt_extinfo10(String txt_extinfo10) {
		this.txt_extinfo10 = txt_extinfo10;
	}
	//SUP_IS173 starts
	/**
	 * @return the cod_incoid
	 */
	public String getCod_incoid() {
		return cod_incoid;
	}

	/**
	 * @param cod_incoid the cod_incoid to set
	 */
	public void setCod_incoid(String cod_incoid) {
		this.cod_incoid = cod_incoid;
	}
	
	/**
	 * @return the cod_agreement
	 */
	public String getCod_agreement() {
		return cod_agreement;
	}

	/**
	 * @param cod_agreement the cod_agreement to set
	 */
	public void setCod_agreement(String cod_agreement) {
		this.cod_agreement = cod_agreement;
	}
	//SUP_IS173 ends
	
	public String getFlg_entrytype() {
		return flg_entrytype;
	}

	public void setFlg_entrytype(String flg_entrytype) {
		this.flg_entrytype = flg_entrytype;
	}
	
}