/**
 *  Copyright  2005   Intellect Design Arena Ltd. All rights reserved.
 *
 *  These materials are confidential and proprietary to Intellect Design Arena Ltd.
 *  and no part of these materials should be reproduced, published, transmitted or
 *  distributed  in any form or by any means, electronic, mechanical, photocopying,
 *  recording or otherwise, or stored in any information storage or retrieval system
 *  of any nature nor should the materials be disclosed to third parties or used in
 *  any other manner for which this is not authorized, without the prior express
 *  written authorization of Intellect Design Arena Ltd.
 *
 * <p>Title       					: Intellect Liquidity</p>
 * <p>Description 					: This Class Generates the AE Template  and Populates the Accounting Entry Table</p>
 * <p>SCF NO      					:   </p>
 * <p>Copyright   					: Copyright 2016 Intellect Design Arena Limited. All rights reserved.</p>
 * <p>Company     					: Intellect Design Arena Limited</p>
 * <p>Date of Creation 					: 17-Jun-2010</p>
 * <p>Source      					: AccountingEntryTemplateUtility.java </p>
 * <p>Package     					: com.intellectdesign.cash.accountingentry </p>
 * 
 * @author Jitendra Behera
 * @version 1.0
 * 
 * <p>------------------------------------------------------------------------------------</p>
 * <p>MODIFICATION HISTORY:</p>
 * <p>------------------------------------------------------------------------------------</p>
 * <p>SERIAL      AUTHOR		    			DATE					SCF				DESCRIPTION</p>
 * <p>------------------------------------------------------------------------------------</p>
 * <p>1       		Jitendra Behera		  19/06/2010				Initial Version.</p>
 * <p>2       		Jitendra Behera		  04/10/2010				BILATERALSETTLEMENT Changes</p>
 * <p>3       		Jitendra Behera		  19/10/2010				Settlement path 'D' used for Delocalize CBT Model</p>
 * <p>4             Ravikumar Maladi      04/01/2011                Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005.</p> 
 * <p>5       		Sandeep M.		  01/02/2011		JTest changes		JTest JDBC changes</p>
 * <p>				28/04/2011			  Vijay Kadam				JTEST FIX on 28ARIL11		JTest JDBC changes</p>
 * <p>6                    Nitin W               20/04/2011                While updating contract for with specific currency , all currency related to the contract where updated  Retro From Release10.2_Support_10.2_SIT022 to Release10.2_DEV_LIQ_RELEASE10.6_SIT007
 * <p>7		    Shripad_Retro Nitin		13/09/2011			HSBC_DEF_FIX_888				Wrong cod_triggering_gl population in src host as cbt due to which setlmnt is happening at wrong time. Retro From Release10.2_Support_10.2_SIT022 to Release10.2_DEV_LIQ_RELEASE10.6_SIT007
 * <p>8   Prateek Aole			11/05/2012		 		        		JTest Fixes</p>
* <p> 9	    kalpana Manjare      15/10/2012	Retro_Mashreq	(Retro from Mashreq LIQ_RELEASE10.5_SIT014.12.17)
* <p>10     Harikesh Singh       08/12/2012   SUP_ANZ_5103               Accounting Entry is not getting generated for online instructions
*<p>11		    Shripad Kale		25/09/2012		HSBC_OSL_2643_CR_ICL_INTR		Changes for Main Nostro Model.
*<p>12		    Amit Kumar		12/12/2013			Sweep_ENH Support 2864		Code added in generateAEFromTemplate() method for DAG and Drain POOL
 *<p>13	Amit Kumar 	04-JAN-2014		Sweeps_ENH for issue id 2862
 *<p>14 jan 2014		Megha Maheshwari	ENH_Nostroloro_branch		Branch for Nostro loro has been added
 *<p>15  Amit Kumar 			13/02/2014   	  JTest Changes 11.5.1   JTest Fixes</p>
 *<p>16  Sandeep M. 			11/08/2014   	  SUP_IS6597   DM/CM issue fixed </p>
 *<p>17 Anita Suhanda			14/08/2014			SUP_IS6597 for Nostroloro legs
 *<p>18 Sandeep M.			SUPIS_123			for Nostroloro legs for DM/CM for bodreversal
 *<p>19 Anita Suhanda		    03/09/2014			RBS - 132 ActiveTransaction Nostro Leg
 *<p>20 	Sandeep M.			SUPIS_157			PENDING accounting entry move into history table
 *<p>21 Amit Kumar Jha		    10/09/2014			RBS - 153,154 ICL Nostro Leg
 *<p>22	Anita Suhanda			30/10/2014			SUP_RBS_226 Transaction codes for n leg
 *<p>23 Anita Suhanda			11/04/2014			SUP_RBS_239	WHT n leg Accounting Entry.
 *<p>24 Deeksha Prakash         06/08/2015          FIX_FOR_BLE END </p>
 *<p>25 Deeksha Prakash         13/08/2015          Duplicate Condition Removed - Not required for BLE </p>
 *<p>26 Bibek Satapathy	 	  	09/09/2015  ENH_15.2_LOG_FIXES   Logger enabling Check Added </p> 
 *<p>27 Rahul Yerande 	  	   16/11/2015  15.2_PERFORMANCE_CHANGE    </p> 
 *<p>28 Deeksha Prakash	  	   29/02/2016  PERFORMANCE FIXES    </p> 
 *<p>29 DeekshaP	    	   18-May-2016 		LMS_45_PERF_CERTIFICATION_PF78				Performance Changes	</p>
 */

package com.intellectdesign.cash.accountingentry;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

import com.orbitech.frameworks.mline.DBUtils;
import com.intellectdesign.cash.lms.constants.LMSConstants;
import com.intellectdesign.cash.lms.execution.sweeps.constant.ExecutionConstants;
import com.intellectdesign.cash.lms.execution.sweeps.messagelogger.MessageEvent;
import com.intellectdesign.cash.lms.execution.sweeps.messagelogger.MessageLogger;
import com.intellectdesign.cash.sweeps.util.LMSConnectionUtility;
import com.intellectdesign.cash.sweeps.util.LMSUtility;
import com.intellectdesign.exception.LMSException;
import com.polaris.intellect.commons.logging.Log;
import com.polaris.intellect.commons.logging.LogFactory;

/**
 * 
 * @author jitendra.behera
 *
 */
public class AccountingEntryTemplateUtility {

    private static Log logger = LogFactory.getLog(AccountingEntryTemplateUtility.class.getName());
    private int legCount = 3;
    boolean flgPersistTemplate = true; 
    /**
     * Constructor....	
     *     
     */     
    public AccountingEntryTemplateUtility(){
                
    }

    /**
     * 
     * @param referenceType
     * @param referenceId
     * @throws LMSException
     */
    public void generarteAE(String referenceType, String referenceId) throws LMSException{
        Connection conn = null;
        PreparedStatement pstmt = null;
        PreparedStatement pstmtSettlemet = null;

        String templateInsertQry = "INSERT INTO OLM_AETEMPLATE_DTLS (NBR_TEMPLATE_ID, FLG_DRCR_SET , NBR_LEGID , " +
        "NBR_OWNACCOUNT, NBR_COREACCOUNT, COD_BIC, COD_BNKLGLENT, COD_BRANCH, COD_CCY, " +
        "COD_GL, COD_COUNTRY, FLG_ACCTTYPE, FLG_DRCR, " +
        "FLG_SRCTGTTYP, TXT_IBANACCT,NBR_CONTRACT_NO,NBR_SEQ,FLG_ISSETTLEMENT_ENTRY,NBR_SETTLEMENT_DAYS, " +
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
        //"DAT_EFFECTIVE,COD_TRIGGERING_GL,DAT_END " +
        //") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
        "DAT_EFFECTIVE,COD_TRIGGERING_GL,DAT_END,FLG_SETTLEMENTPATH " +
        ") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
        
        
        //String endDateUpdateQuery = "UPDATE OLM_AETEMPLATE_DTLS SET DAT_END = ? WHERE NBR_TEMPLATE_ID = ? ";
        String endDateUpdateQuery = "UPDATE OLM_AETEMPLATE_DTLS SET DAT_END = ? WHERE NBR_TEMPLATE_ID = ? and (dat_end is Null OR dat_end > ? ) ";
        PreparedStatement pstmtEndDateUpdate = null;
        
	try{
		if(logger.isDebugEnabled())//JTest changes on 01-feb-11
	    	logger.debug("generarteAE....");
	    

	    boolean insertFlag = false; 
	    conn = LMSConnectionUtility.getConnection();
	    pstmt = conn.prepareStatement(templateInsertQry);
	    pstmtSettlemet = conn.prepareStatement(templateInsertQry);
	    pstmtEndDateUpdate = conn.prepareStatement(endDateUpdateQuery);
	    if(logger.isDebugEnabled()){//JTest changes on 01-feb-11
	    	logger.debug("referenceType :: "+referenceType);
	    	logger.debug("referenceId :: "+referenceId);
		}
		//RBS - 132 ActiveTransaction Nostro Leg Start
	    if(referenceType.equals("CS")){
			//getCSDetailsAndgenerateAE(referenceId, conn, pstmt, pstmtSettlemet);
		getCSDetailsAndgenerateAE(referenceType,referenceId, conn, pstmt, pstmtSettlemet);
	    }else if(referenceType.equals("NL")) {
       //		getNLDetailsAndgenerateAE(referenceId, conn, pstmt, pstmtSettlemet,pstmtEndDateUpdate);
		  getNLDetailsAndgenerateAE(referenceType,referenceId, conn, pstmt, pstmtSettlemet,pstmtEndDateUpdate);
	    }

	   
	    else if(referenceType.equals("AT")) {
			getATDetailsAndgenerateAE(referenceType,referenceId, conn, pstmt, pstmtSettlemet);
	    }
	    //RBS - 132 ActiveTransaction Nostro Leg Ends
			// RBS - 153,154 ICL Nostro Leg
			else if (referenceType.equals("ICL")) {
				getICLDetailsAndgenerateAE(referenceType, referenceId, conn,
						pstmt, pstmtSettlemet);
			}
			// RBS - 153,154 ICL Nostro Leg

	  
	    /*if(flgPersistTemplate){
		
		int endUpdate[] = pstmtEndDateUpdate.executeBatch();
		logger.debug("endUpdate :: "+endUpdate.length);
		
		int a[] = pstmt.executeBatch();
		insertFlag = true;
		logger.debug("generarteAE...."+a.length);
	    }

	    if(insertFlag){
		int a[] = pstmtSettlemet.executeBatch();
		logger.debug("generarteAE...."+a.length);
	    }
*/

	}catch(Exception e){
	    logger.fatal("generarteAE...."+e);
	    throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, "", e);
	    //TODO exception is to be handled
	}
	finally
	{
		LMSConnectionUtility.close(pstmtEndDateUpdate);//JTest changes on 01-feb-11
	    LMSConnectionUtility.close(pstmt);
	    LMSConnectionUtility.close(pstmtSettlemet);
	    LMSConnectionUtility.close(conn);
	    
	    
	}
	if(logger.isDebugEnabled())//JTest changes on 01-feb-11
    	logger.debug("generarteAE....Leaving....");
    }


    /**
     * 
     * @param nbrStructureId
     * @throws LMSException
     */
  //RBS - 132 ActiveTransaction Nostro Leg Start
//  private void getCSDetailsAndgenerateAE(String nbrStructureId, Connection conn, PreparedStatement pstmt, PreparedStatement pstmtSettlemet) throws LMSException{
    private void getCSDetailsAndgenerateAE(String referenceType,String nbrStructureId, Connection conn, PreparedStatement pstmt, PreparedStatement pstmtSettlemet) throws LMSException{
    	//RBS - 132 ActiveTransaction Nostro Leg Ends
    	if(logger.isDebugEnabled())//JTest changes on 01-feb-11
	    	logger.debug("getCSDetailsAndgenerateAE....");
    	//JTest changes on 01-feb-11 starts
	//Statement stmt = null;
    PreparedStatement stmt = null;
	boolean insertFlag = true;
	PreparedStatement updEffDate = null;
	ResultSet rs = null;
	//JTest changes on 01-feb-11 ends
	
	try{
	  //RBS - 132 ActiveTransaction Nostro Leg Start
		//int masterCount = generateAETemplateMaster(nbrStructureId, conn);
	    int masterCount = generateAETemplateMaster(referenceType,nbrStructureId, conn); 
		//RBS - 132 ActiveTransaction Nostro Leg Ends
	    if(logger.isDebugEnabled())//JTest changes on 01-feb-11
	    	logger.debug("masterCount...."+masterCount);

	    if(masterCount > 0 ){
		String insertDtlSql = "";
//where dat_end>=dat_business
		if("HostSystem".equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
		    /*insertDtlSql = "SELECT DISTINCT OSAD.COD_GLSRCACCT, OSAD.COD_CCYSRCACCT, OSAD.FLG_SRC_ACC_TYPE, OSAD.COD_GLTGTACCT, OSAD.COD_CCYTGTACCT, OSAD.FLG_TGT_ACC_TYPE, OAM.NBR_TEMPLATE_ID FROM OLM_SOURCE_ACCOUNT_DTLS OSAD, " +
		    "OLM_AETEMPLATE_MST OAM WHERE " +
		    "OSAD.NBR_STRCID = '"+nbrStructureId+"' " +
		    "AND ((OSAD.COD_GLSRCACCT||OSAD.COD_GLTGTACCT||OSAD.COD_CCYSRCACCT) IN " +
		    "(OAM.COD_ENTITY1||OAM.COD_ENTITY2||OAM.COD_CCY)) " +
		    "AND OAM.NBR_TEMPLATE_ID NOT IN (SELECT NBR_TEMPLATE_ID FROM OLM_AETEMPLATE_DTLS) ";*/
		    insertDtlSql ="SELECT DISTINCT OSAD.COD_GLSRCACCT, OSAD.COD_CCYSRCACCT, " +
		    		"OSAD.FLG_SRC_ACC_TYPE, OSAD.COD_GLTGTACCT, OSAD.COD_CCYTGTACCT, " +
		    		"OSAD.FLG_TGT_ACC_TYPE, OAM.NBR_TEMPLATE_ID FROM OLM_SOURCE_ACCOUNT_DTLS OSAD, " +
		    		//"OLM_AETEMPLATE_MST OAM WHERE OSAD.NBR_STRCID = '"+nbrStructureId+"' AND OAM.TYP_ENTITY = '"+ExecutionConstants.ENTITY_TYPE+"' " +
		    		"OLM_AETEMPLATE_MST OAM WHERE OSAD.NBR_STRCID = ? AND OAM.TYP_ENTITY = ? " +//JTest changes on 01-feb-11
		    		"AND ((   OSAD.COD_GLSRCACCT || OSAD.COD_GLTGTACCT || OSAD.COD_CCYSRCACCT ) " +
		    		"IN (OAM.COD_ENTITY1 || OAM.COD_ENTITY2 || OAM.COD_CCY) ) " +
		    		"AND NOT EXISTS (SELECT OAD.NBR_TEMPLATE_ID FROM OLM_AETEMPLATE_DTLS OAD " +
		    		"WHERE OAD.NBR_TEMPLATE_ID = OAM.NBR_TEMPLATE_ID AND " +
		    		"(SELECT MIN(DAT_TODAY) FROM OLM_HOSTSYSTEMDATES WHERE COD_GL IN(OSAD.COD_GLSRCACCT,OSAD.COD_GLTGTACCT)) " +
		    		"BETWEEN OAD.DAT_EFFECTIVE AND NVL(OAD.DAT_END, TO_DATE('31-DEC-9999','DD-MON-YYYY')))";
		}else if("BLE".equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
		    /*insertDtlSql = "SELECT DISTINCT OSAD.COD_GLSRCACCT, OSAD.COD_CCYSRCACCT, OSAD.FLG_SRC_ACC_TYPE, OSAD.COD_GLTGTACCT, OSAD.COD_CCYTGTACCT, OSAD.FLG_TGT_ACC_TYPE, SRCACCT.COD_BNK_LGL COD_SRCBNKLGLENT,TGTACCT.COD_BNK_LGL COD_TGTBNKLGLENT, " +
		    		"OAM.NBR_TEMPLATE_ID FROM OLM_SOURCE_ACCOUNT_DTLS OSAD,ORBICASH_ACCOUNTS SRCACCT, " +
		    		"ORBICASH_ACCOUNTS TGTACCT, OLM_AETEMPLATE_MST OAM " +
		    		"WHERE OSAD.NBR_STRCID = '"+nbrStructureId+"' AND OAM.TYP_ENTITY ='BLE' " +
		    		"AND OSAD.NBR_SRCACCT = SRCACCT.NBR_OWNACCOUNT " +
		    		"AND OSAD.NBR_TGTACCT = TGTACCT.NBR_OWNACCOUNT " +
		    		"AND ((SRCACCT.COD_BNK_LGL || TGTACCT.COD_BNK_LGL || OSAD.COD_CCYSRCACCT " +
		    		") IN (OAM.COD_ENTITY1 || OAM.COD_ENTITY2 || OAM.COD_CCY)) " +
		    		"AND OAM.NBR_TEMPLATE_ID NOT IN (SELECT NBR_TEMPLATE_ID FROM OLM_AETEMPLATE_DTLS) ";*/
		    insertDtlSql = "SELECT DISTINCT OSAD.COD_GLSRCACCT, OSAD.COD_CCYSRCACCT, OSAD.FLG_SRC_ACC_TYPE, " +
		    		"OSAD.COD_GLTGTACCT, OSAD.COD_CCYTGTACCT, OSAD.FLG_TGT_ACC_TYPE, " +
		    		"SRCACCT.COD_BNK_LGL COD_SRCBNKLGLENT, TGTACCT.COD_BNK_LGL COD_TGTBNKLGLENT, OAM.NBR_TEMPLATE_ID " +
		    		"FROM OLM_SOURCE_ACCOUNT_DTLS OSAD, ORBICASH_ACCOUNTS SRCACCT, ORBICASH_ACCOUNTS TGTACCT, " +
		    		//JTest changes on 01-feb-11 starts
		    		/*"OLM_AETEMPLATE_MST OAM WHERE OSAD.NBR_STRCID = '"+nbrStructureId+"' " +
		    		"AND OAM.TYP_ENTITY = '"+ExecutionConstants.ENTITY_TYPE+"' AND OSAD.NBR_SRCACCT = SRCACCT.NBR_OWNACCOUNT AND " +*/
		    		"OLM_AETEMPLATE_MST OAM WHERE OSAD.NBR_STRCID = ? " +
		    		"AND OAM.TYP_ENTITY = ? AND OSAD.NBR_SRCACCT = SRCACCT.NBR_OWNACCOUNT AND " +
		    		//JTest changes on 01-feb-11 ends
		    		"OSAD.NBR_TGTACCT = TGTACCT.NBR_OWNACCOUNT " +
		    		"AND ((   SRCACCT.COD_BNK_LGL || TGTACCT.COD_BNK_LGL || OSAD.COD_CCYSRCACCT ) IN " +
		    		"(OAM.COD_ENTITY1 || OAM.COD_ENTITY2 || OAM.COD_CCY) ) " +
		    		"AND NOT EXISTS (SELECT OAD.NBR_TEMPLATE_ID FROM OLM_AETEMPLATE_DTLS OAD " +
		    		"WHERE OAD.NBR_TEMPLATE_ID = OAM.NBR_TEMPLATE_ID AND " +
		    		"(SELECT MIN(DAT_TODAY) FROM OLM_HOSTSYSTEMDATES WHERE COD_GL IN(OSAD.COD_GLSRCACCT,OSAD.COD_GLTGTACCT)) " +
		    		"BETWEEN OAD.DAT_EFFECTIVE  AND NVL(OAD.DAT_END, TO_DATE('31-DEC-9999','DD-MON-YYYY')))";
		}
		//ENH_Nostroloro_branch start
		else if(ExecutionConstants.NOSTRO_LORO_BRANCH.equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
			
			//insertDtlSql = "SELECT DISTINCT osad.cod_glsrcacct, osad.cod_ccysrcacct, osad.flg_src_acc_type, osad.cod_gltgtacct, osad.cod_ccytgtacct, osad.flg_tgt_acc_type, osad.cod_brsrcacct, osad.cod_brtgtacct, oam.nbr_template_id FROM olm_source_account_dtls osad, olm_aetemplate_mst oam WHERE osad.nbr_strcid = ? AND oam.typ_entity = ? AND ((   osad.cod_brsrcacct || osad.cod_brtgtacct || osad.cod_ccysrcacct ) IN (oam.cod_entity1 || oam.cod_entity2 || oam.cod_ccy) ) AND NOT EXISTS ( SELECT oad.nbr_template_id FROM olm_aetemplate_dtls oad WHERE oad.nbr_template_id = oam.nbr_template_id AND (SELECT MIN (dat_today) FROM olm_hostsystemdates WHERE cod_gl IN (osad.cod_glsrcacct, osad.cod_gltgtacct)) BETWEEN oad.dat_effective AND NVL (oad.dat_end, TO_DATE ('31-DEC-9999', 'DD-MON-YYYY' ) ))";
			insertDtlSql = "SELECT DISTINCT osad.cod_glsrcacct, osad.cod_ccysrcacct, osad.flg_src_acc_type, osad.cod_gltgtacct, osad.cod_ccytgtacct,"
				+" osad.flg_tgt_acc_type, osad.cod_brsrcacct, osad.cod_brtgtacct, oam.nbr_template_id FROM olm_source_account_dtls osad, " 
				+" olm_aetemplate_mst oam WHERE osad.nbr_strcid = ? AND oam.typ_entity = ? AND (( "
				+" osad.cod_brsrcacct || osad.cod_brtgtacct || osad.cod_ccysrcacct ) IN (oam.cod_entity1 || oam.cod_entity2 || oam.cod_ccy) ) AND "
				+" (   osad.flg_groupsweeprule IS NULL OR osad.flg_groupsweeprule NOT IN ('CM', 'DM') ) AND NOT EXISTS "
				+" ( SELECT oad.nbr_template_id FROM olm_aetemplate_dtls oad WHERE oad.nbr_template_id = oam.nbr_template_id AND "
				+" (SELECT MIN (dat_today) FROM olm_hostsystemdates WHERE cod_gl IN (osad.cod_glsrcacct, osad.cod_gltgtacct)) " +
				" BETWEEN oad.dat_effective AND NVL (oad.dat_end, TO_DATE ('31-DEC-9999', 'DD-MON-YYYY' ) )) " +
				" UNION SELECT DISTINCT osad.cod_glsrcacct, osad.cod_ccysrcacct, osad.flg_src_acc_type, osad.cod_gltgtacct, " +
				" osad.cod_ccytgtacct, osad.flg_tgt_acc_type, osad.cod_brsrcacct, ?, oam.nbr_template_id FROM olm_source_account_dtls osad, " +
				" olm_aetemplate_mst oam WHERE osad.nbr_strcid = ? AND oam.typ_entity = ? AND osad.cod_brsrcacct != ? AND" +
				"  ((osad.cod_brsrcacct || ? || osad.cod_ccysrcacct) IN (oam.cod_entity1 || oam.cod_entity2 || oam.cod_ccy) )" +
				"  AND osad.flg_groupsweeprule IN ('CM', 'DM') AND NOT EXISTS ( SELECT oad.nbr_template_id FROM olm_aetemplate_dtls oad" +
				" WHERE oad.nbr_template_id = oam.nbr_template_id AND (SELECT MIN (dat_today) FROM olm_hostsystemdates WHERE cod_gl " +
				" IN (osad.cod_glsrcacct, osad.cod_gltgtacct)) BETWEEN oad.dat_effective AND NVL " +
				" (oad.dat_end, TO_DATE ('31-DEC-9999', 'DD-MON-YYYY' ) )) UNION SELECT DISTINCT osad.cod_glsrcacct, osad.cod_ccysrcacct, " +
				" osad.flg_src_acc_type, osad.cod_gltgtacct, osad.cod_ccytgtacct, osad.flg_tgt_acc_type, cod_brtgtacct, ?, " +
				" oam.nbr_template_id FROM olm_source_account_dtls osad, olm_aetemplate_mst oam WHERE osad.nbr_strcid = ? AND " +
				" oam.typ_entity = ? AND osad.cod_brtgtacct != ? AND " +
				" ((osad.cod_brtgtacct || ? || osad.cod_ccysrcacct) IN (oam.cod_entity1 || oam.cod_entity2 || oam.cod_ccy) ) AND " +
				" osad.flg_groupsweeprule IN ('CM', 'DM') AND NOT EXISTS ( SELECT oad.nbr_template_id FROM olm_aetemplate_dtls oad " +
				" WHERE oad.nbr_template_id = oam.nbr_template_id AND (SELECT MIN (dat_today) FROM olm_hostsystemdates WHERE cod_gl " +
				" IN (osad.cod_glsrcacct, osad.cod_gltgtacct)) BETWEEN oad.dat_effective AND NVL " +
				" (oad.dat_end, TO_DATE ('31-DEC-9999', 'DD-MON-YYYY' ) ))";
		}
		//ENH_Nostroloro_branch end
		//JTest changes on 01-feb-11 starts
		if(logger.isDebugEnabled())
	    	logger.debug("insertDtlSql :: "+insertDtlSql);
		
		//stmt = conn.createStatement();
		stmt = conn.prepareStatement(insertDtlSql);
		stmt.setString(1, nbrStructureId);
		stmt.setString(2, ExecutionConstants.ENTITY_TYPE);
		if("Branch".equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
			
			stmt.setString(3, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
			stmt.setString(4, nbrStructureId);
			stmt.setString(5, ExecutionConstants.ENTITY_TYPE);
			stmt.setString(6, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
			stmt.setString(7, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
			stmt.setString(8, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
			stmt.setString(9, nbrStructureId);
			stmt.setString(10, ExecutionConstants.ENTITY_TYPE);
			stmt.setString(11, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
			stmt.setString(12, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
		}
		//JTest changes on 01-feb-11 ends
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		//Below Query Updated - BILATERALSETTLEMENT
		//Effective Date & End Date Updation
		/*String updateTemplateEffDate = "UPDATE OLM_AETEMPLATE_DTLS SET DAT_EFFECTIVE = (SELECT MAX (DAT_EFFECTIVE) " +
				"FROM OLM_AETEMPLATE_DTLS WHERE NBR_TEMPLATE_ID = ? ), DAT_END = " +
				"(SELECT MIN (DAT_END) FROM OLM_AETEMPLATE_DTLS WHERE NBR_TEMPLATE_ID = ?) " +
				"WHERE NBR_TEMPLATE_ID = ? ";*/
		String updateTemplateEffDate = "UPDATE OLM_AETEMPLATE_DTLS OAD SET(DAT_EFFECTIVE,DAT_END) = " +
				"(SELECT CASE WHEN OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' " +
				"AND B.EFFDATE < C.EFFDATE THEN C.EFFDATE ELSE B.EFFDATE END, " +
				"CASE WHEN OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' " +
				"AND B.DATEND > C.DATEND THEN C.DATEND ELSE B.DATEND END " +
				"FROM( SELECT NBR_TEMPLATE_ID,FLG_ISSETTLEMENT_ENTRY," +
				"MAX(DAT_EFFECTIVE) EFFDATE,MIN (DAT_END) DATEND " +
				"FROM OLM_AETEMPLATE_DTLS WHERE  NBR_TEMPLATE_ID = ? " +
				"GROUP BY NBR_TEMPLATE_ID,FLG_ISSETTLEMENT_ENTRY )B, " +
				"(SELECT NBR_TEMPLATE_ID,MAX(EFFDATE) EFFDATE,MIN (DATEND) DATEND " +
				"FROM ( SELECT NBR_TEMPLATE_ID,FLG_ISSETTLEMENT_ENTRY,MAX(DAT_EFFECTIVE) EFFDATE," +
				"MIN (DAT_END) DATEND FROM OLM_AETEMPLATE_DTLS WHERE  NBR_TEMPLATE_ID = ? " +
				"GROUP BY NBR_TEMPLATE_ID,FLG_ISSETTLEMENT_ENTRY )GROUP BY NBR_TEMPLATE_ID ) C " +
				"WHERE B.NBR_TEMPLATE_ID = C.NBR_TEMPLATE_ID  " +
				"AND OAD.FLG_ISSETTLEMENT_ENTRY  = B.FLG_ISSETTLEMENT_ENTRY " +
				"AND OAD.NBR_TEMPLATE_ID = ?  ) WHERE NBR_TEMPLATE_ID = ? ";
		if(logger.isDebugEnabled())//JTest changes on 01-feb-11
	    	logger.debug("updateTemplateEffDate :: "+updateTemplateEffDate);
		// - BILATERALSETTLEMENT
		updEffDate = conn.prepareStatement(updateTemplateEffDate); //JTest changes on 01-feb-11
		//Effective Date & End Date Updation
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		rs = stmt.executeQuery();//JTest changes on 01-feb-11
		while(rs.next()){
		    legCount = 3;
		    flgPersistTemplate = true;
		    insertFlag = false;
		    
		    AccountingEntryTemplateDO srcTemplateDo = new AccountingEntryTemplateDO();
		    AccountingEntryTemplateDO tgtTemplateDo = new AccountingEntryTemplateDO();
		    populateAETemplateDO(rs,srcTemplateDo,tgtTemplateDo);
		    
		    if(logger.isDebugEnabled()){//JTest changes on 01-feb-11
		    	logger.debug("Calling generateAETemplateDetail For Entity1"+srcTemplateDo);
		    	logger.debug("Calling generateAETemplateDetail For Entity2"+tgtTemplateDo);
		    }
		    generateAETemplateDetail(srcTemplateDo,tgtTemplateDo,"DO", conn, pstmt, pstmtSettlemet);
		    
		    if(flgPersistTemplate){
			int a[] = pstmt.executeBatch();
			insertFlag = true;
			if(logger.isDebugEnabled())//JTest changes on 01-feb-11
		    	logger.debug("getCSDetailsAndgenerateAE...."+a.length);
		    }
		    else
		    {
		    	   HashMap hp = new HashMap();
		    	   hp.put("COD_GLSRCACCT", srcTemplateDo.getCodGl());
		    	   hp.put("COD_GLTGTACCT", tgtTemplateDo.getCodGl());
		    	   hp.put("COD_CCY", srcTemplateDo.getCodCcy());
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		    	   // Jiten - BILATERALSETTLEMENT		    	   
		    	   hp.put("MSGCODE", "M0161");
					
		    	   if(logger.isDebugEnabled())//JTest changes on 01-feb-11
		   	    	logger.debug("Message is :::::.... "+hp);
		    	   
		    	   // - Jiten BILATERALSETTLEMENT - Commented the Code as Method name changed
				    //alertForPathNotCompleted(hp);
		    	   		generateAlert(hp);
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		    }

		    if(insertFlag){
			int a[] = pstmtSettlemet.executeBatch();
			if(logger.isDebugEnabled())//JTest changes on 01-feb-11
		    	logger.debug("getCSDetailsAndgenerateAE...."+a.length);
			
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
			//Effective Date & End Date Updation
			updEffDate.setString(1, rs.getString("NBR_TEMPLATE_ID"));
			updEffDate.setString(2, rs.getString("NBR_TEMPLATE_ID"));
			updEffDate.setString(3, rs.getString("NBR_TEMPLATE_ID"));
			// - BILATERALSETTLEMENT
			updEffDate.setString(4, rs.getString("NBR_TEMPLATE_ID"));
			// - BILATERALSETTLEMENT
			
			int updCnt = updEffDate.executeUpdate();
			if(logger.isDebugEnabled())//JTest changes on 01-feb-11
		    	logger.debug("Updating the Effective Date & End Date Count...."+updCnt);			
			//Effective Date & End Date Updation
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		    }
		    
		    pstmt.clearBatch();
		    pstmtSettlemet.clearBatch();
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		    //Effective Date & End Date Updation
		    updEffDate.clearParameters();
		    //Effective Date & End Date Updation
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		}
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		//Effective Date & End Date Updation
		LMSConnectionUtility.close(updEffDate);
		//Effective Date & End Date Updation
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
	    }
	}catch(Exception e){
	    logger.fatal("getCSDetailsAndgenerateAE...."+e);
	    throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, "", e);
	}
	finally{
		//JTest changes on 01-feb-11 starts
		LMSConnectionUtility.close(rs);
		LMSConnectionUtility.close(updEffDate);
		LMSConnectionUtility.close(stmt);
		//JTest changes on 01-feb-11 ends 
	}
	if(logger.isDebugEnabled())//JTest changes on 01-feb-11
    	logger.debug("getCSDetailsAndgenerateAE....Leaving");
	return;
    }


    /**
     * 
     * @param referenceId
     * @throws LMSException
     */
	 //RBS - 132 ActiveTransaction Nostro Leg Start
//	 private void getNLDetailsAndgenerateAE(String referenceId, Connection conn, PreparedStatement pstmt, PreparedStatement pstmtSettlemet,PreparedStatement pstmtEndDateUpdate) throws LMSException{
    private void getNLDetailsAndgenerateAE(String referenceType,String referenceId, Connection conn, PreparedStatement pstmt, PreparedStatement pstmtSettlemet,PreparedStatement pstmtEndDateUpdate) throws LMSException{
	//RBS - 132 ActiveTransaction Nostro Leg Ends
	ResultSet rs = null;
	//JTest changes on 01-feb-11 starts
	//Statement stmt = null;
	PreparedStatement stmt = null;
	//PreparedStatement pstmtUpdate = null;
	PreparedStatement pstmtUpdateEffDate = null;
	//JTest changes on 01-feb-11 ends
	PreparedStatement pstmtSelectDetail = null;
	boolean insertFlag =false;
	if(logger.isDebugEnabled())//JTest changes on 01-feb-11
		logger.debug("Inside getNLDetailsAndgenerateAE........");
	try{
	    
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
	    // - BILATERALSETTLEMENT Commented the below Code
	    /*String entity1 = "";
	    String entity2 = "";
	    String selectEntity = "SELECT DISTINCT COD_ENTITY_LORO,COD_ENTITY_NOSTRO FROM OLM_NOSTROLORO_DTLS WHERE NBR_CONTRACT_NO = '"+referenceId+"'";
	    
	    stmt = conn.createStatement();
	    rs = stmt.executeQuery(selectEntity);
	    if(rs.next()){
		entity1 = rs.getString("COD_ENTITY_LORO");
		entity2 = rs.getString("COD_ENTITY_NOSTRO");
	    }
	    
	    logger.debug("entity1 :: "+entity1);
	    logger.debug("entity2 :: "+entity2);
	    
	    rs.close();
	    stmt.close();*/
	    // - BILATERALSETTLEMENT Commented the above code
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
	    
	    java.sql.Date sqlDateToday = null;
	  //JTest changes on 01-feb-11 starts
	    /*String selectSqlBusinessDate = "SELECT MAX(DAT_TODAY) DAT_TODAY FROM OLM_HOSTSYSTEMDATES WHERE COD_GL " +
	    		"IN(SELECT COD_GL FROM ORBICASH_ACCOUNTS WHERE NBR_OWNACCOUNT IN( " +
	    		"SELECT NBR_ACCTNO_NOSTRO FROM OLM_NOSTROLORO_DTLS WHERE NBR_CONTRACT_NO = '"+referenceId+"' " +
	    		"UNION SELECT NBR_ACCTNO_LORO FROM OLM_NOSTROLORO_DTLS WHERE NBR_CONTRACT_NO = '"+referenceId+"' " +
	    		"UNION SELECT NBR_ACCNO_MAIN_SETTELMENT FROM OLM_NOSTROLORO_DTLS WHERE NBR_CONTRACT_NO = '"+referenceId+"' " +
	    		"UNION SELECT NBR_ACCTNO_MIRROR_SETTELMENT FROM OLM_NOSTROLORO_DTLS WHERE NBR_CONTRACT_NO = '"+referenceId+"' ))";*/
	    String selectSqlBusinessDate = "SELECT MAX(DAT_TODAY) DAT_TODAY FROM OLM_HOSTSYSTEMDATES WHERE COD_GL " +
		"IN(SELECT COD_GL FROM ORBICASH_ACCOUNTS WHERE NBR_OWNACCOUNT IN( " +
		"SELECT NBR_ACCTNO_NOSTRO FROM OLM_NOSTROLORO_DTLS WHERE NBR_CONTRACT_NO = ? " +
		"UNION SELECT NBR_ACCTNO_LORO FROM OLM_NOSTROLORO_DTLS WHERE NBR_CONTRACT_NO = ? " +
		"UNION SELECT NBR_ACCNO_MAIN_SETTELMENT FROM OLM_NOSTROLORO_DTLS WHERE NBR_CONTRACT_NO = ? " +
		"UNION SELECT NBR_ACCTNO_MIRROR_SETTELMENT FROM OLM_NOSTROLORO_DTLS WHERE NBR_CONTRACT_NO = ? ))";
	    
	    if(logger.isDebugEnabled())//JTest changes on 01-feb-11
	    	logger.debug("selectSqlBusinessDate :: "+selectSqlBusinessDate);
	    
	    //stmt = conn.createStatement();
	    stmt = conn.prepareStatement(selectSqlBusinessDate);
	    //rs = stmt.executeQuery(selectSqlBusinessDate);
	    stmt.setString(1, referenceId);
	    stmt.setString(2, referenceId);
	    stmt.setString(3, referenceId);
	    stmt.setString(4, referenceId);
	    rs = stmt.executeQuery();
	    //JTest changes on 01-feb-11 ends
	    
	    if(rs.next()){
		sqlDateToday = rs.getDate("DAT_TODAY");
	    }
	    
	    if(logger.isDebugEnabled()){
	    	logger.debug("sqlDateToday :: "+sqlDateToday);
	    	logger.debug("referenceId :: "+referenceId);
	    }
	    rs.close();
	    stmt.close();
	    
	    GregorianCalendar cal = new GregorianCalendar();
	    cal.setTimeInMillis(sqlDateToday.getTime());
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
	    // - BILATERALSETTLEMENT
	    /*int date = cal.get(Calendar.DATE);
	    cal.set(Calendar.DATE, date+1);*/
	    cal.add(Calendar.DATE, 1);
	    // - BILATERALSETTLEMENT
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
	    java.sql.Date nextEffDate = new java.sql.Date(cal.getTimeInMillis());
	    if(logger.isDebugEnabled())//JTest changes on 01-feb-11
	    	logger.debug("nextEffDate :: "+nextEffDate);
	    
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
	    // - BILATERALSETTLEMENT - Commented the Query & Changed to new 
	    /*String sqlCentral = "SELECT DISTINCT OAM.* FROM OLM_AETEMPLATE_MST OAM WHERE NOT EXISTS " +
	    		"(SELECT NBR_TEMPLATE_ID FROM OLM_AETEMPLATE_DTLS OAD WHERE " +
	    		"OAD.NBR_TEMPLATE_ID = OAM.NBR_TEMPLATE_ID AND ? BETWEEN " +
	    		"OAD.DAT_EFFECTIVE AND NVL (OAD.DAT_END,TO_DATE ('31-DEC-9999', 'DD-MON_YYYY'))) " +
	    		"UNION SELECT DISTINCT OAM.* FROM OLM_AETEMPLATE_MST OAM  WHERE OAM.TYP_ENTITY = 'HostSystem' " +
	    		"AND EXISTS (SELECT OAD.NBR_TEMPLATE_ID FROM OLM_AETEMPLATE_DTLS OAD WHERE " +
	    		"OAD.NBR_TEMPLATE_ID = OAM.NBR_TEMPLATE_ID AND COD_GL IN (?,?) AND COD_CCY = OAM.COD_CCY) " +
	    		"UNION SELECT DISTINCT OAM.* FROM OLM_AETEMPLATE_MST OAM  WHERE OAM.TYP_ENTITY = 'BLE'  " +
	    		"AND EXISTS (SELECT NBR_TEMPLATE_ID FROM OLM_AETEMPLATE_DTLS OAD WHERE " +
	    		"OAD.NBR_TEMPLATE_ID = OAM.NBR_TEMPLATE_ID AND OAD.COD_BNKLGLENT IN (?,?) " +
	    		"AND OAD.COD_CCY = OAM.COD_CCY)";	    */
	    //JTest changes on 01-feb-11 starts
	    //String sqlCentral = "SELECT * FROM OLM_AETEMPLATE_MST";
	   // String sqlCentral = "SELECT NBR_TEMPLATE_ID, COD_ENTITY1, COD_ENTITY2, COD_CCY FROM OLM_AETEMPLATE_MST"; 
	    // HSBC_Issue_fix
	    String sqlCentral = "SELECT NBR_TEMPLATE_ID, COD_ENTITY1, COD_ENTITY2, COD_CCY FROM OLM_AETEMPLATE_MST WHERE COD_CCY = (SELECT DISTINCT COD_CCY from OLM_NOSTROLORO_DTLS WHERE NBR_CONTRACT_NO = ?)";
	    //JTest changes on 01-feb-11 ends
	    // - BILATERALSETTLEMENT
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
	    if(logger.isDebugEnabled())//JTest changes on 01-feb-11
	    	logger.debug("sqlCentral :: "+sqlCentral);
	    
	    pstmtSelectDetail = conn.prepareStatement(sqlCentral);
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
	    //- BILATERALSETTLEMENT - Commented the Code
	    /*pstmtSelectDetail.setDate(1,LMSUtility.truncSqlDate( sqlDateToday));
	    pstmtSelectDetail.setString(2, entity1);
	    pstmtSelectDetail.setString(3, entity2);
	    pstmtSelectDetail.setString(4, entity1);
	    pstmtSelectDetail.setString(5, entity2);*/
	    // - BILATERALSETTLEMENT
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
	    // HSBC_Issue_fix
	    pstmtSelectDetail.setString(1,referenceId);
	    rs = pstmtSelectDetail.executeQuery();

//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
	    //Effective Date & End Date Updation
	    // - Query Updated - BILATERALSETTLEMENT
	    /*String updateEffDate = "UPDATE OLM_AETEMPLATE_DTLS SET DAT_EFFECTIVE = " +
	    		"(SELECT MAX (DAT_EFFECTIVE) FROM OLM_AETEMPLATE_DTLS WHERE NBR_TEMPLATE_ID = ? " +
	    		"AND (DAT_END IS NULL OR DAT_END > ? ))," +
	    		"DAT_END = (SELECT MIN (DAT_END)FROM OLM_AETEMPLATE_DTLS WHERE NBR_TEMPLATE_ID = ? " +
	    		"AND (DAT_END IS NULL OR DAT_END > ? )) " +
	    		"WHERE NBR_TEMPLATE_ID = ? AND (DAT_END IS NULL OR DAT_END > ? ) ";*/
	    
	    String updateEffDate = "UPDATE OLM_AETEMPLATE_DTLS OAD SET(DAT_EFFECTIVE,DAT_END) = " +
	    		"(SELECT CASE WHEN OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' " +
	    		"AND B.EFFDATE < C.EFFDATE THEN C.EFFDATE ELSE B.EFFDATE END, " +
	    		"CASE WHEN OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' AND B.DATEND > C.DATEND " +
	    		"THEN C.DATEND ELSE B.DATEND END FROM( SELECT NBR_TEMPLATE_ID," +
	    		"FLG_ISSETTLEMENT_ENTRY,MAX(DAT_EFFECTIVE) EFFDATE,MIN (DAT_END) DATEND " +
	    		"FROM OLM_AETEMPLATE_DTLS WHERE  NBR_TEMPLATE_ID = ? " +
	    		"AND (DAT_END IS NULL OR DAT_END > ?) GROUP BY NBR_TEMPLATE_ID," +
	    		"FLG_ISSETTLEMENT_ENTRY )B, (SELECT NBR_TEMPLATE_ID,MAX(EFFDATE) EFFDATE," +
	    		"MIN (DATEND) DATEND FROM ( SELECT NBR_TEMPLATE_ID,FLG_ISSETTLEMENT_ENTRY," +
	    		"MAX(DAT_EFFECTIVE) EFFDATE,MIN (DAT_END) DATEND FROM OLM_AETEMPLATE_DTLS " +
	    		"WHERE  NBR_TEMPLATE_ID = ? AND (DAT_END IS NULL OR DAT_END > ?) " +
	    		"GROUP BY NBR_TEMPLATE_ID,FLG_ISSETTLEMENT_ENTRY )GROUP BY NBR_TEMPLATE_ID ) C " +
	    		"WHERE B.NBR_TEMPLATE_ID = C.NBR_TEMPLATE_ID  " +
	    		"AND OAD.FLG_ISSETTLEMENT_ENTRY  = B.FLG_ISSETTLEMENT_ENTRY " +
	    		"AND OAD.NBR_TEMPLATE_ID = ? AND (DAT_END IS NULL OR DAT_END > ?) ) " +
	    		"WHERE NBR_TEMPLATE_ID = ? AND (DAT_END IS NULL OR DAT_END > ?) ";
	    if(logger.isDebugEnabled())//JTest changes on 01-feb-11
	    	logger.debug("updateEffDate :: "+updateEffDate);
	    // - BILATERALSETTLEMENT
	    pstmtUpdateEffDate = conn.prepareStatement(updateEffDate);//JTest changes on 01-feb-11
	    //Effective Date & End Date Updation
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
	    while(rs.next()){
		legCount = 3;
		insertFlag = false;
		flgPersistTemplate = true;
		AccountingEntryTemplateDO srcAETemplateDO = new AccountingEntryTemplateDO();
		AccountingEntryTemplateDO tgtAETemplateDO = new AccountingEntryTemplateDO();

		srcAETemplateDO.setNbrTemplateId(rs.getString("NBR_TEMPLATE_ID"));
		//srcAETemplateDO.setCodGl(rs.getString("COD_ENTITY1"));
		
		if("HostSystem".equals(ExecutionConstants.ENTITY_TYPE)){
		    srcAETemplateDO.setCodGl(rs.getString("COD_ENTITY1"));
		}else if("BLE".equals(ExecutionConstants.ENTITY_TYPE)){
		    srcAETemplateDO.setCodBnkLglEnt(rs.getString("COD_ENTITY1"));
		}
		//ENH_Nostroloro_branch start
		else if(ExecutionConstants.NOSTRO_LORO_BRANCH.equals(ExecutionConstants.ENTITY_TYPE)){
		    srcAETemplateDO.setCodBranch(rs.getString("COD_ENTITY1"));
		}
		//ENH_Nostroloro_branch end

		
		srcAETemplateDO.setCodCcy(rs.getString("COD_CCY"));
		srcAETemplateDO.setDateEffective(nextEffDate);
		srcAETemplateDO.setFlgPersist("N");

		tgtAETemplateDO.setNbrTemplateId(rs.getString("NBR_TEMPLATE_ID"));
		//tgtAETemplateDO.setCodGl(rs.getString("COD_ENTITY2"));
		if("HostSystem".equals(ExecutionConstants.ENTITY_TYPE)){
		    tgtAETemplateDO.setCodGl(rs.getString("COD_ENTITY2"));
		}else if("BLE".equals(ExecutionConstants.ENTITY_TYPE)){
		    tgtAETemplateDO.setCodBnkLglEnt(rs.getString("COD_ENTITY2"));
		}
		//ENH_Nostroloro_branch start
		else if(ExecutionConstants.NOSTRO_LORO_BRANCH.equals(ExecutionConstants.ENTITY_TYPE)){
		    tgtAETemplateDO.setCodBranch(rs.getString("COD_ENTITY2"));
		}
		//ENH_Nostroloro_branch end
		tgtAETemplateDO.setCodCcy(rs.getString("COD_CCY"));
		tgtAETemplateDO.setDateEffective(nextEffDate);
		tgtAETemplateDO.setFlgPersist("N");
		if(logger.isDebugEnabled()){//JTest changes on 01-feb-11
			logger.debug("Calling generateAETemplateDetail For :: Entity1 :: "+rs.getString("COD_ENTITY1"));
			logger.debug("Calling generateAETemplateDetail For :: Entity2 :: "+rs.getString("COD_ENTITY2"));
		}
		generateAETemplateDetail(srcAETemplateDO,tgtAETemplateDO,"DO",conn, pstmt, pstmtSettlemet);
		
		
		if(flgPersistTemplate){
		    pstmtEndDateUpdate.clearParameters();
		    pstmtEndDateUpdate.setDate(1,LMSUtility.truncSqlDate( sqlDateToday));
		    pstmtEndDateUpdate.setString(2, rs.getString("NBR_TEMPLATE_ID"));
		    pstmtEndDateUpdate.setDate(3,LMSUtility.truncSqlDate(sqlDateToday));
		    int endUpdateCnt = pstmtEndDateUpdate.executeUpdate();
		    
		    if(logger.isDebugEnabled())//JTest changes on 01-feb-11
		    	logger.debug("endUpdate :: "+endUpdateCnt);

		    int a[] = pstmt.executeBatch();
		    insertFlag = true;
		    
		    if(logger.isDebugEnabled())//JTest changes on 01-feb-11
		    	logger.debug("getNLDetailsAndgenerateAE...."+a.length);
		}

		if(insertFlag){
		    int a[] = pstmtSettlemet.executeBatch();
		    
		    if(logger.isDebugEnabled())//JTest changes on 01-feb-11
		    	logger.debug("getNLDetailsAndgenerateAE...."+a.length);
		    
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		    //Effective Date & End Date Updation
		    pstmtUpdateEffDate.setString(1, rs.getString("NBR_TEMPLATE_ID"));
		    pstmtUpdateEffDate.setDate(2,LMSUtility.truncSqlDate( sqlDateToday));
		    pstmtUpdateEffDate.setString(3, rs.getString("NBR_TEMPLATE_ID"));
		    pstmtUpdateEffDate.setDate(4,LMSUtility.truncSqlDate( sqlDateToday));
		    pstmtUpdateEffDate.setString(5, rs.getString("NBR_TEMPLATE_ID"));
		    pstmtUpdateEffDate.setDate(6,LMSUtility.truncSqlDate( sqlDateToday));
		    //- BILATERALSETTLEMENT
		    pstmtUpdateEffDate.setString(7, rs.getString("NBR_TEMPLATE_ID"));
		    pstmtUpdateEffDate.setDate(8,LMSUtility.truncSqlDate( sqlDateToday));
		    //- BILATERALSETTLEMENT
		    
		    int updCnt = pstmtUpdateEffDate.executeUpdate();
		    
		    if(logger.isDebugEnabled())//JTest changes on 01-feb-11
		    	logger.debug("Update Effective Date & End Date Count...."+updCnt);
		    //Effective Date & End Date Updation
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		}
		pstmt.clearBatch();
		pstmtSettlemet.clearBatch();
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		//Effective Date & End Date Updation
		pstmtUpdateEffDate.clearParameters();
		//Effective Date & End Date Updation
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends

	    }
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
	    //Effective Date & End Date Updation
	    LMSConnectionUtility.close(pstmtUpdateEffDate);
	    //Effective Date & End Date Updation
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
	}catch(Exception e){
	    logger.fatal("generateAETemplateMaster........."+e);
	    throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, "", e);
	}
	finally{
		//JTest changes on 01-feb-11 starts
	    LMSConnectionUtility.close(rs);
	    LMSConnectionUtility.close(stmt);
		LMSConnectionUtility.close(pstmtUpdateEffDate);
		LMSConnectionUtility.close(pstmtSelectDetail);
		//JTest changes on 01-feb-11 ends
	}
	if(logger.isDebugEnabled())//JTest changes on 01-feb-11
		logger.debug("Leaving getNLDetailsAndgenerateAE........");
	return;
    }

    
    //RBS - 132 ActiveTransaction Nostro Leg Start
    /**
     * 
     * @param referenceId
     * @throws LMSException
     */
    private void getATDetailsAndgenerateAE(String referenceType,String nbrActiveTxnId, Connection conn, PreparedStatement pstmt, PreparedStatement pstmtSettlemet) throws LMSException{
    	if(logger.isDebugEnabled())
	    	logger.debug("getATDetailsAndgenerateAE....");
    
    PreparedStatement stmt = null;
	boolean insertFlag = true;
	PreparedStatement updEffDate = null;
	ResultSet rs = null;	
	
	try{
	    int masterCount = generateAETemplateMaster(referenceType,nbrActiveTxnId, conn);
	    if(logger.isDebugEnabled())
	    	logger.debug("masterCount...."+masterCount);

	    if(masterCount > 0 ){
		String insertDtlSql = "";

		if("HostSystem".equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
		 
		    insertDtlSql ="SELECT DISTINCT osad.cod_glsrcacct, osad.cod_ccysrcacct, osad.flg_src_acc_type, osad.cod_gltgtacct, osad.cod_ccytgtacct, " +
		    		"osad.flg_tgt_acc_type, oam.nbr_template_id FROM olm_activetransaction osad, olm_aetemplate_mst oam WHERE osad.NBR_ACTIVETRANSACTIONID = ? " +
		    		"AND oam.typ_entity = ? AND ((   osad.cod_glsrcacct || osad.cod_gltgtacct || osad.cod_ccysrcacct ) IN " +
		    		"(oam.cod_entity1 || oam.cod_entity2 || oam.cod_ccy) ) AND NOT EXISTS ( SELECT oad.nbr_template_id FROM olm_aetemplate_dtls oad " +
		    		"WHERE oad.nbr_template_id = oam.nbr_template_id AND (SELECT MIN (dat_today) FROM olm_hostsystemdates " +
		    		"WHERE cod_gl IN (osad.cod_glsrcacct, osad.cod_gltgtacct)) BETWEEN oad.dat_effective AND NVL " +
		    		"(oad.dat_end, TO_DATE ('31-DEC-9999', 'DD-MON-YYYY' ) ))";
		    
		    
		    
		}else if("BLE".equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
		  insertDtlSql = "SELECT DISTINCT osad.cod_glsrcacct, osad.cod_ccysrcacct, osad.flg_src_acc_type, " +
	  		    		"osad.cod_gltgtacct, osad.cod_ccytgtacct, osad.flg_tgt_acc_type, srcacct.cod_bnk_lgl cod_srcbnklglent, " +
	  		    		"tgtacct.cod_bnk_lgl cod_tgtbnklglent, oam.nbr_template_id FROM olm_activetransaction osad, " +
	  		    		"orbicash_accounts srcacct, orbicash_accounts tgtacct, olm_aetemplate_mst oam " +
	  		    		"WHERE osad.NBR_ACTIVETRANSACTIONID = ? AND oam.typ_entity = ? AND osad.nbr_srcacct = srcacct.nbr_ownaccount " +
	  		    		"AND osad.nbr_tgtacct = tgtacct.nbr_ownaccount AND ((   srcacct.cod_bnk_lgl || tgtacct.cod_bnk_lgl || osad.cod_ccysrcacct )" +
	  		    		" IN (oam.cod_entity1 || oam.cod_entity2 || oam.cod_ccy) ) AND NOT EXISTS " +
	  		    		"( SELECT oad.nbr_template_id FROM olm_aetemplate_dtls oad WHERE oad.nbr_template_id = oam.nbr_template_id " +
	  		    		"AND (SELECT MIN (dat_today) FROM olm_hostsystemdates WHERE cod_gl IN (osad.cod_glsrcacct, osad.cod_gltgtacct)) " +
	  		    		"BETWEEN oad.dat_effective AND NVL (oad.dat_end, TO_DATE ('31-DEC-9999', 'DD-MON-YYYY' ) ))";	  		    
		}
		//ENH_Nostroloro_branch start
		else if(ExecutionConstants.NOSTRO_LORO_BRANCH.equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){		
			
			insertDtlSql = "SELECT DISTINCT osad.cod_glsrcacct, osad.cod_ccysrcacct, osad.flg_src_acc_type, osad.cod_gltgtacct, " +
					"osad.cod_ccytgtacct, osad.flg_tgt_acc_type, osad.cod_brsrcacct, osad.cod_brtgtacct, oam.nbr_template_id " +
					"FROM olm_activetransaction osad, olm_aetemplate_mst oam WHERE osad.NBR_ACTIVETRANSACTIONID = ? AND oam.typ_entity = ? " +
					"AND ((   osad.cod_brsrcacct || osad.cod_brtgtacct || osad.cod_ccysrcacct ) IN " +
					"(oam.cod_entity1 || oam.cod_entity2 || oam.cod_ccy) ) AND NOT EXISTS ( SELECT oad.nbr_template_id " +
					"FROM olm_aetemplate_dtls oad WHERE oad.nbr_template_id = oam.nbr_template_id AND (SELECT MIN (dat_today) " +
					"FROM olm_hostsystemdates WHERE cod_gl IN (osad.cod_glsrcacct, osad.cod_gltgtacct)) BETWEEN oad.dat_effective " +
					"AND NVL (oad.dat_end, TO_DATE ('31-DEC-9999', 'DD-MON-YYYY' ) )) UNION SELECT DISTINCT osad.cod_glsrcacct, " +
					"osad.cod_ccysrcacct, osad.flg_src_acc_type, osad.cod_gltgtacct, osad.cod_ccytgtacct, osad.flg_tgt_acc_type, " +
					"osad.cod_brsrcacct, ?, oam.nbr_template_id FROM olm_activetransaction osad, olm_aetemplate_mst oam " +
					"WHERE osad.NBR_ACTIVETRANSACTIONID = ? AND oam.typ_entity = ? AND osad.cod_brsrcacct != ? AND " +
					"((osad.cod_brsrcacct || ? || osad.cod_ccysrcacct) IN (oam.cod_entity1 || oam.cod_entity2 || oam.cod_ccy) ) " +
					"AND NOT EXISTS ( SELECT oad.nbr_template_id FROM olm_aetemplate_dtls oad WHERE oad.nbr_template_id = oam.nbr_template_id " +
					"AND (SELECT MIN (dat_today) FROM olm_hostsystemdates WHERE cod_gl IN (osad.cod_glsrcacct, osad.cod_gltgtacct)) " +
					"BETWEEN oad.dat_effective AND NVL (oad.dat_end, TO_DATE ('31-DEC-9999', 'DD-MON-YYYY' ) )) " +
					"UNION SELECT DISTINCT osad.cod_glsrcacct, osad.cod_ccysrcacct, osad.flg_src_acc_type, osad.cod_gltgtacct, " +
					"osad.cod_ccytgtacct, osad.flg_tgt_acc_type, cod_brtgtacct, ?, oam.nbr_template_id FROM olm_activetransaction osad, " +
					"olm_aetemplate_mst oam WHERE osad.NBR_ACTIVETRANSACTIONID = ? AND oam.typ_entity = ? AND osad.cod_brtgtacct != ? " +
					"AND ((osad.cod_brtgtacct || ? || osad.cod_ccysrcacct) IN (oam.cod_entity1 || oam.cod_entity2 || oam.cod_ccy) ) " +
					"AND NOT EXISTS ( SELECT oad.nbr_template_id FROM olm_aetemplate_dtls oad WHERE oad.nbr_template_id = oam.nbr_template_id " +
					"AND (SELECT MIN (dat_today) FROM olm_hostsystemdates WHERE cod_gl IN (osad.cod_glsrcacct, osad.cod_gltgtacct)) " +
					"BETWEEN oad.dat_effective AND NVL (oad.dat_end, TO_DATE ('31-DEC-9999', 'DD-MON-YYYY' ) ))";
		}
		//ENH_Nostroloro_branch end
		//JTest changes on 01-feb-11 starts
		if(logger.isDebugEnabled())
	    	logger.debug("insertDtlSql :: "+insertDtlSql);
		
		//stmt = conn.createStatement();
		stmt = conn.prepareStatement(insertDtlSql);
		stmt.setString(1, nbrActiveTxnId);
		stmt.setString(2, ExecutionConstants.ENTITY_TYPE);
		if("Branch".equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
			
			stmt.setString(3, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
			stmt.setString(4, nbrActiveTxnId);
			stmt.setString(5, ExecutionConstants.ENTITY_TYPE);
			stmt.setString(6, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
			stmt.setString(7, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
			stmt.setString(8, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
			stmt.setString(9, nbrActiveTxnId);
			stmt.setString(10, ExecutionConstants.ENTITY_TYPE);
			stmt.setString(11, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
			stmt.setString(12, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
		}
		//JTest changes on 01-feb-11 ends
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		//Below Query Updated - BILATERALSETTLEMENT
		//Effective Date & End Date Updation
		/*String updateTemplateEffDate = "UPDATE OLM_AETEMPLATE_DTLS SET DAT_EFFECTIVE = (SELECT MAX (DAT_EFFECTIVE) " +
				"FROM OLM_AETEMPLATE_DTLS WHERE NBR_TEMPLATE_ID = ? ), DAT_END = " +
				"(SELECT MIN (DAT_END) FROM OLM_AETEMPLATE_DTLS WHERE NBR_TEMPLATE_ID = ?) " +
				"WHERE NBR_TEMPLATE_ID = ? ";*/
		String updateTemplateEffDate = "UPDATE OLM_AETEMPLATE_DTLS OAD SET(DAT_EFFECTIVE,DAT_END) = " +
				"(SELECT CASE WHEN OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' " +
				"AND B.EFFDATE < C.EFFDATE THEN C.EFFDATE ELSE B.EFFDATE END, " +
				"CASE WHEN OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' " +
				"AND B.DATEND > C.DATEND THEN C.DATEND ELSE B.DATEND END " +
				"FROM( SELECT NBR_TEMPLATE_ID,FLG_ISSETTLEMENT_ENTRY," +
				"MAX(DAT_EFFECTIVE) EFFDATE,MIN (DAT_END) DATEND " +
				"FROM OLM_AETEMPLATE_DTLS WHERE  NBR_TEMPLATE_ID = ? " +
				"GROUP BY NBR_TEMPLATE_ID,FLG_ISSETTLEMENT_ENTRY )B, " +
				"(SELECT NBR_TEMPLATE_ID,MAX(EFFDATE) EFFDATE,MIN (DATEND) DATEND " +
				"FROM ( SELECT NBR_TEMPLATE_ID,FLG_ISSETTLEMENT_ENTRY,MAX(DAT_EFFECTIVE) EFFDATE," +
				"MIN (DAT_END) DATEND FROM OLM_AETEMPLATE_DTLS WHERE  NBR_TEMPLATE_ID = ? " +
				"GROUP BY NBR_TEMPLATE_ID,FLG_ISSETTLEMENT_ENTRY )GROUP BY NBR_TEMPLATE_ID ) C " +
				"WHERE B.NBR_TEMPLATE_ID = C.NBR_TEMPLATE_ID  " +
				"AND OAD.FLG_ISSETTLEMENT_ENTRY  = B.FLG_ISSETTLEMENT_ENTRY " +
				"AND OAD.NBR_TEMPLATE_ID = ?  ) WHERE NBR_TEMPLATE_ID = ? ";
		if(logger.isDebugEnabled())//JTest changes on 01-feb-11
	    	logger.debug("updateTemplateEffDate :: "+updateTemplateEffDate);
		// - BILATERALSETTLEMENT
		updEffDate = conn.prepareStatement(updateTemplateEffDate); //JTest changes on 01-feb-11
		//Effective Date & End Date Updation
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		rs = stmt.executeQuery();//JTest changes on 01-feb-11
		while(rs.next()){
		    legCount = 3;
		    flgPersistTemplate = true;
		    insertFlag = false;
		    
		    AccountingEntryTemplateDO srcTemplateDo = new AccountingEntryTemplateDO();
		    AccountingEntryTemplateDO tgtTemplateDo = new AccountingEntryTemplateDO();
		    populateAETemplateDO(rs,srcTemplateDo,tgtTemplateDo);
		    
		    if(logger.isDebugEnabled()){//JTest changes on 01-feb-11
		    	logger.debug("Calling generateAETemplateDetail For Entity1"+srcTemplateDo);
		    	logger.debug("Calling generateAETemplateDetail For Entity2"+tgtTemplateDo);
		    }
		    generateAETemplateDetail(srcTemplateDo,tgtTemplateDo,"DO", conn, pstmt, pstmtSettlemet);
		    
		    if(flgPersistTemplate){
			int a[] = pstmt.executeBatch();
			insertFlag = true;
			if(logger.isDebugEnabled())//JTest changes on 01-feb-11
		    	logger.debug("getCSDetailsAndgenerateAE...."+a.length);
		    }
		    else
		    {
		    	   HashMap hp = new HashMap();
		    	   hp.put("COD_GLSRCACCT", srcTemplateDo.getCodGl());
		    	   hp.put("COD_GLTGTACCT", tgtTemplateDo.getCodGl());
		    	   hp.put("COD_CCY", srcTemplateDo.getCodCcy());
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		    	   // Jiten - BILATERALSETTLEMENT		    	   
		    	   hp.put("MSGCODE", "M0161");
					
		    	   if(logger.isDebugEnabled())//JTest changes on 01-feb-11
		   	    	logger.debug("Message is :::::.... "+hp);
		    	   
		    	   // - Jiten BILATERALSETTLEMENT - Commented the Code as Method name changed
				    //alertForPathNotCompleted(hp);
		    	   		generateAlert(hp);
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		    }

		    if(insertFlag){
			int a[] = pstmtSettlemet.executeBatch();
			if(logger.isDebugEnabled())//JTest changes on 01-feb-11
		    	logger.debug("getCSDetailsAndgenerateAE...."+a.length);
			
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
			//Effective Date & End Date Updation
			updEffDate.setString(1, rs.getString("NBR_TEMPLATE_ID"));
			updEffDate.setString(2, rs.getString("NBR_TEMPLATE_ID"));
			updEffDate.setString(3, rs.getString("NBR_TEMPLATE_ID"));
			// - BILATERALSETTLEMENT
			updEffDate.setString(4, rs.getString("NBR_TEMPLATE_ID"));
			// - BILATERALSETTLEMENT
			
			int updCnt = updEffDate.executeUpdate();
			if(logger.isDebugEnabled())//JTest changes on 01-feb-11
		    	logger.debug("Updating the Effective Date & End Date Count...."+updCnt);			
			//Effective Date & End Date Updation
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		    }
		    
		    pstmt.clearBatch();
		    pstmtSettlemet.clearBatch();
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		    //Effective Date & End Date Updation
		    updEffDate.clearParameters();
		    //Effective Date & End Date Updation
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		}
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		//Effective Date & End Date Updation
		LMSConnectionUtility.close(updEffDate);
		//Effective Date & End Date Updation
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
     }
	}catch(Exception e){
	    logger.fatal("getCSDetailsAndgenerateAE...."+e);
	    throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, "", e);
	}
	finally{
		//JTest changes on 01-feb-11 starts
		LMSConnectionUtility.close(rs);
		LMSConnectionUtility.close(updEffDate);
		LMSConnectionUtility.close(stmt);
		//JTest changes on 01-feb-11 ends 
	}
	if(logger.isDebugEnabled())//JTest changes on 01-feb-11
    	logger.debug("getCSDetailsAndgenerateAE....Leaving");
	return;
    }
    
    //RBS - 132 ActiveTransaction Nostro Leg Ends

    
    
    
    
    //RBS - 132 ActiveTransaction Nostro Leg Starts
    /**
     * 
     * @param referenceID
     * @return
     * @throws LMSException
     */
    /*private int generateAETemplateMaster(String referenceType, String referenceID, Connection conn)throws LMSException{
	    if(logger.isDebugEnabled())//JTest changes on 01-feb-11
	    	logger.debug("generateAETemplateMaster...."+ExecutionConstants.ENTITY_TYPE);
		int insertCount = 0;
		PreparedStatement insertMstStmt = null;//JTest changes on 01-feb-11
		try{
		    
		    String insertAETemplateMst = "";
	
		    if("HostSystem".equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
			insertAETemplateMst = "INSERT INTO OLM_AETEMPLATE_MST(NBR_TEMPLATE_ID,TYP_ENTITY," +
			"COD_ENTITY1,COD_ENTITY2,COD_CCY)" +
			//"(SELECT SEQ_AE_TEMPLATEID.NEXTVAL,'"+ExecutionConstants.ENTITY_TYPE+"',M.COD_GLSRCACCT,M.COD_GLTGTACCT," +
			"(SELECT SEQ_AE_TEMPLATEID.NEXTVAL,?, M.COD_GLSRCACCT, M.COD_GLTGTACCT, " +//JTest changes on 01-feb-11
			"M.COD_CCYSRCACCT " +
			"FROM (SELECT DISTINCT COD_GLSRCACCT,COD_GLTGTACCT,COD_CCYSRCACCT " +
			"FROM OLM_SOURCE_ACCOUNT_DTLS OSAD,ORBICASH_ACCOUNTS SRCACCT,ORBICASH_ACCOUNTS TGTACCT " +
			//"WHERE NBR_STRCID = '"+nbrStructureId+"' AND COD_GLSRCACCT != COD_GLTGTACCT " +
			"WHERE NBR_STRCID = ? AND COD_GLSRCACCT != COD_GLTGTACCT " +//JTest changes on 01-feb-11
			"AND OSAD.NBR_SRCACCT = SRCACCT.NBR_OWNACCOUNT "+
			"AND OSAD.NBR_TGTACCT = TGTACCT.NBR_OWNACCOUNT "+ 
			"AND SRCACCT.COD_COUNTRY != TGTACCT.COD_COUNTRY " +
			"MINUS " +
			"SELECT OAM.COD_ENTITY1,OAM.COD_ENTITY2,OAM.COD_CCY FROM OLM_AETEMPLATE_MST OAM) M )";
		    }else if("BLE".equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
	    	//HSBC_OSL_2643_CR_ICL_INTR Start
			 insertAETemplateMst = "INSERT INTO OLM_AETEMPLATE_MST (NBR_TEMPLATE_ID, TYP_ENTITY, COD_ENTITY1, " +
			//"COD_ENTITY2, COD_CCY) (SELECT SEQ_AE_TEMPLATEID.NEXTVAL, '"+ExecutionConstants.ENTITY_TYPE+"', " +
			"COD_ENTITY2, COD_CCY) (SELECT SEQ_AE_TEMPLATEID.NEXTVAL, ?, " +//JTest changes on 01-feb-11
			"M.COD_SRCBNKLGLENT, M.COD_TGTBNKLGLENT, M.COD_CCYSRCACCT FROM  " +
			"(SELECT DISTINCT SRCACCT.COD_BNK_LGL COD_SRCBNKLGLENT, TGTACCT.COD_BNK_LGL COD_TGTBNKLGLENT, " +
			"OSAD.COD_CCYSRCACCT FROM OLM_SOURCE_ACCOUNT_DTLS OSAD, ORBICASH_ACCOUNTS SRCACCT, " +
			//"ORBICASH_ACCOUNTS TGTACCT WHERE NBR_STRCID = '"+nbrStructureId+"' " +
			"ORBICASH_ACCOUNTS TGTACCT WHERE NBR_STRCID = ? " +//JTest changes on 01-feb-11
			"AND SRCACCT.COD_BNK_LGL != TGTACCT.COD_BNK_LGL AND OSAD.NBR_SRCACCT = SRCACCT.NBR_OWNACCOUNT " +
			"AND OSAD.NBR_TGTACCT = TGTACCT.NBR_OWNACCOUNT AND SRCACCT.COD_COUNTRY != TGTACCT.COD_COUNTRY " +
			"MINUS SELECT OAM.COD_ENTITY1, OAM.COD_ENTITY2, OAM.COD_CCY FROM OLM_AETEMPLATE_MST OAM) M) "; 
insertAETemplateMst = "INSERT INTO OLM_AETEMPLATE_MST (NBR_TEMPLATE_ID, TYP_ENTITY, COD_ENTITY1, COD_ENTITY2, COD_CCY)(SELECT SEQ_AE_TEMPLATEID.NEXTVAL, ?, "+
			"M.COD_SRCBNKLGLENT, M.COD_TGTBNKLGLENT, M.COD_CCYSRCACCT FROM ((SELECT DISTINCT SRCACCT.COD_BNK_LGL COD_SRCBNKLGLENT, "+
			"TGTACCT.COD_BNK_LGL COD_TGTBNKLGLENT, OSAD.COD_CCYSRCACCT  FROM OLM_SOURCE_ACCOUNT_DTLS OSAD, ORBICASH_ACCOUNTS SRCACCT, "+
			"ORBICASH_ACCOUNTS TGTACCT WHERE NBR_STRCID = ? AND SRCACCT.COD_BNK_LGL != TGTACCT.COD_BNK_LGL AND OSAD.NBR_SRCACCT = SRCACCT.NBR_OWNACCOUNT "+
			"AND OSAD.NBR_TGTACCT = TGTACCT.NBR_OWNACCOUNT AND SRCACCT.COD_COUNTRY != TGTACCT.COD_COUNTRY UNION "+
			"SELECT DISTINCT SRCACCT.COD_BNK_LGL COD_SRCBNKLGLENT, TGTACCT.COD_BNK_LGL COD_TGTBNKLGLENT, OSAD.COD_CCYSRCACCT "+
			" FROM OLM_SOURCE_ACCOUNT_DTLS OSAD, ORBICASH_ACCOUNTS SRCACCT, ORBICASH_ACCOUNTS TGTACCT WHERE "+ 
			"NBR_STRCID = ? AND SRCACCT.COD_BNK_LGL != TGTACCT.COD_BNK_LGL AND OSAD.NBR_SRCACCT = SRCACCT.NBR_OWNACCOUNT "+
			"AND OSAD.NBR_TGTACCT = TGTACCT.NBR_OWNACCOUNT AND SRCACCT.COD_COUNTRY != TGTACCT.COD_COUNTRY AND OSAD.COD_INCOID IS NOT NULL ) "+
			"MINUS SELECT OAM.COD_ENTITY1, OAM.COD_ENTITY2, OAM.COD_CCY  FROM OLM_AETEMPLATE_MST OAM ) M  ) ";
			//HSBC_OSL_2643_CR_ICL_INTR End
		    }
		    //ENH_Nostroloro_branch start
		    else if(ExecutionConstants.NOSTRO_LORO_BRANCH.equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
		    	
		    	//insertAETemplateMst = "INSERT INTO olm_aetemplate_mst (nbr_template_id, typ_entity, cod_entity1, cod_entity2, cod_ccy) (SELECT seq_ae_templateid.NEXTVAL, ?, m.cod_brsrcacct, m.cod_brtgtacct, m.cod_ccysrcacct FROM (SELECT DISTINCT cod_brsrcacct, cod_brtgtacct, cod_ccysrcacct FROM olm_source_account_dtls osad, orbicash_accounts srcacct, orbicash_accounts tgtacct WHERE nbr_strcid = ? AND cod_brsrcacct != cod_brtgtacct AND osad.nbr_srcacct = srcacct.nbr_ownaccount AND osad.nbr_tgtacct = tgtacct.nbr_ownaccount MINUS SELECT oam.cod_entity1, oam.cod_entity2, oam.cod_ccy FROM olm_aetemplate_mst oam) m)";
		    	 insertAETemplateMst = "INSERT INTO olm_aetemplate_mst (nbr_template_id, typ_entity, cod_entity1, cod_entity2, cod_ccy) " +
		    	 " (SELECT seq_ae_templateid.NEXTVAL, ?, m.cod_brsrcacct, m.cod_brtgtacct, m.cod_ccysrcacct FROM " +
		    	 " ((SELECT DISTINCT cod_brsrcacct, cod_brtgtacct, cod_ccysrcacct FROM olm_source_account_dtls osad, orbicash_accounts srcacct, " +
		    	 " orbicash_accounts tgtacct WHERE nbr_strcid = ? AND cod_brsrcacct != cod_brtgtacct AND osad.nbr_srcacct = srcacct.nbr_ownaccount " +
		    	 " AND osad.nbr_tgtacct = tgtacct.nbr_ownaccount AND (   osad.flg_groupsweeprule IS NULL OR osad.flg_groupsweeprule NOT IN " +
		    	 " ('DM', 'CM') ) UNION SELECT DISTINCT cod_brsrcacct, ?, cod_ccysrcacct FROM olm_source_account_dtls osad, " +
		    	 " orbicash_accounts srcacct WHERE nbr_strcid = ? AND osad.nbr_srcacct = srcacct.nbr_ownaccount AND osad.flg_groupsweeprule IN " +
		    	 " ('DM', 'CM') AND cod_brsrcacct != ? UNION SELECT DISTINCT cod_brtgtacct, ?, cod_ccysrcacct FROM olm_source_account_dtls osad, " +
		    	 " orbicash_accounts tgtacct WHERE nbr_strcid = ? AND osad.nbr_srcacct = tgtacct.nbr_ownaccount AND osad.flg_groupsweeprule IN " +
		    	 " ('DM', 'CM') AND cod_brtgtacct != ?) MINUS SELECT oam.cod_entity1, oam.cod_entity2, oam.cod_ccy FROM olm_aetemplate_mst oam) m)";
		    }
		    //ENH_Nostroloro_branch end
		  //JTest changes on 01-feb-11 starts
		    if(logger.isDebugEnabled())
		    	logger.debug("insertAETemplateMst :: "+insertAETemplateMst);
	
		    //insertMstStmt = conn.createStatement();
		    insertMstStmt = conn.prepareStatement(insertAETemplateMst);
		    //insertCount = insertMstStmt.executeUpdate(insertAETemplateMst);
		    insertMstStmt.setString(1, ExecutionConstants.ENTITY_TYPE);
		    insertMstStmt.setString(2, referenceID);
		    if("Branch".equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
		    	
		    	insertMstStmt.setString(3, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
		    	insertMstStmt.setString(4, referenceID);
		    	insertMstStmt.setString(5, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
		    	insertMstStmt.setString(6, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
		    	insertMstStmt.setString(7, referenceID);
		    	insertMstStmt.setString(8, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
		    }
		    	logger.debug("Entering insdide BLE ");
		    	insertMstStmt.setString(1, ExecutionConstants.ENTITY_TYPE);
			    insertMstStmt.setString(2, nbrStructureId);
			  //HSBC_OSL_2643_CR_ICL_INTR Start
				insertMstStmt.setString(3, nbrStructureId);
			 //HSBC_OSL_2643_CR_ICL_INTR End
	
		    
		    insertCount = insertMstStmt.executeUpdate();
		    
		    if(logger.isDebugEnabled())
		    	logger.debug("insertAETemplateMst....insertCount :: "+insertCount);
		  //JTest changes on 01-feb-11 ends
		}catch(Exception e){
		    logger.fatal("generateAETemplateMaster........."+e);
		    throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, "", e);
		}
		finally{
			LMSConnectionUtility.close(insertMstStmt);//JTest changes on 01-feb-11
		}
		if(logger.isDebugEnabled())//JTest changes on 01-feb-11
	    	logger.debug("generateAETemplateMaster....Leaving");
	return insertCount;
    }*/

private int generateAETemplateMaster(String referenceType,
			String referenceID, Connection conn) throws LMSException {
		if (logger.isDebugEnabled())// JTest changes on 01-feb-11
			logger.debug("generateAETemplateMaster...."
					+ ExecutionConstants.ENTITY_TYPE);
		int insertCount = 0;
		PreparedStatement insertMstStmt = null;// JTest changes on 01-feb-11
		try {

			String insertAETemplateMst = "";
			String table = "";
			String condition = "";
			// RBS - 132 ActiveTransaction Nostro Leg Start
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("referenceTypeee...." + referenceType);

			if ("AT".equalsIgnoreCase(referenceType)) {
				table = "OLM_ACTIVETRANSACTION OSAD";
				condition = "nbr_activetransactionid = ? ";
			} else if ("ICL".equalsIgnoreCase(referenceType)) {
				 // RBS - 153,154 ICL Nostro Leg Start
				table = "OLM_INCO_DEFINITION OSAD";
				condition = "COD_INCOID = ? ";
				 // RBS - 153,154 ICL Nostro Leg Ends
			} else {
				table = "OLM_SOURCE_ACCOUNT_DTLS OSAD";
				condition = "NBR_STRCID = ? ";
			}
			// RBS - 132 ActiveTransaction Nostro Leg Ends

			if ("HostSystem".equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)) {
				
				if ("ICL".equalsIgnoreCase(referenceType)) {
					insertAETemplateMst = " INSERT INTO olm_aetemplate_mst "
							+ "(nbr_template_id, typ_entity, cod_entity1, cod_entity2, cod_ccy) "
							+ "(SELECT seq_ae_templateid.NEXTVAL, ?, m.cod_glsrcacct, "
							+ " m.cod_gltgtacct, m.cod_ccysrcacct "
							+ " FROM (SELECT DISTINCT srcacct.cod_gl AS cod_glsrcacct, "
							+ "            tgtacct.cod_gl AS cod_gltgtacct,"
							+ "            srcacct.cod_ccy AS cod_ccysrcacct "
							+ "       FROM olm_inco_definition osad, "
							+ "            orbicash_accounts srcacct, "
							+ "            orbicash_accounts tgtacct "
							+ "      WHERE cod_incoid =? "
							+ "        AND srcacct.cod_gl != tgtacct.cod_gl "
							+ "        AND osad.cod_entity1 = srcacct.nbr_ownaccount "
							+ "        AND osad.flg_entity1type = 'I' "
							+ "        AND osad.cod_entity2 = tgtacct.nbr_ownaccount "
							+ "        AND osad.flg_entity2type = 'I' "
							+ "        AND srcacct.cod_country != tgtacct.cod_country "
							+ " MINUS "
							+ " SELECT oam.cod_entity1, oam.cod_entity2, oam.cod_ccy "
							+ " FROM olm_aetemplate_mst oam) m) ";
				}// RBS - 153,154 ICL Nostro Leg ends
				else{
				insertAETemplateMst = "INSERT INTO OLM_AETEMPLATE_MST(NBR_TEMPLATE_ID,TYP_ENTITY,"
						+ "COD_ENTITY1,COD_ENTITY2,COD_CCY)"
						+
						// "(SELECT SEQ_AE_TEMPLATEID.NEXTVAL,'"+ExecutionConstants.ENTITY_TYPE+"',M.COD_GLSRCACCT,M.COD_GLTGTACCT,"
						// +
						"(SELECT SEQ_AE_TEMPLATEID.NEXTVAL,?, M.COD_GLSRCACCT, M.COD_GLTGTACCT, "
						+ // JTest changes on 01-feb-11
						"M.COD_CCYSRCACCT "
						+ "FROM (SELECT DISTINCT COD_GLSRCACCT,COD_GLTGTACCT,COD_CCYSRCACCT "
						+
						// "FROM OLM_ACTIVETRANSACTION OSAD,ORBICASH_ACCOUNTS SRCACCT,ORBICASH_ACCOUNTS TGTACCT "
						// +
						" FROM "
						+ table
						+ ",ORBICASH_ACCOUNTS SRCACCT,ORBICASH_ACCOUNTS TGTACCT "
						+
						// "WHERE NBR_STRCID = '"+nbrStructureId+"' AND COD_GLSRCACCT != COD_GLTGTACCT "
						// +
						"WHERE "
						+ condition
						+ " AND COD_GLSRCACCT != COD_GLTGTACCT "
						+ // JTest changes on 01-feb-11
						// "WHERE NBR_STRCID = ? AND COD_GLSRCACCT != COD_GLTGTACCT "
						// +//JTest changes on 01-feb-11
						"AND OSAD.NBR_SRCACCT = SRCACCT.NBR_OWNACCOUNT "
						+ "AND OSAD.NBR_TGTACCT = TGTACCT.NBR_OWNACCOUNT "
						+ "AND SRCACCT.COD_COUNTRY != TGTACCT.COD_COUNTRY "
						+ "MINUS "
						+ "SELECT OAM.COD_ENTITY1,OAM.COD_ENTITY2,OAM.COD_CCY FROM OLM_AETEMPLATE_MST OAM) M )";
				}
			} else if ("BLE".equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)) {
				// HSBC_OSL_2643_CR_ICL_INTR Start
				/*
				 * insertAETemplateMst =
				 * "INSERT INTO OLM_AETEMPLATE_MST (NBR_TEMPLATE_ID, TYP_ENTITY, COD_ENTITY1, "
				 * +
				 * //"COD_ENTITY2, COD_CCY) (SELECT SEQ_AE_TEMPLATEID.NEXTVAL, '"
				 * +ExecutionConstants.ENTITY_TYPE+"', " +
				 * "COD_ENTITY2, COD_CCY) (SELECT SEQ_AE_TEMPLATEID.NEXTVAL, ?, "
				 * +//JTest changes on 01-feb-11
				 * "M.COD_SRCBNKLGLENT, M.COD_TGTBNKLGLENT, M.COD_CCYSRCACCT FROM  "
				 * +
				 * "(SELECT DISTINCT SRCACCT.COD_BNK_LGL COD_SRCBNKLGLENT, TGTACCT.COD_BNK_LGL COD_TGTBNKLGLENT, "
				 * +
				 * "OSAD.COD_CCYSRCACCT FROM OLM_SOURCE_ACCOUNT_DTLS OSAD, ORBICASH_ACCOUNTS SRCACCT, "
				 * +
				 * //"ORBICASH_ACCOUNTS TGTACCT WHERE NBR_STRCID = '"+nbrStructureId
				 * +"' " + "ORBICASH_ACCOUNTS TGTACCT WHERE NBR_STRCID = ? "
				 * +//JTest changes on 01-feb-11
				 * "AND SRCACCT.COD_BNK_LGL != TGTACCT.COD_BNK_LGL AND OSAD.NBR_SRCACCT = SRCACCT.NBR_OWNACCOUNT "
				 * +
				 * "AND OSAD.NBR_TGTACCT = TGTACCT.NBR_OWNACCOUNT AND SRCACCT.COD_COUNTRY != TGTACCT.COD_COUNTRY "
				 * +
				 * "MINUS SELECT OAM.COD_ENTITY1, OAM.COD_ENTITY2, OAM.COD_CCY FROM OLM_AETEMPLATE_MST OAM) M) "
				 * ;
				 */
				// RBS - 153,154 ICL Nostro Leg Start
				if ("ICL".equalsIgnoreCase(referenceType)) {
					insertAETemplateMst = " INSERT INTO olm_aetemplate_mst "
							+ "(nbr_template_id, typ_entity, cod_entity1, cod_entity2, cod_ccy) "
							+ "(SELECT seq_ae_templateid.NEXTVAL, ?,M.COD_SRCBNKLGLENT , "
							+ "  M.COD_TGTBNKLGLENT , m.cod_ccysrcacct "
							+ " FROM (SELECT DISTINCT SRCACCT.COD_BNK_LGL COD_SRCBNKLGLENT, "
							+ "            TGTACCT.COD_BNK_LGL COD_TGTBNKLGLENT ,"
							+ "            srcacct.cod_ccy AS cod_ccysrcacct "
							+ "       FROM olm_inco_definition osad, "
							+ "            orbicash_accounts srcacct, "
							+ "            orbicash_accounts tgtacct "
							+ "      WHERE cod_incoid =? "
							+ "        AND SRCACCT.COD_BNK_LGL != TGTACCT.COD_BNK_LGL  "
							+ "        AND osad.cod_entity1 = srcacct.nbr_ownaccount "
							+ "        AND osad.flg_entity1type = 'I' "
							+ "        AND osad.cod_entity2 = tgtacct.nbr_ownaccount "
							+ "        AND osad.flg_entity2type = 'I' "
							+ "        AND srcacct.cod_country != tgtacct.cod_country "
							+ " MINUS "
							+ " SELECT oam.cod_entity1, oam.cod_entity2, oam.cod_ccy "
							+ " FROM olm_aetemplate_mst oam) m) ";
				}
				// RBS - 153,154 ICL Nostro Leg ends
				else{
				insertAETemplateMst = "INSERT INTO OLM_AETEMPLATE_MST (NBR_TEMPLATE_ID, TYP_ENTITY, COD_ENTITY1, COD_ENTITY2, COD_CCY)(SELECT SEQ_AE_TEMPLATEID.NEXTVAL, ?, "
						+ "M.COD_SRCBNKLGLENT, M.COD_TGTBNKLGLENT, M.COD_CCYSRCACCT FROM ((SELECT DISTINCT SRCACCT.COD_BNK_LGL COD_SRCBNKLGLENT, "
						+
						"TGTACCT.COD_BNK_LGL COD_TGTBNKLGLENT, OSAD.COD_CCYSRCACCT  FROM "
						+ table
						+ ", ORBICASH_ACCOUNTS SRCACCT, "
						+
						"ORBICASH_ACCOUNTS TGTACCT WHERE "
						+ condition
						+ " AND SRCACCT.COD_BNK_LGL != TGTACCT.COD_BNK_LGL AND OSAD.NBR_SRCACCT = SRCACCT.NBR_OWNACCOUNT "
						+ "AND OSAD.NBR_TGTACCT = TGTACCT.NBR_OWNACCOUNT AND SRCACCT.COD_COUNTRY != TGTACCT.COD_COUNTRY ) "
						/*+ "SELECT DISTINCT SRCACCT.COD_BNK_LGL COD_SRCBNKLGLENT, TGTACCT.COD_BNK_LGL COD_TGTBNKLGLENT, OSAD.COD_CCYSRCACCT "
						+
						" FROM "
						+ table
						+ ", ORBICASH_ACCOUNTS SRCACCT, ORBICASH_ACCOUNTS TGTACCT WHERE "
						+
						""
						+ condition
						+ " AND SRCACCT.COD_BNK_LGL != TGTACCT.COD_BNK_LGL AND OSAD.NBR_SRCACCT = SRCACCT.NBR_OWNACCOUNT "
						+ "AND OSAD.NBR_TGTACCT = TGTACCT.NBR_OWNACCOUNT AND SRCACCT.COD_COUNTRY != TGTACCT.COD_COUNTRY AND OSAD.COD_INCOID IS NOT NULL ) "*/ //Duplicate Condition Removed - Not required
						+ "MINUS SELECT OAM.COD_ENTITY1, OAM.COD_ENTITY2, OAM.COD_CCY  FROM OLM_AETEMPLATE_MST OAM ) M  ) ";
				}
				// HSBC_OSL_2643_CR_ICL_INTR End
			}
			// ENH_Nostroloro_branch start
			else if (ExecutionConstants.NOSTRO_LORO_BRANCH
					.equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)) {

				if ("AT".equalsIgnoreCase(referenceType)) {
					insertAETemplateMst = "INSERT INTO olm_aetemplate_mst (nbr_template_id, typ_entity, cod_entity1, cod_entity2, cod_ccy) "
							+ " (SELECT seq_ae_templateid.NEXTVAL, ?, m.cod_brsrcacct, m.cod_brtgtacct, m.cod_ccysrcacct FROM "
							+ " ((SELECT DISTINCT cod_brsrcacct, cod_brtgtacct, cod_ccysrcacct FROM OLM_ACTIVETRANSACTION osad, orbicash_accounts srcacct, "
							+ " orbicash_accounts tgtacct WHERE nbr_activetransactionid = ? AND cod_brsrcacct != cod_brtgtacct AND osad.nbr_srcacct = srcacct.nbr_ownaccount AND osad.FLG_SRC_ACC_TYPE = 'I' "
							+ " AND osad.nbr_tgtacct = tgtacct.nbr_ownaccount AND osad.FLG_TGT_ACC_TYPE = 'I') MINUS SELECT oam.cod_entity1, oam.cod_entity2, oam.cod_ccy FROM olm_aetemplate_mst oam) m)";
				} else if ("ICL".equalsIgnoreCase(referenceType)) {
					logger.debug("In ICL....");
					// RBS - 153,154 ICL Nostro Leg Start
					insertAETemplateMst = "INSERT INTO olm_aetemplate_mst (nbr_template_id, typ_entity, cod_entity1, cod_entity2, cod_ccy) "
							+ " (SELECT seq_ae_templateid.NEXTVAL, ?, m.cod_brsrcacct, m.cod_brtgtacct, m.cod_ccysrcacct FROM "
							+ " ((SELECT DISTINCT srcacct.COD_BRANCH as cod_brsrcacct, tgtacct.COD_BRANCH as cod_brtgtacct, srcacct.COD_CCY as cod_ccysrcacct FROM  OLM_INCO_DEFINITION osad, orbicash_accounts srcacct, "
							+ " orbicash_accounts tgtacct WHERE COD_INCOID = ? AND srcacct.COD_BRANCH != tgtacct.COD_BRANCH AND osad.COD_ENTITY1 = srcacct.nbr_ownaccount AND osad.FLG_ENTITY1TYPE = 'I' "
							+ " AND osad.COD_ENTITY2 = tgtacct.nbr_ownaccount AND osad.FLG_ENTITY2TYPE = 'I') MINUS SELECT oam.cod_entity1, oam.cod_entity2, oam.cod_ccy FROM olm_aetemplate_mst oam) m)";
					  // RBS - 153,154 ICL Nostro Leg Ends
				} else {
					// "INSERT INTO olm_aetemplate_mst (nbr_template_id, typ_entity, cod_entity1, cod_entity2, cod_ccy) (SELECT seq_ae_templateid.NEXTVAL, ?, m.cod_brsrcacct, m.cod_brtgtacct, m.cod_ccysrcacct FROM (SELECT DISTINCT cod_brsrcacct, cod_brtgtacct, cod_ccysrcacct FROM olm_source_account_dtls osad, orbicash_accounts srcacct, orbicash_accounts tgtacct WHERE nbr_strcid = ? AND cod_brsrcacct != cod_brtgtacct AND osad.nbr_srcacct = srcacct.nbr_ownaccount AND osad.nbr_tgtacct = tgtacct.nbr_ownaccount MINUS SELECT oam.cod_entity1, oam.cod_entity2, oam.cod_ccy FROM olm_aetemplate_mst oam) m)";
					insertAETemplateMst = "INSERT INTO olm_aetemplate_mst (nbr_template_id, typ_entity, cod_entity1, cod_entity2, cod_ccy) "
							+ " (SELECT seq_ae_templateid.NEXTVAL, ?, m.cod_brsrcacct, m.cod_brtgtacct, m.cod_ccysrcacct FROM "
							+ " ((SELECT DISTINCT cod_brsrcacct, cod_brtgtacct, cod_ccysrcacct FROM olm_source_account_dtls osad, orbicash_accounts srcacct, "
							+ " orbicash_accounts tgtacct WHERE nbr_strcid = ? AND cod_brsrcacct != cod_brtgtacct AND osad.nbr_srcacct = srcacct.nbr_ownaccount "
							+ " AND osad.nbr_tgtacct = tgtacct.nbr_ownaccount AND (   osad.flg_groupsweeprule IS NULL OR osad.flg_groupsweeprule NOT IN "
							+ " ('DM', 'CM') ) UNION SELECT DISTINCT cod_brsrcacct, ?, cod_ccysrcacct FROM olm_source_account_dtls osad, "
							+ " orbicash_accounts srcacct WHERE nbr_strcid = ? AND osad.nbr_srcacct = srcacct.nbr_ownaccount AND osad.flg_groupsweeprule IN "
							+ " ('DM', 'CM') AND cod_brsrcacct != ? UNION SELECT DISTINCT cod_brtgtacct, ?, cod_ccysrcacct FROM olm_source_account_dtls osad, "
							+ " orbicash_accounts tgtacct WHERE nbr_strcid = ? AND osad.nbr_srcacct = tgtacct.nbr_ownaccount AND osad.flg_groupsweeprule IN "
							+ " ('DM', 'CM') AND cod_brtgtacct != ?) MINUS SELECT oam.cod_entity1, oam.cod_entity2, oam.cod_ccy FROM olm_aetemplate_mst oam) m)";
				}
			}
			// RBS - 132 ActiveTransaction Nostro Leg Ends
			// ENH_Nostroloro_branch end
			// JTest changes on 01-feb-11 starts
			if (logger.isDebugEnabled())
				logger.debug("insertAETemplateMst :: " + insertAETemplateMst);

			// insertMstStmt = conn.createStatement();
			insertMstStmt = conn.prepareStatement(insertAETemplateMst);
			// insertCount = insertMstStmt.executeUpdate(insertAETemplateMst);
			insertMstStmt.setString(1, ExecutionConstants.ENTITY_TYPE);
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("referenceID inserted --- " + referenceID);
			insertMstStmt.setString(2, referenceID);
			if ("Branch".equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)) {
				if (!("ICL".equalsIgnoreCase(referenceType))) {
					if (!("AT".equalsIgnoreCase(referenceType))) { // RBS - 132
																	// ActiveTransaction
																	// Nostro
																	// Leg
																	// Starts
						logger.debug("Innnn :: ");
						insertMstStmt
								.setString(
										3,
										ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER);
						insertMstStmt.setString(4, referenceID);
						insertMstStmt
								.setString(
										5,
										ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER);
						insertMstStmt
								.setString(
										6,
										ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER);
						insertMstStmt.setString(7, referenceID);
						insertMstStmt
								.setString(
										8,
										ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER);
					}
				}
				// RBS - 132 ActiveTransaction Nostro Leg Ends
			}
			/*
			 * logger.debug("Entering insdide BLE "); insertMstStmt.setString(1,
			 * ExecutionConstants.ENTITY_TYPE); insertMstStmt.setString(2,
			 * nbrStructureId); //HSBC_OSL_2643_CR_ICL_INTR Start
			 * insertMstStmt.setString(3, nbrStructureId);
			 * //HSBC_OSL_2643_CR_ICL_INTR End
			 */
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("insertMstStmt = " + insertMstStmt.toString());
			
			insertCount = insertMstStmt.executeUpdate();
			
			if (logger.isDebugEnabled())
				logger.debug("insertAETemplateMst....insertCount :: "
						+ insertCount);
			
			// JTest changes on 01-feb-11 ends
		} catch (Exception e) {
			logger.fatal("generateAETemplateMaster........." + e);
			throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, "", e);
		} finally {
			LMSConnectionUtility.close(insertMstStmt);// JTest changes on
														// 01-feb-11
		}
		if (logger.isDebugEnabled())// JTest changes on 01-feb-11
			logger.debug("generateAETemplateMaster....Leaving");
		return insertCount;
	}


    /**
     * 
     * @param srcAETemplateDO
     * @param tgtAETemplateDO
     * @param flgEntityType
     * @throws LMSException
     */
    private void generateAETemplateDetail(AccountingEntryTemplateDO srcAETemplateDO, 
	    AccountingEntryTemplateDO tgtAETemplateDO,String flgEntityType, Connection conn, PreparedStatement pstmt, PreparedStatement pstmtSettlemet) throws LMSException{
    	if(logger.isDebugEnabled())//JTest changes on 01-feb-11
	    	logger.debug("generateAETemplateDetail...."+legCount);
		String entity1 = "";
		String entity2 = "";
		if(logger.isDebugEnabled()){//JTest changes on 01-feb-11
	    	logger.debug("ExecutionConstants.ENTITY_TYPE :: "+ExecutionConstants.ENTITY_TYPE);
	    	logger.debug("ExecutionConstants.NLEG_AEPRIORITY :: "+ExecutionConstants.NLEG_AEPRIORITY);
		}
		if("HostSystem".equals(ExecutionConstants.ENTITY_TYPE)){
		    entity1 = srcAETemplateDO.getCodGl();
		    entity2 = tgtAETemplateDO.getCodGl();
		}else if("BLE".equals(ExecutionConstants.ENTITY_TYPE)){
		    entity1 = srcAETemplateDO.getCodBnkLglEnt();
		    entity2 = tgtAETemplateDO.getCodBnkLglEnt();
		}
		//ENH_Nostroloro_branch start
		else if(ExecutionConstants.NOSTRO_LORO_BRANCH.equals(ExecutionConstants.ENTITY_TYPE)){
		    entity1 = srcAETemplateDO.getCodBranch() ;
		    entity2 = tgtAETemplateDO.getCodBranch() ;
		}
		//ENH_Nostroloro_branch end
		//JTest changes on 01-feb-11 starts
		if(logger.isDebugEnabled()){
	    	logger.debug("srcAETemplateDO :: "+srcAETemplateDO);
			logger.debug("tgtAETemplateDO :: "+tgtAETemplateDO);
			logger.debug("flgEntityType :: "+flgEntityType);
			logger.debug("entity1 :: "+entity1);
			logger.debug("entity2 :: "+entity2);
		}
		//JTest changes on 01-feb-11 ends
	if(flgEntityType.equalsIgnoreCase("DS") && 
	(entity1 == null || entity2 == null || 
	entity1.trim().equals("") || entity2.trim().equals(""))){
	    return;
	}
	
	if(entity1.equalsIgnoreCase(entity2)){
	    persistAETemplateDetails(srcAETemplateDO,tgtAETemplateDO,flgEntityType, pstmt, pstmtSettlemet);
	    return;
	}
	
	ArrayList srcTgtNspNostroOwnerList =  new ArrayList();
	ArrayList srcTgtCBTSettlementList =  new ArrayList();
	ArrayList srcTgtCBTNostroAcctList =  new ArrayList();

	ArrayList srcCBTNostroList = new ArrayList();
	ArrayList tgtCBTNostroList = new ArrayList();
	ArrayList srcCBTSettlementList = new ArrayList();
	ArrayList tgtCBTSettlementList = new ArrayList();
	
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
	//Jiten -BILATERALSETTLEMENT
	ArrayList srcBiLateralSettlementList = new ArrayList(); 
	ArrayList tgtBiLateralSettlementList = new ArrayList();
	//Jiten -BILATERALSETTLEMENT
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
	
	if(flgEntityType.equals("DO")){
	    if(ExecutionConstants.BI_LATERAL.equals(ExecutionConstants.NLEG_AEPRIORITY)){
		if(isHostsExistAsBiLateral(srcAETemplateDO,tgtAETemplateDO,srcTgtNspNostroOwnerList, conn)){
		    generateAETemplateDetail(srcAETemplateDO,(AccountingEntryTemplateDO)srcTgtNspNostroOwnerList.get(0),"DO",conn, pstmt, pstmtSettlemet);
		    generateAETemplateDetail((AccountingEntryTemplateDO)srcTgtNspNostroOwnerList.get(1),tgtAETemplateDO,"DO", conn, pstmt, pstmtSettlemet);
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		    //BILATERALSETTLEMENT
		    generateAETemplateDetail((AccountingEntryTemplateDO)srcTgtNspNostroOwnerList.get(0),(AccountingEntryTemplateDO)srcTgtNspNostroOwnerList.get(1),"BS", conn, pstmt, pstmtSettlemet);
		    //BILATERALSETTLEMENT
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		}else if(isHostsExistAsCBT(srcAETemplateDO,tgtAETemplateDO,srcTgtCBTNostroAcctList,srcTgtCBTSettlementList, conn)){
		    generateAETemplateDetail(srcAETemplateDO,(AccountingEntryTemplateDO)srcTgtCBTNostroAcctList.get(0),"DO", conn, pstmt, pstmtSettlemet);

		    generateAETemplateDetail((AccountingEntryTemplateDO)srcTgtCBTNostroAcctList.get(1),tgtAETemplateDO,"DO", conn, pstmt, pstmtSettlemet);

		    generateAETemplateDetail((AccountingEntryTemplateDO)srcTgtCBTNostroAcctList.get(0),(AccountingEntryTemplateDO)srcTgtCBTSettlementList.get(0),"DS", conn, pstmt, pstmtSettlemet);

		    generateAETemplateDetail((AccountingEntryTemplateDO)srcTgtCBTSettlementList.get(1),(AccountingEntryTemplateDO)srcTgtCBTNostroAcctList.get(1),"DS", conn, pstmt, pstmtSettlemet);

		}
			//HSBC_DEF_FIX_14022011 -- Start
			else if(isSrcHostsExistAsCBT(srcAETemplateDO, tgtAETemplateDO, srcTgtCBTNostroAcctList, srcTgtCBTSettlementList, conn)){
			    generateAETemplateDetail(srcAETemplateDO,(AccountingEntryTemplateDO)srcTgtCBTNostroAcctList.get(0),"DO", conn, pstmt, pstmtSettlemet);
			    generateAETemplateDetail((AccountingEntryTemplateDO)srcTgtCBTNostroAcctList.get(1),tgtAETemplateDO,"DO", conn, pstmt, pstmtSettlemet);
			    generateAETemplateDetail((AccountingEntryTemplateDO)srcTgtCBTNostroAcctList.get(0),(AccountingEntryTemplateDO)srcTgtCBTSettlementList.get(0),"DS", conn, pstmt, pstmtSettlemet);
			    generateAETemplateDetail((AccountingEntryTemplateDO)srcTgtCBTSettlementList.get(1),(AccountingEntryTemplateDO)srcTgtCBTNostroAcctList.get(1),"DS", conn, pstmt, pstmtSettlemet);
			}
			//HSBC_DEF_FIX_14022011 -- End
		else if(isCBTExistsForHost(srcAETemplateDO,tgtAETemplateDO,srcCBTNostroList,tgtCBTNostroList,srcCBTSettlementList,tgtCBTSettlementList, conn)){	    
		    generateAETemplateDetail(srcAETemplateDO,(AccountingEntryTemplateDO)srcCBTNostroList.get(0),"DO", conn, pstmt, pstmtSettlemet);
		    generateAETemplateDetail((AccountingEntryTemplateDO)srcCBTNostroList.get(1),(AccountingEntryTemplateDO)tgtCBTNostroList.get(1),"DO", conn, pstmt, pstmtSettlemet);
		    generateAETemplateDetail((AccountingEntryTemplateDO)tgtCBTNostroList.get(0),tgtAETemplateDO,"DO", conn, pstmt, pstmtSettlemet);
		    
		    generateAETemplateDetail((AccountingEntryTemplateDO)srcCBTNostroList.get(0),(AccountingEntryTemplateDO)srcCBTSettlementList.get(0),"DS", conn, pstmt, pstmtSettlemet);
		    generateAETemplateDetail((AccountingEntryTemplateDO)srcCBTSettlementList.get(1),(AccountingEntryTemplateDO)srcCBTNostroList.get(1),"DS", conn, pstmt, pstmtSettlemet);
		    generateAETemplateDetail((AccountingEntryTemplateDO)tgtCBTNostroList.get(1),(AccountingEntryTemplateDO)tgtCBTSettlementList.get(1),"DS", conn, pstmt, pstmtSettlemet);
		    generateAETemplateDetail((AccountingEntryTemplateDO)tgtCBTSettlementList.get(0),(AccountingEntryTemplateDO)tgtCBTNostroList.get(0),"DS", conn, pstmt, pstmtSettlemet);
		}else{
				if(logger.isDebugEnabled())//JTest changes on 01-feb-11
					logger.debug("Getting No Entry, so making persistence value to false....");
		    flgPersistTemplate = false;
		    return; 
		}
	    }else if(ExecutionConstants.NLEG_CENTRAL.equals(ExecutionConstants.NLEG_AEPRIORITY)){
		if(isHostsExistAsCBT(srcAETemplateDO,tgtAETemplateDO,srcTgtCBTNostroAcctList,srcTgtCBTSettlementList, conn)){
		    generateAETemplateDetail(srcAETemplateDO,(AccountingEntryTemplateDO)srcTgtCBTNostroAcctList.get(0),"DO", conn, pstmt, pstmtSettlemet);

		    generateAETemplateDetail((AccountingEntryTemplateDO)srcTgtCBTNostroAcctList.get(1),tgtAETemplateDO,"DO", conn, pstmt, pstmtSettlemet);

		    generateAETemplateDetail((AccountingEntryTemplateDO)srcTgtCBTNostroAcctList.get(0),(AccountingEntryTemplateDO)srcTgtCBTSettlementList.get(0),"DS", conn, pstmt, pstmtSettlemet);

		    generateAETemplateDetail((AccountingEntryTemplateDO)srcTgtCBTSettlementList.get(1),(AccountingEntryTemplateDO)srcTgtCBTNostroAcctList.get(1),"DS", conn, pstmt, pstmtSettlemet);

		}
			//HSBC_DEF_FIX_14022011 -- Start
			else if(isSrcHostsExistAsCBT(srcAETemplateDO, tgtAETemplateDO, srcTgtCBTNostroAcctList, srcTgtCBTSettlementList, conn)){
			    generateAETemplateDetail(srcAETemplateDO,(AccountingEntryTemplateDO)srcTgtCBTNostroAcctList.get(0),"DO", conn, pstmt, pstmtSettlemet);
			    generateAETemplateDetail((AccountingEntryTemplateDO)srcTgtCBTNostroAcctList.get(1),tgtAETemplateDO,"DO", conn, pstmt, pstmtSettlemet);
			    generateAETemplateDetail((AccountingEntryTemplateDO)srcTgtCBTNostroAcctList.get(0),(AccountingEntryTemplateDO)srcTgtCBTSettlementList.get(0),"DS", conn, pstmt, pstmtSettlemet);
			    generateAETemplateDetail((AccountingEntryTemplateDO)srcTgtCBTSettlementList.get(1),(AccountingEntryTemplateDO)srcTgtCBTNostroAcctList.get(1),"DS", conn, pstmt, pstmtSettlemet);
			}
			//HSBC_DEF_FIX_14022011 -- End
		else if(isCBTExistsForHost(srcAETemplateDO,tgtAETemplateDO,srcCBTNostroList,tgtCBTNostroList,srcCBTSettlementList,tgtCBTSettlementList, conn)){	    
		    generateAETemplateDetail(srcAETemplateDO,(AccountingEntryTemplateDO)srcCBTNostroList.get(0),"DO", conn, pstmt, pstmtSettlemet);
		    generateAETemplateDetail((AccountingEntryTemplateDO)srcCBTNostroList.get(1),(AccountingEntryTemplateDO)tgtCBTNostroList.get(1),"DO", conn, pstmt, pstmtSettlemet);
		    generateAETemplateDetail((AccountingEntryTemplateDO)tgtCBTNostroList.get(0),tgtAETemplateDO,"DO", conn, pstmt, pstmtSettlemet);
		    
		    generateAETemplateDetail((AccountingEntryTemplateDO)srcCBTNostroList.get(0),(AccountingEntryTemplateDO)srcCBTSettlementList.get(0),"DS", conn, pstmt, pstmtSettlemet);
		    generateAETemplateDetail((AccountingEntryTemplateDO)srcCBTSettlementList.get(1),(AccountingEntryTemplateDO)srcCBTNostroList.get(1),"DS", conn, pstmt, pstmtSettlemet);
		    generateAETemplateDetail((AccountingEntryTemplateDO)tgtCBTNostroList.get(1),(AccountingEntryTemplateDO)tgtCBTSettlementList.get(1),"DS", conn, pstmt, pstmtSettlemet);
		    generateAETemplateDetail((AccountingEntryTemplateDO)tgtCBTSettlementList.get(0),(AccountingEntryTemplateDO)tgtCBTNostroList.get(0),"DS", conn, pstmt, pstmtSettlemet);
		}else if(isHostsExistAsBiLateral(srcAETemplateDO,tgtAETemplateDO,srcTgtNspNostroOwnerList, conn)){
		    generateAETemplateDetail(srcAETemplateDO,(AccountingEntryTemplateDO)srcTgtNspNostroOwnerList.get(0),"DO", conn, pstmt, pstmtSettlemet);
		    generateAETemplateDetail((AccountingEntryTemplateDO)srcTgtNspNostroOwnerList.get(1),tgtAETemplateDO,"DO", conn, pstmt, pstmtSettlemet);
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		    //BILATERALSETTLEMENT
		    generateAETemplateDetail((AccountingEntryTemplateDO)srcTgtNspNostroOwnerList.get(0),(AccountingEntryTemplateDO)srcTgtNspNostroOwnerList.get(1),"BS", conn, pstmt, pstmtSettlemet);
		    //BILATERALSETTLEMENT
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		}else{
		    logger.debug("Getting No Entry, so making persistence value to false....");
		    flgPersistTemplate = false;
		    return;
		}
	    }
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
	}else if(flgEntityType.equals("DS") || flgEntityType.equals("BS")){
	    if(isHostsExistAsSettlement(srcAETemplateDO,tgtAETemplateDO,srcTgtCBTSettlementList, conn)){
		generateAETemplateDetail(srcAETemplateDO,(AccountingEntryTemplateDO)srcTgtCBTSettlementList.get(1),flgEntityType, conn, pstmt, pstmtSettlemet);
		generateAETemplateDetail((AccountingEntryTemplateDO)srcTgtCBTSettlementList.get(0),tgtAETemplateDO,flgEntityType, conn, pstmt, pstmtSettlemet);
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
	    }/*else if(isCBTExistsForHost(srcAETemplateDO,tgtAETemplateDO,srcCBTNostroList,tgtCBTNostroList,srcCBTSettlementList,tgtCBTSettlementList)){
		generateAETemplateDetail(srcAETemplateDO,(AccountingEntryTemplateDO)srcCBTSettlementList.get(0),"DS");
		generateAETemplateDetail((AccountingEntryTemplateDO)srcCBTSettlementList.get(1),(AccountingEntryTemplateDO)tgtCBTSettlementList.get(1),"DS");
		generateAETemplateDetail((AccountingEntryTemplateDO)tgtCBTSettlementList.get(0),tgtAETemplateDO,"DS");
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
	    }*/else if(flgEntityType.equals("BS") && isSettlementExistForBiLateral(srcAETemplateDO, tgtAETemplateDO, 
		    srcBiLateralSettlementList, tgtBiLateralSettlementList, conn)){
		try{
		    if(srcBiLateralSettlementList.size() > 0){
			generateAETemplateDetail(srcAETemplateDO,(AccountingEntryTemplateDO)srcBiLateralSettlementList.get(0),flgEntityType, conn, pstmt, pstmtSettlemet);
			AccountingEntryTemplateDO srcClonedTemplateDO = new  AccountingEntryTemplateDO();
			srcClonedTemplateDO =  (AccountingEntryTemplateDO)(((AccountingEntryTemplateDO)srcBiLateralSettlementList.get(1)).clone());
			srcClonedTemplateDO.setFlgPersist("N");
			generateAETemplateDetail((AccountingEntryTemplateDO)srcBiLateralSettlementList.get(1),srcClonedTemplateDO,flgEntityType, conn, pstmt, pstmtSettlemet);
		    }
		    if(tgtBiLateralSettlementList.size() > 0){
			generateAETemplateDetail((AccountingEntryTemplateDO)tgtBiLateralSettlementList.get(0),tgtAETemplateDO,flgEntityType, conn, pstmt, pstmtSettlemet);
			AccountingEntryTemplateDO tgtClonedTemplateDO = new  AccountingEntryTemplateDO();
			tgtClonedTemplateDO =  (AccountingEntryTemplateDO)(((AccountingEntryTemplateDO)tgtBiLateralSettlementList.get(1)).clone());
			tgtClonedTemplateDO.setFlgPersist("N");
			generateAETemplateDetail(tgtClonedTemplateDO,(AccountingEntryTemplateDO)tgtBiLateralSettlementList.get(1),flgEntityType, conn, pstmt, pstmtSettlemet);
		    }
		}catch(Exception e){
		    logger.fatal("generateAETemplateDetail........."+e);
		    throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, "", e);
		}

	    }else{
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		return;
	    }
	}
		if(logger.isDebugEnabled())//JTest changes on 01-feb-11
			logger.debug("generateAETemplateDetail....Leaving");
	return;

    }
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
    // Jiten - BILATERALSETTLEMENT - Changed the Method name & Passed the message Code
    //private void alertForPathNotCompleted(HashMap messageDetails) throws LMSException
    private void generateAlert(HashMap messageDetails) throws LMSException
    {
	// TODO Auto-generated method stub
	Connection conn=null;
	if (logger.isDebugEnabled())
	    logger.debug("Entering generateAlert  method");
	try{

			conn = LMSConnectionUtility.getTxConnection();
				
		    if(logger.isDebugEnabled())//JTest changes on 01-feb-11
		    	logger.debug("messageDetails>> "+messageDetails);

	    MessageEvent msgEvent = new MessageEvent();
	    msgEvent.setMessageDate(LMSUtility.getBusinessDate().getTime());
	    // - Jiten BILATERALSETTLEMENT - Passes the Message Code
	    //msgEvent.setMessageCode("M0161");
	    msgEvent.setMessageCode((String)messageDetails.get("MSGCODE"));
	    //- Jiten BILATERALSETTLEMENT
	    msgEvent.setMessageDetails(messageDetails);
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends

	    MessageLogger msgLog = MessageLogger.getInstance();
	    msgLog.logMessage(conn,msgEvent);

	}
	catch (Exception e)
	{
	    logger.fatal("Exception in alertForSweepExecutionFailure() method of AccountEntryRequestSLSBBean : ", e);
	    throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, e.getMessage());
	}
		finally{
			LMSConnectionUtility.close(conn);//JTest changes on 01-feb-11
		}
    }

    /**
     * 
     * @param srcAETemplateDO
     * @param tgtAETemplateDO
     * @param flgEntityType
     * @throws LMSException
     */
    private void persistAETemplateDetails(AccountingEntryTemplateDO srcAETemplateDO,
	    AccountingEntryTemplateDO tgtAETemplateDO,String flgEntityType, PreparedStatement pstmt, PreparedStatement pstmtSettlemet) throws LMSException{
	try{
	    if(flgEntityType.equalsIgnoreCase("DO")){
		addToPreparedStatement(srcAETemplateDO,tgtAETemplateDO,pstmt);
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
	    //}else if(flgEntityType.equalsIgnoreCase("DS")){// - BILATERALSETTLEMENT
	    }else if(flgEntityType.equalsIgnoreCase("DS") || flgEntityType.equalsIgnoreCase("BS")){
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		srcAETemplateDO.setFlgSettlementEntry("Y");
		tgtAETemplateDO.setFlgSettlementEntry("Y");
		addToPreparedStatement(srcAETemplateDO,tgtAETemplateDO,pstmtSettlemet);
	    }
	}catch(Exception e){
	    logger.fatal("Exception persistAETemplateDetails "+e);
	    throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, "", e);
	}
    }


    /**
     * 
     * @param srcAETemplateDO
     * @param tgtAETemplateDO
     * @param pstmt
     * @throws LMSException
     */
    private void addToPreparedStatement(AccountingEntryTemplateDO srcAETemplateDO,
	    AccountingEntryTemplateDO tgtAETemplateDO,PreparedStatement pstmt) throws LMSException{
	try{
		if(logger.isDebugEnabled())//JTest changes on 01-feb-11
	    	logger.debug("Inside addToPreparedStatement........... ");
	    //Leg1
	    if(srcAETemplateDO.getFlgPersist().equals("Y")){
		//Adding For Debit Set
		pstmt.clearParameters();
		pstmt.setString(1, srcAETemplateDO.getNbrTemplateId());
		pstmt.setString(2, "D");
		pstmt.setLong(3, legCount);
		pstmt.setString(4, srcAETemplateDO.getNbrOwnAccount());
		pstmt.setString(5, srcAETemplateDO.getNbrCoreAccount());
		pstmt.setString(6, srcAETemplateDO.getCodBic());
		pstmt.setString(7, srcAETemplateDO.getCodBnkLglEnt());
		pstmt.setString(8, srcAETemplateDO.getCodBranch());
		pstmt.setString(9, srcAETemplateDO.getCodCcy());
		pstmt.setString(10, srcAETemplateDO.getCodGl());
		pstmt.setString(11, srcAETemplateDO.getCodCountry());
		pstmt.setString(12, srcAETemplateDO.getFlgAcctType());
		pstmt.setString(13, ExecutionConstants.DEBIT);
		//pstmt.setString(14, srcAETemplateDO.getFlgSrcTgtTyp());
		pstmt.setString(14, ExecutionConstants.SOURCE);
		pstmt.setString(15, srcAETemplateDO.getTxtIbanAcct());

		pstmt.setString(16, srcAETemplateDO.getNbrContractNo());
		pstmt.setString(17, srcAETemplateDO.getNbrSeqNo());
		pstmt.setString(18, srcAETemplateDO.getFlgSettlementEntry());
		pstmt.setString(19, srcAETemplateDO.getNbrSettlementDays());
		pstmt.setDate(20,LMSUtility.truncSqlDate( new java.sql.Date(srcAETemplateDO.getDateEffective().getTime())));
		pstmt.setString(21,srcAETemplateDO.getCodTriggeringGl());
		if(srcAETemplateDO.getDateEnd() != null){
		    pstmt.setDate(22,LMSUtility.truncSqlDate((java.sql.Date)srcAETemplateDO.getDateEnd()));
		}else{
		    pstmt.setNull(22,Types.DATE);
		}
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		//Jiten - BILATERALSETTLEMENT - Start
		if(srcAETemplateDO.getFlgSettlementPath() != null 
			&& srcAETemplateDO.getFlgSettlementPath().trim().length() > 0
			&& srcAETemplateDO.getFlgSettlementEntry().equalsIgnoreCase("Y")){
		    pstmt.setString(23,srcAETemplateDO.getFlgSettlementPath());
		}else{
		    pstmt.setNull(23,Types.VARCHAR);
		}
		//Jiten - BILATERALSETTLEMENT - End
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		pstmt.addBatch();   

		//Adding For CREDIT Set
		pstmt.clearParameters();
		pstmt.setString(1, srcAETemplateDO.getNbrTemplateId());
		pstmt.setString(2, "C");
		pstmt.setLong(3, legCount);
		pstmt.setString(4, srcAETemplateDO.getNbrOwnAccount());
		pstmt.setString(5, srcAETemplateDO.getNbrCoreAccount());
		pstmt.setString(6, srcAETemplateDO.getCodBic());
		pstmt.setString(7, srcAETemplateDO.getCodBnkLglEnt());
		pstmt.setString(8, srcAETemplateDO.getCodBranch());
		pstmt.setString(9, srcAETemplateDO.getCodCcy());
		pstmt.setString(10, srcAETemplateDO.getCodGl());
		pstmt.setString(11, srcAETemplateDO.getCodCountry());
		pstmt.setString(12, srcAETemplateDO.getFlgAcctType());
		pstmt.setString(13, ExecutionConstants.CREDIT);

		//pstmt.setString(14, srcAETemplateDO.getFlgSrcTgtTyp());
		pstmt.setString(14, ExecutionConstants.TARGET);
		pstmt.setString(15, srcAETemplateDO.getTxtIbanAcct());

		pstmt.setString(16, srcAETemplateDO.getNbrContractNo());
		pstmt.setString(17, srcAETemplateDO.getNbrSeqNo());
		pstmt.setString(18, srcAETemplateDO.getFlgSettlementEntry());
		pstmt.setString(19, srcAETemplateDO.getNbrSettlementDays());
		pstmt.setDate(20,LMSUtility.truncSqlDate( new java.sql.Date(srcAETemplateDO.getDateEffective().getTime())));
		pstmt.setString(21,srcAETemplateDO.getCodTriggeringGl());
		if(srcAETemplateDO.getDateEnd() != null){
		    pstmt.setDate(22,LMSUtility.truncSqlDate((java.sql.Date)srcAETemplateDO.getDateEnd()));
		}else{
		    pstmt.setNull(22,Types.DATE);
		}
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		//Jiten - BILATERALSETTLEMENT - Start
		if(srcAETemplateDO.getFlgSettlementPath() != null 
			&& srcAETemplateDO.getFlgSettlementPath().trim().length() > 0
			&& srcAETemplateDO.getFlgSettlementEntry().equalsIgnoreCase("Y")){
		    pstmt.setString(23,srcAETemplateDO.getFlgSettlementPath());
		}else{
		    pstmt.setNull(23,Types.VARCHAR);
		}
		//Jiten - BILATERALSETTLEMENT - End
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		pstmt.addBatch(); 
		legCount++;
	    }
	    //Leg2

	    if(tgtAETemplateDO.getFlgPersist().equals("Y")){
		pstmt.clearParameters();

		//Adding For DEBIT Set
		pstmt.setString(1, tgtAETemplateDO.getNbrTemplateId());
		pstmt.setString(2, "D");
		pstmt.setLong(3, legCount);
		pstmt.setString(4, tgtAETemplateDO.getNbrOwnAccount());
		pstmt.setString(5, tgtAETemplateDO.getNbrCoreAccount());
		pstmt.setString(6, tgtAETemplateDO.getCodBic());
		pstmt.setString(7, tgtAETemplateDO.getCodBnkLglEnt());
		pstmt.setString(8, tgtAETemplateDO.getCodBranch());
		pstmt.setString(9, tgtAETemplateDO.getCodCcy());
		pstmt.setString(10, tgtAETemplateDO.getCodGl());
		pstmt.setString(11, tgtAETemplateDO.getCodCountry());
		pstmt.setString(12, tgtAETemplateDO.getFlgAcctType());
		pstmt.setString(13, ExecutionConstants.CREDIT);
		//pstmt.setString(14, tgtAETemplateDO.getFlgSrcTgtTyp());
		pstmt.setString(14, ExecutionConstants.TARGET);
		pstmt.setString(15, tgtAETemplateDO.getTxtIbanAcct());

		pstmt.setString(16, tgtAETemplateDO.getNbrContractNo());
		pstmt.setString(17, tgtAETemplateDO.getNbrSeqNo());
		pstmt.setString(18, tgtAETemplateDO.getFlgSettlementEntry());
		pstmt.setString(19, tgtAETemplateDO.getNbrSettlementDays());
		pstmt.setDate(20,LMSUtility.truncSqlDate( new java.sql.Date(tgtAETemplateDO.getDateEffective().getTime())));
		pstmt.setString(21,tgtAETemplateDO.getCodTriggeringGl());
		if(tgtAETemplateDO.getDateEnd() != null){
		    pstmt.setDate(22,LMSUtility.truncSqlDate((java.sql.Date)tgtAETemplateDO.getDateEnd()));
		}else{
		    pstmt.setNull(22,Types.DATE);
		}
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		//Jiten - BILATERALSETTLEMENT - Start
		if(tgtAETemplateDO.getFlgSettlementPath() != null 
			&& tgtAETemplateDO.getFlgSettlementPath().trim().length() > 0
			&& tgtAETemplateDO.getFlgSettlementEntry().equalsIgnoreCase("Y")){
		    pstmt.setString(23,tgtAETemplateDO.getFlgSettlementPath());
		}else{
		    pstmt.setNull(23,Types.VARCHAR);
		}
		//Jiten - BILATERALSETTLEMENT - End
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		pstmt.addBatch();

		//Adding For CREDIT Set
		pstmt.clearParameters();

		pstmt.setString(1, tgtAETemplateDO.getNbrTemplateId());
		pstmt.setString(2, "C");
		pstmt.setLong(3, legCount);
		pstmt.setString(4, tgtAETemplateDO.getNbrOwnAccount());
		pstmt.setString(5, tgtAETemplateDO.getNbrCoreAccount());
		pstmt.setString(6, tgtAETemplateDO.getCodBic());
		pstmt.setString(7, tgtAETemplateDO.getCodBnkLglEnt());
		pstmt.setString(8, tgtAETemplateDO.getCodBranch());
		pstmt.setString(9, tgtAETemplateDO.getCodCcy());
		pstmt.setString(10, tgtAETemplateDO.getCodGl());
		pstmt.setString(11, tgtAETemplateDO.getCodCountry());
		pstmt.setString(12, tgtAETemplateDO.getFlgAcctType());
		pstmt.setString(13, ExecutionConstants.DEBIT);

		//pstmt.setString(14, tgtAETemplateDO.getFlgSrcTgtTyp());
		pstmt.setString(14, ExecutionConstants.SOURCE);
		pstmt.setString(15, tgtAETemplateDO.getTxtIbanAcct());

		pstmt.setString(16, tgtAETemplateDO.getNbrContractNo());
		pstmt.setString(17, tgtAETemplateDO.getNbrSeqNo());
		pstmt.setString(18, tgtAETemplateDO.getFlgSettlementEntry());
		pstmt.setString(19, tgtAETemplateDO.getNbrSettlementDays());
		pstmt.setDate(20,LMSUtility.truncSqlDate( new java.sql.Date(tgtAETemplateDO.getDateEffective().getTime())));
		pstmt.setString(21,tgtAETemplateDO.getCodTriggeringGl());
		if(tgtAETemplateDO.getDateEnd() != null){
		    pstmt.setDate(22,LMSUtility.truncSqlDate((java.sql.Date)tgtAETemplateDO.getDateEnd()));
		}else{
		    pstmt.setNull(22,Types.DATE);
		}
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		//Jiten - BILATERALSETTLEMENT - Start
		if(tgtAETemplateDO.getFlgSettlementPath() != null 
			&& tgtAETemplateDO.getFlgSettlementPath().trim().length() > 0
			&& tgtAETemplateDO.getFlgSettlementEntry().equalsIgnoreCase("Y")){
		    pstmt.setString(23,tgtAETemplateDO.getFlgSettlementPath());
		}else{
		    pstmt.setNull(23,Types.VARCHAR);
		}
		//Jiten - BILATERALSETTLEMENT - End
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		pstmt.addBatch(); 

		legCount++;
	    }
	    
	}catch(Exception e){
	    logger.fatal("Exception addToPreparedStatement "+e);
	    throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, "", e);
	}
	if(logger.isDebugEnabled())//JTest changes on 01-feb-11
    	logger.debug("addToPreparedStatement....Leaving"+legCount);
    }

    /**
     *  
     * @param connection
     * @param nbrProcessId
     * @throws LMSException
     */
    public void markInstrFailedForNoAETemplate
    (Connection connection,long nbrProcessId)throws LMSException{
    	//JTest changes on 01-feb-11 starts
    	if(logger.isDebugEnabled())
    		logger.debug("Inside markInstrFailedForNoAETemplate....");
		PreparedStatement stmt = null;
		//JTest changes on 01-feb-11 ends
		try {
		    /*String sqlQuery = "UPDATE OLM_INSTRTXN SET TXT_STATUS = 'FAILED',COD_REASON = 'OLM999017' " +
		    "WHERE NBR_PROCESSID = '"+nbrProcessId+"' AND   NBR_INSTRTXNID IN ( " +
		    "SELECT OI.NBR_INSTRTXNID FROM OLM_INSTRTXN OI,OLM_AETEMPLATE_MST OAM " +
		    "WHERE (OI.COD_GLSRCACCT||OI.COD_GLTGTACCT||OI.COD_CCYSRCACCT) " +
		    "= (OAM.COD_ENTITY1||OAM.COD_ENTITY2||OAM.COD_CCY) " +
		    "AND OAM.NBR_TEMPLATE_ID NOT IN (SELECT NBR_TEMPLATE_ID FROM OLM_AETEMPLATE_DTLS) " +
		    "AND OI.NBR_PROCESSID = '"+nbrProcessId+"')";*/
		    
		    StringBuilder sqlQueryBuff = new StringBuilder("UPDATE OLM_INSTRTXN SET TXT_STATUS = 'FAILED', COD_REASON = 'OLM999017' ");
		    //sqlQueryBuff.append("WHERE NBR_PROCESSID = '"+nbrProcessId+"' AND NBR_INSTRTXNID IN ");
		    sqlQueryBuff.append("WHERE NBR_PROCESSID = ? AND NBR_INSTRTXNID IN ");//JTest changes on 01-feb-11
		    sqlQueryBuff.append("( SELECT OI.NBR_INSTRTXNID FROM OLM_INSTRTXN OI, OLM_AETEMPLATE_MST OAM WHERE ");
		    
		    if("HostSystem".equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
			sqlQueryBuff.append(" OI.COD_GLSRCACCT = OAM.COD_ENTITY1 AND OI.COD_GLTGTACCT = OAM.COD_ENTITY2 AND OI.COD_CCYSRCACCT = OAM.COD_CCY AND "); //Retro_Mashreq
		    }else if("BLE".equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
			sqlQueryBuff.append(" OI.COD_SRCBNKLGLENT = OAM.COD_ENTITY1 AND OI.COD_TGTBNKLGLENT = OAM.COD_ENTITY2 AND OI.COD_CCYSRCACCT = OAM.COD_CCY AND "); //Retro_Mashreq
		    }
		    //ENH_Nostroloro_branch start
		    else if(ExecutionConstants.NOSTRO_LORO_BRANCH.equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
				sqlQueryBuff.append(" OI.COD_BRSRCACCT = OAM.COD_ENTITY1 AND OI.COD_BRTGTACCT = OAM.COD_ENTITY2 AND OI.COD_CCYSRCACCT = OAM.COD_CCY AND "); 
			}
		    //ENH_Nostroloro_branch end
		    /*sqlQueryBuff.append(" AND( OAM.NBR_TEMPLATE_ID NOT IN (SELECT NBR_TEMPLATE_ID FROM OLM_AETEMPLATE_DTLS)  ");
		    sqlQueryBuff.append(" OR (OAM.NBR_TEMPLATE_ID IN (SELECT NBR_TEMPLATE_ID FROM OLM_AETEMPLATE_DTLS) ");
		    sqlQueryBuff.append(" AND OI.DAT_BUSINESS < (SELECT DISTINCT DAT_EFFECTIVE FROM OLM_AETEMPLATE_DTLS OAD  ");
		    sqlQueryBuff.append(" WHERE OAD.NBR_TEMPLATE_ID = OAM.NBR_TEMPLATE_ID))) AND OI.NBR_PROCESSID = '"+nbrProcessId+"') ");*/
		    
		    sqlQueryBuff.append(" NOT EXISTS (SELECT NBR_TEMPLATE_ID FROM OLM_AETEMPLATE_DTLS OAD "); //Retro_Mashreq
		    sqlQueryBuff.append("WHERE   OAD.NBR_TEMPLATE_ID = OAM.NBR_TEMPLATE_ID  AND OI.DAT_BUSINESS ");
		    sqlQueryBuff.append("BETWEEN OAD.DAT_EFFECTIVE AND NVL(OAD.DAT_END, TO_DATE('31-DEC-9999','DD-MON-YYYY') )) ");
		    //sqlQueryBuff.append("AND OI.NBR_PROCESSID = '"+nbrProcessId+"') ");
		    sqlQueryBuff.append("AND OI.NBR_PROCESSID = ? AND (OI.COD_SRCBNKLGLENT <> OI.COD_TGTBNKLGLENT OR OI.COD_CCYSRCACCT <> OI.COD_CCYTGTACCT) AND OI.FLG_SRC_ACC_TYPE = 'I' AND OI.FLG_TGT_ACC_TYPE = 'I' )");//JTest changes on 01-feb-11
		    if(logger.isDebugEnabled())
		    	logger.debug("sqlQuery :: "+sqlQueryBuff.toString());
		    
		    //JTest changes on 01-feb-11 starts
		    //stmt = connection.createStatement();
		    stmt = connection.prepareStatement(sqlQueryBuff.toString());
		    stmt.setLong(1, nbrProcessId);
		    stmt.setLong(2, nbrProcessId);
		    int updCnt = stmt.executeUpdate();
		    
		    if(logger.isDebugEnabled())
		    	logger.debug("Instruction Marked as Failed Count :: "+updCnt);
		    //JTest changes on 01-feb-11 ends
	
		} catch (SQLException e) {
		    logger.fatal("Exception in markInstrFailedForNoAETemplate :: "+e);
		    throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, "", e);
		}finally{
			DBUtils.closeResource(stmt);
		}
		if(logger.isDebugEnabled())//JTest changes on 01-feb-11
			logger.debug("Leaving markInstrFailedForNoAETemplate....");
    }		

    /**
     * 
     */
    public void generateAEFromTemplateForBODReversal(Connection connection,long nbrProcessId)
    throws LMSException{
    	//JTest changes on 01-feb-11 starts
    	if(logger.isDebugEnabled())
    		logger.debug("Inside generateAEFromTemplateForBODReversal........");
		PreparedStatement stmt = null;
		//JTest changes on 01-feb-11 ends
		try{
		    StringBuilder insertSqlBuff = new StringBuilder();
		    //SUP_IS6597 starts
		    insertSqlBuff.append("INSERT INTO OLM_ACCOUNTINGENTRY (NBR_PROCESSID, NBR_ACCTENTRYID,"); 
		    insertSqlBuff.append("NBR_LEGID, NBR_INSTRTXNID, NBR_LOTID, NBR_OWNACCOUNT, NBR_COREACCOUNT,");
		    insertSqlBuff.append("FLG_ACCTTYPE, COD_BRANCH, COD_BIC, COD_GL, COD_CCY, FLG_DRCR, AMT_TXN,");
		    insertSqlBuff.append("DAT_VALUE, TXT_STATUS, COD_BNKLGLENT, TXT_IBANACCT, DAT_BUSINESS, FLG_SRCTGTTYP,");
		    insertSqlBuff.append("FLG_MIRRORENTRY, DAT_SYSTEM, FLG_FORCEDR, FLG_REVERSE, FLG_ISSETTLEMENT_ENTRY,");
		    insertSqlBuff.append("COD_TRIGGERING_GL, COD_TXN, TXT_NARRATION1, COD_REF_ID, FLG_ENTRYTYPE, NBR_SETTLEMENT_DAYS,COD_ROUTING) ");//15.2_PERFORMANCE_CHANGE
		    
		    //insertSqlBuff.append("(SELECT CASE WHEN OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' THEN '-2' ELSE '"+nbrProcessId+"' END, ");
		    /*insertSqlBuff.append("(SELECT CASE WHEN OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' THEN '-2' ELSE ? END, ");//JTest changes on 01-feb-11//SUP_IS6597 ENDS
		    insertSqlBuff.append("OA.NBR_ACCTENTRYID, OAD.NBR_LEGID, OI.NBR_INSTRTXNID, OA.NBR_LOTID, ");
		    insertSqlBuff.append("OAD.NBR_OWNACCOUNT, OAD.NBR_COREACCOUNT, OAD.FLG_ACCTTYPE, OAD.COD_BRANCH, ");
		    insertSqlBuff.append("OAD.COD_BIC, OAD.COD_GL, OAD.COD_CCY, OAD.FLG_DRCR, OA.AMT_TXN, ");
		    insertSqlBuff.append("OA.DAT_BUSINESS, 'PENDING', OAD.COD_BNKLGLENT, OAD.TXT_IBANACCT, ");
		    insertSqlBuff.append("OA.DAT_BUSINESS, OAD.FLG_SRCTGTTYP, 'N', OA.DAT_SYSTEM, OA.FLG_FORCEDR,");
		    insertSqlBuff.append("OA.FLG_REVERSE, OAD.FLG_ISSETTLEMENT_ENTRY, OAD.COD_TRIGGERING_GL, ");
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		    //Jiten - BILATERALSETTLEMENT - Start
		    //insertSqlBuff.append("OA.COD_TXN, OA.TXT_NARRATION1, OA.NBR_ACCTENTRYID || '.' || OAD.NBR_LEGID, ");
		    //- DeLocalize Settlement path - Start
		    //insertSqlBuff.append("CASE WHEN OAD.FLG_SETTLEMENTPATH = 'C' AND OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' ");
		    insertSqlBuff.append("CASE WHEN (OAD.FLG_SETTLEMENTPATH = 'C' OR OAD.FLG_SETTLEMENTPATH = 'D') AND OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' ");
		    //- DeLocalize Settlement path - End
		    insertSqlBuff.append("THEN OA.COD_TXN || 'CS' ");
		    insertSqlBuff.append("WHEN OAD.FLG_SETTLEMENTPATH = 'B' AND OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' ");
		    insertSqlBuff.append("THEN OA.COD_TXN || 'BS' ");
		    insertSqlBuff.append("ELSE OA.COD_TXN END, ");
		    //Jiten - BILATERALSETTLEMENT - 2
		    /*insertSqlBuff.append("CASE WHEN OAD.FLG_SETTLEMENTPATH = 'C' AND OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' ");
		    insertSqlBuff.append("THEN OA.TXT_NARRATION1 || ' Central Settlement' ");
		    insertSqlBuff.append("WHEN OAD.FLG_SETTLEMENTPATH = 'B' AND OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' ");
		    insertSqlBuff.append("THEN OA.TXT_NARRATION1 || ' BI-Lateral Settlement' ");
		    insertSqlBuff.append("ELSE OA.TXT_NARRATION1 END,");*/
/*
		  
		    insertSqlBuff.append("OTC.TXT_NARRATION1,");
		    //Jiten - BILATERALSETTLEMENT - 2
		    insertSqlBuff.append("OA.NBR_ACCTENTRYID || '.' || OAD.NBR_LEGID,");
		    //Jiten - BILATERALSETTLEMENT - End
		    insertSqlBuff.append("OA.FLG_ENTRYTYPE, OAD.NBR_SETTLEMENT_DAYS FROM OLM_INSTRTXNVW OI, ");
		    //Jiten - BILATERALSETTLEMENT - 2
		    //insertSqlBuff.append("OLM_ACCOUNTINGENTRY OA, OLM_AETEMPLATE_MST OAM, OLM_AETEMPLATE_DTLS OAD ");
		    insertSqlBuff.append("OLM_ACCOUNTINGENTRY OA, OLM_AETEMPLATE_MST OAM, OLM_AETEMPLATE_DTLS OAD,OPOOL_TXN_CODES OTC ");
		    //Jiten - BILATERALSETTLEMENT - 2
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		    insertSqlBuff.append("WHERE OI.NBR_PROCESSID = OA.NBR_PROCESSID AND OI.NBR_INSTRTXNID = OA.NBR_INSTRTXNID ");
		    
		    if("BLE".equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){		
			insertSqlBuff.append("AND (OI.COD_SRCBNKLGLENT||OI.COD_TGTBNKLGLENT||OI.COD_CCYSRCACCT) ");
		    }
		    //ENH_Nostroloro_branch start
		    else if("Branch".equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){		
		    	insertSqlBuff.append("AND (OI.COD_BRSRCACCT||OI.COD_BRTGTACCT||OI.COD_CCYSRCACCT) ");
			}
		    //ENH_Nostroloro_branch end
		    else{
			insertSqlBuff.append("AND (OI.COD_GLSRCACCT||OI.COD_GLTGTACCT||OI.COD_CCYSRCACCT) ");
		    }
		    
		    insertSqlBuff.append("= (OAM.COD_ENTITY1 || OAM.COD_ENTITY2 || OAM.COD_CCY ) AND ");
		    insertSqlBuff.append("OAM.NBR_TEMPLATE_ID = OAD.NBR_TEMPLATE_ID AND OAD.FLG_DRCR_SET = OI.FLG_SRCDRCR ");
		    //insertSqlBuff.append("AND OA.TXT_STATUS = 'PENDING' AND OI.TXT_STATUS = 'SUCCESSFUL' AND OI.FLG_REVERSE = 'Y' AND OA.FLG_REVERSE = 'Y' AND OI.NBR_PROCESSID = '"+nbrProcessId+"' ");
		    insertSqlBuff.append("AND OA.TXT_STATUS = ? AND OI.TXT_STATUS = ? AND OI.FLG_REVERSE IN('Y','F') AND OA.FLG_REVERSE IN('Y','F') AND OI.NBR_PROCESSID = ? ");//JTest changes on 01-feb-11//Retro_Mashreq
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		    //Jiten - BILATERALSETTLEMENT - 2
		    //insertSqlBuff.append("AND OAD.FLG_DRCR = OA.FLG_DRCR AND OI.DAT_BUSINESS BETWEEN OAD.DAT_EFFECTIVE ");
		    insertSqlBuff.append("AND OAD.FLG_DRCR = OA.FLG_DRCR ");
		    //- DeLocalize Settlement path - Start
		    //insertSqlBuff.append("AND (CASE WHEN OAD.FLG_SETTLEMENTPATH = 'C' ");
		    insertSqlBuff.append("AND (CASE WHEN (OAD.FLG_SETTLEMENTPATH = 'C' OR OAD.FLG_SETTLEMENTPATH = 'D') ");
		    //- DeLocalize Settlement path - End
		    insertSqlBuff.append("AND OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' ");
		    insertSqlBuff.append("THEN OA.COD_TXN || 'CS' WHEN OAD.FLG_SETTLEMENTPATH = 'B' ");
		    insertSqlBuff.append("AND OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' ");
		    insertSqlBuff.append("THEN OA.COD_TXN || 'BS' ");
		    insertSqlBuff.append("ELSE OA.COD_TXN  END) = OTC.COD_TXN ");
		    insertSqlBuff.append("AND OI.DAT_BUSINESS BETWEEN OAD.DAT_EFFECTIVE ");
		    //Jiten - BILATERALSETTLEMENT - 2
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		    insertSqlBuff.append("AND NVL (OAD.DAT_END, TO_DATE ('31-DEC-9999', 'DD-MON-YYYY') ))");
		  //JTest changes on 01-feb-11 starts
		   * 
		   
		   */
		    //ENH_Nostroloro_branch start
		  //SUP_IS6597 starts 
//		    insertSqlBuff.append(" SELECT dtls.nbr_processid, dtls.nbr_acctentryid, dtls.nbr_legid, dtls.nbr_instrtxnid, dtls.nbr_lotid, "+
//		    		" dtls.nbr_ownaccount, dtls.nbr_coreaccount, dtls.flg_accttype, dtls.cod_branch, dtls.cod_bic, dtls.cod_gl, dtls.cod_ccy, " +
//		    		" CASE WHEN dtls.flg_drcr = 'C' THEN 'D' WHEN dtls.flg_drcr = 'D' THEN 'C' END flg_drcr, dtls.amt_txn, dtls.dat_business, " +
//		    		" 'PENDING', dtls.cod_bnklglent, dtls.txt_ibanacct, dtls.dat_business, CASE WHEN dtls.flg_srctgttyp = 'T' THEN 'S' " +
//		    		" WHEN dtls.flg_srctgttyp = 'S' THEN 'T' END flg_srctgttyp, 'N', dtls.dat_system, dtls.flg_forcedr, dtls.flg_reverse, " +
//		    		" dtls.flg_issettlement_entry, dtls.cod_triggering_gl, CASE WHEN SUBSTR (dtls.cod_txn, 3, 1) = 'C' THEN REGEXP_REPLACE " +
//		    		" (dtls.cod_txn, 'C', 'D', 1, 1) WHEN SUBSTR (dtls.cod_txn, 3, 1) = 'D' THEN REGEXP_REPLACE (dtls.cod_txn, 'D', 'C', 1, 1) END" +
//		    		"  cod_txn, DECODE (SUBSTR (txt_narration1, INSTR (txt_narration1, 'Credit'), 6), 'Credit', REPLACE (txt_narration1, 'Credit', " +
//		    		" 'Debit'), SUBSTR (txt_narration1, INSTR (txt_narration1, 'Debit'), 5), 'Debit', REPLACE (txt_narration1, 'Debit', 'Credit') )," +
//		    		"  dtls.cod_ref_id, dtls.flg_entrytype, dtls.nbr_settlement_days FROM (SELECT CASE WHEN oad.flg_issettlement_entry = 'Y' THEN '-2'" +
//		    		"  ELSE ? END nbr_processid, oa.nbr_acctentryid, oad.nbr_legid, oi.nbr_instrtxnid, oa.nbr_lotid, oad.nbr_ownaccount, " +
//		    		" oad.nbr_coreaccount, oad.flg_accttype, oad.cod_branch, oad.cod_bic, oad.cod_gl, oad.cod_ccy, oad.flg_drcr, oa.amt_txn, " +
//		    		" 'PENDING', oad.cod_bnklglent, oad.txt_ibanacct, oa.dat_business, oad.flg_srctgttyp, 'N', oa.dat_system, oa.flg_forcedr, " +
//		    		" oa.flg_reverse, oad.flg_issettlement_entry, oad.cod_triggering_gl, CASE WHEN (   oad.flg_settlementpath = 'C' OR " +
//		    		" oad.flg_settlementpath = 'D' ) AND oad.flg_issettlement_entry = 'Y' THEN oa.cod_txn || 'CS' WHEN oad.flg_settlementpath = 'B' " +
//		    		" AND oad.flg_issettlement_entry = 'Y' THEN oa.cod_txn || 'BS' ELSE oa.cod_txn END cod_txn, otc.txt_narration1, " +
//		    		" oa.nbr_acctentryid || '.' || oad.nbr_legid AS cod_ref_id, oa.flg_entrytype, oad.nbr_settlement_days FROM olm_instrtxnvw oi, " +
//		    		" olm_accountingentry oa, olm_aetemplate_mst oam, olm_aetemplate_dtls oad, opool_txn_codes otc WHERE oi.nbr_processid = " +
//		    		" oa.nbr_processid AND oi.nbr_instrtxnid = oa.nbr_instrtxnid ");
		    
		    //15.2_PERFORMANCE_CHANGE START
		    
            
		    
		    insertSqlBuff.append("SELECT dtls.nbr_processid, dtls.nbr_acctentryid, dtls.nbr_legid, dtls.nbr_instrtxnid, dtls.nbr_lotid, dtls.nbr_ownaccount, dtls.nbr_coreaccount, dtls.flg_accttype, dtls.cod_branch, dtls.cod_bic, dtls.cod_gl, dtls.cod_ccy, CASE WHEN dtls.flg_drcr = 'C' THEN 'D' WHEN dtls.flg_drcr = 'D' THEN 'C' END flg_drcr, dtls.amt_txn, dtls.dat_business, 'PENDING', dtls.cod_bnklglent, dtls.txt_ibanacct, dtls.dat_business, CASE WHEN dtls.flg_srctgttyp = 'T' THEN 'S' WHEN dtls.flg_srctgttyp = 'S' THEN 'T' END flg_srctgttyp, 'N', dtls.dat_system, dtls.flg_forcedr, dtls.flg_reverse, dtls.flg_issettlement_entry, dtls.cod_triggering_gl, SUBSTR(dtls.COD_TXN,1,LENGTH(dtls.COD_TXN)-1)||decode(substr(dtls.COD_TXN,-1,1),'C','D','D','C') cod_txn, DECODE (SUBSTR (txt_narration1, INSTR (txt_narration1, 'Credit'), 6), 'Credit', REPLACE (txt_narration1, 'Credit', 'Debit'), SUBSTR (txt_narration1, INSTR (txt_narration1, 'Debit'), 5), 'Debit', REPLACE (txt_narration1, 'Debit', 'Credit') ), dtls.cod_ref_id, dtls.flg_entrytype, dtls.nbr_settlement_days,'SERVICES' FROM (SELECT CASE WHEN oad.flg_issettlement_entry = 'Y' THEN '-2' ELSE ? END nbr_processid, oa.nbr_acctentryid, oad.nbr_legid, oi.nbr_instrtxnid, oa.nbr_lotid, oad.nbr_ownaccount, oad.nbr_coreaccount, oad.flg_accttype, oad.cod_branch, oad.cod_bic, oad.cod_gl, oad.cod_ccy, oad.flg_drcr, oa.amt_txn, 'PENDING', oad.cod_bnklglent, oad.txt_ibanacct, oa.dat_business, oad.flg_srctgttyp, 'N', oa.dat_system, oa.flg_forcedr, oa.flg_reverse, oad.flg_issettlement_entry, oad.cod_triggering_gl, CASE WHEN (   oad.flg_settlementpath = 'C' OR oad.flg_settlementpath = 'D' ) AND oad.flg_issettlement_entry = 'Y' THEN oa.cod_txn || 'CS' WHEN oad.flg_settlementpath = 'B' AND oad.flg_issettlement_entry = 'Y' THEN oa.cod_txn || 'BS' ELSE oa.cod_txn END cod_txn, otc.txt_narration1, oa.nbr_acctentryid || '.' || oad.nbr_legid AS cod_ref_id, oa.flg_entrytype, oad.nbr_settlement_days FROM olm_instrtxnvw oi, olm_accountingentry oa, olm_aetemplate_mst oam, olm_aetemplate_dtls oad, opool_txn_codes otc WHERE oi.nbr_processid = oa.nbr_processid AND oi.nbr_instrtxnid = oa.nbr_instrtxnid");
		     
		    //15.2_PERFORMANCE_CHANGE END
		    if("BLE".equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){		
		    	
		    	insertSqlBuff.append(" AND (OI.COD_SRCBNKLGLENT||OI.COD_TGTBNKLGLENT||OI.COD_CCYSRCACCT) ");
		    	
		    }else if(ExecutionConstants.NOSTRO_LORO_BRANCH.equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){		
		    	
		    	insertSqlBuff.append(" AND (OI.COD_BRSRCACCT||OI.COD_BRTGTACCT||OI.COD_CCYSRCACCT) ");
		    	
		    	
			}else{				
		    	insertSqlBuff.append(" AND (OI.COD_GLSRCACCT||OI.COD_GLTGTACCT||OI.COD_CCYSRCACCT) ");
		    	
		    }
			  //SUP_IS6597 Ends
		    insertSqlBuff.append(" = (oam.cod_entity1 || oam.cod_entity2 || oam.cod_ccy ) AND oam.nbr_template_id = oad.nbr_template_id AND "+
		    		" oad.flg_drcr_set = oi.flg_srcdrcr AND oa.txt_status = ? AND oi.txt_status = ? AND oi.flg_reverse IN ('Y', 'F') AND" +
		    		"  oa.flg_reverse IN ('Y', 'F') AND oi.nbr_processid = ? AND oad.flg_drcr = oa.flg_drcr AND (CASE WHEN " +
		    		" (   oad.flg_settlementpath = 'C' OR oad.flg_settlementpath = 'D' ) AND oad.flg_issettlement_entry = 'Y' THEN " +
		    		" oa.cod_txn || 'CS' WHEN oad.flg_settlementpath = 'B' AND oad.flg_issettlement_entry = 'Y' THEN oa.cod_txn || 'BS' ELSE " +
		    		" oa.cod_txn END ) = otc.cod_txn AND oi.dat_business BETWEEN oad.dat_effective AND NVL (oad.dat_end, " +
		    		" TO_DATE ('31-DEC-9999', 'DD-MON-YYYY' ) )) dtls ");
		  //ENH_Nostroloro_branch end
		    if(logger.isDebugEnabled())
		    	logger.debug("Accounting Entry SQL For BOD Reversal :: "+insertSqlBuff.toString());
	
		    //stmt = connection.createStatement();
		    stmt = connection.prepareStatement(insertSqlBuff.toString());
		    stmt.setString(1,String.valueOf(nbrProcessId));
		    stmt.setString(2, ExecutionConstants.PENDING);
		    stmt.setString(3, ExecutionConstants.SUCCESSFUL);
		    stmt.setLong(4, nbrProcessId);
		    int insertCnt = stmt.executeUpdate();
		    //JTest changes on 01-feb-11 ends
		    if(logger.isDebugEnabled())
		    	logger.debug("Accounting Entry For BOD Reversal Insert Count :: "+insertCnt);
		    
		}catch(Exception e){
		    logger.fatal("Exception generateAEFromTemplate :: "+e);
		    throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, "", e);
		}
		finally{
			DBUtils.closeResource(stmt);
		}
		if(logger.isDebugEnabled())//JTest changes on 01-feb-11
			logger.debug("Leaving generateAEFromTemplateForBODReversal........");
    }
    
    /**
     * 
     * @param connection
     * @param nbrProcessId
     * @throws LMSException
     */
    public void generateAEFromTemplate(Connection connection,long nbrProcessId,long nbrLotId, boolean isBODReversal)//SUPIS_123
    throws LMSException{
    	//JTest changes on 01-feb-11 starts
    	if(logger.isDebugEnabled())
    		logger.debug("Inside generateAEFromTemplate.........");
		PreparedStatement stmt = null;
		//JTest changes on 01-feb-11 ends
		StringBuilder sbuf=new StringBuilder();
		try{
		    /*String sql = "INSERT INTO OLM_ACCOUNTINGENTRY ( " +
		    "NBR_PROCESSID,NBR_ACCTENTRYID,NBR_LEGID,NBR_INSTRTXNID, " +
		    "NBR_LOTID,NBR_OWNACCOUNT,NBR_COREACCOUNT,FLG_ACCTTYPE, " +
		    "COD_BRANCH,COD_BIC,COD_GL,COD_CCY,FLG_DRCR,AMT_TXN, " +
		    "DAT_VALUE,TXT_STATUS,COD_BNKLGLENT,TXT_IBANACCT," +
		    "DAT_BUSINESS,FLG_SRCTGTTYP,FLG_MIRRORENTRY,DAT_SYSTEM,FLG_FORCEDR,FLG_REVERSE," +
		    "FLG_ISSETTLEMENT_ENTRY,COD_TRIGGERING_GL,COD_TXN,TXT_NARRATION1,COD_REF_ID,FLG_ENTRYTYPE,NBR_SETTLEMENT_DAYS) " +
		    "(SELECT CASE WHEN OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' THEN '-2' ELSE '"+nbrProcessId+"' END,OA.NBR_ACCTENTRYID, OAD.NBR_LEGID, " +
		    "OI.NBR_INSTRTXNID,OA.NBR_LOTID,OAD.NBR_OWNACCOUNT, " +
		    "OAD.NBR_COREACCOUNT,OAD.FLG_ACCTTYPE,OAD.COD_BRANCH," +
		    "OAD.COD_BIC,OAD.COD_GL,OAD.COD_CCY,OAD.FLG_DRCR," +
		    "OA.AMT_TXN," +
		    "OI.DAT_BUSINESS,'PENDING',OAD.COD_BNKLGLENT," +
		    "OAD.TXT_IBANACCT,OI.DAT_BUSINESS,OAD.FLG_SRCTGTTYP,'N',OA.DAT_SYSTEM,OA.FLG_FORCEDR,OA.FLG_REVERSE,OAD.FLG_ISSETTLEMENT_ENTRY,OAD.COD_TRIGGERING_GL,OA.COD_TXN,OA.TXT_NARRATION1," +
		    "OA.NBR_ACCTENTRYID||'.'||OAD.NBR_LEGID,OA.FLG_ENTRYTYPE,OAD.NBR_SETTLEMENT_DAYS FROM " +
		    "OLM_INSTRTXN OI,OLM_ACCOUNTINGENTRY OA,OLM_AETEMPLATE_MST OAM,OLM_AETEMPLATE_DTLS OAD " +
		    "WHERE OI.NBR_PROCESSID = OA.NBR_PROCESSID " +
		    "AND OI.NBR_INSTRTXNID = OA.NBR_INSTRTXNID " +
		   // else if("BLE".equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
		    //if(ExecutionConstants.BLE.equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)))
		    "AND (OI.COD_GLSRCACCT||OI.COD_GLTGTACCT||OI.COD_CCYSRCACCT) " +
		   // else
		    "AND (OI.COD_SRCBNKLGLENT||OI.COD_TGTBNKLGLENT||OI.COD_CCYSRCACCT) " +		
	
		    "= (OAM.COD_ENTITY1||OAM.COD_ENTITY2||OAM.COD_CCY) " +
		    "AND OAM.NBR_TEMPLATE_ID = OAD.NBR_TEMPLATE_ID " +
		    "AND OAD.FLG_DRCR_SET = OI.FLG_SRCDRCR " +
		    "AND OA.TXT_STATUS = 'PENDING' " +
		    "AND OI.NBR_PROCESSID = '"+nbrProcessId+"' AND OAD.FLG_DRCR = OA.FLG_DRCR )";*/
		    
		    /*sbuf.setLength(0);
		    sbuf.append("INSERT INTO OLM_ACCOUNTINGENTRY ( NBR_PROCESSID,NBR_ACCTENTRYID,NBR_LEGID,NBR_INSTRTXNID, ");
		    sbuf.append("NBR_LOTID,NBR_OWNACCOUNT,NBR_COREACCOUNT,FLG_ACCTTYPE, COD_BRANCH,COD_BIC,COD_GL,COD_CCY,FLG_DRCR,AMT_TXN, ");
		    sbuf.append("DAT_VALUE,TXT_STATUS,COD_BNKLGLENT,TXT_IBANACCT, DAT_BUSINESS,FLG_SRCTGTTYP,FLG_MIRRORENTRY,DAT_SYSTEM,FLG_FORCEDR,FLG_REVERSE,  ");
		    sbuf.append("FLG_ISSETTLEMENT_ENTRY,COD_TRIGGERING_GL,COD_TXN,TXT_NARRATION1,COD_REF_ID,FLG_ENTRYTYPE,NBR_SETTLEMENT_DAYS) ");
		    sbuf.append("(SELECT CASE WHEN OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' THEN '-2' ELSE '"+nbrProcessId+"' END,OA.NBR_ACCTENTRYID, OAD.NBR_LEGID, ");
		    sbuf.append("OI.NBR_INSTRTXNID,OA.NBR_LOTID,OAD.NBR_OWNACCOUNT,OAD.NBR_COREACCOUNT,OAD.FLG_ACCTTYPE,OAD.COD_BRANCH, ");
		    sbuf.append("OAD.COD_BIC,OAD.COD_GL,OAD.COD_CCY,OAD.FLG_DRCR,OA.AMT_TXN, OI.DAT_BUSINESS,'PENDING',OAD.COD_BNKLGLENT,  ");
		    sbuf.append("OAD.TXT_IBANACCT,OI.DAT_BUSINESS,OAD.FLG_SRCTGTTYP,'N',OA.DAT_SYSTEM,OA.FLG_FORCEDR,OA.FLG_REVERSE,OAD.FLG_ISSETTLEMENT_ENTRY,OAD.COD_TRIGGERING_GL,OA.COD_TXN,OA.TXT_NARRATION1, ");
		    sbuf.append("OA.NBR_ACCTENTRYID||'.'||OAD.NBR_LEGID,OA.FLG_ENTRYTYPE,OAD.NBR_SETTLEMENT_DAYS FROM ");
		    sbuf.append("OLM_INSTRTXN OI,OLM_ACCOUNTINGENTRY OA,OLM_AETEMPLATE_MST OAM,OLM_AETEMPLATE_DTLS OAD ");
		    sbuf.append("WHERE OI.NBR_PROCESSID = OA.NBR_PROCESSID AND OI.NBR_INSTRTXNID = OA.NBR_INSTRTXNID  ");
		    if("BLE".equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
			
			sbuf.append("AND (OI.COD_SRCBNKLGLENT||OI.COD_TGTBNKLGLENT||OI.COD_CCYSRCACCT) ");
		    }
		    else{
			sbuf.append("AND (OI.COD_GLSRCACCT||OI.COD_GLTGTACCT||OI.COD_CCYSRCACCT) ");
		    }
		    sbuf.append(" = (OAM.COD_ENTITY1||OAM.COD_ENTITY2||OAM.COD_CCY) AND OAM.NBR_TEMPLATE_ID = OAD.NBR_TEMPLATE_ID  ");
		    sbuf.append("AND OAD.FLG_DRCR_SET = OI.FLG_SRCDRCR AND OA.TXT_STATUS = 'PENDING' ");
		    sbuf.append("AND OI.NBR_PROCESSID = '"+nbrProcessId+"' AND OAD.FLG_DRCR = OA.FLG_DRCR ");
		    sbuf.append("AND OI.TXT_STATUS = 'PENDING' AND OI.NBR_LOTID = OA.NBR_LOTID AND OI.NBR_LOTID = '"+nbrLotId+"' ");
		    sbuf.append("AND OI.DAT_BUSINESS BETWEEN OAD.DAT_EFFECTIVE AND NVL(OAD.DAT_END, TO_DATE('31-DEC-9999','DD-MON-YYYY')))");
		*/
		    /////////////////////////////
		    sbuf.setLength(0);
		    sbuf.append("INSERT INTO OLM_ACCOUNTINGENTRY (NBR_PROCESSID, NBR_ACCTENTRYID, NBR_LEGID,");
		    sbuf.append("NBR_INSTRTXNID, NBR_LOTID, NBR_OWNACCOUNT, NBR_COREACCOUNT,");
		    sbuf.append("FLG_ACCTTYPE, COD_BRANCH, COD_BIC, COD_GL, COD_CCY, FLG_DRCR,");
		    sbuf.append("AMT_TXN, DAT_VALUE, TXT_STATUS, COD_BNKLGLENT, TXT_IBANACCT,");
		    sbuf.append("DAT_BUSINESS, FLG_SRCTGTTYP, FLG_MIRRORENTRY, DAT_SYSTEM,");
		    sbuf.append("FLG_FORCEDR, FLG_REVERSE, FLG_ISSETTLEMENT_ENTRY, COD_TRIGGERING_GL,");
		    //15.2_PERFORMANCE_CHANGE  start
		    sbuf.append("COD_TXN, TXT_NARRATION1, COD_REF_ID, FLG_ENTRYTYPE, NBR_SETTLEMENT_DAYS,COD_ROUTING) ");
		    //15.2_PERFORMANCE_CHANGE  end
		    //sbuf.append("(SELECT CASE WHEN OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' THEN '-2' ELSE '"+nbrProcessId+"' END,");
		    //SUP_ANZ_5103 starts
		   // sbuf.append("(SELECT CASE WHEN OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' THEN '-2' ELSE ? END,");//JTest changes on 01-feb-11
		    sbuf.append("(SELECT DISTINCT CASE WHEN OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' THEN '-2' ELSE ? END,");//JTest changes on 01-feb-11
		    //SUP_ANZ_5103 ends
		    sbuf.append("OIA.NBR_ACCTENTRYID, OAD.NBR_LEGID, OIA.NBR_INSTRTXNID, OIA.NBR_LOTID,");
		    sbuf.append("OAD.NBR_OWNACCOUNT, OAD.NBR_COREACCOUNT, OAD.FLG_ACCTTYPE, OAD.COD_BRANCH,");
		    
		    //sbuf.append("OAD.COD_BIC, OAD.COD_GL, OAD.COD_CCY, OAD.FLG_DRCR, OIA.AMT_TXN, OIA.DAT_BUSINESS,");
		    sbuf.append("OAD.COD_BIC, OAD.COD_GL, OAD.COD_CCY, OAD.FLG_DRCR, OIA.AMT_TXN, OIA.DAT_VALUE,");
		    sbuf.append("'PENDING', OAD.COD_BNKLGLENT, OAD.TXT_IBANACCT, OIA.DAT_BUSINESS,");
		    sbuf.append("OAD.FLG_SRCTGTTYP, 'N', OIA.DAT_SYSTEM, OIA.FLG_FORCEDR,");
		    sbuf.append("OIA.FLG_REVERSE, OAD.FLG_ISSETTLEMENT_ENTRY, OAD.COD_TRIGGERING_GL,");
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		    //Jiten - BILATERALSETTLEMENT - Start
		    //sbuf.append("OIA.COD_TXN, OIA.TXT_NARRATION1, OIA.NBR_ACCTENTRYID || '.' || OAD.NBR_LEGID,");
		    //- DeLocalize Settlement path - Start
		    //sbuf.append("CASE WHEN OAD.FLG_SETTLEMENTPATH = 'C' AND OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' ");
		    sbuf.append("CASE WHEN (OAD.FLG_SETTLEMENTPATH = 'C' OR OAD.FLG_SETTLEMENTPATH = 'D') AND OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' ");
		    //- DeLocalize Settlement path - End
		    sbuf.append("THEN OIA.COD_TXN || 'CS' ");
		    sbuf.append("WHEN OAD.FLG_SETTLEMENTPATH = 'B' AND OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' ");
		    sbuf.append("THEN OIA.COD_TXN || 'BS' ELSE OIA.COD_TXN END, ");
		    //Jiten - BILATERALSETTLEMENT - 2
		    /*sbuf.append("CASE WHEN OAD.FLG_SETTLEMENTPATH = 'C' AND OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' ");
		    sbuf.append("THEN OIA.TXT_NARRATION1 || ' Central Settlement' ");
		    sbuf.append("WHEN OAD.FLG_SETTLEMENTPATH = 'B' AND OAD.FLG_ISSETTLEMENT_ENTRY = 'Y'  ");
		    sbuf.append("THEN OIA.TXT_NARRATION1 || ' BI-Lateral Settlement' ELSE OIA.TXT_NARRATION1 END,");*/
		    sbuf.append("OTC.TXT_NARRATION1,");
		    //Jiten - BILATERALSETTLEMENT - 2
		    sbuf.append("OIA.NBR_ACCTENTRYID || '.' || OAD.NBR_LEGID,");
		    //Jiten - BILATERALSETTLEMENT - End
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		    //15.2_PERFORMANCE_CHANGE start
		    sbuf.append("OIA.FLG_ENTRYTYPE, OAD.NBR_SETTLEMENT_DAYS ,'SERVICES' FROM ( ");
		    //15.2_PERFORMANCE_CHANGE end
		    sbuf.append("SELECT OA.NBR_ACCTENTRYID, OI.NBR_INSTRTXNID, OA.NBR_LOTID, OA.AMT_TXN,");
		    //sbuf.append("OI.DAT_BUSINESS, OA.FLG_DRCR, OA.DAT_SYSTEM, OA.FLG_FORCEDR, OA.FLG_REVERSE,");
		    //SUPIS_157 starts
		    	sbuf.append("OA.DAT_VALUE,OA.DAT_BUSINESS, OA.FLG_DRCR, OA.DAT_SYSTEM, OA.FLG_FORCEDR, OA.FLG_REVERSE,");
		    //SUPIS_157 ends
		    sbuf.append("OA.COD_TXN, OA.TXT_NARRATION1, OA.FLG_ENTRYTYPE, OI.COD_GLSRCACCT,");
		    //sbuf.append("OI.COD_GLTGTACCT, OI.COD_CCYSRCACCT, OI.COD_SRCBNKLGLENT,OI.COD_TGTBNKLGLENT,CASE WHEN OI.NBR_SRCACCT = OA.NBR_OWNACCOUNT ");//Sweeps_ENH Issue id 2864
		    //sbuf.append("OI.COD_GLTGTACCT, OI.COD_CCYSRCACCT, OI.COD_SRCBNKLGLENT,OI.COD_TGTBNKLGLENT,CASE WHEN OI.FLG_SRC_ACC_TYPE IN ('GI','GE')AND OI.NBR_SRCACCT = OA.DRAIN_GRP_ID  ");
		    //ENH_Nostroloro_branch start
		    sbuf.append("OI.COD_GLTGTACCT, OI.COD_CCYSRCACCT, OI.COD_SRCBNKLGLENT,OI.COD_TGTBNKLGLENT,oi.COD_BRSRCACCT, oi.COD_BRTGTACCT,CASE WHEN OI.FLG_SRC_ACC_TYPE IN ('GI','GE')AND OI.NBR_SRCACCT = OA.DRAIN_GRP_ID  ");
		    //ENH_Nostroloro_branch end
		    //sbuf.append("THEN OA.FLG_DRCR ELSE (CASE WHEN OA.FLG_DRCR = 'D' THEN 'C' ELSE 'D' END) END  ");//Sweeps_ENH Issue id 2864
		    sbuf.append(" THEN OA.FLG_DRCR  WHEN OI.FLG_SRC_ACC_TYPE ='P' AND OI.NBR_SRCACCT = OA.ID_POOL THEN OA.FLG_DRCR  WHEN OI.NBR_SRCACCT = OA.NBR_OWNACCOUNT THEN OA.FLG_DRCR ELSE (CASE WHEN OA.FLG_DRCR = 'D' THEN 'C' ELSE 'D' END) END  ");
			//SUPIS_123 STARTS
		    if(!isBODReversal){
		    	sbuf.append("FLG_SRCDRCR FROM OLM_INSTRTXN OI, OLM_ACCOUNTINGENTRY OA ");
		    }else{
		    	sbuf.append("FLG_SRCDRCR FROM olm_instrtxnvw OI, OLM_ACCOUNTINGENTRY OA ");
		    }
		    
		    
		    sbuf.append("WHERE OI.NBR_PROCESSID = OA.NBR_PROCESSID ");
		    sbuf.append("AND OI.NBR_INSTRTXNID = OA.NBR_INSTRTXNID ");
		    if(!isBODReversal){
			    sbuf.append("AND OI.NBR_LOTID = OA.NBR_LOTID ");
			    }

		    //JTest changes on 01-feb-11 starts
		   /* sbuf.append("AND OA.TXT_STATUS = 'PENDING' AND OI.NBR_PROCESSID = '"+nbrProcessId+"' ");
		    sbuf.append("AND OI.TXT_STATUS = 'PENDING' AND OI.NBR_LOTID = '"+nbrLotId+"' ) OIA, ");*/
		    sbuf.append("AND OA.TXT_STATUS = ? AND OI.NBR_PROCESSID = ? ");
		    sbuf.append("AND OI.TXT_STATUS = ? ");
		    if(!isBODReversal){
		    	sbuf.append("AND OI.NBR_LOTID = ? ");
		    }
		    //LMS_45_PERF_CERTIFICATION_PF78 Start
		    if("BLE".equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
				
				sbuf.append(" AND OI.COD_SRCBNKLGLENT!=OI.COD_TGTBNKLGLENT ");
    	    }
    	    else if(ExecutionConstants.NOSTRO_LORO_BRANCH.equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE))
    	    {
    	    	sbuf.append(" AND OI.COD_BRSRCACCT!=OI.COD_BRTGTACCT ");
			}
			else
			{
				sbuf.append(" AND OI.COD_GLSRCACCT!=OI.COD_GLTGTACCT ");
		    }
		    //LMS_45_PERF_CERTIFICATION_PF78 End

			//SUPIS_123 ENDS
		  //ENH_Nostroloro_branch start
		    if(ExecutionConstants.NOSTRO_LORO_BRANCH.equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
		    	sbuf.append(" AND (   oi.flg_groupsweeprule IS NULL  OR oi.flg_groupsweeprule NOT IN ('CM', 'DM') ) " );
		    }
		  //ENH_Nostroloro_branch end
		    sbuf.append(" ) OIA, ");
		    //JTest changes on 01-feb-11 ends
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		    //Jiten - BILATERALSETTLEMENT - 2
		    //sbuf.append("OLM_AETEMPLATE_MST OAM, OLM_AETEMPLATE_DTLS OAD WHERE ");
		    sbuf.append("OLM_AETEMPLATE_MST OAM, OLM_AETEMPLATE_DTLS OAD,OPOOL_TXN_CODES OTC WHERE");
		    //Jiten - BILATERALSETTLEMENT - 2
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		    /* PERFORMANCE FIXES Start*/
		    if("BLE".equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
			
			sbuf.append(" OIA.COD_SRCBNKLGLENT=OAM.COD_ENTITY1 AND OIA.COD_TGTBNKLGLENT=OAM.COD_ENTITY2 AND OIA.COD_CCYSRCACCT =OAM.COD_CCY ");
		    }
		    //ENH_Nostroloro_branch start
		    else if(ExecutionConstants.NOSTRO_LORO_BRANCH.equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
				
				sbuf.append(" OIA.COD_BRSRCACCT=OAM.COD_ENTITY1 AND OIA.COD_BRTGTACCT=OAM.COD_ENTITY2 AND OIA.COD_CCYSRCACCT =OAM.COD_CCY ");

			}
		    //ENH_Nostroloro_branch end
		    else{
			sbuf.append(" OIA.COD_GLSRCACCT=OAM.COD_ENTITY1 AND OIA.COD_GLTGTACCT=OAM.COD_ENTITY2 AND OIA.COD_CCYSRCACCT =OAM.COD_CCY ");

		    }
		    
		   // sbuf.append("= (OAM.COD_ENTITY1 || OAM.COD_ENTITY2 || OAM.COD_CCY ) ");
		    /* PERFORMANCE FIXES End*/
		    sbuf.append("AND OAM.NBR_TEMPLATE_ID = OAD.NBR_TEMPLATE_ID ");
		    sbuf.append("AND OAD.FLG_DRCR_SET = OIA.FLG_SRCDRCR AND OAD.FLG_DRCR = OIA.FLG_DRCR ");
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		    //Jiten - BILATERALSETTLEMENT - 2
		    //- DeLocalize Settlement path - Start
		    //sbuf.append("AND (CASE WHEN OAD.FLG_SETTLEMENTPATH = 'C' ");
		    sbuf.append("AND ( CASE WHEN (OAD.FLG_SETTLEMENTPATH = 'C' OR OAD.FLG_SETTLEMENTPATH = 'D') ");
		    //- DeLocalize Settlement path - End
		    sbuf.append("AND OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' ");
		    sbuf.append("THEN OIA.COD_TXN || 'CS' WHEN OAD.FLG_SETTLEMENTPATH = 'B' ");
		    sbuf.append("AND OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' ");
		    sbuf.append("THEN OIA.COD_TXN || 'BS' ELSE OIA.COD_TXN ");
		    sbuf.append("END) = OTC.COD_TXN ");
		    //Jiten - BILATERALSETTLEMENT - 2
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		    sbuf.append("AND OIA.DAT_BUSINESS BETWEEN OAD.DAT_EFFECTIVE AND NVL(OAD.DAT_END, TO_DATE('31-DEC-9999','DD-MON-YYYY'))");
		  //ENH_Nostroloro_branch start
		    if(ExecutionConstants.NOSTRO_LORO_BRANCH.equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
		    	//SUP_IS6597 STARTS
		    	//sbuf.append(" UNION SELECT DISTINCT CASE WHEN oad.flg_issettlement_entry = 'Y' THEN '-2' ELSE ? END, oia.nbr_acctentryid, oad.nbr_legid, oia.nbr_instrtxnid, oia.nbr_lotid, oad.nbr_ownaccount, oad.nbr_coreaccount, oad.flg_accttype, oad.cod_branch, oad.cod_bic, oad.cod_gl, oad.cod_ccy, oad.flg_drcr, oia.amt_txn, oia.dat_value, 'PENDING', oad.cod_bnklglent, oad.txt_ibanacct, oia.dat_business, oad.flg_srctgttyp, 'N', oia.dat_system, oia.flg_forcedr, oia.flg_reverse, oad.flg_issettlement_entry, oad.cod_triggering_gl, CASE WHEN (   oad.flg_settlementpath = 'C' OR oad.flg_settlementpath = 'D' ) AND oad.flg_issettlement_entry = 'Y' THEN oia.cod_txn || 'CS' WHEN oad.flg_settlementpath = 'B' AND oad.flg_issettlement_entry = 'Y' THEN oia.cod_txn || 'BS' ELSE oia.cod_txn END, otc.txt_narration1, oia.nbr_acctentryid || '.' || oad.nbr_legid, oia.flg_entrytype, oad.nbr_settlement_days FROM (SELECT oa.nbr_acctentryid, oi.nbr_instrtxnid, oa.nbr_lotid, oa.amt_txn, oa.dat_value, oi.dat_business, oa.flg_drcr, oa.dat_system, oa.flg_forcedr, oa.flg_reverse, oa.cod_txn, oa.txt_narration1, oa.flg_entrytype, oi.cod_glsrcacct, oi.cod_gltgtacct, oi.cod_ccysrcacct, oi.cod_srcbnklglent, oi.cod_tgtbnklglent, oi.cod_brsrcacct, oi.cod_brtgtacct, CASE WHEN oi.flg_src_acc_type IN ('GI', 'GE') AND oi.nbr_srcacct = oa.drain_grp_id THEN oa.flg_drcr WHEN oi.flg_src_acc_type = 'P' AND oi.nbr_srcacct = oa.id_pool THEN oa.flg_drcr WHEN oi.nbr_srcacct = oa.nbr_ownaccount THEN oa.flg_drcr ELSE (CASE WHEN oa.flg_drcr = 'D' THEN 'C' ELSE 'D' END) END flg_srcdrcr FROM olm_instrtxn oi, olm_accountingentry oa WHERE oi.nbr_processid = oa.nbr_processid AND oi.nbr_instrtxnid = oa.nbr_instrtxnid AND oi.nbr_lotid = oa.nbr_lotid AND oa.txt_status = ? AND oi.nbr_processid = ? AND oi.txt_status = ? AND oi.nbr_lotid = ? AND oi.nbr_tgtacct != oa.nbr_ownaccount AND oi.cod_brsrcacct != ? AND oi.flg_groupsweeprule IN ('CM', 'DM') UNION SELECT oa.nbr_acctentryid, oi.nbr_instrtxnid, oa.nbr_lotid, oa.amt_txn, oa.dat_value, oi.dat_business, CASE WHEN oa.flg_drcr = 'C' THEN 'D' WHEN oa.flg_drcr = 'D' THEN 'C' END flg_drcr, oa.dat_system, oa.flg_forcedr, oa.flg_reverse, CASE WHEN SUBSTR (oa.cod_txn, 3, 1) = 'C' THEN REGEXP_REPLACE (oa.cod_txn, 'C', 'D', 1, 1 ) WHEN SUBSTR (oa.cod_txn, 3, 1) = 'D' THEN REGEXP_REPLACE (oa.cod_txn, 'D', 'C', 1, 1) END cod_txn, DECODE (SUBSTR (oa.txt_narration1, INSTR (oa.txt_narration1, 'Credit'), 6 ), 'Credit', REPLACE (oa.txt_narration1, 'Credit', 'Debit' ), SUBSTR (oa.txt_narration1, INSTR (oa.txt_narration1, 'Debit'), 5 ), 'Debit', REPLACE (oa.txt_narration1, 'Debit', 'Credit') ), oa.flg_entrytype, oi.cod_glsrcacct, oi.cod_gltgtacct, oi.cod_ccysrcacct, oi.cod_srcbnklglent, oi.cod_tgtbnklglent, oi.cod_brsrcacct, oi.cod_brtgtacct, CASE WHEN oi.flg_src_acc_type IN ('GI', 'GE') AND oi.nbr_srcacct = oa.drain_grp_id THEN oa.flg_drcr WHEN oi.flg_src_acc_type = 'P' AND oi.nbr_srcacct = oa.id_pool THEN oa.flg_drcr WHEN oi.nbr_srcacct = oa.nbr_ownaccount THEN oa.flg_drcr ELSE (CASE WHEN oa.flg_drcr = 'D' THEN 'C' ELSE 'D' END) END flg_srcdrcr FROM olm_instrtxn oi, olm_accountingentry oa WHERE oi.nbr_processid = oa.nbr_processid AND oi.nbr_instrtxnid = oa.nbr_instrtxnid AND oi.nbr_lotid = oa.nbr_lotid AND oa.txt_status = ? AND oi.nbr_processid = ? AND oi.txt_status = ? AND oi.nbr_lotid = ? AND oi.nbr_tgtacct != oa.nbr_ownaccount AND oi.cod_brsrcacct != ? AND oi.flg_groupsweeprule IN ('CM', 'DM') and oi.NBR_BANKINTERNALACCOUNT is null ) oia, olm_aetemplate_mst oam, olm_aetemplate_dtls oad, opool_txn_codes otc WHERE (   (oia.cod_brsrcacct || oam.cod_entity2 || oia.cod_ccysrcacct ) = (oam.cod_entity1 || oam.cod_entity2 || oam.cod_ccy) OR (oam.cod_entity1 || oia.cod_brsrcacct || oia.cod_ccysrcacct ) = (oam.cod_entity1 || oam.cod_entity2 || oam.cod_ccy) ) AND oam.nbr_template_id = oad.nbr_template_id AND oad.flg_drcr_set = oia.flg_srcdrcr AND oad.flg_drcr = oia.flg_drcr AND (CASE WHEN (   oad.flg_settlementpath = 'C' OR oad.flg_settlementpath = 'D' ) AND oad.flg_issettlement_entry = 'Y' THEN oia.cod_txn || 'CS' WHEN oad.flg_settlementpath = 'B' AND oad.flg_issettlement_entry = 'Y' THEN oia.cod_txn || 'BS' ELSE oia.cod_txn END ) = otc.cod_txn AND oia.dat_business BETWEEN oad.dat_effective AND NVL (oad.dat_end, TO_DATE ('31-DEC-9999', 'DD-MON-YYYY' ) ) UNION SELECT DISTINCT CASE WHEN oad.flg_issettlement_entry = 'Y' THEN '-2' ELSE ? END, oia.nbr_acctentryid, oad.nbr_legid, oia.nbr_instrtxnid, oia.nbr_lotid, oad.nbr_ownaccount, oad.nbr_coreaccount, oad.flg_accttype, oad.cod_branch, oad.cod_bic, oad.cod_gl, oad.cod_ccy, CASE WHEN oad.flg_drcr = 'C' THEN 'D' WHEN oad.flg_drcr = 'D' THEN 'C' END, oia.amt_txn, oia.dat_value, 'PENDING', oad.cod_bnklglent, oad.txt_ibanacct, oia.dat_business, oad.flg_srctgttyp, 'N', oia.dat_system, oia.flg_forcedr, oia.flg_reverse, oad.flg_issettlement_entry, oad.cod_triggering_gl, CASE WHEN (   oad.flg_settlementpath = 'C' OR oad.flg_settlementpath = 'D' ) AND oad.flg_issettlement_entry = 'Y' THEN oia.cod_txn || 'CS' WHEN oad.flg_settlementpath = 'B' AND oad.flg_issettlement_entry = 'Y' THEN oia.cod_txn || 'BS' ELSE oia.cod_txn END, otc.txt_narration1, oia.nbr_acctentryid || '.' || oad.nbr_legid, oia.flg_entrytype, oad.nbr_settlement_days FROM (SELECT oa.nbr_acctentryid, oi.nbr_instrtxnid, oa.nbr_lotid, oa.amt_txn, oa.dat_value, oi.dat_business, oa.flg_drcr, oa.dat_system, oa.flg_forcedr, oa.flg_reverse, oa.cod_txn, oa.txt_narration1, oa.flg_entrytype, oi.cod_glsrcacct, oi.cod_gltgtacct, oi.cod_ccysrcacct, oi.cod_srcbnklglent, oi.cod_tgtbnklglent, oi.cod_brsrcacct, oi.cod_brtgtacct, CASE WHEN oi.flg_src_acc_type IN ('GI', 'GE') AND oi.nbr_srcacct = oa.drain_grp_id THEN oa.flg_drcr WHEN oi.flg_src_acc_type = 'P' AND oi.nbr_srcacct = oa.id_pool THEN oa.flg_drcr WHEN oi.nbr_srcacct = oa.nbr_ownaccount THEN oa.flg_drcr ELSE (CASE WHEN oa.flg_drcr = 'D' THEN 'C' ELSE 'D' END) END flg_srcdrcr FROM olm_instrtxn oi, olm_accountingentry oa WHERE oi.nbr_processid = oa.nbr_processid AND oi.nbr_instrtxnid = oa.nbr_instrtxnid AND oi.nbr_lotid = oa.nbr_lotid AND oa.txt_status = ? AND oi.nbr_processid = ? AND oi.txt_status = ? AND oi.nbr_lotid = ? AND oi.nbr_tgtacct = oa.nbr_ownaccount AND oi.cod_brtgtacct != ? AND oi.flg_groupsweeprule IN ('CM', 'DM') UNION SELECT oa.nbr_acctentryid, oi.nbr_instrtxnid, oa.nbr_lotid, oa.amt_txn, oa.dat_value, oi.dat_business, CASE WHEN oa.flg_drcr = 'C' THEN 'D' WHEN oa.flg_drcr = 'D' THEN 'C' END flg_drcr, oa.dat_system, oa.flg_forcedr, oa.flg_reverse, CASE WHEN SUBSTR (oa.cod_txn, 3, 1) = 'C' THEN REGEXP_REPLACE (oa.cod_txn, 'C', 'D', 1, 1 ) WHEN SUBSTR (oa.cod_txn, 3, 1) = 'D' THEN REGEXP_REPLACE (oa.cod_txn, 'D', 'C', 1, 1) END cod_txn, DECODE (SUBSTR (oa.txt_narration1, INSTR (oa.txt_narration1, 'Credit'), 6 ), 'Credit', REPLACE (oa.txt_narration1, 'Credit', 'Debit' ), SUBSTR (oa.txt_narration1, INSTR (oa.txt_narration1, 'Debit'), 5 ), 'Debit', REPLACE (oa.txt_narration1, 'Debit', 'Credit') ), oa.flg_entrytype, oi.cod_glsrcacct, oi.cod_gltgtacct, oi.cod_ccysrcacct, oi.cod_srcbnklglent, oi.cod_tgtbnklglent, oi.cod_brsrcacct, oi.cod_brtgtacct, CASE WHEN oi.flg_src_acc_type IN ('GI', 'GE') AND oi.nbr_srcacct = oa.drain_grp_id THEN oa.flg_drcr WHEN oi.flg_src_acc_type = 'P' AND oi.nbr_srcacct = oa.id_pool THEN oa.flg_drcr WHEN oi.nbr_srcacct = oa.nbr_ownaccount THEN oa.flg_drcr ELSE (CASE WHEN oa.flg_drcr = 'D' THEN 'C' ELSE 'D' END) END flg_srcdrcr FROM olm_instrtxn oi, olm_accountingentry oa WHERE oi.nbr_processid = oa.nbr_processid AND oi.nbr_instrtxnid = oa.nbr_instrtxnid AND oi.nbr_lotid = oa.nbr_lotid AND oa.txt_status = ? AND oi.nbr_processid = ? AND oi.txt_status = ? AND oi.nbr_lotid = ? AND oi.nbr_tgtacct = oa.nbr_ownaccount AND oi.cod_brtgtacct != ? AND oi.flg_groupsweeprule IN ('CM', 'DM') and oi.NBR_BANKINTERNALACCOUNT is null) oia, olm_aetemplate_mst oam, olm_aetemplate_dtls oad, opool_txn_codes otc WHERE (   (oia.cod_brtgtacct || oam.cod_entity2 || oia.cod_ccysrcacct ) = (oam.cod_entity1 || oam.cod_entity2 || oam.cod_ccy) OR (oam.cod_entity1 || oia.cod_brtgtacct || oia.cod_ccysrcacct ) = (oam.cod_entity1 || oam.cod_entity2 || oam.cod_ccy) ) AND oam.nbr_template_id = oad.nbr_template_id AND oad.flg_drcr_set = oia.flg_srcdrcr AND oad.flg_drcr = oia.flg_drcr AND (CASE WHEN (   oad.flg_settlementpath = 'C' OR oad.flg_settlementpath = 'D' ) AND oad.flg_issettlement_entry = 'Y' THEN oia.cod_txn || 'CS' WHEN oad.flg_settlementpath = 'B' AND oad.flg_issettlement_entry = 'Y' THEN oia.cod_txn || 'BS' ELSE oia.cod_txn END ) = otc.cod_txn AND oia.dat_business BETWEEN oad.dat_effective AND NVL (oad.dat_end, TO_DATE ('31-DEC-9999', 'DD-MON-YYYY' ) )"); 
		    	//sbuf.append(" UNION SELECT DISTINCT CASE WHEN oad.flg_issettlement_entry = 'Y' THEN '-2' ELSE ? END, oia.nbr_acctentryid, oad.nbr_legid, oia.nbr_instrtxnid, oia.nbr_lotid, oad.nbr_ownaccount, oad.nbr_coreaccount, oad.flg_accttype, oad.cod_branch, oad.cod_bic, oad.cod_gl, oad.cod_ccy, oad.flg_drcr, oia.amt_txn, oia.dat_value, 'PENDING', oad.cod_bnklglent, oad.txt_ibanacct, oia.dat_business, oad.flg_srctgttyp, 'N', oia.dat_system, oia.flg_forcedr, oia.flg_reverse, oad.flg_issettlement_entry, oad.cod_triggering_gl, CASE WHEN (   oad.flg_settlementpath = 'C' OR oad.flg_settlementpath = 'D' ) AND oad.flg_issettlement_entry = 'Y' THEN oia.cod_txn || 'CS' WHEN oad.flg_settlementpath = 'B' AND oad.flg_issettlement_entry = 'Y' THEN oia.cod_txn || 'BS' ELSE oia.cod_txn END, otc.txt_narration1, oia.nbr_acctentryid || '.' || oad.nbr_legid, oia.flg_entrytype, oad.nbr_settlement_days FROM (SELECT oa.nbr_acctentryid, oi.nbr_instrtxnid, oa.nbr_lotid, oa.amt_txn, oa.dat_value, oi.dat_business, oa.flg_drcr, oa.dat_system, oa.flg_forcedr, oa.flg_reverse, oa.cod_txn, oa.txt_narration1, oa.flg_entrytype, oi.cod_glsrcacct, oi.cod_gltgtacct, oi.cod_ccysrcacct, oi.cod_srcbnklglent, oi.cod_tgtbnklglent, oi.cod_brsrcacct, oi.cod_brtgtacct, CASE WHEN oi.flg_src_acc_type IN ('GI', 'GE') AND oi.nbr_srcacct = oa.drain_grp_id THEN oa.flg_drcr WHEN oi.flg_src_acc_type = 'P' AND oi.nbr_srcacct = oa.id_pool THEN oa.flg_drcr WHEN oi.nbr_srcacct = oa.nbr_ownaccount THEN oa.flg_drcr ELSE (CASE WHEN oa.flg_drcr = 'D' THEN 'C' ELSE 'D' END) END flg_srcdrcr FROM olm_instrtxn oi, olm_accountingentry oa WHERE oi.nbr_processid = oa.nbr_processid AND oi.nbr_instrtxnid = oa.nbr_instrtxnid AND oi.nbr_lotid = oa.nbr_lotid AND oa.txt_status = ? AND oi.nbr_processid = ? AND oi.txt_status = ? AND oi.nbr_lotid = ? AND oi.nbr_tgtacct != oa.nbr_ownaccount AND oi.cod_brsrcacct != ? AND oi.flg_groupsweeprule IN ('CM', 'DM') UNION SELECT oa.nbr_acctentryid, oi.nbr_instrtxnid, oa.nbr_lotid, oa.amt_txn, oa.dat_value, oi.dat_business, CASE WHEN oa.flg_drcr = 'C' THEN 'D' WHEN oa.flg_drcr = 'D' THEN 'C' END flg_drcr, oa.dat_system, oa.flg_forcedr, oa.flg_reverse, CASE WHEN SUBSTR (oa.cod_txn, 3, 1) = 'C' THEN REGEXP_REPLACE (oa.cod_txn, 'C', 'D', 1, 1 ) WHEN SUBSTR (oa.cod_txn, 3, 1) = 'D' THEN REGEXP_REPLACE (oa.cod_txn, 'D', 'C', 1, 1) END cod_txn, DECODE (SUBSTR (oa.txt_narration1, INSTR (oa.txt_narration1, 'Credit'), 6 ), 'Credit', REPLACE (oa.txt_narration1, 'Credit', 'Debit' ), SUBSTR (oa.txt_narration1, INSTR (oa.txt_narration1, 'Debit'), 5 ), 'Debit', REPLACE (oa.txt_narration1, 'Debit', 'Credit') ), oa.flg_entrytype, oi.cod_glsrcacct, oi.cod_gltgtacct, oi.cod_ccysrcacct, oi.cod_srcbnklglent, oi.cod_tgtbnklglent, oi.cod_brsrcacct, oi.cod_brtgtacct, CASE WHEN oi.flg_src_acc_type IN ('GI', 'GE') AND oi.nbr_srcacct = oa.drain_grp_id THEN oa.flg_drcr WHEN oi.flg_src_acc_type = 'P' AND oi.nbr_srcacct = oa.id_pool THEN oa.flg_drcr WHEN oi.nbr_srcacct = oa.nbr_ownaccount THEN oa.flg_drcr ELSE (CASE WHEN oa.flg_drcr = 'D' THEN 'C' ELSE 'D' END) END flg_srcdrcr FROM olm_instrtxn oi, olm_accountingentry oa WHERE oi.nbr_processid = oa.nbr_processid AND oi.nbr_instrtxnid = oa.nbr_instrtxnid AND oi.nbr_lotid = oa.nbr_lotid AND oa.txt_status = ? AND oi.nbr_processid = ? AND oi.txt_status = ? AND oi.nbr_lotid = ? AND oi.nbr_tgtacct != oa.nbr_ownaccount AND oi.cod_brsrcacct != ? AND oi.flg_groupsweeprule IN ('CM', 'DM') and oi.NBR_BANKINTERNALACCOUNT is null ) oia, olm_aetemplate_mst oam, olm_aetemplate_dtls oad, opool_txn_codes otc WHERE (   (oia.cod_brsrcacct || ? || oia.cod_ccysrcacct ) = (oam.cod_entity1 || oam.cod_entity2 || oam.cod_ccy) OR ( ? || oia.cod_brsrcacct || oia.cod_ccysrcacct ) = (oam.cod_entity1 || oam.cod_entity2 || oam.cod_ccy) ) AND oam.nbr_template_id = oad.nbr_template_id AND oad.flg_drcr_set = oia.flg_srcdrcr AND oad.flg_drcr = oia.flg_drcr AND (CASE WHEN (   oad.flg_settlementpath = 'C' OR oad.flg_settlementpath = 'D' ) AND oad.flg_issettlement_entry = 'Y' THEN oia.cod_txn || 'CS' WHEN oad.flg_settlementpath = 'B' AND oad.flg_issettlement_entry = 'Y' THEN oia.cod_txn || 'BS' ELSE oia.cod_txn END ) = otc.cod_txn AND oia.dat_business BETWEEN oad.dat_effective AND NVL (oad.dat_end, TO_DATE ('31-DEC-9999', 'DD-MON-YYYY' ) ) UNION SELECT DISTINCT CASE WHEN oad.flg_issettlement_entry = 'Y' THEN '-2' ELSE ? END, oia.nbr_acctentryid, oad.nbr_legid, oia.nbr_instrtxnid, oia.nbr_lotid, oad.nbr_ownaccount, oad.nbr_coreaccount, oad.flg_accttype, oad.cod_branch, oad.cod_bic, oad.cod_gl, oad.cod_ccy, CASE WHEN oad.flg_drcr = 'C' THEN 'D' WHEN oad.flg_drcr = 'D' THEN 'C' END, oia.amt_txn, oia.dat_value, 'PENDING', oad.cod_bnklglent, oad.txt_ibanacct, oia.dat_business, oad.flg_srctgttyp, 'N', oia.dat_system, oia.flg_forcedr, oia.flg_reverse, oad.flg_issettlement_entry, oad.cod_triggering_gl, CASE WHEN (   oad.flg_settlementpath = 'C' OR oad.flg_settlementpath = 'D' ) AND oad.flg_issettlement_entry = 'Y' THEN oia.cod_txn || 'CS' WHEN oad.flg_settlementpath = 'B' AND oad.flg_issettlement_entry = 'Y' THEN oia.cod_txn || 'BS' ELSE oia.cod_txn END, otc.txt_narration1, oia.nbr_acctentryid || '.' || oad.nbr_legid, oia.flg_entrytype, oad.nbr_settlement_days FROM (SELECT oa.nbr_acctentryid, oi.nbr_instrtxnid, oa.nbr_lotid, oa.amt_txn, oa.dat_value, oi.dat_business, oa.flg_drcr, oa.dat_system, oa.flg_forcedr, oa.flg_reverse, oa.cod_txn, oa.txt_narration1, oa.flg_entrytype, oi.cod_glsrcacct, oi.cod_gltgtacct, oi.cod_ccysrcacct, oi.cod_srcbnklglent, oi.cod_tgtbnklglent, oi.cod_brsrcacct, oi.cod_brtgtacct, CASE WHEN oi.flg_src_acc_type IN ('GI', 'GE') AND oi.nbr_srcacct = oa.drain_grp_id THEN oa.flg_drcr WHEN oi.flg_src_acc_type = 'P' AND oi.nbr_srcacct = oa.id_pool THEN oa.flg_drcr WHEN oi.nbr_srcacct = oa.nbr_ownaccount THEN oa.flg_drcr ELSE (CASE WHEN oa.flg_drcr = 'D' THEN 'C' ELSE 'D' END) END flg_srcdrcr FROM olm_instrtxn oi, olm_accountingentry oa WHERE oi.nbr_processid = oa.nbr_processid AND oi.nbr_instrtxnid = oa.nbr_instrtxnid AND oi.nbr_lotid = oa.nbr_lotid AND oa.txt_status = ? AND oi.nbr_processid = ? AND oi.txt_status = ? AND oi.nbr_lotid = ? AND oi.nbr_tgtacct = oa.nbr_ownaccount AND oi.cod_brtgtacct != ? AND oi.flg_groupsweeprule IN ('CM', 'DM') UNION SELECT oa.nbr_acctentryid, oi.nbr_instrtxnid, oa.nbr_lotid, oa.amt_txn, oa.dat_value, oi.dat_business, CASE WHEN oa.flg_drcr = 'C' THEN 'D' WHEN oa.flg_drcr = 'D' THEN 'C' END flg_drcr, oa.dat_system, oa.flg_forcedr, oa.flg_reverse, CASE WHEN SUBSTR (oa.cod_txn, 3, 1) = 'C' THEN REGEXP_REPLACE (oa.cod_txn, 'C', 'D', 1, 1 ) WHEN SUBSTR (oa.cod_txn, 3, 1) = 'D' THEN REGEXP_REPLACE (oa.cod_txn, 'D', 'C', 1, 1) END cod_txn, DECODE (SUBSTR (oa.txt_narration1, INSTR (oa.txt_narration1, 'Credit'), 6 ), 'Credit', REPLACE (oa.txt_narration1, 'Credit', 'Debit' ), SUBSTR (oa.txt_narration1, INSTR (oa.txt_narration1, 'Debit'), 5 ), 'Debit', REPLACE (oa.txt_narration1, 'Debit', 'Credit') ), oa.flg_entrytype, oi.cod_glsrcacct, oi.cod_gltgtacct, oi.cod_ccysrcacct, oi.cod_srcbnklglent, oi.cod_tgtbnklglent, oi.cod_brsrcacct, oi.cod_brtgtacct, CASE WHEN oi.flg_src_acc_type IN ('GI', 'GE') AND oi.nbr_srcacct = oa.drain_grp_id THEN oa.flg_drcr WHEN oi.flg_src_acc_type = 'P' AND oi.nbr_srcacct = oa.id_pool THEN oa.flg_drcr WHEN oi.nbr_srcacct = oa.nbr_ownaccount THEN oa.flg_drcr ELSE (CASE WHEN oa.flg_drcr = 'D' THEN 'C' ELSE 'D' END) END flg_srcdrcr FROM olm_instrtxn oi, olm_accountingentry oa WHERE oi.nbr_processid = oa.nbr_processid AND oi.nbr_instrtxnid = oa.nbr_instrtxnid AND oi.nbr_lotid = oa.nbr_lotid AND oa.txt_status = ? AND oi.nbr_processid = ? AND oi.txt_status = ? AND oi.nbr_lotid = ? AND oi.nbr_tgtacct = oa.nbr_ownaccount AND oi.cod_brtgtacct != ? AND oi.flg_groupsweeprule IN ('CM', 'DM') and oi.NBR_BANKINTERNALACCOUNT is null) oia, olm_aetemplate_mst oam, olm_aetemplate_dtls oad, opool_txn_codes otc WHERE (   (oia.cod_brtgtacct || ? || oia.cod_ccysrcacct ) = (oam.cod_entity1 || oam.cod_entity2 || oam.cod_ccy) OR ( ? || oia.cod_brtgtacct || oia.cod_ccysrcacct ) = (oam.cod_entity1 || oam.cod_entity2 || oam.cod_ccy) ) AND oam.nbr_template_id = oad.nbr_template_id AND oad.flg_drcr_set = oia.flg_srcdrcr AND oad.flg_drcr = oia.flg_drcr AND (CASE WHEN (   oad.flg_settlementpath = 'C' OR oad.flg_settlementpath = 'D' ) AND oad.flg_issettlement_entry = 'Y' THEN oia.cod_txn || 'CS' WHEN oad.flg_settlementpath = 'B' AND oad.flg_issettlement_entry = 'Y' THEN oia.cod_txn || 'BS' ELSE oia.cod_txn END ) = otc.cod_txn AND oia.dat_business BETWEEN oad.dat_effective AND NVL (oad.dat_end, TO_DATE ('31-DEC-9999', 'DD-MON-YYYY' ) )"); 
		    	
		    	sbuf.append(" UNION SELECT DISTINCT CASE WHEN oad.flg_issettlement_entry = 'Y' THEN '-2' ELSE ? END, oia.nbr_acctentryid, oad.nbr_legid, oia.nbr_instrtxnid, " +
		    			"oia.nbr_lotid, oad.nbr_ownaccount, oad.nbr_coreaccount, oad.flg_accttype, oad.cod_branch, oad.cod_bic, oad.cod_gl, oad.cod_ccy, oad.flg_drcr, " +
		    			"oia.amt_txn, oia.dat_value, 'PENDING', oad.cod_bnklglent, oad.txt_ibanacct, oia.dat_business, oad.flg_srctgttyp, 'N', oia.dat_system, " +
		    			"oia.flg_forcedr, oia.flg_reverse, oad.flg_issettlement_entry, oad.cod_triggering_gl, CASE WHEN (   oad.flg_settlementpath = 'C' OR " +
		    			"oad.flg_settlementpath = 'D' ) AND oad.flg_issettlement_entry = 'Y' THEN oia.cod_txn || 'CS' WHEN oad.flg_settlementpath = 'B' " +
		    			"AND oad.flg_issettlement_entry = 'Y' THEN oia.cod_txn || 'BS' ELSE oia.cod_txn END, otc.txt_narration1, " +
		    			//15.2_PERFORMANCE_CHANGE start
		    			"oia.nbr_acctentryid || '.' || oad.nbr_legid, oia.flg_entrytype, oad.nbr_settlement_days ,'SERVICES' FROM (SELECT oa.nbr_acctentryid, oi.nbr_instrtxnid, ") ;
		    	        //15.2_PERFORMANCE_CHANGE end
		    	        //SUPIS_157 starts
    					sbuf.append("oa.nbr_lotid, oa.amt_txn, oa.dat_value, oa.dat_business, oa.flg_drcr, oa.dat_system, oa.flg_forcedr, oa.flg_reverse, oa.cod_txn, ");
		    			//SUPIS_157 ends
		    			sbuf.append("oa.txt_narration1, oa.flg_entrytype, oi.cod_glsrcacct, oi.cod_gltgtacct, oi.cod_ccysrcacct, oi.cod_srcbnklglent, oi.cod_tgtbnklglent, " +
		    			"oi.cod_brsrcacct, oi.cod_brtgtacct, CASE WHEN oi.flg_src_acc_type IN ('GI', 'GE') AND oi.nbr_srcacct = oa.drain_grp_id " +
		    			"THEN oa.flg_drcr WHEN oi.flg_src_acc_type = 'P' AND oi.nbr_srcacct = oa.id_pool THEN oa.flg_drcr WHEN oi.nbr_srcacct = oa.nbr_ownaccount " +
						//SUPIS_123 STARTS
		    			"THEN oa.flg_drcr ELSE (CASE WHEN oa.flg_drcr = 'D' THEN 'C' ELSE 'D' END ) END flg_srcdrcr FROM ");
		    			
		    			if(!isBODReversal){
		    				sbuf.append("olm_instrtxn oi, olm_accountingentry oa ");
		    			}else{
		    				sbuf.append("olm_instrtxnvw oi, olm_accountingentry oa ");
		    			}
		    			sbuf.append("WHERE oi.nbr_processid = oa.nbr_processid AND oi.nbr_instrtxnid = oa.nbr_instrtxnid " );
		    			
		    			if(!isBODReversal){
							sbuf.append("AND oi.nbr_lotid = oa.nbr_lotid ");
						}
		    			
		    			sbuf.append("AND oa.txt_status = ? ");
		    			sbuf.append("AND oi.nbr_processid = ? AND oi.txt_status = ? ");
		    			
    					if(!isBODReversal){
							sbuf.append("AND oi.nbr_lotid = ? ");
						}
		    			
		    			sbuf.append("AND oi.nbr_tgtacct != oa.nbr_ownaccount AND oi.cod_brsrcacct != ? " +
						//SUPIS_123 ENDS
		    			"AND oi.flg_groupsweeprule IN ('CM', 'DM') UNION SELECT oa.nbr_acctentryid, oi.nbr_instrtxnid, oa.nbr_lotid, oa.amt_txn, oa.dat_value, " );
		    			//SUPIS_157 starts
	    				sbuf.append("oa.dat_business, CASE WHEN oa.flg_drcr = 'C' THEN 'D' WHEN oa.flg_drcr = 'D' THEN 'C' END flg_drcr, oa.dat_system, oa.flg_forcedr, ");
		    			//SUPIS_157 ends
		    			sbuf.append("oa.flg_reverse, SUBSTR (oa.cod_txn, 1, LENGTH (oa.cod_txn) - 1 ) || DECODE (SUBSTR (oa.cod_txn, -1, 1), 'C', 'D', 'D', 'C' ) cod_txn, " +
		    			"DECODE (SUBSTR (oa.txt_narration1, INSTR (oa.txt_narration1, 'Credit'), 6 ), 'Credit', REPLACE (oa.txt_narration1, 'Credit', 'Debit' ), " +
		    			"SUBSTR (oa.txt_narration1, INSTR (oa.txt_narration1, 'Debit'), 5 ), 'Debit', REPLACE (oa.txt_narration1, 'Debit', 'Credit' ) ), " +
		    			"oa.flg_entrytype, oi.cod_glsrcacct, oi.cod_gltgtacct, oi.cod_ccysrcacct, oi.cod_srcbnklglent, oi.cod_tgtbnklglent, oi.cod_brsrcacct, " +
		    			"oi.cod_brtgtacct, CASE WHEN oi.flg_src_acc_type IN ('GI', 'GE') AND oi.nbr_srcacct = oa.drain_grp_id THEN oa.flg_drcr WHEN oi.flg_src_acc_type = 'P' " +
		    			"AND oi.nbr_srcacct = oa.id_pool THEN oa.flg_drcr WHEN oi.nbr_srcacct = oa.nbr_ownaccount THEN oa.flg_drcr " +
						//SUPIS_123 STARTS
		    			"ELSE (CASE WHEN oa.flg_drcr = 'D' THEN 'C' ELSE 'D' END ) END flg_srcdrcr " );
		    			
		    			if(!isBODReversal){
		    				sbuf.append("FROM olm_instrtxn oi, olm_accountingentry oa ");
		    			}else{
		    				sbuf.append("FROM olm_instrtxnvw oi, olm_accountingentry oa ");
		    			}
		    			sbuf.append(" WHERE oi.nbr_processid = oa.nbr_processid AND oi.nbr_instrtxnid = oa.nbr_instrtxnid " );
		    			
		    			
		    			if(!isBODReversal){
							sbuf.append("AND oi.nbr_lotid = oa.nbr_lotid ");
						}
		    			
		    			sbuf.append("AND oa.txt_status = ? AND oi.nbr_processid = ? AND oi.txt_status = ? ");
		    			
		    			if(!isBODReversal){
							sbuf.append("AND oi.nbr_lotid = ? ");
						}
		    			
		    			sbuf.append(" AND oi.nbr_tgtacct != oa.nbr_ownaccount AND oi.cod_brsrcacct != ? " +
						//SUPIS_123 ENDS
		    			"AND oi.flg_groupsweeprule IN ('CM', 'DM') AND oi.nbr_bankinternalaccount IS NULL) oia, olm_aetemplate_mst oam, olm_aetemplate_dtls oad, " +
		    			"opool_txn_codes otc WHERE (oia.cod_brsrcacct || ? || oia.cod_ccysrcacct) = (oam.cod_entity1 || oam.cod_entity2 || oam.cod_ccy ) " +
		    			"AND oam.nbr_template_id = oad.nbr_template_id AND oad.flg_drcr_set = oia.flg_srcdrcr AND oad.flg_drcr = oia.flg_drcr AND " +
		    			"(CASE WHEN (   oad.flg_settlementpath = 'C' OR oad.flg_settlementpath = 'D' ) AND oad.flg_issettlement_entry = 'Y' THEN oia.cod_txn || 'CS' " +
		    			"WHEN oad.flg_settlementpath = 'B' AND oad.flg_issettlement_entry = 'Y' THEN oia.cod_txn || 'BS' ELSE oia.cod_txn END ) = otc.cod_txn " +
		    			"AND oia.dat_business BETWEEN oad.dat_effective AND NVL (oad.dat_end, TO_DATE ('31-DEC-9999', 'DD-MON-YYYY' ) ) UNION SELECT " +
		    			"DISTINCT CASE WHEN oad.flg_issettlement_entry = 'Y' THEN '-2' ELSE ? END, oia.nbr_acctentryid, oad.nbr_legid, oia.nbr_instrtxnid, oia.nbr_lotid, " +
		    			"oad.nbr_ownaccount, oad.nbr_coreaccount, oad.flg_accttype, oad.cod_branch, oad.cod_bic, oad.cod_gl, oad.cod_ccy, " +
		    			"CASE WHEN oad.flg_drcr = 'C' THEN 'D' WHEN oad.flg_drcr = 'D' THEN 'C' END, oia.amt_txn, oia.dat_value, 'PENDING', oad.cod_bnklglent, " +
		    			"oad.txt_ibanacct, oia.dat_business, oad.flg_srctgttyp, 'N', oia.dat_system, oia.flg_forcedr, oia.flg_reverse, oad.flg_issettlement_entry, " +
		    			"oad.cod_triggering_gl, CASE WHEN (   oad.flg_settlementpath = 'C' OR oad.flg_settlementpath = 'D' ) AND oad.flg_issettlement_entry = 'Y' " +
		    			// SUP_RBS_226 Starts .... 
		    			//"THEN oia.cod_txn || 'CS' WHEN oad.flg_settlementpath = 'B' AND oad.flg_issettlement_entry = 'Y' THEN oia.cod_txn || 'BS' ELSE oia.cod_txn END, " +
		    			"THEN SUBSTR (oia.cod_txn, 1, LENGTH (oia.cod_txn) - 1 ) || DECODE (SUBSTR (oia.cod_txn, -1, 1), 'C', 'D', 'D', 'C' )  || 'CS' " +
		    			"WHEN oad.flg_settlementpath = 'B' AND oad.flg_issettlement_entry = 'Y' THEN SUBSTR (oia.cod_txn, 1, LENGTH (oia.cod_txn) - 1 ) || " +
		    			"DECODE (SUBSTR (oia.cod_txn, -1, 1), 'C', 'D', 'D', 'C' )  || 'BS' ELSE " +
		    			"SUBSTR (oia.cod_txn, 1, LENGTH (oia.cod_txn) - 1 ) || DECODE (SUBSTR (oia.cod_txn, -1, 1), 'C', 'D', 'D', 'C' )  END, " +
		    			// SUP_RBS_226 Ends
		    			//15.2_PERFORMANCE_CHANGE start
		    			"otc.txt_narration1, oia.nbr_acctentryid || '.' || oad.nbr_legid, oia.flg_entrytype, oad.nbr_settlement_days ,'SERVICES' FROM (SELECT oa.nbr_acctentryid, " +
		    			//15.2_PERFORMANCE_CHANGE end
		    			//SUPIS_157 STARTS
		    			"oi.nbr_instrtxnid, oa.nbr_lotid, oa.amt_txn, oa.dat_value, ");
	    				sbuf.append("oa.dat_business, ");
		    			sbuf.append("oa.flg_drcr, oa.dat_system, oa.flg_forcedr, oa.flg_reverse, " +
						//SUPIS_157 ends
		    			"oa.cod_txn, oa.txt_narration1, oa.flg_entrytype, oi.cod_glsrcacct, oi.cod_gltgtacct, oi.cod_ccysrcacct, oi.cod_srcbnklglent, oi.cod_tgtbnklglent, " +
		    			"oi.cod_brsrcacct, oi.cod_brtgtacct, CASE WHEN oi.flg_src_acc_type IN ('GI', 'GE') AND oi.nbr_srcacct = oa.drain_grp_id THEN oa.flg_drcr " +
		    			"WHEN oi.flg_src_acc_type = 'P' AND oi.nbr_srcacct = oa.id_pool THEN oa.flg_drcr WHEN oi.nbr_srcacct = oa.nbr_ownaccount THEN oa.flg_drcr " +
						//SUPIS_123 ENDS
		    			"ELSE (CASE WHEN oa.flg_drcr = 'D' THEN 'C' ELSE 'D' END ) END flg_srcdrcr " );
		    			if(!isBODReversal){
		    				sbuf.append("FROM olm_instrtxn oi, olm_accountingentry oa ");
		    			}else{
		    				sbuf.append("FROM olm_instrtxnvw oi, olm_accountingentry oa ");
		    			}
		    			sbuf.append("WHERE oi.nbr_processid = oa.nbr_processid AND oi.nbr_instrtxnid = oa.nbr_instrtxnid ");
		    			
		    			
		    			
		    			if(!isBODReversal){
							sbuf.append("AND oi.nbr_lotid = oa.nbr_lotid ");
						}
		    			
		    			sbuf.append("AND oa.txt_status = ? AND oi.nbr_processid = ? AND oi.txt_status = ? ");
		    			
		    			if(!isBODReversal){
							sbuf.append("AND oi.nbr_lotid = ? ");
						}
		    			
		    			sbuf.append("AND oi.nbr_tgtacct = oa.nbr_ownaccount AND oi.cod_brtgtacct != ? " +
						//SUPIS_123 ENDS
						//SUPIS_157 STARTS
		    			"AND oi.flg_groupsweeprule IN ('CM', 'DM') UNION SELECT oa.nbr_acctentryid, oi.nbr_instrtxnid, oa.nbr_lotid, oa.amt_txn, oa.dat_value, ");
	    				sbuf.append("oa.dat_business, ");
		    			sbuf.append("CASE WHEN oa.flg_drcr = 'C' THEN 'D' WHEN oa.flg_drcr = 'D' THEN 'C' END flg_drcr, oa.dat_system, oa.flg_forcedr, " +
						//SUPIS_157 ends
		    			"oa.flg_reverse, SUBSTR (oa.cod_txn, 1, LENGTH (oa.cod_txn) - 1 ) || DECODE (SUBSTR (oa.cod_txn, -1, 1), 'C', 'D', 'D', 'C' ) cod_txn, " +
		    			"DECODE (SUBSTR (oa.txt_narration1, INSTR (oa.txt_narration1, 'Credit'), 6 ), 'Credit', REPLACE (oa.txt_narration1, 'Credit', 'Debit' ), " +
		    			"SUBSTR (oa.txt_narration1, INSTR (oa.txt_narration1, 'Debit'), 5 ), 'Debit', REPLACE (oa.txt_narration1, 'Debit', 'Credit' ) ), " +
		    			"oa.flg_entrytype, oi.cod_glsrcacct, oi.cod_gltgtacct, oi.cod_ccysrcacct, oi.cod_srcbnklglent, oi.cod_tgtbnklglent, oi.cod_brsrcacct, " +
		    			"oi.cod_brtgtacct, CASE WHEN oi.flg_src_acc_type IN ('GI', 'GE') AND oi.nbr_srcacct = oa.drain_grp_id THEN oa.flg_drcr " +
		    			"WHEN oi.flg_src_acc_type = 'P' AND oi.nbr_srcacct = oa.id_pool THEN oa.flg_drcr WHEN oi.nbr_srcacct = oa.nbr_ownaccount " +
						//SUPIS_123 STARTS
		    			"THEN oa.flg_drcr ELSE (CASE WHEN oa.flg_drcr = 'D' THEN 'C' ELSE 'D' END ) END flg_srcdrcr " );
		    			
		    			if(!isBODReversal){
		    				sbuf.append("FROM olm_instrtxn oi, olm_accountingentry oa ");
		    			}else{
		    				sbuf.append("FROM olm_instrtxnvw oi, olm_accountingentry oa ");
		    			}
		    			
		    			sbuf.append("WHERE oi.nbr_processid = oa.nbr_processid AND oi.nbr_instrtxnid = oa.nbr_instrtxnid ");
		    			
		    			if(!isBODReversal){
							sbuf.append("AND oi.nbr_lotid = oa.nbr_lotid ");
						}
		    			
		    			
		    			sbuf.append("AND oa.txt_status = ? AND oi.nbr_processid = ? AND oi.txt_status = ? ");
		    			
		    			if(!isBODReversal){
							sbuf.append("AND oi.nbr_lotid = ? ");
						}
		    			
		    			sbuf.append("AND oi.nbr_tgtacct = oa.nbr_ownaccount AND oi.cod_brtgtacct != ? " +
						//SUPIS_123 ENDS
		    			"AND oi.flg_groupsweeprule IN ('CM', 'DM') AND oi.nbr_bankinternalaccount IS NULL) oia, olm_aetemplate_mst oam, olm_aetemplate_dtls oad, " +
		    			"opool_txn_codes otc WHERE (oia.cod_brtgtacct || ? || oia.cod_ccysrcacct) = (oam.cod_entity1 || oam.cod_entity2 || oam.cod_ccy ) " +
		    			"AND oam.nbr_template_id = oad.nbr_template_id AND oad.flg_drcr_set = oia.flg_srcdrcr AND oad.flg_drcr = oia.flg_drcr " +
		    			"AND (CASE WHEN (   oad.flg_settlementpath = 'C' OR oad.flg_settlementpath = 'D' ) AND oad.flg_issettlement_entry = 'Y' " +
		    			"THEN oia.cod_txn || 'CS' WHEN oad.flg_settlementpath = 'B' " +
		    			"AND oad.flg_issettlement_entry = 'Y' THEN oia.cod_txn || 'BS' ELSE oia.cod_txn END ) = otc.cod_txn " +
		    			"AND oia.dat_business BETWEEN oad.dat_effective AND NVL (oad.dat_end, TO_DATE ('31-DEC-9999', 'DD-MON-YYYY' ) )");
		    	
		    	
		    	//SUP_IS6597 ENDS
		    }
		  //ENH_Nostroloro_branch end
		    sbuf.append(" )");
		    if(logger.isDebugEnabled())
		    	logger.debug("Accounting Entry SQL :: "+sbuf.toString());
		    //JTest changes on 01-feb-11 starts
		    //stmt = connection.createStatement();
		    stmt = connection.prepareStatement(sbuf.toString());
			//SUPIS_123 STARTS
		    int index=1;
		    
		    if(!isBODReversal){
		    	stmt.setString(1, String.valueOf(nbrProcessId));
			    stmt.setString(2, ExecutionConstants.PENDING);
			    
			    stmt.setLong(3, nbrProcessId);
			    stmt.setString(4, ExecutionConstants.PENDING);
		    	stmt.setLong(5, nbrLotId);
			    
			    //ENH_Nostroloro_branch start
			    if(ExecutionConstants.NOSTRO_LORO_BRANCH.equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
			    	stmt.setString(6, String.valueOf(nbrProcessId));
			    	stmt.setString(7, ExecutionConstants.PENDING);
				    stmt.setLong(8, nbrProcessId);
				    stmt.setString(9, ExecutionConstants.PENDING);
				    stmt.setLong(10, nbrLotId);
				    stmt.setString(11, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
				    stmt.setString(12, ExecutionConstants.PENDING);
				    stmt.setLong(13, nbrProcessId);
				    stmt.setString(14, ExecutionConstants.PENDING);
				    stmt.setLong(15, nbrLotId);
				    stmt.setString(16, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
				    //SUP_IS6597 STARTS
				    stmt.setString(17, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
				    //SUP_IS6597 STARTS
				    //stmt.setString(18, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
				    stmt.setString(18, String.valueOf(nbrProcessId));
				    stmt.setString(19, ExecutionConstants.PENDING);
				    stmt.setLong(20, nbrProcessId);
				    stmt.setString(21, ExecutionConstants.PENDING);
				    stmt.setLong(22, nbrLotId);
				    stmt.setString(23, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
				    stmt.setString(24, ExecutionConstants.PENDING);
				    stmt.setLong(25, nbrProcessId);
				    stmt.setString(26, ExecutionConstants.PENDING);
				    stmt.setLong(27, nbrLotId);
				    stmt.setString(28, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
				    stmt.setString(29, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
				   // stmt.setString(31, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
				  //SUP_IS6597 ENDS
			    }
		    }else{
		    	index=1;
		    	stmt.setString(index++, String.valueOf(nbrProcessId));
			    stmt.setString(index++, ExecutionConstants.PENDING);
			    stmt.setLong(index++, nbrProcessId);
			    stmt.setString(index++, ExecutionConstants.SUCCESSFUL);
		    	
			    if(ExecutionConstants.NOSTRO_LORO_BRANCH.equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
			    	stmt.setString(index++, String.valueOf(nbrProcessId));
			    	stmt.setString(index++, ExecutionConstants.PENDING);
				    stmt.setLong(index++, nbrProcessId);
				    stmt.setString(index++, ExecutionConstants.SUCCESSFUL);
				    stmt.setString(index++, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
				    stmt.setString(index++, ExecutionConstants.PENDING);
				    stmt.setLong(index++, nbrProcessId);
				    stmt.setString(index++, ExecutionConstants.SUCCESSFUL);
				    stmt.setString(index++, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
				    stmt.setString(index++, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
				    stmt.setString(index++, String.valueOf(nbrProcessId));
				    stmt.setString(index++, ExecutionConstants.PENDING);
				    stmt.setLong(index++, nbrProcessId);
				    stmt.setString(index++, ExecutionConstants.SUCCESSFUL);
				    stmt.setString(index++, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
				    stmt.setString(index++, ExecutionConstants.PENDING);
				    stmt.setLong(index++, nbrProcessId);
				    stmt.setString(index++, ExecutionConstants.SUCCESSFUL);
				    stmt.setString(index++, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
				    stmt.setString(index++, ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER );
			    }
		    }
			//SUPIS_123 ENDS
		    //ENH_Nostroloro_branch end
		    //int insertCnt = stmt.executeUpdate(sbuf.toString());
		    int insertCnt = stmt.executeUpdate();
		    if(logger.isDebugEnabled())
		    	logger.debug("Accounting Entry Insert Count :: "+insertCnt);
		    //JTest changes on 01-feb-11 ends
	
		}catch(Exception e){
		    logger.fatal("Exception generateAEFromTemplate :: "+e);
		    throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, "", e);
		}
		finally{
			DBUtils.closeResource(stmt);
		}
		if(logger.isDebugEnabled())//JTest changes on 01-feb-11
			logger.debug("Leaving generateAEFromTemplate........");
    }

    /**
     * 
     * @param rs
     * @param srcTemplateDo
     * @param tgtTemplateDo
     * @throws LMSException
     */
    private void populateAETemplateDO(ResultSet rs,AccountingEntryTemplateDO srcTemplateDo,
	    AccountingEntryTemplateDO tgtTemplateDo) throws LMSException{
    	if(logger.isDebugEnabled())//JTest changes on 01-feb-11
    		logger.debug("populateAETemplateDO....");
		try{
		    srcTemplateDo.setFlgDrcrSet(ExecutionConstants.DEBIT);
	
		    srcTemplateDo.setCodGl(rs.getString("COD_GLSRCACCT"));
		    srcTemplateDo.setCodCcy(rs.getString("COD_CCYSRCACCT"));
		    srcTemplateDo.setFlgAcctType(rs.getString("FLG_SRC_ACC_TYPE"));
		    srcTemplateDo.setNbrCoreAccount("");
		    //srcTemplateDo.setNbrOwnAccount(rs.getString("NBR_SRCACCT"));
		    srcTemplateDo.setNbrTemplateId(rs.getString("NBR_TEMPLATE_ID"));
		    srcTemplateDo.setFlgDrcr(ExecutionConstants.DEBIT);
		    srcTemplateDo.setFlgSrcTgtTyp(ExecutionConstants.SOURCE);
		    srcTemplateDo.setCodTriggeringGl(rs.getString("COD_GLSRCACCT"));
		    if("BLE".equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
		    srcTemplateDo.setCodBnkLglEnt(rs.getString("COD_SRCBNKLGLENT"));
		    }
		    //ENH_Nostroloro_branch start
		    else if(ExecutionConstants.NOSTRO_LORO_BRANCH.equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
			    srcTemplateDo.setCodBranch(rs.getString("COD_BRSRCACCT"));
			}
		    //ENH_Nostroloro_branch end
		    tgtTemplateDo.setFlgDrcrSet(ExecutionConstants.DEBIT);
	
		    tgtTemplateDo.setCodGl(rs.getString("COD_GLTGTACCT"));
		    tgtTemplateDo.setCodCcy(rs.getString("COD_CCYTGTACCT"));
		    tgtTemplateDo.setFlgAcctType(rs.getString("FLG_TGT_ACC_TYPE"));
		    tgtTemplateDo.setNbrCoreAccount("");
		    //tgtTemplateDo.setNbrOwnAccount(rs.getString("NBR_TGTACCT"));
		    tgtTemplateDo.setNbrTemplateId(rs.getString("NBR_TEMPLATE_ID"));
		    tgtTemplateDo.setFlgDrcr(ExecutionConstants.CREDIT);
		    tgtTemplateDo.setFlgSrcTgtTyp(ExecutionConstants.TARGET);
		    tgtTemplateDo.setCodTriggeringGl(rs.getString("COD_GLTGTACCT"));
		    if("BLE".equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
		    	
		    tgtTemplateDo.setCodBnkLglEnt(rs.getString("COD_TGTBNKLGLENT"));
		    
		    }
		    //ENH_Nostroloro_branch start
		    else if(ExecutionConstants.NOSTRO_LORO_BRANCH.equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
		    	tgtTemplateDo.setCodBranch(rs.getString("COD_BRTGTACCT"));
			}
		    //ENH_Nostroloro_branch end
	
		}catch(Exception e){
		    throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, "", e);
		}
		if(logger.isDebugEnabled())//JTest changes on 01-feb-11
			logger.debug("populateAETemplateDO....Leaving");
    }

    /**
     * 
     * @param srcAETemplateDO
     * @param tgtAETemplateDO
     * @param srcTgtNspNostroOwnerList
     * @return
     * @throws LMSException
     */
    private boolean isHostsExistAsBiLateral(AccountingEntryTemplateDO srcAETemplateDO,
	    AccountingEntryTemplateDO tgtAETemplateDO,ArrayList srcTgtNspNostroOwnerList, Connection conn) throws LMSException{
    	//JTest changes on 01-feb-11 starts
    	if(logger.isDebugEnabled())
    		logger.debug("isHostsExistAsBiLateral....");
    	
		PreparedStatement stmt = null;
		//JTest changes on 01-feb-11 ends
		ResultSet rs = null;
		boolean isBiLateralExist = false;
		try{
		    String entity1 = "";
		    String entity2 = "";
	
		    String codCcy = srcAETemplateDO.getCodCcy();
	
		    if("HostSystem".equals(ExecutionConstants.ENTITY_TYPE)){
			entity1 = srcAETemplateDO.getCodGl();
			entity2 = tgtAETemplateDO.getCodGl();
		    }else if("BLE".equals(ExecutionConstants.ENTITY_TYPE)){
			entity1 = srcAETemplateDO.getCodBnkLglEnt();
			entity2 = tgtAETemplateDO.getCodBnkLglEnt();
		    }
		    //ENH_Nostroloro_branch start
		    else if(ExecutionConstants.NOSTRO_LORO_BRANCH.equals(ExecutionConstants.ENTITY_TYPE)){
				entity1 = srcAETemplateDO.getCodBranch();
				entity2 = tgtAETemplateDO.getCodBranch();
			}
		    //ENH_Nostroloro_branch end
	
		    /*String sql = "SELECT OND.*, OA1.COD_BNK_LGL COD_BNK_LGL_NSP, OA1.COD_COUNTRY COD_COUNTRY_NSP, OA1.NBR_COREACCOUNT NBR_COREACCOUNT_NSP,OA1.COD_BRANCH COD_BRANCH_NSP," +
		    "OA2.COD_BNK_LGL COD_BNK_LGL_NO, OA2.COD_COUNTRY COD_COUNTRY_NO, OA2.NBR_COREACCOUNT NBR_COREACCOUNT_NO,OA2.COD_BRANCH COD_BRANCH_NO " +
		    "FROM OLM_NOSTROLORO_MST ONM, OLM_NOSTROLORO_DTLS OND, ORBICASH_ACCOUNTS OA1, " +
		    "ORBICASH_ACCOUNTS OA2 WHERE ONM.NBR_CONTRACT_NO = OND.NBR_CONTRACT_NO " +
		    "AND ONM.COD_LINKAGE_TYPE = '"+ExecutionConstants.BI_LATERAL+"' " +
		    "AND ONM.TYP_ENTITY = '"+ExecutionConstants.ENTITY_TYPE+"' " +
		    "AND OND.COD_ENTITY_NOSTRO = '"+entity1+"' AND OND.COD_ENTITY_LORO = '"+entity2+"' " +
		    "AND OND.COD_CCY = '"+codCcy+"' AND OND.NBR_ACCTNO_NOSTRO = OA1.NBR_OWNACCOUNT " +
		    "AND OND.NBR_ACCTNO_LORO = OA2.NBR_OWNACCOUNT";*/
		  //JTest changes on 01-feb-11 starts
		    /*String sql = "SELECT OND.*, OA1.COD_BNK_LGL COD_BNK_LGL_NSP, OA1.COD_COUNTRY COD_COUNTRY_NSP, " +
		    		"OA1.NBR_COREACCOUNT NBR_COREACCOUNT_NSP, OA1.COD_BRANCH COD_BRANCH_NSP, " +
		    		//- BILATERALSETTLEMENT - Start
		    		"OA1.COD_GL COD_GL_NSP,OA2.COD_GL COD_GL_NO,"+
		    		//- BILATERALSETTLEMENT - End
		    		"OA2.COD_BNK_LGL COD_BNK_LGL_NO, OA2.COD_COUNTRY COD_COUNTRY_NO, " +
		    		"OA2.NBR_COREACCOUNT NBR_COREACCOUNT_NO, OA2.COD_BRANCH COD_BRANCH_NO " +
		    		"FROM OLM_NOSTROLORO_MST ONM, OLM_NOSTROLORO_DTLS OND, ORBICASH_ACCOUNTS OA1, " +
		    		"ORBICASH_ACCOUNTS OA2 WHERE ONM.NBR_CONTRACT_NO = OND.NBR_CONTRACT_NO " +
		    		"AND ONM.COD_LINKAGE_TYPE = '"+ExecutionConstants.BI_LATERAL+"' " +
		    		//Jiten - BILATERALSETTLEMENT - Start
		    		"AND ONM.FLG_CLOSED = '"+ExecutionConstants.NO+"' " +
		    		//Jiten - BILATERALSETTLEMENT - End
		    		"AND ONM.TYP_ENTITY = '"+ExecutionConstants.ENTITY_TYPE+"' " +
		    		"AND OND.COD_ENTITY_NOSTRO = '"+entity1+"' AND OND.COD_ENTITY_LORO = '"+entity2+"' " +
		    		"AND OND.COD_CCY = '"+codCcy+"' AND OND.NBR_ACCTNO_NOSTRO = OA1.NBR_OWNACCOUNT " +
		    		"AND OND.NBR_ACCTNO_LORO = OA2.NBR_OWNACCOUNT AND " +
		    		"(SELECT MAX(DAT_TODAY) + 1 FROM OLM_HOSTSYSTEMDATES WHERE COD_GL IN(OA1.COD_GL,OA2.COD_GL)) " +
		    		"BETWEEN OND.DAT_EFFECTIVE AND NVL( OND.DAT_END , TO_DATE('31-DEC-9999','DD-MON-YYYY')) ";*/
		    String sql = "SELECT OND.*, OA1.COD_BNK_LGL COD_BNK_LGL_NSP, OA1.COD_COUNTRY COD_COUNTRY_NSP, " +
    		"OA1.NBR_COREACCOUNT NBR_COREACCOUNT_NSP, OA1.COD_BRANCH COD_BRANCH_NSP, " +
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
    		//- BILATERALSETTLEMENT - Start
    		"OA1.COD_GL COD_GL_NSP,OA2.COD_GL COD_GL_NO,"+
    		//- BILATERALSETTLEMENT - End
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
    		"OA2.COD_BNK_LGL COD_BNK_LGL_NO, OA2.COD_COUNTRY COD_COUNTRY_NO, " +
    		"OA2.NBR_COREACCOUNT NBR_COREACCOUNT_NO, OA2.COD_BRANCH COD_BRANCH_NO " +
    		"FROM OLM_NOSTROLORO_MST ONM, OLM_NOSTROLORO_DTLS OND, ORBICASH_ACCOUNTS OA1, " +
    		"ORBICASH_ACCOUNTS OA2 WHERE ONM.NBR_CONTRACT_NO = OND.NBR_CONTRACT_NO " +
    		"AND ONM.COD_LINKAGE_TYPE = ? " +
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
    		//Jiten - BILATERALSETTLEMENT - Start
    		"AND ONM.FLG_CLOSED = ? " +
    		//Jiten - BILATERALSETTLEMENT - End
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
    		"AND ONM.TYP_ENTITY = ? " +
    		"AND OND.COD_ENTITY_NOSTRO = ? AND OND.COD_ENTITY_LORO = ? " +
    		"AND OND.COD_CCY = ? AND OND.NBR_ACCTNO_NOSTRO = OA1.NBR_OWNACCOUNT " +
    		"AND OND.NBR_ACCTNO_LORO = OA2.NBR_OWNACCOUNT AND " +
    		"(SELECT MAX(DAT_TODAY) + 1 FROM OLM_HOSTSYSTEMDATES WHERE COD_GL IN(OA1.COD_GL,OA2.COD_GL)) " +
    		"BETWEEN OND.DAT_EFFECTIVE AND NVL( OND.DAT_END , TO_DATE('31-DEC-9999','DD-MON-YYYY')) ";
		    
		    if(logger.isDebugEnabled())
		    	logger.debug("sql :: "+sql);
	
		    stmt = conn.prepareStatement(sql);
		    stmt.setString(1, ExecutionConstants.BI_LATERAL);
		    stmt.setString(2, ExecutionConstants.NO);
		    stmt.setString(3, ExecutionConstants.ENTITY_TYPE);
		    stmt.setString(4, entity1);
		    stmt.setString(5, entity2);
		    stmt.setString(6, codCcy);
		    rs = stmt.executeQuery();
		    //JTest changes on 01-feb-11 ends
		    if(rs.next()){
			isBiLateralExist = true;
	
			AccountingEntryTemplateDO nspAETemplateDO =  new AccountingEntryTemplateDO();
			AccountingEntryTemplateDO noAETemplateDO =  new AccountingEntryTemplateDO();
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
			//- BILATERALSETTLEMENT - Start
			if(srcAETemplateDO.getCodTriggeringGl() == null || srcAETemplateDO.getCodTriggeringGl().trim().equals("")){
			    srcAETemplateDO.setCodTriggeringGl(rs.getString("COD_GL_NO"));
			}
			if(tgtAETemplateDO.getCodTriggeringGl() == null || tgtAETemplateDO.getCodTriggeringGl().trim().equals("")){
			    tgtAETemplateDO.setCodTriggeringGl(rs.getString("COD_GL_NO"));
			}
			//- BILATERALSETTLEMENT - End
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
			nspAETemplateDO.setNbrOwnAccount(rs.getString("NBR_ACCTNO_NOSTRO"));
			nspAETemplateDO.setNbrCoreAccount(rs.getString("NBR_COREACCOUNT_NSP"));
			nspAETemplateDO.setCodBnkLglEnt(rs.getString("COD_BNK_LGL_NSP"));
			nspAETemplateDO.setCodCcy(codCcy);
			nspAETemplateDO.setCodCountry(rs.getString("COD_COUNTRY_NSP"));
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
			//- BILATERALSETTLEMENT - Start
			//nspAETemplateDO.setCodGl(srcAETemplateDO.getCodGl());
			nspAETemplateDO.setCodGl(rs.getString("COD_GL_NSP"));
			//- BILATERALSETTLEMENT - End
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
			nspAETemplateDO.setFlgPersist("Y");
			nspAETemplateDO.setNbrTemplateId(srcAETemplateDO.getNbrTemplateId());
			nspAETemplateDO.setNbrContractNo(rs.getString("NBR_CONTRACT_NO"));
			nspAETemplateDO.setNbrSeqNo(rs.getString("NBR_SEQ"));
			nspAETemplateDO.setFlgSettlementEntry("N");
	
			nspAETemplateDO.setNbrSettlementDays(rs.getString("NBR_SETTLEMENT_DAYS"));
			if(srcAETemplateDO.getDateEffective() != null && srcAETemplateDO.getDateEffective().compareTo(rs.getDate("DAT_EFFECTIVE")) > 0 ){
			    nspAETemplateDO.setDateEffective(srcAETemplateDO.getDateEffective());
			}else{
			    nspAETemplateDO.setDateEffective(rs.getDate("DAT_EFFECTIVE"));
			}
			nspAETemplateDO.setFlgDrcr(srcAETemplateDO.getFlgDrcr());
			nspAETemplateDO.setFlgAcctType("I");
			nspAETemplateDO.setFlgSrcTgtTyp("S");
			nspAETemplateDO.setCodBranch(rs.getString("COD_BRANCH_NSP"));
			nspAETemplateDO.setDateEnd(rs.getDate("DAT_END"));
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
			
			//- BILATERALSETTLEMENT - Start
			nspAETemplateDO.setCodTriggeringGl(tgtAETemplateDO.getCodTriggeringGl());
			nspAETemplateDO.setFlgSettlementPath("B");
			//- BILATERALSETTLEMENT - End
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
	
			noAETemplateDO.setNbrOwnAccount(rs.getString("NBR_ACCTNO_LORO"));
			noAETemplateDO.setNbrCoreAccount(rs.getString("NBR_COREACCOUNT_NO"));
			noAETemplateDO.setCodBnkLglEnt(rs.getString("COD_BNK_LGL_NO"));
			noAETemplateDO.setCodCcy(codCcy);
			noAETemplateDO.setCodCountry(rs.getString("COD_COUNTRY_NO"));
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
			//- BILATERALSETTLEMENT - Start
			//noAETemplateDO.setCodGl(tgtAETemplateDO.getCodGl());
			noAETemplateDO.setCodGl(rs.getString("COD_GL_NO"));
			//- BILATERALSETTLEMENT - End
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
			noAETemplateDO.setFlgPersist("Y");
			noAETemplateDO.setNbrTemplateId(tgtAETemplateDO.getNbrTemplateId());
			noAETemplateDO.setNbrContractNo(rs.getString("NBR_CONTRACT_NO"));
			noAETemplateDO.setNbrSeqNo(rs.getString("NBR_SEQ"));
			noAETemplateDO.setFlgSettlementEntry("N");
	
			noAETemplateDO.setNbrSettlementDays(rs.getString("NBR_SETTLEMENT_DAYS"));
			if(tgtAETemplateDO.getDateEffective() != null && tgtAETemplateDO.getDateEffective().compareTo(rs.getDate("DAT_EFFECTIVE")) > 0 ){
			    noAETemplateDO.setDateEffective(tgtAETemplateDO.getDateEffective());
			}else{
			    noAETemplateDO.setDateEffective(rs.getDate("DAT_EFFECTIVE"));
			}
			
			noAETemplateDO.setFlgDrcr(tgtAETemplateDO.getFlgDrcr());
			noAETemplateDO.setFlgAcctType("I");
			noAETemplateDO.setFlgSrcTgtTyp("T");
			noAETemplateDO.setCodBranch(rs.getString("COD_BRANCH_NO"));
			noAETemplateDO.setDateEnd(rs.getDate("DAT_END"));
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
			
			//- BILATERALSETTLEMENT - Start
			noAETemplateDO.setCodTriggeringGl(tgtAETemplateDO.getCodTriggeringGl());
			noAETemplateDO.setFlgSettlementPath("B");
			//- BILATERALSETTLEMENT - End
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
			
			srcTgtNspNostroOwnerList.add(nspAETemplateDO);
			srcTgtNspNostroOwnerList.add(noAETemplateDO);
	
		    }
	
		}catch(Exception e){
		    throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, "", e);
		}
		finally{
			//JTest changes on 01-feb-11 start
			LMSConnectionUtility.close(rs);
			LMSConnectionUtility.close(stmt);
			//JTest changes on 01-feb-11 ends
		}
		if(logger.isDebugEnabled())//JTest changes on 01-feb-11
			logger.debug("isHostsExistAsBiLateral....Leaving");
		return isBiLateralExist;
    }
    
    /**
     * 
     * @param srcAETemplateDO
     * @param tgtAETemplateDO
     * @param srcTgtCBTSettlementList
     * @return
     * @throws LMSException
     */
    private boolean isHostsExistAsSettlement(AccountingEntryTemplateDO srcAETemplateDO,
	    AccountingEntryTemplateDO tgtAETemplateDO,ArrayList srcTgtCBTSettlementList, Connection conn) throws LMSException{
    	//JTest changes on 01-feb-11 starts
    	if(logger.isDebugEnabled())
    		logger.debug("Inside isHostsExistAsSettlement..........");
    	
		PreparedStatement stmt = null;
		//JTest changes on 01-feb-11 ends
		ResultSet rs = null;
		
		//PreparedStatement pstmt=null;
		//ResultSet rsDays = null;
		
		boolean isSettlementExist = false;
		try{
		    String entity1 = "";
		    String entity2 = "";
		    
		    String codCcy = srcAETemplateDO.getCodCcy();
		    
		    if("HostSystem".equals(ExecutionConstants.ENTITY_TYPE)){
			entity1 = srcAETemplateDO.getCodGl();
			entity2 = tgtAETemplateDO.getCodGl();
		    }else if("BLE".equals(ExecutionConstants.ENTITY_TYPE)){
			entity1 = srcAETemplateDO.getCodBnkLglEnt();
			entity2 = tgtAETemplateDO.getCodBnkLglEnt();
		    }
		    //ENH_Nostroloro_branch start
		    else if(ExecutionConstants.NOSTRO_LORO_BRANCH.equals(ExecutionConstants.ENTITY_TYPE)){
				entity1 = srcAETemplateDO.getCodBranch();
				entity2 = tgtAETemplateDO.getCodBranch();
			}
		    //ENH_Nostroloro_branch end
		   /* String sql = "SELECT OND.*, OA1.COD_BNK_LGL COD_BNK_LGL_SSP, OA1.COD_COUNTRY COD_COUNTRY_SSP," +
		    	"OA1.NBR_COREACCOUNT NBR_COREACCOUNT_SSP, OA1.COD_BRANCH COD_BRANCH_SSP, " +
		    	"OA2.COD_BNK_LGL COD_BNK_LGL_SO, OA2.COD_COUNTRY COD_COUNTRY_SO, " +
		    	"OA2.NBR_COREACCOUNT NBR_COREACCOUNT_SO, OA2.COD_BRANCH COD_BRANCH_SO " +
		    	"FROM OLM_NOSTROLORO_MST ONM, OLM_NOSTROLORO_DTLS OND, ORBICASH_ACCOUNTS OA1, " +
		    	"ORBICASH_ACCOUNTS OA2 WHERE ONM.NBR_CONTRACT_NO = OND.NBR_CONTRACT_NO " +
		    	"AND ONM.COD_LINKAGE_TYPE = '"+ExecutionConstants.SETTLEMENT+"' AND ONM.TYP_ENTITY = '"+ExecutionConstants.ENTITY_TYPE+"' AND " +
		    	"((OND.COD_ENTITY_NOSTRO_SETTLEMENT = '"+entity1+"' AND OND.COD_ENTITY_LORO_SETTLEMENT = '"+entity2+"') " +
		    	" OR (OND.COD_ENTITY_LORO_SETTLEMENT = '"+entity1+"' AND OND.COD_ENTITY_NOSTRO_SETTLEMENT = '"+entity2+"')) " +
		    	"AND OND.COD_CCY = '"+codCcy+"' AND OND.NBR_ACCNO_MAIN_SETTELMENT = OA1.NBR_OWNACCOUNT " +
		    	"AND OND.NBR_ACCTNO_MIRROR_SETTELMENT = OA2.NBR_OWNACCOUNT "; */
		  //JTest changes on 01-feb-11 starts
		    /*String sql = "SELECT OND.*, OA1.COD_BNK_LGL COD_BNK_LGL_SSP, OA1.COD_GL COD_GL_SSP, OA1.COD_COUNTRY COD_COUNTRY_SSP," +
		    "OA1.NBR_COREACCOUNT NBR_COREACCOUNT_SSP, OA1.COD_BRANCH COD_BRANCH_SSP, " +
		    "OA2.COD_BNK_LGL COD_BNK_LGL_SO, OA2.COD_GL COD_GL_SO, OA2.COD_COUNTRY COD_COUNTRY_SO, " +
		    "OA2.NBR_COREACCOUNT NBR_COREACCOUNT_SO, OA2.COD_BRANCH COD_BRANCH_SO " +
		    "FROM OLM_NOSTROLORO_MST ONM, OLM_NOSTROLORO_DTLS OND, ORBICASH_ACCOUNTS OA1, " +
		    "ORBICASH_ACCOUNTS OA2 WHERE ONM.NBR_CONTRACT_NO = OND.NBR_CONTRACT_NO " +
		    "AND ONM.COD_LINKAGE_TYPE = '"+ExecutionConstants.SETTLEMENT+"' AND ONM.TYP_ENTITY = '"+ExecutionConstants.ENTITY_TYPE+"' AND " +
		    //Jiten - BILATERALSETTLEMENT - Start
		    " ONM.FLG_CLOSED = '"+ExecutionConstants.NO+"' AND " +
		    //Jiten - BILATERALSETTLEMENT - End
		    "((OND.COD_ENTITY_NOSTRO_SETTLEMENT = '"+entity1+"' AND OND.COD_ENTITY_LORO_SETTLEMENT = '"+entity2+"') " +
		    " OR (OND.COD_ENTITY_LORO_SETTLEMENT = '"+entity1+"' AND OND.COD_ENTITY_NOSTRO_SETTLEMENT = '"+entity2+"')) " +
		    "AND OND.COD_CCY = '"+codCcy+"' AND OND.NBR_ACCNO_MAIN_SETTELMENT = OA1.NBR_OWNACCOUNT " +
		    "AND OND.NBR_ACCTNO_MIRROR_SETTELMENT = OA2.NBR_OWNACCOUNT " +
		    "AND (SELECT MAX(DAT_TODAY) + 1 FROM OLM_HOSTSYSTEMDATES WHERE COD_GL IN(OA1.COD_GL,OA2.COD_GL)) " +
		    "BETWEEN OND.DAT_EFFECTIVE AND NVL( OND.DAT_END , TO_DATE('31-DEC-9999','DD-MON-YYYY'))";*/
		    String sql = "SELECT OND.*, OA1.COD_BNK_LGL COD_BNK_LGL_SSP, OA1.COD_GL COD_GL_SSP, OA1.COD_COUNTRY COD_COUNTRY_SSP," +
		    "OA1.NBR_COREACCOUNT NBR_COREACCOUNT_SSP, OA1.COD_BRANCH COD_BRANCH_SSP, " +
		    "OA2.COD_BNK_LGL COD_BNK_LGL_SO, OA2.COD_GL COD_GL_SO, OA2.COD_COUNTRY COD_COUNTRY_SO, " +
		    "OA2.NBR_COREACCOUNT NBR_COREACCOUNT_SO, OA2.COD_BRANCH COD_BRANCH_SO " +
		    "FROM OLM_NOSTROLORO_MST ONM, OLM_NOSTROLORO_DTLS OND, ORBICASH_ACCOUNTS OA1, " +
		    "ORBICASH_ACCOUNTS OA2 WHERE ONM.NBR_CONTRACT_NO = OND.NBR_CONTRACT_NO " +
		    "AND ONM.COD_LINKAGE_TYPE = ? AND ONM.TYP_ENTITY = ? AND " +
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
	    //Jiten - BILATERALSETTLEMENT - Start
		    " ONM.FLG_CLOSED = ? AND " +
	    //Jiten - BILATERALSETTLEMENT - End
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		    "((OND.COD_ENTITY_NOSTRO_SETTLEMENT = ? AND OND.COD_ENTITY_LORO_SETTLEMENT = ?) " +
		    " OR (OND.COD_ENTITY_LORO_SETTLEMENT = ? AND OND.COD_ENTITY_NOSTRO_SETTLEMENT = ?)) " +
		    "AND OND.COD_CCY = ? AND OND.NBR_ACCNO_MAIN_SETTELMENT = OA1.NBR_OWNACCOUNT " +
		    "AND OND.NBR_ACCTNO_MIRROR_SETTELMENT = OA2.NBR_OWNACCOUNT " +
		    "AND (SELECT MAX(DAT_TODAY) + 1 FROM OLM_HOSTSYSTEMDATES WHERE COD_GL IN(OA1.COD_GL,OA2.COD_GL)) " +
		    "BETWEEN OND.DAT_EFFECTIVE AND NVL( OND.DAT_END , TO_DATE('31-DEC-9999','DD-MON-YYYY'))";
		    if(logger.isDebugEnabled())
		    	logger.debug("sql :: "+sql);
	
		    stmt = conn.prepareStatement(sql);
		    stmt.setString(1, ExecutionConstants.SETTLEMENT);
		    stmt.setString(2, ExecutionConstants.ENTITY_TYPE);
		    stmt.setString(3, ExecutionConstants.NO);
		    stmt.setString(4, entity1);
		    stmt.setString(5, entity2);
		    stmt.setString(6, entity1);
		    stmt.setString(7, entity2);
		    stmt.setString(8, codCcy);
		    rs = stmt.executeQuery();
		    //JTest changes on 01-feb-11 ends
	    if(rs.next()){
	    	isSettlementExist = true;

	    /*	StringBuffer sbuff= new StringBuffer();
	    	String settlementDays=null;
	    	sbuff.setLength(0);
	    	sbuff.append("SELECT ond.NBR_SETTLEMENT_DAYS FROM olm_nostroloro_mst onm, olm_nostroloro_dtls ond, orbicash_accounts oa1, orbicash_accounts oa2 ");
	    	sbuff.append("WHERE onm.nbr_contract_no = ond.nbr_contract_no AND onm.cod_linkage_type = ? AND onm.typ_entity = ? AND  ");
	    	sbuff.append("( ond.cod_entity_nostro_settlement = ? AND ond.cod_entity_loro_settlement = ?  ) AND ond.cod_ccy = ? AND  ");
	    	sbuff.append("ond.nbr_accno_main_settelment = oa1.nbr_ownaccount AND ond.nbr_acctno_mirror_settelment = oa2.nbr_ownaccount AND (SELECT MAX (dat_today) + 1 ");
	    	sbuff.append("FROM olm_hostsystemdates  WHERE cod_gl IN (oa1.cod_gl, oa2.cod_gl)) BETWEEN ond.dat_effective AND NVL (ond.dat_end, TO_DATE ('31-DEC-9999', 'DD-MON-YYYY' )) ");

	    	if(logger.isDebugEnabled())
	    	{
	    		logger.debug("Query for calculating the settlement days :: "+sbuff.toString());
	    		logger.debug(" settlement Host is :: "+rs.getString("COD_ENTITY_NOSTRO_SETTLEMENT"));
	    		logger.debug("cod Triggering Gl is : "+tgtAETemplateDO.getCodTriggeringGl());
	    		logger.debug("Currency is : "+codCcy);
	    	}

	    	pstmt = conn.prepareStatement(sbuff.toString());

	    	pstmt.setString(1,ExecutionConstants.NLEG_CENTRAL);
	    	pstmt.setString(2,ExecutionConstants.ENTITY_TYPE);
	    	pstmt.setString(3,rs.getString("COD_ENTITY_NOSTRO_SETTLEMENT"));
	    	pstmt.setString(4,tgtAETemplateDO.getCodTriggeringGl());
	    	pstmt.setString(5,codCcy);

	    	rsDays= pstmt.executeQuery();

	    	if(rsDays.next())
	    	{
	    		settlementDays= rsDays.getString("NBR_SETTLEMENT_DAYS");
	    		if(logger.isDebugEnabled())
	    			logger.debug("settlementDays is :::"+settlementDays);
	    	}*/

	    	AccountingEntryTemplateDO mainSetAETemplateDO =  new AccountingEntryTemplateDO();
	    	AccountingEntryTemplateDO mirrorSetAETemplateDO =  new AccountingEntryTemplateDO();

	    	mirrorSetAETemplateDO.setNbrOwnAccount(rs.getString("NBR_ACCTNO_MIRROR_SETTELMENT"));
	    	mirrorSetAETemplateDO.setNbrCoreAccount(rs.getString("NBR_COREACCOUNT_SO"));
	    	mirrorSetAETemplateDO.setCodBnkLglEnt(rs.getString("COD_BNK_LGL_SO"));
	    	mirrorSetAETemplateDO.setCodCcy(codCcy);
	    	mirrorSetAETemplateDO.setCodCountry(rs.getString("COD_COUNTRY_SO"));
	    	//mirrorSetAETemplateDO.setCodGl(rs.getString("COD_ENTITY_LORO_SETTLEMENT"));
	    	mirrorSetAETemplateDO.setCodGl(rs.getString("COD_GL_SO"));
	    	mirrorSetAETemplateDO.setFlgAcctType("I");
	    	mirrorSetAETemplateDO.setFlgSrcTgtTyp("S");
	    	mirrorSetAETemplateDO.setFlgSettlementEntry("Y");
	    	mirrorSetAETemplateDO.setNbrTemplateId(tgtAETemplateDO.getNbrTemplateId());
	    	mirrorSetAETemplateDO.setNbrContractNo(rs.getString("NBR_CONTRACT_NO"));
	    	mirrorSetAETemplateDO.setNbrSeqNo(rs.getString("NBR_SEQ"));
	    	mirrorSetAETemplateDO.setFlgPersist("Y");
	    	//mirrorSetAETemplateDO.setNbrSettlementDays(rs.getString("NBR_SETTLEMENT_DAYS"));
	    	mirrorSetAETemplateDO.setNbrSettlementDays(srcAETemplateDO.getNbrSettlementDays());

	    	if(tgtAETemplateDO.getDateEffective() != null && tgtAETemplateDO.getDateEffective().compareTo(rs.getDate("DAT_EFFECTIVE")) > 0){
	    		mirrorSetAETemplateDO.setDateEffective(tgtAETemplateDO.getDateEffective());
	    	}else{
	    		mirrorSetAETemplateDO.setDateEffective(rs.getDate("DAT_EFFECTIVE"));
	    	}

	    	mirrorSetAETemplateDO.setFlgDrcr(srcAETemplateDO.getFlgDrcr());
	    	//mirrorSetAETemplateDO.setCodTriggeringGl(rs.getString("COD_ENTITY_LORO"));
	    	mirrorSetAETemplateDO.setCodTriggeringGl(tgtAETemplateDO.getCodTriggeringGl());
	    	mirrorSetAETemplateDO.setCodBranch(rs.getString("COD_BRANCH_SO"));
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
	    	
	    	//Jiten - BILATERALSETTLEMENT - Start
	    	mirrorSetAETemplateDO.setFlgSettlementPath(srcAETemplateDO.getFlgSettlementPath());
	    	mirrorSetAETemplateDO.setDateEnd(rs.getDate("DAT_END"));
	    	//Jiten - BILATERALSETTLEMENT - End
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
	    	mainSetAETemplateDO.setNbrOwnAccount(rs.getString("NBR_ACCNO_MAIN_SETTELMENT"));
	    	mainSetAETemplateDO.setNbrCoreAccount(rs.getString("NBR_COREACCOUNT_SSP"));
	    	mainSetAETemplateDO.setCodBnkLglEnt(rs.getString("COD_BNK_LGL_SSP"));
	    	mainSetAETemplateDO.setCodCcy(codCcy);
	    	mainSetAETemplateDO.setCodCountry(rs.getString("COD_COUNTRY_SSP"));
	    	//mainSetAETemplateDO.setCodGl(rs.getString("COD_ENTITY_NOSTRO_SETTLEMENT"));
	    	mainSetAETemplateDO.setCodGl(rs.getString("COD_GL_SSP"));
	    	mainSetAETemplateDO.setFlgAcctType("I");
	    	mainSetAETemplateDO.setFlgSrcTgtTyp("T");
	    	mainSetAETemplateDO.setFlgSettlementEntry("Y");
	    	mainSetAETemplateDO.setNbrTemplateId(srcAETemplateDO.getNbrTemplateId());
	    	mainSetAETemplateDO.setNbrContractNo(rs.getString("NBR_CONTRACT_NO"));
	    	mainSetAETemplateDO.setNbrSeqNo(rs.getString("NBR_SEQ"));
	    	mainSetAETemplateDO.setFlgPersist("Y");
	    	//mainSetAETemplateDO.setNbrSettlementDays(rs.getString("NBR_SETTLEMENT_DAYS"));
	    	mainSetAETemplateDO.setNbrSettlementDays(tgtAETemplateDO.getNbrSettlementDays());
	    	if(srcAETemplateDO.getDateEffective() != null && srcAETemplateDO.getDateEffective().compareTo(rs.getDate("DAT_EFFECTIVE")) > 0){
	    		mainSetAETemplateDO.setDateEffective(srcAETemplateDO.getDateEffective());
	    	}else{
	    		mainSetAETemplateDO.setDateEffective(rs.getDate("DAT_EFFECTIVE"));
	    	}

	    	mainSetAETemplateDO.setFlgDrcr(srcAETemplateDO.getFlgDrcr());
	    	//mainSetAETemplateDO.setCodTriggeringGl(rs.getString("COD_ENTITY_LORO"));
	    	mainSetAETemplateDO.setCodTriggeringGl(srcAETemplateDO.getCodTriggeringGl());
	    	mainSetAETemplateDO.setCodBranch(rs.getString("COD_BRANCH_SSP"));
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
	    	
	    	//Jiten - BILATERALSETTLEMENT - Start
	    	mainSetAETemplateDO.setFlgSettlementPath(srcAETemplateDO.getFlgSettlementPath());
	    	mainSetAETemplateDO.setDateEnd(rs.getDate("DAT_END"));
	    	//Jiten - BILATERALSETTLEMENT - End
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
	    	
	    	if(entity1.equalsIgnoreCase(rs.getString("COD_ENTITY_NOSTRO_SETTLEMENT"))){
	    		srcTgtCBTSettlementList.add(mirrorSetAETemplateDO);
	    		srcTgtCBTSettlementList.add(mainSetAETemplateDO);
	    	}else{
	    		srcTgtCBTSettlementList.add(mainSetAETemplateDO);
	    		srcTgtCBTSettlementList.add(mirrorSetAETemplateDO);
	    	}
	    }
	    
	}catch(Exception e){
	    logger.fatal("Exeception :: "+e);
	    throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, "", e);
	}
	finally{
		//JTest changes on 01-feb-11 start
		LMSConnectionUtility.close(rs);
		LMSConnectionUtility.close(stmt);
		//JTest changes on 01-feb-11 ends
	}
		
	if(logger.isDebugEnabled())//JTest changes on 01-feb-11
		logger.debug("Leaving isHostsExistAsSettlement..........");
		
	return isSettlementExist;
    }
    /**
     * 
     * @param srcAETemplateDO
     * @param tgtAETemplateDO
     * @param srcTgtCBTAccountList
     * @param srcTgtCBTSettlementList
     * @return
     * @throws LMSException
     */
    private boolean isHostsExistAsCBT(AccountingEntryTemplateDO srcAETemplateDO,
	    AccountingEntryTemplateDO tgtAETemplateDO,
	    ArrayList srcTgtCBTAccountList,ArrayList srcTgtCBTSettlementList, Connection conn) throws LMSException{
    	//JTest changes on 01-feb-11 starts
    	if(logger.isDebugEnabled())
    		logger.debug("isHostsExistAsCBT....");
		
    	PreparedStatement stmt = null;
    	//JTest changes on 01-feb-11 ends
		ResultSet rs = null;
		boolean isCBTExist = false;
	
		try{
		    String entity1 = "";
		    String entity2 = "";
	
		    String codCcy = srcAETemplateDO.getCodCcy();
	
		    if("HostSystem".equals(ExecutionConstants.ENTITY_TYPE)){
			entity1 = srcAETemplateDO.getCodGl();
			entity2 = tgtAETemplateDO.getCodGl();
		    }else if("BLE".equals(ExecutionConstants.ENTITY_TYPE)){
			entity1 = srcAETemplateDO.getCodBnkLglEnt();
			entity2 = tgtAETemplateDO.getCodBnkLglEnt();
		    }
		    //ENH_Nostroloro_branch start
		    else if(ExecutionConstants.NOSTRO_LORO_BRANCH.equals(ExecutionConstants.ENTITY_TYPE)){
				entity1 = srcAETemplateDO.getCodBranch();
				entity2 = tgtAETemplateDO.getCodBranch();
			}
		    //ENH_Nostroloro_branch end
	
		  /*  String sql = "SELECT OND.*, OA1.COD_BNK_LGL COD_BNK_LGL_NSP, OA1.COD_COUNTRY COD_COUNTRY_NSP, " +
		    "OA1.NBR_COREACCOUNT NBR_COREACCOUNT_NSP,OA1.COD_BRANCH COD_BRANCH_NSP, OA2.COD_BNK_LGL COD_BNK_LGL_NO, " +
		    "OA2.COD_COUNTRY COD_COUNTRY_NO, OA2.NBR_COREACCOUNT NBR_COREACCOUNT_NO,OA2.COD_BRANCH COD_BRANCH_NO, " +
		    "OA3.COD_BNK_LGL COD_BNK_LGL_MAIN, OA3.COD_COUNTRY COD_COUNTRY_MAIN, " +
		    "OA3.NBR_COREACCOUNT NBR_COREACCOUNT_MAIN,OA3.COD_BRANCH COD_BRANCH_MAIN, OA4.COD_BNK_LGL COD_BNK_LGL_MIRROR, " +
		    "OA4.COD_COUNTRY COD_COUNTRY_MIRROR, OA4.NBR_COREACCOUNT NBR_COREACCOUNT_MIRROR,OA4.COD_BRANCH COD_BRANCH_MIRROR  " +
		    "FROM OLM_NOSTROLORO_MST ONM, OLM_NOSTROLORO_DTLS OND, ORBICASH_ACCOUNTS OA1, " +
		    "ORBICASH_ACCOUNTS OA2, ORBICASH_ACCOUNTS OA3, ORBICASH_ACCOUNTS OA4 " +
		    "WHERE ONM.NBR_CONTRACT_NO = OND.NBR_CONTRACT_NO " +
		    "AND ONM.COD_LINKAGE_TYPE = '"+ExecutionConstants.NLEG_CENTRAL+"' " +
		    "AND ONM.TYP_ENTITY = '"+ExecutionConstants.ENTITY_TYPE+"' " +
		    "AND OND.COD_ENTITY_NOSTRO  = '"+entity2+"' "+
		    "AND OND.COD_ENTITY_LORO = '"+entity1+"' "+
		    "AND OND.COD_CCY = '"+codCcy+"' AND OND.NBR_ACCTNO_NOSTRO = OA1.NBR_OWNACCOUNT " +
		    "AND OND.NBR_ACCTNO_LORO = OA2.NBR_OWNACCOUNT  " +
		    "AND OND.NBR_ACCNO_MAIN_SETTELMENT = OA3.NBR_OWNACCOUNT(+) " +
		    "AND OND.NBR_ACCTNO_MIRROR_SETTELMENT = OA4.NBR_OWNACCOUNT(+) ";*/
		    
		    String sql = "SELECT OND.*, OA1.COD_BNK_LGL COD_BNK_LGL_NSP, OA1.COD_GL COD_GL_NSP, OA1.COD_COUNTRY COD_COUNTRY_NSP, " +
		    "OA1.NBR_COREACCOUNT NBR_COREACCOUNT_NSP,OA1.COD_BRANCH COD_BRANCH_NSP, OA2.COD_BNK_LGL COD_BNK_LGL_NO, OA2.COD_GL COD_GL_NO, " +
		    "OA2.COD_COUNTRY COD_COUNTRY_NO, OA2.NBR_COREACCOUNT NBR_COREACCOUNT_NO,OA2.COD_BRANCH COD_BRANCH_NO, " +
		    "OA3.COD_BNK_LGL COD_BNK_LGL_MAIN, OA3.COD_GL COD_GL_MAIN, OA3.COD_COUNTRY COD_COUNTRY_MAIN, " +
		    "OA3.NBR_COREACCOUNT NBR_COREACCOUNT_MAIN,OA3.COD_BRANCH COD_BRANCH_MAIN, OA4.COD_BNK_LGL COD_BNK_LGL_MIRROR, OA4.COD_GL COD_GL_MIRROR, " +
		    "OA4.COD_COUNTRY COD_COUNTRY_MIRROR, OA4.NBR_COREACCOUNT NBR_COREACCOUNT_MIRROR,OA4.COD_BRANCH COD_BRANCH_MIRROR  " +
		    "FROM OLM_NOSTROLORO_MST ONM, OLM_NOSTROLORO_DTLS OND, ORBICASH_ACCOUNTS OA1, " +
		    "ORBICASH_ACCOUNTS OA2, ORBICASH_ACCOUNTS OA3, ORBICASH_ACCOUNTS OA4 " +
		    "WHERE ONM.NBR_CONTRACT_NO = OND.NBR_CONTRACT_NO " +
		    /*"AND ONM.COD_LINKAGE_TYPE = '"+ExecutionConstants.NLEG_CENTRAL+"' " +
		    "AND ONM.TYP_ENTITY = '"+ExecutionConstants.ENTITY_TYPE+"' " +
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		    //Jiten - BILATERALSETTLEMENT - Start
		    "AND ONM.FLG_CLOSED = '"+ExecutionConstants.NO+"' " +
		    //Jiten - BILATERALSETTLEMENT - End
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		    "AND OND.COD_ENTITY_NOSTRO  = '"+entity2+"' "+
		    "AND OND.COD_ENTITY_LORO = '"+entity1+"' "+
		    "AND OND.COD_CCY = '"+codCcy+"' AND OND.NBR_ACCTNO_NOSTRO = OA1.NBR_OWNACCOUNT " +*/
		    "AND ONM.COD_LINKAGE_TYPE in (?,?) " +
		    "AND ONM.TYP_ENTITY = ? " +
		    //Jiten - BILATERALSETTLEMENT - Start
		    "AND ONM.FLG_CLOSED = ? " +
		    //Jiten - BILATERALSETTLEMENT - End
		    "AND OND.COD_ENTITY_NOSTRO  = ? "+
		    "AND OND.COD_ENTITY_LORO = ? "+
		    "AND OND.COD_CCY = ? AND OND.NBR_ACCTNO_NOSTRO = OA1.NBR_OWNACCOUNT " +
		    "AND OND.NBR_ACCTNO_LORO = OA2.NBR_OWNACCOUNT  " +
		    "AND OND.NBR_ACCNO_MAIN_SETTELMENT = OA3.NBR_OWNACCOUNT(+) " +
		    "AND OND.NBR_ACCTNO_MIRROR_SETTELMENT = OA4.NBR_OWNACCOUNT(+) " +
		    "AND (SELECT MAX(DAT_TODAY)+ 1 FROM OLM_HOSTSYSTEMDATES WHERE COD_GL IN(OA1.COD_GL,OA2.COD_GL)) " +
		    "BETWEEN OND.DAT_EFFECTIVE AND NVL( OND.DAT_END , TO_DATE('31-DEC-9999','DD-MON-YYYY')) ";
		  //JTest changes on 01-feb-11 starts
		    if(logger.isDebugEnabled())
		    	logger.debug("sql :: "+sql);
	
		    stmt = conn.prepareStatement(sql);
		    //HSBC_OSL_2643_CR_ICL_INTR Start
		
		    	stmt.setString(1, ExecutionConstants.NLEG_CENTRAL);
		 
		    	stmt.setString(2, ExecutionConstants.CENTRAL_TREASURY);
		    //HSBC_OSL_2643_CR_ICL_INTR End
		    stmt.setString(3, ExecutionConstants.ENTITY_TYPE);
		    stmt.setString(4, ExecutionConstants.NO);
		    stmt.setString(5, entity2);
		    stmt.setString(6, entity1);
		    stmt.setString(7, codCcy);
		    rs = stmt.executeQuery();
		    //JTest changes on 01-feb-11 ends
	
		    if(rs.next()){
			isCBTExist = true;
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
			//Triggering GL Population from Nostro Linkage
			if(srcAETemplateDO.getCodTriggeringGl() == null || srcAETemplateDO.getCodTriggeringGl().equals("")){
			    srcAETemplateDO.setCodTriggeringGl(rs.getString("COD_GL_NO")); 
			    tgtAETemplateDO.setCodTriggeringGl(rs.getString("COD_GL_NO"));
			}
			//Triggering GL Population from Nostro Linkage
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
			AccountingEntryTemplateDO nspAETemplateDO =  new AccountingEntryTemplateDO();
			AccountingEntryTemplateDO noAETemplateDO =  new AccountingEntryTemplateDO();
	
			noAETemplateDO.setNbrOwnAccount(rs.getString("NBR_ACCTNO_LORO"));
			noAETemplateDO.setNbrCoreAccount(rs.getString("NBR_COREACCOUNT_NO"));
			noAETemplateDO.setCodBnkLglEnt(rs.getString("COD_BNK_LGL_NO"));
			noAETemplateDO.setCodCcy(codCcy);
			noAETemplateDO.setCodCountry(rs.getString("COD_COUNTRY_NO"));
			//noAETemplateDO.setCodGl(rs.getString("COD_ENTITY_LORO"));
			noAETemplateDO.setCodGl(rs.getString("COD_GL_NO"));
			noAETemplateDO.setFlgPersist("Y");
			noAETemplateDO.setNbrTemplateId(srcAETemplateDO.getNbrTemplateId());
			noAETemplateDO.setNbrContractNo(rs.getString("NBR_CONTRACT_NO"));
			noAETemplateDO.setNbrSeqNo(rs.getString("NBR_SEQ"));
			noAETemplateDO.setFlgSettlementEntry("N");
			noAETemplateDO.setNbrSettlementDays(rs.getString("NBR_SETTLEMENT_DAYS"));
			if(srcAETemplateDO.getDateEffective() != null && srcAETemplateDO.getDateEffective().compareTo(rs.getDate("DAT_EFFECTIVE")) > 0){
			    noAETemplateDO.setDateEffective(srcAETemplateDO.getDateEffective());
			}else{
			    noAETemplateDO.setDateEffective(rs.getDate("DAT_EFFECTIVE"));
			}
			
			if(srcAETemplateDO.getFlgDrcr().equalsIgnoreCase(ExecutionConstants.CREDIT))
			    noAETemplateDO.setFlgDrcr(ExecutionConstants.DEBIT);
			else
			    noAETemplateDO.setFlgDrcr(ExecutionConstants.CREDIT);
			noAETemplateDO.setFlgAcctType("I");
			noAETemplateDO.setFlgSrcTgtTyp("S");
			//noAETemplateDO.setCodTriggeringGl(rs.getString("COD_GL_NO"));
			noAETemplateDO.setCodTriggeringGl(srcAETemplateDO.getCodTriggeringGl());
			noAETemplateDO.setCodBranch(rs.getString("COD_BRANCH_NO"));
			noAETemplateDO.setDateEnd(rs.getDate("DAT_END"));
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
			//Jiten - BILATERALSETTLEMENT - Start
			//noAETemplateDO.setFlgSettlementPath("C");
			//Jiten - BILATERALSETTLEMENT - End
	
			//- DeLocalize Settlement path - Start
			if(rs.getString("COD_ENTITY_NOSTRO").equalsIgnoreCase
			    (rs.getString("COD_ENTITY_NOSTRO_SETTLEMENT")) ){
			    noAETemplateDO.setFlgSettlementPath("C");
			}else{
			    noAETemplateDO.setFlgSettlementPath("D");
			}
			//- DeLocalize Settlement path - End
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
			nspAETemplateDO.setNbrOwnAccount(rs.getString("NBR_ACCTNO_NOSTRO"));
			nspAETemplateDO.setNbrCoreAccount(rs.getString("NBR_COREACCOUNT_NSP"));
			nspAETemplateDO.setCodBnkLglEnt(rs.getString("COD_BNK_LGL_NSP"));
			nspAETemplateDO.setCodCcy(codCcy);
			nspAETemplateDO.setCodCountry(rs.getString("COD_COUNTRY_NSP"));
		//	nspAETemplateDO.setCodGl(rs.getString("COD_ENTITY_NOSTRO"));
			nspAETemplateDO.setCodGl(rs.getString("COD_GL_NSP"));
			nspAETemplateDO.setFlgPersist("Y");
			nspAETemplateDO.setNbrTemplateId(tgtAETemplateDO.getNbrTemplateId());
			nspAETemplateDO.setNbrContractNo(rs.getString("NBR_CONTRACT_NO"));
			nspAETemplateDO.setNbrSeqNo(rs.getString("NBR_SEQ"));
			nspAETemplateDO.setFlgSettlementEntry("N");
			nspAETemplateDO.setNbrSettlementDays(rs.getString("NBR_SETTLEMENT_DAYS"));
			if(tgtAETemplateDO.getDateEffective() != null && tgtAETemplateDO.getDateEffective().compareTo(rs.getDate("DAT_EFFECTIVE")) > 0){
			    nspAETemplateDO.setDateEffective(tgtAETemplateDO.getDateEffective());
			}else{
			    nspAETemplateDO.setDateEffective(rs.getDate("DAT_EFFECTIVE"));
			}
			
			if(tgtAETemplateDO.getFlgDrcr().equalsIgnoreCase(ExecutionConstants.CREDIT))
			    nspAETemplateDO.setFlgDrcr(ExecutionConstants.DEBIT);
			else
			    nspAETemplateDO.setFlgDrcr(ExecutionConstants.CREDIT);
			nspAETemplateDO.setFlgAcctType("I");
			nspAETemplateDO.setFlgSrcTgtTyp("T");
			//nspAETemplateDO.setCodTriggeringGl(rs.getString("COD_GL_NO"));
			nspAETemplateDO.setCodTriggeringGl(srcAETemplateDO.getCodTriggeringGl());
			nspAETemplateDO.setCodBranch(rs.getString("COD_BRANCH_NSP"));
			nspAETemplateDO.setDateEnd(rs.getDate("DAT_END"));
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
			//Jiten - BILATERALSETTLEMENT - Start
			//nspAETemplateDO.setFlgSettlementPath("C");
			//Jiten - BILATERALSETTLEMENT - End
	
			//- DeLocalize Settlement path - Start
			if(rs.getString("COD_ENTITY_NOSTRO").equalsIgnoreCase
			    (rs.getString("COD_ENTITY_NOSTRO_SETTLEMENT")) ){
			    nspAETemplateDO.setFlgSettlementPath("C");
			}else{
			    nspAETemplateDO.setFlgSettlementPath("D");
			}
			//- DeLocalize Settlement path - End
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
			srcTgtCBTAccountList.add(noAETemplateDO);
			srcTgtCBTAccountList.add(nspAETemplateDO);
	
			AccountingEntryTemplateDO mainSetAETemplateDO =  new AccountingEntryTemplateDO();
			AccountingEntryTemplateDO mirrorSetAETemplateDO =  new AccountingEntryTemplateDO();
	
			mirrorSetAETemplateDO.setNbrOwnAccount(rs.getString("NBR_ACCTNO_MIRROR_SETTELMENT"));
			mirrorSetAETemplateDO.setNbrCoreAccount(rs.getString("NBR_COREACCOUNT_MIRROR"));
			mirrorSetAETemplateDO.setCodBnkLglEnt(rs.getString("COD_BNK_LGL_MIRROR"));
			mirrorSetAETemplateDO.setCodCcy(codCcy);
			mirrorSetAETemplateDO.setCodCountry(rs.getString("COD_COUNTRY_MIRROR"));
			//mirrorSetAETemplateDO.setCodGl(rs.getString("COD_ENTITY_LORO_SETTLEMENT"));
			mirrorSetAETemplateDO.setCodGl(rs.getString("COD_GL_MIRROR"));
			mirrorSetAETemplateDO.setFlgAcctType("I");
			mirrorSetAETemplateDO.setFlgSrcTgtTyp("S");
			mirrorSetAETemplateDO.setFlgSettlementEntry("Y");
			mirrorSetAETemplateDO.setNbrTemplateId(tgtAETemplateDO.getNbrTemplateId());
			mirrorSetAETemplateDO.setNbrContractNo(rs.getString("NBR_CONTRACT_NO"));
			mirrorSetAETemplateDO.setNbrSeqNo(rs.getString("NBR_SEQ"));
			mirrorSetAETemplateDO.setFlgPersist("Y");
			mirrorSetAETemplateDO.setNbrSettlementDays(rs.getString("NBR_SETTLEMENT_DAYS"));
			if(tgtAETemplateDO.getDateEffective() != null && tgtAETemplateDO.getDateEffective().compareTo(rs.getDate("DAT_EFFECTIVE")) > 0){
			    mirrorSetAETemplateDO.setDateEffective(tgtAETemplateDO.getDateEffective());
			}else{
			    mirrorSetAETemplateDO.setDateEffective(rs.getDate("DAT_EFFECTIVE"));
			}
			
			mirrorSetAETemplateDO.setFlgDrcr(noAETemplateDO.getFlgDrcr());
			//mirrorSetAETemplateDO.setCodTriggeringGl(rs.getString("COD_GL_NO"));
			mirrorSetAETemplateDO.setCodTriggeringGl(srcAETemplateDO.getCodTriggeringGl());
			mirrorSetAETemplateDO.setCodBranch(rs.getString("COD_BRANCH_MIRROR"));
			mirrorSetAETemplateDO.setDateEnd(rs.getDate("DAT_END"));
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
			//Jiten - BILATERALSETTLEMENT - Start
			//mirrorSetAETemplateDO.setFlgSettlementPath("C");
			//Jiten - BILATERALSETTLEMENT - End
			
			//- DeLocalize Settlement path - Start
			if(rs.getString("COD_ENTITY_NOSTRO").equalsIgnoreCase
			    (rs.getString("COD_ENTITY_NOSTRO_SETTLEMENT")) ){
			    mirrorSetAETemplateDO.setFlgSettlementPath("C");
			}else{
			    mirrorSetAETemplateDO.setFlgSettlementPath("D");
			}
			//- DeLocalize Settlement path - End
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
			mainSetAETemplateDO.setNbrOwnAccount(rs.getString("NBR_ACCNO_MAIN_SETTELMENT"));
			mainSetAETemplateDO.setNbrCoreAccount(rs.getString("NBR_COREACCOUNT_MAIN"));
			mainSetAETemplateDO.setCodBnkLglEnt(rs.getString("COD_BNK_LGL_MAIN"));
			mainSetAETemplateDO.setCodCcy(codCcy);
			mainSetAETemplateDO.setCodCountry(rs.getString("COD_COUNTRY_MAIN"));
			//mainSetAETemplateDO.setCodGl(rs.getString("COD_ENTITY_NOSTRO_SETTLEMENT"));
			mainSetAETemplateDO.setCodGl(rs.getString("COD_GL_MAIN"));
			mainSetAETemplateDO.setFlgAcctType("I");
			mainSetAETemplateDO.setFlgSrcTgtTyp("T");
			mainSetAETemplateDO.setFlgSettlementEntry("Y");
			mainSetAETemplateDO.setNbrTemplateId(srcAETemplateDO.getNbrTemplateId());
			mainSetAETemplateDO.setNbrContractNo(rs.getString("NBR_CONTRACT_NO"));
			mainSetAETemplateDO.setNbrSeqNo(rs.getString("NBR_SEQ"));
			mainSetAETemplateDO.setFlgPersist("Y");
			mainSetAETemplateDO.setNbrSettlementDays(rs.getString("NBR_SETTLEMENT_DAYS"));
			if(srcAETemplateDO.getDateEffective() != null && srcAETemplateDO.getDateEffective().compareTo(rs.getDate("DAT_EFFECTIVE")) > 0){
			    mainSetAETemplateDO.setDateEffective(srcAETemplateDO.getDateEffective());
			}else{
			    mainSetAETemplateDO.setDateEffective(rs.getDate("DAT_EFFECTIVE"));
			}
			
			mainSetAETemplateDO.setFlgDrcr(nspAETemplateDO.getFlgDrcr());
			//mainSetAETemplateDO.setCodTriggeringGl(rs.getString("COD_GL_NO"));
			mainSetAETemplateDO.setCodTriggeringGl(srcAETemplateDO.getCodTriggeringGl());
			
			mainSetAETemplateDO.setCodBranch(rs.getString("COD_BRANCH_MAIN"));
			mainSetAETemplateDO.setDateEnd(rs.getDate("DAT_END"));
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
			//Jiten - BILATERALSETTLEMENT - Start
			//mainSetAETemplateDO.setFlgSettlementPath("C");
			//Jiten - BILATERALSETTLEMENT - End
	
			//- DeLocalize Settlement path - Start
			if(rs.getString("COD_ENTITY_NOSTRO").equalsIgnoreCase
			    (rs.getString("COD_ENTITY_NOSTRO_SETTLEMENT")) ){
			    mainSetAETemplateDO.setFlgSettlementPath("C");
			}else{
			    mainSetAETemplateDO.setFlgSettlementPath("D");
			}
			//- DeLocalize Settlement path - End
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
			srcTgtCBTSettlementList.add(mirrorSetAETemplateDO);
			srcTgtCBTSettlementList.add(mainSetAETemplateDO);
	
	
		    }
	
		}catch(Exception e){
		    throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, "", e);
		}
		finally{
			//JTest changes on 01-feb-11 start
			LMSConnectionUtility.close(rs);
			LMSConnectionUtility.close(stmt);
			//JTest changes on 01-feb-11 ends
		}
		
		if(logger.isDebugEnabled())//JTest changes on 01-feb-11
			logger.debug("isHostsExistAsCBT....Leaving");
		
	return isCBTExist;
    }

	//HSBC_DEF_FIX_14022011 -- Start (Added following method)    
    /**The following method  isSrcHostsExistAsCBT() was added to cater to the condition where SUB account 
     * acts as a CBT site / Nostro Service Provider, To maker this changes  
     * the query where condition  has changed to check entity 1 (SRC / SUB ) as 
     * Service provider / CBT and entity 2 as  Mirror/Loro. While adding the Nostro and mirror 
     * as below srcTgtCBTAccountList.add(nspAETemplateDO); srcTgtCBTAccountList.add(noAETemplateDO);
     * The Src Targert list adds the Service provider (SUB as CBT) first. This facilitates 
     * breaking the recursive calling from the location where this is called. The same is 
     * done for Settlement entries in the list srcTgtCBTSettlementList
     * Chnages to Narration and identification of entries that is sub, master ... are also done
     * 
     **/
    private boolean isSrcHostsExistAsCBT(
			AccountingEntryTemplateDO srcAETemplateDO,
			AccountingEntryTemplateDO tgtAETemplateDO,
			ArrayList srcTgtCBTAccountList, ArrayList srcTgtCBTSettlementList,
			Connection conn) throws LMSException {
    	logger.debug("isSrcHostsExistAsCBT....");
    	//JTest Changes 11.5.1.Starts
    	//Statement stmt = null;
    	PreparedStatement stmt = null;
    	//JTest Changes 11.5.1.Ends
    	ResultSet rs = null;
    	boolean isCBTExist = false;

    	try{
    	    String entity1 = "";
    	    String entity2 = "";
    	    String codCcy = srcAETemplateDO.getCodCcy();

    	    if("HostSystem".equals(ExecutionConstants.ENTITY_TYPE)){
	    		entity1 = srcAETemplateDO.getCodGl();
	    		entity2 = tgtAETemplateDO.getCodGl();
    	    }else if("BLE".equals(ExecutionConstants.ENTITY_TYPE)){
	    		entity1 = srcAETemplateDO.getCodBnkLglEnt();
	    		entity2 = tgtAETemplateDO.getCodBnkLglEnt();
    	    }
    	    //ENH_Nostroloro_branch start
    	    else if(ExecutionConstants.NOSTRO_LORO_BRANCH.equals(ExecutionConstants.ENTITY_TYPE)){
	    		entity1 = srcAETemplateDO.getCodBranch();
	    		entity2 = tgtAETemplateDO.getCodBranch();
    	    }
    	    //ENH_Nostroloro_branch end

    	    String sql = "SELECT OND.*, OA1.COD_BNK_LGL COD_BNK_LGL_NSP, OA1.COD_GL COD_GL_NSP, OA1.COD_COUNTRY COD_COUNTRY_NSP, " +
    	    "OA1.NBR_COREACCOUNT NBR_COREACCOUNT_NSP,OA1.COD_BRANCH COD_BRANCH_NSP, OA2.COD_BNK_LGL COD_BNK_LGL_NO, OA2.COD_GL COD_GL_NO, " +
    	    "OA2.COD_COUNTRY COD_COUNTRY_NO, OA2.NBR_COREACCOUNT NBR_COREACCOUNT_NO,OA2.COD_BRANCH COD_BRANCH_NO, " +
    	    "OA3.COD_BNK_LGL COD_BNK_LGL_MAIN, OA3.COD_GL COD_GL_MAIN, OA3.COD_COUNTRY COD_COUNTRY_MAIN, " +
    	    "OA3.NBR_COREACCOUNT NBR_COREACCOUNT_MAIN,OA3.COD_BRANCH COD_BRANCH_MAIN, OA4.COD_BNK_LGL COD_BNK_LGL_MIRROR, OA4.COD_GL COD_GL_MIRROR, " +
    	    "OA4.COD_COUNTRY COD_COUNTRY_MIRROR, OA4.NBR_COREACCOUNT NBR_COREACCOUNT_MIRROR,OA4.COD_BRANCH COD_BRANCH_MIRROR  " +
    	    "FROM OLM_NOSTROLORO_MST ONM, OLM_NOSTROLORO_DTLS OND, ORBICASH_ACCOUNTS OA1, " +
    	    "ORBICASH_ACCOUNTS OA2, ORBICASH_ACCOUNTS OA3, ORBICASH_ACCOUNTS OA4 " +
    	    "WHERE ONM.NBR_CONTRACT_NO = OND.NBR_CONTRACT_NO " +
    	  //JTest Changes 11.5.1.Starts
    	    //HSBC_OSL_2643_CR_ICL_INTR Start
    	    //"AND ONM.COD_LINKAGE_TYPE = '"+ExecutionConstants.NLEG_CENTRAL+"' " +
    	    //"AND ONM.COD_LINKAGE_TYPE  in ('"+ExecutionConstants.NLEG_CENTRAL+"','"+ExecutionConstants.CENTRAL_TREASURY+"') " +
    	    //HSBC_OSL_2643_CR_ICL_INTR End
    	    //"AND ONM.TYP_ENTITY = '"+ExecutionConstants.ENTITY_TYPE+"' " +
    	    //Jiten - BILATERALSETTLEMENT - Start
    	    //" AND ONM.FLG_CLOSED = '"+ExecutionConstants.NO+"' " +
    	    //Jiten - BILATERALSETTLEMENT - End
    	    //"AND (OND.COD_ENTITY_NOSTRO  = '"+entity1+"') "+
    	    //"AND (OND.COD_ENTITY_LORO = '"+entity2+"') "+
    	    //"AND OND.COD_CCY = '"+codCcy+"' AND OND.NBR_ACCTNO_NOSTRO = OA1.NBR_OWNACCOUNT " +
    	    "AND ONM.COD_LINKAGE_TYPE  in (?,?) " +
    	    "AND ONM.TYP_ENTITY = ? " +
    	   " AND ONM.FLG_CLOSED = ? " +
    	    "AND (OND.COD_ENTITY_NOSTRO  = ? ) "+
    	    "AND (OND.COD_ENTITY_LORO = ? ) "+
    	    "AND OND.COD_CCY = ? AND OND.NBR_ACCTNO_NOSTRO = OA1.NBR_OWNACCOUNT " +
    	    "AND OND.NBR_ACCTNO_LORO = OA2.NBR_OWNACCOUNT  " +
    	    "AND OND.NBR_ACCNO_MAIN_SETTELMENT = OA3.NBR_OWNACCOUNT(+) " +
    	    "AND OND.NBR_ACCTNO_MIRROR_SETTELMENT = OA4.NBR_OWNACCOUNT(+) " +
    	    "AND (SELECT MAX(DAT_TODAY)+ 1 FROM OLM_HOSTSYSTEMDATES WHERE COD_GL IN(OA1.COD_GL,OA2.COD_GL)) " +
    	    "BETWEEN OND.DAT_EFFECTIVE AND NVL( OND.DAT_END , TO_DATE('31-DEC-9999','DD-MON-YYYY')) ";
    	    if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
    	    	logger.debug("sql :: "+sql);
    	   // stmt = conn.createStatement();
    	   // rs = stmt.executeQuery(sql);
    	    stmt = conn.prepareStatement(sql);
    	    stmt.setString(1, ExecutionConstants.NLEG_CENTRAL);
 		   	stmt.setString(2, ExecutionConstants.CENTRAL_TREASURY);
	 	    stmt.setString(3, ExecutionConstants.ENTITY_TYPE);
	 	    stmt.setString(4, ExecutionConstants.NO);
	 	    stmt.setString(5, entity1);
	 	    stmt.setString(6, entity2);
	 	    stmt.setString(7, codCcy);
	 	    rs = stmt.executeQuery();
	 	 //JTest Changes 11.5.1.Ends
    	    if(rs.next()){
	    		isCBTExist = true;
	    		//PROD_DEF_FIX_20100915 Start : Triggering GL Population from Nostro Linkage
	    		if (srcAETemplateDO.getCodTriggeringGl() == null
						|| srcAETemplateDO.getCodTriggeringGl().equals("")) {
	    		    srcAETemplateDO.setCodTriggeringGl(rs.getString("COD_GL_NO")); 
	    		    tgtAETemplateDO.setCodTriggeringGl(rs.getString("COD_GL_NO"));
	    		}
	    		//PROD_DEF_FIX_20100915 End : Triggering GL Population from Nostro Linkage
	    		
	    		// HSBC_DEF_FIX Start : Suman
	    		String lContractNo = rs.getString("NBR_CONTRACT_NO");
	    		String lSeqNo = rs.getString("NBR_SEQ");
	    		if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
	    			logger.debug("lContractNo :: "+lContractNo+", lSeqNo :: "+lSeqNo);
	    		// HSBC_DEF_FIX End : Suman
	
	    		AccountingEntryTemplateDO nspAETemplateDO =  new AccountingEntryTemplateDO();
	    		AccountingEntryTemplateDO noAETemplateDO =  new AccountingEntryTemplateDO();
	
	    		noAETemplateDO.setNbrOwnAccount(rs.getString("NBR_ACCTNO_LORO"));
	    		noAETemplateDO.setNbrCoreAccount(rs.getString("NBR_COREACCOUNT_NO"));
	    		noAETemplateDO.setCodBnkLglEnt(rs.getString("COD_BNK_LGL_NO"));
	    		noAETemplateDO.setCodCcy(codCcy);
	    		noAETemplateDO.setCodCountry(rs.getString("COD_COUNTRY_NO"));
	    		noAETemplateDO.setCodGl(rs.getString("COD_GL_NO"));
	    		noAETemplateDO.setFlgPersist("Y");
	    		noAETemplateDO.setNbrTemplateId(srcAETemplateDO.getNbrTemplateId());
	    		noAETemplateDO.setNbrContractNo(rs.getString("NBR_CONTRACT_NO"));
	    		noAETemplateDO.setNbrSeqNo(rs.getString("NBR_SEQ"));
	    		noAETemplateDO.setFlgSettlementEntry("N");
	    		noAETemplateDO.setNbrSettlementDays(rs.getString("NBR_SETTLEMENT_DAYS"));
	    		
	    		if (srcAETemplateDO.getDateEffective() != null
						&& srcAETemplateDO.getDateEffective().compareTo(
								rs.getDate("DAT_EFFECTIVE")) > 0) {
	    		    noAETemplateDO.setDateEffective(srcAETemplateDO.getDateEffective());
	    		}else{
	    		    noAETemplateDO.setDateEffective(rs.getDate("DAT_EFFECTIVE"));
	    		}
	    		
	    		if(srcAETemplateDO.getFlgDrcr().equalsIgnoreCase(ExecutionConstants.CREDIT)) {
	    			noAETemplateDO.setFlgDrcr(ExecutionConstants.DEBIT);
	    		} else {
	    			noAETemplateDO.setFlgDrcr(ExecutionConstants.CREDIT);
	    		}
	    		
	    		noAETemplateDO.setFlgAcctType("I");
	    		noAETemplateDO.setFlgSrcTgtTyp("S");
	    		noAETemplateDO.setCodTriggeringGl(srcAETemplateDO.getCodTriggeringGl());
	    		noAETemplateDO.setCodBranch(rs.getString("COD_BRANCH_NO"));
	    		noAETemplateDO.setDateEnd(rs.getDate("DAT_END"));
	
	    		//- DeLocalize Settlement path - Start
	    		if(rs.getString("COD_ENTITY_NOSTRO").equalsIgnoreCase
	    		    (rs.getString("COD_ENTITY_NOSTRO_SETTLEMENT")) ){
	    		    noAETemplateDO.setFlgSettlementPath("C");
	    		}else{
	    		    noAETemplateDO.setFlgSettlementPath("D");
	    		}
	    		//- DeLocalize Settlement path - End
	    		
	    		nspAETemplateDO.setNbrOwnAccount(rs.getString("NBR_ACCTNO_NOSTRO"));
	    		nspAETemplateDO.setNbrCoreAccount(rs.getString("NBR_COREACCOUNT_NSP"));
	    		nspAETemplateDO.setCodBnkLglEnt(rs.getString("COD_BNK_LGL_NSP"));
	    		nspAETemplateDO.setCodCcy(codCcy);
	    		nspAETemplateDO.setCodCountry(rs.getString("COD_COUNTRY_NSP"));
	    		nspAETemplateDO.setCodGl(rs.getString("COD_GL_NSP"));
	    		nspAETemplateDO.setFlgPersist("Y");
	    		nspAETemplateDO.setNbrTemplateId(tgtAETemplateDO.getNbrTemplateId());
	    		nspAETemplateDO.setNbrContractNo(rs.getString("NBR_CONTRACT_NO"));
	    		nspAETemplateDO.setNbrSeqNo(rs.getString("NBR_SEQ"));
	    		nspAETemplateDO.setFlgSettlementEntry("N");
	    		nspAETemplateDO.setNbrSettlementDays(rs.getString("NBR_SETTLEMENT_DAYS"));
	    		
	    		if (tgtAETemplateDO.getDateEffective() != null
						&& tgtAETemplateDO.getDateEffective().compareTo(
								rs.getDate("DAT_EFFECTIVE")) > 0) {
	    		    nspAETemplateDO.setDateEffective(tgtAETemplateDO.getDateEffective());
	    		}else{
	    		    nspAETemplateDO.setDateEffective(rs.getDate("DAT_EFFECTIVE"));
	    		}
	    		
	    		if(tgtAETemplateDO.getFlgDrcr().equalsIgnoreCase(ExecutionConstants.CREDIT)) {
	    			nspAETemplateDO.setFlgDrcr(ExecutionConstants.DEBIT);	    			
	    		} else {
	    			nspAETemplateDO.setFlgDrcr(ExecutionConstants.CREDIT);
	    		}
	    		
	    		nspAETemplateDO.setFlgAcctType("I");
	    		nspAETemplateDO.setFlgSrcTgtTyp("T");
//Retro_Mashreq
	    		nspAETemplateDO.setCodTriggeringGl(srcAETemplateDO.getCodTriggeringGl());
	    		nspAETemplateDO.setCodBranch(rs.getString("COD_BRANCH_NSP"));
	    		nspAETemplateDO.setDateEnd(rs.getDate("DAT_END"));
	
	    		//- DeLocalize Settlement path - Start
	    		if(rs.getString("COD_ENTITY_NOSTRO").equalsIgnoreCase
	    		    (rs.getString("COD_ENTITY_NOSTRO_SETTLEMENT")) ){
	    		    nspAETemplateDO.setFlgSettlementPath("C");
	    		}else{
	    		    nspAETemplateDO.setFlgSettlementPath("D");
	    		}
	    		//- DeLocalize Settlement path - End
	
	    		srcTgtCBTAccountList.add(nspAETemplateDO);
	    		srcTgtCBTAccountList.add(noAETemplateDO);
	
	    		AccountingEntryTemplateDO mainSetAETemplateDO =  new AccountingEntryTemplateDO();
	    		AccountingEntryTemplateDO mirrorSetAETemplateDO =  new AccountingEntryTemplateDO();
	
	    		mirrorSetAETemplateDO.setNbrOwnAccount(rs.getString("NBR_ACCTNO_MIRROR_SETTELMENT"));
	    		mirrorSetAETemplateDO.setNbrCoreAccount(rs.getString("NBR_COREACCOUNT_MIRROR"));
	    		mirrorSetAETemplateDO.setCodBnkLglEnt(rs.getString("COD_BNK_LGL_MIRROR"));
	    		mirrorSetAETemplateDO.setCodCcy(codCcy);
	    		mirrorSetAETemplateDO.setCodCountry(rs.getString("COD_COUNTRY_MIRROR"));
	    		mirrorSetAETemplateDO.setCodGl(rs.getString("COD_GL_MIRROR"));
	    		mirrorSetAETemplateDO.setFlgAcctType("I");
	    		mirrorSetAETemplateDO.setFlgSrcTgtTyp("S");
	    		mirrorSetAETemplateDO.setFlgSettlementEntry("Y");
	    		mirrorSetAETemplateDO.setNbrTemplateId(tgtAETemplateDO.getNbrTemplateId());
	    		mirrorSetAETemplateDO.setNbrContractNo(rs.getString("NBR_CONTRACT_NO"));
	    		mirrorSetAETemplateDO.setNbrSeqNo(rs.getString("NBR_SEQ"));
	    		mirrorSetAETemplateDO.setFlgPersist("Y");
	    		mirrorSetAETemplateDO.setNbrSettlementDays(rs.getString("NBR_SETTLEMENT_DAYS"));
	    		
	    		if (tgtAETemplateDO.getDateEffective() != null
						&& tgtAETemplateDO.getDateEffective().compareTo(
								rs.getDate("DAT_EFFECTIVE")) > 0) {
	    		    mirrorSetAETemplateDO.setDateEffective(tgtAETemplateDO.getDateEffective());
	    		}else{
	    		    mirrorSetAETemplateDO.setDateEffective(rs.getDate("DAT_EFFECTIVE"));
	    		}
	    		
	    		mirrorSetAETemplateDO.setFlgDrcr(noAETemplateDO.getFlgDrcr());
//Retro_Mashreq
	    		mirrorSetAETemplateDO.setCodTriggeringGl(srcAETemplateDO.getCodTriggeringGl());
	    		mirrorSetAETemplateDO.setCodBranch(rs.getString("COD_BRANCH_MIRROR"));
	    		mirrorSetAETemplateDO.setDateEnd(rs.getDate("DAT_END"));
	    		
	    		//- DeLocalize Settlement path - Start
	    		if(rs.getString("COD_ENTITY_NOSTRO").equalsIgnoreCase
	    		    (rs.getString("COD_ENTITY_NOSTRO_SETTLEMENT")) ){
	    		    mirrorSetAETemplateDO.setFlgSettlementPath("C");
	    		}else{
	    		    mirrorSetAETemplateDO.setFlgSettlementPath("D");
	    		}
	    		//- DeLocalize Settlement path - End
	    		if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
	    			logger.debug("mirrorSetAETemplateDO.setTxnRefId(lContractNo+lSeqNo) ::setTxnRefId"+lContractNo+lSeqNo);
	    		
	    		mainSetAETemplateDO.setNbrOwnAccount(rs.getString("NBR_ACCNO_MAIN_SETTELMENT"));
	    		mainSetAETemplateDO.setNbrCoreAccount(rs.getString("NBR_COREACCOUNT_MAIN"));
	    		mainSetAETemplateDO.setCodBnkLglEnt(rs.getString("COD_BNK_LGL_MAIN"));
	    		mainSetAETemplateDO.setCodCcy(codCcy);
	    		mainSetAETemplateDO.setCodCountry(rs.getString("COD_COUNTRY_MAIN"));
	    		mainSetAETemplateDO.setCodGl(rs.getString("COD_GL_MAIN"));
	    		mainSetAETemplateDO.setFlgAcctType("I");
	    		mainSetAETemplateDO.setFlgSrcTgtTyp("T");
	    		mainSetAETemplateDO.setFlgSettlementEntry("Y");
	    		mainSetAETemplateDO.setNbrTemplateId(srcAETemplateDO.getNbrTemplateId());
	    		mainSetAETemplateDO.setNbrContractNo(rs.getString("NBR_CONTRACT_NO"));
	    		mainSetAETemplateDO.setNbrSeqNo(rs.getString("NBR_SEQ"));
	    		mainSetAETemplateDO.setFlgPersist("Y");
	    		mainSetAETemplateDO.setNbrSettlementDays(rs.getString("NBR_SETTLEMENT_DAYS"));
	    		
	    		if (srcAETemplateDO.getDateEffective() != null
						&& srcAETemplateDO.getDateEffective().compareTo(
								rs.getDate("DAT_EFFECTIVE")) > 0) {
	    		    mainSetAETemplateDO.setDateEffective(srcAETemplateDO.getDateEffective());
	    		}else{
	    		    mainSetAETemplateDO.setDateEffective(rs.getDate("DAT_EFFECTIVE"));
	    		}
	    		
	    		mainSetAETemplateDO.setFlgDrcr(nspAETemplateDO.getFlgDrcr());
	//Retro_Mashreq
	    		mainSetAETemplateDO.setCodTriggeringGl(srcAETemplateDO.getCodTriggeringGl());	    		
	    		mainSetAETemplateDO.setCodBranch(rs.getString("COD_BRANCH_MAIN"));
	    		mainSetAETemplateDO.setDateEnd(rs.getDate("DAT_END"));
	
	    		//- DeLocalize Settlement path - Start
	    		if(rs.getString("COD_ENTITY_NOSTRO").equalsIgnoreCase
	    		    (rs.getString("COD_ENTITY_NOSTRO_SETTLEMENT")) ){
	    		    mainSetAETemplateDO.setFlgSettlementPath("C");
	    		}else{
	    		    mainSetAETemplateDO.setFlgSettlementPath("D");
	    		}
	    		//- DeLocalize Settlement path - End
	    		if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
	    			logger.debug("mainSetAETemplateDO.setTxnRefId(lContractNo+lSeqNo) ::setTxnRefId"+lContractNo+lSeqNo);
	    		srcTgtCBTSettlementList.add(mainSetAETemplateDO);
	    		srcTgtCBTSettlementList.add(mirrorSetAETemplateDO);
	    		if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
	    			logger.debug("isHostExistsas CBT : NBR_ACCTNO_LORO = "+rs.getString("NBR_ACCTNO_LORO")+" NBR_COREACCOUNT_NO = "+rs.getString("NBR_COREACCOUNT_NO")+" NBR_COREACCOUNT_NSP = "+rs.getString("NBR_COREACCOUNT_NSP")+" NBR_COREACCOUNT_MIRROR = "+rs.getString("NBR_COREACCOUNT_MIRROR")+" NBR_COREACCOUNT_MAIN = "+rs.getString("NBR_COREACCOUNT_MAIN"));
    	    }
    	}catch(Exception e){
    	    throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, "Exception in isSrcHostsExistAsCBT()", e);
    	}
    	//JTEST - 31012011 - Starrt
    	finally
    	{
    		LMSConnectionUtility.close(stmt);
    		LMSConnectionUtility.close(rs);
    	}
    	//JTEST - 31012011 -- End
    	logger.debug("isSrcHostsExistAsCBT....Leaving");
    	return isCBTExist;
    }    
    //HSBC_DEF_FIX_14022011 -- End

    /**
     * 
     * @param srcAETemplateDO
     * @param tgtAETemplateDO
     * @param srcCBTNostroList
     * @param tgtCBTNostroList
     * @param srcCBTSettlementList
     * @param tgtCBTSettlementList
     * @return
     * @throws LMSException
     */
    private boolean isCBTExistsForHost(AccountingEntryTemplateDO srcAETemplateDO,
	    AccountingEntryTemplateDO tgtAETemplateDO,ArrayList srcCBTNostroList,
	    ArrayList tgtCBTNostroList,ArrayList srcCBTSettlementList,ArrayList tgtCBTSettlementList, Connection conn) throws LMSException{
    	//JTest changes on 01-feb-11 starts
    	if(logger.isDebugEnabled())
    		logger.debug("isCBTExistsForHost....");
    	
		PreparedStatement stmt = null;
		//JTest changes on 01-feb-11 ends
		ResultSet rs = null;
		boolean isCBTExistForHost = false;
		boolean entity1CBTExist = false;
		boolean entity2CBTExist = false;
		try{
	
		    String entity1 = "";
		    String entity2 = "";
	
		    String codCcy = srcAETemplateDO.getCodCcy();
	
		    if("HostSystem".equals(ExecutionConstants.ENTITY_TYPE)){
			entity1 = srcAETemplateDO.getCodGl();
			entity2 = tgtAETemplateDO.getCodGl();
		    }else if("BLE".equals(ExecutionConstants.ENTITY_TYPE)){
			entity1 = srcAETemplateDO.getCodBnkLglEnt();
			entity2 = tgtAETemplateDO.getCodBnkLglEnt();
		    }
		    //ENH_Nostroloro_branch start
		    else if(ExecutionConstants.NOSTRO_LORO_BRANCH.equals(ExecutionConstants.ENTITY_TYPE)){
				entity1 = srcAETemplateDO.getCodBranch();
				entity2 = tgtAETemplateDO.getCodBranch();
			    }
		    //ENH_Nostroloro_branch end
	
		/*    String sql = "SELECT OND.*, OA1.COD_BNK_LGL COD_BNK_LGL_NSP, OA1.COD_COUNTRY COD_COUNTRY_NSP, " +
		    "OA1.NBR_COREACCOUNT NBR_COREACCOUNT_NSP,OA1.COD_BRANCH COD_BRANCH_NSP, OA2.COD_BNK_LGL COD_BNK_LGL_NO, " +
		    "OA2.COD_COUNTRY COD_COUNTRY_NO, OA2.NBR_COREACCOUNT NBR_COREACCOUNT_NO,OA2.COD_BRANCH COD_BRANCH_NO, " +
		    "OA3.COD_BNK_LGL COD_BNK_LGL_MAIN, OA3.COD_COUNTRY COD_COUNTRY_MAIN, " +
		    "OA3.NBR_COREACCOUNT NBR_COREACCOUNT_MAIN,OA3.COD_BRANCH COD_BRANCH_MAIN, OA4.COD_BNK_LGL COD_BNK_LGL_MIRROR, " +
		    "OA4.COD_COUNTRY COD_COUNTRY_MIRROR, OA4.NBR_COREACCOUNT NBR_COREACCOUNT_MIRROR,OA4.COD_BRANCH COD_BRANCH_MIRROR " +
		    "FROM OLM_NOSTROLORO_MST ONM, OLM_NOSTROLORO_DTLS OND, ORBICASH_ACCOUNTS OA1, " +
		    "ORBICASH_ACCOUNTS OA2, ORBICASH_ACCOUNTS OA3, ORBICASH_ACCOUNTS OA4 " +
		    "WHERE ONM.NBR_CONTRACT_NO = OND.NBR_CONTRACT_NO " +
		    "AND ONM.COD_LINKAGE_TYPE = '"+ExecutionConstants.NLEG_CENTRAL+"' " +
		    "AND ONM.TYP_ENTITY = '"+ExecutionConstants.ENTITY_TYPE+"' " +
		    "AND OND.COD_ENTITY_LORO IN ('"+entity1+"','"+entity2+"') " +
		    "AND OND.COD_CCY = '"+codCcy+"' AND OND.NBR_ACCTNO_NOSTRO = OA1.NBR_OWNACCOUNT " +
		    "AND OND.NBR_ACCTNO_LORO = OA2.NBR_OWNACCOUNT "+
		    "AND OND.NBR_ACCNO_MAIN_SETTELMENT = OA3.NBR_OWNACCOUNT(+) " +
		    "AND OND.NBR_ACCTNO_MIRROR_SETTELMENT = OA4.NBR_OWNACCOUNT(+) "; */
	
		    
		    String sql = "SELECT OND.*, OA1.COD_BNK_LGL COD_BNK_LGL_NSP, OA1.COD_GL COD_GL_NSP, OA1.COD_COUNTRY COD_COUNTRY_NSP, " +
		    "OA1.NBR_COREACCOUNT NBR_COREACCOUNT_NSP,OA1.COD_BRANCH COD_BRANCH_NSP, OA2.COD_BNK_LGL COD_BNK_LGL_NO, OA2.COD_GL COD_GL_NO, " +
		    "OA2.COD_COUNTRY COD_COUNTRY_NO, OA2.NBR_COREACCOUNT NBR_COREACCOUNT_NO,OA2.COD_BRANCH COD_BRANCH_NO, " +
		    "OA3.COD_BNK_LGL COD_BNK_LGL_MAIN, OA3.COD_GL COD_GL_MAIN, OA3.COD_COUNTRY COD_COUNTRY_MAIN, " +
		    "OA3.NBR_COREACCOUNT NBR_COREACCOUNT_MAIN,OA3.COD_BRANCH COD_BRANCH_MAIN, OA4.COD_BNK_LGL COD_BNK_LGL_MIRROR, OA4.COD_GL COD_GL_MIRROR, " +
		    "OA4.COD_COUNTRY COD_COUNTRY_MIRROR, OA4.NBR_COREACCOUNT NBR_COREACCOUNT_MIRROR,OA4.COD_BRANCH COD_BRANCH_MIRROR " +
		    "FROM OLM_NOSTROLORO_MST ONM, OLM_NOSTROLORO_DTLS OND, ORBICASH_ACCOUNTS OA1, " +
		    "ORBICASH_ACCOUNTS OA2, ORBICASH_ACCOUNTS OA3, ORBICASH_ACCOUNTS OA4 " +
		    "WHERE ONM.NBR_CONTRACT_NO = OND.NBR_CONTRACT_NO " +
		    //JTest changes on 01-feb-11 starts
		    /*"AND ONM.COD_LINKAGE_TYPE = '"+ExecutionConstants.NLEG_CENTRAL+"' " +
		    "AND ONM.TYP_ENTITY = '"+ExecutionConstants.ENTITY_TYPE+"' " +
		    //Jiten - BILATERALSETTLEMENT - Start
		    "AND ONM.FLG_CLOSED = '"+ExecutionConstants.NO+"' " +
		    //Jiten - BILATERALSETTLEMENT - End
		    "AND OND.COD_ENTITY_LORO IN ('"+entity1+"','"+entity2+"') " +
		    "AND OND.COD_CCY = '"+codCcy+"' AND OND.NBR_ACCTNO_NOSTRO = OA1.NBR_OWNACCOUNT " +*/
		    "AND ONM.COD_LINKAGE_TYPE in (?,?) " +
		    "AND ONM.TYP_ENTITY = ? " +
		    //Jiten - BILATERALSETTLEMENT - Start
		    "AND ONM.FLG_CLOSED = ? " +
		    //Jiten - BILATERALSETTLEMENT - End
		    "AND OND.COD_ENTITY_LORO IN (?,?) " +
		    "AND OND.COD_CCY = ? AND OND.NBR_ACCTNO_NOSTRO = OA1.NBR_OWNACCOUNT " +
		    //JTest changes on 01-feb-11 ends
		    "AND OND.NBR_ACCTNO_LORO = OA2.NBR_OWNACCOUNT "+
		    "AND OND.NBR_ACCNO_MAIN_SETTELMENT = OA3.NBR_OWNACCOUNT(+) " +
		    "AND OND.NBR_ACCTNO_MIRROR_SETTELMENT = OA4.NBR_OWNACCOUNT(+) " +
		    "AND (SELECT MAX(DAT_TODAY)+1 FROM OLM_HOSTSYSTEMDATES WHERE COD_GL IN(OA1.COD_GL,OA2.COD_GL)) " +
		    "BETWEEN OND.DAT_EFFECTIVE AND NVL( OND.DAT_END , TO_DATE('31-DEC-9999','DD-MON-YYYY')) ";
		    //JTest changes on 01-feb-11 starts
		    if(logger.isDebugEnabled())
		    	logger.debug("isCBTExistsForHost....sql :: "+sql);
	
		    stmt = conn.prepareStatement(sql);
		  //HSBC_OSL_2643_CR_ICL_INTR Start
		
		    	stmt.setString(1, ExecutionConstants.NLEG_CENTRAL);
		   
		    	stmt.setString(2, ExecutionConstants.CENTRAL_TREASURY);
    	  //HSBC_OSL_2643_CR_ICL_INTR End
		    stmt.setString(3, ExecutionConstants.ENTITY_TYPE);
		    stmt.setString(4, ExecutionConstants.NO);
		    stmt.setString(5, entity1);
		    stmt.setString(6, entity2);
		    stmt.setString(7, codCcy);
		    rs = stmt.executeQuery();
		    //JTest changes on 01-feb-11 ends
	    while(rs.next()){
		isCBTExistForHost = true;

		if(entity1.equals(rs.getString("COD_ENTITY_LORO"))){
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
			//Triggering GL Population from Nostro Linkage
		    if(srcAETemplateDO.getCodTriggeringGl() == null || srcAETemplateDO.getCodTriggeringGl().trim().equals("")){
				srcAETemplateDO.setCodTriggeringGl(rs.getString("COD_GL_NO"));
		    }
			//Triggering GL Population from Nostro Linkage
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		    AccountingEntryTemplateDO srcNspTemplateDO =  new AccountingEntryTemplateDO();
		    AccountingEntryTemplateDO srcNOTemplateDO =  new AccountingEntryTemplateDO();

		    srcNspTemplateDO.setNbrOwnAccount(rs.getString("NBR_ACCTNO_NOSTRO"));
		    srcNspTemplateDO.setNbrCoreAccount(rs.getString("NBR_COREACCOUNT_NSP"));
		    srcNspTemplateDO.setCodBnkLglEnt(rs.getString("COD_BNK_LGL_NSP"));
		    srcNspTemplateDO.setCodCcy(codCcy);
		    srcNspTemplateDO.setCodCountry(rs.getString("COD_COUNTRY_NSP"));
		  //  srcNspTemplateDO.setCodGl(rs.getString("COD_ENTITY_NOSTRO"));
		    srcNspTemplateDO.setCodGl(rs.getString("COD_GL_NSP"));
		    srcNspTemplateDO.setFlgPersist("Y");
		    srcNspTemplateDO.setNbrTemplateId(srcAETemplateDO.getNbrTemplateId());
		    srcNspTemplateDO.setNbrContractNo(rs.getString("NBR_CONTRACT_NO"));
		    srcNspTemplateDO.setNbrSeqNo(rs.getString("NBR_SEQ"));
		    srcNspTemplateDO.setFlgSettlementEntry("N");

		    srcNspTemplateDO.setNbrSettlementDays(rs.getString("NBR_SETTLEMENT_DAYS"));
		    if(srcAETemplateDO.getDateEffective() != null && srcAETemplateDO.getDateEffective().compareTo(rs.getDate("DAT_EFFECTIVE")) > 0 ){
			srcNspTemplateDO.setDateEffective(srcAETemplateDO.getDateEffective());
		    }else{
			srcNspTemplateDO.setDateEffective(rs.getDate("DAT_EFFECTIVE"));
		    }
		    
		    srcNspTemplateDO.setFlgDrcr(srcAETemplateDO.getFlgDrcr());
		    srcNspTemplateDO.setFlgAcctType("I");
		    srcNspTemplateDO.setFlgSrcTgtTyp("T");
		    //srcNspTemplateDO.setCodTriggeringGl(rs.getString("COD_GL_NO"));
		    srcNspTemplateDO.setCodTriggeringGl(srcAETemplateDO.getCodTriggeringGl());
		    
		    srcNspTemplateDO.setCodBranch(rs.getString("COD_BRANCH_NSP"));
		    srcNspTemplateDO.setDateEnd(rs.getDate("DAT_END"));
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		    //Jiten - BILATERALSETTLEMENT - Start
		    //srcNspTemplateDO.setFlgSettlementPath("C");
		    //Jiten - BILATERALSETTLEMENT - End
		    
		    //- DeLocalize Settlement path - Start
		    if(rs.getString("COD_ENTITY_NOSTRO").equalsIgnoreCase
			    (rs.getString("COD_ENTITY_NOSTRO_SETTLEMENT")) ){
			srcNspTemplateDO.setFlgSettlementPath("C");
		    }else{
			srcNspTemplateDO.setFlgSettlementPath("D");
		    }
		    //- DeLocalize Settlement path - End
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends

		    srcNOTemplateDO.setNbrOwnAccount(rs.getString("NBR_ACCTNO_LORO"));
		    srcNOTemplateDO.setNbrCoreAccount(rs.getString("NBR_COREACCOUNT_NO"));
		    srcNOTemplateDO.setCodBnkLglEnt(rs.getString("COD_BNK_LGL_NO"));
		    srcNOTemplateDO.setCodCcy(codCcy);
		    srcNOTemplateDO.setCodCountry(rs.getString("COD_COUNTRY_NO"));
		  //  srcNOTemplateDO.setCodGl(rs.getString("COD_ENTITY_LORO"));
		    srcNOTemplateDO.setCodGl(rs.getString("COD_GL_NO"));
		    srcNOTemplateDO.setFlgPersist("Y");
		    srcNOTemplateDO.setNbrTemplateId(srcAETemplateDO.getNbrTemplateId());
		    srcNOTemplateDO.setNbrContractNo(rs.getString("NBR_CONTRACT_NO"));
		    srcNOTemplateDO.setNbrSeqNo(rs.getString("NBR_SEQ"));
		    srcNOTemplateDO.setFlgSettlementEntry("N");
		    srcNOTemplateDO.setNbrSettlementDays(rs.getString("NBR_SETTLEMENT_DAYS"));
		    if(srcAETemplateDO.getDateEffective() != null && srcAETemplateDO.getDateEffective().compareTo(rs.getDate("DAT_EFFECTIVE")) > 0){
			srcNOTemplateDO.setDateEffective(srcAETemplateDO.getDateEffective());
		    }else{
			srcNOTemplateDO.setDateEffective(rs.getDate("DAT_EFFECTIVE"));
		    }
		    
		    if(srcAETemplateDO.getFlgDrcr().equalsIgnoreCase(ExecutionConstants.CREDIT)){
			srcNOTemplateDO.setFlgDrcr(ExecutionConstants.DEBIT);
		    }else{
			srcNOTemplateDO.setFlgDrcr(ExecutionConstants.CREDIT);
		    }
		    srcNOTemplateDO.setFlgAcctType("I");
		    srcNOTemplateDO.setFlgSrcTgtTyp("S");
		    //srcNOTemplateDO.setCodTriggeringGl(rs.getString("COD_GL_NO"));
		    srcNOTemplateDO.setCodTriggeringGl(srcAETemplateDO.getCodTriggeringGl());
		    srcNOTemplateDO.setCodBranch(rs.getString("COD_BRANCH_NO"));
		    srcNOTemplateDO.setDateEnd(rs.getDate("DAT_END"));
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		    //Jiten - BILATERALSETTLEMENT - Start
		    //srcNOTemplateDO.setFlgSettlementPath("C");
		    //Jiten - BILATERALSETTLEMENT - End
		    
		    //- DeLocalize Settlement path - Start
		    if(rs.getString("COD_ENTITY_NOSTRO").equalsIgnoreCase
			    (rs.getString("COD_ENTITY_NOSTRO_SETTLEMENT")) ){
			srcNOTemplateDO.setFlgSettlementPath("C");
		    }else{
			srcNOTemplateDO.setFlgSettlementPath("D");
		    }
		    //- DeLocalize Settlement path - End
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends

		    srcCBTNostroList.add(srcNOTemplateDO);
		    srcCBTNostroList.add(srcNspTemplateDO);
		    entity1CBTExist = true;
		    /////////////////////////////////////////

		    ///////////////////////////////////////////
		    AccountingEntryTemplateDO srcMainTemplateDO =  new AccountingEntryTemplateDO();
		    AccountingEntryTemplateDO srcMirrTemplateDO =  new AccountingEntryTemplateDO();

		    srcMainTemplateDO.setNbrOwnAccount(rs.getString("NBR_ACCNO_MAIN_SETTELMENT"));
		    srcMainTemplateDO.setNbrCoreAccount(rs.getString("NBR_COREACCOUNT_MAIN"));
		    srcMainTemplateDO.setCodBnkLglEnt(rs.getString("COD_BNK_LGL_MAIN"));
		    srcMainTemplateDO.setCodCcy(codCcy);
		    srcMainTemplateDO.setCodCountry(rs.getString("COD_COUNTRY_MAIN"));
		   // srcMainTemplateDO.setCodGl(rs.getString("COD_ENTITY_NOSTRO_SETTLEMENT"));
		    srcMainTemplateDO.setCodGl(rs.getString("COD_GL_MAIN"));
		    srcMainTemplateDO.setNbrTemplateId(srcAETemplateDO.getNbrTemplateId());
		    srcMainTemplateDO.setNbrContractNo(rs.getString("NBR_CONTRACT_NO"));
		    srcMainTemplateDO.setNbrSeqNo(rs.getString("NBR_SEQ"));
		    srcMainTemplateDO.setFlgSettlementEntry("Y");
		    srcMainTemplateDO.setNbrSettlementDays(rs.getString("NBR_SETTLEMENT_DAYS"));
		    if(srcAETemplateDO.getDateEffective() != null && srcAETemplateDO.getDateEffective().compareTo(rs.getDate("DAT_EFFECTIVE")) > 0){
			srcMainTemplateDO.setDateEffective(srcAETemplateDO.getDateEffective());
		    }else{
			srcMainTemplateDO.setDateEffective(rs.getDate("DAT_EFFECTIVE"));
		    }
		    
		    srcMainTemplateDO.setFlgDrcr(srcAETemplateDO.getFlgDrcr());
		    srcMainTemplateDO.setFlgAcctType("I");
		    srcMainTemplateDO.setFlgSrcTgtTyp("T");
		    srcMainTemplateDO.setFlgPersist("Y");
		    //srcMainTemplateDO.setCodTriggeringGl(rs.getString("COD_GL_NO"));
		    srcMainTemplateDO.setCodTriggeringGl(srcAETemplateDO.getCodTriggeringGl());
		    
		    srcMainTemplateDO.setCodBranch(rs.getString("COD_BRANCH_MAIN"));
		    srcMainTemplateDO.setDateEnd(rs.getDate("DAT_END"));
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		    //Jiten - BILATERALSETTLEMENT - Start
		    //srcMainTemplateDO.setFlgSettlementPath("C");
		    //Jiten - BILATERALSETTLEMENT - End
		    
		    //- DeLocalize Settlement path - Start
		    if(rs.getString("COD_ENTITY_NOSTRO").equalsIgnoreCase
			    (rs.getString("COD_ENTITY_NOSTRO_SETTLEMENT")) ){
			srcMainTemplateDO.setFlgSettlementPath("C");
		    }else{
			srcMainTemplateDO.setFlgSettlementPath("D");
		    }
		    //- DeLocalize Settlement path - End
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		    srcMirrTemplateDO.setNbrOwnAccount(rs.getString("NBR_ACCTNO_MIRROR_SETTELMENT"));
		    srcMirrTemplateDO.setNbrCoreAccount(rs.getString("NBR_COREACCOUNT_MIRROR"));
		    srcMirrTemplateDO.setCodBnkLglEnt(rs.getString("COD_BNK_LGL_MIRROR"));
		    srcMirrTemplateDO.setCodCcy(codCcy);
		    srcMirrTemplateDO.setCodCountry(rs.getString("COD_COUNTRY_MIRROR"));
		   // srcMirrTemplateDO.setCodGl(rs.getString("COD_ENTITY_LORO_SETTLEMENT"));
		    srcMirrTemplateDO.setCodGl(rs.getString("COD_GL_MIRROR"));
		    srcMirrTemplateDO.setNbrTemplateId(srcAETemplateDO.getNbrTemplateId());
		    srcMirrTemplateDO.setNbrContractNo(rs.getString("NBR_CONTRACT_NO"));
		    srcMirrTemplateDO.setNbrSeqNo(rs.getString("NBR_SEQ"));
		    srcMirrTemplateDO.setFlgSettlementEntry("Y");
		    srcMirrTemplateDO.setNbrSettlementDays(rs.getString("NBR_SETTLEMENT_DAYS"));
		    if(srcAETemplateDO.getDateEffective() != null && srcAETemplateDO.getDateEffective().compareTo(rs.getDate("DAT_EFFECTIVE")) > 0){
			srcMirrTemplateDO.setDateEffective(srcAETemplateDO.getDateEffective());
		    }else{
			srcMirrTemplateDO.setDateEffective(rs.getDate("DAT_EFFECTIVE"));
		    }
		    
		    if(srcAETemplateDO.getFlgDrcr().equalsIgnoreCase(ExecutionConstants.CREDIT)){
			srcMirrTemplateDO.setFlgDrcr(ExecutionConstants.DEBIT);
		    }else{
			srcMirrTemplateDO.setFlgDrcr(ExecutionConstants.CREDIT);
		    }
		    srcMirrTemplateDO.setFlgAcctType("I");
		    srcMirrTemplateDO.setFlgSrcTgtTyp("S");
		    srcMirrTemplateDO.setFlgPersist("Y");
		    //srcMirrTemplateDO.setCodTriggeringGl(rs.getString("COD_GL_NO"));
		    srcMirrTemplateDO.setCodTriggeringGl(srcAETemplateDO.getCodTriggeringGl());
		    
		    srcMirrTemplateDO.setCodBranch(rs.getString("COD_BRANCH_MIRROR"));
		    srcMirrTemplateDO.setDateEnd(rs.getDate("DAT_END"));
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		    //Jiten - BILATERALSETTLEMENT - Start
		    //srcMirrTemplateDO.setFlgSettlementPath("C");
		    //Jiten - BILATERALSETTLEMENT - End
		    
		    //- DeLocalize Settlement path - Start
		    if(rs.getString("COD_ENTITY_NOSTRO").equalsIgnoreCase
			    (rs.getString("COD_ENTITY_NOSTRO_SETTLEMENT")) ){
			srcMirrTemplateDO.setFlgSettlementPath("C");
		    }else{
			srcMirrTemplateDO.setFlgSettlementPath("D");
		    }
		    //- DeLocalize Settlement path - End
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		    srcCBTSettlementList.add(srcMirrTemplateDO);
		    srcCBTSettlementList.add(srcMainTemplateDO);
		    /////////////////////////////////////////////
		}else if(entity2.equals(rs.getString("COD_ENTITY_LORO"))){
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
			//Triggering GL Population from Nostro Linkage		    
		    if(tgtAETemplateDO.getCodTriggeringGl() == null || tgtAETemplateDO.getCodTriggeringGl().trim().equals("")){
				tgtAETemplateDO.setCodTriggeringGl(rs.getString("COD_GL_NO"));
		    }
		    //Triggering GL Population from Nostro Linkage
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		    AccountingEntryTemplateDO tgtNspTemplateDO =  new AccountingEntryTemplateDO();
		    AccountingEntryTemplateDO tgtNOTemplateDO =  new AccountingEntryTemplateDO();

		    tgtNspTemplateDO.setNbrOwnAccount(rs.getString("NBR_ACCTNO_NOSTRO"));
		    tgtNspTemplateDO.setNbrCoreAccount(rs.getString("NBR_COREACCOUNT_NSP"));
		    tgtNspTemplateDO.setCodBnkLglEnt(rs.getString("COD_BNK_LGL_NSP"));
		    tgtNspTemplateDO.setCodCcy(codCcy);
		    tgtNspTemplateDO.setCodCountry(rs.getString("COD_COUNTRY_NSP"));
		  //  tgtNspTemplateDO.setCodGl(rs.getString("COD_ENTITY_NOSTRO"));
		    tgtNspTemplateDO.setCodGl(rs.getString("COD_GL_NSP"));
		    tgtNspTemplateDO.setFlgPersist("Y");
		    tgtNspTemplateDO.setNbrTemplateId(srcAETemplateDO.getNbrTemplateId());
		    tgtNspTemplateDO.setFlgDrcr(tgtAETemplateDO.getFlgDrcr());
		    tgtNspTemplateDO.setNbrContractNo(rs.getString("NBR_CONTRACT_NO"));
		    tgtNspTemplateDO.setNbrSeqNo(rs.getString("NBR_SEQ"));
		    tgtNspTemplateDO.setFlgSettlementEntry("N");
		    tgtNspTemplateDO.setNbrSettlementDays(rs.getString("NBR_SETTLEMENT_DAYS"));
		    if(tgtAETemplateDO.getDateEffective() != null && tgtAETemplateDO.getDateEffective().compareTo(rs.getDate("DAT_EFFECTIVE")) > 0){
			tgtNspTemplateDO.setDateEffective(tgtAETemplateDO.getDateEffective());
		    }else{
			tgtNspTemplateDO.setDateEffective(rs.getDate("DAT_EFFECTIVE"));
		    }
		    
		    tgtNspTemplateDO.setFlgAcctType("I");
		    tgtNspTemplateDO.setFlgSrcTgtTyp("T");
		    //tgtNspTemplateDO.setCodTriggeringGl(rs.getString("COD_GL_NO"));
		    tgtNspTemplateDO.setCodTriggeringGl(tgtAETemplateDO.getCodTriggeringGl());
		    tgtNspTemplateDO.setCodBranch(rs.getString("COD_BRANCH_NSP"));
		    tgtNspTemplateDO.setDateEnd(rs.getDate("DAT_END"));
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		    //Jiten - BILATERALSETTLEMENT - Start
		    //tgtNspTemplateDO.setFlgSettlementPath("C");
		    //Jiten - BILATERALSETTLEMENT - End		    

		    //- DeLocalize Settlement path - Start
		    if(rs.getString("COD_ENTITY_NOSTRO").equalsIgnoreCase
			    (rs.getString("COD_ENTITY_NOSTRO_SETTLEMENT")) ){
			tgtNspTemplateDO.setFlgSettlementPath("C");
		    }else{
			tgtNspTemplateDO.setFlgSettlementPath("D");
		    }
		    //- DeLocalize Settlement path - End		    
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		    tgtNOTemplateDO.setNbrOwnAccount(rs.getString("NBR_ACCTNO_LORO"));
		    tgtNOTemplateDO.setNbrCoreAccount(rs.getString("NBR_COREACCOUNT_NO"));
		    tgtNOTemplateDO.setCodBnkLglEnt(rs.getString("COD_BNK_LGL_NO"));
		    tgtNOTemplateDO.setCodCcy(codCcy);
		    tgtNOTemplateDO.setCodCountry(rs.getString("COD_COUNTRY_NO"));
		  //  tgtNOTemplateDO.setCodGl(rs.getString("COD_ENTITY_LORO"));
		    tgtNOTemplateDO.setCodGl(rs.getString("COD_GL_NO"));
		    tgtNOTemplateDO.setFlgPersist("Y");
		    tgtNOTemplateDO.setNbrTemplateId(srcAETemplateDO.getNbrTemplateId());
		    tgtNOTemplateDO.setNbrContractNo(rs.getString("NBR_CONTRACT_NO"));
		    tgtNOTemplateDO.setNbrSeqNo(rs.getString("NBR_SEQ"));
		    tgtNOTemplateDO.setFlgSettlementEntry("N");
		    tgtNOTemplateDO.setNbrSettlementDays(rs.getString("NBR_SETTLEMENT_DAYS"));
		    if(tgtAETemplateDO.getDateEffective() != null && tgtAETemplateDO.getDateEffective().compareTo(rs.getDate("DAT_EFFECTIVE")) > 0){
			tgtNOTemplateDO.setDateEffective(tgtAETemplateDO.getDateEffective());
		    }else{
			tgtNOTemplateDO.setDateEffective(rs.getDate("DAT_EFFECTIVE"));
		    }
		    
		    if(tgtAETemplateDO.getFlgDrcr().equalsIgnoreCase(ExecutionConstants.CREDIT)){
			tgtNOTemplateDO.setFlgDrcr(ExecutionConstants.DEBIT);
		    }else{
			tgtNOTemplateDO.setFlgDrcr(ExecutionConstants.CREDIT);
		    }
		    tgtNOTemplateDO.setFlgAcctType("I");
		    tgtNOTemplateDO.setFlgSrcTgtTyp("S");
		    //tgtNOTemplateDO.setCodTriggeringGl(rs.getString("COD_GL_NO"));
		    tgtNOTemplateDO.setCodTriggeringGl(tgtAETemplateDO.getCodTriggeringGl());
		    
		    tgtNOTemplateDO.setCodBranch(rs.getString("COD_BRANCH_NO"));
		    tgtNOTemplateDO.setDateEnd(rs.getDate("DAT_END"));
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		    //Jiten - BILATERALSETTLEMENT - Start
		    //tgtNOTemplateDO.setFlgSettlementPath("C");
		    //Jiten - BILATERALSETTLEMENT - End			    

		    //- DeLocalize Settlement path - Start
		    if(rs.getString("COD_ENTITY_NOSTRO").equalsIgnoreCase
			    (rs.getString("COD_ENTITY_NOSTRO_SETTLEMENT")) ){
			tgtNOTemplateDO.setFlgSettlementPath("C");
		    }else{
			tgtNOTemplateDO.setFlgSettlementPath("D");
		    }
		    //- DeLocalize Settlement path - End
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		    tgtCBTNostroList.add(tgtNOTemplateDO); // Nostro Owner
		    tgtCBTNostroList.add(tgtNspTemplateDO); // Nostro Service Provider
		    entity2CBTExist = true;
		    //////////////////////////////////////////////

		    ////////////////////////////////////////////
		    AccountingEntryTemplateDO tgtMainTemplateDO =  new AccountingEntryTemplateDO();
		    AccountingEntryTemplateDO tgtMirrTemplateDO =  new AccountingEntryTemplateDO();

		    tgtMainTemplateDO.setNbrOwnAccount(rs.getString("NBR_ACCNO_MAIN_SETTELMENT"));
		    tgtMainTemplateDO.setNbrCoreAccount(rs.getString("NBR_COREACCOUNT_MAIN"));
		    tgtMainTemplateDO.setCodBnkLglEnt(rs.getString("COD_BNK_LGL_MAIN"));
		    tgtMainTemplateDO.setCodCcy(codCcy);
		    tgtMainTemplateDO.setCodCountry(rs.getString("COD_COUNTRY_MAIN"));
		 //   tgtMainTemplateDO.setCodGl(rs.getString("COD_ENTITY_NOSTRO_SETTLEMENT"));
		    tgtMainTemplateDO.setCodGl(rs.getString("COD_GL_MAIN"));
		    tgtMainTemplateDO.setNbrTemplateId(srcAETemplateDO.getNbrTemplateId());
		    tgtMainTemplateDO.setFlgDrcr(tgtAETemplateDO.getFlgDrcr());
		    tgtMainTemplateDO.setNbrContractNo(rs.getString("NBR_CONTRACT_NO"));
		    tgtMainTemplateDO.setNbrSeqNo(rs.getString("NBR_SEQ"));
		    tgtMainTemplateDO.setFlgSettlementEntry("Y");
		    tgtMainTemplateDO.setNbrSettlementDays(rs.getString("NBR_SETTLEMENT_DAYS"));
		    if(tgtAETemplateDO.getDateEffective() != null && tgtAETemplateDO.getDateEffective().compareTo(rs.getDate("DAT_EFFECTIVE")) > 0){
			tgtMainTemplateDO.setDateEffective(tgtAETemplateDO.getDateEffective());
		    }else{
			tgtMainTemplateDO.setDateEffective(rs.getDate("DAT_EFFECTIVE"));
		    }
		    
		    tgtMainTemplateDO.setFlgAcctType("I");
		    tgtMainTemplateDO.setFlgSrcTgtTyp("T");
		    tgtMainTemplateDO.setFlgPersist("Y");
		    //tgtMainTemplateDO.setCodTriggeringGl(rs.getString("COD_GL_NO"));
		    tgtMainTemplateDO.setCodTriggeringGl(tgtAETemplateDO.getCodTriggeringGl());
		    
		    tgtMainTemplateDO.setCodBranch(rs.getString("COD_BRANCH_MAIN"));
		    tgtMainTemplateDO.setDateEnd(rs.getDate("DAT_END"));
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		    //Jiten - BILATERALSETTLEMENT - Start
		    //tgtMainTemplateDO.setFlgSettlementPath("C");
		    //Jiten - BILATERALSETTLEMENT - End

		    //- DeLocalize Settlement path - Start
		    if(rs.getString("COD_ENTITY_NOSTRO").equalsIgnoreCase
			    (rs.getString("COD_ENTITY_NOSTRO_SETTLEMENT")) ){
			tgtMainTemplateDO.setFlgSettlementPath("C");
		    }else{
			tgtMainTemplateDO.setFlgSettlementPath("D");
		    }
		    //- DeLocalize Settlement path - End
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends

		    tgtMirrTemplateDO.setNbrOwnAccount(rs.getString("NBR_ACCTNO_MIRROR_SETTELMENT"));
		    tgtMirrTemplateDO.setNbrCoreAccount(rs.getString("NBR_COREACCOUNT_MIRROR"));
		    tgtMirrTemplateDO.setCodBnkLglEnt(rs.getString("COD_BNK_LGL_MIRROR"));
		    tgtMirrTemplateDO.setCodCcy(codCcy);
		    tgtMirrTemplateDO.setCodCountry(rs.getString("COD_COUNTRY_MIRROR"));
		//    tgtMirrTemplateDO.setCodGl(rs.getString("COD_ENTITY_LORO_SETTLEMENT"));
		    tgtMirrTemplateDO.setCodGl(rs.getString("COD_GL_MIRROR"));
		    tgtMirrTemplateDO.setNbrTemplateId(srcAETemplateDO.getNbrTemplateId());
		    tgtMirrTemplateDO.setNbrContractNo(rs.getString("NBR_CONTRACT_NO"));
		    tgtMirrTemplateDO.setNbrSeqNo(rs.getString("NBR_SEQ"));
		    tgtMirrTemplateDO.setFlgSettlementEntry("Y");
		    tgtMirrTemplateDO.setNbrSettlementDays(rs.getString("NBR_SETTLEMENT_DAYS"));
		    if(tgtAETemplateDO.getDateEffective() != null && tgtAETemplateDO.getDateEffective().compareTo(rs.getDate("DAT_EFFECTIVE")) > 0){
			tgtMirrTemplateDO.setDateEffective(tgtAETemplateDO.getDateEffective());
		    }else{
			tgtMirrTemplateDO.setDateEffective(rs.getDate("DAT_EFFECTIVE"));
		    }
		    
		    if(tgtAETemplateDO.getFlgDrcr().equalsIgnoreCase(ExecutionConstants.CREDIT)){
			tgtMirrTemplateDO.setFlgDrcr(ExecutionConstants.DEBIT);
		    }else{
			tgtMirrTemplateDO.setFlgDrcr(ExecutionConstants.CREDIT);
		    }
		    tgtMirrTemplateDO.setFlgAcctType("I");
		    tgtMirrTemplateDO.setFlgSrcTgtTyp("S");
		    tgtMirrTemplateDO.setFlgPersist("Y");
		    //tgtMirrTemplateDO.setCodTriggeringGl(rs.getString("COD_GL_NO"));
		    tgtMirrTemplateDO.setCodTriggeringGl(tgtAETemplateDO.getCodTriggeringGl());
		    
		    tgtMirrTemplateDO.setCodBranch(rs.getString("COD_BRANCH_MIRROR"));
		    tgtMirrTemplateDO.setDateEnd(rs.getDate("DAT_END"));
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
		    //Jiten - BILATERALSETTLEMENT - Start
		    //tgtMirrTemplateDO.setFlgSettlementPath("C");
		    //Jiten - BILATERALSETTLEMENT - End

		    //- DeLocalize Settlement path - Start
		    if(rs.getString("COD_ENTITY_NOSTRO").equalsIgnoreCase
			    (rs.getString("COD_ENTITY_NOSTRO_SETTLEMENT")) ){
			tgtMirrTemplateDO.setFlgSettlementPath("C");
		    }else{
			tgtMirrTemplateDO.setFlgSettlementPath("D");
		    }
		    //- DeLocalize Settlement path - End
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
		    tgtCBTSettlementList.add(tgtMirrTemplateDO);
		    tgtCBTSettlementList.add(tgtMainTemplateDO);
		    /////////////////////////////////////////////
		}
	    }
		    if(logger.isDebugEnabled())//JTest changes on 01-feb-11
		    {
			    logger.debug("isCBTExistForHost :: "+isCBTExistForHost);
			    logger.debug("entity1CBTExist :: "+entity1CBTExist);
			    logger.debug("entity2CBTExist :: "+entity2CBTExist);
		    }
	    if(isCBTExistForHost && !entity1CBTExist){
		AccountingEntryTemplateDO srcNspTemplateDO =  new AccountingEntryTemplateDO();
		AccountingEntryTemplateDO srcNOTemplateDO =  new AccountingEntryTemplateDO();
		srcNspTemplateDO = (AccountingEntryTemplateDO)srcAETemplateDO.clone();
		srcNspTemplateDO.setFlgPersist("N");
		srcNOTemplateDO = srcNspTemplateDO; 
		srcCBTNostroList.add(srcNOTemplateDO);
		srcCBTNostroList.add(srcNspTemplateDO);
		
		AccountingEntryTemplateDO srcMainTemplateDO =  new AccountingEntryTemplateDO();
		AccountingEntryTemplateDO srcMirrTemplateDO =  new AccountingEntryTemplateDO();

		srcCBTSettlementList.add(srcMirrTemplateDO);
		srcCBTSettlementList.add(srcMainTemplateDO);
	    }

	    if(isCBTExistForHost && !entity2CBTExist){
		AccountingEntryTemplateDO tgtNspTemplateDO =  new AccountingEntryTemplateDO();
		AccountingEntryTemplateDO tgtNOTemplateDO =  new AccountingEntryTemplateDO();
		tgtNspTemplateDO = (AccountingEntryTemplateDO)tgtAETemplateDO.clone();
		tgtNspTemplateDO.setFlgPersist("N");
		tgtNOTemplateDO = tgtNspTemplateDO; 
		tgtCBTNostroList.add(tgtNOTemplateDO); 
		tgtCBTNostroList.add(tgtNspTemplateDO);
		
		AccountingEntryTemplateDO tgtMainTemplateDO =  new AccountingEntryTemplateDO();
		AccountingEntryTemplateDO tgtMirrTemplateDO =  new AccountingEntryTemplateDO();

		tgtCBTSettlementList.add(tgtMirrTemplateDO);
		tgtCBTSettlementList.add(tgtMainTemplateDO);
	    }

	    if(isCBTExistForHost){
		    	//JTest changes on 01-feb-11 starts
		    	if(logger.isDebugEnabled()){
					logger.debug("Entity 1 Nostro owner :: "+(AccountingEntryTemplateDO)srcCBTNostroList.get(0));
					logger.debug("Entity 1 Nostro provider :: "+(AccountingEntryTemplateDO)srcCBTNostroList.get(1));
			
					logger.debug("Entity 2 Nostro owner :: "+(AccountingEntryTemplateDO)tgtCBTNostroList.get(0));
					logger.debug("Entity 2 Nostro provider :: "+(AccountingEntryTemplateDO)tgtCBTNostroList.get(1));
			
					logger.debug("Entity 1 Mirror :: "+(AccountingEntryTemplateDO)srcCBTSettlementList.get(0));
					logger.debug("Entity 1 Main :: "+(AccountingEntryTemplateDO)srcCBTSettlementList.get(1));
			
					logger.debug("Entity 2 Mirror :: "+(AccountingEntryTemplateDO)tgtCBTSettlementList.get(0));
					logger.debug("Entity 2 Main :: "+(AccountingEntryTemplateDO)tgtCBTSettlementList.get(1));
		    	}
		    	//JTest changes on 01-feb-11 ends
		    }
		}catch(Exception e){
		    logger.fatal("Exception isCBTExistsForHost ..."+e);
		    throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, "", e);
		}
		finally{
			//JTest changes on 01-feb-11 start
			LMSConnectionUtility.close(rs);
			LMSConnectionUtility.close(stmt);
			//JTest changes on 01-feb-11 ends
		}
		
		if(logger.isDebugEnabled())
			logger.debug("isCBTExistsForHost....Leaving");
		return isCBTExistForHost;
		
    }

// 	Generating n-leg accounting entries starts
	/**
	 *   Connection- con - 
	 *   processId - processId 
	 */	
	public static void generateAccountingEntryForBVTNLeg(Connection con, String accountingEntryForBVTNLeg, long processId, String status, PreparedStatement pstmt)throws LMSException	
	{
		if(logger.isDebugEnabled())//JTest changes on 01-feb-11
			logger.debug("Entering") ;		
		
		PreparedStatement lPstmt = null ;
		//String lTemp = null ;//JTest changes on 01-feb-11
		
		try
		{
			if(pstmt!=null){
				pstmt.setString(1, processId+"") ;
				pstmt.setString(2, status) ;
				pstmt.setString(3, processId+"") ;
				pstmt.setString(4, ExecutionConstants.PENDING) ;
				pstmt.addBatch();
				
			}else
			{
				lPstmt = con.prepareStatement(accountingEntryForBVTNLeg) ;
				lPstmt.setString(1, processId+"") ;
				lPstmt.setString(2, status) ;
				lPstmt.setString(3, processId+"") ;
				lPstmt.setString(4, ExecutionConstants.PENDING) ;
				
				String[] lValues = new String[4] ;
				lValues[0] = "'" + processId + "'" ;
				lValues[1] = "'" + status + "'" ;
				lValues[2] = "'" + processId + "'" ;
				lValues[3] = "'" + status + "'" ;			
				
				if(logger.isDebugEnabled())//JTest changes on 01-feb-11
					logger.debug("Insert Query " + DBUtils.logPreparedStatement(accountingEntryForBVTNLeg, lValues)) ;
	
				int count = lPstmt.executeUpdate() ;
				
				if(logger.isDebugEnabled())//JTest changes on 01-feb-11
					logger.debug("Inserted Record count : " + count ) ;
			}
		}
		catch(SQLException sqle)
		{
		   logger.fatal("SQL Exception Occured ", sqle ) ;
		   throw new LMSException("ML0003" , " SQL Exception occured") ;
		}
		finally
		{
			LMSConnectionUtility.close(lPstmt) ;
		}
		
		if(logger.isDebugEnabled())//JTest changes on 01-feb-11
			logger.debug("Leaving") ;
	}
	
	
	public static void generateAccountingEntryForBVTNLeg(Connection con, String accountingEntryForBVTNLeg, long processId)throws LMSException	
	{
		generateAccountingEntryForBVTNLeg(con, accountingEntryForBVTNLeg, processId, ExecutionConstants.PENDING, null);	
		
	}
	// Generating n-leg accounting entries Ends	
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
	
	/**
	 * -BILATERALSETTLEMENT - new method added
	 * @param args srcAETemplateDO
	 * @param args tgtAETemplateDO
	 * @param args srcBiLtaeralSettlementList
	 * @param args tgtBiLateralSettlementList
	 * @param args conn
	 * returns boolean
	 */
	private boolean isSettlementExistForBiLateral(AccountingEntryTemplateDO srcAETemplateDO,
		AccountingEntryTemplateDO tgtAETemplateDO,
		ArrayList srcBiLateralSettlementList,ArrayList tgtBiLateralSettlementList,
		Connection conn) throws LMSException{
	    PreparedStatement stmt = null;//JTest changes on 01-feb-11
	    ResultSet rs = null;

	    boolean isSettlementExist = false;	    
	    try{
		String entity1 = "";
		String entity2 = "";

		String codCcy = srcAETemplateDO.getCodCcy();

		if("HostSystem".equals(ExecutionConstants.ENTITY_TYPE)){
		    entity1 = srcAETemplateDO.getCodGl();
		    entity2 = tgtAETemplateDO.getCodGl();
		}else if("BLE".equals(ExecutionConstants.ENTITY_TYPE)){
		    entity1 = srcAETemplateDO.getCodBnkLglEnt();
		    entity2 = tgtAETemplateDO.getCodBnkLglEnt();
		}
		//ENH_Nostroloro_branch start
		else if(ExecutionConstants.NOSTRO_LORO_BRANCH.equals(ExecutionConstants.ENTITY_TYPE)){
		    entity1 = srcAETemplateDO.getCodBranch();
		    entity2 = tgtAETemplateDO.getCodBranch();
		}
		//ENH_Nostroloro_branch end
		int entity1Cnt = 0;
		int entity2Cnt = 0;
		/*
		 *  
		 */
		String sql = "SELECT OND.*, OA1.COD_BNK_LGL COD_BNK_LGL_SSP, OA1.COD_GL COD_GL_SSP, " +
			"OA1.COD_COUNTRY COD_COUNTRY_SSP, OA1.NBR_COREACCOUNT NBR_COREACCOUNT_SSP, " +
			"OA1.COD_BRANCH COD_BRANCH_SSP, OA2.COD_BNK_LGL COD_BNK_LGL_SO, " +
			"OA2.COD_GL COD_GL_SO, OA2.COD_COUNTRY COD_COUNTRY_SO, OA2.NBR_COREACCOUNT NBR_COREACCOUNT_SO, " +
			"OA2.COD_BRANCH COD_BRANCH_SO " +
			"FROM OLM_NOSTROLORO_MST ONM, OLM_NOSTROLORO_DTLS OND, ORBICASH_ACCOUNTS OA1, " +
			"ORBICASH_ACCOUNTS OA2 WHERE ONM.FLG_CLOSED = 'N' AND ONM.NBR_CONTRACT_NO = OND.NBR_CONTRACT_NO " +
			//JTest changes on 01-feb-11 starts
			/*"AND ONM.COD_LINKAGE_TYPE = '"+ExecutionConstants.SETTLEMENT+"' " +
			"AND ONM.TYP_ENTITY = '"+ExecutionConstants.ENTITY_TYPE+"' " +
			"AND (OND.COD_ENTITY_NOSTRO_SETTLEMENT IN ( '"+entity1+"','"+entity2+"') " +
			"OR OND.COD_ENTITY_LORO_SETTLEMENT IN( '"+entity1+"','"+entity2+"'))  " +
			"AND OND.COD_CCY = '"+codCcy+"' AND OND.NBR_ACCNO_MAIN_SETTELMENT = OA1.NBR_OWNACCOUNT " +*/
			"AND ONM.COD_LINKAGE_TYPE = ? " +
			"AND ONM.TYP_ENTITY = ? " +
			"AND (OND.COD_ENTITY_NOSTRO_SETTLEMENT IN ( ?, ?) " +
			"OR OND.COD_ENTITY_LORO_SETTLEMENT IN( ?, ?))  " +
			"AND OND.COD_CCY = ? AND OND.NBR_ACCNO_MAIN_SETTELMENT = OA1.NBR_OWNACCOUNT " +
			//JTest changes on 01-feb-11 ends
			"AND OND.NBR_ACCTNO_MIRROR_SETTELMENT = OA2.NBR_OWNACCOUNT " +
			"AND (SELECT MAX (DAT_TODAY) + 1 FROM OLM_HOSTSYSTEMDATES " +
			"WHERE COD_GL IN (OA1.COD_GL, OA2.COD_GL)) BETWEEN " +
			"OND.DAT_EFFECTIVE AND NVL (OND.DAT_END, TO_DATE ('31-DEC-9999', 'DD-MON-YYYY' ) )";
		//JTest changes on 01-feb-11 starts
		if(logger.isDebugEnabled())
			logger.debug("sql :: "+sql);

		stmt = conn.prepareStatement(sql);
		stmt.setString(1, ExecutionConstants.SETTLEMENT);
		stmt.setString(2, ExecutionConstants.ENTITY_TYPE);
		stmt.setString(3, entity1);
		stmt.setString(4, entity2);
		stmt.setString(5, entity1);
		stmt.setString(6, entity2);
		stmt.setString(7, codCcy);
		rs = stmt.executeQuery();
		//JTest changes on 01-feb-11 ends
		while(rs.next()){
		    isSettlementExist = true;
		    AccountingEntryTemplateDO srcMainSetAETemplateDO =  new AccountingEntryTemplateDO();
		    AccountingEntryTemplateDO srcMirrorSetAETemplateDO =  new AccountingEntryTemplateDO();
		    
		    AccountingEntryTemplateDO tgtMainSetAETemplateDO =  new AccountingEntryTemplateDO();
		    AccountingEntryTemplateDO tgtMirrorSetAETemplateDO =  new AccountingEntryTemplateDO();
		    
		    String mainEntity = rs.getString("COD_ENTITY_NOSTRO_SETTLEMENT");
		    String mirrorEntity = rs.getString("COD_ENTITY_LORO_SETTLEMENT");
		    
		    if(entity1.equalsIgnoreCase(mainEntity) || entity1.equalsIgnoreCase(mirrorEntity)){
			srcMirrorSetAETemplateDO.setNbrOwnAccount(rs.getString("NBR_ACCTNO_MIRROR_SETTELMENT"));
			srcMirrorSetAETemplateDO.setNbrCoreAccount(rs.getString("NBR_COREACCOUNT_SO"));
			srcMirrorSetAETemplateDO.setCodBnkLglEnt(rs.getString("COD_BNK_LGL_SO"));
			srcMirrorSetAETemplateDO.setCodCcy(codCcy);
			srcMirrorSetAETemplateDO.setCodCountry(rs.getString("COD_COUNTRY_SO"));
			srcMirrorSetAETemplateDO.setCodGl(rs.getString("COD_GL_SO"));
			srcMirrorSetAETemplateDO.setFlgAcctType("I");
			srcMirrorSetAETemplateDO.setFlgSrcTgtTyp("S");
			srcMirrorSetAETemplateDO.setFlgSettlementEntry("Y");
			srcMirrorSetAETemplateDO.setNbrTemplateId(srcAETemplateDO.getNbrTemplateId());
			srcMirrorSetAETemplateDO.setNbrContractNo(rs.getString("NBR_CONTRACT_NO"));
			srcMirrorSetAETemplateDO.setNbrSeqNo(rs.getString("NBR_SEQ"));
			srcMirrorSetAETemplateDO.setFlgPersist("Y");
			srcMirrorSetAETemplateDO.setNbrSettlementDays(srcAETemplateDO.getNbrSettlementDays());

		    	if(srcAETemplateDO.getDateEffective() != null && srcAETemplateDO.getDateEffective().compareTo(rs.getDate("DAT_EFFECTIVE")) > 0){
		    	    srcMirrorSetAETemplateDO.setDateEffective(srcAETemplateDO.getDateEffective());
		    	}else{
		    	    srcMirrorSetAETemplateDO.setDateEffective(rs.getDate("DAT_EFFECTIVE"));
		    	}

		    	srcMirrorSetAETemplateDO.setFlgDrcr(srcAETemplateDO.getFlgDrcr());
		    	srcMirrorSetAETemplateDO.setCodTriggeringGl(tgtAETemplateDO.getCodTriggeringGl());
		    	srcMirrorSetAETemplateDO.setCodBranch(rs.getString("COD_BRANCH_SO"));
		    	
		    	srcMirrorSetAETemplateDO.setFlgSettlementPath(srcAETemplateDO.getFlgSettlementPath());
		    	srcMirrorSetAETemplateDO.setDateEnd(rs.getDate("DAT_END"));
		    	
		    	srcMainSetAETemplateDO.setNbrOwnAccount(rs.getString("NBR_ACCNO_MAIN_SETTELMENT"));
		    	srcMainSetAETemplateDO.setNbrCoreAccount(rs.getString("NBR_COREACCOUNT_SSP"));
		    	srcMainSetAETemplateDO.setCodBnkLglEnt(rs.getString("COD_BNK_LGL_SSP"));
		    	srcMainSetAETemplateDO.setCodCcy(codCcy);
		    	srcMainSetAETemplateDO.setCodCountry(rs.getString("COD_COUNTRY_SSP"));
		    	srcMainSetAETemplateDO.setCodGl(rs.getString("COD_GL_SSP"));
		    	srcMainSetAETemplateDO.setFlgAcctType("I");
		    	srcMainSetAETemplateDO.setFlgSrcTgtTyp("T");
		    	srcMainSetAETemplateDO.setFlgSettlementEntry("Y");
		    	srcMainSetAETemplateDO.setNbrTemplateId(srcAETemplateDO.getNbrTemplateId());
		    	srcMainSetAETemplateDO.setNbrContractNo(rs.getString("NBR_CONTRACT_NO"));
		    	srcMainSetAETemplateDO.setNbrSeqNo(rs.getString("NBR_SEQ"));
		    	srcMainSetAETemplateDO.setFlgPersist("Y");
		    	srcMainSetAETemplateDO.setNbrSettlementDays(tgtAETemplateDO.getNbrSettlementDays());
		    	if(srcAETemplateDO.getDateEffective() != null && srcAETemplateDO.getDateEffective().compareTo(rs.getDate("DAT_EFFECTIVE")) > 0){
		    	    srcMainSetAETemplateDO.setDateEffective(srcAETemplateDO.getDateEffective());
		    	}else{
		    	    srcMainSetAETemplateDO.setDateEffective(rs.getDate("DAT_EFFECTIVE"));
		    	}

		    	srcMainSetAETemplateDO.setFlgDrcr(srcAETemplateDO.getFlgDrcr());
		    	srcMainSetAETemplateDO.setCodTriggeringGl(srcAETemplateDO.getCodTriggeringGl());
		    	srcMainSetAETemplateDO.setCodBranch(rs.getString("COD_BRANCH_SSP"));
		    	
		    	srcMainSetAETemplateDO.setFlgSettlementPath(srcAETemplateDO.getFlgSettlementPath());
		    	srcMainSetAETemplateDO.setDateEnd(rs.getDate("DAT_END"));
		    	
		    	if(entity1.equalsIgnoreCase(mainEntity)){
		    	    srcBiLateralSettlementList.add(srcMainSetAETemplateDO);
		    	    srcBiLateralSettlementList.add(srcMirrorSetAETemplateDO);
		    	}else{
		    	    srcBiLateralSettlementList.add(srcMirrorSetAETemplateDO);
		    	    srcBiLateralSettlementList.add(srcMainSetAETemplateDO);
		    	}
			entity1Cnt = entity1Cnt + 1;
		    }else if(entity2.equalsIgnoreCase(mainEntity) || entity2.equalsIgnoreCase(mirrorEntity)){
			tgtMirrorSetAETemplateDO.setNbrOwnAccount(rs.getString("NBR_ACCTNO_MIRROR_SETTELMENT"));
			tgtMirrorSetAETemplateDO.setNbrCoreAccount(rs.getString("NBR_COREACCOUNT_SO"));
			tgtMirrorSetAETemplateDO.setCodBnkLglEnt(rs.getString("COD_BNK_LGL_SO"));
			tgtMirrorSetAETemplateDO.setCodCcy(codCcy);
			tgtMirrorSetAETemplateDO.setCodCountry(rs.getString("COD_COUNTRY_SO"));
			tgtMirrorSetAETemplateDO.setCodGl(rs.getString("COD_GL_SO"));
			tgtMirrorSetAETemplateDO.setFlgAcctType("I");
			tgtMirrorSetAETemplateDO.setFlgSrcTgtTyp("S");
			tgtMirrorSetAETemplateDO.setFlgSettlementEntry("Y");
			tgtMirrorSetAETemplateDO.setNbrTemplateId(srcAETemplateDO.getNbrTemplateId());
			tgtMirrorSetAETemplateDO.setNbrContractNo(rs.getString("NBR_CONTRACT_NO"));
			tgtMirrorSetAETemplateDO.setNbrSeqNo(rs.getString("NBR_SEQ"));
			tgtMirrorSetAETemplateDO.setFlgPersist("Y");
			tgtMirrorSetAETemplateDO.setNbrSettlementDays(srcAETemplateDO.getNbrSettlementDays());

		    	if(tgtAETemplateDO.getDateEffective() != null && tgtAETemplateDO.getDateEffective().compareTo(rs.getDate("DAT_EFFECTIVE")) > 0){
		    	    tgtMirrorSetAETemplateDO.setDateEffective(tgtAETemplateDO.getDateEffective());
		    	}else{
		    	    tgtMirrorSetAETemplateDO.setDateEffective(rs.getDate("DAT_EFFECTIVE"));
		    	}

		    	tgtMirrorSetAETemplateDO.setFlgDrcr(srcAETemplateDO.getFlgDrcr());
		    	tgtMirrorSetAETemplateDO.setCodTriggeringGl(tgtAETemplateDO.getCodTriggeringGl());
		    	tgtMirrorSetAETemplateDO.setCodBranch(rs.getString("COD_BRANCH_SO"));
		    	
		    	tgtMirrorSetAETemplateDO.setFlgSettlementPath(srcAETemplateDO.getFlgSettlementPath());
		    	tgtMirrorSetAETemplateDO.setDateEnd(rs.getDate("DAT_END"));
		    	
		    	tgtMainSetAETemplateDO.setNbrOwnAccount(rs.getString("NBR_ACCNO_MAIN_SETTELMENT"));
		    	tgtMainSetAETemplateDO.setNbrCoreAccount(rs.getString("NBR_COREACCOUNT_SSP"));
		    	tgtMainSetAETemplateDO.setCodBnkLglEnt(rs.getString("COD_BNK_LGL_SSP"));
		    	tgtMainSetAETemplateDO.setCodCcy(codCcy);
		    	tgtMainSetAETemplateDO.setCodCountry(rs.getString("COD_COUNTRY_SSP"));
		    	tgtMainSetAETemplateDO.setCodGl(rs.getString("COD_GL_SSP"));
		    	tgtMainSetAETemplateDO.setFlgAcctType("I");
		    	tgtMainSetAETemplateDO.setFlgSrcTgtTyp("T");
		    	tgtMainSetAETemplateDO.setFlgSettlementEntry("Y");
		    	tgtMainSetAETemplateDO.setNbrTemplateId(srcAETemplateDO.getNbrTemplateId());
		    	tgtMainSetAETemplateDO.setNbrContractNo(rs.getString("NBR_CONTRACT_NO"));
		    	tgtMainSetAETemplateDO.setNbrSeqNo(rs.getString("NBR_SEQ"));
		    	tgtMainSetAETemplateDO.setFlgPersist("Y");
		    	tgtMainSetAETemplateDO.setNbrSettlementDays(tgtAETemplateDO.getNbrSettlementDays());
		    	if(tgtAETemplateDO.getDateEffective() != null && tgtAETemplateDO.getDateEffective().compareTo(rs.getDate("DAT_EFFECTIVE")) > 0){
		    	    tgtMainSetAETemplateDO.setDateEffective(tgtAETemplateDO.getDateEffective());
		    	}else{
		    	    tgtMainSetAETemplateDO.setDateEffective(rs.getDate("DAT_EFFECTIVE"));
		    	}

		    	tgtMainSetAETemplateDO.setFlgDrcr(tgtAETemplateDO.getFlgDrcr());
		    	tgtMainSetAETemplateDO.setCodTriggeringGl(tgtAETemplateDO.getCodTriggeringGl());
		    	tgtMainSetAETemplateDO.setCodBranch(rs.getString("COD_BRANCH_SSP"));
		    	
		    	tgtMainSetAETemplateDO.setFlgSettlementPath(tgtAETemplateDO.getFlgSettlementPath());
		    	tgtMainSetAETemplateDO.setDateEnd(rs.getDate("DAT_END"));
			
			if(entity2.equalsIgnoreCase(mainEntity)){
			    tgtBiLateralSettlementList.add(tgtMainSetAETemplateDO);
			    tgtBiLateralSettlementList.add(tgtMirrorSetAETemplateDO);
		    	}else{
		    	    tgtBiLateralSettlementList.add(tgtMirrorSetAETemplateDO);
		    	    tgtBiLateralSettlementList.add(tgtMainSetAETemplateDO);
		    	}
			entity2Cnt = entity2Cnt + 1;
		    }
		}
		
		if(entity1Cnt > 1 || entity2Cnt > 1){
		    HashMap hp = new HashMap();
		    if(entity1Cnt > 1)
			hp.put("COD_GLSRCACCT", srcAETemplateDO.getCodGl());
		    else if(entity2Cnt > 1)
			hp.put("COD_GLSRCACCT", tgtAETemplateDO.getCodGl());
		    else if(entity1Cnt > 1 && entity2Cnt > 1)
			hp.put("COD_GLSRCACCT", srcAETemplateDO.getCodGl()+","+tgtAETemplateDO.getCodGl());
		    
		    hp.put("COD_CCY", srcAETemplateDO.getCodCcy());
		    hp.put("MSGCODE", "M0162");
		    if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
		    	logger.debug("Message is :::::.... "+hp);
		    generateAlert(hp);
		    
		    if (entity2Cnt > 1 )
			tgtBiLateralSettlementList.clear();
		    if (entity1Cnt > 1 )
			srcBiLateralSettlementList.clear();
		}
		
	    }catch(Exception e){
		logger.fatal("Exception isSettlementExistForBiLateral ..."+e);
		throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, "", e);
		//JTEST FIX on 28ARIL11
	    }finally{
	    	DBUtils.closeResource(stmt); 
	    	DBUtils.closeResource(rs);
	    }
	    return isSettlementExist;
	}
	
	
	//Sweeps_ENH starts for issue id 2862
	//SUP_RBS_239 Starts
	//public void generateAEFromTemplateForICL(Connection connection, long nbrProcessId) throws LMSException{
	public void generateAEFromTemplateForICL(Connection connection, long nbrProcessId, String callType) throws LMSException{
		//SUP_RBS_239 Ends
		
		if(logger.isDebugEnabled()) {//Changes for ENH_15.2_LOG_FIXES
			logger.debug("Inside generateAEFromTemplateForICL.........");
			logger.debug("callType = "+callType);
		}
		PreparedStatement stmt = null;
		//JTest Changes 11.5.1.Starts
		PreparedStatement stmt1 = null;
		ResultSet rs1 = null;
		//JTest Changes 11.5.1.Ends
		StringBuilder sbuf=new StringBuilder();
	
		try{
			String testQuery = "Select count(*) from olm_accountingentry where nbr_processid = '"+String.valueOf(nbrProcessId)+"' and TXT_STATUS = '"+ExecutionConstants.PENDING+"'";
			//JTest Changes 11.5.1.Starts
			//PreparedStatement stmt1 = connection.prepareStatement(testQuery);
			stmt1 = connection.prepareStatement(testQuery);
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("Executing test query : "+testQuery);
			//ResultSet rs1 = stmt1.executeQuery();
			rs1 = stmt1.executeQuery();
			//JTest Changes 11.5.1.Ends
			if(rs1.next()){
				int count = rs1.getInt(1);
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("Count is "+count);
			}
			else
				logger.debug("No data found");

			sbuf.setLength(0);
			sbuf.append("INSERT INTO OLM_ACCOUNTINGENTRY (NBR_PROCESSID, NBR_ACCTENTRYID, NBR_LEGID, NBR_INSTRTXNID, NBR_LOTID, NBR_OWNACCOUNT, NBR_COREACCOUNT, FLG_ACCTTYPE, COD_BRANCH, ");
		    sbuf.append(" COD_BIC, COD_GL, COD_CCY, FLG_DRCR, AMT_TXN, DAT_VALUE, TXT_STATUS, COD_BNKLGLENT, TXT_IBANACCT, DAT_BUSINESS, FLG_SRCTGTTYP, FLG_MIRRORENTRY, DAT_SYSTEM, FLG_FORCEDR, ");
		    sbuf.append(" FLG_REVERSE, FLG_ISSETTLEMENT_ENTRY, COD_TRIGGERING_GL, COD_TXN, TXT_NARRATION1, COD_REF_ID, FLG_ENTRYTYPE, NBR_SETTLEMENT_DAYS,");
		    sbuf.append(" COD_INCOID, COD_AGREEMENT,TXT_REFCODE,TXT_REFTYPE )");
		    sbuf.append(" (SELECT DISTINCT CASE WHEN OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' ");
		    sbuf.append(" THEN '-2' ELSE ? END, OIA.NBR_ACCTENTRYID, OAD.NBR_LEGID, OIA.NBR_INSTRTXNID, OIA.NBR_LOTID, OAD.NBR_OWNACCOUNT, OAD.NBR_COREACCOUNT, ");
		    sbuf.append(" OAD.FLG_ACCTTYPE, OAD.COD_BRANCH, OAD.COD_BIC, OAD.COD_GL, OAD.COD_CCY, OAD.FLG_DRCR, OIA.AMT_TXN, OIA.DAT_VALUE, 'PENDING', OAD.COD_BNKLGLENT, OAD.TXT_IBANACCT, ");
		    sbuf.append(" OIA.DAT_BUSINESS, OAD.FLG_SRCTGTTYP, 'N', OIA.DAT_SYSTEM, OIA.FLG_FORCEDR, OIA.FLG_REVERSE, OAD.FLG_ISSETTLEMENT_ENTRY, OAD.COD_TRIGGERING_GL, CASE WHEN (OAD.FLG_SETTLEMENTPATH = 'C' ");
		    sbuf.append(" OR OAD.FLG_SETTLEMENTPATH = 'D') AND OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' THEN OIA.COD_TXN || 'CS' WHEN OAD.FLG_SETTLEMENTPATH = 'B' AND OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' ");
		    sbuf.append(" THEN OIA.COD_TXN || 'BS' ELSE OIA.COD_TXN END, OIA.TXT_NARRATION1, ");
		    sbuf.append(" OIA.FLG_ENTRYTYPE, OAD.NBR_SETTLEMENT_DAYS,OAD.FLG_SETTLEMENTPATH,OIA.COD_INCOID, ");
		    sbuf.append(" OIA.COD_AGREEMENT,OIA.TXT_REFCODE,OIA.TXT_REFTYPE FROM (SELECT OA.NBR_ACCTENTRYID, OA.NBR_INSTRTXNID, OA.NBR_LOTID, OA.AMT_TXN, OA.DAT_VALUE, OA.DAT_BUSINESS, OA.FLG_DRCR, OA.DAT_SYSTEM, OA.FLG_FORCEDR, ");
		    //sbuf.append(" OA.FLG_REVERSE, OA.COD_TXN, OA.TXT_NARRATION1, OA.FLG_ENTRYTYPE, OACC1.COD_CCY COD_CCYSRCACCT,OACC1.Cod_gl COD_GLSRCACCT,OACC2.Cod_gl COD_GLTGTACCT, ");\
		    //ENH_Nostroloro_branch start
		    sbuf.append(" OA.FLG_REVERSE, OA.COD_TXN, OA.TXT_NARRATION1, OA.FLG_ENTRYTYPE, OACC1.COD_CCY COD_CCYSRCACCT,OACC1.Cod_gl COD_GLSRCACCT,OACC2.Cod_gl COD_GLTGTACCT, oacc1.cod_branch cod_brsrcacct, oacc2.cod_branch cod_brtgtacct,");
		    //ENH_Nostroloro_branch end
		    //FIX_FOR_BLE START 
		    sbuf.append(" OACC1.COD_BNKLGLENT COD_SRCBNKLGLENT,OACC2.COD_BNKLGLENT COD_TGTBNKLGLENT,");
		    //FIX_FOR_BLE END
		    sbuf.append(" CASE WHEN OACC1.NBR_OWNACCOUNT = OA.NBR_OWNACCOUNT THEN OA.FLG_DRCR ELSE ( CASE WHEN OA.FLG_DRCR = 'D' THEN 'C' ELSE 'D' END) END FLG_SRCDRCR, ");
		    sbuf.append(" OA.COD_INCOID, OA.COD_AGREEMENT,OA.TXT_REFCODE,OA.TXT_REFTYPE FROM ORBICASH_ACCOUNTS_EXTERNAL OACC1, ORBICASH_ACCOUNTS_EXTERNAL OACC2, OLM_INCO_LOAN_DETAILS OILD, OLM_INCO_DEFINITION OIDN, OLM_ACCOUNTINGENTRY OA WHERE OA.NBR_PROCESSID = ? ");
		    
		    //SUP_RBS_239 Starts
		    // sbuf.append(" AND OA.TXT_STATUS = ? AND OA.COD_INCOID = OIDN.COD_INCOID AND OIDN.COD_INCOID = OILD.COD_INCOID AND OILD.NBR_HDRACCT = OACC1.NBR_OWNACCOUNT AND OILD.NBR_PARTICIPANTACCT = OACC2.NBR_OWNACCOUNT ) OIA, ");
		    sbuf.append(" AND OA.TXT_STATUS = ? AND OA.COD_INCOID = OIDN.COD_INCOID AND OIDN.COD_INCOID = OILD.COD_INCOID AND OILD.NBR_HDRACCT = OACC1.NBR_OWNACCOUNT AND OILD.NBR_PARTICIPANTACCT = OACC2.NBR_OWNACCOUNT " );
		    sbuf.append(" AND OACC1.ACCOUNT_TYPE=OILD.FLG_HDRACCTTYPE AND OACC2.ACCOUNT_TYPE=OILD.FLG_PARTICIPANTACCTTYPE ");
		    
		    if(callType != null && callType.equalsIgnoreCase(ExecutionConstants.TAX)){
		    	sbuf.append("AND oa.COD_TXN Like('%TAX%')");
		    }
		    	
		    sbuf.append(" ) OIA, ");
		    
		    //SUP_RBS_239 Ends
		    sbuf.append(" OLM_AETEMPLATE_MST OAM, OLM_AETEMPLATE_DTLS OAD, OPOOL_TXN_CODES OTC WHERE ");
		    if("BLE".equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
				
					sbuf.append("OIA.COD_SRCBNKLGLENT = OAM.COD_ENTITY1 AND OIA.COD_TGTBNKLGLENT=OAM.COD_ENTITY2 AND OIA.COD_CCYSRCACCT = OAM.COD_CCY ");
			    }
		    //ENH_Nostroloro_branch start
		    else if(ExecutionConstants.NOSTRO_LORO_BRANCH.equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
				
					sbuf.append("OIA.cod_brsrcacct = OAM.COD_ENTITY1 AND OIA.cod_brtgtacct=OAM.COD_ENTITY2 AND OIA.COD_CCYSRCACCT = OAM.COD_CCY ");
			    }
		    //ENH_Nostroloro_branch end
			    else{
				sbuf.append("OIA.COD_GLSRCACCT = OAM.COD_ENTITY1 AND OIA.COD_GLTGTACCT=OAM.COD_ENTITY2 AND OIA.COD_CCYSRCACCT = OAM.COD_CCY ");
			    }
		       
		    sbuf.append(" AND OAM.NBR_TEMPLATE_ID = OAD.NBR_TEMPLATE_ID AND OAD.FLG_DRCR_SET = OIA.FLG_SRCDRCR AND OAD.FLG_DRCR = OIA.FLG_DRCR AND ( CASE WHEN (OAD.FLG_SETTLEMENTPATH = 'C' OR OAD.FLG_SETTLEMENTPATH = 'D') AND OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' ") ;
		    sbuf.append(" THEN OIA.COD_TXN || 'CS' WHEN OAD.FLG_SETTLEMENTPATH = 'B' AND OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' THEN OIA.COD_TXN || 'BS' ELSE OIA.COD_TXN END) = OTC.COD_TXN AND OIA.DAT_BUSINESS BETWEEN OAD.DAT_EFFECTIVE AND NVL(OAD.DAT_END, TO_DATE('31-DEC-9999','DD-MON-YYYY'))  ) ");  
		      
		      
		    logger.fatal("***XB ICL****::::::generateAEFromTemplateForICL >>> Accounting Entry SQL :: "+sbuf.toString());
		    stmt = connection.prepareStatement(sbuf.toString());
		    stmt.setString(1, String.valueOf(nbrProcessId));
		    stmt.setLong(2, nbrProcessId);
		    stmt.setString(3, ExecutionConstants.PENDING);
		    
		    if(logger.isDebugEnabled())
			logger.debug("Suryabhan:::String.valueOf(nbrProcessId)>>-"+String.valueOf(nbrProcessId)  +"nbrProcessId>>-"+ nbrProcessId+"ExecutionConstants.PENDING>>-"+ExecutionConstants.PENDING);
		    
		    int insertCnt = stmt.executeUpdate();
		  
		    
		   	logger.fatal("generateAEFromTemplateForICL >>> Accounting Entry Insert Count :: "+insertCnt);
			
		  	
		}catch(Exception e){
		    logger.fatal("Exception generateAEFromTemplateForICL :: "+e);
		    throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, "", e);
		}
		finally{
			
			LMSConnectionUtility.close(stmt);
			//JTest Changes 11.5.1.Starts
			LMSConnectionUtility.close(stmt1);
			LMSConnectionUtility.close(rs1);
			//JTest Changes 11.5.1.Ends
		}
		if(logger.isDebugEnabled())
			logger.debug("Leaving generateAEFromTemplateForICL........");
	}
	//Sweeps_ENH Ends for issue id 2862
	//Sweeps_ENH starts for issue id 2862
	//SUP_RBS_239 Starts
	//public void generateAEFromTemplateForICL(Connection connection, long nbrProcessId) throws LMSException{
//LMS_45_PERF_CERTIFICATION_PF103 start
	public void generateAEFromTemplateForICL(Connection connection, long nbrProcessId, String callType , long nbr_lotid) throws LMSException{
		//SUP_RBS_239 Ends
		
		if(logger.isDebugEnabled()) {//Changes for ENH_15.2_LOG_FIXES
			logger.debug("Inside generateAEFromTemplateForICL.........");
			logger.debug("callType = "+callType);
		}
		PreparedStatement stmt = null;
		//JTest Changes 11.5.1.Starts
		PreparedStatement stmt1 = null;
		ResultSet rs1 = null;
		//JTest Changes 11.5.1.Ends
		StringBuilder sbuf=new StringBuilder();
	
		try{
			String testQuery = "Select count(*) from olm_accountingentry where nbr_processid = "+ nbrProcessId +" and TXT_STATUS = '"+ExecutionConstants.PENDING+"'";
			//JTest Changes 11.5.1.Starts
			//PreparedStatement stmt1 = connection.prepareStatement(testQuery);
			stmt1 = connection.prepareStatement(testQuery);
			if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
				logger.debug("Executing test query : "+testQuery);
			//ResultSet rs1 = stmt1.executeQuery();
			rs1 = stmt1.executeQuery();
			//JTest Changes 11.5.1.Ends
			if(rs1.next()){
				int count = rs1.getInt(1);
				if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
					logger.debug("Count is "+count);
			}
			else
				logger.debug("No data found");

			sbuf.setLength(0);
			sbuf.append("INSERT INTO OLM_ACCOUNTINGENTRY (NBR_PROCESSID, NBR_ACCTENTRYID, NBR_LEGID, NBR_INSTRTXNID, NBR_LOTID, NBR_OWNACCOUNT, NBR_COREACCOUNT, FLG_ACCTTYPE, COD_BRANCH, ");
		    sbuf.append(" COD_BIC, COD_GL, COD_CCY, FLG_DRCR, AMT_TXN, DAT_VALUE, TXT_STATUS, COD_BNKLGLENT, TXT_IBANACCT, DAT_BUSINESS, FLG_SRCTGTTYP, FLG_MIRRORENTRY, DAT_SYSTEM, FLG_FORCEDR, ");
		    sbuf.append(" FLG_REVERSE, FLG_ISSETTLEMENT_ENTRY, COD_TRIGGERING_GL, COD_TXN, TXT_NARRATION1, COD_REF_ID, FLG_ENTRYTYPE, NBR_SETTLEMENT_DAYS,");
		    sbuf.append(" COD_INCOID, COD_AGREEMENT,TXT_REFCODE,TXT_REFTYPE )");
		    sbuf.append(" (SELECT DISTINCT CASE WHEN OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' ");
		    sbuf.append(" THEN '-2' ELSE ? END, OIA.NBR_ACCTENTRYID, OAD.NBR_LEGID, OIA.NBR_INSTRTXNID, OIA.NBR_LOTID, OAD.NBR_OWNACCOUNT, OAD.NBR_COREACCOUNT, ");
		    sbuf.append(" OAD.FLG_ACCTTYPE, OAD.COD_BRANCH, OAD.COD_BIC, OAD.COD_GL, OAD.COD_CCY, OAD.FLG_DRCR, OIA.AMT_TXN, OIA.DAT_VALUE, 'PENDING', OAD.COD_BNKLGLENT, OAD.TXT_IBANACCT, ");
		    sbuf.append(" OIA.DAT_BUSINESS, OAD.FLG_SRCTGTTYP, 'N', OIA.DAT_SYSTEM, OIA.FLG_FORCEDR, OIA.FLG_REVERSE, OAD.FLG_ISSETTLEMENT_ENTRY, OAD.COD_TRIGGERING_GL, CASE WHEN (OAD.FLG_SETTLEMENTPATH = 'C' ");
		    sbuf.append(" OR OAD.FLG_SETTLEMENTPATH = 'D') AND OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' THEN OIA.COD_TXN || 'CS' WHEN OAD.FLG_SETTLEMENTPATH = 'B' AND OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' ");
		    sbuf.append(" THEN OIA.COD_TXN || 'BS' ELSE OIA.COD_TXN END, OIA.TXT_NARRATION1, ");
		    sbuf.append(" OIA.FLG_ENTRYTYPE, OAD.NBR_SETTLEMENT_DAYS,OAD.FLG_SETTLEMENTPATH,OIA.COD_INCOID, ");
		    sbuf.append(" OIA.COD_AGREEMENT,OIA.TXT_REFCODE,OIA.TXT_REFTYPE FROM (SELECT OA.NBR_ACCTENTRYID, OA.NBR_INSTRTXNID, OA.NBR_LOTID, OA.AMT_TXN, OA.DAT_VALUE, OA.DAT_BUSINESS, OA.FLG_DRCR, OA.DAT_SYSTEM, OA.FLG_FORCEDR, ");
		    //sbuf.append(" OA.FLG_REVERSE, OA.COD_TXN, OA.TXT_NARRATION1, OA.FLG_ENTRYTYPE, OACC1.COD_CCY COD_CCYSRCACCT,OACC1.Cod_gl COD_GLSRCACCT,OACC2.Cod_gl COD_GLTGTACCT, ");\
		    //ENH_Nostroloro_branch start
		    sbuf.append(" OA.FLG_REVERSE, OA.COD_TXN, OA.TXT_NARRATION1, OA.FLG_ENTRYTYPE, OACC1.COD_CCY COD_CCYSRCACCT,OACC1.Cod_gl COD_GLSRCACCT,OACC2.Cod_gl COD_GLTGTACCT, oacc1.cod_branch cod_brsrcacct, oacc2.cod_branch cod_brtgtacct,");
		    //ENH_Nostroloro_branch end
		    //FIX_FOR_BLE START 
		    sbuf.append(" OACC1.COD_BNK_LGL COD_SRCBNKLGLENT,OACC2.COD_BNK_LGL COD_TGTBNKLGLENT,");
		    //FIX_FOR_BLE END
		    sbuf.append(" CASE WHEN OACC1.NBR_OWNACCOUNT = OA.NBR_OWNACCOUNT THEN OA.FLG_DRCR ELSE ( CASE WHEN OA.FLG_DRCR = 'D' THEN 'C' ELSE 'D' END) END FLG_SRCDRCR, ");
		    sbuf.append(" OA.COD_INCOID, OA.COD_AGREEMENT,OA.TXT_REFCODE,OA.TXT_REFTYPE FROM ORBICASH_ACCOUNTS OACC1, ORBICASH_ACCOUNTS OACC2, OLM_INCO_LOAN_DETAILS OILD, OLM_ACCOUNTINGENTRY OA ,  OLM_TXNACCOUNT_INTERESTINFO intinfo WHERE OA.NBR_PROCESSID = ?   ");
	        if( nbr_lotid != -1){
	        	
	        	sbuf.append("and OA.NBR_LOTID = ?");
	        }
		    //SUP_RBS_239 Starts
		    // sbuf.append(" AND OA.TXT_STATUS = ? AND OA.COD_INCOID = OIDN.COD_INCOID AND OIDN.COD_INCOID = OILD.COD_INCOID AND OILD.NBR_HDRACCT = OACC1.NBR_OWNACCOUNT AND OILD.NBR_PARTICIPANTACCT = OACC2.NBR_OWNACCOUNT ) OIA, ");
		    
		    sbuf.append(" AND OA.TXT_STATUS = ? AND OA.COD_INCOID = OILD.COD_INCOID AND intinfo.NBR_PROCESSID = OA.NBR_PROCESSID AND intinfo.NBR_LOTID = OA.NBR_LOTID AND OILD.COD_INCOLOANID = intinfo.cod_entity AND OILD.NBR_HDRACCT = OACC1.NBR_OWNACCOUNT AND OILD.NBR_PARTICIPANTACCT = OACC2.NBR_OWNACCOUNT " );
		    sbuf.append(" AND 'I'=OILD.FLG_HDRACCTTYPE AND 'I'=OILD.FLG_PARTICIPANTACCTTYPE ");
		    
		    if(callType != null && callType.equalsIgnoreCase(ExecutionConstants.TAX)){
		    	sbuf.append("AND oa.COD_TXN Like('%TAX%')");
		    }
		    	
		    sbuf.append(" ) OIA, ");
		    
		    //SUP_RBS_239 Ends
		    sbuf.append(" OLM_AETEMPLATE_MST OAM, OLM_AETEMPLATE_DTLS OAD, OPOOL_TXN_CODES OTC WHERE ");
		    if("BLE".equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
				
					sbuf.append("OIA.COD_SRCBNKLGLENT = OAM.COD_ENTITY1 AND OIA.COD_TGTBNKLGLENT=OAM.COD_ENTITY2 AND OIA.COD_CCYSRCACCT = OAM.COD_CCY ");
			    }
		    //ENH_Nostroloro_branch start
		    else if(ExecutionConstants.NOSTRO_LORO_BRANCH.equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)){
				
					sbuf.append("OIA.cod_brsrcacct = OAM.COD_ENTITY1 AND OIA.cod_brtgtacct=OAM.COD_ENTITY2 AND OIA.COD_CCYSRCACCT = OAM.COD_CCY ");
			    }
		    //ENH_Nostroloro_branch end
			    else{
				sbuf.append("OIA.COD_GLSRCACCT = OAM.COD_ENTITY1 AND OIA.COD_GLTGTACCT=OAM.COD_ENTITY2 AND OIA.COD_CCYSRCACCT = OAM.COD_CCY ");
			    }
		       
		    sbuf.append(" AND OAM.NBR_TEMPLATE_ID = OAD.NBR_TEMPLATE_ID AND OAD.FLG_DRCR_SET = OIA.FLG_SRCDRCR AND OAD.FLG_DRCR = OIA.FLG_DRCR AND ( CASE WHEN (OAD.FLG_SETTLEMENTPATH = 'C' OR OAD.FLG_SETTLEMENTPATH = 'D') AND OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' ") ;
		    sbuf.append(" THEN OIA.COD_TXN || 'CS' WHEN OAD.FLG_SETTLEMENTPATH = 'B' AND OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' THEN OIA.COD_TXN || 'BS' ELSE OIA.COD_TXN END) = OTC.COD_TXN AND OIA.DAT_BUSINESS BETWEEN OAD.DAT_EFFECTIVE AND NVL(OAD.DAT_END, TO_DATE('31-DEC-9999','DD-MON-YYYY'))  ) ");  
		      
		      
		    logger.fatal("***XB ICL****::::::generateAEFromTemplateForICL >>> Accounting Entry SQL :: "+sbuf.toString());
		    stmt = connection.prepareStatement(sbuf.toString());
		    int count =1;
		    stmt.setString(count++, String.valueOf(nbrProcessId));
		    stmt.setLong(count++, nbrProcessId);
            if( nbr_lotid != -1){
	        	
    		    stmt.setLong(count++, nbr_lotid);
	        }
		    stmt.setString(count++, ExecutionConstants.PENDING);
		    
		    if(logger.isDebugEnabled())
			logger.debug("Suryabhan:::String.valueOf(nbrProcessId)>>-"+String.valueOf(nbrProcessId)  +"nbrProcessId>>-"+ nbrProcessId+"ExecutionConstants.PENDING>>-"+ExecutionConstants.PENDING);
		    
		    int insertCnt = stmt.executeUpdate();
		  
		    
		   	logger.fatal("generateAEFromTemplateForICL >>> Accounting Entry Insert Count :: "+insertCnt);
			
		  	
		}catch(Exception e){
		    logger.fatal("Exception generateAEFromTemplateForICL :: "+e);
		    throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, "", e);
		}
		finally{
			
			LMSConnectionUtility.close(stmt);
			//JTest Changes 11.5.1.Starts
			LMSConnectionUtility.close(stmt1);
			LMSConnectionUtility.close(rs1);
			//JTest Changes 11.5.1.Ends
		}
		if(logger.isDebugEnabled())
			logger.debug("Leaving generateAEFromTemplateForICL........");
	}
	//Sweeps_ENH Ends for issue id 2862
	//LMS_45_PERF_CERTIFICATION_PF103 end

   
    /**
     * @param args
     */
    public static void main(String[] args) {
	// TODO Auto-generated method stub

    }
	
	 // RBS - 153,154 ICL Nostro Leg
	/**
	 * @param referenceType
	 * @param nbrActiveTxnId
	 * @param conn
	 * @param pstmt
	 * @param pstmtSettlemet
	 * @throws LMSException
	 */
	private void getICLDetailsAndgenerateAE(String referenceType,
			String nbrActiveTxnId, Connection conn, PreparedStatement pstmt,
			PreparedStatement pstmtSettlemet) throws LMSException {
		if (logger.isDebugEnabled())
			logger.debug("getICLDetailsAndgenerateAE....");

		PreparedStatement stmt = null;
		boolean insertFlag = true;
		PreparedStatement updEffDate = null;
		ResultSet rs = null;

		try {
			int masterCount = generateAETemplateMaster(referenceType,
					nbrActiveTxnId, conn);
			if (logger.isDebugEnabled())
				logger.debug("masterCount...." + masterCount);

			if (masterCount > 0) {
				String insertDtlSql = "";

				if ("HostSystem"
						.equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)) {

					/*insertDtlSql = "SELECT DISTINCT osad.cod_glsrcacct, osad.cod_ccysrcacct, osad.flg_src_acc_type, osad.cod_gltgtacct, osad.cod_ccytgtacct, "
							+ "osad.flg_tgt_acc_type, oam.nbr_template_id FROM olm_activetransaction osad, olm_aetemplate_mst oam WHERE osad.NBR_ACTIVETRANSACTIONID = ? "
							+ "AND oam.typ_entity = ? AND ((   osad.cod_glsrcacct || osad.cod_gltgtacct || osad.cod_ccysrcacct ) IN "
							+ "(oam.cod_entity1 || oam.cod_entity2 || oam.cod_ccy) ) AND NOT EXISTS ( SELECT oad.nbr_template_id FROM olm_aetemplate_dtls oad "
							+ "WHERE oad.nbr_template_id = oam.nbr_template_id AND (SELECT MIN (dat_today) FROM olm_hostsystemdates "
							+ "WHERE cod_gl IN (osad.cod_glsrcacct, osad.cod_gltgtacct)) BETWEEN oad.dat_effective AND NVL "
							+ "(oad.dat_end, TO_DATE ('31-DEC-9999', 'DD-MON-YYYY' ) ))";*/
					insertDtlSql = " SELECT DISTINCT osad.cod_glsrcacct, osad.cod_ccysrcacct,osad.flg_src_acc_type, osad.cod_gltgtacct, osad.cod_ccytgtacct, osad.flg_tgt_acc_type, oam.nbr_template_id FROM "
						+ "((SELECT DISTINCT srcacct.COD_GL AS cod_glsrcacct,srcacct.COD_CCY AS cod_ccysrcacct, icl.flg_entity1type AS flg_src_acc_type, tgtacct.COD_GL AS cod_gltgtacct, tgtacct.COD_CCY AS cod_ccytgtacct, icl.flg_entity2type AS flg_tgt_acc_type,COD_INCOID "
						+ " FROM olm_inco_definition icl,orbicash_accounts srcacct, orbicash_accounts tgtacct "
						+ " where icl.cod_entity1 = srcacct.nbr_ownaccount AND icl.flg_entity1type = 'I'  AND icl.cod_entity2 = tgtacct.nbr_ownaccount AND icl.flg_entity2type = 'I') ) osad , "
						+ " olm_aetemplate_mst oam "
						+ " WHERE osad.COD_INCOID =? "
						+ " AND oam.typ_entity =? AND ((   osad.cod_glsrcacct || osad.cod_gltgtacct  || osad.cod_ccysrcacct ) IN (oam.cod_entity1 || oam.cod_entity2 || oam.cod_ccy) ) "
						+ " AND NOT EXISTS ( SELECT oad.nbr_template_id FROM olm_aetemplate_dtls oad "
						+ "   WHERE oad.nbr_template_id = oam.nbr_template_id AND (SELECT MIN (dat_today)  FROM olm_hostsystemdates   WHERE cod_gl IN  (osad.cod_glsrcacct, osad.cod_gltgtacct))  BETWEEN oad.dat_effective AND NVL (oad.dat_end,   TO_DATE ('31-DEC-9999',  'DD-MON-YYYY'          ) )) ";


				} else if ("BLE"
						.equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)) {
					/*insertDtlSql = "SELECT DISTINCT osad.cod_glsrcacct, osad.cod_ccysrcacct, osad.flg_src_acc_type, "
							+ "osad.cod_gltgtacct, osad.cod_ccytgtacct, osad.flg_tgt_acc_type, srcacct.cod_bnk_lgl cod_srcbnklglent, "
							+ "tgtacct.cod_bnk_lgl cod_tgtbnklglent, oam.nbr_template_id FROM olm_activetransaction osad, "
							+ "orbicash_accounts srcacct, orbicash_accounts tgtacct, olm_aetemplate_mst oam "
							+ "WHERE osad.NBR_ACTIVETRANSACTIONID = ? AND oam.typ_entity = ? AND osad.nbr_srcacct = srcacct.nbr_ownaccount "
							+ "AND osad.nbr_tgtacct = tgtacct.nbr_ownaccount AND ((   srcacct.cod_bnk_lgl || tgtacct.cod_bnk_lgl || osad.cod_ccysrcacct )"
							+ " IN (oam.cod_entity1 || oam.cod_entity2 || oam.cod_ccy) ) AND NOT EXISTS "
							+ "( SELECT oad.nbr_template_id FROM olm_aetemplate_dtls oad WHERE oad.nbr_template_id = oam.nbr_template_id "
							+ "AND (SELECT MIN (dat_today) FROM olm_hostsystemdates WHERE cod_gl IN (osad.cod_glsrcacct, osad.cod_gltgtacct)) "
							+ "BETWEEN oad.dat_effective AND NVL (oad.dat_end, TO_DATE ('31-DEC-9999', 'DD-MON-YYYY' ) ))";*/
					insertDtlSql = " SELECT DISTINCT osad.cod_glsrcacct, osad.cod_ccysrcacct, osad.flg_src_acc_type, osad.cod_gltgtacct,  osad.cod_ccytgtacct, osad.flg_tgt_acc_type, cod_srcbnklglent, cod_tgtbnklglent, oam.nbr_template_id "
						+ " FROM  ((SELECT DISTINCT srcacct.cod_gl AS cod_glsrcacct, srcacct.cod_ccy AS cod_ccysrcacct, icl.flg_entity1type AS flg_src_acc_type, tgtacct.cod_gl AS cod_gltgtacct,tgtacct.cod_ccy AS cod_ccytgtacct, "
						+ " icl.flg_entity2type AS flg_tgt_acc_type, srcacct.cod_bnk_lgl cod_srcbnklglent, tgtacct.cod_bnk_lgl cod_tgtbnklglent,cod_incoid "
						+ " FROM olm_inco_definition icl, orbicash_accounts srcacct, orbicash_accounts tgtacct "
						+ " WHERE icl.cod_entity1 = srcacct.nbr_ownaccount AND icl.flg_entity1type = 'I' AND icl.cod_entity2 = tgtacct.nbr_ownaccount AND icl.flg_entity2type = 'I')) osad,  olm_aetemplate_mst oam "
						+ " WHERE osad.cod_incoid =?  AND oam.typ_entity = ?   AND ((   osad.cod_srcbnklglent || osad.cod_tgtbnklglent || osad.cod_ccysrcacct ) IN (oam.cod_entity1 || oam.cod_entity2 || oam.cod_ccy)) "
						+ " AND NOT EXISTS ( "
						+ " SELECT oad.nbr_template_id FROM olm_aetemplate_dtls oad WHERE oad.nbr_template_id = oam.nbr_template_id AND (SELECT MIN (dat_today) FROM olm_hostsystemdates WHERE cod_gl IN (osad.cod_glsrcacct, osad.cod_gltgtacct)) BETWEEN oad.dat_effective AND NVL (oad.dat_end, "
						+ " TO_DATE ('31-DEC-9999', 'DD-MON-YYYY'  ) )) ";
				}
				// ENH_Nostroloro_branch start
				else if (ExecutionConstants.NOSTRO_LORO_BRANCH
						.equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)) {
					logger.debug("NOSTRO_LORO_BRANCH....");

					insertDtlSql = "SELECT DISTINCT srcacct.cod_gl as COD_GLSRCACCT, srcacct.cod_ccy as COD_CCYSRCACCT, osad.flg_entity1type as FLG_SRC_ACC_TYPE,"
						+ "tgtacct.cod_gl as COD_GLTGTACCT, tgtacct.cod_ccy as COD_CCYTGTACCT, osad.flg_entity2type as FLG_TGT_ACC_TYPE,"
						+ "srcacct.cod_branch as COD_BRSRCACCT, tgtacct.cod_branch as COD_BRTGTACCT, oam.nbr_template_id as NBR_TEMPLATE_ID"
						+ " FROM olm_inco_definition osad,"
						+ "olm_aetemplate_mst oam,"
						+ "(SELECT oae.cod_gl, oae.cod_ccy, oae.cod_branch,osad1.COD_INCOID"
						+ " FROM orbicash_accounts oae,olm_inco_definition osad1"
						+ " WHERE oae.nbr_ownaccount = osad1.cod_entity2"
						+ ") tgtacct,"
						+ "(SELECT oa.cod_gl, oa.cod_ccy, oa.cod_branch,osad2.COD_INCOID"
						+ " FROM orbicash_accounts oa,olm_inco_definition osad2"
						+ " WHERE oa.nbr_ownaccount = osad2.cod_entity1"
						+ ") srcacct"
						+ " WHERE osad.cod_incoid = ? and tgtacct.COD_INCOID = osad.COD_INCOID"
						+ " and srcacct.COD_INCOID = osad.COD_INCOID and srcacct.COD_INCOID = tgtacct.COD_INCOID"
						+ " AND oam.typ_entity = ?"
						+ " AND ((srcacct.cod_branch || tgtacct.cod_branch || srcacct.cod_ccy"
						+ ") IN (oam.cod_entity1 || oam.cod_entity2 || oam.cod_ccy)"
						+ ")"
						+ " AND NOT EXISTS ("
						+ "SELECT oad.nbr_template_id"
						+ " FROM olm_aetemplate_dtls oad"
						+ " WHERE oad.nbr_template_id = oam.nbr_template_id"
						+ " AND (SELECT MIN (dat_today)"
						+ " FROM olm_hostsystemdates"
						+ " WHERE cod_gl IN (srcacct.cod_gl, tgtacct.cod_gl))"
						+ " BETWEEN oad.dat_effective"
						+ " AND NVL (oad.dat_end,"
						+ " TO_DATE ('31-DEC-9999',"
						+ "'DD-MON-YYYY'"
						+ ")"
						+ "))";
						
						
				}
				// ENH_Nostroloro_branch end
				// JTest changes on 01-feb-11 starts
				if (logger.isDebugEnabled())
					logger.debug("insertDtlSql :: " + insertDtlSql);

				// stmt = conn.createStatement();
				stmt = conn.prepareStatement(insertDtlSql);
				stmt.setString(1, nbrActiveTxnId);
				stmt.setString(2, ExecutionConstants.ENTITY_TYPE);
				/*if ("Branch".equalsIgnoreCase(ExecutionConstants.ENTITY_TYPE)) {

					stmt.setString(3,
							ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER);
					stmt.setString(4, nbrActiveTxnId);
					stmt.setString(5, ExecutionConstants.ENTITY_TYPE);
					stmt.setString(6,
							ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER);
					stmt.setString(7,
							ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER);
					stmt.setString(8,
							ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER);
					stmt.setString(9, nbrActiveTxnId);
					stmt.setString(10, ExecutionConstants.ENTITY_TYPE);
					stmt.setString(11,
							ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER);
					stmt.setString(12,
							ExecutionConstants.TAXEFF_NOSTRO_SERVICE_PROVIDER);
				}*/
				
				// JTest changes on 01-feb-11 ends
				// Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005
				// Starts
				// Below Query Updated - BILATERALSETTLEMENT
				// Effective Date & End Date Updation
				/*
				 * String updateTemplateEffDate =
				 * "UPDATE OLM_AETEMPLATE_DTLS SET DAT_EFFECTIVE = (SELECT MAX (DAT_EFFECTIVE) "
				 * +
				 * "FROM OLM_AETEMPLATE_DTLS WHERE NBR_TEMPLATE_ID = ? ), DAT_END = "
				 * +
				 * "(SELECT MIN (DAT_END) FROM OLM_AETEMPLATE_DTLS WHERE NBR_TEMPLATE_ID = ?) "
				 * + "WHERE NBR_TEMPLATE_ID = ? ";
				 */
				String updateTemplateEffDate = "UPDATE OLM_AETEMPLATE_DTLS OAD SET(DAT_EFFECTIVE,DAT_END) = "
						+ "(SELECT CASE WHEN OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' "
						+ "AND B.EFFDATE < C.EFFDATE THEN C.EFFDATE ELSE B.EFFDATE END, "
						+ "CASE WHEN OAD.FLG_ISSETTLEMENT_ENTRY = 'Y' "
						+ "AND B.DATEND > C.DATEND THEN C.DATEND ELSE B.DATEND END "
						+ "FROM( SELECT NBR_TEMPLATE_ID,FLG_ISSETTLEMENT_ENTRY,"
						+ "MAX(DAT_EFFECTIVE) EFFDATE,MIN (DAT_END) DATEND "
						+ "FROM OLM_AETEMPLATE_DTLS WHERE  NBR_TEMPLATE_ID = ? "
						+ "GROUP BY NBR_TEMPLATE_ID,FLG_ISSETTLEMENT_ENTRY )B, "
						+ "(SELECT NBR_TEMPLATE_ID,MAX(EFFDATE) EFFDATE,MIN (DATEND) DATEND "
						+ "FROM ( SELECT NBR_TEMPLATE_ID,FLG_ISSETTLEMENT_ENTRY,MAX(DAT_EFFECTIVE) EFFDATE,"
						+ "MIN (DAT_END) DATEND FROM OLM_AETEMPLATE_DTLS WHERE  NBR_TEMPLATE_ID = ? "
						+ "GROUP BY NBR_TEMPLATE_ID,FLG_ISSETTLEMENT_ENTRY )GROUP BY NBR_TEMPLATE_ID ) C "
						+ "WHERE B.NBR_TEMPLATE_ID = C.NBR_TEMPLATE_ID  "
						+ "AND OAD.FLG_ISSETTLEMENT_ENTRY  = B.FLG_ISSETTLEMENT_ENTRY "
						+ "AND OAD.NBR_TEMPLATE_ID = ?  ) WHERE NBR_TEMPLATE_ID = ? ";
				if (logger.isDebugEnabled())// JTest changes on 01-feb-11
					logger.debug("updateTemplateEffDate :: "
							+ updateTemplateEffDate);
				// - BILATERALSETTLEMENT
				updEffDate = conn.prepareStatement(updateTemplateEffDate); // JTest
																			// changes
																			// on
																			// 01-feb-11
				// Effective Date & End Date Updation
				// Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005
				// Ends
				rs = stmt.executeQuery();// JTest changes on 01-feb-11
				while (rs.next()) {
					legCount = 3;
					flgPersistTemplate = true;
					insertFlag = false;

					AccountingEntryTemplateDO srcTemplateDo = new AccountingEntryTemplateDO();
					AccountingEntryTemplateDO tgtTemplateDo = new AccountingEntryTemplateDO();
					populateAETemplateDO(rs, srcTemplateDo, tgtTemplateDo);

					if (logger.isDebugEnabled()) {// JTest changes on 01-feb-11
						logger.debug("Calling generateAETemplateDetail For Entity1"
								+ srcTemplateDo);
						logger.debug("Calling generateAETemplateDetail For Entity2"
								+ tgtTemplateDo);
					}
					generateAETemplateDetail(srcTemplateDo, tgtTemplateDo,
							"DO", conn, pstmt, pstmtSettlemet);

					if (flgPersistTemplate) {
						int a[] = pstmt.executeBatch();
						insertFlag = true;
						if (logger.isDebugEnabled())// JTest changes on
													// 01-feb-11
							logger.debug("getCSDetailsAndgenerateAE...."
									+ a.length);
					} else {
						HashMap hp = new HashMap();
						hp.put("COD_GLSRCACCT", srcTemplateDo.getCodGl());
						hp.put("COD_GLTGTACCT", tgtTemplateDo.getCodGl());
						hp.put("COD_CCY", srcTemplateDo.getCodCcy());
						// Retro From LIQ_RELEASE10.2_SIT013 to
						// LIQ_RELEASE10.4_SIT005 Starts
						// Jiten - BILATERALSETTLEMENT
						hp.put("MSGCODE", "M0161");

						if (logger.isDebugEnabled()){// JTest changes on
													// 01-feb-11
							logger.debug("Message is :::::.... " + hp);
						}//Changes for ENH_15.2_LOG_FIXES

						// - Jiten BILATERALSETTLEMENT - Commented the Code as
						// Method name changed
						// alertForPathNotCompleted(hp);
						generateAlert(hp);
						// Retro From LIQ_RELEASE10.2_SIT013 to
						// LIQ_RELEASE10.4_SIT005 Ends
					}

					if (insertFlag) {
						int a[] = pstmtSettlemet.executeBatch();
						if (logger.isDebugEnabled()){// JTest changes on
													// 01-feb-11
							logger.debug("getCSDetailsAndgenerateAE...."
									+ a.length);
						}	//Changes for ENH_15.2_LOG_FIXES
						// Retro From LIQ_RELEASE10.2_SIT013 to
						// LIQ_RELEASE10.4_SIT005 Starts
						// Effective Date & End Date Updation
						updEffDate
								.setString(1, rs.getString("NBR_TEMPLATE_ID"));
						updEffDate
								.setString(2, rs.getString("NBR_TEMPLATE_ID"));
						updEffDate
								.setString(3, rs.getString("NBR_TEMPLATE_ID"));
						// - BILATERALSETTLEMENT
						updEffDate
								.setString(4, rs.getString("NBR_TEMPLATE_ID"));
						// - BILATERALSETTLEMENT

						int updCnt = updEffDate.executeUpdate();
						if (logger.isDebugEnabled()){// JTest changes on
													// 01-feb-11
							logger.debug("Updating the Effective Date & End Date Count...."
									+ updCnt);
						}//Changes for ENH_15.2_LOG_FIXES
						// Effective Date & End Date Updation
						// Retro From LIQ_RELEASE10.2_SIT013 to
						// LIQ_RELEASE10.4_SIT005 Ends
					}

					pstmt.clearBatch();
					pstmtSettlemet.clearBatch();
					// Retro From LIQ_RELEASE10.2_SIT013 to
					// LIQ_RELEASE10.4_SIT005 Starts
					// Effective Date & End Date Updation
					updEffDate.clearParameters();
					// Effective Date & End Date Updation
					// Retro From LIQ_RELEASE10.2_SIT013 to
					// LIQ_RELEASE10.4_SIT005 Ends
				}
				// Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005
				// Starts
				// Effective Date & End Date Updation
				LMSConnectionUtility.close(updEffDate);
				// Effective Date & End Date Updation
				// Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005
				// Ends
			}
		} catch (Exception e) {
			logger.fatal("getICLDetailsAndgenerateAE...." + e);
			throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, "", e);
		} finally {
			// JTest changes on 01-feb-11 starts
			LMSConnectionUtility.close(rs);
			LMSConnectionUtility.close(updEffDate);
			LMSConnectionUtility.close(stmt);
			// JTest changes on 01-feb-11 ends
		}
		if (logger.isDebugEnabled())// JTest changes on 01-feb-11
			logger.debug("getICLDetailsAndgenerateAE....Leaving");
		return;
	}

	 // RBS - 153,154 ICL Nostro Leg

}
