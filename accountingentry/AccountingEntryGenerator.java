
/**
 * *    These materials are confidential and proprietary to Intellect Design Arena Ltd.
 *  and no part of these materials should be reproduced, published, transmitted or
 *  distributed in any form or by any means, electronic, mechanical, photocopying,
 *  recording or otherwise, or stored in any information storage or retrieval system
 *  of any nature nor should the materials be disclosed to third parties or used in
	 *  any other manner for which this is not authorized, without the prior express
 *  written authorization of Intellect Design Arena Ltd.
 *
 * <p>Title       : Intellect Liquidity</p>
 * <p>Description : This class geneartes the the accounting entries and sets the values in
 *                  the prpepared statement to insert accounting entries in the OLM_ACCOUNTINGENTRY table </p>
 * <p>Copyright   : Copyright 2016 Intellect Design Arena Limited. All rights reserved.</p>
 * <p>Company     : Intellect Design Arena Limited</p>
 * <p>Date of Creation : 23-May-2005</p>
 * <p>Source      : AccountingEntryGenerator.java </p>
 * <p>Package     : com.intellectdesign.cash.accountingentry  </p>
 * @author Ritesh Grover
 * @version 1.0
 * <p>------------------------------------------------------------------------------------</p>
 * <p>MODIFICATION HISTORY:</p>
 * <p>------------------------------------------------------------------------------------</p>
 * <p>SERIAL      AUTHOR		    DATE		SCF	DESCRIPTION</p>
 * <p>------------------------------------------------------------------------------------</p>
 * <p>1           Ritesh Grover     21/02/2006  	Initial Version.</p>
 * <p>2           Girija		    17/03/2006  INT_1008Removed checking of FlgConsolidation at POOL LEVEL.
 * 														It will go as a column COD_FIELD1 IN OLM_ACCOUNTINGENTRY
 * 												INT_1002Bank Share Treasury Share
 * <p>3       	Girija		    	23-Mar-2006	INT_1009 removed the condition of Host in the queries as we should do it only for Lot
 * <p>4       	Girija		    	24-Mar-2006	INT_1027 corrected posting interest query FOR adding nvl for minimum amount interest condition when posting
 * <p>5       	Girija		    	28-Mar-2006	INT_1025 removed validation for IBAN check
 * <p>6       	Girija		    	31-Mar-2006	INT_1026  Add TO_NUMBER to BVTID

 * <p>7           Anirban     		05/04/2006  BVT_TXN 	BVT Accounting Entries</p>
 * <p>8           Anirban     		06/04/2006  BVT_TXN Inc 	BVT Accounting Entries incremental change</p>
 * <p>9           Girija     		06/04/2006  SUP_1028  Handle Reallocation Entries from Parent</p>
 * <p>10          Girija     		12/04/2006  SUP_1040  From Accrual Posting query, remove the condition TYP_ACCTINT_CRACT <> 'SUPP' AND TXN.TYP_ACCTINT_DRACT <> 'SUPP')<P>
 * <p>11		  Anirban			12/04/2006	INT_1039  Zero carryforward will not get inserted into database.<P>
 * 														  Caching of unposted amounts are done.<P>
 * <p>12          Girija     		26/04/2006  SUP_1045  Corrected Posting Queries</p>
 * <p>13		  Anirban Roy		08/05/2006	SUP_1031  Decimal Diff in accrual and posting amounts due to rounding off error.</p>
 * <p>14	  Deepak K. Menghani    10/05/2006	INT_1055  DAT_SENT to have (ValueDate + 1)<P>
 * <p>15	      Gopal Kosaraju	 					  DAT_SENT to have (ValueDate + 1) for BVT<P>
 * <p>16	      Girija	 		16/05/2006  SUP_1062  for performing Accrual on Accrual Date and using AdjustedAccrual for DAT_POSTING IN OLM_ACCOUNTINGENTRY TABLE<P>
 * <p>17		  Anirban			22/05/2006	INT_1017  Participant End Accrual and Posting.<P>
 * <p>18		  Sachin			30/05/2006	SUP_1073  Accounting entries should be generated on dat_posting value.<P>
 * <p>19	   Gopal Kosaraju	 	31/05/2006	BVI_BTS	  Bank Share, Treasury Share
 *    20       Deepak K. Menghani   31/05/2006	INT_1069  Updation of NextDebitDelay and NextCreditDelay for BVT<P>
 * <p>21		Gopal				06-Jun-2006	BVT_AE	  BVTAccrualBasedOnPosting
 * <p>22       Anirban Roy   		21/06/2006	SUP_1078  Accrual and Posting of account getting closed is done onlne,<P>
 * <P>													  NonWorking Adjustments done for participant end accrual and posting<P>
 * <p>23        Gopal  				Jul-07-2006	SUP_1079a  minAmtCredit/minAmtDebit limits not to be applied.</p>
 * <p>24        Gopal  				Jul-26-2006	SUP_1082b  For BVT accrual for posting of adjustment entries needed</p> 
 * <p>25        Gopal  				Jul-26-2006	SUP_1082a  For BVT accrual date and adjusted accrual date should be >= current date</p>
 * <p>26        Gopal  				Jul-27-2006	SUP_1082c  value date of BVT posting entries & accrual for last cycle fix</p>  
 * <p>27        Sachin Patil  		Oct-31-2006	SUP_2034:Issue659  	statement was getting closed in while loop... this is removed</p>
 * <p>28        Gopal		  		Nov-14-2006	SUP_2039:Issue672  	BVT Pool Settlement bug-fix</p>  
 * <p>29        Gopal		  		Nov-27-2006	ENH_10007  	System Date to Host Date</p>
 * <p>30	Amey Kulkarni			Jun-19-2007	ENH_LIQ_0005	Liquidity Execution</p>
 * <p>31        Gopal		  		Jan-19-2007	ENH_1105  	Set hard-coded internal acc</p>
 * <p>32        Gopal		  		Feb-26-2007	SUP_HSBC_025  	Allocation Credit and Debit constants added  for BVT</p>
 * <p>33        Gopal		  		Mar-29-2007	SUP_HSBC_025  	BVT Allocation posting is happening twice</p>
 * <p>33        Gopal		  		Apr-12-2007	ENH_1110  	Participants ending on today's business date Fix </p>
 * <p>34        Gopal		  		Apr-24-2007	SUP_HSBC_041  	TYP_PARTICIPANT = 'ACCOUNT' added for OPOOL_TXNPOOL_PARTYDTLS joins in case of BVT </p>
 * <p>35        Vaibhav Sharma		Jun-14-2007	SUP_HSBC_019	Checking participant for the same run date. </p>
 * <p>36        Gopal				Jun-16-2007	SUP_HSBC_072	No need to check whether the participant dat_end has been null or > business date through out</p> 
 * <p>37	Satish 				Jun-09-2007	ENH_IS686 changes made  for Posting Cycle added one argument in methods	</p>
 * <p>      28/04/2011			Vijay Kadam				JTEST FIX on 28ARIL11		JTest JDBC changes</p>
 * <p>      20/09/2011			Abhisek Sawant			JT fix 20SEP11</p> 
 * <p>      28/04/2012			Vijay Kadam 			J-Test OOP.HIF-3</p>
 * <p>      17/01/2012			Nishant Sapdhare		SystemErrLogs issue		logs added</p>
 * <p>      16/04/2012			Prateek Aole		JTest Fixes OPT.SBS-3 & OPT.USCL-1		</p>
 * <p>		15/03/2012			Vipul Depal				Mashreq_AE_Hook		Hook provided for Customizing Accounting Entry</p>
 * <p>		20/08/2015			Dipankar Biswas		15.1.1_Resource_Leaks_Fix  Fix</p>
 * <p>45    09/09/2015          Bibek Satapathy 	 ENH_15.2_LOG_FIXES   Logger enabling Check Added </p>
 * <p>		03/11/2015			Apurva Goyani		15.2_JTest_ResultSet_Loop  Fix</p> 
 */
package com.intellectdesign.cash.accountingentry;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
//SUP_1082 Starts
import java.util.Calendar;
//SUP_1082 Ends
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.naming.NamingException;
import javax.swing.text.html.HTMLDocument.HTMLReader.PreAction;

import org.apache.fop.viewer.LoadableProperties;
//ENH_LIQ_0005  Starts
import com.intellectdesign.cash.lms.constants.LMSConstants;
import com.intellectdesign.cash.lms.execution.pooling.SweepsTriggerDetails;
import com.intellectdesign.cash.lms.execution.pooling.constant.ExecutionConstants;
import com.intellectdesign.cash.lms.execution.bvt.entity.BVTDetails;
import com.intellectdesign.cash.lms.execution.pooling.util.ExecutionUtility;
import com.intellectdesign.cash.lms.execution.sweeps.util.CustomisationHelper;
import com.intellectdesign.cash.lms.execution.sweeps.util.CustomisationHelperFactory;
//ENH_LIQ_0005  Ends
import com.intellectdesign.cash.sweeps.calendar.CalendarService;//SUP_1078
import com.intellectdesign.cash.sweeps.util.DBUtility;
import com.intellectdesign.cash.sweeps.util.LMSConnectionUtility;
import com.intellectdesign.cash.sweeps.util.LMSUtility;
import com.intellectdesign.exception.LMSException;
import com.polaris.intellect.commons.logging.Log;
import com.polaris.intellect.commons.logging.LogFactory;


public class AccountingEntryGenerator {
	private static Log logger = LogFactory.getLog(AccountingEntryGenerator.class.getName());
	//INT_1039--Started
	private HashMap _hmCR_INT_UNPOSTEDMap=new HashMap();
	private HashMap _hmDR_INT_UNPOSTEDMap=new HashMap();
	private HashMap _hmNET_INT_UNPOSTEDMap=new HashMap();
	private HashMap _hmALLOCATIONCR_UNPOSTEDMap=new HashMap();//SUP_1031
	private HashMap _hmALLOCATIONDR_UNPOSTEDMap=new HashMap();//SUP_1031
	private HashMap _hmCENTRAL_INT_UNPOSTEDMap=new HashMap();
	private HashMap _hmREALLOCATION_UNPOSTEDMap=new HashMap();
	//INT_1039--Ends
	//SUP_1031--Starts
	private HashMap _hmCR_INT_UNACCRUEDMap=new HashMap();
	private HashMap _hmDR_INT_UNACCRUEDMap=new HashMap();
	private HashMap _hmNET_INT_UNACCRUEDMap=new HashMap();
	private HashMap _hmALLOCATIONCR_UNACCRUEDMap=new HashMap();//SUP_1031
	private HashMap _hmALLOCATIONDR_UNACCRUEDMap=new HashMap();//SUP_1031
	//SUP_1031--Ends

	private HashMap _hmKeyAccountDO;
//SUP_1031--Starts
	/*
	private HashMap _hmCurrCarryFwdMap;
	private HashMap _hmCurrNumCarryFwdMap;
	private HashMap _hmCurrTruncMap;
	private HashMap _hmCurrDecimalMap;
	*/
//SUP_1031--Ends
	private static String seqNoQuery = "SELECT "+DBUtility.getSeqSyntax("SEQ_ACCTENTRY")+", COD_PRODUCT FROM OLM_PROCESSQUEUE WHERE NBR_PROCESSID = ?";
	private BigDecimal zero = new BigDecimal(0);
	//SUP_1082a starts
	//Store the accrual dates and posting dates of the pools executed for the BVT in the cache
	private HashMap accCache = new HashMap(); 
	private HashMap postCache = new HashMap();
	//SUP_1082a ends

	boolean[] accrualEntries = {false,false,false,false,false};//for executing preparedStatemnts of Accrual Entries in LotProcessorBean
	boolean[] postingEntries = {false,false,false,false,false};	//for executing preparedStatemnts of Posting Entries in LotProcessorBean

	public AccountingEntryGenerator(String product,long lotId) throws LMSException{//INT_1039
		//load the product properties file.
		//chack for the product select.
		// Load the properties file for the corresponding product.
		_hmKeyAccountDO = new HashMap();
		//SUP_1031--Starts
		//_hmCurrCarryFwdMap = new HashMap();
		//_hmCurrNumCarryFwdMap = new HashMap();
		//_hmCurrTruncMap = new HashMap();
		//_hmCurrDecimalMap = new HashMap();
		//SUP_1031--Ends
		try {
			//fillHashmap();//SUP_1031s
			logger.debug("Calling populateUnpostedAmountsCache ");//INT_1039
			populateUnpostedAmountsCache(lotId);//INT_1039
			populateUnaccruedAmountsCache(lotId);//SUP_1031
		}
		catch (LMSException e)
		{
			throw e;
		}
	}
	//Mashreq_AE_Hook
	public AccountingEntryGenerator(){
		
	} // Mashreq_AE_Hook ends
	//SUP_1031--Starts
	/*
	private void fillHashmap() throws LMSException
	{
		Statement stmt = null;
		ResultSet rs=null;
		Connection conn = null;


		try
		{
			conn = LMSConnectionUtility.getConnection();
			stmt = conn.createStatement();


			//First get the Mappings from OPOOL_CURRLINK
			String s_OpoolCurrLink = "SELECT COD_CCY, FLG_CARRYFWD, NUM_CARRYFWD_DECIMAL FROM OPOOL_CURRLINK";

			rs = stmt.executeQuery(s_OpoolCurrLink);
			if (logger.isDebugEnabled())
				logger.debug("Select Query executed :" + s_OpoolCurrLink);

			boolean results = false;

			while(rs.next())
			{
				results =  true;
				String s_codCCY = rs.getString("COD_CCY");
				String s_flgCarryFwd = rs.getString("FLG_CARRYFWD");
				BigDecimal bd_numcarryFwd = rs.getBigDecimal("NUM_CARRYFWD_DECIMAL");

				if (logger.isDebugEnabled())
					logger.debug("Values retrieved:CodCCY:" + s_codCCY + ",FLG:" + s_flgCarryFwd + ",NUM:"+bd_numcarryFwd);

				_hmCurrCarryFwdMap.put(s_codCCY,s_flgCarryFwd);
				_hmCurrNumCarryFwdMap.put(s_codCCY,bd_numcarryFwd);
			}
			if (!results)
			{
				logger.fatal(" No records for OPOOL_CURRLINK" + s_OpoolCurrLink);
			}

			//Now set the mappings from ORBICASH_CURRENCY
			String s_OrbicashCurrency = "SELECT COD_CCY, FLG_TRUNCATE, NBR_DECIMAL FROM ORBICASH_CURRENCY";

			rs = stmt.executeQuery(s_OrbicashCurrency);
			if (logger.isDebugEnabled())
				logger.debug("Select Query executed :" + s_OrbicashCurrency);

			results = false;

			while(rs.next())
			{
				results = true;
				String s_codccy = rs.getString("COD_CCY");
				_hmCurrTruncMap.put(s_codccy,rs.getString("FLG_TRUNCATE"));
				_hmCurrDecimalMap.put(s_codccy,rs.getBigDecimal("NBR_DECIMAL"));
			}
			if (!results)
			{
				logger.fatal(" No records for ORBICASH_CURRENCY" + s_OrbicashCurrency);
			}
		}
		catch (Exception e)
		{
			logger.fatal(
			  "Exception in filling hashmap", e);
		  throw new LMSException("101300", e.toString());
		}
		finally
		{
			  LMSConnectionUtility.close(rs);
			  LMSConnectionUtility.close(stmt);
			  LMSConnectionUtility.close(conn);
		}
	}
	*/
	//SUP_1031--Ends
	/**
	 * @param pstmtAccountingEntry
	 * @param type
	 * @param generateFor
	 * @param generateForId
	 * @param product
	 */
	public boolean[] generateAccountingEntries(PreparedStatement pstmtAccountingEntry,PreparedStatement pstmtTxnPoolPartyDtls,
			PreparedStatement pstmtTxnPoolAllocation,PreparedStatement pstmtAccountUnposted,PreparedStatement pstmtAcctUnpostedInsert , String type, String generateFor, String generateForId, String product,
			Date businessdate, String client,HashMap hpPoolList )throws LMSException{
		boolean entries[] = null;
		if (logger.isDebugEnabled())
			logger.debug("Entering");

		try{
			if(type!=null){
			if((ExecutionConstants.ACCRUAL).equalsIgnoreCase(type.trim())){
				generateAccrualEntries(pstmtAccountingEntry,
						pstmtTxnPoolPartyDtls,
						pstmtTxnPoolAllocation,pstmtAccountUnposted,pstmtAcctUnpostedInsert, generateFor,
						generateForId,businessdate, client, hpPoolList  );
				entries= accrualEntries;
			}
			else if((ExecutionConstants.POSTING).equalsIgnoreCase(type.trim())){
				generatePostingEntries(pstmtAccountingEntry,
						pstmtTxnPoolPartyDtls, pstmtTxnPoolAllocation,pstmtAccountUnposted,
						pstmtAcctUnpostedInsert,generateFor,
						generateForId,businessdate,client );
				entries= postingEntries;
			}
			}
		}
		/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//		catch(Exception e){
		catch(SQLException e){
		/** ****** FindBugs Fix End ***********  on 14DEC09*/
			logger.fatal("General Exception: ", e);
			throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, e.getMessage());
		}
		return entries;
	}

	private boolean[] generateAccrualEntries(PreparedStatement pstmtAccountingEntry,PreparedStatement pstmtTxnPoolPartyDtls,
				PreparedStatement pstmtTxnPoolAllocation,PreparedStatement pstmtAccountUnposted,
				PreparedStatement pstmtAcctUnpostedInsert, String generateFor,
				String generateForId,Date businessdate, String client,HashMap hpPoolList  )
				throws LMSException, SQLException{

			String sqlSelectAccnts;
			String sqlString;
			String sqlGroupAccnts;
			StringBuilder sbufWhereAccnts = new StringBuilder("");
			//INT_1017
			String sqlSelectAccntsSuddenEnd;
			String sqlGroupAccntsSuddenEnd;
			//String sqlGroupAccntsSuddenClose;//SUP_1078
			StringBuilder sbufWhereAccntsSuddenEnd = new StringBuilder("");
			//String sqlSelectAccntsSuddenClose;//SUP_1078
			//StringBuffer sbufWhereAccntsSuddenClose = new StringBuffer("");//SUP_1078
			//INT_1017
			String accountingEntryId = null;
			String poolId = null;

			/** ****** JTEST FIX start ************  on 14DEC09*/
//			Statement stmt = null;
//			Statement stmtCal = null;//SUP_1078
//			Statement stmtNonWorkingDay = null;//SUP_1078
			
			PreparedStatement pStmt = null;
			PreparedStatement pStmtCal = null;//SUP_1078
			PreparedStatement pStmtNonWorkingDay = null;//SUP_1078
			/** ****** JTEST FIX end **************  on 14DEC09*/
			ResultSet rs=null;
			Connection conn = null;
			AccountingDO acctDoObj = null;
			//JTEST FIX on 28ARIL11 start
			ResultSet rSet=null;
			ResultSet rSet1=null;
			ResultSet resultSet=null;
			ResultSet resultSet1=null;
			//JTEST FIX on 28ARIL11 end
			boolean flgSeperate = true;
			BigDecimal amtToPostCredit = null;
			BigDecimal amountToPostDebit= null;
			BigDecimal amountToPost = null;
			BigDecimal bd_amtNotPosted=new BigDecimal(0);//SUP_1031

			//StringBuffer sbufUnpostedAmtInsr = new StringBuffer("");

			String dateFormat = LMSConstants.DATE_FORMAT; // ENH_IS1297
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
			SimpleDateFormat formatter = new SimpleDateFormat(dateFormat, Locale.US);
			/** ****** JTEST FIX end **************  on 14DEC09*/
			String strbusinessdate = formatter.format(businessdate);

			if (logger.isDebugEnabled())
				logger.debug("Entering");

			// Get the relevant Accounts for accrural
			sqlSelectAccnts = (new StringBuffer()

			//.append(" SELECT PARTY.ID_POOL,PARTY.PARTICIPANT_ID,COD_PARTY_CCY,")//INT_1017
			.append(" SELECT PARTY.ID_POOL,PARTY.PARTICIPANT_ID,COD_PARTY_CCY, TXN.NBR_PROCESSID, ")// Mashreq_AE_Hook
			//FOR CREDIT INTEREST CONSIDER TXN.TYP_ACCTINT_CRACT , if this is POST OR IF IT IS APPLY IF POOL POSITION IS CR SUPPCR
			.append(" SUM((CASE WHEN (  TXN.TYP_ACCTINT_DRACT = 'POST' OR ")
			.append(" (TXN.AMT_POOL_INTCALC_BALANCE < 0 AND TXN.TYP_ACCTINT_DRACT = 'SUPPDR') OR ")
			.append(" (TXN.AMT_POOL_INTCALC_BALANCE >= 0 AND TXN.TYP_ACCTINT_DRACT = 'SUPPCR')) ")//INT_1027 changed TYP_ACCTINT_CRACT to TYP_ACCTINT_DRACT
			.append(" THEN PARTY.AMT_DR_INTEREST ELSE 0 END )) SUM_DR_INT, ")
			.append(" SUM((CASE WHEN (  TXN.TYP_ACCTINT_CRACT = 'POST' OR ")
			.append(" (TXN.AMT_POOL_INTCALC_BALANCE < 0 AND TXN.TYP_ACCTINT_CRACT = 'SUPPDR') OR ")
			.append(" (TXN.AMT_POOL_INTCALC_BALANCE >= 0 AND    TXN.TYP_ACCTINT_CRACT = 'SUPPCR')) ")
			.append(" THEN PARTY.AMT_CR_INTEREST ELSE 0 END )) SUM_CR_INT,")
			.append(" NBR_COREACCOUNT, COD_BRANCH,PARTY.COD_GL,PARTY.FLG_DEF_DRCR_CONSOL CONSOLIDATE,COD_BNK_LGL, NBR_IBANACCNT ")
			.append(",DAT_ADJUSTEDACCRUAL,DAT_ACCRUAL ")//SUP_1062//INT_1017
			.append(" FROM OPOOL_TXNPOOL_PARTYDTLS PARTY,OPOOL_TXNPOOLHDR TXN")).toString();//INT_1017

			sbufWhereAccnts.append(" WHERE PARTY.ID_POOL = TXN.ID_POOL")
			//.append(" AND PRCPNT.ID_POOL = TXN.ID_POOL")//INT_1017
			.append(" AND TXN.DAT_ACCRUAL = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')")//SUP_1062
			//.append(" AND PRCPNT.PARTICIPANT_ID=PARTY.PARTICIPANT_ID ")//INT_1017
			.append(" AND (PARTY.DAT_END IS NULL OR PARTY.DAT_END > TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "'))")//INT_1017
			//.append(" AND (PRCPNT.DAT_END IS NULL OR PRCPNT.DAT_END > TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "'))")//INT_1017
			.append(" AND PARTY.PARTICIPANT_ID IN (SELECT PRCPNT.PARTICIPANT_ID FROM OPOOL_POOLPARTICIPANT PRCPNT WHERE PRCPNT.ID_POOL = TXN.ID_POOL AND PRCPNT.DAT_END IS NULL OR PRCPNT.DAT_END > TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "'))")//INT_1017
			//.append(" AND TXN.COD_GL = '" + client + "'") //INT_1009
			.append(" AND TXN.NBR_PROCESSID = PARTY.NBR_PROCESSID AND TXN.DAT_BUSINESS_POOLRUN = PARTY.DAT_BUSINESS_POOLRUN")
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			.append(" AND TXN.TXT_STATUS ='" + ExecutionConstants.ALLOCATED+ "'")
			.append(" AND TXN.TXT_STATUS = ?")
			/** ****** JTEST FIX end **************  on 14DEC09*/
			.append(" AND (TYP_POSTING  = 'R' )")//COR TYP_POSTING  = 'I' donotINSERT INTO ACCOUNTINGENTRY TABLE
			.append(" AND TXN.COD_COUNTER_ACCOUNT = 'B' ") //DO ACCRUAL ONLY FOR BANK ACCOUNT
			//.append(" AND (TXN.TYP_ACCTINT_CRACT <> 'SUPP' AND TXN.TYP_ACCTINT_DRACT <> 'SUPP') ")//POST PARTICIPANT INTEREST  SUP_1040
			.append(" AND PARTY.TYP_PARTICIPANT = 'ACCOUNT' AND TXN.FLG_REALPOOL = 'Y' ")//INT_1017
			.append(" AND PARTY.ID_BVT = (SELECT MAX(TO_NUMBER(M.ID_BVT)) FROM OPOOL_TXNPOOLHDR M ")//INT_1026
			.append(" WHERE M.ID_POOL = TXN.ID_POOL AND M.DAT_BUSINESS_POOLRUN = TXN.DAT_BUSINESS_POOLRUN ")
			.append(" AND M.COD_GL = TXN.COD_GL)")//CONSIDERING THE RECORDS FOR THE LATEST BVT
			.append(" AND PARTY.DAT_BUSINESS_POOLRUN BETWEEN  NVL(TXN.DAT_LASTACCRUALCALC,TXN.DAT_START-1)+1 AND TXN.DAT_ACCRUAL  ");//SUP_1062
			// CONSIDER ALL THOSE BUSINESS DATES FOR WHICH THE ACCRUAL IS BEING DONE, IF NOT DONE ALREADY ie between lastaccrual and current accrual
			//nvl(ALLOC.DAT_BUSINESS_POOLRUN,TXN.DAT_START -1)
			if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
				sbufWhereAccnts.append(" AND TXN.ID_POOL IN (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR LOT ");
				/** ****** JTEST FIX start ************  on 14DEC09*/
//				sbufWhereAccnts.append(" WHERE LOT.NBR_LOTID =" + generateForId+")");
				sbufWhereAccnts.append(" WHERE LOT.NBR_LOTID =?)");
				/** ****** JTEST FIX end **************  on 14DEC09*/
				
			}

			//sqlGroupAccnts = " GROUP BY PARTY.ID_POOL, PARTY.PARTICIPANT_ID,COD_PARTY_CCY,NBR_COREACCOUNT,COD_BRANCH,PARTY.COD_GL,FLG_DEF_DRCR_CONSOL,COD_BNK_LGL,NBR_IBANACCNT,DAT_ADJUSTEDACCRUAL,DAT_ACCRUAL";//SUP_1062//INT_1017
			sqlGroupAccnts = " GROUP BY PARTY.ID_POOL, PARTY.PARTICIPANT_ID,COD_PARTY_CCY,NBR_COREACCOUNT,COD_BRANCH,PARTY.COD_GL,FLG_DEF_DRCR_CONSOL,COD_BNK_LGL,NBR_IBANACCNT,DAT_ADJUSTEDACCRUAL,DAT_ACCRUAL,TXN.NBR_PROCESSID";//SUP_1062//INT_1017//Mashreq_AE_Hook
			//SUP_1078--Starts
			//INT_1017--Starts
			//Records of the accounts which are having an end date as today
			/*
			sqlSelectAccntsSuddenEnd= (new StringBuffer()
					.append(" SELECT PARTY.ID_POOL,PARTICIPANT_ID,COD_PARTY_CCY,")
					//FOR CREDIT INTEREST CONSIDER TXN.TYP_ACCTINT_CRACT , if this is POST OR IF IT IS APPLY IF POOL POSITION IS CR SUPPCR
					.append(" SUM((CASE WHEN (  TXN.TYP_ACCTINT_DRACT = 'POST' OR ")
					.append(" (TXN.AMT_POOL_INTCALC_BALANCE < 0 AND TXN.TYP_ACCTINT_DRACT = 'SUPPDR') OR ")
					.append(" (TXN.AMT_POOL_INTCALC_BALANCE >= 0 AND TXN.TYP_ACCTINT_DRACT = 'SUPPCR')) ")//INT_1027 changed TYP_ACCTINT_CRACT to TYP_ACCTINT_DRACT
					.append(" THEN PARTY.AMT_DR_INTEREST ELSE 0 END )) SUM_DR_INT, ")
					.append(" SUM((CASE WHEN (  TXN.TYP_ACCTINT_CRACT = 'POST' OR ")
					.append(" (TXN.AMT_POOL_INTCALC_BALANCE < 0 AND TXN.TYP_ACCTINT_CRACT = 'SUPPDR') OR ")
					.append(" (TXN.AMT_POOL_INTCALC_BALANCE >= 0 AND    TXN.TYP_ACCTINT_CRACT = 'SUPPCR')) ")
					.append(" THEN PARTY.AMT_CR_INTEREST ELSE 0 END )) SUM_CR_INT,")
					.append(" NBR_COREACCOUNT, COD_BRANCH,PARTY.COD_GL,PARTY.FLG_DEF_DRCR_CONSOL CONSOLIDATE,COD_BNK_LGL, NBR_IBANACCNT ")
					.append(",PARTY.DAT_END DAT_ADJUSTEDACCRUAL,PARTY.DAT_END DAT_ACCRUAL ")//INT_1017
					.append(" FROM OPOOL_TXNPOOL_PARTYDTLS PARTY,OPOOL_TXNPOOLHDR TXN")).toString();

			sbufWhereAccntsSuddenEnd.append(" WHERE PARTY.ID_POOL = TXN.ID_POOL")
			.append(" AND PARTY.DAT_END = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "') ")
			.append(" AND TXN.NBR_PROCESSID = PARTY.NBR_PROCESSID AND TXN.DAT_BUSINESS_POOLRUN = PARTY.DAT_BUSINESS_POOLRUN")
			.append(" AND TXN.TXT_STATUS ='" + ExecutionConstants.ALLOCATED + "'")
			.append(" AND (TYP_POSTING  = 'R' )")//COR TYP_POSTING  = 'I' donotINSERT INTO ACCOUNTINGENTRY TABLE
			.append(" AND TXN.COD_COUNTER_ACCOUNT = 'B' ") //DO ACCRUAL ONLY FOR BANK ACCOUNT
			.append(" AND TYP_PARTICIPANT = 'ACCOUNT' AND TXN.FLG_REALPOOL = 'Y' ")
			.append(" AND PARTY.ID_BVT = (SELECT MAX(TO_NUMBER(M.ID_BVT)) FROM OPOOL_TXNPOOLHDR M ")
			.append(" WHERE M.ID_POOL = TXN.ID_POOL AND M.DAT_BUSINESS_POOLRUN = TXN.DAT_BUSINESS_POOLRUN ")
			.append(" AND M.COD_GL = TXN.COD_GL)")//CONSIDERING THE RECORDS FOR THE LATEST BVT
			.append(" AND PARTY.DAT_BUSINESS_POOLRUN BETWEEN  NVL(TXN.DAT_LASTACCRUALCALC,TXN.DAT_START-1)+1 AND TXN.DAT_ACCRUAL  ");//SUP_1062
			if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
				sbufWhereAccntsSuddenEnd.append(" AND TXN.ID_POOL IN (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR LOT ");
				sbufWhereAccntsSuddenEnd.append(" WHERE LOT.NBR_LOTID =" + generateForId+")");
			}
			*/
			//Participants which have been closed
			/*
			sqlSelectAccntsSuddenClose= (new StringBuffer()
					.append(" SELECT PARTY.ID_POOL,PARTY.PARTICIPANT_ID,COD_PARTY_CCY,")
					//FOR CREDIT INTEREST CONSIDER TXN.TYP_ACCTINT_CRACT , if this is POST OR IF IT IS APPLY IF POOL POSITION IS CR SUPPCR
					.append(" SUM((CASE WHEN (  TXN.TYP_ACCTINT_DRACT = 'POST' OR ")
					.append(" (TXN.AMT_POOL_INTCALC_BALANCE < 0 AND TXN.TYP_ACCTINT_DRACT = 'SUPPDR') OR ")
					.append(" (TXN.AMT_POOL_INTCALC_BALANCE >= 0 AND TXN.TYP_ACCTINT_DRACT = 'SUPPCR')) ")//INT_1027 changed TYP_ACCTINT_CRACT to TYP_ACCTINT_DRACT
					.append(" THEN PARTY.AMT_DR_INTEREST ELSE 0 END )) SUM_DR_INT, ")
					.append(" SUM((CASE WHEN (  TXN.TYP_ACCTINT_CRACT = 'POST' OR ")
					.append(" (TXN.AMT_POOL_INTCALC_BALANCE < 0 AND TXN.TYP_ACCTINT_CRACT = 'SUPPDR') OR ")
					.append(" (TXN.AMT_POOL_INTCALC_BALANCE >= 0 AND    TXN.TYP_ACCTINT_CRACT = 'SUPPCR')) ")
					.append(" THEN PARTY.AMT_CR_INTEREST ELSE 0 END )) SUM_CR_INT,")
					.append(" NBR_COREACCOUNT, COD_BRANCH,PARTY.COD_GL,PARTY.FLG_DEF_DRCR_CONSOL CONSOLIDATE,COD_BNK_LGL, NBR_IBANACCNT ")
					.append(",(PRCIPANT.DAT_END + 1) DAT_ADJUSTEDACCRUAL,(PRCIPANT.DAT_END + 1) DAT_ACCRUAL ")//INT_1017
					.append(" FROM OPOOL_TXNPOOL_PARTYDTLS PARTY,OPOOL_TXNPOOLHDR TXN, OPOOL_POOLPARTICIPANT PRCIPANT ")).toString();

			sbufWhereAccntsSuddenClose.append(" WHERE PARTY.ID_POOL = TXN.ID_POOL AND TXN.ID_POOL = PRCIPANT.ID_POOL ")
			.append(" AND PARTY.PARTICIPANT_ID=PRCIPANT.PARTICIPANT_ID ")
			.append(" AND PRCIPANT.DAT_END = (TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')-1) ")
			.append(" AND PRCIPANT.FLG_ACCOUNTCLS = 'Y' ")
			.append(" AND TXN.NBR_PROCESSID = PARTY.NBR_PROCESSID AND TXN.DAT_BUSINESS_POOLRUN = PARTY.DAT_BUSINESS_POOLRUN")
			.append(" AND TXN.TXT_STATUS ='" + ExecutionConstants.ALLOCATED + "'")
			.append(" AND (TYP_POSTING  = 'R' )")//COR TYP_POSTING  = 'I' donotINSERT INTO ACCOUNTINGENTRY TABLE
			.append(" AND TXN.COD_COUNTER_ACCOUNT = 'B' ") //DO ACCRUAL ONLY FOR BANK ACCOUNT
			.append(" AND PARTY.TYP_PARTICIPANT = 'ACCOUNT' AND TXN.FLG_REALPOOL = 'Y' ")
			.append(" AND PARTY.ID_BVT = (SELECT MAX(TO_NUMBER(M.ID_BVT)) FROM OPOOL_TXNPOOLHDR M ")
			.append(" WHERE M.ID_POOL = TXN.ID_POOL AND M.DAT_BUSINESS_POOLRUN = TXN.DAT_BUSINESS_POOLRUN ")
			.append(" AND M.COD_GL = TXN.COD_GL)")//CONSIDERING THE RECORDS FOR THE LATEST BVT
			.append(" AND PARTY.DAT_BUSINESS_POOLRUN BETWEEN  NVL(TXN.DAT_LASTACCRUALCALC,TXN.DAT_START-1)+1 AND TXN.DAT_ACCRUAL  ");//SUP_1062
			if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
				sbufWhereAccntsSuddenClose.append(" AND TXN.ID_POOL IN (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR LOT ");
				sbufWhereAccntsSuddenClose.append(" WHERE LOT.NBR_LOTID =" + generateForId+")");
			}

			//sqlGroupAccntsSuddenEnd = " GROUP BY PARTY.ID_POOL, PARTY.PARTICIPANT_ID,COD_PARTY_CCY,NBR_COREACCOUNT,COD_BRANCH,PARTY.COD_GL,FLG_DEF_DRCR_CONSOL,COD_BNK_LGL,NBR_IBANACCNT,PARTY.DAT_END";
			sqlGroupAccntsSuddenClose = " GROUP BY PARTY.ID_POOL, PARTY.PARTICIPANT_ID,COD_PARTY_CCY,NBR_COREACCOUNT,COD_BRANCH,PARTY.COD_GL,FLG_DEF_DRCR_CONSOL,COD_BNK_LGL,NBR_IBANACCNT,PRCIPANT.DAT_END";
			*/
			//INT_1017--Ends
			//Combine the subParts of the query
			sqlString = sqlSelectAccnts + sbufWhereAccnts.toString() + sqlGroupAccnts;
						/*
						+" UNION "
						//+ sqlSelectAccntsSuddenEnd + sbufWhereAccntsSuddenEnd.toString() +sqlGroupAccntsSuddenEnd
						//+" UNION "
						+sqlSelectAccntsSuddenClose+sbufWhereAccntsSuddenClose.toString()+sqlGroupAccntsSuddenClose;//INT_1017
						*/
			//SUP_1078--Ends
			if (logger.isDebugEnabled())
				logger.debug("Query ACCRUAL PARTICIPANT POSTING" + sqlString);

			try{
				conn = LMSConnectionUtility.getConnection();
				/** ****** JTEST FIX start ************  on 14DEC09*/
//				stmt = conn.createStatement();
//				stmtNonWorkingDay = conn.createStatement();//SUP_1078
//				stmtCal = conn.createStatement();//SUP_1078
				pStmt = conn.prepareStatement(sqlString);
				pStmt.setString(1, ExecutionConstants.ALLOCATED);
				if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
					pStmt.setString(2, generateForId);
				}
//				rs = stmt.executeQuery(sqlString);
				rs = pStmt.executeQuery();				
				/** ****** JTEST FIX end **************  on 14DEC09*/
				

				if (logger.isDebugEnabled()) {
					logger.debug(" Executed Query ACCRUAL PARTICIPANT POSTING ");
				}
				HashMap hmFields = new HashMap();
				hmFields.put(ExecutionConstants.BUSINESSDATE, businessdate); // Mashreq_AE_Hook fix
				//hmFields.put("DAT_POSTING",businessdate);//SUP_1062 THIS SHOULD BE ADJUSTEDACCRUAL
				//BVT_TXN Inc Starts
				//hmFields.put("DAT_SENT",businessdate);//INT_1017
				//BVT_TXN Inc Ends
				String flagAllocation = null;
				boolean results = false;
				while(rs.next()){
					results = true;
					hmFields.put("DAT_POSTING",(Date)rs.getDate("DAT_ADJUSTEDACCRUAL"));//SUP_1062
					hmFields.put("DAT_SENT",(Date)rs.getDate("DAT_ACCRUAL"));//INT_1017
					acctDoObj = new AccountingDO();
					String flg_def_drcr_consol = rs.getString("CONSOLIDATE");
					flgSeperate = true;
					accountingEntryId = "0";
					poolId = "";
					amtToPostCredit = new BigDecimal(rs.getString("SUM_CR_INT"));
					amountToPostDebit = new BigDecimal(rs.getString("SUM_DR_INT"));
					hmFields.put(ExecutionConstants.PROCESSID, rs.getLong("NBR_PROCESSID")); //Mashreq_AE_Hook
					poolId = rs.getString("ID_POOL");

					if (logger.isDebugEnabled()) {
						logger.debug("Resultset flg_def_drcr_consol" + flg_def_drcr_consol);
						logger.debug("Resultset POOLID" + poolId);
					}
					if("Y".equalsIgnoreCase(flg_def_drcr_consol!=null
																?flg_def_drcr_consol.trim()
																:"" )){
						//Get the NetInterest,if consolidate credit and debit interests
						amountToPost = amtToPostCredit.subtract(amountToPostDebit);
						// Set the seperate posting flag to False
						flgSeperate = false;

						if(amtToPostCredit.compareTo(zero)!=0 && amountToPostDebit.compareTo(zero)!=0){
							flgSeperate = true;
							//if both debit and credit interest are present.pass separate entries. as there will be both payables and receivables in this case
						}

					}
					if (logger.isDebugEnabled()) {
						logger.debug(" Flag Separate" + flgSeperate);
					}
					//fill in the AccountingDo object's attributes.
					acctDoObj.set_cod_bnk_lgl(rs.getString("COD_BNK_LGL"));
					acctDoObj.set_cod_branch(rs.getString("COD_BRANCH"));
					acctDoObj.set_cod_gl(rs.getString("COD_GL"));
					acctDoObj.set_cod_ccy(rs.getString("COD_PARTY_CCY"));
					acctDoObj.set_nbr_coreaccount(rs.getString("NBR_COREACCOUNT"));
					acctDoObj.set_nbr_ibanacct(rs.getString("NBR_IBANACCNT"));
					acctDoObj.set_nbr_ownaccount(rs.getString("PARTICIPANT_ID"));

					if (logger.isDebugEnabled()) {
						logger.debug("Account Object" + acctDoObj);
					}
					if(flgSeperate){
						//SUP_1031--Starts
						bd_amtNotPosted = setUnpostedAmounts(ExecutionConstants.CREDIT_INTEREST,acctDoObj,
								pstmtAccountUnposted,businessdate,ExecutionConstants.ACCRUAL);

						if (logger.isDebugEnabled()) {
							logger.debug("Unaccrued Amount "  + bd_amtNotPosted +"for ExecutionConstants.CREDIT_INTEREST");
						}
						//chk if any amount is left to be posted
						if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
							amtToPostCredit = amtToPostCredit.add(bd_amtNotPosted);
							if (logger.isDebugEnabled()) {
								logger.debug("Adding Unaccrued Amount  "  + bd_amtNotPosted +"for ExecutionConstants.CREDIT_INTEREST "+amtToPostCredit);
							}
						}// UNPOSTED AMOUNT
						//SUP_1031--Ends
						if (amtToPostCredit.compareTo(zero) != 0){
							accountingEntryId =  "" +processEntry(acctDoObj,
									amtToPostCredit,ExecutionConstants.ACCRUAL,
									ExecutionConstants.CREDIT, ExecutionConstants.CREDIT_INTEREST,
									/** ****** JTEST FIX start ************  on 14DEC09*/
//									pstmtAccountingEntry,pstmtAcctUnpostedInsert,poolId,hmFields);//INT_1039
									pstmtAccountingEntry,pstmtAcctUnpostedInsert,poolId,hmFields, conn);//INT_1039
									/** ****** JTEST FIX end **************  on 14DEC09*/
						}
						else{
							logger.fatal("Credit Interest is 0 for"  + acctDoObj + "SO not accruing the amount ");
						}
						//SUP_1031--Starts
						bd_amtNotPosted = setUnpostedAmounts(ExecutionConstants.DEBIT_INTEREST,acctDoObj,
								pstmtAccountUnposted,businessdate,ExecutionConstants.ACCRUAL);

						if (logger.isDebugEnabled()) {
							logger.debug("Unaccrued Amount "  + bd_amtNotPosted +"for ExecutionConstants.DEBIT_INTEREST");
						}
						//chk if any amount is left to be posted
						if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
							amountToPostDebit = amountToPostDebit.add(bd_amtNotPosted);
							if (logger.isDebugEnabled()) {
								logger.debug("Adding Unaccrued Amount  "  + bd_amtNotPosted +"for ExecutionConstants.DEBIT_INTEREST "+amountToPostDebit);
							}
						}// UNPOSTED AMOUNT
						//SUP_1031--Ends
						if (amountToPostDebit.compareTo(zero) != 0){
							accountingEntryId = accountingEntryId + "|" + processEntry(acctDoObj,
								amountToPostDebit,ExecutionConstants.ACCRUAL,
								ExecutionConstants.DEBIT,ExecutionConstants.DEBIT_INTEREST,
								/** ****** JTEST FIX start ************  on 14DEC09*/
//								pstmtAccountingEntry,pstmtAcctUnpostedInsert,poolId,hmFields);//INT_1039
								pstmtAccountingEntry,pstmtAcctUnpostedInsert,poolId,hmFields, conn);//INT_1039
								/** ****** JTEST FIX end **************  on 14DEC09*/
						}
						else{
							logger.fatal("Debit Interest is 0 for"  + acctDoObj + "SO not accruing the amount ");
						}
					}
					else{
						//call processEntry for the NetAmount --consolidated
						//SUP_1031--Starts
						bd_amtNotPosted = setUnpostedAmounts(ExecutionConstants.NET_INTEREST,acctDoObj,
								pstmtAccountUnposted,businessdate,ExecutionConstants.ACCRUAL);

						if (logger.isDebugEnabled()) {
							logger.debug("Unaccrued Amount "  + bd_amtNotPosted +"for ExecutionConstants.NET_INTEREST");
						}
						//chk if any amount is left to be posted
						if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
							amountToPost = amountToPost.add(bd_amtNotPosted);
							if (logger.isDebugEnabled()) {
								logger.debug("Adding Unaccrued Amount  "  + bd_amtNotPosted +"for ExecutionConstants.NET_INTEREST "+amountToPost);
							}
						}// UNPOSTED AMOUNT
						//SUP_1031--Ends
						if (amountToPost.compareTo(zero) != 0){
							accountingEntryId =  ""+processEntry(acctDoObj,
								amountToPost,ExecutionConstants.ACCRUAL,ExecutionConstants.NET,
								ExecutionConstants.NET_INTEREST,pstmtAccountingEntry,
								/** ****** JTEST FIX start ************  on 14DEC09*/
//								pstmtAcctUnpostedInsert,poolId,hmFields);//INT_1039
								pstmtAcctUnpostedInsert,poolId,hmFields, conn);//INT_1039
								/** ****** JTEST FIX end **************  on 14DEC09*/
						}else{
							logger.fatal("Net Interest is 0 for"  + acctDoObj + "SO not accruing the amount ");
						}
					}

					//Add to preparedStmt for AccountingEntryId updation
					if (logger.isDebugEnabled()) {
						logger.debug("After processEntry s_accountingEntryId"  + accountingEntryId);
					}
					//Update the Prepared Statements for OPOOL_PARTYDETAILS
					updateInterest(pstmtTxnPoolPartyDtls,accountingEntryId,poolId,acctDoObj.get_nbr_ownaccount(),ExecutionConstants.ACCRUAL);
					//Add to the Pool List
					//updatePoolList(poolId, hpPoolList);

					//accrualEntries[ExecutionConstants.PARTY_DTLS_UPDATE] = true;
				}
				if (!results){
					logger.fatal(" No records for Accrual -participant interest" + sqlString);
				}
				//15.1.1_Resource_Leaks_Fix
				LMSConnectionUtility.close(pStmt);
				//SUP_1078--Starts
				//For accounts which are ending their participation today
				sqlSelectAccntsSuddenEnd= (new StringBuffer()
						//.append(" SELECT PARTY.ID_POOL,PARTICIPANT_ID,COD_PARTY_CCY,")
						.append(" SELECT PARTY.ID_POOL,PARTICIPANT_ID,COD_PARTY_CCY,TXN.NBR_PROCESSID, ") //Mashreq_AE_Hook
						//FOR CREDIT INTEREST CONSIDER TXN.TYP_ACCTINT_CRACT , if this is POST OR IF IT IS APPLY IF POOL POSITION IS CR SUPPCR
						.append(" SUM((CASE WHEN (  TXN.TYP_ACCTINT_DRACT = 'POST' OR ")
						.append(" (TXN.AMT_POOL_INTCALC_BALANCE < 0 AND TXN.TYP_ACCTINT_DRACT = 'SUPPDR') OR ")
						.append(" (TXN.AMT_POOL_INTCALC_BALANCE >= 0 AND TXN.TYP_ACCTINT_DRACT = 'SUPPCR')) ")//INT_1027 changed TYP_ACCTINT_CRACT to TYP_ACCTINT_DRACT
						.append(" THEN PARTY.AMT_DR_INTEREST ELSE 0 END )) SUM_DR_INT, ")
						.append(" SUM((CASE WHEN (  TXN.TYP_ACCTINT_CRACT = 'POST' OR ")
						.append(" (TXN.AMT_POOL_INTCALC_BALANCE < 0 AND TXN.TYP_ACCTINT_CRACT = 'SUPPDR') OR ")
						.append(" (TXN.AMT_POOL_INTCALC_BALANCE >= 0 AND    TXN.TYP_ACCTINT_CRACT = 'SUPPCR')) ")
						.append(" THEN PARTY.AMT_CR_INTEREST ELSE 0 END )) SUM_CR_INT,")
						.append(" NBR_COREACCOUNT, COD_BRANCH,PARTY.COD_GL,PARTY.FLG_DEF_DRCR_CONSOL CONSOLIDATE,COD_BNK_LGL, NBR_IBANACCNT ")
						.append(",PARTY.DAT_END DAT_ADJUSTEDACCRUAL,PARTY.DAT_END DAT_ACCRUAL ")//INT_1017
						.append(" FROM OPOOL_TXNPOOL_PARTYDTLS PARTY,OPOOL_TXNPOOLHDR TXN")).toString();

				sbufWhereAccntsSuddenEnd.append(" WHERE PARTY.ID_POOL = TXN.ID_POOL")
				/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
				/* SUP_HSBC_019 Starts::: checking participant for the same run date. */
				//.append(" AND PARTY.DAT_END = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')")
				.append(" AND PARTY.DAT_END = (select distinct DAT_END from OPOOL_TXNPOOL_PARTYDTLS party_temp " + 
				        " where party_temp.dat_business_poolrun = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "') " +
		 				" and party_temp.dat_end = party_temp.dat_business_poolrun "  +
						" and party_temp.participant_id = PARTY.participant_id 	) ")
				.append(" AND TXN.NBR_PROCESSID = PARTY.NBR_PROCESSID AND TXN.DAT_BUSINESS_POOLRUN = PARTY.DAT_BUSINESS_POOLRUN")
 				/* SUP_HSBC_019 ENDS...*/
				/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
				/** ****** JTEST FIX start ************  on 14DEC09*/
//				.append(" AND TXN.TXT_STATUS ='" + ExecutionConstants.ALLOCATED + "'")
				.append(" AND TXN.TXT_STATUS =?")
				/** ****** JTEST FIX end **************  on 14DEC09*/
				.append(" AND (TYP_POSTING  = 'R' )")//COR TYP_POSTING  = 'I' donotINSERT INTO ACCOUNTINGENTRY TABLE
				.append(" AND TXN.COD_COUNTER_ACCOUNT = 'B' ") //DO ACCRUAL ONLY FOR BANK ACCOUNT
				.append(" AND TYP_PARTICIPANT = 'ACCOUNT' AND TXN.FLG_REALPOOL = 'Y' ")
				.append(" AND PARTY.ID_BVT = (SELECT MAX(TO_NUMBER(M.ID_BVT)) FROM OPOOL_TXNPOOLHDR M ")
				.append(" WHERE M.ID_POOL = TXN.ID_POOL AND M.DAT_BUSINESS_POOLRUN = TXN.DAT_BUSINESS_POOLRUN ")
				.append(" AND M.COD_GL = TXN.COD_GL)")//CONSIDERING THE RECORDS FOR THE LATEST BVT
				.append(" AND PARTY.DAT_BUSINESS_POOLRUN BETWEEN  NVL(TXN.DAT_LASTACCRUALCALC,TXN.DAT_START-1)+1 AND TXN.DAT_ACCRUAL  ");//SUP_1062
				if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
					sbufWhereAccntsSuddenEnd.append(" AND TXN.ID_POOL IN (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR LOT ");
					/** ****** JTEST FIX start ************  on 14DEC09*/
//					sbufWhereAccntsSuddenEnd.append(" WHERE LOT.NBR_LOTID =" + generateForId+")");
					sbufWhereAccntsSuddenEnd.append(" WHERE LOT.NBR_LOTID = ?)");
					/** ****** JTEST FIX end **************  on 14DEC09*/
					
				}
				
				//sqlGroupAccntsSuddenEnd = " GROUP BY PARTY.ID_POOL, PARTY.PARTICIPANT_ID,COD_PARTY_CCY,NBR_COREACCOUNT,COD_BRANCH,PARTY.COD_GL,FLG_DEF_DRCR_CONSOL,COD_BNK_LGL,NBR_IBANACCNT,PARTY.DAT_END";
				sqlGroupAccntsSuddenEnd = " GROUP BY PARTY.ID_POOL, PARTY.PARTICIPANT_ID,COD_PARTY_CCY,NBR_COREACCOUNT,COD_BRANCH,PARTY.COD_GL,FLG_DEF_DRCR_CONSOL,COD_BNK_LGL,NBR_IBANACCNT,PARTY.DAT_END,TXN.NBR_PROCESSID";// Mashreq_AE_Hook
				
				String sqlStringEnd = sqlSelectAccntsSuddenEnd + sbufWhereAccntsSuddenEnd.toString() +sqlGroupAccntsSuddenEnd;
				if (logger.isDebugEnabled())
					logger.debug("Query ACCRUAL PARTICIPANT FOR PARTICIPANTS END" + sqlStringEnd);
				/** ****** JTEST FIX start ************  on 14DEC09*/
				
				pStmt = conn.prepareStatement(sqlStringEnd);
				pStmt.setString(1, ExecutionConstants.ALLOCATED);
				if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
					pStmt.setString(2, generateForId);
				}
//				rs = stmt.executeQuery(sqlStringEnd);
				LMSConnectionUtility.close(rs); //15.1.1_Resource_Leaks_Fix
				rs = pStmt.executeQuery();
				/** ****** JTEST FIX end **************  on 14DEC09*/
				java.util.Date adjustedAccrual=new Date();
				String sqlQuery="";
				
				
				String calendar="";
				boolean isHoliday;
				String sqlQueryForOption="";
				String Option="";
				while(rs.next()){
					adjustedAccrual=rs.getDate("DAT_ADJUSTEDACCRUAL");
					
					hmFields.put(ExecutionConstants.PROCESSID, rs.getLong("NBR_PROCESSID")); //Mashreq_AE_Hook
					//ENH_LIQ_0005 Starts
					//sqlQuery="SELECT COD_CALENDAR FROM ORBICASH_GLMST WHERE COD_GL = (SELECT COD_GL FROM ORBICASH_ACCOUNTS WHERE NBR_OWNACCOUNT ='"+rs.getString("PARTICIPANT_ID")+"')";
					/** ****** JTEST FIX start ************  on 14DEC09*/
//					sqlQuery="SELECT COD_CALENDAR FROM OLM_GLMST WHERE COD_GL = (SELECT COD_GL FROM ORBICASH_ACCOUNTS WHERE NBR_OWNACCOUNT ='"+rs.getString("PARTICIPANT_ID")+"')";
//					//ENH_LIQ_0005 Ends
//					
//					rSet=stmtCal.executeQuery(sqlQuery);
					
					sqlQuery="SELECT COD_CALENDAR FROM OLM_GLMST WHERE COD_GL = (SELECT COD_GL FROM ORBICASH_ACCOUNTS WHERE NBR_OWNACCOUNT = ? )";
					//ENH_LIQ_0005 Ends
					
					pStmtCal = conn.prepareStatement(sqlQuery);
					pStmtCal.setString(1, rs.getString("PARTICIPANT_ID"));
					rSet = pStmtCal.executeQuery();
					/** ****** JTEST FIX end **************  on 14DEC09*/
					if(rSet.next()){
						calendar=rSet.getString("COD_CALENDAR");
						CalendarService cal=CalendarService.getCalendarService();
						isHoliday=cal.isDateHoliday(calendar,adjustedAccrual);
						if(isHoliday){
							/** ****** JTEST FIX start ************  on 14DEC09*/
//							sqlQueryForOption="SELECT COD_NON_WORKING_DAY FROM ORATE_ACCRUAL_CYCLE WHERE COD_ACCR_CYCLE = " +
//							"(SELECT COD_ACCR_CYCLE FROM OPOOL_TXNPOOLHDR WHERE ID_POOL ='"+rs.getString("ID_POOL")+"' " +
//									"AND DAT_BUSINESS_POOLRUN =TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "'))";
//							resultSet=stmtNonWorkingDay.executeQuery(sqlQueryForOption);
					
							sqlQueryForOption="SELECT COD_NON_WORKING_DAY FROM ORATE_ACCRUAL_CYCLE WHERE COD_ACCR_CYCLE = " +
							"(SELECT COD_ACCR_CYCLE FROM OPOOL_TXNPOOLHDR WHERE ID_POOL = ? " +
									"AND DAT_BUSINESS_POOLRUN = ?)";
							
							pStmtNonWorkingDay = conn.prepareStatement(sqlQueryForOption);
							pStmtNonWorkingDay.setString(1, rs.getString("ID_POOL"));
							pStmtNonWorkingDay.setDate(2,LMSUtility.truncSqlDate( new java.sql.Date(businessdate.getTime())));
							resultSet= pStmtNonWorkingDay.executeQuery();
							/** ****** JTEST FIX end **************  on 14DEC09*/
							if(resultSet.next()){
								Option=resultSet.getString("COD_NON_WORKING_DAY");
								if(Option.equalsIgnoreCase("NEXT")){
									adjustedAccrual=cal.getNextWorkingDay(calendar,adjustedAccrual);
								}else if(Option.equalsIgnoreCase("PREV")){
									adjustedAccrual=cal.getPrevWorkingDay(calendar,adjustedAccrual);
								}
							}
						}
					}
					LMSConnectionUtility.close(resultSet);
					LMSConnectionUtility.close(rSet);
					
					hmFields.put("DAT_POSTING",adjustedAccrual);//SUP_1062
					hmFields.put("DAT_SENT",(Date)rs.getDate("DAT_ACCRUAL"));//INT_1017
					acctDoObj = new AccountingDO();
					/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
					//ENH_1110 Starts
					acctDoObj.setExpiringToday(true);
					//ENH_1110 Ends
					/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
					String flg_def_drcr_consol = rs.getString("CONSOLIDATE");
					flgSeperate = true;
					accountingEntryId = "0";
					poolId = "";
					amtToPostCredit = new BigDecimal(rs.getString("SUM_CR_INT"));
					amountToPostDebit = new BigDecimal(rs.getString("SUM_DR_INT"));
					poolId = rs.getString("ID_POOL");
					if (logger.isDebugEnabled()) {
						logger.debug("Resultset flg_def_drcr_consol" + flg_def_drcr_consol);
						logger.debug("Resultset POOLID" + poolId);
					}
					if("Y".equalsIgnoreCase(flg_def_drcr_consol!=null
																?flg_def_drcr_consol.trim()
																:"" )){
						//Get the NetInterest,if consolidate credit and debit interests
						amountToPost = amtToPostCredit.subtract(amountToPostDebit);
						// Set the seperate posting flag to False
						flgSeperate = false;

						if(amtToPostCredit.compareTo(zero)!=0 && amountToPostDebit.compareTo(zero)!=0){
							flgSeperate = true;
							//if both debit and credit interest are present.pass separate entries. as there will be both payables and receivables in this case
						}

					}
					if (logger.isDebugEnabled()) {
						logger.debug(" Flag Separate" + flgSeperate);
					}
					//fill in the AccountingDo object's attributes.
					acctDoObj.set_cod_bnk_lgl(rs.getString("COD_BNK_LGL"));
					acctDoObj.set_cod_branch(rs.getString("COD_BRANCH"));
					acctDoObj.set_cod_gl(rs.getString("COD_GL"));
					acctDoObj.set_cod_ccy(rs.getString("COD_PARTY_CCY"));
					acctDoObj.set_nbr_coreaccount(rs.getString("NBR_COREACCOUNT"));
					acctDoObj.set_nbr_ibanacct(rs.getString("NBR_IBANACCNT"));
					acctDoObj.set_nbr_ownaccount(rs.getString("PARTICIPANT_ID"));

					if (logger.isDebugEnabled()) {
						logger.debug("Account Object" + acctDoObj);
					}
					if(flgSeperate){
						//SUP_1031--Starts
						bd_amtNotPosted = setUnpostedAmounts(ExecutionConstants.CREDIT_INTEREST,acctDoObj,
								pstmtAccountUnposted,businessdate,ExecutionConstants.ACCRUAL);

						if (logger.isDebugEnabled()) {
							logger.debug("Unaccrued Amount "  + bd_amtNotPosted +"for ExecutionConstants.CREDIT_INTEREST");
						}
						//chk if any amount is left to be posted
						if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
							amtToPostCredit = amtToPostCredit.add(bd_amtNotPosted);
							if (logger.isDebugEnabled()) {
								logger.debug("Adding Unaccrued Amount  "  + bd_amtNotPosted +"for ExecutionConstants.CREDIT_INTEREST "+amtToPostCredit);
							}
						}// UNPOSTED AMOUNT
						//SUP_1031--Ends
						if (amtToPostCredit.compareTo(zero) != 0){
							accountingEntryId =  "" +processEntry(acctDoObj,
									amtToPostCredit,ExecutionConstants.ACCRUAL,
									ExecutionConstants.CREDIT, ExecutionConstants.CREDIT_INTEREST,
									/** ****** JTEST FIX start ************  on 14DEC09*/
//									pstmtAccountingEntry,pstmtAcctUnpostedInsert,poolId,hmFields);//INT_1039
									pstmtAccountingEntry,pstmtAcctUnpostedInsert,poolId,hmFields, conn);//INT_1039
									/** ****** JTEST FIX end **************  on 14DEC09*/
						}
						else{
							logger.fatal("Credit Interest is 0 for"  + acctDoObj + "SO not accruing the amount ");
						}
						//SUP_1031--Starts
						bd_amtNotPosted = setUnpostedAmounts(ExecutionConstants.DEBIT_INTEREST,acctDoObj,
								pstmtAccountUnposted,businessdate,ExecutionConstants.ACCRUAL);

						if (logger.isDebugEnabled()) {
							logger.debug("Unaccrued Amount "  + bd_amtNotPosted +"for ExecutionConstants.DEBIT_INTEREST");
						}
						//chk if any amount is left to be posted
						if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
							amountToPostDebit = amountToPostDebit.add(bd_amtNotPosted);
							if (logger.isDebugEnabled()) {
								logger.debug("Adding Unaccrued Amount  "  + bd_amtNotPosted +"for ExecutionConstants.DEBIT_INTEREST "+amountToPostDebit);
							}
						}// UNPOSTED AMOUNT
						//SUP_1031--Ends
						if (amountToPostDebit.compareTo(zero) != 0){
							accountingEntryId = accountingEntryId + "|" + processEntry(acctDoObj,
								amountToPostDebit,ExecutionConstants.ACCRUAL,
								ExecutionConstants.DEBIT,ExecutionConstants.DEBIT_INTEREST,
								/** ****** JTEST FIX start ************  on 14DEC09*/
//								pstmtAccountingEntry,pstmtAcctUnpostedInsert,poolId,hmFields);//INT_1039
								pstmtAccountingEntry,pstmtAcctUnpostedInsert,poolId,hmFields, conn);//INT_1039
								/** ****** JTEST FIX end **************  on 14DEC09*/
						}
						else{
							logger.fatal("Debit Interest is 0 for"  + acctDoObj + "SO not accruing the amount ");
						}
					}
					else{
						//call processEntry for the NetAmount --consolidated
						//SUP_1031--Starts
						bd_amtNotPosted = setUnpostedAmounts(ExecutionConstants.NET_INTEREST,acctDoObj,
								pstmtAccountUnposted,businessdate,ExecutionConstants.ACCRUAL);

						if (logger.isDebugEnabled()) {
							logger.debug("Unaccrued Amount "  + bd_amtNotPosted +"for ExecutionConstants.NET_INTEREST");
						}
						//chk if any amount is left to be posted
						if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
							amountToPost = amountToPost.add(bd_amtNotPosted);
							if (logger.isDebugEnabled()) {
								logger.debug("Adding Unaccrued Amount  "  + bd_amtNotPosted +"for ExecutionConstants.NET_INTEREST "+amountToPost);
							}
						}// UNPOSTED AMOUNT
						//SUP_1031--Ends
						if (amountToPost.compareTo(zero) != 0){
							accountingEntryId =  ""+processEntry(acctDoObj,
								amountToPost,ExecutionConstants.ACCRUAL,ExecutionConstants.NET,
								ExecutionConstants.NET_INTEREST,pstmtAccountingEntry,
								/** ****** JTEST FIX start ************  on 14DEC09*/
//								pstmtAcctUnpostedInsert,poolId,hmFields);//INT_1039
								pstmtAcctUnpostedInsert,poolId,hmFields, conn);//INT_1039
								/** ****** JTEST FIX end **************  on 14DEC09*/
						}else{
							logger.fatal("Net Interest is 0 for"  + acctDoObj + "SO not accruing the amount ");
						}
					}

					//Add to preparedStmt for AccountingEntryId updation
					if (logger.isDebugEnabled()) {
						logger.debug("After processEntry s_accountingEntryId"  + accountingEntryId);
					}
					//Update the Prepared Statements for OPOOL_PARTYDETAILS
					updateInterest(pstmtTxnPoolPartyDtls,accountingEntryId,poolId,acctDoObj.get_nbr_ownaccount(),ExecutionConstants.ACCRUAL);
					//Add to the Pool List
					//updatePoolList(poolId, hpPoolList);
					//accrualEntries[ExecutionConstants.PARTY_DTLS_UPDATE] = true;
				}
				//End for accrual of participants ending today
				//SUP_1078--Ends
				//FOR ALLOCATION
				String sqlAllocAccrualDtlsSelect1 = (new StringBuffer("SELECT ")
				//.append(" ID_POOL,FLG_CONSOLIDATION, PARTICIPANT_ID, COD_CCY_ACCOUNT, TYP_SETTLE, ")
				.append(" ID_POOL,FLG_CONSOLIDATION, PARTICIPANT_ID, COD_CCY_ACCOUNT, TYP_SETTLE, NBR_PROCESSID, ") // Mashreq_AE_Hook
				.append(" SUM(EQV_AMT_ALLOCATION) SUM,  ALLOCATOR,  NBR_COREACCOUNT,  ")
				.append(" COD_BRANCH, COD_GL, COD_BNK_LGL,  NBR_IBANACCNT ,FLG_ALLOCATION "))
				.append(",DAT_ADJUSTEDACCRUAL,DAT_ACCRUAL ").toString();//SUP_1062//INT_1017

				String sqlAllocAccrualDtlsSelect2 = (new StringBuffer(" SELECT ")
				//.append(" TXN.ID_POOL, TXN.FLG_CONSOLIDATION, PARTY.PARTICIPANT_ID PARTICIPANT_ID, ")
				.append(" TXN.ID_POOL, TXN.FLG_CONSOLIDATION, PARTY.PARTICIPANT_ID PARTICIPANT_ID, TXN.NBR_PROCESSID NBR_PROCESSID, ") // Mashreq_AE_Hook
				.append(" ALLOC.DAT_BUSINESS_POOLRUN, COD_CCY_ACCOUNT, TYP_SETTLE, ")
				.append(" EQV_AMT_ALLOCATION, ALLOCATOR, NARRATION ,NBR_COREACCOUNT, ")
				.append(" COD_BRANCH,PARTY.COD_GL, COD_BNK_LGL, NBR_IBANACCNT,ALLOC.FLG_ALLOCATION ,")
				.append(" ALLOC.PARTY_ALLOC_SEQUENCE ,DAT_ADJUSTEDACCRUAL,TXN.DAT_ACCRUAL FROM")//SUP_1062//INT_1017
				.append(" OPOOL_TXNPOOL_ALLOCATIONDTLS ALLOC, ")
				.append(" OPOOL_TXNPOOLHDR TXN, ")
				.append(" OPOOL_TXNPOOL_PARTYDTLS PARTY")).toString();//INT_1017

				String sqlAllocAccrualDtlsSelect3 = (new StringBuffer(" SELECT ")
				//.append(" TXN.ID_POOL, TXN.FLG_CONSOLIDATION, PARTY.NBR_OWNACCOUNT PARTICIPANT_ID, ")
				.append(" TXN.ID_POOL, TXN.FLG_CONSOLIDATION, PARTY.NBR_OWNACCOUNT PARTICIPANT_ID, TXN.NBR_PROCESSID NBR_PROCESSID,  ")// Mashreq_AE_Hook
				.append(" ALLOC.DAT_BUSINESS_POOLRUN, COD_CCY_ACCOUNT, TYP_SETTLE, ")
				.append(" EQV_AMT_ALLOCATION, ALLOCATOR, NARRATION ,NBR_COREACCOUNT, ")
				.append(" COD_BRANCH,PARTY.COD_GL, COD_BNK_LGL, NBR_IBANACCNT,ALLOC.FLG_ALLOCATION, ALLOC.PARTY_ALLOC_SEQUENCE,DAT_ADJUSTEDACCRUAL,TXN.DAT_ACCRUAL  FROM")//SUP_1062//INT_1017
				.append(" OPOOL_TXNPOOL_ALLOCATIONDTLS ALLOC, ")
				.append(" OPOOL_TXNPOOLHDR TXN, ")
				.append(" ORBICASH_ACCOUNTS PARTY")).toString();

				StringBuilder sqlAllocAccrualDtlsWhere1 = new StringBuilder(" TXN.ID_POOL = ALLOC.ID_POOL AND TXN.ID_POOL = PARTY.ID_POOL ")//INT_1017
				.append(" AND  TXN.DAT_ACCRUAL = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')")//SUP_1062
				//.append(" AND PRCPNT.PARTICIPANT_ID=PARTY.PARTICIPANT_ID ")//INT_1017
				.append(" AND (PARTY.DAT_END IS NULL OR PARTY.DAT_END > TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "'))")//INT_1017
				//.append(" AND (PRCPNT.DAT_END IS NULL OR PRCPNT.DAT_END > TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "'))")//INT_1017
				.append(" AND PARTY.PARTICIPANT_ID IN (SELECT PRCPNT.PARTICIPANT_ID FROM OPOOL_POOLPARTICIPANT PRCPNT WHERE PRCPNT.ID_POOL = TXN.ID_POOL AND PRCPNT.DAT_END IS NULL OR PRCPNT.DAT_END > TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "'))")//INT_1017
				//.append(" AND TXN.COD_GL = '" + client + "'") //INT_1009
				.append(" AND TXN.NBR_PROCESSID = ALLOC.NBR_PROCESSID   ")
				.append(" AND TXN.DAT_BUSINESS_POOLRUN = ALLOC.DAT_BUSINESS_POOLRUN ")
				.append(" AND TXN.NBR_PROCESSID = PARTY.NBR_PROCESSID  ")
				.append(" AND TXN.DAT_BUSINESS_POOLRUN = PARTY.DAT_BUSINESS_POOLRUN ")
				.append(" AND PARTY.ID_POOL = ALLOC.ID_POOL ")
				.append(" AND PARTY.ID_BVT	 = ALLOC.ID_BVT ")
				.append(" AND PARTY.PARTICIPANT_ID = ALLOC.PARTICIPANT_ID      ")
				.append(" AND FLG_REAL = 'Y' ")
				/** ****** JTEST FIX start ************  on 14DEC09*/
//				.append(" AND TXN.TXT_STATUS ='" + ExecutionConstants.ALLOCATED+ "'")
				.append(" AND TXN.TXT_STATUS = ? ")
				/** ****** JTEST FIX end **************  on 14DEC09*/
				.append(" AND FLG_ALLOCATION <> 'R' AND FLG_ALLOCATION <> 'T'   ")//SUP_1028
				.append(" AND (ALLOC.TYP_PARTICIPANT = 'ACCOUNT' AND PARTY.TYP_PARTICIPANT = 'ACCOUNT' ) ")//SUP_1045
				.append(" AND TXN.FLG_REALPOOL = 'Y'   ")
				.append(" AND  ALLOC.ID_BVT = (SELECT MAX(TO_NUMBER(M.ID_BVT)) ")//INT_1026
				.append(" FROM 	 OPOOL_TXNPOOLHDR M ")
				.append(" WHERE M.ID_POOL = TXN.ID_POOL ")
				.append(" AND M.DAT_BUSINESS_POOLRUN = TXN.DAT_BUSINESS_POOLRUN   ")
				.append(" AND M.COD_GL = TXN.COD_GL)")
				.append(" AND ALLOC.DAT_BUSINESS_POOLRUN BETWEEN  NVL(TXN.DAT_LASTACCRUALCALC,TXN.DAT_START-1)+1  AND TXN.DAT_ACCRUAL  ");//SUP_1062
				if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
					sqlAllocAccrualDtlsWhere1.append(" AND TXN.ID_POOL IN (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR LOT ");
					/** ****** JTEST FIX start ************  on 14DEC09*/
//					sqlAllocAccrualDtlsWhere1.append(" WHERE LOT.NBR_LOTID =" + generateForId+")");
					sqlAllocAccrualDtlsWhere1.append(" WHERE LOT.NBR_LOTID =?)");
					/** ****** JTEST FIX end **************  on 14DEC09*/
				}

				StringBuilder sqlAllocAccrualDtlsWhere2 = new StringBuilder(" TXN.ID_POOL = ALLOC.ID_POOL  ")
				.append(" AND TXN.DAT_ACCRUAL = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')")//SUP_1062
				//.append(" AND TXN.COD_GL = '" + client + "'") //INT_1009
				.append(" AND TXN.NBR_PROCESSID = ALLOC.NBR_PROCESSID    ")
				.append(" AND TXN.DAT_BUSINESS_POOLRUN = ALLOC.DAT_BUSINESS_POOLRUN  ")
				.append(" AND PARTY.NBR_OWNACCOUNT = ALLOC.PARTICIPANT_ID ")
				/** ****** JTEST FIX start ************  on 14DEC09*/
//				.append(" AND TXN.TXT_STATUS ='" + ExecutionConstants.ALLOCATED+ "'")
				.append(" AND TXN.TXT_STATUS =?")
				/** ****** JTEST FIX end **************  on 14DEC09*/
				.append(" AND FLG_REAL = 'Y'  ")
				.append(" AND FLG_ALLOCATION <> 'R' AND FLG_ALLOCATION <> 'T' ")//SUP_1028
				.append(" AND (ALLOC.TYP_PARTICIPANT = 'CENTRAL' ) ")
				.append(" AND TXN.FLG_REALPOOL = 'Y'    ")
				.append(" AND ALLOC.ID_BVT = (SELECT MAX(TO_NUMBER(M.ID_BVT)) ")//INT_1026
				.append(" FROM 	 OPOOL_TXNPOOLHDR M ")
				.append(" WHERE M.ID_POOL = TXN.ID_POOL ")
				.append(" AND M.DAT_BUSINESS_POOLRUN = TXN.DAT_BUSINESS_POOLRUN   ")
				.append(" AND M.COD_GL = TXN.COD_GL)")
				.append(" AND ALLOC.DAT_BUSINESS_POOLRUN BETWEEN  NVL(TXN.DAT_LASTACCRUALCALC,TXN.DAT_START-1)+1  AND TXN.DAT_ACCRUAL  ");//SUP_1062
				if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
					sqlAllocAccrualDtlsWhere2.append(" AND TXN.ID_POOL IN (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR LOT ");
					/** ****** JTEST FIX start ************  on 14DEC09*/
//					sqlAllocAccrualDtlsWhere2.append(" WHERE LOT.NBR_LOTID =" + generateForId+")");
					sqlAllocAccrualDtlsWhere2.append(" WHERE LOT.NBR_LOTID =?)");
					/** ****** JTEST FIX end **************  on 14DEC09*/
				}

				String sqlAllocAccrualDtlsgroupBy1 = (new StringBuffer("ID_POOL,")
				.append(" FLG_CONSOLIDATION, PARTICIPANT_ID, ")
				.append(" COD_CCY_ACCOUNT, TYP_SETTLE, ALLOCATOR,  ")
				.append(" NARRATION ,NBR_COREACCOUNT, COD_BRANCH,")
				.append(" COD_GL,COD_BNK_LGL, NBR_IBANACCNT,FLG_ALLOCATION,")
				.append(" PARTY_ALLOC_SEQUENCE "))
				//.append(",DAT_ADJUSTEDACCRUAL,DAT_ACCRUAL ").toString();//SUP_1062//INT_1017
				.append(",DAT_ADJUSTEDACCRUAL,DAT_ACCRUAL,NBR_PROCESSID ").toString();//SUP_1062//INT_1017 // Mashreq_AE_Hook

				String sqlAllocAccrualDtlsgroupBy2 = (new StringBuffer(" ID_POOL,")
				.append(" FLG_CONSOLIDATION,  PARTICIPANT_ID, ")
				.append(" COD_CCY_ACCOUNT, TYP_SETTLE, ALLOCATOR,  ")
				.append(" NBR_COREACCOUNT, COD_BRANCH,COD_GL,")
				.append(" COD_BNK_LGL, NBR_IBANACCNT, FLG_ALLOCATION"))
				.append(",DAT_ADJUSTEDACCRUAL,DAT_ACCRUAL ").toString();//SUP_1062//INT_1017
				//SUP_1078--Starts
				//INT_1017--Starts
				/*
				//Participants which are ending today
				String sqlAllocAccrualDtlsSuddenEnd=(new StringBuffer("SELECT ")
						.append(" TXN.ID_POOL,TXN.FLG_CONSOLIDATION,PARTY.PARTICIPANT_ID, ")
						.append(" COD_CCY_ACCOUNT, TYP_SETTLE, ")
						.append(" SUM(EQV_AMT_ALLOCATION) SUM, ALLOCATOR, NBR_COREACCOUNT,  ")
						.append(" COD_BRANCH,PARTY.COD_GL, COD_BNK_LGL, NBR_IBANACCNT,ALLOC.FLG_ALLOCATION , ")
						.append(" PARTY.DAT_END DAT_ADJUSTEDACCRUAL,PARTY.DAT_END DAT_ACCRUAL ")//INT_1017
						.append(" FROM OPOOL_TXNPOOL_ALLOCATIONDTLS ALLOC, "))
						.append(" OPOOL_TXNPOOLHDR TXN, OPOOL_TXNPOOL_PARTYDTLS PARTY ").toString();

				StringBuffer sqlAllocAccrualSuddenEndWhere = new StringBuffer(" TXN.ID_POOL = ALLOC.ID_POOL AND TXN.ID_POOL = PARTY.ID_POOL  ")
				.append(" AND PARTY.DAT_END = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "') ")
				.append(" AND TXN.NBR_PROCESSID = ALLOC.NBR_PROCESSID   ")
				.append(" AND TXN.DAT_BUSINESS_POOLRUN = ALLOC.DAT_BUSINESS_POOLRUN ")
				.append(" AND TXN.NBR_PROCESSID = PARTY.NBR_PROCESSID  ")
				.append(" AND TXN.DAT_BUSINESS_POOLRUN = PARTY.DAT_BUSINESS_POOLRUN ")
				.append(" AND PARTY.ID_POOL = ALLOC.ID_POOL ")
				.append(" AND PARTY.ID_BVT	 = ALLOC.ID_BVT ")
				.append(" AND PARTY.PARTICIPANT_ID = ALLOC.PARTICIPANT_ID      ")
				.append(" AND FLG_REAL = 'Y' ")
				.append(" AND TXN.TXT_STATUS ='" + ExecutionConstants.ALLOCATED+ "'")
				.append(" AND FLG_ALLOCATION <> 'R' AND FLG_ALLOCATION <> 'T'   ")
				.append(" AND (ALLOC.TYP_PARTICIPANT = 'ACCOUNT' AND PARTY.TYP_PARTICIPANT = 'ACCOUNT' ) ")
				.append(" AND TXN.FLG_REALPOOL = 'Y'   ")
				.append(" AND  ALLOC.ID_BVT = (SELECT MAX(TO_NUMBER(M.ID_BVT)) ")
				.append(" FROM 	 OPOOL_TXNPOOLHDR M ")
				.append(" WHERE M.ID_POOL = TXN.ID_POOL ")
				.append(" AND M.DAT_BUSINESS_POOLRUN = TXN.DAT_BUSINESS_POOLRUN   ")
				.append(" AND M.COD_GL = TXN.COD_GL)")
				.append(" AND ALLOC.DAT_BUSINESS_POOLRUN BETWEEN  NVL(TXN.DAT_LASTACCRUALCALC,TXN.DAT_START-1)+1  AND TXN.DAT_ACCRUAL  ");//SUP_1062
				if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
					sqlAllocAccrualSuddenEndWhere.append(" AND TXN.ID_POOL IN (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR LOT ");
					sqlAllocAccrualSuddenEndWhere.append(" WHERE LOT.NBR_LOTID =" + generateForId+")");
				}
				*/
				//Participants which have been closed
				/*
				String sqlAllocAccrualDtlsSuddenClose=(new StringBuffer("SELECT ")
						.append(" TXN.ID_POOL,TXN.FLG_CONSOLIDATION,PARTY.PARTICIPANT_ID, ")
						.append(" COD_CCY_ACCOUNT, TYP_SETTLE, ")
						.append(" SUM(EQV_AMT_ALLOCATION) SUM, ALLOCATOR, NBR_COREACCOUNT,  ")
						.append(" COD_BRANCH,PARTY.COD_GL, COD_BNK_LGL, NBR_IBANACCNT,ALLOC.FLG_ALLOCATION , ")
						.append(" (PRCIPANT.DAT_END + 1) DAT_ADJUSTEDACCRUAL,(PRCIPANT.DAT_END + 1) DAT_ACCRUAL ")
						.append(" FROM OPOOL_TXNPOOL_ALLOCATIONDTLS ALLOC, "))
						.append(" OPOOL_TXNPOOLHDR TXN, OPOOL_TXNPOOL_PARTYDTLS PARTY, OPOOL_POOLPARTICIPANT PRCIPANT ").toString();

				StringBuffer sqlAllocAccrualSuddenCloseWhere = new StringBuffer(" TXN.ID_POOL = ALLOC.ID_POOL AND TXN.ID_POOL = PARTY.ID_POOL AND TXN.ID_POOL = PRCIPANT.ID_POOL ")
				.append(" AND PARTY.PARTICIPANT_ID=PRCIPANT.PARTICIPANT_ID ")
				.append(" AND TXN.DAT_ACCRUAL > TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')")
				.append(" AND PRCIPANT.DAT_END = (TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')-1 )")
				.append(" AND PRCIPANT.FLG_ACCOUNTCLS = 'Y'  ")
				.append(" AND TXN.NBR_PROCESSID = ALLOC.NBR_PROCESSID   ")
				.append(" AND TXN.DAT_BUSINESS_POOLRUN = ALLOC.DAT_BUSINESS_POOLRUN ")
				.append(" AND TXN.NBR_PROCESSID = PARTY.NBR_PROCESSID  ")
				.append(" AND TXN.DAT_BUSINESS_POOLRUN = PARTY.DAT_BUSINESS_POOLRUN ")
				.append(" AND PARTY.ID_POOL = ALLOC.ID_POOL ")
				.append(" AND PARTY.ID_BVT	 = ALLOC.ID_BVT ")
				.append(" AND PARTY.PARTICIPANT_ID = ALLOC.PARTICIPANT_ID      ")
				.append(" AND FLG_REAL = 'Y' ")
				.append(" AND TXN.TXT_STATUS ='" + ExecutionConstants.ALLOCATED+ "'")
				.append(" AND FLG_ALLOCATION <> 'R' AND FLG_ALLOCATION <> 'T'   ")
				.append(" AND (ALLOC.TYP_PARTICIPANT = 'ACCOUNT' AND PARTY.TYP_PARTICIPANT = 'ACCOUNT' ) ")
				.append(" AND TXN.FLG_REALPOOL = 'Y'   ")
				.append(" AND  ALLOC.ID_BVT = (SELECT MAX(TO_NUMBER(M.ID_BVT)) ")
				.append(" FROM 	 OPOOL_TXNPOOLHDR M ")
				.append(" WHERE M.ID_POOL = TXN.ID_POOL ")
				.append(" AND M.DAT_BUSINESS_POOLRUN = TXN.DAT_BUSINESS_POOLRUN   ")
				.append(" AND M.COD_GL = TXN.COD_GL)")
				.append(" AND ALLOC.DAT_BUSINESS_POOLRUN BETWEEN  NVL(TXN.DAT_LASTACCRUALCALC,TXN.DAT_START-1)+1  AND TXN.DAT_ACCRUAL  ");//SUP_1062
				if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
					sqlAllocAccrualSuddenCloseWhere.append(" AND TXN.ID_POOL IN (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR LOT ");
					sqlAllocAccrualSuddenCloseWhere.append(" WHERE LOT.NBR_LOTID =" + generateForId+")");
				}
				/*
				String sqlAllocAccrualSuddenEndGrpBy = (new StringBuffer("TXN.ID_POOL,")
						.append(" FLG_CONSOLIDATION, PARTY.PARTICIPANT_ID, ")
						.append(" COD_CCY_ACCOUNT, TYP_SETTLE, ALLOCATOR,  ")
						.append(" NBR_COREACCOUNT, COD_BRANCH, PARTY.COD_GL,")
						.append(" COD_BNK_LGL, NBR_IBANACCNT,FLG_ALLOCATION, ")
						.append(" PARTY_ALLOC_SEQUENCE  "))
						.append(",PARTY.DAT_END ").toString();
                */
				/*
				String sqlAllocAccrualSuddenCloseGrpBy = (new StringBuffer("TXN.ID_POOL,")
						.append(" FLG_CONSOLIDATION, PARTY.PARTICIPANT_ID, ")
						.append(" COD_CCY_ACCOUNT, TYP_SETTLE, ALLOCATOR,  ")
						.append(" NBR_COREACCOUNT, COD_BRANCH, PARTY.COD_GL,")
						.append(" COD_BNK_LGL, NBR_IBANACCNT,FLG_ALLOCATION, ")
						.append(" PARTY_ALLOC_SEQUENCE  "))
						.append(",PRCIPANT.DAT_END ").toString();
				*/
//INT_1017--Ends
				StringBuilder sqlAllocAccrualDtls = new StringBuilder("")
				.append(sqlAllocAccrualDtlsSelect1)
				.append(" FROM ( ")
				.append(sqlAllocAccrualDtlsSelect2)
				.append(" WHERE ")
				.append(sqlAllocAccrualDtlsWhere1.toString())
				.append(" UNION ")
				.append(sqlAllocAccrualDtlsSelect3)
				.append(" WHERE ")
				.append(sqlAllocAccrualDtlsWhere2.toString())
				.append(")")
				//INT_1008 START
				//.append(")V	WHERE 	(FLG_CONSOLIDATION = 'N' OR FLG_CONSOLIDATION IS NULL) ")
				//ALWAYS PASS SEPARATE ENTRIES
				.append(" GROUP BY ")
				.append(sqlAllocAccrualDtlsgroupBy1);//SUP_1004
				//INT_1017--Starts
				//.append(" UNION ")
				//.append(sqlAllocAccrualDtlsSuddenEnd)
				//.append(" WHERE ")
				//.append(sqlAllocAccrualSuddenEndWhere)
				//.append(" GROUP BY ")
				//.append(sqlAllocAccrualSuddenEndGrpBy)
				/*.append(" UNION ")
				.append(sqlAllocAccrualDtlsSuddenClose)
				.append(" WHERE ")
				.append(sqlAllocAccrualSuddenCloseWhere)
				.append(" GROUP BY ")
				.append(sqlAllocAccrualSuddenCloseGrpBy);*/
//INT_1017--Ends
				/*.append(" UNION ")
				.append(sqlAllocAccrualDtlsSelect1)
				.append(" FROM (")
				.append(sqlAllocAccrualDtlsSelect2)
				.append(" WHERE ")
				.append(sqlAllocAccrualDtlsWhere1.toString())
				.append(" UNION ")
				.append(sqlAllocAccrualDtlsSelect3)
				.append(" WHERE ")
				.append(sqlAllocAccrualDtlsWhere2.toString())
				.append(")V	 WHERE 	FLG_CONSOLIDATION = 'Y'")
				.append(" GROUP BY ")
				.append(sqlAllocAccrualDtlsgroupBy2);
				//INT_1008 END
				*/
				//SUP_1078--Ends
				if (logger.isDebugEnabled())
					logger.debug("Query ACCRUAL ALLOCATION: " + sqlAllocAccrualDtls);

				/** ****** JTEST FIX start ************  on 14DEC09*/
//				rs = stmt.executeQuery(sqlAllocAccrualDtls.toString());
				pStmt = conn.prepareStatement(sqlAllocAccrualDtls.toString());
				pStmt.setString(1, ExecutionConstants.ALLOCATED);
				pStmt.setString(2, ExecutionConstants.ALLOCATED);
				if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
					pStmt.setString(3, generateForId);
					pStmt.setString(4, generateForId);
				}
				rs = pStmt.executeQuery();
				/** ****** JTEST FIX end **************  on 14DEC09*/

				if (logger.isDebugEnabled()) {
					logger.debug(" Executed Query  ACCRUAL ALLOCATION ");
				}
				results = false;
				String details = "";
				String extraDetails = "";
				String typSettle="";//SUP_1031
				while(rs.next()){
					results = true;
					//bd_amountToPost = new BigDecimal(0);
					hmFields.put("DAT_POSTING",(Date)rs.getDate("DAT_ADJUSTEDACCRUAL"));//SUP_1062
					hmFields.put("DAT_SENT",(Date)rs.getDate("DAT_ACCRUAL"));//INT_1017
					acctDoObj = new AccountingDO();
					extraDetails = "";
					details = "";
					poolId = "";
					accountingEntryId = "";

					poolId = rs.getString("ID_POOL");
					hmFields.put(ExecutionConstants.PROCESSID, rs.getLong("NBR_PROCESSID")); // Mashreq_AE_Hook
					amountToPost = new BigDecimal(rs.getString("SUM"));
					flagAllocation = rs.getString("FLG_ALLOCATION");

					//fill in the AccountingDo object's attributes.
					acctDoObj.set_nbr_ownaccount(rs.getString("PARTICIPANT_ID"));
					acctDoObj.set_nbr_coreaccount(rs.getString("NBR_COREACCOUNT"));
					acctDoObj.set_cod_branch(rs.getString("COD_BRANCH"));
					acctDoObj.set_cod_gl(rs.getString("COD_GL"));
					acctDoObj.set_cod_bnk_lgl(rs.getString("COD_BNK_LGL"));
					acctDoObj.set_nbr_ibanacct(rs.getString("NBR_IBANACCNT"));
					acctDoObj.set_cod_ccy(rs.getString("COD_CCY_ACCOUNT"));
					//put this in extraDetailsField HashMap
					extraDetails = "ALLOCATOR" + rs.getString("ALLOCATOR")+ "TYP_SETTLE" + rs.getString("TYP_SETTLE");
					//details = "ALLOCATION";//this maps to TXT_EXTINFO2//SUP_1031
					hmFields.put("POOL_FLG_CONSOLIDATION",rs.getString("FLG_CONSOLIDATION"));//INT_1008 add FLG_CONSOLIDATION in fieldsHashMap
					if (logger.isDebugEnabled()) {
						logger.debug("Account Object for Allocation" + acctDoObj);
					}
//SUP_1031--Starts
					typSettle=rs.getString("TYP_SETTLE");
					if(typSettle.equalsIgnoreCase("PARTICIPANTCREDIT")||typSettle.equalsIgnoreCase("CENTRALCREDIT")){
						details = ExecutionConstants.ALLOCATIONCR;//this maps to TXT_EXTINFO2
					}else if(typSettle.equalsIgnoreCase("PARTICIPANTDEBIT")||typSettle.equalsIgnoreCase("CENTRALDEBIT")){
						details = ExecutionConstants.ALLOCATIONDR;//this maps to TXT_EXTINFO2
					}
					//SUP_1031--Starts
					/*
					bd_amtNotPosted = setUnpostedAmounts(ExecutionConstants.ALLOCATION,acctDoObj,
							pstmtAccountUnposted,businessdate,ExecutionConstants.ACCRUAL);
					*/
					bd_amtNotPosted = setUnpostedAmounts(details,acctDoObj,
							pstmtAccountUnposted,businessdate,ExecutionConstants.ACCRUAL);

					if (logger.isDebugEnabled()) {
						logger.debug("Unposted Amount "  + bd_amtNotPosted +"for " + details);
					}
					//chk if any amount is left to be posted
					if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
						amountToPost = amountToPost.add(bd_amtNotPosted);
						if (logger.isDebugEnabled()) {
							logger.debug("Adding Unposted Amount  "  + bd_amtNotPosted +"for ExecutionConstants.ALLOCATION "+amountToPost);
						}
					}// UNPOSTED AMOUNT
					//SUP_1031--Ends
					if (amountToPost.compareTo(zero) != 0){
						accountingEntryId = "" + processEntry(acctDoObj,
								amountToPost,ExecutionConstants.ACCRUAL,
								ExecutionConstants.NET, details,pstmtAccountingEntry,
								/** ****** JTEST FIX start ************  on 14DEC09*/
//								pstmtAcctUnpostedInsert,poolId,hmFields);//INT_1039
								pstmtAcctUnpostedInsert,poolId,hmFields, conn);//INT_1039
								/** ****** JTEST FIX end **************  on 14DEC09*/
					}else{
						if (logger.isDebugEnabled()) {
							logger.debug("Not Accruing alocated amount as it is 0 " + acctDoObj);
						}
					}
					//accrualEntries[ExecutionConstants.OLM_ACCOUNTINGENTRY_INSERT] = true;
					//call Allocation update
					//updateAllocationAccrual(pstmtTxnPoolAllocation,acctDoObj.get_nbr_ownaccount(),s_accountingEntryId,s_poolId, s_flagAllocation);
					//accrualEntries[ExecutionConstants.ALLOCATION_UPDATE] = true;
				}
				if (!results){
					logger.fatal(" No records for Accrual -Allocation" + sqlAllocAccrualDtls.toString());
				}
				//SUP_1078--Starts
				//For participants which are ending today
				String sqlAllocAccrualDtlsSuddenEnd=(new StringBuffer("SELECT ")
						//.append(" TXN.ID_POOL,TXN.FLG_CONSOLIDATION,PARTY.PARTICIPANT_ID, ")
						.append(" TXN.ID_POOL,TXN.FLG_CONSOLIDATION,PARTY.PARTICIPANT_ID,  TXN.NBR_PROCESSID, ") //Mashreq_AE_Hook
						.append(" COD_CCY_ACCOUNT, TYP_SETTLE, ")
						.append(" SUM(EQV_AMT_ALLOCATION) SUM, ALLOCATOR, NBR_COREACCOUNT,  ")
						.append(" COD_BRANCH,PARTY.COD_GL, COD_BNK_LGL, NBR_IBANACCNT,ALLOC.FLG_ALLOCATION , ")
						.append(" PARTY.DAT_END DAT_ADJUSTEDACCRUAL,PARTY.DAT_END DAT_ACCRUAL ")//INT_1017
						.append(" FROM OPOOL_TXNPOOL_ALLOCATIONDTLS ALLOC, "))
						.append(" OPOOL_TXNPOOLHDR TXN, OPOOL_TXNPOOL_PARTYDTLS PARTY ").toString();

				StringBuilder sqlAllocAccrualSuddenEndWhere = new StringBuilder(" TXN.ID_POOL = ALLOC.ID_POOL AND TXN.ID_POOL = PARTY.ID_POOL  ")
				/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
				/* SUP_HSBC_019 Starts::: checking participant for the same run date. */
				//.append(" AND PARTY.DAT_END = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')")
				.append(" AND PARTY.DAT_END = (select distinct DAT_END from OPOOL_TXNPOOL_PARTYDTLS party_temp " + 
				        " where party_temp.dat_business_poolrun = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "') " +
		 				" and party_temp.dat_end = party_temp.dat_business_poolrun "  +
						" and party_temp.participant_id = PARTY.participant_id 	) ")
				.append(" AND TXN.NBR_PROCESSID = PARTY.NBR_PROCESSID AND TXN.DAT_BUSINESS_POOLRUN = PARTY.DAT_BUSINESS_POOLRUN")
 				/* SUP_HSBC_019 ENDS...*/
				/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
				.append(" AND TXN.NBR_PROCESSID = ALLOC.NBR_PROCESSID   ")
				.append(" AND TXN.DAT_BUSINESS_POOLRUN = ALLOC.DAT_BUSINESS_POOLRUN ")
				.append(" AND TXN.NBR_PROCESSID = PARTY.NBR_PROCESSID  ")
				.append(" AND TXN.DAT_BUSINESS_POOLRUN = PARTY.DAT_BUSINESS_POOLRUN ")
				.append(" AND PARTY.ID_POOL = ALLOC.ID_POOL ")
				.append(" AND PARTY.ID_BVT	 = ALLOC.ID_BVT ")
				.append(" AND PARTY.PARTICIPANT_ID = ALLOC.PARTICIPANT_ID      ")
				.append(" AND FLG_REAL = 'Y' ")
				/** ****** JTEST FIX start ************  on 14DEC09*/
//				.append(" AND TXN.TXT_STATUS ='" + ExecutionConstants.ALLOCATED+ "'")
				.append(" AND TXN.TXT_STATUS = ?")
				/** ****** JTEST FIX end **************  on 14DEC09*/
				.append(" AND FLG_ALLOCATION <> 'R' AND FLG_ALLOCATION <> 'T'   ")
				.append(" AND (ALLOC.TYP_PARTICIPANT = 'ACCOUNT' AND PARTY.TYP_PARTICIPANT = 'ACCOUNT' ) ")
				.append(" AND TXN.FLG_REALPOOL = 'Y'   ")
				.append(" AND  ALLOC.ID_BVT = (SELECT MAX(TO_NUMBER(M.ID_BVT)) ")
				.append(" FROM 	 OPOOL_TXNPOOLHDR M ")
				.append(" WHERE M.ID_POOL = TXN.ID_POOL ")
				.append(" AND M.DAT_BUSINESS_POOLRUN = TXN.DAT_BUSINESS_POOLRUN   ")
				.append(" AND M.COD_GL = TXN.COD_GL)")
				.append(" AND ALLOC.DAT_BUSINESS_POOLRUN BETWEEN  NVL(TXN.DAT_LASTACCRUALCALC,TXN.DAT_START-1)+1  AND TXN.DAT_ACCRUAL  ");//SUP_1062
				if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
					sqlAllocAccrualSuddenEndWhere.append(" AND TXN.ID_POOL IN (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR LOT ");
					/** ****** JTEST FIX start ************  on 14DEC09*/
//					sqlAllocAccrualSuddenEndWhere.append(" WHERE LOT.NBR_LOTID =" + generateForId+")");
					sqlAllocAccrualSuddenEndWhere.append(" WHERE LOT.NBR_LOTID =?)");
					/** ****** JTEST FIX end **************  on 14DEC09*/
				}
				String sqlAllocAccrualSuddenEndGrpBy = (new StringBuffer("TXN.ID_POOL,")
						.append(" FLG_CONSOLIDATION, PARTY.PARTICIPANT_ID, ")
						.append(" COD_CCY_ACCOUNT, TYP_SETTLE, ALLOCATOR,  ")
						.append(" NBR_COREACCOUNT, COD_BRANCH, PARTY.COD_GL,")
						.append(" COD_BNK_LGL, NBR_IBANACCNT,FLG_ALLOCATION, ")
						.append(" PARTY_ALLOC_SEQUENCE  "))
						//.append(",PARTY.DAT_END ").toString();
						.append(",PARTY.DAT_END, TXN.NBR_PROCESSID ").toString(); // Mashreq_AE_Hook

				StringBuilder sqlAllocAccrualEnd = new StringBuilder("")
				.append(sqlAllocAccrualDtlsSuddenEnd)
				.append(" WHERE ")
				.append(sqlAllocAccrualSuddenEndWhere)
				.append(" GROUP BY ")
				.append(sqlAllocAccrualSuddenEndGrpBy);
				
				if (logger.isDebugEnabled())
					logger.debug("Query ACCRUAL ALLOCATION FOR PARTICIPANTS END: " + sqlAllocAccrualEnd);

				/** ****** JTEST FIX start ************  on 14DEC09*/
//				rs = stmt.executeQuery(sqlAllocAccrualEnd.toString());
				
				pStmt = conn.prepareStatement(sqlAllocAccrualEnd.toString());
				pStmt.setString(1, ExecutionConstants.ALLOCATED);
				if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
					pStmt.setString(2, generateForId);
				}
				rs = pStmt.executeQuery();
				/** ****** JTEST FIX end **************  on 14DEC09*/
				
				while(rs.next()){
					adjustedAccrual=rs.getDate("DAT_ADJUSTEDACCRUAL");
					
					hmFields.put(ExecutionConstants.PROCESSID, rs.getLong("NBR_PROCESSID")); // Mashreq_AE_Hook
					//ENH_LIQ_0005 Starts
					//sqlQuery="SELECT COD_CALENDAR FROM ORBICASH_GLMST WHERE COD_GL = (SELECT COD_GL FROM ORBICASH_ACCOUNTS WHERE NBR_OWNACCOUNT ='"+rs.getString("PARTICIPANT_ID")+"')";
					/** ****** JTEST FIX start ************  on 14DEC09*/
//					sqlQuery="SELECT COD_CALENDAR FROM OLM_GLMST WHERE COD_GL = (SELECT COD_GL FROM ORBICASH_ACCOUNTS WHERE NBR_OWNACCOUNT ='"+rs.getString("PARTICIPANT_ID")+"')";
//					//ENH_LIQ_0005 Ends
//					
//					rSet=stmtCal.executeQuery(sqlQuery);
					
					sqlQuery="SELECT COD_CALENDAR FROM OLM_GLMST WHERE COD_GL = (SELECT COD_GL FROM ORBICASH_ACCOUNTS WHERE NBR_OWNACCOUNT = ?)";
					//ENH_LIQ_0005 Ends
					
					pStmtCal = conn.prepareStatement(sqlQuery);
					pStmtCal.setString(1, rs.getString("PARTICIPANT_ID"));
					rSet1 = pStmtCal.executeQuery();
					/** ****** JTEST FIX end **************  on 14DEC09*/
					if(rSet1.next()){
						calendar=rSet1.getString("COD_CALENDAR");
						CalendarService cal=CalendarService.getCalendarService();
						isHoliday=cal.isDateHoliday(calendar,adjustedAccrual);
						if(isHoliday){
							sqlQueryForOption="SELECT COD_NON_WORKING_DAY FROM ORATE_ACCRUAL_CYCLE WHERE COD_ACCR_CYCLE = " +
							"(SELECT COD_ACCR_CYCLE FROM OPOOL_TXNPOOLHDR WHERE ID_POOL = ? " +
											"AND DAT_BUSINESS_POOLRUN = ?)";
							pStmtNonWorkingDay = conn.prepareStatement(sqlQueryForOption);
							pStmtNonWorkingDay.setString(1, rs.getString("ID_POOL"));
							pStmtNonWorkingDay.setDate(2,LMSUtility.truncSqlDate( new java.sql.Date(businessdate.getTime())));
							resultSet1= pStmtNonWorkingDay.executeQuery();
							/** ****** JTEST FIX end **************  on 14DEC09*/
							if(resultSet1.next()){
								Option=resultSet1.getString("COD_NON_WORKING_DAY");
								if(Option.equalsIgnoreCase("NEXT")){
									adjustedAccrual=cal.getNextWorkingDay(calendar,adjustedAccrual);
								}else if(Option.equalsIgnoreCase("PREV")){
									adjustedAccrual=cal.getPrevWorkingDay(calendar,adjustedAccrual);
								}
							}
						}
					}
					LMSConnectionUtility.close(resultSet1);
					LMSConnectionUtility.close(rSet1);
					// SUP_2034:Issue659 - Start: commented
					//LMSConnectionUtility.close(stmtCal);
					//LMSConnectionUtility.close(stmtNonWorkingDay);
					// SUP_2034:Issue659 - End
					hmFields.put("DAT_POSTING",adjustedAccrual);//SUP_1062
					hmFields.put("DAT_SENT",(Date)rs.getDate("DAT_ACCRUAL"));//INT_1017
					acctDoObj = new AccountingDO();
					/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
					//ENH_1110 Starts
					acctDoObj.setExpiringToday(true);
					//ENH_1110 Ends
					/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
					extraDetails = "";
					details = "";
					poolId = "";
					accountingEntryId = "";

					poolId = rs.getString("ID_POOL");
					amountToPost = new BigDecimal(rs.getString("SUM"));
					//flagAllocation = rs.getString("FLG_ALLOCATION"); jtest

					//fill in the AccountingDo object's attributes.
					acctDoObj.set_nbr_ownaccount(rs.getString("PARTICIPANT_ID"));
					acctDoObj.set_nbr_coreaccount(rs.getString("NBR_COREACCOUNT"));
					acctDoObj.set_cod_branch(rs.getString("COD_BRANCH"));
					acctDoObj.set_cod_gl(rs.getString("COD_GL"));
					acctDoObj.set_cod_bnk_lgl(rs.getString("COD_BNK_LGL"));
					acctDoObj.set_nbr_ibanacct(rs.getString("NBR_IBANACCNT"));
					acctDoObj.set_cod_ccy(rs.getString("COD_CCY_ACCOUNT"));
					//put this in extraDetailsField HashMap
					extraDetails = "ALLOCATOR" + rs.getString("ALLOCATOR")+ "TYP_SETTLE" + rs.getString("TYP_SETTLE");
					//details = "ALLOCATION";//this maps to TXT_EXTINFO2//SUP_1031
					hmFields.put("POOL_FLG_CONSOLIDATION",rs.getString("FLG_CONSOLIDATION"));//INT_1008 add FLG_CONSOLIDATION in fieldsHashMap
					if (logger.isDebugEnabled()) {
						logger.debug("Account Object for Allocation" + acctDoObj);
					}
//SUP_1031--Starts
					typSettle=rs.getString("TYP_SETTLE");
					if(typSettle.equalsIgnoreCase("PARTICIPANTCREDIT")||typSettle.equalsIgnoreCase("CENTRALCREDIT")){
						details = ExecutionConstants.ALLOCATIONCR;//this maps to TXT_EXTINFO2
					}else if(typSettle.equalsIgnoreCase("PARTICIPANTDEBIT")||typSettle.equalsIgnoreCase("CENTRALDEBIT")){
						details = ExecutionConstants.ALLOCATIONDR;//this maps to TXT_EXTINFO2
					}
					//SUP_1031--Starts
					/*
					bd_amtNotPosted = setUnpostedAmounts(ExecutionConstants.ALLOCATION,acctDoObj,
							pstmtAccountUnposted,businessdate,ExecutionConstants.ACCRUAL);
					*/
					bd_amtNotPosted = setUnpostedAmounts(details,acctDoObj,
							pstmtAccountUnposted,businessdate,ExecutionConstants.ACCRUAL);

					if (logger.isDebugEnabled()) {
						logger.debug("Unposted Amount "  + bd_amtNotPosted +"for " + details);
					}
					//chk if any amount is left to be posted
					if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
						amountToPost = amountToPost.add(bd_amtNotPosted);
						if (logger.isDebugEnabled()) {
							logger.debug("Adding Unposted Amount  "  + bd_amtNotPosted +"for ExecutionConstants.ALLOCATION "+amountToPost);
						}
					}// UNPOSTED AMOUNT
					//SUP_1031--Ends
					if (amountToPost.compareTo(zero) != 0){
						accountingEntryId = "" + processEntry(acctDoObj,
								amountToPost,ExecutionConstants.ACCRUAL,
								ExecutionConstants.NET, details,pstmtAccountingEntry,
								/** ****** JTEST FIX start ************  on 14DEC09*/
//								pstmtAcctUnpostedInsert,poolId,hmFields);//INT_1039
								pstmtAcctUnpostedInsert,poolId,hmFields, conn);//INT_1039
								/** ****** JTEST FIX end **************  on 14DEC09*/
					}else{
						if (logger.isDebugEnabled()) {
							logger.debug("Not Accruing alocated amount as it is 0 " + acctDoObj);
						}
					}
					
				}
				//Ending for participants which are ending today
				//SUP_1078--Ends
				/*INT_1039
				if (logger.isDebugEnabled()) {
					logger.debug("Buffer value FOR INSERTING ACCOUNT UNPOSTED TABLE=  " + sbufUnpostedAmtInsr);
				}

				if(!("".equals(sbufUnpostedAmtInsr.toString()))){
					accrualEntries[ExecutionConstants.UNPOSTED_AMOUNT_INSERT] = (new Boolean(sbufUnpostedAmtInsr.toString())).booleanValue();
				}
				*/
				//INT_1002 generate BankShare and Treasury Share Entries
				generateBankShareTreasuryShareEntries(pstmtAccountingEntry,
							pstmtAcctUnpostedInsert,businessdate,strbusinessdate,ExecutionConstants.ACCRUAL,generateFor,generateForId,client);

				/** ****** FindBugs Fix Start ***********  on 14DEC09*/
			}catch(SQLException e){
				logger.fatal("generateAccrualEntries: ", e);
				throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, e.getMessage());
			}catch(NamingException e){
				logger.fatal("generateAccrualEntries: ", e);
				throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, e.getMessage());
				/** ****** FindBugs Fix End ***********  on 14DEC09*/
			}finally{
				LMSConnectionUtility.close(rs);
				/** ****** JTEST FIX start ************  on 14DEC09*/
//				LMSConnectionUtility.close(stmt);
//				// SUP_2034:Issue659 - Start: added here
//				LMSConnectionUtility.close(stmtCal);
//				LMSConnectionUtility.close(stmtNonWorkingDay);
				LMSConnectionUtility.close(pStmt);
				LMSConnectionUtility.close(pStmtCal);
				LMSConnectionUtility.close(pStmtNonWorkingDay);
				/** ****** JTEST FIX end **************  on 14DEC09*/
				// SUP_2034:Issue659 - End
				//JTEST FIX on 28ARIL11 start
				LMSConnectionUtility.close(conn);
				LMSConnectionUtility.close(resultSet);
				LMSConnectionUtility.close(rSet);
				LMSConnectionUtility.close(resultSet1);
				LMSConnectionUtility.close(rSet1);
				//JTEST FIX on 28ARIL11 end
			}
			return accrualEntries;
		}


	/**
	 * @param pstmtAccountUnposted
	 * @param businessdate
	* @param accrualEntries
	 * @param acctUnposted
	 * @param type
		 * @throws SQLException
		 * @throws LMSException
	 */
	private BigDecimal setUnpostedAmounts(String amountType,AccountingDO account,
			PreparedStatement pstmtAccountUnposted, Date businessdate,String type) throws LMSException{
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		Connection conn = null;
//		Statement stmt = null;
		/** ****** JTEST FIX end **************  on 14DEC09*/
		/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//		ResultSet rs = null;
		/** ****** FindBugs Fix End ***********  on 14DEC09*/

		BigDecimal amtUnposted = new BigDecimal(0);
		String nbrAcctUnposted = "";
		/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//		String dateFormate = LMSConstants.DATE_FORMAT;
		/** ****** FindBugs Fix End ***********  on 14DEC09*/
		if (logger.isDebugEnabled())
			logger.debug("Entering");

		try{
			if(type.equalsIgnoreCase(ExecutionConstants.POSTING)){//SUP_1031
				//FIRST CHECK IF IT IS PRESENT IN THE HASHMAP OF CREDIT_INTEREST
	//			INT_1039--Starts
				if(amountType.equalsIgnoreCase(ExecutionConstants.CREDIT_INTEREST)){
					amtUnposted=(BigDecimal)_hmCR_INT_UNPOSTEDMap.get(account.get_nbr_ownaccount());
					_hmCR_INT_UNPOSTEDMap.remove(account.get_nbr_ownaccount());//SUP_1031
				}
				else if(amountType.equalsIgnoreCase(ExecutionConstants.DEBIT_INTEREST)){
					amtUnposted=(BigDecimal)_hmDR_INT_UNPOSTEDMap.get(account.get_nbr_ownaccount());
					_hmDR_INT_UNPOSTEDMap.remove(account.get_nbr_ownaccount());//SUP_1031
				}
				else if(amountType.equalsIgnoreCase(ExecutionConstants.ALLOCATIONCR)){
					amtUnposted=(BigDecimal)_hmALLOCATIONCR_UNPOSTEDMap.get(account.get_nbr_ownaccount());
					_hmALLOCATIONCR_UNPOSTEDMap.remove(account.get_nbr_ownaccount());//SUP_1031
				}
//SUP_1031--Starts
				else if(amountType.equalsIgnoreCase(ExecutionConstants.ALLOCATIONDR)){
					amtUnposted=(BigDecimal)_hmALLOCATIONDR_UNPOSTEDMap.get(account.get_nbr_ownaccount());
					_hmALLOCATIONDR_UNPOSTEDMap.remove(account.get_nbr_ownaccount());
				}
//SUP_1031--Ends
				else if(amountType.equalsIgnoreCase(ExecutionConstants.REALLOCATION)){
					amtUnposted=(BigDecimal)_hmREALLOCATION_UNPOSTEDMap.get(account.get_nbr_ownaccount());
					_hmREALLOCATION_UNPOSTEDMap.remove(account.get_nbr_ownaccount());//SUP_1031
				}
				else if(amountType.equalsIgnoreCase(ExecutionConstants.NET_INTEREST)){
					amtUnposted=(BigDecimal)_hmNET_INT_UNPOSTEDMap.get(account.get_nbr_ownaccount());
					_hmNET_INT_UNPOSTEDMap.remove(account.get_nbr_ownaccount());//SUP_1031
				}
				else if(amountType.equalsIgnoreCase(ExecutionConstants.CENTRAL_INTEREST)){
					amtUnposted=(BigDecimal)_hmCENTRAL_INT_UNPOSTEDMap.get(account.get_nbr_ownaccount());
					_hmCENTRAL_INT_UNPOSTEDMap.remove(account.get_nbr_ownaccount());//SUP_1031
				}
				else{
					logger.fatal("No Unposted amount for account" +account.get_nbr_ownaccount());
				}
				if (logger.isDebugEnabled())
					logger.debug("Unposted amount :"+amtUnposted);
		/*		StringBuffer sbufUnpostedAmt = new StringBuffer(" SELECT ")
							.append(" SUM(AMOUNT)SUM_AMT, NBR_OWNACCOUNT ")
							.append(" FROM OPOOL_ACCOUNT_UNPOSTED")
							.append(" WHERE USE_DATE IS NULL ")
							.append(" AND POSTING_TYPE = '"+type+"'")
							.append(" AND NBR_OWNACCOUNT = "+account.get_nbr_ownaccount())
							.append(" AND AMOUNT_TYPE ='" + amountType +"'")
							//.append(" AND POST_DATE  <> TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "'")
							.append(" GROUP BY NBR_OWNACCOUNT,AMOUNT_TYPE ");

				if (logger.isDebugEnabled()) {
					logger.debug("Amount Unposted Query  =  "+ sbufUnpostedAmt.toString());
				}
				//execute the query

				conn = LMSConnectionUtility.getConnection();
				stmt = conn.createStatement();

				rs = stmt.executeQuery(sbufUnpostedAmt.toString());

				if (rs.next()){
					nbrAcctUnposted = rs.getString("NBR_OWNACCOUNT");
					amtUnposted = rs.getBigDecimal("SUM_AMT");

					//Add this in the HashMap for AmountUnposted CREDIT_INTEREST
					if (logger.isDebugEnabled()) {
						logger.debug("Amount Unposted Details: AMOUNT = "+ amtUnposted +
								",AmountType= "+ amountType +rs.getBigDecimal("SUM_AMT"));
					}
					*/
				if(amtUnposted!=null && amtUnposted.compareTo(new BigDecimal(0))!=0){
					nbrAcctUnposted = account.get_nbr_ownaccount();
				}
				//SUP_1031--Starts
			}else if(type.equalsIgnoreCase(ExecutionConstants.ACCRUAL)){

				if(amountType.equalsIgnoreCase(ExecutionConstants.CREDIT_INTEREST)){
					amtUnposted=(BigDecimal)_hmCR_INT_UNACCRUEDMap.get(account.get_nbr_ownaccount());
					_hmCR_INT_UNACCRUEDMap.remove(account.get_nbr_ownaccount());//SUP_1031
				}
				else if(amountType.equalsIgnoreCase(ExecutionConstants.DEBIT_INTEREST)){
					amtUnposted=(BigDecimal)_hmDR_INT_UNACCRUEDMap.get(account.get_nbr_ownaccount());
					_hmDR_INT_UNACCRUEDMap.remove(account.get_nbr_ownaccount());//SUP_1031
				}
				else if(amountType.equalsIgnoreCase(ExecutionConstants.ALLOCATIONCR)){
					amtUnposted=(BigDecimal)_hmALLOCATIONCR_UNACCRUEDMap.get(account.get_nbr_ownaccount());
					_hmALLOCATIONCR_UNACCRUEDMap.remove(account.get_nbr_ownaccount());//SUP_1031
				}
//SUP_1031--Starts
				else if(amountType.equalsIgnoreCase(ExecutionConstants.ALLOCATIONDR)){
					amtUnposted=(BigDecimal)_hmALLOCATIONDR_UNACCRUEDMap.get(account.get_nbr_ownaccount());
					_hmALLOCATIONDR_UNACCRUEDMap.remove(account.get_nbr_ownaccount());
				}
//SUP_1031--Starts
				else if(amountType.equalsIgnoreCase(ExecutionConstants.NET_INTEREST)){
					amtUnposted=(BigDecimal)_hmNET_INT_UNACCRUEDMap.get(account.get_nbr_ownaccount());
					_hmNET_INT_UNACCRUEDMap.remove(account.get_nbr_ownaccount());//SUP_1031
				}
				else{
					logger.fatal("No Unaccrued amount for account" +account.get_nbr_ownaccount());
				}
				if (logger.isDebugEnabled())
					logger.debug("Unaccued amount :"+amtUnposted);

				if(amtUnposted!=null && amtUnposted.compareTo(new BigDecimal(0))!=0){
					nbrAcctUnposted = account.get_nbr_ownaccount();
				}

			}
			if(amtUnposted==null){
				return null;
			}
			//SUP_1031--Ends
//			INT_1039--Ends
			if (logger.isDebugEnabled()) {
					logger.debug("pstmtAccountUnposted.setDate(1,new java.sql.Date(businessdate.getTime())"+ businessdate);
					logger.debug("pstmtAccountUnposted.setString(2,nbrAcctUnposted)" + nbrAcctUnposted);
					logger.debug("pstmtAccountUnposted.setString(3,amountType" + amountType);
					logger.debug("pstmtAccountUnposted.setString(4,type)" + type);
				}
				pstmtAccountUnposted.setDate(1,LMSUtility.truncSqlDate(new java.sql.Date(businessdate.getTime())));//setting use date
				pstmtAccountUnposted.setString(2,nbrAcctUnposted);
				pstmtAccountUnposted.setString(3,amountType);
				pstmtAccountUnposted.setString(4,type);
				pstmtAccountUnposted.addBatch();
				//_hmCR_INT_UNPOSTEDMap.put(account.get_nbr_ownaccount(),amtUnposted);
				if (ExecutionConstants.ACCRUAL.equals(type)){
					accrualEntries[ExecutionConstants.UNPOSTED_AMOUNT_UPDATE] = true;
				}else{
					postingEntries[ExecutionConstants.UNPOSTED_AMOUNT_UPDATE] = true;
				}


		}
		/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//		catch(Exception e){
		catch(SQLException e){
		/** ****** FindBugs Fix End ***********  on 14DEC09*/
			logger.fatal(
				  "Exception setting values into OPOOL_ACCOUNT_UNPOSTED ", e);
			  throw new LMSException("101302", e.toString());
		}finally{
			/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//			LMSConnectionUtility.close(rs);
			/** ****** FindBugs Fix End ***********  on 14DEC09*/
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			LMSConnectionUtility.close(stmt);
//			LMSConnectionUtility.close(conn);
			/** ****** JTEST FIX end **************  on 14DEC09*/
		}//CLOSE CONNECTIN IN FINALLY
		return amtUnposted;
	}
		private boolean[] generatePostingEntries(
				PreparedStatement pstmtAccountingEntry,
				PreparedStatement pstmtTxnPoolPartyDtls,PreparedStatement pstmtTxnPoolAllocation,
				PreparedStatement pstmtAccountUnposted,	PreparedStatement pstmtAcctUnpostedInsert,
				String generateFor,String generateForId,Date businessdate, String client )throws LMSException, SQLException{
//J-Test OOP.HIF-3
			boolean postingEntries1[] = {false,false,false,false,false};

			/** ****** JTEST FIX start ************  on 14DEC09*/
//			SimpleDateFormat formatter = new SimpleDateFormat(LMSConstants.DATE_FORMAT);
			SimpleDateFormat formatter = new SimpleDateFormat(LMSConstants.DATE_FORMAT, Locale.US);
			/** ****** JTEST FIX end **************  on 14DEC09*/
			String strbusinessdate = formatter.format(businessdate);

			String dateFormat = LMSConstants.DATE_FORMAT;
			String sqlSelectAccnts;
			String sqlString;
			String sqlGroupAccnts;
			StringBuilder sbufWhereAccnts = new StringBuilder("");
			//StringBuffer sbufUnpostedAmtInsr = new StringBuffer("");//INT_1039
//INT_1017--Starts
			String sqlSelectAccntsSuddenEnd;
			String sqlGroupAccntsSuddenEnd;
			//String sqlGroupAccntsSuddenClose;//SUP_1078
			StringBuilder sbufWhereAccntsSuddenEnd = new StringBuilder("");
			//String sqlSelectAccntsSuddenClose;//SUP_1078
			//StringBuffer sbufWhereAccntsSuddenClose = new StringBuffer("");//SUP_1078
//INT_1017--Ends
			String accountingEntryId = null;
			String poolId = null;

			/** ****** JTEST FIX start ************  on 14DEC09*/
//			Statement stmt = null;
//			Statement stmtCal = null;//SUP_1078
//			Statement stmtNonWorkingDay = null;//SUP_1078
			
			PreparedStatement pStmt = null;
			PreparedStatement pStmtCal = null;//SUP_1078
			PreparedStatement pStmtNonWorkingDay = null;//SUP_1078
			/** ****** JTEST FIX end **************  on 14DEC09*/
			ResultSet rs=null;
			Connection conn = null;
			AccountingDO acctDoObj = null;
			boolean b_flgSeperate = true;
			BigDecimal amtToPostCredit = null;
			BigDecimal amountToPostDebit= null;
			BigDecimal amountToPost = null;
			Date postingDate = null;
			// INT_1055 Start
			Date valueDate = null;
			// INT_1055 End

			// Get the relevant Accounts for accrural
			sqlSelectAccnts = (new StringBuffer("SELECT ")
					.append("   PARTY.ID_POOL,")
					.append("   TXN.NBR_PROCESSID,") // Mashreq_AE_Hook
					.append("   PARTY.PARTICIPANT_ID,")//INT_1017
					.append("   COD_PARTY_CCY, ")
					.append("   NBR_COREACCOUNT, ")
					.append("   COD_BRANCH,")
					.append("   PARTY.COD_GL, ")
					.append("   COD_BNK_LGL,")
					.append("   NBR_IBANACCNT, ")
					//SUP_1045 START
					//.append("   SUM(PARTY.AMT_CR_INTEREST) SUM_CR_INT, ")
					//.append("   SUM(PARTY.AMT_DR_INTEREST) SUM_DR_INT, ")
					.append(" SUM((CASE WHEN (  TXN.TYP_ACCTINT_DRACT = 'POST' OR ")
					.append(" (TXN.AMT_POOL_INTCALC_BALANCE < 0 AND TXN.TYP_ACCTINT_DRACT = 'SUPPDR') OR ")
					.append(" (TXN.AMT_POOL_INTCALC_BALANCE >= 0 AND TXN.TYP_ACCTINT_DRACT = 'SUPPCR')) ")//INT_1027 changed TYP_ACCTINT_CRACT to TYP_ACCTINT_DRACT
					.append(" THEN PARTY.AMT_DR_INTEREST ELSE 0 END )) SUM_DR_INT, ")
					.append(" SUM((CASE WHEN (  TXN.TYP_ACCTINT_CRACT = 'POST' OR ")
					.append(" (TXN.AMT_POOL_INTCALC_BALANCE < 0 AND TXN.TYP_ACCTINT_CRACT = 'SUPPDR') OR ")
					.append(" (TXN.AMT_POOL_INTCALC_BALANCE >= 0 AND    TXN.TYP_ACCTINT_CRACT = 'SUPPCR')) ")
					.append(" THEN PARTY.AMT_CR_INTEREST ELSE 0 END )) SUM_CR_INT,")
					//SUP_1045 END
					// INT_1055 Start
					.append("   DAT_POSTING, DAT_POST_SETTLEMENT,TXN.DAT_POST_CREDITDELAY CREDITDELAY,")
					// INT_1055 End
					.append("   TXN.DAT_POST_DEBITDELAY DEBITDELAY,")
					.append("   PARTY.FLG_DEF_DRCR_CONSOL CONSOLIDATE,")
					.append("   NVL(PARTY.AMT_MIN_CR,0) AMT_MIN_CR,")//INT_1027 Added NVL
					.append("   NVL(PARTY.AMT_MIN_DR,0) AMT_MIN_DR ")//INT_1027 Added NVL
					.append(" FROM OPOOL_TXNPOOL_PARTYDTLS PARTY,")
					.append("	 OPOOL_TXNPOOLHDR TXN  ")).toString();//INT_1017

			sbufWhereAccnts.append(" WHERE PARTY.ID_POOL = TXN.ID_POOL ")
							//.append(" AND  TXN.DAT_POST_SETTLEMENT = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')") // SUP_1073: Commented
							//.append(" AND PRTCPNT.ID_POOL = TXN.ID_POOL ")//INT_1017
							.append(" AND  TXN.DAT_POSTING = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')") // SUP_1073: Added
							//.append(" AND PRTCPNT.PARTICIPANT_ID=PARTY.PARTICIPANT_ID ")//INT_1017
							/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
							//SUP_HSBC_072 Starts
							// No need to check for the end date for the whole settlement period
							// The ones whose end date is maintained and are ended on that day are already posted, so their status won't be in 'ACCRUED'
							// So those won't be selected.
							//.append(" AND  (PARTY.DAT_END IS NULL OR  PARTY.DAT_END > TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "'))")//INT_1017
							//SUP_HSBC_072 Ends
							/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
							//.append(" AND  (PRTCPNT.DAT_END IS NULL OR  PRTCPNT.DAT_END > TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "'))")//INT_1017
							.append(" AND PARTY.PARTICIPANT_ID IN (SELECT PRCPNT.PARTICIPANT_ID FROM OPOOL_POOLPARTICIPANT PRCPNT WHERE PRCPNT.ID_POOL = TXN.ID_POOL AND PRCPNT.DAT_END IS NULL OR PRCPNT.DAT_END > TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "'))")//INT_1017
							//.append(" AND	   TXN.COD_GL =  '").append(client)//INT_1009
							.append(" AND	   TXN.NBR_PROCESSID = PARTY.NBR_PROCESSID   ")
							.append(" AND	   TXN.DAT_BUSINESS_POOLRUN = PARTY.DAT_BUSINESS_POOLRUN  ")
							.append(" AND	   TXN.TXT_STATUS = 'ACCRUED' ")
							//.append(" AND	   (ID_ACCRUAL IS NOT NULL) ")//SUP_1045
							//SUP_1045 START
							.append(" AND (TYP_POSTING  = 'R' ) AND TXN.FLG_REALPOOL = 'Y' ")//COR TYP_POSTING  = 'I' donotINSERT INTO ACCOUNTINGENTRY TABLE
							.append(" AND TXN.COD_COUNTER_ACCOUNT = 'B' ") //DO ACCRUAL ONLY FOR BANK ACCOUNT
							//SUP_1045 END
							.append(" AND	 PARTY.TYP_PARTICIPANT = 'ACCOUNT'  ")//INT_1017
							.append(" AND	 PARTY.DAT_BUSINESS_POOLRUN BETWEEN  NVL(TXN.DAT_LASTPOSTINGCALC,TXN.DAT_START-1)+1  AND  TXN.DAT_ADJUSTEDPOSTING  ")
							.append(" AND	 PARTY.ID_BVT = (SELECT MAX(TO_NUMBER(M.ID_BVT))   ")//INT_1026
							.append("		 			  			FROM 	 OPOOL_TXNPOOLHDR M  ")
							.append("								WHERE M.ID_POOL = TXN.ID_POOL   ")
							.append("								AND M.COD_GL = TXN.COD_GL ")
							.append("								AND M.DAT_BUSINESS_POOLRUN = TXN.DAT_BUSINESS_POOLRUN 								")
							.append("					 )").toString();

			if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
				sbufWhereAccnts.append(" AND TXN.ID_POOL IN (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR LOT ");
				/** ****** JTEST FIX start ************  on 14DEC09*/
//				sbufWhereAccnts.append(" WHERE LOT.NBR_LOTID =" + generateForId+")");
				sbufWhereAccnts.append(" WHERE LOT.NBR_LOTID = ?)");
				/** ****** JTEST FIX end **************  on 14DEC09*/
			}
			sqlGroupAccnts = new StringBuffer(" GROUP BY ")
										.append("   PARTY.ID_POOL,")
										.append("   TXN.NBR_PROCESSID,") // Mashreq_AE_Hook
										.append("   PARTY.PARTICIPANT_ID, ")//INT_1017
										.append("   COD_PARTY_CCY, ")
										.append("   NBR_COREACCOUNT, ")
										.append("   COD_BRANCH,")
										.append("   PARTY.COD_GL,")
										.append("   COD_BNK_LGL,")
										.append("   NBR_IBANACCNT, ")
										// INT_1055 Start
										.append("   DAT_POSTING, ")
										// INT_1055 End
										.append("   DAT_POST_SETTLEMENT,TXN.DAT_POST_CREDITDELAY ,")
										.append("   TXN.DAT_POST_DEBITDELAY ,")
										.append("   PARTY.FLG_DEF_DRCR_CONSOL,")
										.append("   PARTY.AMT_MIN_CR,")
										.append("   PARTY.AMT_MIN_DR ").toString();
			//SUP_1078--Starts
//INT_1017--Starts
			/*
			sqlSelectAccntsSuddenEnd = (new StringBuffer("SELECT ")
					.append("   PARTY.ID_POOL,")
					.append("   PARTICIPANT_ID,")
					.append("   COD_PARTY_CCY, ")
					.append("   NBR_COREACCOUNT, ")
					.append("   COD_BRANCH,")
					.append("   PARTY.COD_GL, ")
					.append("   COD_BNK_LGL,")
					.append("   NBR_IBANACCNT, ")
					.append(" SUM((CASE WHEN (  TXN.TYP_ACCTINT_DRACT = 'POST' OR ")
					.append(" (TXN.AMT_POOL_INTCALC_BALANCE < 0 AND TXN.TYP_ACCTINT_DRACT = 'SUPPDR') OR ")
					.append(" (TXN.AMT_POOL_INTCALC_BALANCE >= 0 AND TXN.TYP_ACCTINT_DRACT = 'SUPPCR')) ")
					.append(" THEN PARTY.AMT_DR_INTEREST ELSE 0 END )) SUM_DR_INT, ")
					.append(" SUM((CASE WHEN (  TXN.TYP_ACCTINT_CRACT = 'POST' OR ")
					.append(" (TXN.AMT_POOL_INTCALC_BALANCE < 0 AND TXN.TYP_ACCTINT_CRACT = 'SUPPDR') OR ")
					.append(" (TXN.AMT_POOL_INTCALC_BALANCE >= 0 AND    TXN.TYP_ACCTINT_CRACT = 'SUPPCR')) ")
					.append(" THEN PARTY.AMT_CR_INTEREST ELSE 0 END )) SUM_CR_INT,")
					.append("   PARTY.DAT_END DAT_POSTING,PARTY.DAT_END DAT_POST_SETTLEMENT,PARTY.DAT_END CREDITDELAY,")//INT-1017
					.append("   PARTY.DAT_END DEBITDELAY,")//INT_1017
					.append("   PARTY.FLG_DEF_DRCR_CONSOL CONSOLIDATE,")
					.append("   NVL(PARTY.AMT_MIN_CR,0) AMT_MIN_CR,")
					.append("   NVL(PARTY.AMT_MIN_DR,0) AMT_MIN_DR ")
					.append(" FROM OPOOL_TXNPOOL_PARTYDTLS PARTY,")
					.append("	 OPOOL_TXNPOOLHDR TXN  ")).toString();

			sbufWhereAccntsSuddenEnd.append(" WHERE PARTY.ID_POOL = TXN.ID_POOL ")
					.append(" AND  PARTY.DAT_END = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')")
					.append(" AND	   TXN.NBR_PROCESSID = PARTY.NBR_PROCESSID   ")
					.append(" AND	   TXN.DAT_BUSINESS_POOLRUN = PARTY.DAT_BUSINESS_POOLRUN  ")
					.append(" AND	   TXN.TXT_STATUS <> 'POSTED' ")
					.append(" AND (TYP_POSTING  = 'R' ) AND TXN.FLG_REALPOOL = 'Y' ")
					.append(" AND TXN.COD_COUNTER_ACCOUNT = 'B' ")
					.append(" AND	 TYP_PARTICIPANT = 'ACCOUNT'  ")
					.append(" AND	 PARTY.DAT_BUSINESS_POOLRUN BETWEEN  NVL(TXN.DAT_LASTPOSTINGCALC,TXN.DAT_START-1)+1  AND  TXN.DAT_ADJUSTEDPOSTING  ")
					.append(" AND	 PARTY.ID_BVT = (SELECT MAX(TO_NUMBER(M.ID_BVT))   ")
					.append("		 			  			FROM 	 OPOOL_TXNPOOLHDR M  ")
					.append("								WHERE M.ID_POOL = TXN.ID_POOL   ")
					.append("								AND M.COD_GL = TXN.COD_GL ")
					.append("								AND M.DAT_BUSINESS_POOLRUN = TXN.DAT_BUSINESS_POOLRUN 								")
					.append("					 )").toString();

					if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
						sbufWhereAccntsSuddenEnd.append(" AND TXN.ID_POOL IN (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR LOT ");
						sbufWhereAccntsSuddenEnd.append(" WHERE LOT.NBR_LOTID =" + generateForId+")");
					}

			sqlGroupAccntsSuddenEnd = new StringBuffer(" GROUP BY ")
					.append("   PARTY.ID_POOL,")
					.append("   PARTY.PARTICIPANT_ID, ")
					.append("   COD_PARTY_CCY, ")
					.append("   NBR_COREACCOUNT, ")
					.append("   COD_BRANCH,")
					.append("   PARTY.COD_GL,")
					.append("   COD_BNK_LGL,")
					.append("   NBR_IBANACCNT, ")
					.append("   PARTY.DAT_END, ")
					//.append("   DAT_POST_SETTLEMENT,TXN.DAT_POST_CREDITDELAY ,")//INT_1017
					//.append("   TXN.DAT_POST_DEBITDELAY ,")//INT-1017
					.append("   PARTY.FLG_DEF_DRCR_CONSOL,")
					.append("   PARTY.AMT_MIN_CR,")
					.append("   PARTY.AMT_MIN_DR ").toString();
			*/
			//Takes care of all those accounts which are being closed
			/*
			sqlSelectAccntsSuddenClose = (new StringBuffer("SELECT ")
					.append("   PARTY.ID_POOL,")
					.append("   PARTY.PARTICIPANT_ID,")
					.append("   COD_PARTY_CCY, ")
					.append("   NBR_COREACCOUNT, ")
					.append("   COD_BRANCH,")
					.append("   PARTY.COD_GL, ")
					.append("   COD_BNK_LGL,")
					.append("   NBR_IBANACCNT, ")
					.append(" SUM((CASE WHEN (  TXN.TYP_ACCTINT_DRACT = 'POST' OR ")
					.append(" (TXN.AMT_POOL_INTCALC_BALANCE < 0 AND TXN.TYP_ACCTINT_DRACT = 'SUPPDR') OR ")
					.append(" (TXN.AMT_POOL_INTCALC_BALANCE >= 0 AND TXN.TYP_ACCTINT_DRACT = 'SUPPCR')) ")
					.append(" THEN PARTY.AMT_DR_INTEREST ELSE 0 END )) SUM_DR_INT, ")
					.append(" SUM((CASE WHEN (  TXN.TYP_ACCTINT_CRACT = 'POST' OR ")
					.append(" (TXN.AMT_POOL_INTCALC_BALANCE < 0 AND TXN.TYP_ACCTINT_CRACT = 'SUPPDR') OR ")
					.append(" (TXN.AMT_POOL_INTCALC_BALANCE >= 0 AND    TXN.TYP_ACCTINT_CRACT = 'SUPPCR')) ")
					.append(" THEN PARTY.AMT_CR_INTEREST ELSE 0 END )) SUM_CR_INT,")
					.append("   PRCIPANT.DAT_END DAT_POSTING,(PRCIPANT.DAT_END + 1) DAT_POST_SETTLEMENT,(PRCIPANT.DAT_END + 1) CREDITDELAY,")
					.append("   (PRCIPANT.DAT_END + 1) DEBITDELAY,")
					.append("   PARTY.FLG_DEF_DRCR_CONSOL CONSOLIDATE,")
					.append("   NVL(PARTY.AMT_MIN_CR,0) AMT_MIN_CR,")
					.append("   NVL(PARTY.AMT_MIN_DR,0) AMT_MIN_DR ")
					.append(" FROM OPOOL_TXNPOOL_PARTYDTLS PARTY,")
					.append("	 OPOOL_TXNPOOLHDR TXN, OPOOL_POOLPARTICIPANT PRCIPANT  ")).toString();
//			Takes care of all those accounts which are being closed
			sbufWhereAccntsSuddenClose.append(" WHERE PARTY.ID_POOL = TXN.ID_POOL AND TXN.ID_POOL = PRCIPANT.ID_POOL ")
					.append(" AND PARTY.PARTICIPANT_ID=PRCIPANT.PARTICIPANT_ID ")
					.append(" AND  TXN.DAT_POSTING > TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')")
					.append(" AND  PRCIPANT.DAT_END = (TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')-1) ")
					.append(" AND PRCIPANT.FLG_ACCOUNTCLS = 'Y' ")
					.append(" AND	   TXN.NBR_PROCESSID = PARTY.NBR_PROCESSID   ")
					.append(" AND	   TXN.DAT_BUSINESS_POOLRUN = PARTY.DAT_BUSINESS_POOLRUN  ")
					.append(" AND	   TXN.TXT_STATUS <> 'POSTED' ")
					.append(" AND (TYP_POSTING  = 'R' ) AND TXN.FLG_REALPOOL = 'Y' ")
					.append(" AND TXN.COD_COUNTER_ACCOUNT = 'B' ")
					.append(" AND	 PARTY.TYP_PARTICIPANT = 'ACCOUNT'  ")
					.append(" AND	 PARTY.DAT_BUSINESS_POOLRUN BETWEEN  NVL(TXN.DAT_LASTPOSTINGCALC,TXN.DAT_START-1)+1  AND  TXN.DAT_ADJUSTEDPOSTING  ")
					.append(" AND	 PARTY.ID_BVT = (SELECT MAX(TO_NUMBER(M.ID_BVT))   ")
					.append("		 			  			FROM 	 OPOOL_TXNPOOLHDR M  ")
					.append("								WHERE M.ID_POOL = TXN.ID_POOL   ")
					.append("								AND M.COD_GL = TXN.COD_GL ")
					.append("								AND M.DAT_BUSINESS_POOLRUN = TXN.DAT_BUSINESS_POOLRUN 								")
					.append("					 )").toString();

					if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
						sbufWhereAccntsSuddenClose.append(" AND TXN.ID_POOL IN (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR LOT ");
						sbufWhereAccntsSuddenClose.append(" WHERE LOT.NBR_LOTID =" + generateForId+")");
					}

			sqlGroupAccntsSuddenClose = new StringBuffer(" GROUP BY ")
					.append("   PARTY.ID_POOL,")
					.append("   PARTY.PARTICIPANT_ID, ")
					.append("   COD_PARTY_CCY, ")
					.append("   NBR_COREACCOUNT, ")
					.append("   COD_BRANCH,")
					.append("   PARTY.COD_GL,")
					.append("   COD_BNK_LGL,")
					.append("   NBR_IBANACCNT, ")
					.append("   PRCIPANT.DAT_END, ")
					//.append("   DAT_POST_SETTLEMENT,TXN.DAT_POST_CREDITDELAY ,")
					//.append("   TXN.DAT_POST_DEBITDELAY ,")
					.append("   PARTY.FLG_DEF_DRCR_CONSOL,")
					.append("   PARTY.AMT_MIN_CR,")
					.append("   PARTY.AMT_MIN_DR ").toString();
			*/
//INT_1017--Ends
			//Combine the subParts of the query
			sqlString = new StringBuffer(sqlSelectAccnts+
					sbufWhereAccnts.toString() + sqlGroupAccnts).toString(); //SUP_1017
					//+" UNION "+sqlSelectAccntsSuddenEnd//INT_1017
					//+sbufWhereAccntsSuddenEnd.toString()+sqlGroupAccntsSuddenEnd.toString()
					//+" UNION "+sqlSelectAccntsSuddenClose+sbufWhereAccntsSuddenClose.toString()+sqlGroupAccntsSuddenClose).toString();//INT_1017
//SUP_1078--Ends
			if (logger.isDebugEnabled())
				logger.debug("Query PARTICIPANT POSTING" + sqlString);

			try{
				conn = LMSConnectionUtility.getConnection();
				/** ****** JTEST FIX start ************  on 14DEC09*/
//				stmt = conn.createStatement();
//				stmtNonWorkingDay = conn.createStatement();//SUP_1078
//				stmtCal = conn.createStatement();//SUP_1078
//				rs = stmt.executeQuery(sqlString);
				pStmt = conn.prepareStatement(sqlString);
				if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
					pStmt.setString(1, generateForId);
				}
				rs = pStmt.executeQuery();
				/** ****** JTEST FIX end **************  on 14DEC09*/
				if (logger.isDebugEnabled()) {
					logger.debug(" Executed Query PARTICIPANT POSTING ");
				}

				HashMap hmFields = new HashMap();
				hmFields.put(ExecutionConstants.BUSINESSDATE, businessdate); // Mashreq_AE_Hook fix
				BigDecimal amtToPostCreditP = new BigDecimal(0);
				BigDecimal amountToPostDebitP = new BigDecimal(0);
				BigDecimal minAmtCredit = new BigDecimal(0);
				BigDecimal minAmtDebit = new BigDecimal(0);
				BigDecimal bd_amtNotPosted = new BigDecimal(0);
				BigDecimal minAmtNet = new BigDecimal(0);
				boolean results = false;
				boolean combinedEntry = false;
				while(rs.next())
					{
						results = true;
						acctDoObj = new AccountingDO();
						String flg_def_drcr_consol = rs.getString("CONSOLIDATE");
						b_flgSeperate = true;
						accountingEntryId = "0";
						poolId = "";
						combinedEntry = false;
						amtToPostCreditP = rs.getBigDecimal("SUM_CR_INT");

						amountToPostDebitP = rs.getBigDecimal("SUM_DR_INT");

						poolId = rs.getString("ID_POOL");
						hmFields.put(ExecutionConstants.PROCESSID, rs.getLong("NBR_PROCESSID")); // Mashreq_AE_Hook
						if (logger.isDebugEnabled()) {
							logger.debug("Resultset flg_def_drcr_consol" + flg_def_drcr_consol);
							logger.debug("Resultset POOLID" + poolId);
						}
						if("Y".equalsIgnoreCase(flg_def_drcr_consol!=null
																	?flg_def_drcr_consol.trim()
																	:"" )){
							//Get the NetInterest,if consolidate
							amountToPost = amtToPostCreditP.subtract(amountToPostDebitP);
							// Set the seperate posting flag to False
							//If we are consolidating then we need to check if we have both Dr and Cr Interest and post them
							//CR CUST/DR CUST  depending on which is greater and divide it into 3 legs
							//and need to pass crAmt and DrAMt
							//DR PAYMENTS
							//CR RECE
							if(amtToPostCreditP.compareTo(zero)!=0 && amountToPostDebitP.compareTo(zero)!=0){
								combinedEntry = true;
								//if both debit and credit interest are present
							}
							b_flgSeperate = false;
						}
						if (logger.isDebugEnabled()) {
							logger.debug(" Flag Separate" + b_flgSeperate);
						}
						//fill in the AccountingDo object's attributes.
						acctDoObj.set_cod_bnk_lgl(rs.getString("COD_BNK_LGL"));
						acctDoObj.set_cod_branch(rs.getString("COD_BRANCH"));
						acctDoObj.set_cod_gl(rs.getString("COD_GL"));
						acctDoObj.set_cod_ccy(rs.getString("COD_PARTY_CCY"));
						acctDoObj.set_nbr_coreaccount(rs.getString("NBR_COREACCOUNT"));
						acctDoObj.set_nbr_ibanacct(rs.getString("NBR_IBANACCNT"));
						acctDoObj.set_nbr_ownaccount(rs.getString("PARTICIPANT_ID"));

						// INT_1055 Start
						hmFields.put("DAT_VALUE",(Date)rs.getDate("DAT_POSTING"));
						// INT_1055 End
						hmFields.put("DAT_POST_SETTLEMENT",(Date)rs.getDate("DAT_POST_SETTLEMENT"));
						hmFields.put("DAT_POST_CREDITDELAY",(Date)rs.getDate("CREDITDELAY"));
						hmFields.put("DAT_POST_DEBITDELAY",(Date)rs.getDate("DEBITDELAY"));

						minAmtCredit = rs.getBigDecimal("AMT_MIN_CR");
						minAmtDebit = rs.getBigDecimal("AMT_MIN_DR");

						if (logger.isDebugEnabled()) {
							// INT_1055 Start
							logger.debug("Value Date: " +hmFields.get("DAT_VALUE"));
							// INT_1055 End
							logger.debug("(Date)hmFields.get(DAT_POST_DEBITDELAY" +hmFields.get("DAT_POST_DEBITDELAY"));
							logger.debug("(Date)hmFields.get(DAT_POST_CREDITDELAY" +hmFields.get("DAT_POST_CREDITDELAY"));
							logger.debug("(Date)hmFields.get(DAT_POST_SETTLEMENT" +hmFields.get("DAT_POST_SETTLEMENT"));
							logger.debug("minAmtCredit" + minAmtCredit);
							logger.debug("minAmtDebit" + minAmtDebit);
						}
						// check is any amount is left due to be posted.
						bd_amtNotPosted = new BigDecimal(0);

						// INT_1055 Start
						valueDate = (Date)hmFields.get("DAT_VALUE");
						valueDate = (valueDate == null ? businessdate : addOffsetToDate(valueDate, 1));
						hmFields.put("DAT_SENT", valueDate);
						// INT_1055 End

						if(b_flgSeperate){
							//POSTING CREDIT INTEREST, only if greater than minmCreditInterest
							//chk the order
							//1. add UnPostedAmount
							//2. !=0
							//3. > than mimnAmtInterest
							//then post
							bd_amtNotPosted = setUnpostedAmounts(ExecutionConstants.CREDIT_INTEREST,acctDoObj,
									pstmtAccountUnposted,businessdate,ExecutionConstants.POSTING);
							if (logger.isDebugEnabled()) {
								logger.debug("Unposted Amount "  + bd_amtNotPosted +"for ExecutionConstants.CREDIT_INTEREST");
							}
							//chk if any amount is left to be posted
							if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
								amtToPostCreditP = amtToPostCreditP.add(bd_amtNotPosted);
								if (logger.isDebugEnabled()) {
									logger.debug("Adding Unposted Amount  "  + bd_amtNotPosted +"for ExecutionConstants.CREDIT_INTEREST "+amtToPostCreditP);
								}
							}// UNPOSTED AMOUNT
							//CHECK FOR MINIMUM AMT INTEREST
							if (amtToPostCreditP.compareTo(zero) != 0){
								if(amtToPostCreditP.compareTo(minAmtCredit)>=0){
									postingDate = (Date)hmFields.get("DAT_POST_CREDITDELAY");
									postingDate = postingDate!=null?postingDate:businessdate;
									hmFields.put("DAT_POSTING",postingDate);
									//BVT_TXN Inc Starts
									// INT_1055 Start
									// hmFields.put("DAT_SENT",businessdate);
									// INT_1055 End
									//BVT_TXN Inc Ends
									accountingEntryId = "" + processEntry(acctDoObj,
										amtToPostCreditP,ExecutionConstants.POSTING,
										ExecutionConstants.CREDIT,
										ExecutionConstants.CREDIT_INTEREST,pstmtAccountingEntry,
										/** ****** JTEST FIX start ************  on 14DEC09*/
//										pstmtAcctUnpostedInsert,poolId,hmFields);//INT_1039
										pstmtAcctUnpostedInsert,poolId,hmFields, conn);//INT_1039
										/** ****** JTEST FIX end **************  on 14DEC09*/
								}else{
									addToAcctUnpostedTbl(pstmtAcctUnpostedInsert,acctDoObj.get_nbr_ownaccount(),
										amtToPostCreditP,ExecutionConstants.POSTING,ExecutionConstants.CREDIT_INTEREST,"INTEREST LESS THAN MINIMUM AMOUNT INTEREST",poolId);//INT_1039
								}//minCreditInterest
							}else{
								logger.fatal("Credit Interest is 0 for"  + acctDoObj + "SO not posting the amount ");
							}

						//POSTING DEBIT INTEREST
							bd_amtNotPosted = new BigDecimal(0);//RESETTING AMOUNT NOT POSTED
							bd_amtNotPosted = setUnpostedAmounts(ExecutionConstants.DEBIT_INTEREST,acctDoObj,
							pstmtAccountUnposted,businessdate,ExecutionConstants.POSTING);
							if (logger.isDebugEnabled()) {
								logger.debug("Unposted Amount "  + bd_amtNotPosted +"for ExecutionConstants.DEBIT_INTEREST");
							}
							//chk if any amount is left to be posted
							if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
								amountToPostDebitP = amountToPostDebitP.add(bd_amtNotPosted);
								if (logger.isDebugEnabled()) {
									logger.debug("Adding Unposted Amount  "  + bd_amtNotPosted +"for ExecutionConstants.DEBIT_INTEREST "+amountToPostDebitP);
								}
							}// UNPOSTED DEBIT AMOUNT
							//check with minm debit amount

							if (amountToPostDebitP.compareTo(zero)!=0){
								if(amountToPostDebitP.compareTo(minAmtDebit)>=0){
									postingDate = (Date)hmFields.get("DAT_POST_DEBITDELAY");
									postingDate = postingDate!=null?postingDate:businessdate;
									hmFields.put("DAT_POSTING",postingDate);
									//BVT_TXN Inc Starts
									// INT_1055 Start
									// hmFields.put("DAT_SENT",businessdate);
									// INT_1055 End
									//BVT_TXN Inc Ends
									accountingEntryId = accountingEntryId + "|" + processEntry(acctDoObj,
										amountToPostDebitP,ExecutionConstants.POSTING,
										ExecutionConstants.DEBIT,
										ExecutionConstants.DEBIT_INTEREST,
										pstmtAccountingEntry,pstmtAcctUnpostedInsert,
										/** ****** JTEST FIX start ************  on 14DEC09*/
//										poolId,hmFields);//INT_1039
										poolId,hmFields, conn);//INT_1039
										/** ****** JTEST FIX end **************  on 14DEC09*/
								}else{
									addToAcctUnpostedTbl(pstmtAcctUnpostedInsert,acctDoObj.get_nbr_ownaccount(),
											amountToPostDebitP,ExecutionConstants.POSTING,ExecutionConstants.DEBIT_INTEREST,"INTEREST LESS THAN MINIMUM AMOUNT INTEREST",poolId);//INT_1039
								}//minDebitInterest
							}else{
								logger.fatal("Debit Interest is 0 for"  + acctDoObj + "SO not posting the amount ");
							}

						}else{
							//call processEntry for the NetAmount --consolidated

							bd_amtNotPosted = setUnpostedAmounts(ExecutionConstants.NET_INTEREST,acctDoObj,
														pstmtAccountUnposted,businessdate,ExecutionConstants.POSTING);
							if (logger.isDebugEnabled()) {
							  logger.debug("Unposted Amount "  + bd_amtNotPosted +"for ExecutionConstants.NET_INTEREST");
							 }
							 //chk if any amount is left to be posted
							if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
								amountToPost = amountToPost.add(bd_amtNotPosted);
								if (logger.isDebugEnabled()) {
									logger.debug("Adding Unposted Amount  "  + bd_amtNotPosted +"for ExecutionConstants.NET_INTEREST "+amountToPost);
								 }
							 }// UNPOSTED NET INTEREST
							//	CHECK IF THE NET_INTEREST IS CREDIT(+ve) or DEBIT(-ve)
							//	if (CREDIT)
							if (amountToPost.compareTo(zero) > 0){
								postingDate = (Date)hmFields.get("DAT_POST_CREDITDELAY");
								minAmtNet =  minAmtCredit;
							}else if (amountToPost.compareTo(zero) < 0){
								postingDate = (Date)hmFields.get("DAT_POST_DEBITDELAY");
								minAmtNet = minAmtDebit;
							}else{
								logger.fatal("Net Interest is 0 for"  + acctDoObj + "SO not posting the amount ");
							}
							postingDate = postingDate!=null?postingDate:businessdate;
							hmFields.put("DAT_POSTING",postingDate);
							//BVT_TXN Inc Starts
							// INT_1055 Start
							// hmFields.put("DAT_SENT",businessdate);
							// INT_1055 End
							//BVT_TXN Inc Ends
							//CONSIDERING MINM aMOUNT INTEREST
							if((amountToPost).abs().compareTo(minAmtNet)>=0){//Changed for absolute of NET_INTEREST while posting
								if(combinedEntry){
									if (logger.isDebugEnabled()) {
										logger.debug("Calling combinedEntry ");
									}
									//IF bd_amtNotPosted IS CR ADD IT TO CREDIT INTEREST AMT ELSE DEBIT INTEREST AMT
									if (bd_amtNotPosted.compareTo(zero) > 0){
										amtToPostCreditP = amtToPostCreditP.add(bd_amtNotPosted);
									}else if(bd_amtNotPosted.compareTo(zero) < 0){
										amountToPostDebitP = amountToPostDebitP.add(bd_amtNotPosted);

									accountingEntryId = "" + processCombinedEntry(acctDoObj,
																	amtToPostCreditP,amountToPostDebitP,
																	ExecutionConstants.POSTING,
																	ExecutionConstants.NET,
																	ExecutionConstants.NET_INTEREST,
																	pstmtAccountingEntry,pstmtAcctUnpostedInsert,
																	/** ****** JTEST FIX start ************  on 14DEC09*/
//																	poolId,hmFields);//INT_1039
																	poolId,hmFields, conn);//INT_1039
																	/** ****** JTEST FIX end **************  on 14DEC09*/
									}
								}else{
									accountingEntryId = "" + processEntry(acctDoObj,
																	amountToPost,ExecutionConstants.POSTING,
																	ExecutionConstants.NET,
																	ExecutionConstants.NET_INTEREST,
																	pstmtAccountingEntry,pstmtAcctUnpostedInsert,
																	/** ****** JTEST FIX start ************  on 14DEC09*/
//																	poolId,hmFields);//INT_1039
																	poolId,hmFields, conn);//INT_1039
																	/** ****** JTEST FIX end **************  on 14DEC09*/
								}//combinedEntry
							}else{
								addToAcctUnpostedTbl(pstmtAcctUnpostedInsert,acctDoObj.get_nbr_ownaccount(),
									amountToPost,ExecutionConstants.POSTING,ExecutionConstants.NET_INTEREST,"INTEREST LESS THAN MINIMUM AMOUNT INTEREST",poolId);	//INT_1039
							}//minNetInterest
						}//flgSeparate
						if (logger.isDebugEnabled()) {
								logger.debug("After processEntry accountingEntryId"  + accountingEntryId);
						}
						//Update the Prepared Statements for OPOOL_PARTYDETAILS
						updateInterest(pstmtTxnPoolPartyDtls,accountingEntryId,poolId,acctDoObj.get_nbr_ownaccount(),ExecutionConstants.POSTING);
						//Add to the Pool List
						//updatePoolList(poolId, hmPoolList);
						//postingEntries[ExecutionConstants.OLM_ACCOUNTINGENTRY_INSERT] = true;
						//postingEntries[ExecutionConstants.PARTY_DTLS_UPDATE] = true;

					}//end of while resultset
					if (!results){
						logger.fatal(" No records for Posting -participant interest" + sqlString);
					}
					LMSConnectionUtility.close(rs); //15.1.1_Resource_Leaks_Fix
					LMSConnectionUtility.close(pStmt); //15.1.1_Resource_Leaks_Fix
					//SUP_1078--Starts
					//For participants ending today
					sqlSelectAccntsSuddenEnd = (new StringBuffer("SELECT ")
							.append("   PARTY.ID_POOL,")
							.append("   TXN.NBR_PROCESSID,") // Mashreq_AE_Hook
							.append("   PARTICIPANT_ID,")
							.append("   COD_PARTY_CCY, ")
							.append("   NBR_COREACCOUNT, ")
							.append("   COD_BRANCH,")
							.append("   PARTY.COD_GL, ")
							.append("   COD_BNK_LGL,")
							.append("   NBR_IBANACCNT, ")
							.append(" SUM((CASE WHEN (  TXN.TYP_ACCTINT_DRACT = 'POST' OR ")
							.append(" (TXN.AMT_POOL_INTCALC_BALANCE < 0 AND TXN.TYP_ACCTINT_DRACT = 'SUPPDR') OR ")
							.append(" (TXN.AMT_POOL_INTCALC_BALANCE >= 0 AND TXN.TYP_ACCTINT_DRACT = 'SUPPCR')) ")
							.append(" THEN PARTY.AMT_DR_INTEREST ELSE 0 END )) SUM_DR_INT, ")
							.append(" SUM((CASE WHEN (  TXN.TYP_ACCTINT_CRACT = 'POST' OR ")
							.append(" (TXN.AMT_POOL_INTCALC_BALANCE < 0 AND TXN.TYP_ACCTINT_CRACT = 'SUPPDR') OR ")
							.append(" (TXN.AMT_POOL_INTCALC_BALANCE >= 0 AND    TXN.TYP_ACCTINT_CRACT = 'SUPPCR')) ")
							.append(" THEN PARTY.AMT_CR_INTEREST ELSE 0 END )) SUM_CR_INT,")
							.append("   PARTY.DAT_END DAT_POSTING,PARTY.DAT_END DAT_POST_SETTLEMENT,PARTY.DAT_END CREDITDELAY,")//INT-1017
							.append("   PARTY.DAT_END DEBITDELAY,")//INT_1017
							.append("   PARTY.FLG_DEF_DRCR_CONSOL CONSOLIDATE,")
							.append("   NVL(PARTY.AMT_MIN_CR,0) AMT_MIN_CR,")
							.append("   NVL(PARTY.AMT_MIN_DR,0) AMT_MIN_DR ")
							.append(" FROM OPOOL_TXNPOOL_PARTYDTLS PARTY,")
							.append("	 OPOOL_TXNPOOLHDR TXN  ")).toString();

							sbufWhereAccntsSuddenEnd.append(" WHERE PARTY.ID_POOL = TXN.ID_POOL ")
							/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
							/* SUP_HSBC_019 Starts::: checking participant for the same run date. */
//							.append(" AND PARTY.DAT_END = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')")
							.append(" AND PARTY.DAT_END = (select distinct DAT_END from OPOOL_TXNPOOL_PARTYDTLS party_temp " + 
							        " where party_temp.dat_business_poolrun = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "') " +
		 							" and party_temp.dat_end = party_temp.dat_business_poolrun "  +
									" and party_temp.participant_id = PARTY.participant_id 	) ")
							.append(" AND TXN.NBR_PROCESSID = PARTY.NBR_PROCESSID AND TXN.DAT_BUSINESS_POOLRUN = PARTY.DAT_BUSINESS_POOLRUN")
			 				/* SUP_HSBC_019 ENDS...*/
							/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
							.append(" AND	   TXN.NBR_PROCESSID = PARTY.NBR_PROCESSID   ")
							.append(" AND	   TXN.DAT_BUSINESS_POOLRUN = PARTY.DAT_BUSINESS_POOLRUN  ")
							.append(" AND	   TXN.TXT_STATUS <> 'POSTED' ")
							.append(" AND (TYP_POSTING  = 'R' ) AND TXN.FLG_REALPOOL = 'Y' ")
							.append(" AND TXN.COD_COUNTER_ACCOUNT = 'B' ")
							.append(" AND	 TYP_PARTICIPANT = 'ACCOUNT'  ")
							/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
							// ENH_1110 Starts
							// When an account is added in a same/different pool which is ended previously in 
							// the same/different pool, system is posting the cumulative previous participation interest/allocation amout
							//.append(" AND	 PARTY.DAT_BUSINESS_POOLRUN BETWEEN  NVL(TXN.DAT_LASTPOSTINGCALC,TXN.DAT_START-1)+1  AND  TXN.DAT_ADJUSTEDPOSTING  ")
							.append(" AND PARTY.DAT_BUSINESS_POOLRUN BETWEEN (SELECT MAX(dat_start) from opool_txnpool_partydtls ")
        						.append(" WHERE ID_POOL= party.id_pool  ")
                					.append(" AND PARTICIPANT_ID = party.participant_id ")
                         				.append(" AND TYP_PARTICIPANT= 'ACCOUNT') ")
                          				.append(" AND  TXN.DAT_ADJUSTEDPOSTING ")
                         				 // ENH_1110 Ends
							/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
							.append(" AND	 PARTY.ID_BVT = (SELECT MAX(TO_NUMBER(M.ID_BVT))   ")
							.append("		 			  			FROM 	 OPOOL_TXNPOOLHDR M  ")
							.append("								WHERE M.ID_POOL = TXN.ID_POOL   ")
							.append("								AND M.COD_GL = TXN.COD_GL ")
							.append("								AND M.DAT_BUSINESS_POOLRUN = TXN.DAT_BUSINESS_POOLRUN 								")
							.append("					 )").toString();

							if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
								sbufWhereAccntsSuddenEnd.append(" AND TXN.ID_POOL IN (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR LOT ");
								/** ****** JTEST FIX start ************  on 14DEC09*/
//								sbufWhereAccntsSuddenEnd.append(" WHERE LOT.NBR_LOTID =" + generateForId+")");
								sbufWhereAccntsSuddenEnd.append(" WHERE LOT.NBR_LOTID = ? )");
								/** ****** JTEST FIX end **************  on 14DEC09*/
							}

					sqlGroupAccntsSuddenEnd = new StringBuffer(" GROUP BY ")
							.append("   PARTY.ID_POOL,")
							.append("   TXN.NBR_PROCESSID,") // Mashreq_AE_Hook
							.append("   PARTY.PARTICIPANT_ID, ")
							.append("   COD_PARTY_CCY, ")
							.append("   NBR_COREACCOUNT, ")
							.append("   COD_BRANCH,")
							.append("   PARTY.COD_GL,")
							.append("   COD_BNK_LGL,")
							.append("   NBR_IBANACCNT, ")
							.append("   PARTY.DAT_END, ")
							//.append("   DAT_POST_SETTLEMENT,TXN.DAT_POST_CREDITDELAY ,")//INT_1017
							//.append("   TXN.DAT_POST_DEBITDELAY ,")//INT-1017
							.append("   PARTY.FLG_DEF_DRCR_CONSOL,")
							.append("   PARTY.AMT_MIN_CR,")
							.append("   PARTY.AMT_MIN_DR ").toString();
					
				String postingEndingParticipants=sqlSelectAccntsSuddenEnd +sbufWhereAccntsSuddenEnd.toString()
													+ sqlGroupAccntsSuddenEnd;
				
				if (logger.isDebugEnabled())
					logger.debug("Query PARTICIPANT POSTING FOR PARTICIPANT END" + postingEndingParticipants);
				
				/** ****** JTEST FIX start ************  on 14DEC09*/
//				rs=stmt.executeQuery(postingEndingParticipants);
				
				pStmt = conn.prepareStatement(postingEndingParticipants);
				if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
					pStmt.setString(1, generateForId);
				}
				
				rs = pStmt.executeQuery();				
				/** ****** JTEST FIX end **************  on 14DEC09*/
				String sqlQuery="";
				ResultSet rSet=null;
				ResultSet resultSet=null;
				String calendar="";
				boolean isHoliday;
				String sqlQueryForOption="";
				String Option="";
				while(rs.next())
				{
					acctDoObj = new AccountingDO();
					/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
					//ENH_1110 Starts
					acctDoObj.setExpiringToday(true);
					//ENH_1110 Ends
					/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
					String flg_def_drcr_consol = rs.getString("CONSOLIDATE");
					b_flgSeperate = true;
					accountingEntryId = "0";
					poolId = "";
					combinedEntry = false;
					amtToPostCreditP = rs.getBigDecimal("SUM_CR_INT");

					amountToPostDebitP = rs.getBigDecimal("SUM_DR_INT");

					poolId = rs.getString("ID_POOL");
					hmFields.put(ExecutionConstants.PROCESSID, rs.getLong("NBR_PROCESSID")); // Mashreq_AE_Hook
					if (logger.isDebugEnabled()) {
						logger.debug("Resultset flg_def_drcr_consol" + flg_def_drcr_consol);
						logger.debug("Resultset POOLID" + poolId);
					}
					if("Y".equalsIgnoreCase(flg_def_drcr_consol!=null
																?flg_def_drcr_consol.trim()
																:"" )){
						//Get the NetInterest,if consolidate
						amountToPost = amtToPostCreditP.subtract(amountToPostDebitP);
						// Set the seperate posting flag to False
						//If we are consolidating then we need to check if we have both Dr and Cr Interest and post them
						//CR CUST/DR CUST  depending on which is greater and divide it into 3 legs
						//and need to pass crAmt and DrAMt
						//DR PAYMENTS
						//CR RECE
						if(amtToPostCreditP.compareTo(zero)!=0 && amountToPostDebitP.compareTo(zero)!=0){
							combinedEntry = true;
							//if both debit and credit interest are present
						}
						b_flgSeperate = false;
					}
					if (logger.isDebugEnabled()) {
						logger.debug(" Flag Separate" + b_flgSeperate);
					}
					//fill in the AccountingDo object's attributes.
					acctDoObj.set_cod_bnk_lgl(rs.getString("COD_BNK_LGL"));
					acctDoObj.set_cod_branch(rs.getString("COD_BRANCH"));
					acctDoObj.set_cod_gl(rs.getString("COD_GL"));
					acctDoObj.set_cod_ccy(rs.getString("COD_PARTY_CCY"));
					acctDoObj.set_nbr_coreaccount(rs.getString("NBR_COREACCOUNT"));
					acctDoObj.set_nbr_ibanacct(rs.getString("NBR_IBANACCNT"));
					acctDoObj.set_nbr_ownaccount(rs.getString("PARTICIPANT_ID"));

					// INT_1055 Start
					hmFields.put("DAT_VALUE",(Date)rs.getDate("DAT_POSTING"));
					// INT_1055 End
					hmFields.put("DAT_POST_SETTLEMENT",(Date)rs.getDate("DAT_POST_SETTLEMENT"));
					hmFields.put("DAT_POST_CREDITDELAY",(Date)rs.getDate("CREDITDELAY"));
					hmFields.put("DAT_POST_DEBITDELAY",(Date)rs.getDate("DEBITDELAY"));

					minAmtCredit = rs.getBigDecimal("AMT_MIN_CR");
					minAmtDebit = rs.getBigDecimal("AMT_MIN_DR");

					if (logger.isDebugEnabled()) {
						// INT_1055 Start
						logger.debug("Value Date: " +hmFields.get("DAT_VALUE"));
						// INT_1055 End
						logger.debug("(Date)hmFields.get(DAT_POST_DEBITDELAY" +hmFields.get("DAT_POST_DEBITDELAY"));
						logger.debug("(Date)hmFields.get(DAT_POST_CREDITDELAY" +hmFields.get("DAT_POST_CREDITDELAY"));
						logger.debug("(Date)hmFields.get(DAT_POST_SETTLEMENT" +hmFields.get("DAT_POST_SETTLEMENT"));
						logger.debug("minAmtCredit" + minAmtCredit);
						logger.debug("minAmtDebit" + minAmtDebit);
					}
					// check is any amount is left due to be posted.
					bd_amtNotPosted = new BigDecimal(0);

					// INT_1055 Start
					valueDate = (Date)hmFields.get("DAT_VALUE");
					valueDate = (valueDate == null ? businessdate : addOffsetToDate(valueDate, 1));
					hmFields.put("DAT_SENT", valueDate);
					// INT_1055 End

					if(b_flgSeperate){
						//POSTING CREDIT INTEREST, only if greater than minmCreditInterest
						//chk the order
						//1. add UnPostedAmount
						//2. !=0
						//3. > than mimnAmtInterest
						//then post
						bd_amtNotPosted = setUnpostedAmounts(ExecutionConstants.CREDIT_INTEREST,acctDoObj,
								pstmtAccountUnposted,businessdate,ExecutionConstants.POSTING);
						if (logger.isDebugEnabled()) {
							logger.debug("Unposted Amount "  + bd_amtNotPosted +"for ExecutionConstants.CREDIT_INTEREST");
						}
						//chk if any amount is left to be posted
						if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
							amtToPostCreditP = amtToPostCreditP.add(bd_amtNotPosted);
							if (logger.isDebugEnabled()) {
								logger.debug("Adding Unposted Amount  "  + bd_amtNotPosted +"for ExecutionConstants.CREDIT_INTEREST "+amtToPostCreditP);
							}
						}// UNPOSTED AMOUNT
						//CHECK FOR MINIMUM AMT INTEREST
						if (amtToPostCreditP.compareTo(zero) != 0){
							if(amtToPostCreditP.compareTo(minAmtCredit)>=0){
								postingDate = (Date)hmFields.get("DAT_POST_CREDITDELAY");
								postingDate = postingDate!=null?postingDate:businessdate;
								
								//ENH_LIQ_0005 Starts
								//sqlQuery="SELECT COD_CALENDAR FROM ORBICASH_GLMST WHERE COD_GL = (SELECT COD_GL FROM ORBICASH_ACCOUNTS WHERE NBR_OWNACCOUNT ='"+rs.getString("PARTICIPANT_ID")+"')";
								/** ****** JTEST FIX start ************  on 14DEC09*/
//								sqlQuery="SELECT COD_CALENDAR FROM OLM_GLMST WHERE COD_GL = (SELECT COD_GL FROM ORBICASH_ACCOUNTS WHERE NBR_OWNACCOUNT ='"+rs.getString("PARTICIPANT_ID")+"')";
//								//ENH_LIQ_0005 Ends
//								rSet=stmtCal.executeQuery(sqlQuery);
								
								sqlQuery="SELECT COD_CALENDAR FROM OLM_GLMST WHERE COD_GL = (SELECT COD_GL FROM ORBICASH_ACCOUNTS WHERE NBR_OWNACCOUNT = ?)";
								//ENH_LIQ_0005 Ends
								pStmtCal = conn.prepareStatement(sqlQuery);
								pStmtCal.setString(1, rs.getString("PARTICIPANT_ID"));
								rSet = pStmtCal.executeQuery();
								/** ****** JTEST FIX end **************  on 14DEC09*/
								if(rSet.next()){
									calendar=rSet.getString("COD_CALENDAR");
									CalendarService cal=CalendarService.getCalendarService();
									isHoliday=cal.isDateHoliday(calendar,postingDate);
									if(isHoliday){
										sqlQueryForOption="SELECT COD_NON_WORKING_DAY FROM ORATE_ACCRUAL_CYCLE WHERE COD_ACCR_CYCLE = " +
										/** ****** JTEST FIX start ************  on 14DEC09*/
//										"(SELECT COD_ACCR_CYCLE FROM OPOOL_TXNPOOLHDR WHERE ID_POOL ='"+rs.getString("ID_POOL")+"' " +
//										"AND DAT_BUSINESS_POOLRUN =TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "'))";
//										resultSet=stmtNonWorkingDay.executeQuery(sqlQueryForOption);
						
										"(SELECT COD_ACCR_CYCLE FROM OPOOL_TXNPOOLHDR WHERE ID_POOL = ? AND DAT_BUSINESS_POOLRUN =?)";
										pStmtNonWorkingDay = conn.prepareStatement(sqlQueryForOption);
										pStmtNonWorkingDay.setString(1, rs.getString("ID_POOL"));
										pStmtNonWorkingDay.setDate(2,LMSUtility.truncSqlDate( new java.sql.Date(businessdate.getTime())));
										resultSet=pStmtNonWorkingDay.executeQuery();
										/** ****** JTEST FIX end **************  on 14DEC09*/		
										if(resultSet.next()){
											Option=resultSet.getString("COD_NON_WORKING_DAY");
											if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
												logger.debug("Non Working Day option :"+Option);
											if(Option.equalsIgnoreCase("NEXT")){
												postingDate=cal.getNextWorkingDay(calendar,postingDate);
											}else if(Option.equalsIgnoreCase("PREV")){
												postingDate=cal.getPrevWorkingDay(calendar,postingDate);
											}
										}
									}
								}
								LMSConnectionUtility.close(resultSet);
								LMSConnectionUtility.close(rSet);
								if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
									logger.debug("Calculated Posting Date after Non Working Day :"+postingDate);
								hmFields.put("DAT_POSTING",postingDate);
								hmFields.put("DAT_POST_CREDITDELAY",postingDate);
								hmFields.put("DAT_POST_DEBITDELAY",postingDate);
								hmFields.put("DAT_POST_SETTLEMENT",postingDate);
								//BVT_TXN Inc Starts
								// INT_1055 Start
								// hmFields.put("DAT_SENT",businessdate);
								// INT_1055 End
								//BVT_TXN Inc Ends
								accountingEntryId = "" + processEntry(acctDoObj,
									amtToPostCreditP,ExecutionConstants.POSTING,
									ExecutionConstants.CREDIT,
									ExecutionConstants.CREDIT_INTEREST,pstmtAccountingEntry,
									/** ****** JTEST FIX start ************  on 14DEC09*/
//									pstmtAcctUnpostedInsert,poolId,hmFields);//INT_1039
									pstmtAcctUnpostedInsert,poolId,hmFields, conn);//INT_1039
									/** ****** JTEST FIX end **************  on 14DEC09*/
							}else{
								addToAcctUnpostedTbl(pstmtAcctUnpostedInsert,acctDoObj.get_nbr_ownaccount(),
									amtToPostCreditP,ExecutionConstants.POSTING,ExecutionConstants.CREDIT_INTEREST,"INTEREST LESS THAN MINIMUM AMOUNT INTEREST",poolId);//INT_1039
							}//minCreditInterest
						}else{
							logger.fatal("Credit Interest is 0 for"  + acctDoObj + "SO not posting the amount ");
						}

					//POSTING DEBIT INTEREST
						bd_amtNotPosted = new BigDecimal(0);//RESETTING AMOUNT NOT POSTED
						bd_amtNotPosted = setUnpostedAmounts(ExecutionConstants.DEBIT_INTEREST,acctDoObj,
						pstmtAccountUnposted,businessdate,ExecutionConstants.POSTING);
						if (logger.isDebugEnabled()) {
							logger.debug("Unposted Amount "  + bd_amtNotPosted +"for ExecutionConstants.DEBIT_INTEREST");
						}
						//chk if any amount is left to be posted
						if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
							amountToPostDebitP = amountToPostDebitP.add(bd_amtNotPosted);
							if (logger.isDebugEnabled()) {
								logger.debug("Adding Unposted Amount  "  + bd_amtNotPosted +"for ExecutionConstants.DEBIT_INTEREST "+amountToPostDebitP);
							}
						}// UNPOSTED DEBIT AMOUNT
						//check with minm debit amount

						if (amountToPostDebitP.compareTo(zero)!=0){
							if(amountToPostDebitP.compareTo(minAmtDebit)>=0){
								postingDate = (Date)hmFields.get("DAT_POST_DEBITDELAY");
								postingDate = postingDate!=null?postingDate:businessdate;
								
								//ENH_LIQ_0005 Starts
								//sqlQuery="SELECT COD_CALENDAR FROM ORBICASH_GLMST WHERE COD_GL = (SELECT COD_GL FROM ORBICASH_ACCOUNTS WHERE NBR_OWNACCOUNT ='"+rs.getString("PARTICIPANT_ID")+"')";
								/** ****** JTEST FIX start ************  on 14DEC09*/
//								sqlQuery="SELECT COD_CALENDAR FROM OLM_GLMST WHERE COD_GL = (SELECT COD_GL FROM ORBICASH_ACCOUNTS WHERE NBR_OWNACCOUNT ='"+rs.getString("PARTICIPANT_ID")+"')";
//								//ENH_LIQ_0005 Ends
//								rSet=stmtCal.executeQuery(sqlQuery);
								
								sqlQuery="SELECT COD_CALENDAR FROM OLM_GLMST WHERE COD_GL = (SELECT COD_GL FROM ORBICASH_ACCOUNTS WHERE NBR_OWNACCOUNT = ?)";
								//ENH_LIQ_0005 Ends
								LMSConnectionUtility.close(pStmtCal); //15.1.1_Resource_Leaks_Fix
								pStmtCal = conn.prepareStatement(sqlQuery);
								pStmtCal.setString(1, rs.getString("PARTICIPANT_ID"));
								rSet = pStmtCal.executeQuery();
								/** ****** JTEST FIX end **************  on 14DEC09*/
								if(rSet.next()){
									calendar=rSet.getString("COD_CALENDAR");
									CalendarService cal=CalendarService.getCalendarService();
									isHoliday=cal.isDateHoliday(calendar,postingDate);
									if(isHoliday){
										sqlQueryForOption="SELECT COD_NON_WORKING_DAY FROM ORATE_ACCRUAL_CYCLE WHERE COD_ACCR_CYCLE = " +
										/** ****** JTEST FIX start ************  on 14DEC09*/
//										"(SELECT COD_ACCR_CYCLE FROM OPOOL_TXNPOOLHDR WHERE ID_POOL ='"+rs.getString("ID_POOL")+"' " +
//										"AND DAT_BUSINESS_POOLRUN =TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "'))";
//										resultSet=stmtNonWorkingDay.executeQuery(sqlQueryForOption);
						
										"(SELECT COD_ACCR_CYCLE FROM OPOOL_TXNPOOLHDR WHERE ID_POOL = ? AND DAT_BUSINESS_POOLRUN =?)";
										LMSConnectionUtility.close(pStmtNonWorkingDay); //15.1.1_Resource_Leaks_Fix
										pStmtNonWorkingDay = conn.prepareStatement(sqlQueryForOption);
										pStmtNonWorkingDay.setString(1, rs.getString("ID_POOL"));
										pStmtNonWorkingDay.setDate(2,LMSUtility.truncSqlDate( new java.sql.Date(businessdate.getTime())));
										resultSet=pStmtNonWorkingDay.executeQuery();
										/** ****** JTEST FIX end **************  on 14DEC09*/		
										if(resultSet.next()){
											Option=resultSet.getString("COD_NON_WORKING_DAY");
											if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
												logger.debug("Non Working Day option :"+Option);
											if(Option.equalsIgnoreCase("NEXT")){
												postingDate=cal.getNextWorkingDay(calendar,postingDate);
											}else if(Option.equalsIgnoreCase("PREV")){
												postingDate=cal.getPrevWorkingDay(calendar,postingDate);
											}
										}
									}
								}
								LMSConnectionUtility.close(resultSet);
								LMSConnectionUtility.close(rSet);
								if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
									logger.debug("Calculated Posting Date after Non Working Day :"+postingDate);
								hmFields.put("DAT_POSTING",postingDate);
								hmFields.put("DAT_POST_CREDITDELAY",postingDate);
								hmFields.put("DAT_POST_DEBITDELAY",postingDate);
								hmFields.put("DAT_POST_SETTLEMENT",postingDate);
								//BVT_TXN Inc Starts
								// INT_1055 Start
								// hmFields.put("DAT_SENT",businessdate);
								// INT_1055 End
								//BVT_TXN Inc Ends
								accountingEntryId = accountingEntryId + "|" + processEntry(acctDoObj,
									amountToPostDebitP,ExecutionConstants.POSTING,
									ExecutionConstants.DEBIT,
									ExecutionConstants.DEBIT_INTEREST,
									pstmtAccountingEntry,pstmtAcctUnpostedInsert,
									/** ****** JTEST FIX start ************  on 14DEC09*/
//									poolId,hmFields);//INT_1039
									poolId,hmFields, conn);//INT_1039
									/** ****** JTEST FIX end **************  on 14DEC09*/
							}else{
								addToAcctUnpostedTbl(pstmtAcctUnpostedInsert,acctDoObj.get_nbr_ownaccount(),
										amountToPostDebitP,ExecutionConstants.POSTING,ExecutionConstants.DEBIT_INTEREST,"INTEREST LESS THAN MINIMUM AMOUNT INTEREST",poolId);//INT_1039
							}//minDebitInterest
						}else{
							logger.fatal("Debit Interest is 0 for"  + acctDoObj + "SO not posting the amount ");
						}

					}else{
						//call processEntry for the NetAmount --consolidated

						bd_amtNotPosted = setUnpostedAmounts(ExecutionConstants.NET_INTEREST,acctDoObj,
													pstmtAccountUnposted,businessdate,ExecutionConstants.POSTING);
						if (logger.isDebugEnabled()) {
						  logger.debug("Unposted Amount "  + bd_amtNotPosted +"for ExecutionConstants.NET_INTEREST");
						 }
						 //chk if any amount is left to be posted
						if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
							amountToPost = amountToPost.add(bd_amtNotPosted);
							if (logger.isDebugEnabled()) {
								logger.debug("Adding Unposted Amount  "  + bd_amtNotPosted +"for ExecutionConstants.NET_INTEREST "+amountToPost);
							 }
						 }// UNPOSTED NET INTEREST
						//	CHECK IF THE NET_INTEREST IS CREDIT(+ve) or DEBIT(-ve)
						//	if (CREDIT)
						if (amountToPost.compareTo(zero) > 0){
							postingDate = (Date)hmFields.get("DAT_POST_CREDITDELAY");
							minAmtNet =  minAmtCredit;
						}else if (amountToPost.compareTo(zero) < 0){
							postingDate = (Date)hmFields.get("DAT_POST_DEBITDELAY");
							minAmtNet = minAmtDebit;
						}else{
							logger.fatal("Net Interest is 0 for"  + acctDoObj + "SO not posting the amount ");
						}
						postingDate = postingDate!=null?postingDate:businessdate;
						//Non Working Day Adjustments
						
						//ENH_LIQ_0005 Starts
						//sqlQuery="SELECT COD_CALENDAR FROM ORBICASH_GLMST WHERE COD_GL = (SELECT COD_GL FROM ORBICASH_ACCOUNTS WHERE NBR_OWNACCOUNT ='"+rs.getString("PARTICIPANT_ID")+"')";
						/** ****** JTEST FIX start ************  on 14DEC09*/
//						sqlQuery="SELECT COD_CALENDAR FROM OLM_GLMST WHERE COD_GL = (SELECT COD_GL FROM ORBICASH_ACCOUNTS WHERE NBR_OWNACCOUNT ='"+rs.getString("PARTICIPANT_ID")+"')";
//						//ENH_LIQ_0005 Ends
//						rSet=stmtCal.executeQuery(sqlQuery);
						
						sqlQuery="SELECT COD_CALENDAR FROM OLM_GLMST WHERE COD_GL = (SELECT COD_GL FROM ORBICASH_ACCOUNTS WHERE NBR_OWNACCOUNT = ?)";
						//ENH_LIQ_0005 Ends
						pStmtCal = conn.prepareStatement(sqlQuery);
						pStmtCal.setString(1, rs.getString("PARTICIPANT_ID"));
						rSet = pStmtCal.executeQuery();
						/** ****** JTEST FIX end **************  on 14DEC09*/
						if(rSet.next()){
							calendar=rSet.getString("COD_CALENDAR");
							CalendarService cal=CalendarService.getCalendarService();
							isHoliday=cal.isDateHoliday(calendar,postingDate);
							if(isHoliday){
								sqlQueryForOption="SELECT COD_NON_WORKING_DAY FROM ORATE_ACCRUAL_CYCLE WHERE COD_ACCR_CYCLE = " +
								/** ****** JTEST FIX start ************  on 14DEC09*/
//								"(SELECT COD_ACCR_CYCLE FROM OPOOL_TXNPOOLHDR WHERE ID_POOL ='"+rs.getString("ID_POOL")+"' " +
//								"AND DAT_BUSINESS_POOLRUN =TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "'))";
//								resultSet=stmtNonWorkingDay.executeQuery(sqlQueryForOption);
				
								"(SELECT COD_ACCR_CYCLE FROM OPOOL_TXNPOOLHDR WHERE ID_POOL = ? AND DAT_BUSINESS_POOLRUN =?)";
								pStmtNonWorkingDay = conn.prepareStatement(sqlQueryForOption);
								pStmtNonWorkingDay.setString(1, rs.getString("ID_POOL"));
								pStmtNonWorkingDay.setDate(2,LMSUtility.truncSqlDate( new java.sql.Date(businessdate.getTime())));
								resultSet=pStmtNonWorkingDay.executeQuery();
								/** ****** JTEST FIX end **************  on 14DEC09*/		
								if(resultSet.next()){
									Option=resultSet.getString("COD_NON_WORKING_DAY");
									if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
										logger.debug("Non Working Day option :"+Option);
									if(Option.equalsIgnoreCase("NEXT")){
										postingDate=cal.getNextWorkingDay(calendar,postingDate);
									}else if(Option.equalsIgnoreCase("PREV")){
										postingDate=cal.getPrevWorkingDay(calendar,postingDate);
									}
								}
							}
						}
						LMSConnectionUtility.close(resultSet);
						LMSConnectionUtility.close(rSet);
						//End for Non_Working Day Adjustments
						if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
							logger.debug("Calculated Posting Date after Non Working Day :"+postingDate);
						hmFields.put("DAT_POSTING",postingDate);
						hmFields.put("DAT_POST_CREDITDELAY",postingDate);
						hmFields.put("DAT_POST_DEBITDELAY",postingDate);
						hmFields.put("DAT_POST_SETTLEMENT",postingDate);
						//BVT_TXN Inc Starts
						// INT_1055 Start
						// hmFields.put("DAT_SENT",businessdate);
						// INT_1055 End
						//BVT_TXN Inc Ends
						//CONSIDERING MINM aMOUNT INTEREST
						if((amountToPost).abs().compareTo(minAmtNet)>=0){//Changed for absolute of NET_INTEREST while posting
							if(combinedEntry){
								if (logger.isDebugEnabled()) {
									logger.debug("Calling combinedEntry ");
								}
								//IF bd_amtNotPosted IS CR ADD IT TO CREDIT INTEREST AMT ELSE DEBIT INTEREST AMT
								if (bd_amtNotPosted.compareTo(zero) > 0){
									amtToPostCreditP = amtToPostCreditP.add(bd_amtNotPosted);
								}else if(bd_amtNotPosted.compareTo(zero) < 0){
									amountToPostDebitP = amountToPostDebitP.add(bd_amtNotPosted);

								accountingEntryId = "" + processCombinedEntry(acctDoObj,
																amtToPostCreditP,amountToPostDebitP,
																ExecutionConstants.POSTING,
																ExecutionConstants.NET,
																ExecutionConstants.NET_INTEREST,
																pstmtAccountingEntry,pstmtAcctUnpostedInsert,
																/** ****** JTEST FIX start ************  on 14DEC09*/
//																poolId,hmFields);//INT_1039
																poolId,hmFields, conn);//INT_1039
																/** ****** JTEST FIX end **************  on 14DEC09*/
								}
							}else{
								accountingEntryId = "" + processEntry(acctDoObj,
																amountToPost,ExecutionConstants.POSTING,
																ExecutionConstants.NET,
																ExecutionConstants.NET_INTEREST,
																pstmtAccountingEntry,pstmtAcctUnpostedInsert,
																/** ****** JTEST FIX start ************  on 14DEC09*/
//																poolId,hmFields);//INT_1039
																poolId,hmFields, conn);//INT_1039
																/** ****** JTEST FIX end **************  on 14DEC09*/
							}//combinedEntry
						}else{
							addToAcctUnpostedTbl(pstmtAcctUnpostedInsert,acctDoObj.get_nbr_ownaccount(),
								amountToPost,ExecutionConstants.POSTING,ExecutionConstants.NET_INTEREST,"INTEREST LESS THAN MINIMUM AMOUNT INTEREST",poolId);	//INT_1039
						}//minNetInterest
					}//flgSeparate
					if (logger.isDebugEnabled()) {
							logger.debug("After processEntry accountingEntryId"  + accountingEntryId);
					}
					//Update the Prepared Statements for OPOOL_PARTYDETAILS
					updateInterest(pstmtTxnPoolPartyDtls,accountingEntryId,poolId,acctDoObj.get_nbr_ownaccount(),ExecutionConstants.POSTING);
					//Add to the Pool List
					//updatePoolList(poolId, hmPoolList);
					//postingEntries[ExecutionConstants.OLM_ACCOUNTINGENTRY_INSERT] = true;
					//postingEntries[ExecutionConstants.PARTY_DTLS_UPDATE] = true;

				}//end of while resultset for participants end
				//SUP_1078--Ends
				rs.close();
				LMSConnectionUtility.close(pStmt); //15.1.1_Resource_Leaks_Fix
				//FOR POSTING ALLOCATION ENTRIES
				String sqlAllocAccrualDtlsSelect1 = (new StringBuffer("SELECT ")
				//.append(" ID_POOL,FLG_CONSOLIDATION, PARTICIPANT_ID, COD_CCY_ACCOUNT, TYP_SETTLE, ")
				.append(" ID_POOL,FLG_CONSOLIDATION, PARTICIPANT_ID, COD_CCY_ACCOUNT, TYP_SETTLE, NBR_PROCESSID, ") //Mashreq_AE_Hook
				.append(" SUM(EQV_AMT_ALLOCATION) SUM,  ALLOCATOR,  NBR_COREACCOUNT,  ")
				.append(" COD_BRANCH, COD_GL, COD_BNK_LGL,  NBR_IBANACCNT, "))
				//FETCH AMT_MIN_CR,FLG_ALLOCATION
				//.append("   AMT_FOR_ALLOCATION,")
				//FOR CHECKING IF CR/DR AMT WAS USED FOR ALLOCATION AND THEN APPLY THE CREDITDELAY/DEBITDELAY
				// INT_1055 Start
				.append("   DAT_POSTING, DAT_POST_SETTLEMENT, DAT_POST_CREDITDELAY CREDITDELAY,")
				// INT_1055 End
				.append("   DAT_POST_DEBITDELAY DEBITDELAY").toString();

				String sqlAllocAccrualDtlsSelect2 = (new StringBuffer(" SELECT ")
				//.append(" TXN.ID_POOL, TXN.FLG_CONSOLIDATION, PARTY.PARTICIPANT_ID PARTICIPANT_ID, ")
				.append(" TXN.ID_POOL, TXN.FLG_CONSOLIDATION, PARTY.PARTICIPANT_ID PARTICIPANT_ID, TXN.NBR_PROCESSID NBR_PROCESSID, ") // Mashreq_AE_Hook
				.append(" ALLOC.DAT_BUSINESS_POOLRUN, COD_CCY_ACCOUNT, TYP_SETTLE, ")
				.append(" EQV_AMT_ALLOCATION, ALLOCATOR, NARRATION ,NBR_COREACCOUNT, ")
				.append(" COD_BRANCH,PARTY.COD_GL, COD_BNK_LGL, NBR_IBANACCNT,")
				.append(" ALLOC.PARTY_ALLOC_SEQUENCE, ")
				//.append(" ALLOC.AMT_FOR_ALLOCATION, ")
				// INT_1055 Start
				.append(" TXN.DAT_POSTING, DAT_POST_SETTLEMENT,TXN.DAT_POST_CREDITDELAY, ")
				// INT_1055 End
				.append(" TXN.DAT_POST_DEBITDELAY  ")
				.append(" FROM ")
				.append(" OPOOL_TXNPOOL_ALLOCATIONDTLS ALLOC, ")
				.append(" OPOOL_TXNPOOLHDR TXN, ")
				.append(" OPOOL_TXNPOOL_PARTYDTLS PARTY"))//INT_1017
				.toString();

				String sqlAllocAccrualDtlsSelect3 = (new StringBuffer(" SELECT ")
				//.append(" TXN.ID_POOL, TXN.FLG_CONSOLIDATION, PARTY.NBR_OWNACCOUNT PARTICIPANT_ID, ")
				.append(" TXN.ID_POOL, TXN.FLG_CONSOLIDATION, PARTY.NBR_OWNACCOUNT PARTICIPANT_ID, TXN.NBR_PROCESSID NBR_PROCESSID, ") // Mashreq_AE_Hook
				.append(" ALLOC.DAT_BUSINESS_POOLRUN, COD_CCY_ACCOUNT, TYP_SETTLE, ")
				.append(" EQV_AMT_ALLOCATION, ALLOCATOR, NARRATION ,NBR_COREACCOUNT, ")
				.append(" COD_BRANCH,PARTY.COD_GL, COD_BNK_LGL, NBR_IBANACCNT,")
				.append(" ALLOC.PARTY_ALLOC_SEQUENCE, ")
				//.append(" ALLOC.AMT_FOR_ALLOCATION, ")
				// INT_1055 Start
				.append(" TXN.DAT_POSTING, DAT_POST_SETTLEMENT,TXN.DAT_POST_CREDITDELAY, ")
				// INT_1055 End
				.append(" TXN.DAT_POST_DEBITDELAY  ")
				.append(" FROM ")
				.append(" OPOOL_TXNPOOL_ALLOCATIONDTLS ALLOC, ")
				.append(" OPOOL_TXNPOOLHDR TXN, ")
				.append(" ORBICASH_ACCOUNTS PARTY"))
				.toString();

				StringBuilder sqlAllocAccrualDtlsWhere1 = new StringBuilder(" TXN.ID_POOL = ALLOC.ID_POOL AND TXN.ID_POOL = PARTY.ID_POOL ")//INT_1017
				//.append(" AND  TXN.DAT_POST_SETTLEMENT = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')") // SUP_1073: Commented
				.append(" AND  TXN.DAT_POSTING = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')") // SUP_1073: Added
				//.append(" AND PRTCPNT.PARTICIPANT_ID=PARTY.PARTICIPANT_ID ")//INT_1017
				/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
				//SUP_HSBC_072 Starts
				// No need to check for the end date for the whole settlement period
				// The ones whose end date is maintained and are ended on that day are already posted, so their status won't be in 'ACCRUED'
				// So those won't be selected.
				//.append(" AND (PARTY.DAT_END IS NULL OR PARTY.DAT_END > TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "'))")//INT_1017
				//SUP_HSBC_072 Ends

				/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
				//.append(" AND (PRTCPNT.DAT_END IS NULL OR PRTCPNT.DAT_END > TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "'))")//INT_1017
				.append(" AND PARTY.PARTICIPANT_ID IN (SELECT PRCPNT.PARTICIPANT_ID FROM OPOOL_POOLPARTICIPANT PRCPNT WHERE PRCPNT.ID_POOL = TXN.ID_POOL AND PRCPNT.DAT_END IS NULL OR PRCPNT.DAT_END > TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "'))")//INT_1017
				//.append(" AND TXN.COD_GL = '" + client + "'") //INT_1009
				.append(" AND TXN.NBR_PROCESSID = ALLOC.NBR_PROCESSID   ")
				.append(" AND TXN.DAT_BUSINESS_POOLRUN = ALLOC.DAT_BUSINESS_POOLRUN ")
				.append(" AND TXN.NBR_PROCESSID = PARTY.NBR_PROCESSID  ")
				.append(" AND TXN.DAT_BUSINESS_POOLRUN = PARTY.DAT_BUSINESS_POOLRUN ")
				.append(" AND PARTY.ID_POOL = ALLOC.ID_POOL ")
				.append(" AND PARTY.ID_BVT	 = ALLOC.ID_BVT ")
				.append(" AND PARTY.PARTICIPANT_ID = ALLOC.PARTICIPANT_ID      ")
				.append(" AND TXN.TXT_STATUS = 'ACCRUED'  ")
				.append(" AND FLG_REAL = 'Y' ")
				.append(" AND FLG_ALLOCATION <> 'R' AND FLG_ALLOCATION <> 'T'  ")//SUP_1028
				.append(" AND (ALLOC.TYP_PARTICIPANT = 'ACCOUNT' AND PARTY.TYP_PARTICIPANT = 'ACCOUNT' ) ")//SUP_1045
				.append(" AND TXN.FLG_REALPOOL = 'Y'   ")
				.append(" AND  ALLOC.ID_BVT = (SELECT MAX(TO_NUMBER(M.ID_BVT)) ")//INT_1026
				.append(" FROM 	 OPOOL_TXNPOOLHDR M ")
				.append(" WHERE M.ID_POOL = TXN.ID_POOL ")
				.append(" AND M.DAT_BUSINESS_POOLRUN = TXN.DAT_BUSINESS_POOLRUN   ")
				.append(" AND M.COD_GL = TXN.COD_GL) ")
				.append(" AND ALLOC.DAT_BUSINESS_POOLRUN BETWEEN  NVL(TXN.DAT_LASTPOSTINGCALC,TXN.DAT_START-1)+1  AND TXN.DAT_ADJUSTEDPOSTING ");
				/*
				if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
					sqlAllocAccrualDtlsWhere1.append(" AND TXN.NBR_LOTID =" + generateForId);
				}*/

				if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
					sqlAllocAccrualDtlsWhere1.append(" AND TXN.ID_POOL IN (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR LOT ");
					/** ****** JTEST FIX start ************  on 14DEC09*/
//					sqlAllocAccrualDtlsWhere1.append(" WHERE LOT.NBR_LOTID =" + generateForId+")");
					sqlAllocAccrualDtlsWhere1.append(" WHERE LOT.NBR_LOTID = ? )");
					/** ****** JTEST FIX end **************  on 14DEC09*/
				}

				StringBuilder sqlAllocAccrualDtlsWhere2 = new StringBuilder(" TXN.ID_POOL = ALLOC.ID_POOL  ")
				//.append(" AND    TXN.DAT_POST_SETTLEMENT = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')") // SUP_1073: Commented
				.append(" AND    TXN.DAT_POSTING = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')") // SUP_1073: Added
				//.append(" AND	 TXN.COD_GL = '" + client + "'") //INT_1009
				.append(" AND	 TXN.NBR_PROCESSID = ALLOC.NBR_PROCESSID    ")
				.append(" AND	 TXN.DAT_BUSINESS_POOLRUN = ALLOC.DAT_BUSINESS_POOLRUN  ")
				.append(" AND	 PARTY.NBR_OWNACCOUNT = ALLOC.PARTICIPANT_ID       ")
				.append(" AND	 TXN.TXT_STATUS = 'ACCRUED'  ")
				.append(" AND	 FLG_REAL = 'Y'  ")
				.append(" AND 	 FLG_ALLOCATION <> 'R' AND FLG_ALLOCATION <> 'T'  ")//SUP_1028
				.append(" AND	 (ALLOC.TYP_PARTICIPANT = 'CENTRAL' ) ")
				.append(" AND    TXN.FLG_REALPOOL = 'Y'    ")
				.append(" AND    ALLOC.ID_BVT = (SELECT MAX(TO_NUMBER(M.ID_BVT)) ")//INT_1026
				.append(" FROM 	 OPOOL_TXNPOOLHDR M ")
				.append(" WHERE M.ID_POOL = TXN.ID_POOL ")
				.append(" AND M.DAT_BUSINESS_POOLRUN = TXN.DAT_BUSINESS_POOLRUN   ")
				.append(" AND M.COD_GL = TXN.COD_GL) ")
				.append(" AND   ALLOC.DAT_BUSINESS_POOLRUN BETWEEN  NVL(TXN.DAT_LASTPOSTINGCALC,TXN.DAT_START-1)+1  AND TXN.DAT_ADJUSTEDPOSTING ");
				/*
				if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
					sqlAllocAccrualDtlsWhere2.append(" AND TXN.NBR_LOTID =" + generateForId);
				}*/

				if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
					sqlAllocAccrualDtlsWhere2.append(" AND TXN.ID_POOL IN (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR LOT ");
					/** ****** JTEST FIX start ************  on 14DEC09*/
//					sqlAllocAccrualDtlsWhere2.append(" WHERE LOT.NBR_LOTID =" + generateForId+")");
					sqlAllocAccrualDtlsWhere2.append(" WHERE LOT.NBR_LOTID =?)");
					/** ****** JTEST FIX end **************  on 14DEC09*/
				}

				String sqlAllocAccrualDtlsgroupBy1 = (new StringBuffer("ID_POOL,")
				//.append(" FLG_CONSOLIDATION, PARTICIPANT_ID, ")
				.append(" FLG_CONSOLIDATION, PARTICIPANT_ID, NBR_PROCESSID, ") // Mashreq_AE_Hook
				.append(" COD_CCY_ACCOUNT, TYP_SETTLE, ALLOCATOR,  ")
				.append(" NARRATION ,NBR_COREACCOUNT, COD_BRANCH,")
				.append(" COD_GL,COD_BNK_LGL, NBR_IBANACCNT,")
				.append(" PARTY_ALLOC_SEQUENCE, "))
				//.append("   AMT_FOR_ALLOCATION,")
				// INT_1055 Start
				.append("  DAT_POSTING, DAT_POST_SETTLEMENT, DAT_POST_CREDITDELAY,")
				// INT_1055 End
				.append("   DAT_POST_DEBITDELAY ")
				.toString();

				String sqlAllocAccrualDtlsgroupBy2 = (new StringBuffer(" ID_POOL,")
				.append(" FLG_CONSOLIDATION,  PARTICIPANT_ID, ")
				.append(" COD_CCY_ACCOUNT, TYP_SETTLE, ALLOCATOR,  ")
				.append(" NBR_COREACCOUNT, COD_BRANCH,COD_GL,")
				.append(" COD_BNK_LGL, NBR_IBANACCNT,"))
				//.append("   AMT_FOR_ALLOCATION ,")
				// INT_1055 Start
				.append("   DAT_POSTING, DAT_POST_SETTLEMENT,DAT_POST_CREDITDELAY ,")
				// INT_1055 End
				.append("   DAT_POST_DEBITDELAY ")
				.toString();
				//SUP_1078--Starts
//INT_1017--Starts
				/*
				String sqlAllocAccrualSuddenEnd = (new StringBuffer("SELECT ")
						.append(" TXN.ID_POOL, TXN.FLG_CONSOLIDATION, PARTY.PARTICIPANT_ID PARTICIPANT_ID, ")
						.append(" COD_CCY_ACCOUNT, TYP_SETTLE, SUM(EQV_AMT_ALLOCATION) SUM, ALLOCATOR, NBR_COREACCOUNT,  ")
						.append(" COD_BRANCH,PARTY.COD_GL, COD_BNK_LGL, NBR_IBANACCNT,  ")
						.append(" PARTY.DAT_END DAT_POSTING,PARTY.DAT_END DAT_POST_SETTLEMENT, PARTY.DAT_END CREDITDELAY,")
						.append(" PARTY.DAT_END DEBITDELAY ")
						.append(" FROM  OPOOL_TXNPOOL_ALLOCATIONDTLS ALLOC, OPOOL_TXNPOOLHDR TXN, OPOOL_TXNPOOL_PARTYDTLS PARTY")).toString();

				StringBuffer sqlAllocAccrualSuddenEndWhere = new StringBuffer(" TXN.ID_POOL = ALLOC.ID_POOL AND TXN.ID_POOL = PARTY.ID_POOL  ")
				.append(" AND PARTY.DAT_END = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')")
				.append(" AND TXN.NBR_PROCESSID = ALLOC.NBR_PROCESSID   ")
				.append(" AND TXN.DAT_BUSINESS_POOLRUN = ALLOC.DAT_BUSINESS_POOLRUN ")
				.append(" AND TXN.NBR_PROCESSID = PARTY.NBR_PROCESSID  ")
				.append(" AND TXN.DAT_BUSINESS_POOLRUN = PARTY.DAT_BUSINESS_POOLRUN ")
				.append(" AND PARTY.ID_POOL = ALLOC.ID_POOL ")
				.append(" AND PARTY.ID_BVT	 = ALLOC.ID_BVT ")
				.append(" AND PARTY.PARTICIPANT_ID = ALLOC.PARTICIPANT_ID      ")
				.append(" AND TXN.TXT_STATUS <> 'POSTED'  ")
				.append(" AND FLG_REAL = 'Y' ")
				.append(" AND FLG_ALLOCATION <> 'R' AND FLG_ALLOCATION <> 'T'  ")
				.append(" AND (ALLOC.TYP_PARTICIPANT = 'ACCOUNT' AND PARTY.TYP_PARTICIPANT = 'ACCOUNT' ) ")
				.append(" AND TXN.FLG_REALPOOL = 'Y'   ")
				.append(" AND  ALLOC.ID_BVT = (SELECT MAX(TO_NUMBER(M.ID_BVT)) ")
				.append(" FROM 	 OPOOL_TXNPOOLHDR M ")
				.append(" WHERE M.ID_POOL = TXN.ID_POOL ")
				.append(" AND M.DAT_BUSINESS_POOLRUN = TXN.DAT_BUSINESS_POOLRUN   ")
				.append(" AND M.COD_GL = TXN.COD_GL) ")
				.append(" AND ALLOC.DAT_BUSINESS_POOLRUN BETWEEN  NVL(TXN.DAT_LASTPOSTINGCALC,TXN.DAT_START-1)+1  AND TXN.DAT_ADJUSTEDPOSTING ");

				if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
					sqlAllocAccrualSuddenEndWhere.append(" AND TXN.ID_POOL IN (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR LOT ");
					sqlAllocAccrualSuddenEndWhere.append(" WHERE LOT.NBR_LOTID =" + generateForId+")");
				}

				String sqlAllocAccrualSuddenEndgroupBy1 = (new StringBuffer("TXN.ID_POOL,")
				.append(" TXN.FLG_CONSOLIDATION, PARTY.PARTICIPANT_ID, ")
				.append(" COD_CCY_ACCOUNT, TYP_SETTLE, ALLOCATOR,  ")
				.append(" NARRATION ,NBR_COREACCOUNT, COD_BRANCH,")
				.append(" PARTY.COD_GL,COD_BNK_LGL, NBR_IBANACCNT,")
				.append(" PARTY_ALLOC_SEQUENCE, "))
				.append("  PARTY.DAT_END ")
				//.append("  PARTY.DAT_END, DAT_POST_SETTLEMENT, DAT_POST_CREDITDELAY,")
				//.append("  DAT_POST_DEBITDELAY ")
				.toString();
				*/
				/*
				String sqlAllocAccrualSuddenAccountClose = (new StringBuffer("SELECT ")
						.append(" TXN.ID_POOL, TXN.FLG_CONSOLIDATION, PARTY.PARTICIPANT_ID PARTICIPANT_ID, ")
						.append(" COD_CCY_ACCOUNT, TYP_SETTLE, SUM(EQV_AMT_ALLOCATION) SUM, ALLOCATOR, NBR_COREACCOUNT,  ")
						.append(" COD_BRANCH,PARTY.COD_GL, COD_BNK_LGL, NBR_IBANACCNT,  ")
						.append(" PRCIPANT.DAT_END DAT_POSTING,(PRCIPANT.DAT_END + 1) DAT_POST_SETTLEMENT, (PRCIPANT.DAT_END + 1) CREDITDELAY,")
						.append(" (PRCIPANT.DAT_END + 1) DEBITDELAY ")
						.append(" FROM  OPOOL_TXNPOOL_ALLOCATIONDTLS ALLOC, OPOOL_TXNPOOLHDR TXN, OPOOL_TXNPOOL_PARTYDTLS PARTY, OPOOL_POOLPARTICIPANT PRCIPANT ")).toString();

				StringBuffer sqlAllocAccrualSuddenAccountCloseWhere = new StringBuffer(" TXN.ID_POOL = ALLOC.ID_POOL AND TXN.ID_POOL = PARTY.ID_POOL AND TXN.ID_POOL = PRCIPANT.ID_POOL ")
				.append(" AND PARTY.PARTICIPANT_ID=PRCIPANT.PARTICIPANT_ID ")
				.append(" AND PRCIPANT.DAT_END = (TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')-1)")
				.append(" AND PRCIPANT.FLG_ACCOUNTCLS = 'Y' ")
				.append(" AND TXN.NBR_PROCESSID = ALLOC.NBR_PROCESSID   ")
				.append(" AND TXN.DAT_BUSINESS_POOLRUN = ALLOC.DAT_BUSINESS_POOLRUN ")
				.append(" AND TXN.NBR_PROCESSID = PARTY.NBR_PROCESSID  ")
				.append(" AND TXN.DAT_BUSINESS_POOLRUN = PARTY.DAT_BUSINESS_POOLRUN ")
				.append(" AND PARTY.ID_POOL = ALLOC.ID_POOL ")
				.append(" AND PARTY.ID_BVT	 = ALLOC.ID_BVT ")
				.append(" AND PARTY.PARTICIPANT_ID = ALLOC.PARTICIPANT_ID      ")
				.append(" AND TXN.TXT_STATUS <> 'POSTED'  ")
				.append(" AND FLG_REAL = 'Y' ")
				.append(" AND FLG_ALLOCATION <> 'R' AND FLG_ALLOCATION <> 'T'  ")
				.append(" AND (ALLOC.TYP_PARTICIPANT = 'ACCOUNT' AND PARTY.TYP_PARTICIPANT = 'ACCOUNT' ) ")
				.append(" AND TXN.FLG_REALPOOL = 'Y'   ")
				.append(" AND  ALLOC.ID_BVT = (SELECT MAX(TO_NUMBER(M.ID_BVT)) ")
				.append(" FROM 	 OPOOL_TXNPOOLHDR M ")
				.append(" WHERE M.ID_POOL = TXN.ID_POOL ")
				.append(" AND M.DAT_BUSINESS_POOLRUN = TXN.DAT_BUSINESS_POOLRUN   ")
				.append(" AND M.COD_GL = TXN.COD_GL) ")
				.append(" AND ALLOC.DAT_BUSINESS_POOLRUN BETWEEN  NVL(TXN.DAT_LASTPOSTINGCALC,TXN.DAT_START-1)+1  AND TXN.DAT_ADJUSTEDPOSTING ");

				if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
					sqlAllocAccrualSuddenAccountCloseWhere.append(" AND TXN.ID_POOL IN (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR LOT ");
					sqlAllocAccrualSuddenAccountCloseWhere.append(" WHERE LOT.NBR_LOTID =" + generateForId+")");
				}

				String sqlAllocAccrualSuddenClosegroupBy1 = (new StringBuffer("TXN.ID_POOL,")
						.append(" TXN.FLG_CONSOLIDATION, PARTY.PARTICIPANT_ID, ")
						.append(" COD_CCY_ACCOUNT, TYP_SETTLE, ALLOCATOR,  ")
						.append(" NARRATION ,NBR_COREACCOUNT, COD_BRANCH,")
						.append(" PARTY.COD_GL,COD_BNK_LGL, NBR_IBANACCNT,")
						.append(" PARTY_ALLOC_SEQUENCE, "))
						.append("  PRCIPANT.DAT_END ")
						//.append("  PRCIPANT.DAT_END, DAT_POST_SETTLEMENT, DAT_POST_CREDITDELAY,")
						//.append("  DAT_POST_DEBITDELAY ")
						.toString();
				*/
//INT_1017--Ends
				StringBuilder sqlAllocDtls = new StringBuilder(sqlAllocAccrualDtlsSelect1 )
				.append(" FROM ( ")
				.append(sqlAllocAccrualDtlsSelect2)
				.append(" WHERE ")
				.append(sqlAllocAccrualDtlsWhere1.toString())
				.append(" UNION ")
				.append(sqlAllocAccrualDtlsSelect3)
				.append(" WHERE ")
				.append(sqlAllocAccrualDtlsWhere2.toString())
				.append(")V	")
				//INT_1008 START
				//.append(")V	WHERE 	(FLG_CONSOLIDATION = 'N' OR FLG_CONSOLIDATION IS NULL) ")
				.append(" GROUP BY ")
//INT_1017---Starts
				.append(sqlAllocAccrualDtlsgroupBy1);
				//.append(" UNION ")
				//.append(sqlAllocAccrualSuddenEnd)
				//.append(" WHERE ")
				//.append(sqlAllocAccrualSuddenEndWhere.toString())
				//.append(" GROUP BY ")
				//.append(sqlAllocAccrualSuddenEndgroupBy1)
				//.append(" UNION ")
				//.append(sqlAllocAccrualSuddenAccountClose)
				//.append(" WHERE ")
				//.append(sqlAllocAccrualSuddenAccountCloseWhere.toString())
				//.append(" GROUP BY ")
				//.append(sqlAllocAccrualSuddenClosegroupBy1);
//INT_1017--Ends
				/*
				.append(sqlAllocAccrualDtlsSelect1)
				.append(" FROM (")
				.append(sqlAllocAccrualDtlsSelect2)
				.append(" WHERE ")
				.append(sqlAllocAccrualDtlsWhere1.toString())
				.append(" UNION ")
				.append(sqlAllocAccrualDtlsSelect3)
				.append(" WHERE ")
				.append(sqlAllocAccrualDtlsWhere2.toString())
				.append(")V	 WHERE 	FLG_CONSOLIDATION = 'Y'")
				.append(" GROUP BY ")
				.append(sqlAllocAccrualDtlsgroupBy2);
				INT_1008 END
				*/
				//SUP_1078--Ends
				if (logger.isDebugEnabled())
					logger.debug("Query POSTING ALLOCATION: " + sqlAllocDtls);

				/** ****** JTEST FIX start ************  on 14DEC09*/
//				rs = stmt.executeQuery(sqlAllocDtls.toString());
				
				pStmt = conn.prepareStatement(sqlAllocDtls.toString());
				if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
					pStmt.setString(1, generateForId);
					pStmt.setString(2, generateForId);
				}
				rs = pStmt.executeQuery();
				
				/** ****** JTEST FIX end **************  on 14DEC09*/


				if (logger.isDebugEnabled()) {
					logger.debug(" Executed Query  POSTING ALLOCATION ");
				}
				results = false;
				String details = "";
				String extraDetails = "";
				String typSettle="";//SUP_1031
				while(rs.next()){
					results = true;
					acctDoObj = new AccountingDO();
					poolId = "";
					accountingEntryId = "0";
					details = "";
					poolId = rs.getString("ID_POOL");
					hmFields.put(ExecutionConstants.PROCESSID, rs.getLong("NBR_PROCESSID")); // Mashreq_AE_Hook
					amountToPost = new BigDecimal(rs.getString("SUM"));

					//fill in the AccountingDo object's attributes.
					acctDoObj.set_nbr_ownaccount(rs.getString("PARTICIPANT_ID"));
					acctDoObj.set_nbr_coreaccount(rs.getString("NBR_COREACCOUNT"));
					acctDoObj.set_cod_branch(rs.getString("COD_BRANCH"));
					acctDoObj.set_cod_gl(rs.getString("COD_GL"));
					acctDoObj.set_cod_bnk_lgl(rs.getString("COD_BNK_LGL"));
					acctDoObj.set_nbr_ibanacct(rs.getString("NBR_IBANACCNT"));
					acctDoObj.set_cod_ccy(rs.getString("COD_CCY_ACCOUNT"));

					//BigDecimal amtForAllocation = rs.getBigDecimal("SUM");
					//populate Dates
					// INT_1055 Start
					hmFields.put("DAT_VALUE",(Date)rs.getDate("DAT_POSTING"));
					// INT_1055 End
					hmFields.put("DAT_POST_SETTLEMENT",(Date)rs.getDate("DAT_POST_SETTLEMENT"));
					hmFields.put("DAT_POST_CREDITDELAY",(Date)rs.getDate("CREDITDELAY"));
					hmFields.put("DAT_POST_DEBITDELAY",(Date)rs.getDate("DEBITDELAY"));
					hmFields.put("POOL_FLG_CONSOLIDATION",rs.getString("FLG_CONSOLIDATION"));//INT_1008 add FLG_CONSOLIDATION in fieldsHashMap
					//put extraDetails and flagAllocation in fieldsHashMap
					extraDetails = "ALLOCATOR IS " + rs.getString("ALLOCATOR")+ "TYP_SETTLE IS " + rs.getString("TYP_SETTLE");
//SUP_1031--Starts
					//details = "ALLOCATION";
					typSettle=rs.getString("TYP_SETTLE");
					if(typSettle.equalsIgnoreCase("PARTICIPANTCREDIT")||typSettle.equalsIgnoreCase("CENTRALCREDIT")){
						details = ExecutionConstants.ALLOCATIONCR;
					}
					else if(typSettle.equalsIgnoreCase("PARTICIPANTDEBIT")||typSettle.equalsIgnoreCase("CENTRALDEBIT")){
						details = ExecutionConstants.ALLOCATIONDR;
					}
//SUP_1031--Ends
					if (logger.isDebugEnabled()) {
						// INT_1055 Start
						logger.debug("Value Date: " +hmFields.get("DAT_VALUE"));
						// INT_1055 End
						logger.debug("Account Object for Allocation" + acctDoObj);
					}

					bd_amtNotPosted = setUnpostedAmounts(details,acctDoObj,
											pstmtAccountUnposted,businessdate,ExecutionConstants.POSTING);
					if (logger.isDebugEnabled()) {
					  logger.debug("Unposted Amount "  + bd_amtNotPosted +"for ALLOCATION");
					 }
					 //chk if any amount is left to be posted
					if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
						amountToPost = amountToPost.add(bd_amtNotPosted);
						if (logger.isDebugEnabled()) {
							logger.debug("Adding Unposted Amount  "  + bd_amtNotPosted +"for ALLOCATION "+amountToPost);
						 }
					 }// UNPOSTED ALLOCATION
					//based on amountToPost, decide if it is CREDIT/DEBIT DELAY DAYS FOR POSTING DATE
					if (amountToPost.compareTo(zero) != 0){
						if (amountToPost.compareTo(zero) > 0){
							postingDate = (Date)rs.getDate("CREDITDELAY");
						}else{
							postingDate = (Date)rs.getDate("DEBITDELAY");
						}
						postingDate = postingDate!=null?postingDate:businessdate;
						hmFields.put("DAT_POSTING",postingDate);
						//BVT_TXN Inc Starts
						// INT_1055 Start
						// hmFields.put("DAT_SENT",businessdate);
						valueDate = (Date)hmFields.get("DAT_VALUE");
						valueDate = (valueDate == null ? businessdate : addOffsetToDate(valueDate, 1));
						hmFields.put("DAT_SENT", valueDate);
						// INT_1055 End
				//BVT_TXN Inc Ends
						accountingEntryId = "" + processEntry(acctDoObj,
								amountToPost,ExecutionConstants.POSTING,
								ExecutionConstants.NET, details,pstmtAccountingEntry,
								/** ****** JTEST FIX start ************  on 14DEC09*/
//								pstmtAcctUnpostedInsert,poolId,hmFields);//INT_1039
								pstmtAcctUnpostedInsert,poolId,hmFields, conn);//INT_1039
								/** ****** JTEST FIX end **************  on 14DEC09*/
					}else{
						logger.fatal(" Not Posting  Amount as amountTo Post = 0 amount for account " + acctDoObj);
					}
					//update Allocation entries
					//postingEntries[ExecutionConstants.OLM_ACCOUNTINGENTRY_INSERT] = true;
					postingEntries1[ExecutionConstants.ALLOCATION_UPDATE] = true;
				}
				if (!results){
					logger.fatal(" No records for Posting -Allocation" + sqlAllocDtls.toString());
				}
				LMSConnectionUtility.close(pStmt); //15.1.1_Resource_Leaks_Fix
				//SUP_1078--Starts
				//For participants ending today
				String sqlAllocAccrualSuddenEnd = (new StringBuffer("SELECT ")
						//.append(" TXN.ID_POOL, TXN.FLG_CONSOLIDATION, PARTY.PARTICIPANT_ID PARTICIPANT_ID, ")
						.append(" TXN.ID_POOL, TXN.FLG_CONSOLIDATION, PARTY.PARTICIPANT_ID PARTICIPANT_ID, TXN.NBR_PROCESSID NBR_PROCESSID, ") // Mashreq_AE_Hook
						.append(" COD_CCY_ACCOUNT, TYP_SETTLE, SUM(EQV_AMT_ALLOCATION) SUM, ALLOCATOR, NBR_COREACCOUNT,  ")
						.append(" COD_BRANCH,PARTY.COD_GL, COD_BNK_LGL, NBR_IBANACCNT,  ")
						.append(" PARTY.DAT_END DAT_POSTING,PARTY.DAT_END DAT_POST_SETTLEMENT, PARTY.DAT_END CREDITDELAY,")
						.append(" PARTY.DAT_END DEBITDELAY ")
						.append(" FROM  OPOOL_TXNPOOL_ALLOCATIONDTLS ALLOC, OPOOL_TXNPOOLHDR TXN, OPOOL_TXNPOOL_PARTYDTLS PARTY")).toString();

				StringBuilder sqlAllocAccrualSuddenEndWhere = new StringBuilder(" TXN.ID_POOL = ALLOC.ID_POOL AND TXN.ID_POOL = PARTY.ID_POOL  ")
				/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */

				/* SUP_HSBC_019 Starts::: checking participant for the same run date. */
				//.append(" AND PARTY.DAT_END = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')")
				.append(" AND PARTY.DAT_END = (select distinct DAT_END from OPOOL_TXNPOOL_PARTYDTLS party_temp " + 
				        " where party_temp.dat_business_poolrun = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "') " +
		 				" and party_temp.dat_end = party_temp.dat_business_poolrun "  +
						" and party_temp.participant_id = PARTY.participant_id 	) ")
				.append(" AND TXN.NBR_PROCESSID = PARTY.NBR_PROCESSID AND TXN.DAT_BUSINESS_POOLRUN = PARTY.DAT_BUSINESS_POOLRUN")
 				/* SUP_HSBC_019 ENDS...*/
				/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */

				.append(" AND TXN.NBR_PROCESSID = ALLOC.NBR_PROCESSID   ")
				.append(" AND TXN.DAT_BUSINESS_POOLRUN = ALLOC.DAT_BUSINESS_POOLRUN ")
				.append(" AND TXN.NBR_PROCESSID = PARTY.NBR_PROCESSID  ")
				.append(" AND TXN.DAT_BUSINESS_POOLRUN = PARTY.DAT_BUSINESS_POOLRUN ")
				.append(" AND PARTY.ID_POOL = ALLOC.ID_POOL ")
				.append(" AND PARTY.ID_BVT	 = ALLOC.ID_BVT ")
				.append(" AND PARTY.PARTICIPANT_ID = ALLOC.PARTICIPANT_ID      ")
				.append(" AND TXN.TXT_STATUS <> 'POSTED'  ")
				.append(" AND FLG_REAL = 'Y' ")
				.append(" AND FLG_ALLOCATION <> 'R' AND FLG_ALLOCATION <> 'T'  ")
				.append(" AND (ALLOC.TYP_PARTICIPANT = 'ACCOUNT' AND PARTY.TYP_PARTICIPANT = 'ACCOUNT' ) ")
				.append(" AND TXN.FLG_REALPOOL = 'Y'   ")
				.append(" AND  ALLOC.ID_BVT = (SELECT MAX(TO_NUMBER(M.ID_BVT)) ")
				.append(" FROM 	 OPOOL_TXNPOOLHDR M ")
				.append(" WHERE M.ID_POOL = TXN.ID_POOL ")
				.append(" AND M.DAT_BUSINESS_POOLRUN = TXN.DAT_BUSINESS_POOLRUN   ")
				.append(" AND M.COD_GL = TXN.COD_GL) ")
				/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
				// ENH_1110 Starts
				// When an account is added in a same/different pool which is ended previously in 
				// the same/different pool, system is posting the cumulative previous participation interest/allocation amout
				//.append(" AND ALLOC.DAT_BUSINESS_POOLRUN BETWEEN  NVL(TXN.DAT_LASTPOSTINGCALC,TXN.DAT_START-1)+1  AND TXN.DAT_ADJUSTEDPOSTING ");
				.append(" AND alloc.dat_business_poolrun BETWEEN   (SELECT MAX(dat_start) from opool_txnpool_partydtls ")
				.append(" WHERE ID_POOL= party.id_pool  ")
				.append(" AND PARTICIPANT_ID = party.participant_id ")
             		   	.append(" AND TYP_PARTICIPANT= 'ACCOUNT') ")
                		.append(" AND txn.dat_adjustedposting ");
				// ENH_1110 Ends
				/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */

				if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
					sqlAllocAccrualSuddenEndWhere.append(" AND TXN.ID_POOL IN (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR LOT ");
					/** ****** JTEST FIX start ************  on 14DEC09*/
//					sqlAllocAccrualSuddenEndWhere.append(" WHERE LOT.NBR_LOTID =" + generateForId+")");
					sqlAllocAccrualSuddenEndWhere.append(" WHERE LOT.NBR_LOTID = ?)");
					/** ****** JTEST FIX end **************  on 14DEC09*/
				}

				String sqlAllocAccrualSuddenEndgroupBy1 = (new StringBuffer("TXN.ID_POOL,")
				//.append(" TXN.FLG_CONSOLIDATION, PARTY.PARTICIPANT_ID, ")
				.append(" TXN.FLG_CONSOLIDATION, PARTY.PARTICIPANT_ID, TXN.NBR_PROCESSID, ") // Mashreq_AE_Hook
				.append(" COD_CCY_ACCOUNT, TYP_SETTLE, ALLOCATOR,  ")
				.append(" NARRATION ,NBR_COREACCOUNT, COD_BRANCH,")
				.append(" PARTY.COD_GL,COD_BNK_LGL, NBR_IBANACCNT,")
				.append(" PARTY_ALLOC_SEQUENCE, "))
				.append("  PARTY.DAT_END ")
				//.append("  PARTY.DAT_END, DAT_POST_SETTLEMENT, DAT_POST_CREDITDELAY,")
				//.append("  DAT_POST_DEBITDELAY ")
				.toString();
				
				String sqlAllocationSuddenEnd=sqlAllocAccrualSuddenEnd +" WHERE "+ sqlAllocAccrualSuddenEndWhere.toString() 
												+" GROUP BY "+ sqlAllocAccrualSuddenEndgroupBy1;
				
				if (logger.isDebugEnabled())
					logger.debug("Query POSTING ALLOCATION FOR END OF PARTICIPANTS: " + sqlAllocationSuddenEnd);
				/** ****** JTEST FIX start ************  on 14DEC09*/
//				rs=stmt.executeQuery(sqlAllocationSuddenEnd);
				
				pStmt = conn.prepareStatement(sqlAllocationSuddenEnd);
				if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
					pStmt.setString(1, generateForId);
				}
				rs = pStmt.executeQuery();
				/** ****** JTEST FIX end **************  on 14DEC09*/
				while(rs.next()){
					acctDoObj = new AccountingDO();
					/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
					//ENH_1110 Starts
					acctDoObj.setExpiringToday(true);
					//ENH_1110 Ends
					/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
					poolId = "";
					accountingEntryId = "0";
					details = "";
					poolId = rs.getString("ID_POOL");
					hmFields.put(ExecutionConstants.PROCESSID, rs.getLong("NBR_PROCESSID")); // Mashreq_AE_Hook
					amountToPost = new BigDecimal(rs.getString("SUM"));

					//fill in the AccountingDo object's attributes.
					acctDoObj.set_nbr_ownaccount(rs.getString("PARTICIPANT_ID"));
					acctDoObj.set_nbr_coreaccount(rs.getString("NBR_COREACCOUNT"));
					acctDoObj.set_cod_branch(rs.getString("COD_BRANCH"));
					acctDoObj.set_cod_gl(rs.getString("COD_GL"));
					acctDoObj.set_cod_bnk_lgl(rs.getString("COD_BNK_LGL"));
					acctDoObj.set_nbr_ibanacct(rs.getString("NBR_IBANACCNT"));
					acctDoObj.set_cod_ccy(rs.getString("COD_CCY_ACCOUNT"));

					//BigDecimal amtForAllocation = rs.getBigDecimal("SUM");
					//populate Dates
					// INT_1055 Start
					hmFields.put("DAT_VALUE",(Date)rs.getDate("DAT_POSTING"));
					// INT_1055 End
					hmFields.put("DAT_POST_SETTLEMENT",(Date)rs.getDate("DAT_POST_SETTLEMENT"));
					hmFields.put("DAT_POST_CREDITDELAY",(Date)rs.getDate("CREDITDELAY"));
					hmFields.put("DAT_POST_DEBITDELAY",(Date)rs.getDate("DEBITDELAY"));
					hmFields.put("POOL_FLG_CONSOLIDATION",rs.getString("FLG_CONSOLIDATION"));//INT_1008 add FLG_CONSOLIDATION in fieldsHashMap
					//put extraDetails and flagAllocation in fieldsHashMap
					extraDetails = "ALLOCATOR IS " + rs.getString("ALLOCATOR")+ "TYP_SETTLE IS " + rs.getString("TYP_SETTLE");
//SUP_1031--Starts
					//details = "ALLOCATION";
					typSettle=rs.getString("TYP_SETTLE");
					if(typSettle.equalsIgnoreCase("PARTICIPANTCREDIT")||typSettle.equalsIgnoreCase("CENTRALCREDIT")){
						details = ExecutionConstants.ALLOCATIONCR;
					}
					else if(typSettle.equalsIgnoreCase("PARTICIPANTDEBIT")||typSettle.equalsIgnoreCase("CENTRALDEBIT")){
						details = ExecutionConstants.ALLOCATIONDR;
					}
//SUP_1031--Ends
					if (logger.isDebugEnabled()) {
						// INT_1055 Start
						logger.debug("Value Date: " +hmFields.get("DAT_VALUE"));
						// INT_1055 End
						logger.debug("Account Object for Allocation" + acctDoObj);
					}

					bd_amtNotPosted = setUnpostedAmounts(details,acctDoObj,
											pstmtAccountUnposted,businessdate,ExecutionConstants.POSTING);
					if (logger.isDebugEnabled()) {
					  logger.debug("Unposted Amount "  + bd_amtNotPosted +"for ALLOCATION");
					 }
					 //chk if any amount is left to be posted
					if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
						amountToPost = amountToPost.add(bd_amtNotPosted);
						if (logger.isDebugEnabled()) {
							logger.debug("Adding Unposted Amount  "  + bd_amtNotPosted +"for ALLOCATION "+amountToPost);
						 }
					 }// UNPOSTED ALLOCATION
					//based on amountToPost, decide if it is CREDIT/DEBIT DELAY DAYS FOR POSTING DATE
					if (amountToPost.compareTo(zero) != 0){
						if (amountToPost.compareTo(zero) > 0){
							postingDate = (Date)rs.getDate("CREDITDELAY");
						}else{
							postingDate = (Date)rs.getDate("DEBITDELAY");
						}
						postingDate = postingDate!=null?postingDate:businessdate;
						
						//ENH_LIQ_0005 Starts
						//sqlQuery="SELECT COD_CALENDAR FROM ORBICASH_GLMST WHERE COD_GL = (SELECT COD_GL FROM ORBICASH_ACCOUNTS WHERE NBR_OWNACCOUNT ='"+rs.getString("PARTICIPANT_ID")+"')";
						/** ****** JTEST FIX start ************  on 14DEC09*/
//						sqlQuery="SELECT COD_CALENDAR FROM OLM_GLMST WHERE COD_GL = (SELECT COD_GL FROM ORBICASH_ACCOUNTS WHERE NBR_OWNACCOUNT ='"+rs.getString("PARTICIPANT_ID")+"')";
//						//ENH_LIQ_0005 Ends
//						rSet=stmtCal.executeQuery(sqlQuery);
						
						sqlQuery="SELECT COD_CALENDAR FROM OLM_GLMST WHERE COD_GL = (SELECT COD_GL FROM ORBICASH_ACCOUNTS WHERE NBR_OWNACCOUNT = ?)";
						//ENH_LIQ_0005 Ends
						pStmtCal = conn.prepareStatement(sqlQuery);
						pStmtCal.setString(1, rs.getString("PARTICIPANT_ID"));
						rSet = pStmtCal.executeQuery();
						/** ****** JTEST FIX end **************  on 14DEC09*/
						if(rSet.next()){
							calendar=rSet.getString("COD_CALENDAR");
							CalendarService cal=CalendarService.getCalendarService();
							isHoliday=cal.isDateHoliday(calendar,postingDate);
							if(isHoliday){
								sqlQueryForOption="SELECT COD_NON_WORKING_DAY FROM ORATE_ACCRUAL_CYCLE WHERE COD_ACCR_CYCLE = " +
								/** ****** JTEST FIX start ************  on 14DEC09*/
//									"(SELECT COD_ACCR_CYCLE FROM OPOOL_TXNPOOLHDR WHERE ID_POOL ='"+rs.getString("ID_POOL")+"' " +
//									"AND DAT_BUSINESS_POOLRUN =TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "'))";
//									resultSet=stmtNonWorkingDay.executeQuery(sqlQueryForOption);
				
								"(SELECT COD_ACCR_CYCLE FROM OPOOL_TXNPOOLHDR WHERE ID_POOL = ? AND DAT_BUSINESS_POOLRUN =?)";
								pStmtNonWorkingDay = conn.prepareStatement(sqlQueryForOption);
								pStmtNonWorkingDay.setString(1, rs.getString("ID_POOL"));
								pStmtNonWorkingDay.setDate(2,LMSUtility.truncSqlDate( new java.sql.Date(businessdate.getTime())));
								resultSet=pStmtNonWorkingDay.executeQuery();
								/** ****** JTEST FIX end **************  on 14DEC09*/	
								if(resultSet.next()){
									Option=resultSet.getString("COD_NON_WORKING_DAY");
									if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
										logger.debug("Non Working Day option :"+Option);
									if(Option.equalsIgnoreCase("NEXT")){
										postingDate=cal.getNextWorkingDay(calendar,postingDate);
									}else if(Option.equalsIgnoreCase("PREV")){
										postingDate=cal.getPrevWorkingDay(calendar,postingDate);
									}
								}
							}
						}
						LMSConnectionUtility.close(resultSet);
						LMSConnectionUtility.close(rSet);
						// SUP_2034:Issue659 - Start: commented
						//LMSConnectionUtility.close(stmtCal);
						//LMSConnectionUtility.close(stmtNonWorkingDay);
						// SUP_2034:Issue659 - End
						if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
							logger.debug("Calculated Posting Date after Non Working Day :"+postingDate);
						hmFields.put("DAT_POSTING",postingDate);
						hmFields.put("DAT_POST_SETTLEMENT",postingDate);
						hmFields.put("DAT_POST_CREDITDELAY",postingDate);
						hmFields.put("DAT_POST_DEBITDELAY",postingDate);
						//BVT_TXN Inc Starts
						// INT_1055 Start
						// hmFields.put("DAT_SENT",businessdate);
						valueDate = (Date)hmFields.get("DAT_VALUE");
						valueDate = (valueDate == null ? businessdate : addOffsetToDate(valueDate, 1));
						hmFields.put("DAT_SENT", valueDate);
						// INT_1055 End
				//BVT_TXN Inc Ends
						accountingEntryId = "" + processEntry(acctDoObj,
								amountToPost,ExecutionConstants.POSTING,
								ExecutionConstants.NET, details,pstmtAccountingEntry,
								/** ****** JTEST FIX start ************  on 14DEC09*/
//								pstmtAcctUnpostedInsert,poolId,hmFields);//INT_1039
								pstmtAcctUnpostedInsert,poolId,hmFields, conn);//INT_1039
								/** ****** JTEST FIX end **************  on 14DEC09*/
					}else{
						logger.fatal(" Not Posting  Amount as amountTo Post = 0 amount for account " + acctDoObj);
					}
					//update Allocation entries
					//postingEntries[ExecutionConstants.OLM_ACCOUNTINGENTRY_INSERT] = true;
					postingEntries1[ExecutionConstants.ALLOCATION_UPDATE] = true;
				}
				
				//Ending of participants ending
				//SUP_1078--Ends
				rs.close();
				/////////////pool settlement ///////////////////////////////////////////////////
				  StringBuilder sqlSettlementPosting = new StringBuilder("SELECT  ")
				  .append("   PARTY.ID_POOL, ")
				  .append("   PARTICIPANT_ID,")
				  .append("   TXN.NBR_PROCESSID, ") //Mashreq_AE_Hook
				  .append("   flg_def_drcr_consol CONSOLIDATE, COD_PARTY_CCY, ")
				  .append("   SUM( (CASE WHEN ")
				  .append("   		 (  TXN.TYP_ACCTINT_DRACT = 'POST' OR (TXN.AMT_POOL_INTCALC_BALANCE < 0 AND TXN.TYP_ACCTINT_DRACT = 'SUPPDR') OR (TXN.AMT_POOL_INTCALC_BALANCE >= 0 AND  TXN.TYP_ACCTINT_DRACT = 'SUPPCR'))	 ")////INT_1027 changed TYP_ACCTINT_CRACT to TYP_ACCTINT_DRACT
				  .append("	 	THEN PARTY.AMT_DR_INTEREST ELSE 0 END ) ) SUM_DR_INT, ")
				  .append("   SUM( (CASE WHEN (  TXN.TYP_ACCTINT_CRACT = 'POST' OR (TXN.AMT_POOL_INTCALC_BALANCE < 0 AND TXN.TYP_ACCTINT_CRACT = 'SUPPDR') OR (TXN.AMT_POOL_INTCALC_BALANCE >= 0 AND    TXN.TYP_ACCTINT_CRACT = 'SUPPCR')) ")
				  .append("   		 THEN PARTY.AMT_CR_INTEREST ELSE 0 END ) ) SUM_CR_INT,  ")
				  .append("   NBR_COREACCOUNT, ")
				  .append("   COD_BRANCH,")
				  .append("   PARTY.COD_GL,   ")
				  .append("   COD_BNK_LGL, ")
				  .append("   NBR_IBANACCNT, ")
				  //.append("   TXN.NBR_PROCESSID, ")SUP_1045
				  //.append("   TXN.NBR_LOTID, ")SUP_1045
				  .append("   TXN.ACCT_SETTLE_CR, ")
				  .append("   NVL(AMT_MIN_CR,0) AMT_MIN_CR,NVL(AMT_MIN_DR,0) AMT_MIN_DR, ")//INT_1027 Added NVL
				  .append("   TXN.ACCT_SETTLE_DR, ")
				  // INT_1055 Start
				  .append("    TXN.DAT_POSTING, DAT_POST_SETTLEMENT,TXN.DAT_POST_CREDITDELAY CREDITDELAY,")
					// INT_1055 End
				  .append("   TXN.DAT_POST_DEBITDELAY DEBITDELAY")
				  .append("  FROM OPOOL_TXNPOOL_PARTYDTLS PARTY, ")
				  .append("	 OPOOL_TXNPOOLHDR TXN  ")
				  .append("  WHERE PARTY.ID_POOL = TXN.ID_POOL ")
				  //.append(" AND  TXN.DAT_POST_SETTLEMENT = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')") // SUP_1073: Commented
				  .append(" AND  TXN.DAT_POSTING = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')") // SUP_1073: Added
				  //.append(" AND	   TXN.COD_GL = '" + client +"'")//INT_1002
				  .append(" AND	   TXN.NBR_PROCESSID = PARTY.NBR_PROCESSID  ")
				  .append(" AND	   TXN.DAT_BUSINESS_POOLRUN = PARTY.DAT_BUSINESS_POOLRUN ")
				  .append(" AND	   TXN.TXT_STATUS = 'ACCRUED'  	   ")
				  .append(" AND	   (TYP_POSTING  = 'R' ) ")
				  .append(" AND	   TXN.COD_COUNTER_ACCOUNT = 'S' ")
				  //.append(" AND	   (TXN.TYP_ACCTINT_CRACT <> 'SUPP' AND TXN.TYP_ACCTINT_DRACT <> 'SUPP') ") //SUP_1040
				  .append("AND	   TYP_PARTICIPANT = 'ACCOUNT'  ");

				  if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
					sqlSettlementPosting.append(" AND TXN.ID_POOL IN (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR LOT ");
					/** ****** JTEST FIX start ************  on 14DEC09*/
//					sqlSettlementPosting.append(" WHERE LOT.NBR_LOTID =" + generateForId+")");
					sqlSettlementPosting.append(" WHERE LOT.NBR_LOTID = ? )");
					/** ****** JTEST FIX end **************  on 14DEC09*/
				  }
				//	-- CONSIDER ALL THOSE BUSINESS DATES FOR WHICH THE posting IS BEING DONE, IF NOT DONE ALREADY ie between lastposting cycle and currentposting cycle
				sqlSettlementPosting.append("AND	 PARTY.DAT_BUSINESS_POOLRUN BETWEEN  NVL(TXN.DAT_LASTPOSTINGCALC,TXN.DAT_START-1)+1   AND TXN.DAT_ADJUSTEDPOSTING ")
				  .append("AND	 TXN.FLG_REALPOOL = 'Y'   ")
				  .append("AND	 PARTY.ID_BVT = (SELECT MAX(TO_NUMBER(M.ID_BVT))  ")//INT_1026
				  .append("		 			  			FROM 	 OPOOL_TXNPOOLHDR M ")
				  .append("								WHERE M.ID_POOL = TXN.ID_POOL ")
				  .append("								AND M.DAT_BUSINESS_POOLRUN = TXN.DAT_BUSINESS_POOLRUN ")
				  .append("								AND M.COD_GL = TXN.COD_GL ")
				  .append("								)")
				  .append(" GROUP BY ")
				  .append("   flg_def_drcr_consol, PARTY.ID_POOL, ")
				  .append("   PARTICIPANT_ID,")
				  .append("   TXN.NBR_PROCESSID,") //Mashreq_AE_Hook
				  .append("   COD_PARTY_CCY, ")
				  .append("   NBR_COREACCOUNT, ")
				  .append("   COD_BRANCH,")
				  .append("   PARTY.COD_GL,   ")
				  .append("   COD_BNK_LGL, ")
				  .append("   NBR_IBANACCNT, ")
				  //.append("   TXN.NBR_PROCESSID, ")//SUP_1045
				  //.append("   TXN.NBR_LOTID, ")//SUP_1045
				  .append("   AMT_MIN_CR,AMT_MIN_DR, ")
				  .append("   TXN.ACCT_SETTLE_CR,")
				  .append("   TXN.ACCT_SETTLE_DR,")
				  // INT_1055 Start
				  .append("   DAT_POSTING, DAT_POST_SETTLEMENT,TXN.DAT_POST_CREDITDELAY ,")
					// INT_1055 End
				  .append("   TXN.DAT_POST_DEBITDELAY  ")
				  .toString();

				  if (logger.isDebugEnabled())
					logger.debug("Query POSTING SETTLEMENT : " + sqlSettlementPosting);
				  /** ****** JTEST FIX start ************  on 14DEC09*/
//				  rs = stmt.executeQuery(sqlSettlementPosting.toString());
				  
				  pStmt = conn.prepareStatement(sqlSettlementPosting.toString());
					if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
						pStmt.setString(1, generateForId);
					}
					rs = pStmt.executeQuery();
				  /** ****** JTEST FIX end **************  on 14DEC09*/
				  if (logger.isDebugEnabled()) {
						logger.debug(" Executed Query  POSTING SETTLEMENT ");
				  }
				  results = false;
					while(rs.next()){
						results = true;
						acctDoObj = new AccountingDO();
						String flg_def_drcr_consol = rs.getString("CONSOLIDATE");
						b_flgSeperate = true;
						accountingEntryId = "0";
						details = "";
						poolId = "";
						amtToPostCredit = new BigDecimal(rs.getString("SUM_CR_INT"));
						amountToPostDebit = new BigDecimal(rs.getString("SUM_DR_INT"));
						poolId = rs.getString("ID_POOL");
						hmFields.put(ExecutionConstants.PROCESSID, rs.getLong("NBR_PROCESSID")); //Mashreq_AE_Hook
						if (logger.isDebugEnabled()) {
							logger.debug("Resultset flg_def_drcr_consol" + flg_def_drcr_consol);
							logger.debug("Resultset POOLID" + poolId);
						}
						if("Y".equalsIgnoreCase(flg_def_drcr_consol!=null
																	?flg_def_drcr_consol.trim()
																	:"" )){
							//Get the NetInterest,if consolidate
							amountToPost = amtToPostCredit.subtract(amountToPostDebit);
							// Set the seperate posting flag to False
							b_flgSeperate = false;
						}
						if (logger.isDebugEnabled()) {
							logger.debug(" Flag Separate" + b_flgSeperate);
						}
						//fill in the AccountingDo object's attributes.
						acctDoObj.set_cod_bnk_lgl(rs.getString("COD_BNK_LGL"));
						acctDoObj.set_cod_branch(rs.getString("COD_BRANCH"));
						acctDoObj.set_cod_gl(rs.getString("COD_GL"));
						acctDoObj.set_cod_ccy(rs.getString("COD_PARTY_CCY"));
						acctDoObj.set_nbr_coreaccount(rs.getString("NBR_COREACCOUNT"));
						acctDoObj.set_nbr_ibanacct(rs.getString("NBR_IBANACCNT"));
						acctDoObj.set_nbr_ownaccount(rs.getString("PARTICIPANT_ID"));
						//get the settlement accounts too as the entries for this would be central and participant
						//fetch and set TXN.ACCT_SETTLE_CR,
						//TXN.ACCT_SETTLE_DR, this will be one of the legs
						acctDoObj.setCrBeneficiaryAccount(rs.getString("ACCT_SETTLE_CR"));
						acctDoObj.setDrBeneficiaryAccount(rs.getString("ACCT_SETTLE_DR"));
						minAmtCredit = rs.getBigDecimal("AMT_MIN_CR");
						minAmtDebit = rs.getBigDecimal("AMT_MIN_DR");

						// INT_1055 Start
						hmFields.put("DAT_VALUE",(Date)rs.getDate("DAT_POSTING"));
						// INT_1055 End
						hmFields.put("DAT_POST_SETTLEMENT",(Date)rs.getDate("DAT_POST_SETTLEMENT"));
						hmFields.put("DAT_POST_CREDITDELAY",(Date)rs.getDate("CREDITDELAY"));
						hmFields.put("DAT_POST_DEBITDELAY",(Date)rs.getDate("DEBITDELAY"));

						if (logger.isDebugEnabled()) {
							// INT_1055 Start
							logger.debug("Value Date: " +hmFields.get("DAT_VALUE"));
							// INT_1055 End
							logger.debug("Account Object" + acctDoObj);
						}
						// check is any amount is left due to be posted.
						// checkAmtNotPosted()

						// INT_1055 Start
						valueDate = (Date)hmFields.get("DAT_VALUE");
						valueDate = (valueDate == null ? businessdate : addOffsetToDate(valueDate, 1));
						hmFields.put("DAT_SENT", valueDate);
						// INT_1055 End

						if(b_flgSeperate){
							//POSTING CREDIT INTEREST
//INT_1039--Starts
							bd_amtNotPosted = new BigDecimal(0);//RESETTING AMOUNT NOT POSTED
							bd_amtNotPosted = setUnpostedAmounts(ExecutionConstants.CENTRAL_INTEREST,acctDoObj,
								pstmtAccountUnposted,businessdate,ExecutionConstants.POSTING);
//							chk if any amount is left to be posted
							if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
								if(bd_amtNotPosted.compareTo(zero) >=0 ){
									amtToPostCredit = amtToPostCredit.add(bd_amtNotPosted);
								}else{
									amountToPostDebit = amountToPostDebit.add(bd_amtNotPosted);
								}
								if (logger.isDebugEnabled()) {
									logger.debug("Adding Unposted Amount  "  + bd_amtNotPosted +"for ExecutionConstants.CENTRAL_INTEREST "+amtToPostCredit);
								 }
							 }// UNPOSTED CENTRAL INTEREST
//							INT_1039--Ends
							if (amtToPostCredit.compareTo(zero)!=0){
								//also check for minm CR Interest
								if(amtToPostCredit.compareTo(minAmtCredit)>=0){
									postingDate = (Date)rs.getDate("CREDITDELAY");
									postingDate = postingDate!=null?postingDate:businessdate;
									hmFields.put("DAT_POSTING",postingDate);
									//BVT_TXN Inc Starts
									// INT_1055 Start
									// hmFields.put("DAT_SENT",businessdate);
									// INT_1055 End
									//BVT_TXN Inc Ends
									accountingEntryId = "" + processEntry(acctDoObj,
											amtToPostCredit,ExecutionConstants.POSTING,
											ExecutionConstants.CREDIT,
											ExecutionConstants.CENTRAL_INTEREST,pstmtAccountingEntry,pstmtAcctUnpostedInsert,
											/** ****** JTEST FIX start ************  on 14DEC09*/
//											poolId,hmFields);//INT_1039
											poolId,hmFields, conn);//INT_1039
											/** ****** JTEST FIX end **************  on 14DEC09*/
								}else{
									addToAcctUnpostedTbl(pstmtAcctUnpostedInsert,acctDoObj.get_nbr_ownaccount(),
										amtToPostCreditP,ExecutionConstants.POSTING,ExecutionConstants.CENTRAL_INTEREST,"INTEREST LESS THAN MINIMUM AMOUNT INTEREST",poolId);//INT_1039
								}//minCreditInterest
							}else{
								logger.fatal("Credit Interest is 0 for"  + acctDoObj + "SO not posting the amount ");
							}//if (amtToPostCredit.compareTo(zero)!=0){

							if (amountToPostDebit.compareTo(zero)!=0){
								if(amountToPostDebit.compareTo(minAmtDebit)>=0){
									postingDate = (Date)rs.getDate("DEBITDELAY");
									postingDate = postingDate!=null?postingDate:businessdate;
									hmFields.put("DAT_POSTING",postingDate);
									//BVT_TXN Inc Starts
									// INT_1055 Start
									// hmFields.put("DAT_SENT",businessdate);
									// INT_1055 End
									//BVT_TXN Inc Ends
									accountingEntryId = accountingEntryId+"|" + processEntry(acctDoObj,
									amountToPostDebit,ExecutionConstants.POSTING,
									ExecutionConstants.DEBIT,
									ExecutionConstants.CENTRAL_INTEREST,
									pstmtAccountingEntry,pstmtAcctUnpostedInsert,
									/** ****** JTEST FIX start ************  on 14DEC09*/
//									poolId,hmFields);//INT_1039
									poolId,hmFields, conn);//INT_1039
									/** ****** JTEST FIX end **************  on 14DEC09*/
								}else{
									addToAcctUnpostedTbl(pstmtAcctUnpostedInsert,acctDoObj.get_nbr_ownaccount(),
									amountToPostDebit,ExecutionConstants.POSTING,ExecutionConstants.CENTRAL_INTEREST,"INTEREST LESS THAN MINIMUM AMOUNT INTEREST",poolId);//INT_1039
							}//minDebitInterest
							}else{
								logger.fatal("Debit Interest is 0 for"  + acctDoObj + "SO not posting the amount ");
							}

						}else{
								//chk if any amount is left to be posted
								if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
									amountToPost = amountToPost.add(bd_amtNotPosted);
									if (logger.isDebugEnabled()) {
										logger.debug("Adding Unposted Amount  "  + bd_amtNotPosted +"for ExecutionConstants.CENTRAL_INTEREST "+amountToPost);
									}
								}// UNPOSTED DEBIT AMOUNT

								//call processEntry for the NetAmount --consolidated
								//	CHECK IF THE NET_INTEREST IS CREDIT(+ve) or DEBIT(-ve)
								//	if (CREDIT)
								if (amountToPost.compareTo(zero) > 0){
									postingDate = (Date)rs.getDate("CREDITDELAY");
									minAmtNet = minAmtCredit;
								}else if (amountToPost.compareTo(zero) < 0){
									postingDate = (Date)rs.getDate("DEBITDELAY");
									minAmtNet = minAmtDebit;
								}else{
									logger.fatal("Net Interest is 0 for"  + acctDoObj +
											"SO not posting the amount ");
								}
								postingDate = postingDate!=null?postingDate:businessdate;
								hmFields.put("DAT_POSTING",postingDate);
							//BVT_TXN Inc Starts
							// INT_1055 Start
							// hmFields.put("DAT_SENT",businessdate);
							// INT_1055 End
							//BVT_TXN Inc Ends
							if((amountToPost).abs().compareTo(minAmtNet)>=0){//Changed for absolute of NET_INTEREST while posting
								accountingEntryId = "" + processEntry(acctDoObj,
										amountToPost,ExecutionConstants.POSTING,
										ExecutionConstants.NET,//ENTRY
										ExecutionConstants.CENTRAL_INTEREST,//DETAIL
										pstmtAccountingEntry,pstmtAcctUnpostedInsert,
										/** ****** JTEST FIX start ************  on 14DEC09*/
//										poolId,hmFields);//INT_1039
										poolId,hmFields, conn);//INT_1039
										/** ****** JTEST FIX end **************  on 14DEC09*/
							}else{
								addToAcctUnpostedTbl(pstmtAcctUnpostedInsert,acctDoObj.get_nbr_ownaccount(),
									amountToPostDebit,ExecutionConstants.POSTING,ExecutionConstants.CENTRAL_INTEREST,"INTEREST LESS THAN MINIMUM AMOUNT INTEREST",poolId);//INT_1039
							}//minNetInterest
						}

						if (logger.isDebugEnabled()) {
								logger.debug("After processEntry accountingEntryId"  + accountingEntryId);
						}
						//postingEntries[ExecutionConstants.OLM_ACCOUNTINGENTRY_INSERT] = true;
						//postingEntries[ExecutionConstants.PARTY_DTLS_UPDATE] = true;
						//updateInterest(pstmtTxnPoolPartyDtls,accountingEntryId,poolId,acctDoObj.get_nbr_ownaccount());
						}//end of while resultset
						if (!results){
							logger.fatal(" No records for Posting -participant" +
									" interest Settlement Account" + sqlSettlementPosting);
						}
					rs.close();

/////////////pool ReallocationPosting ////////////////////////////////////////////////////////////
				  StringBuilder sqlReallocationPosting = new StringBuilder("SELECT  ")
						.append("   TXN.ID_POOL,")
						.append("   TXN.NBR_PROCESSID NBR_PROCESSID, ") // Mashreq_AE_Hook
						.append("   TXN.FLG_CONSOLIDATION CONSOLIDATE,  ")
						.append("   PARTY.PARTICIPANT_ID, ")
						.append("   COD_CCY_ACCOUNT, ")
						//.append("   TYP_SETTLE,")
						.append("   NBR_COREACCOUNT,    ")
						.append("   COD_BRANCH,  ")
						.append("   PARTY.COD_GL,      ")
						.append("   COD_BNK_LGL,    ")
						.append("   NBR_IBANACCNT,    ")
						.append("   FLG_ALLOCATION, ")
						.append("   SUM(EQV_AMT_ALLOCATION) SUM, ")
						//.append("   ALLOCATOR, AMT_FOR_ALLOCATION, ")
						.append("   ALLOCATOR, ")
						.append("   NARRATION,")
						//.append("  (CASE WHEN FLG_ALLOCATION = 'T' THEN PARENT_TXN.ACCT_SETTLE_DR ELSE TXN.ACCT_SETTLE_DR END) ACCT_SETTLE_DR,")//SUP_1028
						//.append("  (CASE WHEN FLG_ALLOCATION = 'T' THEN PARENT_TXN.ACCT_SETTLE_CR ELSE TXN.ACCT_SETTLE_CR END) 	ACCT_SETTLE_CR,")//SUP_1028
						.append(" TXN.ACCT_SETTLE_DR,TXN.ACCT_SETTLE_CR, " )//SUP_1045
						//.append("   NVL(AMT_MIN_CR,0) AMT_MIN_CR,NVL(AMT_MIN_DR,0) AMT_MIN_DR,      ")//INT_1027 Added NVL   //INT_1028
						// INT_1055 Start
						.append("   TXN.DAT_POSTING, TXN.DAT_POST_DEBITDELAY DEBITDELAY,")
						// INT_1055 End
						.append("    TXN.DAT_POST_SETTLEMENT,TXN.DAT_POST_CREDITDELAY CREDITDELAY, ") //INT_1028
						.append("	 ALLOC.PARTY_ALLOC_SEQUENCE    ")
						.append(" FROM OPOOL_TXNPOOL_ALLOCATIONDTLS ALLOC, ")
						.append("	  OPOOL_TXNPOOL_PARTYDTLS PARTY,")
						.append("	 OPOOL_TXNPOOLHDR TXN ")//SUP_1045
						//.append("	 OPOOL_TXNPOOLHDR TXN ,     OPOOL_TXNPOOLHDR PARENT_TXN     ")//SUP_1028
						.append(" WHERE ")
						.append("	  TXN.ID_POOL = ALLOC.ID_POOL  AND PARTY.ID_POOL = ALLOC.ID_POOL    ")//SUP_1045 PARTY AND POOL
						//.append(" AND  TXN.DAT_POST_SETTLEMENT = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')") // SUP_1073: Commented
						.append(" AND  TXN.DAT_POSTING = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')") // SUP_1073: Added
						//.append("AND  TXN.COD_GL = '" +client+"'") INT_1009
						//.append("AND  TXN.COD_GL = ALLOC.COD_GL ")//SUP_1045
						.append("AND  TXN.NBR_PROCESSID = ALLOC.NBR_PROCESSID   ")
						.append("AND  TXN.DAT_BUSINESS_POOLRUN = ALLOC.DAT_BUSINESS_POOLRUN    ")
						.append("AND  TXN.NBR_PROCESSID = PARTY.NBR_PROCESSID  	 ")
						.append("AND  TXN.DAT_BUSINESS_POOLRUN = PARTY.DAT_BUSINESS_POOLRUN ")
						.append("AND  PARTY.PARTICIPANT_ID = ALLOC.PARTICIPANT_ID ")
						.append("AND  TXN.TXT_STATUS = 'ACCRUED'  	   ")
						.append("AND  FLG_REAL = 'Y'  ")
						//.append("AND  (FLG_ALLOCATION = 'R' OR FLG_ALLOCATION = 'T')")//SUP_1028
						.append("AND  (FLG_ALLOCATION = 'R') ")//SUP_1045 Handle T seperately
						.append("AND  ALLOC.TYP_PARTICIPANT = 'ACCOUNT' ")
						.append("AND  PARTY.TYP_PARTICIPANT = 'ACCOUNT' ")//SUP_1045
						.append("AND  TXN.FLG_REALPOOL = 'Y' ");
						//.append((generateFor.equalsIgnoreCase(ExecutionConstants.LOT))?" AND TXN.NBR_LOTID =" + generateForId:" ")
						if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
							sqlReallocationPosting.append(" AND TXN.ID_POOL IN (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR LOT ");
							/** ****** JTEST FIX start ************  on 14DEC09*/
//							sqlReallocationPosting.append(" WHERE LOT.NBR_LOTID =" + generateForId+")");
							sqlReallocationPosting.append(" WHERE LOT.NBR_LOTID =?)");
							/** ****** JTEST FIX end **************  on 14DEC09*/
						}
						//				CONSIDER ALL THOSE BUSINESS DATES FOR WHICH THE ACCRUAL IS BEING DONE, IF NOT DONE ALREADY ie between lastaccrual and current accrual
						sqlReallocationPosting.append("AND   ALLOC.DAT_BUSINESS_POOLRUN BETWEEN  NVL(TXN.DAT_LASTPOSTINGCALC,TXN.DAT_START-1)+1  AND  TXN.DAT_ADJUSTEDPOSTING ")
						.append("AND   ALLOC.ID_BVT = (SELECT MAX(TO_NUMBER(M.ID_BVT)) ")//INT_1026
						.append("		 			  			FROM 	 OPOOL_TXNPOOLHDR M ")
						.append("								WHERE M.ID_POOL = TXN.ID_POOL ")
						.append("								AND M.NBR_PROCESSID = TXN.NBR_PROCESSID ")
						.append("					  )")
						//.append(" AND   PARENT_TXN.ID_POOL = TXN.ID_PARENT_POOL ")//SUP_1028
						.append(" GROUP BY ")
						// INT_1055 Start
						.append(" TXN.FLG_CONSOLIDATION, TXN.ID_POOL,    PARTY.PARTICIPANT_ID,   COD_CCY_ACCOUNT, ALLOCATOR,  NARRATION,  NBR_COREACCOUNT,    COD_BRANCH,   PARTY.COD_GL,      COD_BNK_LGL,    NBR_IBANACCNT, AMT_MIN_CR,AMT_MIN_DR,   TXN.ACCT_SETTLE_CR,   TXN.ACCT_SETTLE_DR, TXN.DAT_POSTING, TXN.DAT_POST_SETTLEMENT,TXN.DAT_POST_CREDITDELAY ,   TXN.DAT_POST_DEBITDELAY ,FLG_ALLOCATION ,ALLOC.PARTY_ALLOC_SEQUENCE ")//SUP_1028
						// INT_1055 End
						.append(" ,TXN.ACCT_SETTLE_DR,TXN.ACCT_SETTLE_CR")//SUP_1028//SUP_1045
						.append(" , TXN.NBR_PROCESSID ") // Mashreq_AE_Hook
						//SUP_1028 START --FOR HANDLING PARENT POOL'S CENTRAL -> PARTY POOL'S CENTRAL ACCOUNT
						.append("		  UNION				  		      ")
						.append("		  SELECT  TXN.ID_POOL, ")
						.append("		          TXN.NBR_PROCESSID NBR_PROCESSID, ") // Mashreq_AE_Hook
						.append("		  		  TXN.FLG_CONSOLIDATION CONSOLIDATE, ")
						.append("				  PARTY.NBR_OWNACCOUNT PARTICIPANT_ID,  				   ")
						.append("				  COD_CCY_ACCOUNT,   				         				  ")
						.append("				  NBR_COREACCOUNT,  ")
						.append("				  COD_BRANCH,")
						.append("				  PARTY.COD_GL, ")
						.append("				  COD_BNK_LGL, ")
						.append("				  NBR_IBANACCNT,")
						.append("				  FLG_ALLOCATION,")
						.append("				  SUM(EQV_AMT_ALLOCATION) SUM, ")
						.append("				  ALLOCATOR, ")
						.append("				  NARRATION ,")
						.append("				  TXN_PARENT.ACCT_SETTLE_DR,   ")
						.append("		   		  TXN_PARENT.ACCT_SETTLE_CR,				  		    ")
						// INT_1055 Start
						.append("				  TXN.DAT_POSTING, TXN.DAT_POST_SETTLEMENT,")
						// INT_1055 End
						.append("				  TXN.DAT_POST_CREDITDELAY,")
						.append("				  TXN.DAT_POST_DEBITDELAY,")
						.append("	  		      ALLOC.PARTY_ALLOC_SEQUENCE    ")
						.append("		  FROM  OPOOL_TXNPOOL_ALLOCATIONDTLS ALLOC,  ")
						.append("		  		OPOOL_TXNPOOLHDR TXN,  ")
						.append("				ORBICASH_ACCOUNTS PARTY,")
						.append("				OPOOL_TXNPOOLHDR TXN_PARENT ")
						.append("		  WHERE TXN.ID_POOL = ALLOC.ID_POOL   ")
						//.append("		  AND   TXN.DAT_POST_SETTLEMENT = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "') ") // SUP_1073: Commented
						.append("		  AND   TXN.DAT_POSTING = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "') ") // SUP_1073: Added
						.append("		  AND	TXN.NBR_PROCESSID = ALLOC.NBR_PROCESSID     ")
						.append("		  AND	TXN.DAT_BUSINESS_POOLRUN = ALLOC.DAT_BUSINESS_POOLRUN   ")
						.append("		  AND	 PARTY.NBR_OWNACCOUNT = ALLOC.PARTICIPANT_ID        ")
						.append("		  AND	 TXN.TXT_STATUS = 'ACCRUED'   ")
						.append("		  AND	 FLG_REAL = 'Y'   ")
						.append("		  AND	 FLG_ALLOCATION = 'T'  ")
						.append("		  AND	 (ALLOC.TYP_PARTICIPANT = 'CENTRAL' )  ")
						.append("		  AND    TXN.FLG_REALPOOL = 'Y'     ")
						.append("		  AND    ALLOC.ID_BVT = (SELECT MAX(TO_NUMBER(M.ID_BVT))  FROM 	 OPOOL_TXNPOOLHDR M  ")
						.append("		  		 			  									  WHERE M.ID_POOL = TXN.ID_POOL  ")
						.append("																  AND M.DAT_BUSINESS_POOLRUN = TXN.DAT_BUSINESS_POOLRUN    ")
						.append("																  AND M.COD_GL = TXN.COD_GL)		")
						.append("		  AND   ALLOC.DAT_BUSINESS_POOLRUN BETWEEN  NVL(TXN.DAT_LASTPOSTINGCALC,TXN.DAT_START-1)+1  AND TXN.DAT_ADJUSTEDPOSTING  ");
						if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
							sqlReallocationPosting.append(" AND TXN.ID_POOL IN (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR LOT ");
							/** ****** JTEST FIX start ************  on 14DEC09*/
//							sqlReallocationPosting.append(" WHERE LOT.NBR_LOTID =" + generateForId+")");
							sqlReallocationPosting.append(" WHERE LOT.NBR_LOTID =?)");
							/** ****** JTEST FIX end **************  on 14DEC09*/
						}
						sqlReallocationPosting.append("		  AND   TXN_PARENT.ID_POOL = TXN.ID_PARENT_POOL     AND  TXN_PARENT.NBR_LOTID = TXN.NBR_LOTID ")//SUP_1045
						.append("		  GROUP BY TXN.FLG_CONSOLIDATION, ")
						.append("		  		   TXN.ID_POOL,    ")
						.append("		  		   TXN.NBR_PROCESSID,    ") // Mashreq_AE_Hook
						.append("				   PARTY.NBR_OWNACCOUNT,   ")
						.append("				   COD_CCY_ACCOUNT, ")
						.append("				   ALLOCATOR,  ")
						.append("				   NARRATION,  ")
						.append("				   NBR_COREACCOUNT,    ")
						.append("				   COD_BRANCH,   ")
						.append("				   PARTY.COD_GL,      ")
						.append("				   COD_BNK_LGL,    ")
						.append("				   NBR_IBANACCNT, ")
						.append("				   TXN_PARENT.ACCT_SETTLE_CR,   ")
						.append("				   TXN_PARENT.ACCT_SETTLE_DR,    ")
						// INT_1055 Start
						.append("				   TXN.DAT_POSTING, TXN.DAT_POST_SETTLEMENT,")
						// INT_1055 End
						.append("				   TXN.DAT_POST_CREDITDELAY ,   ")
						.append("				   TXN.DAT_POST_DEBITDELAY ,")
						.append("				   FLG_ALLOCATION,TYP_SETTLE,")
						.append("				   ALLOC.PARTY_ALLOC_SEQUENCE")
						//SUP_1028 END
						.toString();

					  	if (logger.isDebugEnabled()){
							logger.debug("Query POSTING REALLOCATION : " + sqlReallocationPosting);
					   	}
					  	/** ****** JTEST FIX start ************  on 14DEC09*/
//					  	rs = stmt.executeQuery(sqlReallocationPosting.toString());
					  	
					  	pStmt = conn.prepareStatement(sqlReallocationPosting.toString());
						if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
							pStmt.setString(1, generateForId);
							pStmt.setString(2, generateForId);
						}
						rs = pStmt.executeQuery();
					  	/** ****** JTEST FIX end **************  on 14DEC09*/
					  if (logger.isDebugEnabled()) {
							logger.debug(" Executed Query  POSTING REALLOCATION ");
					  }
					results = false;
					while(rs.next()){
						results = true;
						acctDoObj = new AccountingDO();
						accountingEntryId = "";
						details = "";
						poolId = "";
						accountingEntryId = "";
						hmFields.put(ExecutionConstants.PROCESSID, rs.getLong("NBR_PROCESSID")); // Mashreq_AE_Hook

						poolId = rs.getString("ID_POOL");
						amountToPost = new BigDecimal(rs.getString("SUM"));

						//fill in the AccountingDo object's attributes.
						acctDoObj.set_nbr_ownaccount(rs.getString("PARTICIPANT_ID"));
						acctDoObj.set_nbr_coreaccount(rs.getString("NBR_COREACCOUNT"));
						acctDoObj.set_cod_branch(rs.getString("COD_BRANCH"));
						acctDoObj.set_cod_gl(rs.getString("COD_GL"));
						acctDoObj.set_cod_bnk_lgl(rs.getString("COD_BNK_LGL"));
						acctDoObj.set_nbr_ibanacct(rs.getString("NBR_IBANACCNT"));
						acctDoObj.set_cod_ccy(rs.getString("COD_CCY_ACCOUNT"));

						// INT_1055 Start
						hmFields.put("DAT_VALUE",(Date)rs.getDate("DAT_POSTING"));
						// INT_1055 End
						hmFields.put("DAT_POST_SETTLEMENT",(Date)rs.getDate("DAT_POST_SETTLEMENT"));
						hmFields.put("DAT_POST_CREDITDELAY",(Date)rs.getDate("CREDITDELAY"));
						hmFields.put("DAT_POST_DEBITDELAY",(Date)rs.getDate("DEBITDELAY"));

						BigDecimal amtForAllocation = rs.getBigDecimal("SUM");

						//get the settlement accounts too as the entries for this would be central and participant
						//fetch and set TXN.ACCT_SETTLE_CR,
						//TXN.ACCT_SETTLE_DR, this will be one of the legs
						acctDoObj.setCrBeneficiaryAccount(rs.getString("ACCT_SETTLE_CR"));
						acctDoObj.setDrBeneficiaryAccount(rs.getString("ACCT_SETTLE_DR"));
						//put extraDetails in HashMap
						details = "REALLOCATION";
						extraDetails = "ALLOCATOR IS " + rs.getString("ALLOCATOR");
						if (logger.isDebugEnabled()) {
							// INT_1055 Start
							logger.debug("Value Date: " +hmFields.get("DAT_VALUE"));
							// INT_1055 End
							logger.debug("Account Object for ReAllocation" + acctDoObj);
						}
						bd_amtNotPosted = setUnpostedAmounts(details,acctDoObj,
												pstmtAccountUnposted,businessdate,ExecutionConstants.POSTING);
						if (logger.isDebugEnabled()) {
						  logger.debug("Unposted Amount "  + bd_amtNotPosted +"for REALLOCATION");
						 }
						 //chk if any amount is left to be posted
						if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
							amountToPost = amountToPost.add(bd_amtNotPosted);
							if (logger.isDebugEnabled()) {
								logger.debug("Adding Unposted Amount  "  + bd_amtNotPosted +"for REALLOCATION "+amountToPost);
							 }
						 }// UNPOSTED REALLOCATION


						//based on amtForAllocation, decide if it is CREDIT/DEBIT DELAY DAYS FOR POSTING DATE
						if (amountToPost.compareTo(zero) != 0){
							if (amtForAllocation.compareTo(zero) > 0){
								//POSTING CREDIT INTEREST
								postingDate = (Date)rs.getDate("CREDITDELAY");
							}else{
								postingDate = (Date)rs.getDate("DEBITDELAY");
							}
							postingDate = postingDate!=null?postingDate:businessdate;
							hmFields.put("DAT_POSTING",postingDate);
							//BVT_TXN Inc Starts
							// INT_1055 Start
							// hmFields.put("DAT_SENT",businessdate);
							valueDate = (Date)hmFields.get("DAT_VALUE");
							valueDate = (valueDate == null ? businessdate : addOffsetToDate(valueDate, 1));
							hmFields.put("DAT_SENT", valueDate);
							// INT_1055 End
				//BVT_TXN Inc Ends
							accountingEntryId = "" + processEntry(acctDoObj,
									amountToPost,ExecutionConstants.POSTING,
									//ExecutionConstants.NET, ExecutionConstants.CENTRAL_INTEREST,
									ExecutionConstants.NET, details,
									pstmtAccountingEntry,pstmtAcctUnpostedInsert,
									/** ****** JTEST FIX start ************  on 14DEC09*/
//									poolId,hmFields);//INT_1039
									poolId,hmFields, conn);//INT_1039
									/** ****** JTEST FIX end **************  on 14DEC09*/
						}else{
							logger.fatal(" fOR rEALLOCATION Not Posting  Amount as amountTo Post = 0 amount for account " + acctDoObj);
						}
						//postingEntries[ExecutionConstants.OLM_ACCOUNTINGENTRY_INSERT] = true;
						//postingEntries[ExecutionConstants.ALLOCATION_UPDATE] = true;
					}//end of while resultset
					if (!results){
						logger.fatal(" No records for Posting -REALLOCATION"+ sqlReallocationPosting);
					}
					/*INT_1039
					if (logger.isDebugEnabled()) {
						logger.debug("Buffer value for Inserting into ACCOUNT UNPOSTED TABLE =  " + sbufUnpostedAmtInsr);
					}
					postingEntries[ExecutionConstants.UNPOSTED_AMOUNT_INSERT] = (new Boolean(sbufUnpostedAmtInsr.toString())).booleanValue();
					*/
					if (logger.isDebugEnabled()) {
						logger.debug("postingEntries[ExecutionConstants.UNPOSTED_AMOUNT_INSERT] =  " + postingEntries1[ExecutionConstants.UNPOSTED_AMOUNT_INSERT]);
					}
					//INT_1008 generate BankShare and Treasury Share Entries
					generateBankShareTreasuryShareEntries(pstmtAccountingEntry,
								pstmtAcctUnpostedInsert,businessdate,strbusinessdate,ExecutionConstants.POSTING,generateFor,generateForId,client);
				}
			/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//			catch(Exception e){
			catch(SQLException e){
				logger.fatal("General Exception", e);
				throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, e.getMessage());
			} catch(NamingException e){
				logger.fatal("General Exception", e);
				throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, e.getMessage());
			/** ****** FindBugs Fix End ***********  on 14DEC09*/
				}finally{
					LMSConnectionUtility.close(rs);
					/** ****** JTEST FIX start ************  on 14DEC09*/
//					LMSConnectionUtility.close(stmt);
//					// SUP_2034:Issue659 - Start: Added
//					LMSConnectionUtility.close(stmtCal);
//					LMSConnectionUtility.close(stmtNonWorkingDay);
//					// SUP_2034:Issue659 - End
					LMSConnectionUtility.close(pStmt);
					LMSConnectionUtility.close(pStmtCal);
					LMSConnectionUtility.close(pStmtNonWorkingDay);
					/** ****** JTEST FIX end **************  on 14DEC09*/
					LMSConnectionUtility.close(conn);
				}
				return postingEntries1;

		}



	  /**
	   * Gets the Sequence Number from the table
	   * @return
	   * @throws LMSException
	   * @throws SQLException
	   */
	  private long getAESequenceNumber() throws LMSException, SQLException {
		Connection conn = null;
		/** ****** JTEST FIX start ************  on 14DEC09*/
		Statement stmt = null;// JT fix 20SEP11
		//PreparedStatement pStmt = null;// JT fix 20SEP11
		/** ****** JTEST FIX end **************  on 14DEC09*/
		ResultSet rs = null;
		long seqNo = 0;
		try {
		  conn = LMSConnectionUtility.getConnection();

		  /** ****** JTEST FIX start ************  on 14DEC09*/
		  stmt = conn.createStatement();// JT fix 20SEP11
		  /** ****** JTEST FIX end **************  on 14DEC09*/

		  String seqQuery1 = "SELECT SEQ_ACCTENTRY.NEXTVAL FROM DUAL";
		  /** ****** JTEST FIX start ************  on 14DEC09*/
		  rs = stmt.executeQuery(seqQuery1);// JT fix 20SEP11
		  //pStmt = conn.prepareStatement(seqQuery1);
		  //rs = pStmt.executeQuery();// JT fix 20SEP11
		  /** ****** JTEST FIX end **************  on 14DEC09*/

		  while (rs.next()) {
			seqNo = rs.getLong(1);
		  }
		}
		catch (NamingException e) {
			logger.fatal(
			  "Naming Exception in  AESequenceNumber ", e);
		  throw new LMSException("101303", e.toString());

		}
		catch (SQLException e) {
			logger.fatal(
			  "SQL Exception in getting the AESequenceNumber", e);
		  throw new LMSException("101304", e.toString());

		}
		catch (Exception e) {
			logger.fatal(
			  "General Exception in getting the AESequenceNumber", e);
		  throw new LMSException("101305", e.toString());
		}
		finally {
		  LMSConnectionUtility.close(rs);
		  /** ****** JTEST FIX start ************  on 14DEC09*/
		  	LMSConnectionUtility.close(stmt);//JT fix 20SEP11
		 // LMSConnectionUtility.close(pStmt);//JT fix 20SEP11
		  /** ****** JTEST FIX end **************  on 14DEC09*/
		  LMSConnectionUtility.close(conn);
		}

		return seqNo;

	  }
	  //Mashreq_AE_Hook starts
	  /**
	   * Gets the Sequence Number from the table
	   * @param conn Connection Object
	   * @hmDetails HashMap 
	   * @return Accounting Entry sequence number long
	   * @throws LMSException
	   * @throws SQLException
	   */
	  public long getAESequenceNumber(Connection conn, HashMap hmDetails) throws LMSException, SQLException {
	  
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		long seqNo = 0;
		long procssId = 0;
		HashMap hm = new HashMap();
		try {
			
			procssId = (Long) hmDetails.get(ExecutionConstants.PROCESSID);
			pStmt = conn.prepareStatement(seqNoQuery);
			pStmt.setLong(1, procssId);
			
			rs = pStmt.executeQuery();
			while (rs.next()) {
				hm.put(ExecutionConstants.PRODUCT, rs.getString(2));
		    	hm.put(ExecutionConstants.BUSINESSDATE, hmDetails.get(ExecutionConstants.BUSINESSDATE));
		    	hm.put(ExecutionConstants.PROCESSID,procssId);
		    	CustomisationHelper eh = CustomisationHelperFactory.getCustomisationHelper();
				seqNo = eh.evalCustomAccEntryId(rs.getLong(1),hm);
			}
		}
		catch (SQLException e) {
			logger.fatal(
			  "SQL Exception in getting the AESequenceNumber", e);
		  throw new LMSException("101304", e.toString());

		}
		catch (Exception e) {
			logger.fatal(
			  "General Exception in getting the AESequenceNumber", e);
		  throw new LMSException("101305", e.toString());
		}
		finally {
		  LMSConnectionUtility.close(rs);
		  LMSConnectionUtility.close(pStmt);
		  
		}

		return seqNo;

	  }
	  //Mashreq_AE_Hook ends


	/**
	 * Processes the Account to translate it to the Accountingentry
	 * @param account	-participant account
	 * @param amountToPost
	 * @param type		-ACCRUAL/POSTING
	 * @param entry		-Debit/Credit/Net
	 * @param detail	-information to be sent
	 * @param pstmt		-
	 * @param accrualEntries
	 * @param poolId
	 * @return
	 * @throws LMSException
	 * @throws SQLException
	 */
	public long processEntry(AccountingDO account,BigDecimal amountToPost,
			String type,String entry, String detail,
			PreparedStatement pstmt,PreparedStatement pstmtAcctUnpostedInsert,
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			String poolId,HashMap hmFields)
			String poolId,HashMap hmFields, Connection conn)
			/** ****** JTEST FIX end **************  on 14DEC09*/
			throws LMSException,SQLException{//INT_1039
		if (logger.isDebugEnabled())
				logger.debug("Entering");
		/*
		o	getAmountToPost(amountToPost,ACCRUAL) for considering the Accrual amount by truncating/rounding till the decimals
		o	generateLegs(AccountDO,amountToPost,entry,detail,ACCRUAL)  which sets the values in AccountingEntry Leg object for the Dr and Cr legs and returns the list AELegs  which will return the AELegs.
		o	Call the method setPreparedStatement(AELegs) which can return the accountingEntryId
		o	return the AccountingEntryId*/
//		INT_1039--Starts

		//StringBuffer sbuf_carryFwdAmt = new StringBuffer("");
		//CONSIDER THE DECIMALS WHILE POSTING
		//BigDecimal bd_precisionAmt = getAmountToPost(amountToPost,sbuf_carryFwdAmt,type,account.get_cod_ccy());
//SUP_1031--Starts
		//ArrayList precision_carryfwd = getAmountToPost(amountToPost,type,account.get_cod_ccy());
		ExecutionUtility util=new ExecutionUtility();
		/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
		// ENH_1110 Starts
		//BigDecimal[] precision_carryfwd = util.getTruncatedAmt(amountToPost,type,account.get_cod_ccy());
		BigDecimal[] precision_carryfwd ;
		if(account.isExpiringToday()){
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			precision_carryfwd = util.getTruncatedAmt(amountToPost,ExecutionConstants.ROUNDOFF_ACCRUAL_POSTING,account.get_cod_ccy());
			precision_carryfwd = util.getTruncatedAmt(amountToPost,ExecutionConstants.ROUNDOFF_ACCRUAL_POSTING,account.get_cod_ccy(), conn);
			/** ****** JTEST FIX end **************  on 14DEC09*/
		}
		else{
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			precision_carryfwd = util.getTruncatedAmt(amountToPost,type,account.get_cod_ccy()); 
			precision_carryfwd = util.getTruncatedAmt(amountToPost,type,account.get_cod_ccy(), conn); 
			/** ****** JTEST FIX end **************  on 14DEC09*/
		}		
		//ENH_1110 Ends

		/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
		//BigDecimal bd_precisionAmt=(BigDecimal)precision_carryfwd.get(0);//Changed
		//BigDecimal carryFwdAmt=(BigDecimal)precision_carryfwd.get(1);//Chnaged
		BigDecimal bd_precisionAmt=precision_carryfwd[0];
		BigDecimal carryFwdAmt=precision_carryfwd[1];
		//IF CARRYFWD AMOUNT IS PRESENT FOR POSTING ADD IT TO THE TABLE
		if(logger.isDebugEnabled())//Changes for ENH_15.2_LOG_FIXES
			logger.debug("Carry Forward Amount "+carryFwdAmt);

		//if( carryFwdAmt.compareTo(new BigDecimal(0)) != 0 && (ExecutionConstants.POSTING).equals(type.trim())){
		if( carryFwdAmt.compareTo(new BigDecimal(0)) != 0){
//SUP_1031--ENds
			addToAcctUnpostedTbl(pstmtAcctUnpostedInsert,account.get_nbr_ownaccount(),
				carryFwdAmt,type,detail,"AMOUNT_CARRYFORWARD",poolId);//INT_1039
			//accrualEntries[ExecutionConstants.UNPOSTED_AMOUNT_INSERT] = true;
			/*INT_1039
			if("".equals(sbufUnpostedAmtInsr.toString())){
				sbufUnpostedAmtInsr.append("true");
			}

			if (logger.isDebugEnabled()) {
				logger.debug("Insert To Account_Unposted_Tbl_Flg set to ="+ sbufUnpostedAmtInsr);
			}
			*/
		}
		//If posting amount amount zero then return
		if(bd_precisionAmt.compareTo(new BigDecimal(0)) == 0){
			if (logger.isDebugEnabled()) {
				logger.debug("Precision amount is zero so returning");
			}
			return 0;

		}
//		INT_1039--Ends
		//passing precision amount to Post
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		List AELegs = generateLegs(account,bd_precisionAmt,entry,detail,type);
		//List AELegs = generateLegs(account,bd_precisionAmt,entry,detail,type, conn);
		List AELegs = generateLegs(account,bd_precisionAmt,entry,detail,type, conn, hmFields); // Mashreq_AE_Hook
		/** ****** JTEST FIX end **************  on 14DEC09*/
		//List AELegs = generateLegs(account,amountToPost,entry,detail,type);

		if (logger.isDebugEnabled()) {
			logger.debug("setting values to prepared stmt");
		}

		long accId = setPreparedStatementValues(pstmt,AELegs,poolId,hmFields);

		if (logger.isDebugEnabled()){
				logger.debug("Leaving");
		}

		return accId;

	}


	/**
	 * Processes the Account to translate it to the Accountingentry
	 * @param account	-participant account
	 * @param amountToPost
	 * @param type		-ACCRUAL/POSTING
	 * @param entry		-Debit/Credit/Net
	 * @param detail	-information to be sent
	 * @param pstmt		-
	 * @param poolId
	 * @return
	 * @throws LMSException
	 * @throws SQLException
	 */
	public long processCombinedEntry(AccountingDO account,BigDecimal amountToPostCredit,
			BigDecimal amountToPostDebit,String type,String entry, String detail,
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			PreparedStatement pstmt,PreparedStatement pstmtAcctUnpostedInsert,String poolId,HashMap hmFields)
			PreparedStatement pstmt,PreparedStatement pstmtAcctUnpostedInsert,String poolId,HashMap hmFields, Connection conn)
			/** ****** JTEST FIX end **************  on 14DEC09*/
			throws LMSException,SQLException{//INT_1039
		if (logger.isDebugEnabled())
				logger.debug("Entering");
		/*
		o	getAmountToPost(amountToPost,ACCRUAL) for considering the Accrual amount by truncating/rounding till the decimals
		o	generateLegs(AccountDO,amountToPost,entry,detail,ACCRUAL)  which sets the values in AccountingEntry Leg object for the Dr and Cr legs and returns the list AELegs  which will return the AELegs.
		o	Call the method setPreparedStatement(AELegs) which can return the accountingEntryId
		o	return the AccountingEntryId*/
		//first generate legs for creditAmount, which will return 2 legs
		//then generate legs for debitAmount , which will return 2 legs
		//combine the leg of Customer account and create AELegs with 3 legs
		// SEND IT AS SEPARATE LEGS FIRST
//		INT_1039--Starts

		//StringBuffer sbuf_carryFwdAmt = new StringBuffer("");
		//CONSIDER THE DECIMALS WHILE POSTING
		//BigDecimal bd_precisionAmt = getAmountToPost(amountToPostDebit,sbuf_carryFwdAmt,type,account.get_cod_ccy());
//SUP_1031--Starts
		//ArrayList precision_carryfwd = getAmountToPost(amountToPostDebit,type,account.get_cod_ccy());//Changed
		ExecutionUtility util=new ExecutionUtility();
		/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
		// ENH_1110 Starts
		//BigDecimal[] precision_carryfwd = util.getTruncatedAmt(amountToPostDebit,type,account.get_cod_ccy());
		BigDecimal[] precision_carryfwd ;
		if(account.isExpiringToday()){
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			precision_carryfwd = util.getTruncatedAmt(amountToPostDebit,ExecutionConstants.ROUNDOFF_ACCRUAL_POSTING,account.get_cod_ccy());
			precision_carryfwd = util.getTruncatedAmt(amountToPostDebit,ExecutionConstants.ROUNDOFF_ACCRUAL_POSTING,account.get_cod_ccy(), conn);
			/** ****** JTEST FIX end **************  on 14DEC09*/
		}
		else{
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			precision_carryfwd = util.getTruncatedAmt(amountToPostDebit,type,account.get_cod_ccy());
			precision_carryfwd = util.getTruncatedAmt(amountToPostDebit,type,account.get_cod_ccy(), conn);
			/** ****** JTEST FIX end **************  on 14DEC09*/
		}
		//ENH_1110 Ends
		/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
		//BigDecimal bd_precisionAmt=(BigDecimal)precision_carryfwd.get(0);//Changed
		//BigDecimal carryFwdAmt=(BigDecimal)precision_carryfwd.get(1);//Chnaged
		BigDecimal bd_precisionAmt=precision_carryfwd[0];
		BigDecimal carryFwdAmt=precision_carryfwd[1];
		//IF CARRYFWD AMOUNT IS PRESENT ADD IT TO THE TABLE
		//if( carryFwdAmt.compareTo(new BigDecimal(0)) != 0 && (ExecutionConstants.POSTING).equals(type.trim())){//INT_1039
		if( carryFwdAmt.compareTo(new BigDecimal(0)) != 0){
//SUP_1031--ENds
				addToAcctUnpostedTbl(pstmtAcctUnpostedInsert,account.get_nbr_ownaccount(),
				carryFwdAmt,type,detail,"AMOUNT_CARRYFORWARD",poolId);//INT_1039
			//accrualEntries[ExecutionConstants.UNPOSTED_AMOUNT_INSERT] = true;
			/*
				if("".equals(sbufUnpostedAmtInsr.toString())){
				sbufUnpostedAmtInsr.append("true");
			}
			if (logger.isDebugEnabled()) {
				logger.debug("Insert To Account_Unposted_Tbl_Flg set to ="+ sbufUnpostedAmtInsr);
			}
			*/
		}
//		If posting amount amount zero then return
		if(bd_precisionAmt.compareTo(new BigDecimal(0)) == 0){
			if (logger.isDebugEnabled()) {
				logger.debug("Precision amount is zero so returning");
			}
			return 0;

		}
//INT_1039--Ends
		List AEDebitAmtLegs = generateLegs(account,bd_precisionAmt,ExecutionConstants.DEBIT,
				/** ****** JTEST FIX start ************  on 14DEC09*/
//				ExecutionConstants.DEBIT_INTEREST,type);
				//ExecutionConstants.DEBIT_INTEREST,type, conn);
				ExecutionConstants.DEBIT_INTEREST,type, conn, hmFields); // Mashreq_AE_Hook
				/** ****** JTEST FIX end **************  on 14DEC09*/						
		//this can return Dr A1
		//				  Cr RECE
		//OR CR A1,DR PAY

		if (logger.isDebugEnabled()) {
			logger.debug("AEDebitAmtLegs" + AEDebitAmtLegs);
		}
//INT_1039---Starts
		//sbuf_carryFwdAmt.setLength(0);
//SUP_1031--Starts
		//precision_carryfwd = getAmountToPost(amountToPostCredit,type,account.get_cod_ccy());//Changed
		/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */

		// ENH_1110 Starts		
		if(account.isExpiringToday()){
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			precision_carryfwd = util.getTruncatedAmt(amountToPostCredit,ExecutionConstants.ROUNDOFF_ACCRUAL_POSTING,account.get_cod_ccy());
			precision_carryfwd = util.getTruncatedAmt(amountToPostCredit,ExecutionConstants.ROUNDOFF_ACCRUAL_POSTING,account.get_cod_ccy(), conn);
			/** ****** JTEST FIX end **************  on 14DEC09*/
		}
		else{
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			precision_carryfwd = util.getTruncatedAmt(amountToPostCredit,type,account.get_cod_ccy());
			precision_carryfwd = util.getTruncatedAmt(amountToPostCredit,type,account.get_cod_ccy(), conn);
			/** ****** JTEST FIX end **************  on 14DEC09*/
		}
		//ENH_1110 Ends
		/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */

		//bd_precisionAmt=(BigDecimal)precision_carryfwd.get(0);//Changed
		//carryFwdAmt=(BigDecimal)precision_carryfwd.get(1);//Changed
		bd_precisionAmt=precision_carryfwd[0];//Chnged
		carryFwdAmt=precision_carryfwd[1];//Changed
				//IF CARRYFWD AMOUNT IS PRESENT ADD IT TO THE TABLE
		//if(carryFwdAmt.compareTo(new BigDecimal(0)) != 0 && (ExecutionConstants.POSTING).equals(type.trim())){
		if( carryFwdAmt.compareTo(new BigDecimal(0)) != 0){
//SUP_1031--Ends
				addToAcctUnpostedTbl(pstmtAcctUnpostedInsert,account.get_nbr_ownaccount(),
				carryFwdAmt,type,detail,"AMOUNT_CARRYFORWARD",poolId);//INT_1039
			//accrualEntries[ExecutionConstants.UNPOSTED_AMOUNT_INSERT] = true;
			/*INT_1039
				if("".equals(sbufUnpostedAmtInsr.toString())){
				sbufUnpostedAmtInsr.append("true");
			}
			if (logger.isDebugEnabled()) {
				logger.debug("Insert To Account_Unposted_Tbl_Flg set to ="+ sbufUnpostedAmtInsr);
			}
			*/
		}
//		If posting amount amount zero then return
		if(bd_precisionAmt.compareTo(new BigDecimal(0)) == 0){
			if (logger.isDebugEnabled()) {
				logger.debug("Precision amount is zero so returning");
			}
			return 0;

		}
//INT-1039--Ends
		List AECreditAmtLegs = generateLegs(account,bd_precisionAmt,ExecutionConstants.CREDIT,
				/** ****** JTEST FIX start ************  on 14DEC09*/
//				ExecutionConstants.CREDIT_INTEREST,type);
				//ExecutionConstants.CREDIT_INTEREST,type,conn);
				ExecutionConstants.CREDIT_INTEREST,type,conn,hmFields); // Mashreq_AE_Hook
				/** ****** JTEST FIX end **************  on 14DEC09*/						

		if (logger.isDebugEnabled()) {
			logger.debug("AECreditAmtLegs"+AECreditAmtLegs);
		}
		//	combine AEDebitAmtLegs and AECreditAmtLegs and then create AELegs
		//	Both will have customer Legs which have to be clubbed
		List AELegs = new ArrayList();
		AELegData custAccountDebit = new AELegData();
		if ((account.get_nbr_ownaccount()).equals(((AELegData)AEDebitAmtLegs.get(0)).getNbr_ownaccount())){
			custAccountDebit = (AELegData)AEDebitAmtLegs.get(0);
			AELegs.add(AEDebitAmtLegs.get(1));
		}else{
			custAccountDebit = (AELegData)AEDebitAmtLegs.get(1);
			AELegs.add(AEDebitAmtLegs.get(0));
		}
		if (logger.isDebugEnabled()) {
			logger.debug("custAccountDebit"+custAccountDebit);
		}
		AELegData custAccountCredit = new AELegData();
		if ((account.get_nbr_ownaccount())
				.equals(((AELegData)AECreditAmtLegs.get(0)).getNbr_ownaccount())){
			custAccountCredit = (AELegData)AECreditAmtLegs.get(0);
			AELegs.add(AECreditAmtLegs.get(1));
		}else{
			custAccountCredit = (AELegData)AECreditAmtLegs.get(1);
			AELegs.add(AECreditAmtLegs.get(0));
		}

		if (logger.isDebugEnabled()) {
			logger.debug("custAccountCredit"+custAccountCredit);
		}
		//merge the custAccountDebit and custAccountCredit and add it to AELegs
		//ie take a diff of their amounts and depending on the sign apply the dr cr flag.
		//if creditAmount is > debitAmt then flag will be C
//Jtest Fixes
		if(custAccountCredit!=null && custAccountDebit !=null){
			if (custAccountCredit.getAmt_txn()>=custAccountDebit.getAmt_txn()){
				custAccountCredit.setAmt_txn((new BigDecimal(custAccountCredit.getAmt_txn()-custAccountDebit.getAmt_txn())).abs().doubleValue());
				AELegs.add(custAccountCredit);
			}else{
				custAccountDebit.setAmt_txn((new BigDecimal(custAccountCredit.getAmt_txn()-custAccountDebit.getAmt_txn())).abs().doubleValue());
				AELegs.add(custAccountDebit);
			}
		}

		if (logger.isDebugEnabled()) {
			logger.debug("setting values to prepared stmt");
		}

		long accId = setPreparedStatementValues(pstmt,AELegs,poolId,hmFields);

		if (logger.isDebugEnabled()){
				logger.debug("Leaving");
		}

		return accId;

	}


	/**
	 * @param pstmtAcctUnpostedInsert
	 * @param accrualEntries
	 * @param account
	 * @param sbuf_carryFwdAmt
	 * @param type
	 * @param entry
	 * @throws LMSException
	 */
	private void addToAcctUnpostedTbl(PreparedStatement pstmtAcctUnpostedInsert,
			String account,BigDecimal carryFwdAmt,
			String type, String detail,String unPostedDueTo,String poolId) throws LMSException //INT_1039
	{
		if (logger.isDebugEnabled())
			logger.debug("Entering");

		if (logger.isDebugEnabled()) {
			logger.debug("pstmtAcctUnpostedInsert.setString(1,account)" + account);
			logger.debug("pstmtAcctUnpostedInsert.setBigDecimal(2,carryFwdAmt)" + carryFwdAmt);//INT_1039
			logger.debug("pstmtAcctUnpostedInsert.setString(4,type)" + type);
			logger.debug("pstmtAcctUnpostedInsert.setString(5,detail)" + detail);
			logger.debug("pstmtAcctUnpostedInsert.setString(8,poolId)" + poolId);//INT_1039
		}

		try
		{
			pstmtAcctUnpostedInsert.setString(1,account);
			pstmtAcctUnpostedInsert.setBigDecimal(2,carryFwdAmt);//INT_1039
			pstmtAcctUnpostedInsert.setString(3,unPostedDueTo);
			pstmtAcctUnpostedInsert.setString(4,type);
			pstmtAcctUnpostedInsert.setString(5,detail);
			pstmtAcctUnpostedInsert.setDate(6,LMSUtility.truncSqlDate(null));
			pstmtAcctUnpostedInsert.setString(8,poolId);//INT_1039
			pstmtAcctUnpostedInsert.addBatch();
			if (ExecutionConstants.ACCRUAL.equals(type)){
				accrualEntries[ExecutionConstants.UNPOSTED_AMOUNT_INSERT] = true;
			}else{
				postingEntries[ExecutionConstants.UNPOSTED_AMOUNT_INSERT] = true;
			}

		}
		catch (Exception e)
		{
			logger.fatal(
			  "Exception in  addToAcctUnpostedTbl ", e);
		  throw new LMSException("101308", e.toString());

		}

	}
	//INT_1039--Starts
	/**
	 * @param amountToPost
	 * @param accrual
	 * @return
	 * @throws LMSException
	 */
//SUP_1031--Starts
	/*
	private ArrayList getAmountToPost(BigDecimal amountToPost, String type, String currency) throws LMSException {//INT_1039
		//First check the type
		BigDecimal carryfwdAmt= new BigDecimal(0);
		ArrayList precision_carryfwd=new ArrayList();//INT_1039
		BigDecimal precision_amt = new BigDecimal(0);
		BigDecimal precision_carryfwdAmt = new BigDecimal(0);
		//BigDecimal unscaledAmt;//INT_1039
		if (logger.isDebugEnabled())
			logger.debug("Entering");

		//get carryfwd flag and carryforward decimal from opool_currlink
		String s_carryFwdFlg = (String)_hmCurrCarryFwdMap.get(currency);
		BigDecimal bd_numCarryFwd = (BigDecimal)_hmCurrNumCarryFwdMap.get(currency);
		// get flg_truncate and nbr_decimal value from orbicash_currency
		String s_currTruncFlag = (String)_hmCurrTruncMap.get(currency);
		BigDecimal bd_currDecimal	= (BigDecimal)_hmCurrDecimalMap.get(currency);

		if (logger.isDebugEnabled())
			logger.debug("Intial Amount: "+ amountToPost);

		try
		{
			if(ExecutionConstants.ACCRUAL.equalsIgnoreCase(type))
			{
				//This is to add the precision of Amount + CarryFwd to get the full precision
				//Ex: Amt Decimal:3, CarryFwd:3, then Precision:6
				precision_amt = getPrecisionAmount(amountToPost,bd_currDecimal.add(bd_numCarryFwd),s_currTruncFlag.trim());
				//sbuf_carryfwd.append(carryfwdAmt.toString());//INT_1039
				precision_carryfwd.add(0,precision_amt);//INT_1039
				precision_carryfwd.add(1,carryfwdAmt);//INT_1039
			}
			else if(ExecutionConstants.POSTING.equalsIgnoreCase(type))
			{

				//unscaledAmt = amountToPost;
				//precision_amt = getPrecisionAmount(amountToPost,bd_currDecimal,s_currTruncFlag.trim());
				//In Case of Posting we always have to truncate the amount to the precision value.
				precision_amt = getPrecisionAmount(amountToPost,bd_currDecimal,"T");
				if("Y".equalsIgnoreCase(s_carryFwdFlg.trim()))
				{
					carryfwdAmt = amountToPost.subtract(precision_amt);
					precision_carryfwdAmt = getPrecisionAmount(carryfwdAmt,bd_numCarryFwd.add(bd_currDecimal),s_currTruncFlag.trim());
					//sbuf_carryfwd.append(precision_carryfwdAmt.toString());//INT_1039
					precision_carryfwd.add(0,precision_amt);//INT_1039
					precision_carryfwd.add(1,precision_carryfwdAmt);//INT_1039
				}
				//INT_1039--Starts
				else{
					precision_carryfwd.add(0,precision_amt);
					precision_carryfwd.add(1,precision_carryfwdAmt);
				}
				//INT_1039--Ends
			}
		}
		catch(LMSException ex)
		{
			throw ex;
		}

		//if (logger.isDebugEnabled())
			//logger.debug("Final Amount: "+ precision_amt + " , CarryForward Amount:" + sbuf_carryfwd);//INT_1039

		if (logger.isDebugEnabled())
			logger.debug("Leaving");

		return precision_carryfwd;//INT_1039
	}
	*/
	/**
	 * @param amountToPost
	 * @param decimal
	 * @param string
	 * @return
	 * @throws LMSException
	 */
	/*
	private BigDecimal getPrecisionAmount(BigDecimal amount, BigDecimal decimal, String sTruncType) throws LMSException
	{
		BigDecimal bd_amtPrecision = new BigDecimal(0);

		if (logger.isDebugEnabled())
			logger.debug("Entering");

		if (logger.isDebugEnabled())
			logger.debug("Amount: "+ amount + ", DecimalPrecision: "+ decimal + "TruncType: "+ sTruncType);

		if(sTruncType== null ||sTruncType.trim().equals(""))
		{
			throw new LMSException("99999031","W","Rounding Logic Flag value is null or blank."+sTruncType);
		}
		else
		{
			switch(sTruncType.charAt(0))
			{
				case 'M':
						//Have to use method of 5 for this case
						bd_amtPrecision= amount.setScale(decimal.intValue(),BigDecimal.ROUND_HALF_UP);
						break;
				case 'T':
						//Simple truncation for this case
						bd_amtPrecision = amount.setScale(decimal.intValue(),BigDecimal.ROUND_DOWN);
						break;
				default:
					//Need to specify buddy
					break;
			}
		}
		//amount = bd_amtPrecision;
		if (logger.isDebugEnabled())
			logger.debug("Final Amount: "+ bd_amtPrecision);

		if (logger.isDebugEnabled())
			logger.debug("Leaving");

		return bd_amtPrecision;
	}
	*/
//SUP_1031--Ends

	/**
	 *
	 * @param account
	 * @param amountToPost
	 * @param entry
	 * @param detail
	 * @param type
	 * @return
	 * @throws LMSException
	 */

	public List generateLegs(AccountingDO account,BigDecimal amountToPost,String entry,
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			String detail,String type)throws LMSException ,SQLException{
			//String detail,String type,Connection conn)throws LMSException ,SQLException{
			String detail,String type,Connection conn, HashMap hmDetails)throws LMSException ,SQLException{ // Mashreq_AE_Hook
			/** ****** JTEST FIX end **************  on 14DEC09*/
		if (logger.isDebugEnabled())
			logger.debug("Entering");

		//DrCrAllocation starts
		boolean isBVT = false;
		if(entry.equals(ExecutionConstants.BVT_DEBIT)
			|| entry.equals(ExecutionConstants.BVT_CREDIT)
			||entry.equals(ExecutionConstants.BVT_NET)){
			isBVT = true;
		}
		//DrCrAllocation ends
		long accountingEntryId = 0;
		//	Get the Accounting entry Id for the legs by calling getAESequenceNumber, set it to accountingEntryId
		//Mashreq_AE_Hook starts
		//accountingEntryId = getAESequenceNumber();
		accountingEntryId = getAESequenceNumber(conn, hmDetails);
		//Mashreq_AE_Hook ends
		if ((ExecutionConstants.DEBIT).equals(entry)){
			amountToPost = zero.subtract(amountToPost);//(add a -ve sign)
		}
		if (logger.isDebugEnabled()) {
			logger.debug("AMOUNT TO POST " +amountToPost);
		}
		//Consider the following logic for passing Entries
		/*
		o	The Dr/Cr legs are going to be based on the sign of amountToPost
		?	ie if we get positive amt(amountToPost >= 0) then entries will (right now consider 0 amount as credit and post , we can filter this ..ie donot pass an entry for 0 amt)
		*/
		String debitAccount = "";
		String creditAccount = "";
		AccountingDO drAc = null;
		AccountingDO crAc = null;
		String key = "";
		HashMap hm = new HashMap();
		HashMap hmInternalAccts = new HashMap();
		String sKey = "";

		if (ExecutionConstants.ACCRUAL.equals(type)){
		//DrCrAllocation Starts
			if(!isBVT){
			//IDENTIFYING INTERNAL ACCOUNTS FOR ACCRUAL FOR NORMAL EXECUTION
			//DrCrAllocation ends
			if (amountToPost.compareTo(zero) >= 0){
				/*	Dr Expenses A/c
					Cr Payables A/c
				*/
				debitAccount = "EXPENSES";
				creditAccount = "PAYABLES";
			}else if (amountToPost.compareTo(zero) < 0){
				/*then entries will be
					Dr Receivables A/c
					Cr Income A/c creditLeg
				*/
				debitAccount = "RECEIVABLES";
				creditAccount = "INCOME";
			}
			}//DrCrAllocation 
			//DrCrAllocation Starts
			//IDENTIFYING INTERNAL ACCOUNTS FOR ACCRUAL for BVT
			else{
				if (entry.equals(ExecutionConstants.BVT_CREDIT)){
					/*	Dr Expenses A/c
						Cr Payables A/c
					*/
					debitAccount = "EXPENSES";
					creditAccount = "PAYABLES";
					//DrCrAllocation fix starts
					if (amountToPost.compareTo(zero) < 0){
						/*	Cr Expenses A/c
							Dr Payables A/c
						*/
						debitAccount = "PAYABLES";
						creditAccount = "EXPENSES";
					}
					//DrCrAllocation fix ends
				}else if (entry.equals(ExecutionConstants.BVT_DEBIT)){
					/*then entries will be
						Dr Receivables A/c
						Cr Income A/c creditLeg
					*/
					debitAccount = "RECEIVABLES";
					creditAccount = "INCOME";
					//DrCrAllocation fix starts
					if (amountToPost.compareTo(zero) < 0){
						/*
						Cr Receivables A/c
						Dr Income A/c
						*/
						debitAccount = "INCOME";
						creditAccount = "RECEIVABLES";
					}
					//DrCrAllocation fix ends
				}
			}
			//DrCrAllocation Ends
		//GETTING INTERNAL ACCOUNTS FOR ACCRUAL
			if (logger.isDebugEnabled()) {
				logger.debug("Before getting Internal Accounts " +debitAccount + " credit " + creditAccount);
			}
			//get the method for InternalAccount from the properties. we will have to have a mechanism to initiate the method
			checkAccountingDOVals(account, hm);

			sKey = (new StringBuffer((String)hm.get(ExecutionConstants.COD_BNK_LGL))
			.append("|")
			.append((String)hm.get(ExecutionConstants.COD_CCY))
			.append("|")
			.append((String)hm.get(ExecutionConstants.COD_GL))
			.append("|").append(debitAccount))
			.toString();

			if(_hmKeyAccountDO.containsKey(sKey)){
				hmInternalAccts = (HashMap)_hmKeyAccountDO.get(sKey);
			}
			else{
				//_hmAcctTypeSeq
				//arrAccountingDO = getInternalAccountBLE(creditAccount, debitAccount,hm);
				/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
			    	//ENH_1105 Starts
				//If accounting entries are mandatory and ble linkage is assumed to be maintained
				if(ExecutionConstants.GENERATE_ACCOUNTING_ENTRY){
		                //ENH_1105 Ends
				    hmInternalAccts = getInternalAccountBLE(creditAccount, debitAccount,hm);
			    	//ENH_1105 Starts
				//If accounting entries are optional and ble linkage is assumed not to be maintained
				}
				else {
				    hmInternalAccts = getInternalAccountBLE();
                		}
				//ENH_1105 Ends

				_hmKeyAccountDO.put(sKey,hmInternalAccts);
			}
				/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */


			if (logger.isDebugEnabled())
				logger.debug("InternalAccounts hmInternalAccts "+hmInternalAccts);

			if(!hmInternalAccts.containsKey(creditAccount)
					|| !hmInternalAccts.containsKey(debitAccount)){
					throw new LMSException("101306", "W",
							"one or both Dr and Cr BLE Account data Objects not Found: ");
			}
			else{
				crAc = (AccountingDO)hmInternalAccts.get(creditAccount);
				drAc = (AccountingDO)hmInternalAccts.get(debitAccount);
			}

		}else if("POSTING".equals(type)){

		if (!ExecutionConstants.CENTRAL_INTEREST.equals(detail) && !"REALLOCATION".equals(detail)){//If the posting does not require CENTRAL ACCOUNT
			if (logger.isDebugEnabled()) {
				logger.debug("POSTING ENTRIES detail" + detail);
			}
			//DrCrAllocation starts
			if(!isBVT){
			//IDENTIFYING INTERNAL ACCOUNTS FOR POSTING for NORMAL EXECUTION
			//DrCrAllocation ends
				if (amountToPost.compareTo(zero) >= 0){
					/*	Dr Payables A/c
						Cr Customer A/c
					*/
					debitAccount = "PAYABLES";
					creditAccount = "CUSTOMER";
					key = debitAccount;
				}else if (amountToPost.compareTo(zero) < 0){
					/*then entries will be
						Dr Customer A/c
						Cr Receivables A/c
					*/
					debitAccount = "CUSTOMER";
					creditAccount = "RECEIVABLES";
					key = creditAccount;
				}
			//DrCrAllcation Starts
			}
			else{
				//IDENTIFYING INTERNAL ACCOUNTS FOR POSTING for BVT Execution
				if (entry.equals(ExecutionConstants.BVT_CREDIT)){
					/*	Dr Payables A/c
						Cr Customer A/c
					*/
					debitAccount = "PAYABLES";
					creditAccount = "CUSTOMER";
					key = debitAccount;
					//DrCrAllocation fix starts
					if (amountToPost.compareTo(zero) < 0){
						/*	Cr Payables A/c
							Dr Customer A/c
						*/
						debitAccount = "CUSTOMER";
						creditAccount = "PAYABLES";
						key = creditAccount;
					}
					//DrCrAllocation fix ends
				}else if (entry.equals(ExecutionConstants.BVT_DEBIT)){
					/*then entries will be
						Dr Customer A/c
						Cr Receivables A/c
					*/
					debitAccount = "CUSTOMER";
					creditAccount = "RECEIVABLES";
					key = creditAccount;
					//DrCrAllocation fix starts
					if (amountToPost.compareTo(zero) < 0){
						/*then entries will be
							Cr Customer A/c
							Dr Receivables A/c
						*/
						debitAccount = "RECEIVABLES";
						creditAccount = "CUSTOMER";
						key = debitAccount;
					}
					//DrCrAllocation fix ends
				}

			}
			//DrCrAllocation ends
			if (logger.isDebugEnabled())
				logger.debug("key key "+key);
			//GETTING ACCOUNTS FOR POSTING
	//		get the method for InternalAccount from the properties. we will have to have a mechanism to initiate the method
				checkAccountingDOVals(account, hm);
				sKey = (new StringBuffer((String)hm.get(ExecutionConstants.COD_BNK_LGL))
				  .append("|")
				  .append((String)hm.get(ExecutionConstants.COD_CCY))
				  .append("|")
				  .append((String)hm.get(ExecutionConstants.COD_GL))
				  .append("|").append(key))
				  .toString();

				if(_hmKeyAccountDO.containsKey(sKey)){
					hmInternalAccts = (HashMap)_hmKeyAccountDO.get(sKey);
				}
				/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
				else{

				    //ENH_1105 Starts
					//If accounting entries are mandatory and ble linkage is assumed to be maintained
					if(ExecutionConstants.GENERATE_ACCOUNTING_ENTRY){
		                    //ENH_1105 Ends
					    hmInternalAccts = getInternalAccountBLE(creditAccount, debitAccount,hm);
				    //ENH_1105 Starts
					//If accounting entries are optional and ble linkage is assumed not to be maintained
					}
					else {
					    hmInternalAccts = getInternalAccountBLE();
                    			}
					//ENH_1105 Ends

					 _hmKeyAccountDO.put(sKey,hmInternalAccts);
				}
				/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */

				if (logger.isDebugEnabled())
					  logger.debug("InternalAccounts hmInternalAccts "+hmInternalAccts);

				if(!hmInternalAccts.containsKey(key)){
					throw new LMSException("101306", "W",
							" Cr BLE Account data Objects not Found: "+ hmInternalAccts +"FOR KEY" +key);
				 }else{
					if (key.equals(creditAccount)){
						//get the internal account from the hashmap of internal accounts
						crAc = (AccountingDO)hmInternalAccts.get(creditAccount);
						drAc = account;//customer account
					}else if (key.equals(debitAccount)){
						//get the internal account from the hashmap of internal accounts
						drAc = (AccountingDO)hmInternalAccts.get(debitAccount);
						crAc = account;//customer account
					}
				 }//end of if(!hmInternalAccts.containsKey(creditAccount)
			}//if not CENTRAL_INTEREST
			else {
				if(!isBVT){//DrCrAllocation fix
					//FOR Normal Execution
				if (amountToPost.compareTo(zero) >= 0){

					//Cr Customer
					//Dr Central
					debitAccount = "CENTRAL";
					creditAccount = "CUSTOMER";
					if (logger.isDebugEnabled())
						logger.debug("Fetching Central Account for making debit leg");
					/** ****** JTEST FIX start ************  on 14DEC09*/
//					drAc = getAccount(account.getDrBeneficiaryAccount());//as debit is CENTRAL
					drAc = getAccount(account.getDrBeneficiaryAccount(), conn);//as debit is CENTRAL
					/** ****** JTEST FIX end **************  on 14DEC09*/
					crAc = account;
				}else if (amountToPost.compareTo(zero) < 0){
					//Dr Customer
					//Cr Central
					debitAccount = "CUSTOMER";
					creditAccount = "CENTRAL";
					//as credit is CENTRAL
					/** ****** JTEST FIX start ************  on 14DEC09*/
//					crAc = getAccount(account.getCrBeneficiaryAccount());
					crAc = getAccount(account.getCrBeneficiaryAccount(), conn);
					/** ****** JTEST FIX end **************  on 14DEC09*/
					drAc = account;
					if (logger.isDebugEnabled())
						logger.debug("Fetching Central Account for making credit leg");
				}
				//DrCrAllocation starts
				}
				else{
					//FOR BVT Execution
					if (entry.equals(ExecutionConstants.BVT_CREDIT)){

						//Cr Customer
						//Dr Central
						debitAccount = "CENTRAL";
						creditAccount = "CUSTOMER";
						if (logger.isDebugEnabled())
							logger.debug("Fetching Central Account for making debit leg");
						/** ****** JTEST FIX start ************  on 14DEC09*/
//						drAc = getAccount(account.getDrBeneficiaryAccount());//as debit is CENTRAL
						drAc = getAccount(account.getDrBeneficiaryAccount(), conn);//as debit is CENTRAL
						/** ****** JTEST FIX end **************  on 14DEC09*/
						crAc = account;
						//DrCrAllocation fix starts
						if (amountToPost.compareTo(zero) < 0){
							//Dr Customer
							//Cr Central
							debitAccount = "CUSTOMER";
							creditAccount = "CENTRAL";
							if (logger.isDebugEnabled())
								logger.debug("Fetching Central Account for making debit leg");
							/** ****** JTEST FIX start ************  on 14DEC09*/
//							crAc = getAccount(account.getCrBeneficiaryAccount());
							crAc = getAccount(account.getCrBeneficiaryAccount(), conn);
							/** ****** JTEST FIX end **************  on 14DEC09*/
							drAc = account;
						}
						//DrCrAllocation fix ends
					}else if (entry.equals(ExecutionConstants.BVT_DEBIT)){
						//Dr Customer
						//Cr Central
						debitAccount = "CUSTOMER";
						creditAccount = "CENTRAL";
						//as credit is CENTRAL
						/** ****** JTEST FIX start ************  on 14DEC09*/
//						crAc = getAccount(account.getCrBeneficiaryAccount());
						crAc = getAccount(account.getCrBeneficiaryAccount(), conn);
						/** ****** JTEST FIX end **************  on 14DEC09*/
						drAc = account;
						if (logger.isDebugEnabled())
							logger.debug("Fetching Central Account for making credit leg");
						//DrCrAllocation fix starts
						if (amountToPost.compareTo(zero) < 0){
							//Cr Customer
							//Dr Central
							debitAccount = "CENTRAL";
							creditAccount = "CUSTOMER";
							if (logger.isDebugEnabled())
								logger.debug("Fetching Central Account for making debit leg");
							/** ****** JTEST FIX start ************  on 14DEC09*/
//							drAc = getAccount(account.getDrBeneficiaryAccount());
							drAc = getAccount(account.getDrBeneficiaryAccount(), conn);
							/** ****** JTEST FIX end **************  on 14DEC09*/
							crAc = account;
						}
						//DrCrAllocation fix ends
					}

				}
				//DrCrAllocation ends

				/*if(_hmKeyAccountDO.containsKey(sKey)){
					hmInternalAccts = (HashMap)_hmKeyAccountDO.get(sKey);
				}
				else{*/

					 //_hmKeyAccountDO.put(sKey,hmInternalAccts);
				//}

			}//if CENTRAL_INTEREST
		}//end of If POSTING


		if (logger.isDebugEnabled())
			logger.debug("InternalAccount "+debitAccount +drAc + "\n credit " + creditAccount +crAc);
		//Build the narration lines calling NarrationBuilder which will get it from OPOOL_GLLINK_HDR TXT_NARRATIVE_1, TXT_NARRATIVE_2, TXT_NARRATIVE_3, TXT_NARRATIVE_4 and set them to corresponding variables

		String txt_narration1 = "";
		String txt_narration2 = "";
		String txt_narration3 = "";
		String txt_narration4 = "";
		//Depending on the type and details, set  cod_txn from OPOOL_GLLINK_HDR for the Host. COD_DEFCRTXN for Credit, COD_DEFDRTXN for Debit
		List AELegList = new ArrayList();
		AELegData drLeg = new AELegData();
		AELegData crLeg = new AELegData();
		//Populate drLeg with the debit entry
		drLeg.setNbr_acctentryid(accountingEntryId);
		drLeg.setNbr_instrlegid(1);//1st Leg
		drLeg.setNbr_ownaccount(drAc.get_nbr_ownaccount());
		drLeg.setNbr_coreaccount(drAc.get_nbr_coreaccount());
		drLeg.setFlg_accttype(ExecutionConstants.INTERNAL);
		drLeg.setCod_branch(drAc.get_cod_branch());
		drLeg.setCod_bnkglent(drAc.get_cod_bnk_lgl());
		drLeg.setCod_gl(drAc.get_cod_gl());
		drLeg.setCod_ccy(drAc.get_cod_ccy());
		drLeg.setFlg_drcr(ExecutionConstants.DEBIT);
		drLeg.setAmt_txn(amountToPost.abs().doubleValue());
		drLeg.setTxt_narration1(txt_narration1);
		drLeg.setTxt_narration2(txt_narration2);
		drLeg.setTxt_narration3(txt_narration3);
		drLeg.setTxt_narration4(txt_narration4);
		drLeg.setTxt_extinfo1(type);
		drLeg.setFlg_status1(ExecutionConstants.ACCRUAL.equals(type)?"A":"P");//FLG_STATUS1 WILL HAVE TYPE
		drLeg.setTxt_extinfo2(detail);
		drLeg.setTxt_extinfo3(account.get_nbr_ownaccount());
		drLeg.setTxt_extinfo4(debitAccount);//Internal Account
		drLeg.setTxt_status(ExecutionConstants.AE_GENERATED);
		GregorianCalendar dat_sys = new GregorianCalendar();
		drLeg.setDat_system(dat_sys);
		//Add the debitLeg in the AELegList
		if (logger.isDebugEnabled())
					logger.debug("Debit Leg" +drLeg);

		AELegList.add(drLeg);


		//Populate the AELeg with credit entry
		crLeg.setNbr_acctentryid(accountingEntryId);
		crLeg.setNbr_instrlegid(2);//1st Leg
		crLeg.setNbr_acctentryid(accountingEntryId);
		crLeg.setNbr_ownaccount(crAc.get_nbr_ownaccount());
		crLeg.setNbr_coreaccount(crAc.get_nbr_coreaccount());
		crLeg.setFlg_accttype(ExecutionConstants.INTERNAL);
		crLeg.setCod_branch(crAc.get_cod_branch());
		crLeg.setCod_bnkglent(crAc.get_cod_bnk_lgl());
		crLeg.setCod_gl(crAc.get_cod_gl());
		crLeg.setCod_ccy(crAc.get_cod_ccy());
		crLeg.setFlg_drcr(ExecutionConstants.CREDIT);
		crLeg.setAmt_txn(amountToPost.abs().doubleValue());
		crLeg.setTxt_narration1(txt_narration1);
		crLeg.setTxt_narration2(txt_narration2);
		crLeg.setTxt_narration3(txt_narration3);
		crLeg.setTxt_narration4(txt_narration4);
		crLeg.setTxt_extinfo1(type);
		crLeg.setFlg_status1(ExecutionConstants.ACCRUAL.equals(type)?"A":"P");//FLG_STATUS1 WILL HAVE TYPE
		crLeg.setTxt_extinfo2(detail);
		crLeg.setTxt_extinfo3(account.get_nbr_ownaccount());
		crLeg.setTxt_extinfo4(creditAccount);//Internal Account
		crLeg.setTxt_status(ExecutionConstants.AE_GENERATED);
		crLeg.setDat_system(dat_sys);
		//Add the creditLeg in the AELegList
		AELegList.add(crLeg);

		if (logger.isDebugEnabled())
			logger.debug("Credit Leg" +crLeg);

		if (logger.isDebugEnabled())
					logger.debug("Leaving");
		return AELegList;


	}


	  private long setPreparedStatementValues(PreparedStatement pstmt,
												 List AELegs,String poolId,HashMap hmFields) throws SQLException {

		if (logger.isDebugEnabled()){
			logger.debug("Entering");
		}

		long accountingEntryId = 0;
		AELegData leg;
		int AELegSize = AELegs.size();
		for(int i=0;i<AELegSize;i++){

			leg = (AELegData)AELegs.get(i);
			if (logger.isDebugEnabled()){
				logger.debug("Leg[" +i+ "] " + leg);
			}
			long pool= 111;
			pool = Long.valueOf(poolId).longValue();
			Date postingDate = (Date)hmFields.get("DAT_POSTING");
			//BVT_TXN starts here
			Date sendingDate = (Date)hmFields.get("DAT_SENT");
			//BVT_TXN ends here
			accountingEntryId = leg.getNbr_acctentryid();
			java.sql.Date date = new java.sql.Date(leg.getDat_system().getTime().
															   getTime());
			//BVT_TXN starts here
			/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//			String temp = leg.getFlg_status1();
			/** ****** FindBugs Fix End ***********  on 14DEC09*/
			//BVT_TXN ends here
			if (logger.isDebugEnabled()) {
				logger.debug("pstmt.setLong(2,accountingEntryId); //  NBR_ACCTENTRYID"+accountingEntryId);
				logger.debug("pstmt.setLong(3, leg.getNbr_legid()); //  NBR_LEGID" +leg.getNbr_legid());
				logger.debug("pstmt.setLong(4, pool); //  set the pool id here "+pool);
				logger.debug("pstmt.setString(5, leg.getNbr_ownaccount()); //  NBR_OWNACCOUNT" +leg.getNbr_ownaccount());
				logger.debug("pstmt.setString(6, leg.getNbr_coreaccount()); //  NBR_COREACCOUNT"+leg.getNbr_coreaccount());
				logger.debug("pstmt.setString(7, leg.getFlg_accttype()); //	FLG_ACCTTYPE"+ leg.getFlg_accttype());
				logger.debug("pstmt.setString(8, N); //	FLG_MIRRORENTRY"+"N");
				logger.debug("pstmt.setString(9, leg.getCod_branch()); //	COD_BRANCH"+leg.getCod_branch());
				logger.debug("pstmt.setString(10, leg.getCod_bic()); //	TXT_BIC"+leg.getCod_bic());
				logger.debug("pstmt.setString(11, null); // iban accnt NOT REQUIRED"+null);
				logger.debug("pstmt.setString(12, leg.getCod_bnkglent()); //  COD_BNKLGLENT"+leg.getCod_bnkglent());
				logger.debug("pstmt.setString(13, leg.getCod_gl()); //	COD_GL"+leg.getCod_gl());
				logger.debug("pstmt.setString(14, leg.getCod_ccy()); //	COD_CCY"+leg.getCod_ccy());
				logger.debug("pstmt.setString(15, leg.getFlg_drcr()); //	FLG_DRCR"+leg.getFlg_drcr());
				logger.debug("pstmt.setDouble(16, leg.getAmt_txn()); //	AMT_TXN"+ leg.getAmt_txn());
				logger.debug("pstmt.setString(17, leg.getCod_txn()); //	COD_TXN"+leg.getCod_txn());
				logger.debug("pstmt.setString(18, leg.getTxt_narration1()); //	TXT_NARRATION1"+leg.getTxt_narration1());
				logger.debug("pstmt.setString(19, leg.getTxt_narration2()); //	TXT_NARRATION2"+leg.getTxt_narration2());
				logger.debug("pstmt.setString(20, leg.getTxt_narration3()); //	TXT_NARRATION3"+leg.getTxt_narration3());
				logger.debug("pstmt.setString(21, leg.getTxt_narration4()); //	TXT_NARRATION4"+leg.getTxt_narration4());
				logger.debug("pstmt.setString(22, leg.getTxt_extinfo1()); //	TXT_EXTINFO1"+leg.getTxt_extinfo1());
				logger.debug("pstmt.setString(23, leg.getTxt_extinfo2()); //	TXT_EXTINFO2"+leg.getTxt_extinfo2());
				logger.debug("pstmt.setString(24, leg.getTxt_extinfo3()); //	TXT_EXTINFO3"+leg.getTxt_extinfo3());
				logger.debug("pstmt.setString(25, leg.getTxt_extinfo4()); //	TXT_EXTINFO4"+leg.getTxt_extinfo4());
				logger.debug("pstmt.setString(26, leg.getTxt_extinfo5()); //	TXT_EXTINFO5"+leg.getTxt_extinfo5());
				logger.debug("pstmt.setDate(27,LMSUtility.truncSqlDate( date)); //	DAT_SYSTEM"+date);
				logger.debug("pstmt.setString(28, leg.getTxt_status()); //	TXT_STATUS"+ leg.getTxt_status());
				logger.debug("pstmt.setString(31, S);FLG_SRCTGTTYP ");
				logger.debug("pstmt.setString(32, N);FLG_FORCEDR ");
				logger.debug("pstmt.setString(33, leg.getFlg_status1());// FLG_STATUS1"+leg.getFlg_status1());
				logger.debug("pstmt.setDate(34,LMSUtility.truncSqlDate( new java.sql.Date(postingDate.getTime())));//  DAT_POSTING"+new java.sql.Date(postingDate.getTime()));
				logger.debug("pstmt.setDate(35,36,37,LMSUtility.truncSqlDate( new java.sql.Date(settlementDate/creditDelay/DebitDelaypostingDate.getTime())));//  DAT_POSTING"+new java.sql.Date(postingDate.getTime()));
				logger.debug("pstmt.setString(38, (hmFields.containsKey(POOL_FLG_CONSOLIDATION)?(String)hmFields.get(POOL_FLG_CONSOLIDATION):))//COD_FIELD1");//INT_1002
				logger.debug("pstmt.setString(39, null);TXT_HOSTSYSTEMREF");
			}
			//pstmt.setLong(1, leg.getNbrProcessID()); //	NBR_PROCESSID
			pstmt.setLong(2,accountingEntryId); //  NBR_ACCTENTRYID
			pstmt.setLong(3, leg.getNbr_legid()); //  NBR_LEGID
			pstmt.setLong(4, pool); //  set the pool id here
			pstmt.setString(5, leg.getNbr_ownaccount()); //  NBR_OWNACCOUNT
			pstmt.setString(6, leg.getNbr_coreaccount()); //  NBR_COREACCOUNT
			pstmt.setString(7, leg.getFlg_accttype()); //	FLG_ACCTTYPE
			pstmt.setString(8, "N"); //	FLG_MIRRORENTRY
			pstmt.setString(9, leg.getCod_branch()); //	COD_BRANCH
			pstmt.setString(10, leg.getCod_bic()); //	TXT_BIC
			pstmt.setString(11, null); // iban accnt NOT REQUIRED
			pstmt.setString(12, leg.getCod_bnkglent()); //  COD_BNKLGLENT
			pstmt.setString(13, leg.getCod_gl()); //	COD_GL
			pstmt.setString(14, leg.getCod_ccy()); //	COD_CCY
			pstmt.setString(15, leg.getFlg_drcr()); //	FLG_DRCR
			pstmt.setDouble(16, leg.getAmt_txn()); //	AMT_TXN
			pstmt.setString(17, leg.getCod_txn()); //	COD_TXN
			pstmt.setString(18, leg.getTxt_narration1()); //	TXT_NARRATION1
			pstmt.setString(19, leg.getTxt_narration2()); //	TXT_NARRATION2
			pstmt.setString(20, leg.getTxt_narration3()); //	TXT_NARRATION3
			pstmt.setString(21, leg.getTxt_narration4()); //	TXT_NARRATION4
			pstmt.setString(22, leg.getTxt_extinfo1()); //	TXT_EXTINFO1
			pstmt.setString(23, leg.getTxt_extinfo2()); //	TXT_EXTINFO2
			pstmt.setString(24, leg.getTxt_extinfo3()); //	TXT_EXTINFO3
			pstmt.setString(25, leg.getTxt_extinfo4()); //	TXT_EXTINFO4
			pstmt.setString(26, leg.getTxt_extinfo5()); //	TXT_EXTINFO5

			pstmt.setDate(27,LMSUtility.truncSqlDate( date)); //	DAT_SYSTEM
			pstmt.setString(28, leg.getTxt_status()); //	TXT_STATUS
			//pstmt.setDate(29,LMSUtility.truncSqlDate( new java.sql.Date(leg.getDat_business().getTime().getTime()))); //WE CAN SET THIS IN THE PREPARED STMT
			//pstmt.setLong(30, leg.getNbr_lotid());//WE CAN SET THIS IN THE PREPARED STMT
			pstmt.setString(31, "S");// FLG_SRCTGTTYP
			pstmt.setString(32, "N");// FLG_frcdr
			pstmt.setString(33, leg.getFlg_status1());// FLG_STATUS1
			pstmt.setDate(34,LMSUtility.truncSqlDate( new java.sql.Date(postingDate.getTime())));// DAT_POSTING
			//get the dates from HashMap only for Posting
			Date settlementDate = null;
			Date creditDelay = null;
			Date debitDelay = null;
			if("P".equals(leg.flg_status1)){
				creditDelay = (Date)hmFields.get("DAT_POST_CREDITDELAY");
				settlementDate = (Date)hmFields.get("DAT_POST_SETTLEMENT");
				debitDelay = (Date)hmFields.get("DAT_POST_DEBITDELAY");
				//BVT_TXN Starts here
				sendingDate=(Date)hmFields.get("DAT_SENT");
				//BVT_TXN Ends here
				logger.debug("pstmt.setDate(35,LMSUtility.truncSqlDate( settlementDate()));// "+settlementDate);
				logger.debug("pstmt.setDate(36,LMSUtility.truncSqlDate( creditDelay));//  "+creditDelay);
				logger.debug("pstmt.setDate(37,LMSUtility.truncSqlDate( debitDelay));"+debitDelay);
				//BVT_TXN Starts here
				logger.debug("pstmt.setDate(40,LMSUtility.truncSqlDate( sendingDate));"+sendingDate);
				//BVT_TXN Ends here
				postingEntries[ExecutionConstants.OLM_ACCOUNTINGENTRY_INSERT] = true;
			}else{
				accrualEntries[ExecutionConstants.OLM_ACCOUNTINGENTRY_INSERT] = true;
			}

			pstmt.setDate(35,LMSUtility.truncSqlDate(settlementDate==null?null:new java.sql.Date(settlementDate.getTime())));// DAT_POST_SETTLEMENT
			pstmt.setDate(36,LMSUtility.truncSqlDate(creditDelay==null?null:new java.sql.Date(creditDelay.getTime())));// DAT_POST_CREDITDELAY
			pstmt.setDate(37,LMSUtility.truncSqlDate(debitDelay==null?null:new java.sql.Date(debitDelay.getTime())));//DAT_POST_DEBITDELAY
			pstmt.setString(38, (hmFields.containsKey("POOL_FLG_CONSOLIDATION")?(String)hmFields.get("POOL_FLG_CONSOLIDATION"):""));//COD_FIELD1 INT_1002
			pstmt.setString(39, null);// host system ref
			//BVT_TXN Starts here
			pstmt.setDate(40,LMSUtility.truncSqlDate(new java.sql.Date(sendingDate.getTime())));//DAT_SENT
			//BVT_TXN Ends here
			pstmt.addBatch();

			if (logger.isDebugEnabled()) {
			  logger.debug(
				  "Set all the values in the prepared statement ");
			}

		}//end for AELegs

			return accountingEntryId;
	}


	/** ****** JTEST FIX start ************  on 14DEC09*/
//  private AccountingDO getAccount(String central) throws LMSException{//INT_1008
//	  Connection conn = null;
	  
	 private AccountingDO getAccount(String central, Connection conn) throws LMSException{//INT_1008
	/** ****** JTEST FIX start ************  on 14DEC09*/
//	Statement stmt = null;
	PreparedStatement pStmt = null;
	/** ****** JTEST FIX end **************  on 14DEC09*/
	ResultSet rs = null;

	String sqlAcctDtls = null;
	String s_cod_branch = null;
	String s_nbr_coreaccount = null;
	//String s_nbr_ibanacct = null;

	String s_nbr_ownAccount = null;

	sqlAcctDtls = (new StringBuffer(" SELECT COD_CCY, ")
			.append(" COD_BNK_LGL,NBR_COREACCOUNT, COD_BRANCH,COD_GL,NBR_IBANACCNT,NBR_OWNACCOUNT")
			.append(" FROM ORBICASH_ACCOUNTS CENTRAL ")
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			.append(" WHERE CENTRAL.NBR_OWNACCOUNT =  ")
//			.append(central))
			
			.append(" WHERE CENTRAL.NBR_OWNACCOUNT = ? "))
			/** ****** JTEST FIX end **************  on 14DEC09*/
			.toString();
	try{
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		conn = LMSConnectionUtility.getConnection();
//		stmt = conn.createStatement();
//		rs = stmt.executeQuery(sqlAcctDtls);
		
		pStmt = conn.prepareStatement(sqlAcctDtls);
		pStmt.setString(1, central);
		rs = pStmt.executeQuery();
		/** ****** JTEST FIX end **************  on 14DEC09*/
		AccountingDO acc = new AccountingDO();

		if(rs.next()) {

			  acc.set_cod_bnk_lgl((String)rs.getString("COD_BNK_LGL"));
			  acc.set_cod_ccy((String)rs.getString("COD_CCY"));
			  acc.set_cod_gl((String)rs.getString("COD_GL"));

			  s_cod_branch = rs.getString("COD_BRANCH");

			  if (s_cod_branch == null || s_cod_branch.trim().equals("")) {
				  throw new LMSException("99999016", "W","COD Branch value is null or blank"+ s_cod_branch);
			  }
			  acc.set_cod_branch(s_cod_branch);

			  s_nbr_coreaccount = rs.getString("NBR_COREACCOUNT");
			  if (s_nbr_coreaccount == null || s_nbr_coreaccount.trim().equals("")) {
				  throw new LMSException("99999016", "W","Core Account value is null or blank"+ s_nbr_coreaccount);
			  }
			  acc.set_nbr_coreaccount(s_nbr_coreaccount);
	          /*INT_1025 START
			  s_nbr_ibanacct = rs.getString("NBR_IBANACCNT");
			  if (s_nbr_ibanacct == null || s_nbr_ibanacct.trim().equals("")) {
				  throw new LMSException("99999016", "W","IBAN Account value is null or blank"+ s_nbr_ibanacct);
			  }
			  acc.set_nbr_ibanacct(s_nbr_ibanacct);
			  INT_1025 END*/
			  s_nbr_ownAccount = rs.getString("NBR_OWNACCOUNT");
			  if (s_nbr_ownAccount == null || s_nbr_ownAccount.trim().equals("")) {
				  throw new LMSException("99999016", "W","NBR_OWNACCOUNT value is null or blank"+ s_nbr_ownAccount);
			  }
			  acc.set_nbr_ownaccount(s_nbr_ownAccount);

		  }else{
				logger.fatal(" No records for Internal Accounts  for query " + sqlAcctDtls);
				throw new LMSException("99999016", "W","No records for Internal Account");
		  }//if resultset
		  return acc;
		  /** ****** FindBugs Fix Start ***********  on 14DEC09*/
//	}catch(Exception ex){
	}catch(SQLException ex){
		logger.fatal("generateLegs: ", ex);
		throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, ex.getMessage());
		  /** ****** FindBugs Fix End ***********  on 14DEC09*/	
		}finally{
			LMSConnectionUtility.close(rs);
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			LMSConnectionUtility.close(stmt);
			LMSConnectionUtility.close(pStmt);
//			LMSConnectionUtility.close(conn);
			/** ****** JTEST FIX end **************  on 14DEC09*/
		}//end try
	}



	private HashMap getInternalAccountBLE(String creditType,String debitType, HashMap hm) throws LMSException{

	Connection conn = null;
	/** ****** JTEST FIX start ************  on 14DEC09*/
//	Statement stmt = null;
	PreparedStatement pStmt = null;
	/** ****** JTEST FIX end **************  on 14DEC09*/
	ResultSet rs = null;
	HashMap hmInternalAccts = new HashMap();
	String sqlAcctDtls = null;
	String s_cod_branch = null;
	String s_nbr_coreaccount = null;
	String s_nbr_ibanacct = null;
	String s_accountType = null;
	String s_nbr_ownAccount = null;

	sqlAcctDtls = (new StringBuffer(" SELECT   'PAYABLES' ACCOUNT_TYPE,")
			.append(" COD_CCY,COD_BNK_LGL,NBR_COREACCOUNT, COD_BRANCH,COD_GL,NBR_IBANACCNT,NBR_OWNACCOUNT")
			.append(" FROM ORBICASH_ACCOUNTS PAYABLES ")
			.append(" WHERE PAYABLES.NBR_OWNACCOUNT = 		  ")
			.append(" (SELECT ")
			.append("    ACCT_RFI ")
			.append(" FROM OPOOL_BLELINK BLELINK")
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			.append(" WHERE BLELINK.COD_BNK_LGL = '" + hm.get(ExecutionConstants.COD_BNK_LGL)+"'")
//			.append(" AND	  BLELINK.COD_CCY = '" + hm.get(ExecutionConstants.COD_CCY)+ "'")
//			.append(" AND	  BLELINK.COD_GL = '" + hm.get(ExecutionConstants.COD_GL) + "'")
			
			.append(" WHERE BLELINK.COD_BNK_LGL = ? ")
			.append(" AND	  BLELINK.COD_CCY = ? ")
			.append(" AND	  BLELINK.COD_GL = ? ")
			/** ****** JTEST FIX end **************  on 14DEC09*/
			.append(" AND	  BLELINK.FLG_STATUS = 'ACTIVE')")
			.append(" UNION")
			.append(" SELECT  'EXPENSES',")
			.append(" COD_CCY,COD_BNK_LGL,NBR_COREACCOUNT, COD_BRANCH,COD_GL,NBR_IBANACCNT,NBR_OWNACCOUNT")
			.append(" FROM ORBICASH_ACCOUNTS EXPENSES  ")
			.append(" WHERE EXPENSES.NBR_OWNACCOUNT = 		  ")
			.append(" (SELECT ")
			.append("    ACCT_EXPENSE  ")
			.append(" FROM OPOOL_BLELINK BLELINK")
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			.append(" WHERE BLELINK.COD_BNK_LGL = '" + hm.get(ExecutionConstants.COD_BNK_LGL)+"'")
//			.append(" AND	  BLELINK.COD_CCY = '" + hm.get(ExecutionConstants.COD_CCY)+ "'")
//			.append(" AND	  BLELINK.COD_GL = '" + hm.get(ExecutionConstants.COD_GL) + "'")
			
			.append(" WHERE BLELINK.COD_BNK_LGL = ? ")
			.append(" AND	  BLELINK.COD_CCY = ? ")
			.append(" AND	  BLELINK.COD_GL = ? ")
			/** ****** JTEST FIX end **************  on 14DEC09*/
			.append(" AND	  BLELINK.FLG_STATUS = 'ACTIVE')")
			.append(" UNION")
			.append(" SELECT 'RECEIVABLES',")
			.append(" COD_CCY,COD_BNK_LGL,NBR_COREACCOUNT, COD_BRANCH,COD_GL,NBR_IBANACCNT,NBR_OWNACCOUNT")
			.append(" FROM ORBICASH_ACCOUNTS EXPENSES  ")
			.append(" WHERE EXPENSES.NBR_OWNACCOUNT = 		  ")
			.append(" (SELECT ")
			.append("    ACCT_IENC  ")
			.append(" FROM OPOOL_BLELINK BLELINK")
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			.append(" WHERE BLELINK.COD_BNK_LGL = '" + hm.get(ExecutionConstants.COD_BNK_LGL)+"'")
//			.append(" AND	  BLELINK.COD_CCY = '" + hm.get(ExecutionConstants.COD_CCY)+ "'")
//			.append(" AND	  BLELINK.COD_GL = '" + hm.get(ExecutionConstants.COD_GL) + "'")
			
			.append(" WHERE BLELINK.COD_BNK_LGL = ? ")
			.append(" AND	  BLELINK.COD_CCY = ? ")
			.append(" AND	  BLELINK.COD_GL = ? ")
			/** ****** JTEST FIX end **************  on 14DEC09*/
			.append(" AND	  BLELINK.FLG_STATUS = 'ACTIVE')")
			.append(" UNION")
			.append(" SELECT 'INCOME',")
			.append(" COD_CCY,COD_BNK_LGL,NBR_COREACCOUNT, COD_BRANCH,COD_GL,NBR_IBANACCNT,NBR_OWNACCOUNT")
			.append(" FROM ORBICASH_ACCOUNTS EXPENSES  ")
			.append(" WHERE EXPENSES.NBR_OWNACCOUNT = 		  ")
			.append(" (SELECT ")
			.append("    ACCT_INCOME  ")
			.append(" FROM OPOOL_BLELINK BLELINK")
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			.append(" WHERE BLELINK.COD_BNK_LGL = '" + hm.get(ExecutionConstants.COD_BNK_LGL)+"'")
//			.append(" AND	  BLELINK.COD_CCY = '" + hm.get(ExecutionConstants.COD_CCY)+ "'")
//			.append(" AND	  BLELINK.COD_GL = '" + hm.get(ExecutionConstants.COD_GL) + "'")
			
			.append(" WHERE BLELINK.COD_BNK_LGL = ? ")
			.append(" AND	  BLELINK.COD_CCY = ? ")
			.append(" AND	  BLELINK.COD_GL = ? ")
			/** ****** JTEST FIX end **************  on 14DEC09*/
			.append(" AND	  BLELINK.FLG_STATUS = 'ACTIVE')")).toString();

	if (logger.isDebugEnabled())
		logger.debug("SELECT QUERY IS : " + sqlAcctDtls);

	try{
		conn = LMSConnectionUtility.getConnection();
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		stmt = conn.createStatement();
//		rs = stmt.executeQuery(sqlAcctDtls);
		
		pStmt = conn.prepareStatement(sqlAcctDtls);
		pStmt.setObject(1, hm.get(ExecutionConstants.COD_BNK_LGL));
		pStmt.setObject(2, hm.get(ExecutionConstants.COD_CCY));
		pStmt.setObject(3, hm.get(ExecutionConstants.COD_GL));
		
		pStmt.setObject(4, hm.get(ExecutionConstants.COD_BNK_LGL));
		pStmt.setObject(5, hm.get(ExecutionConstants.COD_CCY));
		pStmt.setObject(6, hm.get(ExecutionConstants.COD_GL));
		
		pStmt.setObject(7, hm.get(ExecutionConstants.COD_BNK_LGL));
		pStmt.setObject(8, hm.get(ExecutionConstants.COD_CCY));
		pStmt.setObject(9, hm.get(ExecutionConstants.COD_GL));
		
		pStmt.setObject(10, hm.get(ExecutionConstants.COD_BNK_LGL));
		pStmt.setObject(11, hm.get(ExecutionConstants.COD_CCY));
		pStmt.setObject(12, hm.get(ExecutionConstants.COD_GL));
		
		rs = pStmt.executeQuery();
		/** ****** JTEST FIX end **************  on 14DEC09*/

		  boolean results = false;
		  while(rs.next()) {
			s_accountType = "";
			s_accountType = rs.getString("ACCOUNT_TYPE");
			results = true;
			//SUP_1082b starts
			//Get all the accounts. Not just the debit and credit type of accounts.
			//if(s_accountType.equalsIgnoreCase(debitType) || s_accountType.equalsIgnoreCase(creditType))
			//{
			//SUP_1082b ends
				AccountingDO acc = new AccountingDO();
				acc.set_cod_bnk_lgl((String)hm.get(ExecutionConstants.COD_BNK_LGL));
				acc.set_cod_ccy((String)hm.get(ExecutionConstants.COD_CCY));
				acc.set_cod_gl((String)hm.get(ExecutionConstants.COD_GL));

				s_cod_branch = rs.getString("COD_BRANCH");
				//15.1.1_Resource_Leaks_Fix start
				//Commenting for 15.2_JTest_ResultSet_Loop as it is in a Loop
				/*
				LMSConnectionUtility.close(rs);
				LMSConnectionUtility.close(pStmt);
				LMSConnectionUtility.close(conn);
				*/								
				//15.1.1_Resource_Leaks_Fix end
				if (s_cod_branch == null || s_cod_branch.trim().equals("")) {					
					LMSConnectionUtility.close(rs);		//15.1.1_Resource_Leaks_Fix			 
					throw new LMSException("99999016", "W","COD Branch value is null or blank"+ s_cod_branch);
				}
				acc.set_cod_branch(s_cod_branch);

				s_nbr_coreaccount = rs.getString("NBR_COREACCOUNT");
				if (s_nbr_coreaccount == null || s_nbr_coreaccount.trim().equals("")) {
					LMSConnectionUtility.close(rs); //15.1.1_Resource_Leaks_Fix
					throw new LMSException("99999016", "W","Core Account value is null or blank"+ s_nbr_coreaccount);
				}
				acc.set_nbr_coreaccount(s_nbr_coreaccount);
	          	/*INT_1025 START
				s_nbr_ibanacct = rs.getString("NBR_IBANACCNT");
				if (s_nbr_ibanacct == null || s_nbr_ibanacct.trim().equals("")) {
					throw new LMSException("99999016", "W","IBAN Account value is null or blank"+ s_nbr_ibanacct);
				}
				acc.set_nbr_ibanacct(s_nbr_ibanacct);
				INT_1025 END*/
				s_nbr_ownAccount = rs.getString("NBR_OWNACCOUNT");
				if (s_nbr_ownAccount == null || s_nbr_ownAccount.trim().equals("")) {
					LMSConnectionUtility.close(rs); //15.1.1_Resource_Leaks_Fix
					throw new LMSException("99999016", "W","NBR_OWNACCOUNT  value is null or blank"+ s_nbr_ownAccount);
				}
				acc.set_nbr_ownaccount(s_nbr_ownAccount);

				//arrlstAcctDO.add(acc);
				hmInternalAccts.put(s_accountType,acc);
				//SUP_1082b starts
				//}
				//SUP_1082b ends

		  }
			if (!results){
				logger.fatal(" No records for Internal Accounts for Credit Leg"
						 +  creditType + "debit Leg " +debitType + "and query " + sqlAcctDtls);
				throw new LMSException("99999016", "W","No records for Internal Account");
			}

	}
	/** ****** FindBugs Fix Start ***********  on 14DEC09*/
	catch(SQLException ex)
	{
		logger.fatal("generateLegs: ", ex);
		throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, ex.getMessage());
	}
	catch(NamingException ex)
	{
		logger.fatal("generateLegs: ", ex);
		throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, ex.getMessage());
	}
	/** ****** FindBugs Fix End ***********  on 14DEC09*/
	finally
	{
		LMSConnectionUtility.close(rs);
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		LMSConnectionUtility.close(stmt);
		LMSConnectionUtility.close(pStmt);
		/** ****** JTEST FIX end **************  on 14DEC09*/
		LMSConnectionUtility.close(conn);
	}

	return hmInternalAccts;
}

	/**
	 *
	 * @param account
	 * @param hm
	 * @throws LMSException
	 */
	private void checkAccountingDOVals(AccountingDO account, HashMap hm) throws LMSException {

		   String s_cod_ccy = account.get_cod_ccy();
		   String s_cod_bnk_lgl = account.get_cod_bnk_lgl();
		   String s_cod_gl = account.get_cod_gl();

			if (s_cod_ccy == null || s_cod_ccy.trim().equals("")) {
				throw new LMSException("99999016", "W","Cod Currency value is null or blank"+ s_cod_ccy);
			}
			else if (s_cod_bnk_lgl == null || s_cod_ccy.trim().equals("")) {
				throw new LMSException("99999016", "W","Cod Currency value is null or blank"+ s_cod_bnk_lgl);
			}

			else if (s_cod_gl  == null || s_cod_ccy.trim().equals("")) {
				throw new LMSException("99999016", "W","Cod Currency value is null or blank"+ s_cod_gl);
			}
			else
			{
				hm.put(ExecutionConstants.COD_CCY,s_cod_ccy);
				hm.put(ExecutionConstants.COD_BNK_LGL,s_cod_bnk_lgl);
				hm.put(ExecutionConstants.COD_GL,s_cod_gl);
			}

		}


/**
 * @param id
 * @param bufPoolList
 */
/** ****** FindBugs Fix Start ***********  on 14DEC09*/
	/*private void updatePoolList(String poolId, HashMap hpPoolList)
	{
		hpPoolList.put(poolId, poolId);
	}*/
/** ****** FindBugs Fix End ***********  on 14DEC09*/
	
/**
 * @param pstmtTxnPoolPartyDtls
 * @param entryId
 * @param id
 * @param businessdate
 * @param _nbr_ownaccount
 * @throws SQLException
 * @throws LMSException
 */

	private void updateInterest(PreparedStatement pstmtTxnPoolPartyDtls, String acctEntryId, String poolId, String nbr_ownaccount,String type) throws LMSException {

	try {

		if (logger.isDebugEnabled()) {
			logger.debug("pstmtTxnPoolPartyDtls.setString(1,entryId); //  NBR_ACCTENTRYID"+acctEntryId);
			logger.debug("pstmtTxnPoolPartyDtls.setString(3,poolId); //" +poolId);
			logger.debug("pstmtTxnPoolPartyDtls.setString(2,nbr_ownaccount) "+nbr_ownaccount);
		}

		pstmtTxnPoolPartyDtls.setString(1,acctEntryId);
		pstmtTxnPoolPartyDtls.setString(2, nbr_ownaccount);
		pstmtTxnPoolPartyDtls.setString(3, poolId);


		pstmtTxnPoolPartyDtls.addBatch();
		if(ExecutionConstants.ACCRUAL.equals(type)){
			accrualEntries[ExecutionConstants.PARTY_DTLS_UPDATE] = true;
		}else{
			postingEntries[ExecutionConstants.PARTY_DTLS_UPDATE] = true;
		}



	} catch (SQLException e) {
		logger.fatal(
			  "SQL Exception in updating the PartyDetails Table", e);
	//BVT_TXN Starts here
			//e.printStackTrace();
		  throw new LMSException("99024", e.toString());
		//e.printStackTrace();
	}
	catch (Exception e) {
		logger.fatal(
			  "SQL Exception in updating the PartyDetails Table", e);
			//e.printStackTrace();
	//BVT_TXN Ends here
		  throw new LMSException("99024", e.toString());
		//e.printStackTrace();
	}
	//BVT_TXN Starts here
	if (logger.isDebugEnabled()) {
		logger.debug("Leaving ");
	}
	//BVT_TXN ends here
}

/**
 * @param pstmtTxnPoolAllocation
 * @param entryId
 * @param id
 * @throws LMSException
 */
/** ****** FindBugs Fix Start ***********  on 14DEC09*/
	/*private void updateAllocationAccrual(PreparedStatement pstmtTxnPoolAllocation, String entryId, String poolId, String flagAllocation) throws LMSException
{
	try {

		if (logger.isDebugEnabled()) {
			logger.debug("pstmtTxnPoolAllocation.setString(1,entryId); //  NBR_ACCTENTRYID"+entryId);
			logger.debug("pstmtTxnPoolAllocation.setString(2,poolId); //  NBR_LEGID" +poolId);
			logger.debug("pstmtTxnPoolAllocation.setString(5,flagAllocation) //  set the pool id here "+flagAllocation);
		}
		pstmtTxnPoolAllocation.setString(1,entryId);
		pstmtTxnPoolAllocation.setString(2,poolId);
		pstmtTxnPoolAllocation.setString(5,flagAllocation);
		pstmtTxnPoolAllocation.addBatch();

	} catch (SQLException e) {
		logger.fatal(
				  "SQL Exception in updating the Allocation Table", e);
			  throw new LMSException("99024", e.toString());
			//e.printStackTrace();
	}

		}*/
/** ****** FindBugs Fix End ***********  on 14DEC09*/


/**Created Method for INT_1002 for passing Accrual and Posting entries of Bank Share and Treasury Share
 *
 * @param pstmt
 * @param pstmtAcctUnpostedInsert
 * @param businessDate
 * @param strbusinessdate
 * @param type
 * @param generateFor
 * @param generateForId
 * @param client
 * @throws LMSException
 */
	private void generateBankShareTreasuryShareEntries(PreparedStatement pstmt,PreparedStatement pstmtAcctUnpostedInsert,Date businessDate,
				String strbusinessdate,String type,String generateFor,String generateForId,String client) throws LMSException{

		/** ****** JTEST FIX start ************  on 14DEC09*/
//		Statement stmt = null;
		PreparedStatement pStmt = null;
		/** ****** JTEST FIX end **************  on 14DEC09*/
		ResultSet rs=null;
		Connection conn = null;
		AccountingDO acctDoObj = null;
		BigDecimal amountToPost	= null;
		String acctShare = "";
		String accountingEntryId = "";
		String shareType = "";
		HashMap hmFields = new HashMap();
		String poolId = "";
		boolean results = false;
		try {
			if (ExecutionConstants.ACCRUAL.equals(type)){
				StringBuilder query = new StringBuilder("SELECT SUM(AMT_BANKSHARE_ADV) SUM_AMT, ")
				.append(" TXN.ID_POOL  ,ACCT_BANKSHARE ACCT, 'BANKSHARE' SHARE_TYPE")
				//.append(",DAT_ADJUSTEDACCRUAL ")//SUP_1062
				.append(",DAT_ADJUSTEDACCRUAL, TXN.NBR_PROCESSID NBR_PROCESSID ")//SUP_1062 // Mashreq_AE_Hook
				.append(" FROM OPOOL_TXNPOOLHDR TXN,OPOOL_BLELINK BLELINK ")
				//.append(" WHERE TXN.COD_GL = '" + client +"'") //INT_1009
				//.append(" AND (AMT_BANKSHARE_ADV IS NOT NULL) ")
				.append(" WHERE (AMT_BANKSHARE_ADV IS NOT NULL) ")
				.append(" AND TXN.FLG_REALPOOL = 'Y' ")
				.append(" AND TXN.DAT_BUSINESS_POOLRUN BETWEEN  NVL(TXN.DAT_LASTACCRUALCALC,TXN.DAT_START-1)+1 AND  TXN.DAT_ACCRUAL ")//SUP_1062
				.append(" AND TXN.TXT_STATUS = 'ALLOCATED'    ")
				.append(" AND BLELINK.COD_BNK_LGL = TXN.COD_BASE_BNK_LGL ")
				.append(" AND BLELINK.COD_CCY = TXN.COD_BASE_CCY_POOL ")
				.append(" AND	TXN.ID_BVT = (SELECT MAX(TO_NUMBER(M.ID_BVT)) ")//INT_1026
				.append("			   	  FROM 	OPOOL_TXNPOOLHDR M  ")
				.append("	 			  WHERE M.ID_POOL = TXN.ID_POOL ")
				.append(" 				  AND M.COD_GL = TXN.COD_GL ")
				.append("				  AND M.DAT_BUSINESS_POOLRUN = TXN.DAT_BUSINESS_POOLRUN ) ");
				if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
					query.append(" AND TXN.ID_POOL IN (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR LOT ");
					/** ****** JTEST FIX start ************  on 14DEC09*/
//					query.append(" WHERE LOT.NBR_LOTID =" + generateForId+") ");
					query.append(" WHERE LOT.NBR_LOTID =?) ");
					/** ****** JTEST FIX end **************  on 14DEC09*/
				}
				query.append(" AND TXN.COD_ALLOCATIONSUM = 'A'  ")
				.append(" AND TXN.DAT_ACCRUAL = '"+strbusinessdate+"' ")//SUP_1062
				.append(" GROUP BY  ID_POOL , ACCT_BANKSHARE ")
				//.append(",DAT_ADJUSTEDACCRUAL ")//SUP_1062
				.append(",DAT_ADJUSTEDACCRUAL, NBR_PROCESSID ")//SUP_1062 // Mashreq_AE_Hook
				.append(" UNION ")
				.append(" SELECT SUM(NVL(AMT_TREASURY_ADV,0)) SUM_AMT, TXN.ID_POOL, ")
				.append(" (CASE WHEN (SUM(NVL(AMT_TREASURY_ADV,0))) >= 0 THEN ACCT_TREASURY_CR ELSE ACCT_TREASURY_DR END) ACCT, ")
				.append(" 'TREASURYSHARE' SHARE_TYPE ")
				//.append(",DAT_ADJUSTEDACCRUAL ")//SUP_1062
				.append(",DAT_ADJUSTEDACCRUAL, TXN.NBR_PROCESSID NBR_PROCESSID ")//SUP_1062 // Mashreq_AE_Hook
				.append(" FROM OPOOL_TXNPOOLHDR TXN ")
				//.append(" WHERE TXN.COD_GL = '" + client + "'")	//INT_1009
				.append(" WHERE TXN.TXT_STATUS = 'ALLOCATED'  ")
				.append(" AND TXN.FLG_REALPOOL = 'Y' ")
				.append(" AND AMT_TREASURY_ADV IS NOT NULL ")
				.append(" AND TXN.DAT_BUSINESS_POOLRUN BETWEEN  NVL(TXN.DAT_LASTACCRUALCALC,TXN.DAT_START-1)+1 AND  TXN.DAT_ACCRUAL ")//SUP_1062
				.append(" AND TXN.ID_BVT = (SELECT MAX(TO_NUMBER(M.ID_BVT)) ")//INT_1026
				.append("			   	  FROM 	OPOOL_TXNPOOLHDR M  ")
				.append("	 			  WHERE M.ID_POOL = TXN.ID_POOL ")
				.append(" 				  AND M.COD_GL = TXN.COD_GL ")
				.append("				  AND M.DAT_BUSINESS_POOLRUN = TXN.DAT_BUSINESS_POOLRUN ) ");
				if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
					query.append(" AND TXN.ID_POOL IN (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR LOT ");
					/** ****** JTEST FIX start ************  on 14DEC09*/
//					query.append(" WHERE LOT.NBR_LOTID =" + generateForId+") ");
					query.append(" WHERE LOT.NBR_LOTID =?) ");
					/** ****** JTEST FIX end **************  on 14DEC09*/
				}
				query.append(" AND TXN.DAT_ACCRUAL = '" +strbusinessdate +"'")//SUP_1062
				.append(" GROUP BY  ID_POOL ,ACCT_TREASURY_CR, ACCT_TREASURY_DR")
				//.append(",DAT_ADJUSTEDACCRUAL ");//SUP_1062
				.append(",DAT_ADJUSTEDACCRUAL, NBR_PROCESSID ");//SUP_1062 // Mashreq_AE_Hook
				if (logger.isDebugEnabled()) {
					logger.debug("Bank Share Treasury Share ");
				}
				//EXECUTE BANKSHARE TREASURY SHARE QUERY
				//FOR ACCRUAL
				if (logger.isDebugEnabled()){
					logger.debug("Executing Query Bank Share Treasury Accrual" + query);
				}

				conn = LMSConnectionUtility.getConnection();
				/** ****** JTEST FIX start ************  on 14DEC09*/
//				stmt = conn.createStatement();
//				rs = stmt.executeQuery(query.toString());

				pStmt = conn.prepareStatement(query.toString());
				if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
					pStmt.setString(1, generateForId);
					pStmt.setString(2, generateForId);
				}
				rs = pStmt.executeQuery();
				/** ****** JTEST FIX end **************  on 14DEC09*/
				hmFields.put(ExecutionConstants.BUSINESSDATE,businessDate); // Mashreq_AE_Hook fix
				if (logger.isDebugEnabled()) {
					logger.debug(" Executed Query BANKSHARE ");
				}

				while(rs.next()){
					results = true;
					acctDoObj = new AccountingDO();
					accountingEntryId = "0";
					amountToPost = rs.getBigDecimal("SUM_AMT");
					shareType = rs.getString("SHARE_TYPE");
					acctShare = rs.getString("ACCT");
					poolId = rs.getString("ID_POOL");
					hmFields.put(ExecutionConstants.PROCESSID, rs.getLong("NBR_PROCESSID")); // Mashreq_AE_Hook
					//hmFields.put("DAT_POSTING",businessDate); THIS SHOULD BE ADJUSTEDACCRUAL
					hmFields.put("DAT_POSTING",(Date)rs.getDate("DAT_ADJUSTEDACCRUAL"));//SUP_1062
					//BVT_TXN Inc Starts
					hmFields.put("DAT_SENT",businessDate);
					//BVT_TXN Inc Ends
					if (amountToPost.compareTo(zero) != 0 && !(acctShare == null || "".equals(acctShare))){
						/** ****** JTEST FIX start ************  on 14DEC09*/
//						acctDoObj = getAccount(acctShare);
						acctDoObj = getAccount(acctShare, conn);
						/** ****** JTEST FIX end **************  on 14DEC09*/
						accountingEntryId =  ""+processEntry(acctDoObj,
							amountToPost,type,ExecutionConstants.NET,
							/** ****** JTEST FIX start ************  on 14DEC09*/
//							shareType,pstmt,pstmtAcctUnpostedInsert,poolId,hmFields);//INT_1039
							shareType,pstmt,pstmtAcctUnpostedInsert,poolId,hmFields, conn);//INT_1039
							/** ****** JTEST FIX end **************  on 14DEC09*/
					}else{
						logger.fatal("Bank Share /Treasury Share Amt is 0 for"  + acctDoObj + "SO not accruing the amount ");
					}

					if (logger.isDebugEnabled()) {
						logger.debug("Bank Share NBR_ACCTENTRYID"+accountingEntryId);
					}

				}
				if (!results){
					logger.fatal(" No records for BankShare Accrual" + query.toString());
				}
				rs.close();
			}
			//POSTING
			if (ExecutionConstants.POSTING.equals(type)){
				StringBuilder queryPosting = new StringBuilder("SELECT SUM(AMT_BANKSHARE_ADV) SUM_AMT, ")
						// INT_1055 Start
						.append(" TXN.ID_POOL  ,ACCT_BANKSHARE ACCT, 'BANKSHARE' SHARE_TYPE, DAT_POSTING, DAT_POST_SETTLEMENT,TXN.DAT_POST_CREDITDELAY ,   TXN.DAT_POST_DEBITDELAY")
						// INT_1055 End
						.append(", TXN.NBR_PROCESSID NBR_PROCESSID ") // Mashreq_AE_Hook
						.append(" FROM OPOOL_TXNPOOLHDR TXN,OPOOL_BLELINK BLELINK ")
						//.append(" WHERE TXN.COD_GL = '" + client +"'")//INT_1009
						.append(" WHERE (AMT_BANKSHARE_ADV IS NOT NULL) ")
						.append(" AND TXN.FLG_REALPOOL = 'Y' ")
						.append(" AND TXN.DAT_BUSINESS_POOLRUN BETWEEN  NVL(TXN.DAT_LASTPOSTINGCALC,TXN.DAT_START-1)+1 AND  TXN.DAT_ADJUSTEDPOSTING ")
						.append(" AND TXN.TXT_STATUS = 'ACCRUED' ")
						.append(" AND BLELINK.COD_BNK_LGL = TXN.COD_BASE_BNK_LGL ")
						.append(" AND BLELINK.COD_CCY = TXN.COD_BASE_CCY_POOL ")
						.append(" AND	TXN.ID_BVT = (SELECT MAX(TO_NUMBER(M.ID_BVT)) ")//INT_1026
						.append("			   	  FROM 	OPOOL_TXNPOOLHDR M  ")
						.append("	 			  WHERE M.ID_POOL = TXN.ID_POOL ")
						.append(" 				  AND M.COD_GL = TXN.COD_GL ")
						.append("				  AND M.DAT_BUSINESS_POOLRUN = TXN.DAT_BUSINESS_POOLRUN ) ");
						if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
							queryPosting.append(" AND TXN.ID_POOL IN (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR LOT ");
							queryPosting.append(" WHERE LOT.NBR_LOTID =" + generateForId+") ");
						}
						queryPosting.append(" AND TXN.COD_ALLOCATIONSUM = 'A'  ")
						//.append(" AND TXN.DAT_POST_SETTLEMENT = '"+strbusinessdate+"' ") // SUP_1073: Commented
						.append(" AND TXN.DAT_POSTING = '"+strbusinessdate+"' ") // SUP_1073: Added
						// INT_1055 Start
						.append(" GROUP BY  ID_POOL , ACCT_BANKSHARE, DAT_POSTING, DAT_POST_SETTLEMENT,TXN.DAT_POST_CREDITDELAY ,   TXN.DAT_POST_DEBITDELAY ")
						.append(", NBR_PROCESSID ") // Mashreq_AE_Hook
						// INT_1055 End
						.append(" UNION ")
						.append(" SELECT SUM(NVL(AMT_TREASURY_ADV,0)) SUM_AMT, TXN.ID_POOL, ")
						.append(" (CASE WHEN (SUM(NVL(AMT_TREASURY_ADV,0))) >= 0 THEN ACCT_TREASURY_CR ELSE ACCT_TREASURY_DR END) ACCT, ")
						// INT_1055 Start
						.append(" 'TREASURYSHARE' SHARE_TYPE,DAT_POSTING, DAT_POST_SETTLEMENT,TXN.DAT_POST_CREDITDELAY ,   TXN.DAT_POST_DEBITDELAY ")
						// INT_1055 End
						.append(", TXN.NBR_PROCESSID NBR_PROCESSID ") // Mashreq_AE_Hook
						.append(" FROM OPOOL_TXNPOOLHDR TXN ")
						//.append(" WHERE TXN.COD_GL = '" + client + "'") INT_1009
						.append(" WHERE ")
						.append(" TXN.TXT_STATUS = 'ACCRUED'  ")
						.append(" AND TXN.FLG_REALPOOL = 'Y' ")
						.append(" AND AMT_TREASURY_ADV IS NOT NULL ")
						.append(" AND TXN.DAT_BUSINESS_POOLRUN BETWEEN  NVL(TXN.DAT_LASTPOSTINGCALC,TXN.DAT_START-1)+1 AND  TXN.DAT_ADJUSTEDPOSTING  ")
						.append(" AND TXN.ID_BVT = (SELECT MAX(TO_NUMBER(M.ID_BVT)) ")//INT_1026
						.append("			   	  FROM 	OPOOL_TXNPOOLHDR M  ")
						.append("	 			  WHERE M.ID_POOL = TXN.ID_POOL ")
						.append(" 				  AND M.COD_GL = TXN.COD_GL ")
						.append("				  AND M.DAT_BUSINESS_POOLRUN = TXN.DAT_BUSINESS_POOLRUN ) ");
						if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
							queryPosting.append(" AND TXN.ID_POOL IN (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR LOT ");
							queryPosting.append(" WHERE LOT.NBR_LOTID =" + generateForId+") ");
						}
						//queryPosting.append(" AND TXN.DAT_POST_SETTLEMENT = '" +strbusinessdate +"'") // SUP_1073: Commented
						queryPosting.append(" AND TXN.DAT_POSTING = '" +strbusinessdate +"'") // SUP_1073: Added
						// INT_1055 Start
						//.append(" GROUP BY  ID_POOL ,ACCT_TREASURY_CR, ACCT_TREASURY_DR,DAT_POSTING, DAT_POST_SETTLEMENT,TXN.DAT_POST_CREDITDELAY ,TXN.DAT_POST_DEBITDELAY");
						.append(" GROUP BY  ID_POOL ,ACCT_TREASURY_CR, ACCT_TREASURY_DR,DAT_POSTING, DAT_POST_SETTLEMENT,TXN.DAT_POST_CREDITDELAY ,TXN.DAT_POST_DEBITDELAY, NBR_PROCESSID"); //Mashreq_AE_Hook
						// INT_1055 End

						if (logger.isDebugEnabled()) {
							logger.debug("Bank Share Treasury Share for POSTING");
						}
						//EXECUTE BANKSHARE TREASURY SHARE QUERY
						//FOR ACCRUAL
						if (logger.isDebugEnabled()){
							logger.debug("Executing Query Bank Share Treasury Posting" + queryPosting);
						}

						conn = LMSConnectionUtility.getConnection();
						/** ****** JTEST FIX start ************  on 14DEC09*/
//						stmt = conn.createStatement();
//						rs = stmt.executeQuery(queryPosting.toString());
						LMSConnectionUtility.close(pStmt); //15.1.1_Resource_Leaks_Fix
						pStmt = conn.prepareStatement(queryPosting.toString());
						if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
							pStmt.setString(1, generateForId);
							pStmt.setString(2, generateForId);
						}
						rs = pStmt.executeQuery();
						/** ****** JTEST FIX end **************  on 14DEC09*/

						if (logger.isDebugEnabled()) {
							logger.debug(" Executed Query BANKSHARE POSTING");
						}
						hmFields = new HashMap();
						hmFields.put(ExecutionConstants.BUSINESSDATE,businessDate); // Mashreq_AE_Hook fix
						poolId = "";
						Date postingDate = null;
						// INT_1055 Start
						Date valueDate = null;
						// INT_1055 End
						results = false;

						while(rs.next()){
							results = true;
							acctDoObj = new AccountingDO();
							accountingEntryId = "0";
							hmFields.put(ExecutionConstants.PROCESSID, rs.getLong("NBR_PROCESSID")); // Mashreq_AE_Hook 
							amountToPost = rs.getBigDecimal("SUM_AMT");
							shareType = rs.getString("SHARE_TYPE");
							acctShare = rs.getString("ACCT");
							poolId = rs.getString("ID_POOL");
							hmFields.put("DAT_POST_SETTLEMENT",(Date)rs.getDate("DAT_POST_SETTLEMENT"));
							hmFields.put("DAT_POST_CREDITDELAY",(Date)rs.getDate("DAT_POST_CREDITDELAY"));
							hmFields.put("DAT_POST_DEBITDELAY",(Date)rs.getDate("DAT_POST_DEBITDELAY"));
							// INT_1055 Start
							hmFields.put("DAT_VALUE",(Date)rs.getDate("DAT_POSTING"));
							// INT_1055 End


							if (amountToPost.compareTo(zero) != 0 && !(acctShare == null || "".equals(acctShare))){
								if(amountToPost.compareTo(zero)>0){
									postingDate = (Date)hmFields.get("DAT_POST_CREDITDELAY");
								}else{
									postingDate = (Date)hmFields.get("DAT_POST_DEBITDELAY");
								}
								hmFields.put("DAT_POSTING",(Date)(postingDate!=null?postingDate:businessDate));
								//BVT_TXN Inc Starts
								// INT_1055 Start
								// hmFields.put("DAT_SENT",businessdate);
								valueDate = (Date)hmFields.get("DAT_VALUE");
								valueDate = (valueDate == null ? businessDate : addOffsetToDate(valueDate, 1));
								hmFields.put("DAT_SENT", valueDate);
								// INT_1055 End
								//BVT_TXN Inc Ends
								/** ****** JTEST FIX start ************  on 14DEC09*/
//								acctDoObj = getAccount(acctShare);
								acctDoObj = getAccount(acctShare, conn);
								/** ****** JTEST FIX end **************  on 14DEC09*/
								accountingEntryId =  ""+processEntry(acctDoObj,
									amountToPost,type,ExecutionConstants.NET,
									/** ****** JTEST FIX start ************  on 14DEC09*/
//									shareType,pstmt,pstmtAcctUnpostedInsert,poolId,hmFields);//INT_1039
									shareType,pstmt,pstmtAcctUnpostedInsert,poolId,hmFields, conn);//INT_1039
									/** ****** JTEST FIX end **************  on 14DEC09*/
							}else{
								logger.fatal("Bank Share /Treasury Share Amt Posting is 0 for"  + acctDoObj + "SO not pOSTING the amount ");
							}

							if (logger.isDebugEnabled()) {
								logger.debug("Bank Share Posting NBR_ACCTENTRYID"+accountingEntryId);
							}

						}
						if (!results){
							logger.fatal(" No records for BankShare Trasury Share Posting" + queryPosting.toString());
						}
						rs.close();
					}
			/** ****** FindBugs Fix Start ***********  on 14DEC09*/
		}catch(SQLException e){
			logger.fatal("generateAccrualEntries: ", e);
			//throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, e.getMessage());
		}catch(NamingException e){
			logger.fatal("generateAccrualEntries: ", e);
			//throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, e.getMessage());
			/** ****** FindBugs Fix End ***********  on 14DEC09*/
		}finally{
			LMSConnectionUtility.close(rs);
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			LMSConnectionUtility.close(stmt);
			LMSConnectionUtility.close(pStmt);
			/** ****** JTEST FIX end **************  on 14DEC09*/
			LMSConnectionUtility.close(conn);
		}
	}


	//BVT_TXN Starts here

	public boolean[] generateAccountingEntriesBVT(PreparedStatement pstmtAccountingEntry,PreparedStatement pstmtTxnPoolPartyDtls,
			PreparedStatement pstmtAccountUnposted,PreparedStatement pstmtAcctUnpostedInsert , String type,
			//SUP_1082c Starts
			//Date businessdate, SweepsTriggerDetails std )throws LMSException{
			//ENH_10007 Starts
			// BVT Adjsutmet Accounting Entries should be generated per host. 
			//Date businessdate, SweepsTriggerDetails std, BVTDetails bvtDetails )throws LMSException{
	    	Date businessdate, SweepsTriggerDetails std, BVTDetails bvtDetails,String hostId )throws LMSException{
	    	//ENH_10007 Ends
	    	//SUP_1082c ends
		boolean entries[] = null;
		if (logger.isDebugEnabled())
			logger.debug("Entering");

		try{
			if((ExecutionConstants.ACCRUAL).equalsIgnoreCase(type.trim())){

				generateAccrualEntriesBVT(pstmtAccountingEntry,
						pstmtTxnPoolPartyDtls,
						//SUP_1082c starts
						//pstmtAccountUnposted,pstmtAcctUnpostedInsert, businessdate, std );
						//ENH_10007 Starts
						//BVT Adjsutmet Accounting Entries should be generated per host.
						//pstmtAccountUnposted,pstmtAcctUnpostedInsert, businessdate, std, bvtDetails );
						pstmtAccountUnposted,pstmtAcctUnpostedInsert, businessdate, std, bvtDetails, hostId);
						//ENH_10007 Ends
						//SUP_1082c Ends
				entries= accrualEntries;

			}
			else if((ExecutionConstants.POSTING).equalsIgnoreCase(type.trim())){
				generatePostingEntriesBVT(pstmtAccountingEntry,
						pstmtTxnPoolPartyDtls, pstmtAccountUnposted,
						//ENH_10007 Starts
						// BVT Adjsutmet Accounting Entries should be generated per host.
						//pstmtAcctUnpostedInsert,businessdate,std );
						pstmtAcctUnpostedInsert,businessdate,std, hostId );
						//ENH_10007 Ends
				entries= postingEntries;
			}
		}
		/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//		catch(Exception e){
		catch(SQLException e){
		/** ****** FindBugs Fix End ***********  on 14DEC09*/
			logger.fatal("General Exception: ", e);
			throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, e.getMessage());
		}
		return entries;
	}


	/**This method does the posting for BVT
	 * @param pstmtAccountingEntry
	 * @param pstmtTxnPoolPartyDtls
	 * @param pstmtTxnPoolAllocation
	 * @param pstmtAccountUnposted
	 * @param pstmtAcctUnpostedInsert
	 * @param generateFor
	 * @param generateForId
	 * @param businessdate
	 * @param client
	 * @return
	 * @throws LMSException
	 * @throws SQLException
	 */
	private boolean[] generatePostingEntriesBVT(
			PreparedStatement pstmtAccountingEntry,
			PreparedStatement pstmtTxnPoolPartyDtls,
			PreparedStatement pstmtAccountUnposted,	PreparedStatement pstmtAcctUnpostedInsert,
			// ENH_10007 Starts
			//BVT Adjsutmet Accounting Entries should be generated per host.
			//Date businessdate, SweepsTriggerDetails std )throws LMSException, SQLException{
	    	Date businessdate, SweepsTriggerDetails std, String hostId)throws LMSException, SQLException{
	    	// ENH_10007 Ends
		//J-Test OOP.HIF-3
		boolean postingEntries2[] = {false,false,false,false,false};

		/** ****** JTEST FIX start ************  on 14DEC09*/
//		SimpleDateFormat formatter = new SimpleDateFormat(LMSConstants.DATE_FORMAT);
		SimpleDateFormat formatter = new SimpleDateFormat(LMSConstants.DATE_FORMAT, Locale.US);
		/** ****** JTEST FIX end **************  on 14DEC09*/
		String strbusinessdate = formatter.format(businessdate);
		String dateFormat = LMSConstants.DATE_FORMAT;
		long nbrProcessId=std.getProcessId();
		String sqlString;
		//StringBuffer sbufUnpostedAmtInsr = new StringBuffer("");//INT_1039
		String accountingEntryId = null;
		//SUP_1082b starts
		//store the accounting entry id of the accrual entry corresponding to the posting entry
		String aEPostingAccrualId = null;
		//SUP_1082b ends
		String poolId = null;

		/** ****** JTEST FIX start ************  on 14DEC09*/
//		Statement stmt = null;
		PreparedStatement pStmt = null;
		/** ****** JTEST FIX end **************  on 14DEC09*/
		ResultSet rs=null;
		Connection conn = null;
		AccountingDO acctDoObj = null;
		boolean b_flgSeperate = true;
		BigDecimal amountToPost = null;
		// INT_1055 Start
		//Date postingDate = null;
		//Date sendingDate = null;
		Date postSettlementDate = null;
		Date actualPostingDate = null;
		Date valueDate = null;
		// INT_1055 End

		// Get the relevant BVT entries for posting
				//INT_1055 Starts
//		sqlString = (new StringBuffer(" select participant_id, max_dat, dr_sum, cr_sum, hdr.id_pool, cod_party_ccy, nbr_coreaccount, cod_branch, hdr.cod_gl, cod_bnk_lgl, ")
//				.append("  nbr_ibanaccnt, flg_def_drcr_consol, NVL(amt_min_cr,0) amt_min_cr, NVL(amt_min_dr,0) amt_min_dr, dat_post_settlement, dat_post_debitdelay, dat_post_creditdelay,  ")
//				.append("   (SELECT DISTINCT HDR.DAT_POST_DEBITDELAY FROM OPOOL_TXNPOOL_PARTYDTLS PAR, OPOOL_TXNPOOLHDR HDR WHERE ")
//				.append("   HDR.ID_POOL=PAR.ID_POOL AND HDR.DAT_BUSINESS_POOLRUN = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')  ) DEBIT_DELAY, ")
//				.append("   (SELECT DISTINCT DAT_POST_CREDITDELAY FROM OPOOL_TXNPOOL_PARTYDTLS PAR, OPOOL_TXNPOOLHDR HDR WHERE ")
//				.append("   HDR.ID_POOL=PAR.ID_POOL AND HDR.DAT_BUSINESS_POOLRUN = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')  )  CREDIT_DELAY ")
//				.append("   from opool_txnpoolhdr hdr, ")
//				.append("   (select tmp.participant_id, max_dat, NVL(dr_sum,0) dr_sum, NVL(cr_sum,0) cr_sum, nbr_processid, id_pool, cod_party_ccy, nbr_coreaccount, cod_branch, cod_gl, cod_bnk_lgl, nbr_ibanaccnt, flg_def_drcr_consol, amt_min_cr, amt_min_dr ")
//				.append("   from opool_txnpool_partydtls par,  ")
//				.append("   (select participant_id, max(dat_business_poolrun) max_dat, sum(amt_dr_interest_diff) dr_sum, sum(amt_cr_interest_diff) cr_sum  ")
//				.append("   from opool_txnpool_bvtdiff_int ")
//				.append("   where nbr_processid =" +nbrProcessId )
//				.append("   AND DAT_POSTING < TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "') ")
//				.append("   AND (AMT_DR_INTEREST_DIFF != 0 or AMT_CR_INTEREST_DIFF != 0) ")
//				.append("   AND TYP_PARTICIPANT = 'ACCOUNT' AND FLG_REALPOOL = 'Y' ")
//				.append("   GROUP BY PARTICIPANT_ID ")
//				.append("   ) TMP ")
//				.append("   WHERE PAR.NBR_PROCESSID = " +nbrProcessId)
//				.append("   AND PAR.PARTICIPANT_ID = TMP.PARTICIPANT_ID ")
//				.append("   AND PAR.DAT_BUSINESS_POOLRUN = MAX_DAT  ) t2 ")
//				.append("   where hdr.nbr_processid = t2.nbr_processid ")
//				.append("    and hdr.id_pool = t2.id_pool ")
//				.append("   and hdr.dat_business_poolrun = max_dat	")).toString();
		StringBuilder sBuf = new StringBuilder();
		sBuf.append(" SELECT participant_id, max_dat, dr_sum, cr_sum, hdr.id_pool, cod_party_ccy,");
		sBuf.append(" nbr_coreaccount, cod_branch, hdr.cod_gl, cod_bnk_lgl, nbr_ibanaccnt, ");
		sBuf.append(" flg_def_drcr_consol, NVL (amt_min_cr, 0) amt_min_cr, ");
		sBuf.append(" NVL (amt_min_dr, 0) amt_min_dr, dat_post_settlement, ");
		sBuf.append(" dat_post_debitdelay, dat_post_creditdelay, ");
		sBuf.append(" (SELECT dat_post_debitdelay ");
		sBuf.append(" FROM opool_txnpoolhdr h1 ");
		sBuf.append(" WHERE h1.id_pool = hdr.id_pool ");
		// INT_1069 Start
		//sBuf.append(" AND h1.dat_business_poolrun = max_dat + 1 ");
		sBuf.append(" AND h1.dat_business_poolrun = hdr.dat_posting + 1 ");
		// sBuf.append(" TO_DATE ('" + strbusinessdate + "', '" + dateFormat + "') ");
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		sBuf.append(" AND h1.nbr_processid = " + nbrProcessId);
		sBuf.append(" AND h1.nbr_processid = ? ");
		/** ****** JTEST FIX end **************  on 14DEC09*/
		// sBuf.append(" AND h1.nbr_processid = (SELECT MAX (NBR_PROCESSID) ");
		// sBuf.append(" FROM opool_txnpoolhdr h ");
		// sBuf.append(" WHERE h.id_pool = h1.id_pool ");
		// sBuf.append(" AND h.dat_business_poolrun = h1.dat_business_poolrun ");
		// sBuf.append(" )	");
		// INT_1069 End
		sBuf.append(" ) next_debit_delay, ");
		sBuf.append(" (SELECT dat_post_creditdelay ");
		sBuf.append(" FROM opool_txnpoolhdr h2 ");
		sBuf.append(" WHERE h2.id_pool = hdr.id_pool ");
		// INT_1069 Start
		//sBuf.append(" AND h2.dat_business_poolrun = max_dat + 1 ");
		sBuf.append(" AND h2.dat_business_poolrun = hdr.dat_posting + 1 ");
		// sBuf.append(" TO_DATE ('" + strbusinessdate + "', '" + dateFormat + "') ");
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		sBuf.append(" AND h2.nbr_processid = " + nbrProcessId);
		sBuf.append(" AND h2.nbr_processid = ?");
		/** ****** JTEST FIX end **************  on 14DEC09*/
		// sBuf.append(" AND h2.nbr_processid = (SELECT MAX (NBR_PROCESSID) ");
		// sBuf.append(" FROM opool_txnpoolhdr h ");
		// sBuf.append(" WHERE h.id_pool = h2.id_pool ");
		// sBuf.append(" AND h.dat_business_poolrun = h2.dat_business_poolrun ");
		// sBuf.append(" )	");
		// INT_1069 End
		sBuf.append(" ) next_credit_delay, dat_posting ");
		sBuf.append(" FROM opool_txnpoolhdr hdr, ");
		sBuf.append(" (SELECT tmp.participant_id, max_dat, NVL (dr_sum, 0) dr_sum, ");
		sBuf.append(" NVL (cr_sum, 0) cr_sum, nbr_processid, id_pool, cod_party_ccy, ");
		sBuf.append(" nbr_coreaccount, cod_branch, cod_gl, cod_bnk_lgl, ");
		sBuf.append(" nbr_ibanaccnt, flg_def_drcr_consol, amt_min_cr, amt_min_dr ");
		sBuf.append(" FROM opool_txnpool_partydtls par, ");
		sBuf.append(" (SELECT participant_id, MAX (dat_business_poolrun) max_dat, ");
		sBuf.append(" SUM (amt_dr_interest_diff) dr_sum, ");
		sBuf.append(" SUM (amt_cr_interest_diff) cr_sum ");
				//BVTAccrualBasedOnPosting Change Starts
		//sBuf.append(" FROM opool_txnpool_bvtdiff_int ");
		sBuf.append(" FROM opool_txnpool_bvtdiff_int diff ");
		//BVTAccrualBasedOnPosting Change Ends
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		sBuf.append(" WHERE nbr_processid = "+nbrProcessId);
		sBuf.append(" WHERE nbr_processid = ?");
		/** ****** JTEST FIX end **************  on 14DEC09*/
		//BVTAccrualBasedOnPosting Change Starts
		//sBuf.append(" AND dat_posting < TO_DATE ('" + strbusinessdate + "', '" + dateFormat + "') ");
		sBuf.append(" AND dat_accrual <=  ");
		sBuf.append(" (SELECT MAX(dat_posting) ");
		sBuf.append(" FROM opool_txnpool_bvtdiff_int d ");
		sBuf.append(" WHERE d.NBR_PROCESSID = diff.NBR_PROCESSID ");
		sBuf.append(" AND d.ID_POOL = diff.ID_POOL ");
		sBuf.append(" AND d.PARTICIPANT_ID = diff.PARTICIPANT_ID ");
		sBuf.append(" AND d.DAT_BUSINESS_POOLRUN = diff.DAT_BUSINESS_POOLRUN ");
		sBuf.append(" AND d.DAT_POSTING < TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "') ");
		sBuf.append(" ) ");
		//BVTAccrualBasedOnPosting Change Ends
		sBuf.append(" AND ( amt_dr_interest_diff != 0 ");
		sBuf.append(" OR amt_cr_interest_diff != 0 ");
		sBuf.append(" ) ");
		sBuf.append(" AND typ_participant = 'ACCOUNT' ");
		sBuf.append(" AND flg_realpool = 'Y' ");
		sBuf.append(" GROUP BY participant_id) tmp ");
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		sBuf.append(" WHERE par.nbr_processid = "+nbrProcessId);
		sBuf.append(" WHERE par.nbr_processid = ?");
		/** ****** JTEST FIX end **************  on 14DEC09*/
		sBuf.append(" AND par.participant_id = tmp.participant_id ");
		/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
		//SUP_HSBC_041 Fix starts
		// It's possible that partcipants of type pool & account with same nbr_ownaccout/id_pool exists for a given root pool
		sBuf.append(" AND par.typ_participant = 'ACCOUNT'");
		//SUP_HSBC_041 Fix ends
		/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
		sBuf.append(" AND par.dat_business_poolrun = max_dat) t2 ");
		sBuf.append(" WHERE hdr.nbr_processid = t2.nbr_processid ");
		sBuf.append(" AND hdr.id_pool = t2.id_pool ");
		sBuf.append(" AND hdr.dat_business_poolrun = max_dat ");
		//SUP_2039:Issue672 Starts
		// If the Allocation model is CENTRAL and counter Account is BANK Account
		// Accrue/Post the participant posting entries. 
		// Else don't accrue/post if counter account is SETTLEMENT Account
		sBuf.append(" AND hdr.cod_counter_account <> 'S' ");
		//SUP_2039:Issue672 Ends
		//ENH_10007 Starts
		//BVT Adjsutmet Accounting Entries should be generated per host.
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		sBuf.append("    AND hdr.cod_gl = '"+hostId+"'");
		sBuf.append("    AND hdr.cod_gl = ? ");
		/** ****** JTEST FIX end **************  on 14DEC09*/
		//ENH_10007 Ends
		sqlString = sBuf.toString();
		//replaced and modified DEBIT_DELAY,CREDIT_DELAY with next_debit_delay, next_credit_delay
		//respectively. dat_posting added as last column
		//INT_1055 Ends

		if (logger.isDebugEnabled())
			logger.debug("Query PARTICIPANT POSTING" + sqlString);

		try{
			conn = LMSConnectionUtility.getConnection();
			//SUP_1082a starts
			//Load the posting dates cache
			calculateNextCyclePostingDates(conn, nbrProcessId, businessdate);
			if (postCache == null) {
			    if(logger.isWarnEnabled()){
			        logger.warn(" Unable to form the posting dates Cache ");
			    }
            }
			//SUP_1082a ends
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			stmt = conn.createStatement();
//			rs = stmt.executeQuery(sqlString);
			
			pStmt = conn.prepareStatement(sqlString);
			pStmt.setLong(1, nbrProcessId);
			pStmt.setLong(2, nbrProcessId);
			pStmt.setLong(3, nbrProcessId);
			pStmt.setLong(4, nbrProcessId);
			pStmt.setString(5, hostId);
			rs = pStmt.executeQuery();
			/** ****** JTEST FIX end **************  on 14DEC09*/
			if (logger.isDebugEnabled()) {
				logger.debug(" Executed Query PARTICIPANT POSTING ");
			}

			HashMap hmFields = new HashMap();
			hmFields.put(ExecutionConstants.PROCESSID,nbrProcessId); // Mashreq_AE_Hook
			hmFields.put(ExecutionConstants.BUSINESSDATE, businessdate); // Mashreq_AE_Hook fix
			//SUP_1082b starts
			//Use this hashmap to pass on to the processBVTEntry with type ACCRUAL.
			HashMap hmAccrualFields = new HashMap();
			//SUP_1082b ends
			BigDecimal amtToPostCreditP = new BigDecimal(0);
			BigDecimal amountToPostDebitP = new BigDecimal(0);
			//SUP_1079a starts
			//BigDecimal minAmtCredit = new BigDecimal(0);
			//BigDecimal minAmtDebit = new BigDecimal(0);
			//SUP_1079a ends
			BigDecimal bd_amtNotPosted = new BigDecimal(0);
			//SUP_1079a starts
			//BigDecimal minAmtNet = new BigDecimal(0);
			//SUP_1079a ends
			boolean results = false;
				while(rs.next())
				{
					results = true;
					acctDoObj = new AccountingDO();
					String flg_def_drcr_consol = rs.getString("FLG_DEF_DRCR_CONSOL");
					b_flgSeperate = true;
					accountingEntryId = "0";
					//SUP_1082b starts
					aEPostingAccrualId = "0";
					//SUP_1082b ends
					poolId = "";
					//combinedEntry = false;
					amtToPostCreditP = rs.getBigDecimal("CR_SUM");

					amountToPostDebitP = rs.getBigDecimal("DR_SUM");

					poolId = rs.getString("ID_POOL");
					if (logger.isDebugEnabled()) {
						logger.debug("Resultset flg_def_drcr_consol" + flg_def_drcr_consol);
						logger.debug("Resultset POOLID" + poolId);
					}
					if("Y".equalsIgnoreCase(flg_def_drcr_consol!=null
																?flg_def_drcr_consol.trim()
																:"" )){
						//Get the NetInterest,if consolidate
						amountToPost = amtToPostCreditP.subtract(amountToPostDebitP);
						// Set the seperate posting flag to False
						//If we are consolidating then we need to check if we have both Dr and Cr Interest and post them
						//CR CUST/DR CUST  depending on which is greater and divide it into 3 legs
						//and need to pass crAmt and DrAMt
						//DR PAYMENTS
						//CR RECE
						if(amtToPostCreditP.compareTo(zero)!=0 && amountToPostDebitP.compareTo(zero)!=0){
							//combinedEntry = true;
							//if both debit and credit interest are present
						}
						b_flgSeperate = false;
					}
					if (logger.isDebugEnabled()) {
						logger.debug(" Flag Separate" + b_flgSeperate);
					}
					//fill in the AccountingDo object's attributes.
					acctDoObj.set_cod_bnk_lgl(rs.getString("COD_BNK_LGL"));
					acctDoObj.set_cod_branch(rs.getString("COD_BRANCH"));
					acctDoObj.set_cod_gl(rs.getString("COD_GL"));
					acctDoObj.set_cod_ccy(rs.getString("COD_PARTY_CCY"));
					acctDoObj.set_nbr_coreaccount(rs.getString("NBR_COREACCOUNT"));
					acctDoObj.set_nbr_ibanacct(rs.getString("NBR_IBANACCNT"));
					acctDoObj.set_nbr_ownaccount(rs.getString("PARTICIPANT_ID"));

					hmFields.put("DAT_POST_SETTLEMENT",rs.getDate("DAT_POST_SETTLEMENT"));
					hmFields.put("DAT_POST_CREDITDELAY",rs.getDate("DAT_POST_CREDITDELAY"));
					hmFields.put("DAT_POST_DEBITDELAY",rs.getDate("DAT_POST_DEBITDELAY"));
					// INT_1055 Start
					// hmFields.put("BVT_CREDITDELAY",rs.getDate("CREDIT_DELAY"));
					// hmFields.put("BVT_DEBITDELAY",rs.getDate("DEBIT_DELAY"));
					hmFields.put("NEXT_CREDIT_DELAY",rs.getDate("NEXT_CREDIT_DELAY"));
					hmFields.put("NEXT_DEBIT_DELAY",rs.getDate("NEXT_DEBIT_DELAY"));
					hmFields.put("DAT_VALUE",rs.getDate("DAT_POSTING"));
					// INT_1055 End
					//CAL fix starts
					java.util.Date dbNextCreditDelay = rs.getDate("NEXT_CREDIT_DELAY");
					java.util.Date dbNextDebitDelay = rs.getDate("NEXT_DEBIT_DELAY");
					String codGL = rs.getString("COD_GL");
					java.util.Date[] calcPostingDates = null;
					//SUP_1082c starts
//					if(dbNextCreditDelay == null || dbNextDebitDelay == null){
//					    calcPostingDates = calculateNextCyclePostingDates(conn, poolId, codGL, nbrProcessId, businessdate);
//					}
					if(postCache == null || postCache.get(poolId) == null){
					    if(logger.isWarnEnabled()){
					        logger.warn("Error forming the posting cache OR poolId ["+poolId+"] postingDates not present in the cache.");
					        logger.warn("So getting the dates from database for "+poolId+".");
					    }
						calcPostingDates = calculateNextCyclePostingDates(conn, poolId, codGL, nbrProcessId, businessdate);
					}
					else{
					    calcPostingDates = (Date[])postCache.get(poolId);
					}					
					//SUP_1082c ends
					
					//CAL fix ends

					//SUP_1079a starts
					//minAmtCredit = rs.getBigDecimal("AMT_MIN_CR");
					//minAmtDebit = rs.getBigDecimal("AMT_MIN_DR");
					//SUP_1079a ends

					if (logger.isDebugEnabled()) {
						logger.debug("(Date)hmFields.get(DAT_POST_DEBITDELAY" +hmFields.get("DAT_POST_DEBITDELAY"));
						logger.debug("(Date)hmFields.get(DAT_POST_CREDITDELAY" +hmFields.get("DAT_POST_CREDITDELAY"));
						logger.debug("(Date)hmFields.get(DAT_POST_SETTLEMENT" +hmFields.get("DAT_POST_SETTLEMENT"));
						// INT_1055 Start
						// logger.debug("(Date)hmFields.get(BVT_CREDITDELAY" +hmFields.get("BVT_CREDITDELAY"));
						// logger.debug("(Date)hmFields.get(BVT_DEBITDELAY" +hmFields.get("BVT_DEBITDELAY"));
						logger.debug("Next Credit Delay: " +hmFields.get("NEXT_CREDIT_DELAY"));
						logger.debug("Next Debit Delay: " +hmFields.get("NEXT_DEBIT_DELAY"));
						logger.debug("Value Date: " +hmFields.get("DAT_VALUE"));
						// INT_1055 End
						//SUP_1079a starts
						//logger.debug("minAmtCredit" + minAmtCredit);
						//logger.debug("minAmtDebit" + minAmtDebit);
						//SUP_1079a ends
					}
					// check is any amount is left due to be posted.
					bd_amtNotPosted = new BigDecimal(0);
					//INT_1055 Start
					postSettlementDate = (Date)hmFields.get("DAT_POST_SETTLEMENT");
					//INT_1055 End
					if(b_flgSeperate){
						//POSTING CREDIT INTEREST, only if greater than minmCreditInterest
						//chk the order
						//1. add UnPostedAmount
						//2. !=0
						//3. > than mimnAmtInterest
						//then post
						bd_amtNotPosted = setUnpostedAmounts(ExecutionConstants.CREDIT_INTEREST,acctDoObj,
								pstmtAccountUnposted,businessdate,ExecutionConstants.POSTING);
						if (logger.isDebugEnabled()) {
							logger.debug("Unposted Amount "  + bd_amtNotPosted +"for ExecutionConstants.CREDIT_INTEREST");
						}
						//chk if any amount is left to be posted
						/*
						if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
							amtToPostCreditP = amtToPostCreditP.add(bd_amtNotPosted);
							if (logger.isDebugEnabled()) {
								logger.debug("Adding Unposted Amount  "  + bd_amtNotPosted +"for ExecutionConstants.CREDIT_INTEREST "+amtToPostCreditP);
							}
						}// UNPOSTED AMOUNT
						*/
						//CHECK FOR MINIMUM AMT INTEREST
						if (amtToPostCreditP.compareTo(zero) != 0){

							//SUP_1079a starts
							//if(amtToPostCreditP.compareTo(minAmtCredit)>=0){
							//SUP_1079a ends
								// INT_1055 Start
								// sendingDate = (Date)hmFields.get("BVT_CREDITDELAY");
								// postingDate = (Date)hmFields.get("DAT_POST_CREDITDELAY");
							    if (postSettlementDate.compareTo(businessdate) < 0) {
							        //SUP_1082c starts
							        hmFields.put("DAT_VALUE",calcPostingDates[0]);
//	                                actualPostingDate = (Date) hmFields.get("NEXT_CREDIT_DELAY");
//	                                // CAL fix starts
//									//actualPostingDate = actualPostingDate != null ? actualPostingDate
//	                                //                                             : businessdate;
//									if(actualPostingDate == null){										
//										// Calculate the NEXT_CREDIT_DELAY and assign it to actualPostingDate 
//										actualPostingDate = calcPostingDates[3];
//										if(logger.isDebugEnabled()){
//											logger.debug(" calculated next_credit_delay is "+ actualPostingDate);
//										}
//									}		
//									// CAL fix ends
										actualPostingDate = calcPostingDates[3];
							        //SUP_1082c ends
							    }
							    else
							        actualPostingDate = (Date) hmFields.get("DAT_POST_CREDITDELAY");

							    hmFields.put("DAT_POSTING",actualPostingDate);

								valueDate = (Date)hmFields.get("DAT_VALUE");
								//postingDate = postingDate!=null?postingDate:businessdate;
								//If the pool is already been closed for the respective businessDate then posting will be done on the business date
								// sendingDate = sendingDate!=null?sendingDate:businessdate;
								// hmFields.put("DAT_POSTING",postingDate);
								// hmFields.put("DAT_SENT",sendingDate);
								hmFields.put("DAT_SENT", addOffsetToDate(valueDate, 1));
								// INT_1055 End
								//SUP_1082b starts
								hmAccrualFields = new HashMap(hmFields);
								hmAccrualFields.put(ExecutionConstants.PROCESSID,nbrProcessId); // Mashreq_AE_Hook
								hmAccrualFields.put(ExecutionConstants.BUSINESSDATE, businessdate); // Mashreq_AE_Hook fix
								//15.1.1_Resource_Leaks_Fix start
								LMSConnectionUtility.close(rs);
								LMSConnectionUtility.close(pStmt);								
								//15.1.1_Resource_Leaks_Fix end
								if (accCache == null || accCache.get(poolId)== null) {
								    throw new LMSException("101302"," Error occured in forming the cache ");
					            }
								Date[] accDates = (Date[])accCache.get(poolId);
//								if(accDates[0] == null || accDates[0].compareTo(businessdate)<0){
//								    hmAccrualFields.put("DAT_SENT", accDates[2]);
//								    hmAccrualFields.put("DAT_POSTING",accDates[3]);
//								}
//								else {
								//Always consider the next cycle accrual dates
								    hmAccrualFields.put("DAT_SENT", accDates[0]);
								    hmAccrualFields.put("DAT_POSTING",accDates[1]);
//				                }								
								//SUP_1082b ends
								//SUP_1082b starts
								//First accrue the adjustment amount
								aEPostingAccrualId = ""+ processBVTEntry(acctDoObj,
 									amtToPostCreditP,ExecutionConstants.ACCRUAL,
 									ExecutionConstants.BVT_CREDIT,
 									ExecutionConstants.CREDIT_INTEREST,pstmtAccountingEntry,
 									//SUP_1082b starts
 									//pstmtAcctUnpostedInsert,poolId,hmFields);
 									/** ****** JTEST FIX start ************  on 14DEC09*/
 									pstmtAcctUnpostedInsert,poolId,hmAccrualFields, conn);
 									/** ****** JTEST FIX end **************  on 14DEC09*/
									//SUP_1082b ends
								//accountingEntryId = "" + processBVTEntry(acctDoObj,
                                accountingEntryId = aEPostingAccrualId + "|" + processBVTEntry(acctDoObj,
                                //SUP_1082b ends
									amtToPostCreditP,ExecutionConstants.POSTING,
									//ExecutionConstants.CREDIT,
									ExecutionConstants.BVT_CREDIT,//DrCrAllocation
									ExecutionConstants.CREDIT_INTEREST,pstmtAccountingEntry,
									/** ****** JTEST FIX start ************  on 14DEC09*/
									pstmtAcctUnpostedInsert,poolId,hmFields, conn);//INT_1039
									/** ****** JTEST FIX end **************  on 14DEC09*/
							//SUP_1079a starts
							// minimum credit amount and minimum debit amount not applicable for BVT
							//}							 
							//else{
								//addToAcctUnpostedTbl(pstmtAcctUnpostedInsert,acctDoObj.get_nbr_ownaccount(),
								//amtToPostCreditP,ExecutionConstants.POSTING,ExecutionConstants.CREDIT_INTEREST,"INTEREST LESS THAN MINIMUM AMOUNT INTEREST",poolId);//INT_1039
							//}//minCreditInterest
                            //SUP_1079a ends
						}else{
							logger.fatal("Credit Interest is 0 for"  + acctDoObj + "SO not posting the amount ");
						}

					//POSTING DEBIT INTEREST
						bd_amtNotPosted = new BigDecimal(0);//RESETTING AMOUNT NOT POSTED
						bd_amtNotPosted = setUnpostedAmounts(ExecutionConstants.DEBIT_INTEREST,acctDoObj,
						pstmtAccountUnposted,businessdate,ExecutionConstants.POSTING);
						if (logger.isDebugEnabled()) {
							logger.debug("Unposted Amount "  + bd_amtNotPosted +"for ExecutionConstants.DEBIT_INTEREST");
						}
						//chk if any amount is left to be posted
						/*
						if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
							amountToPostDebitP = amountToPostDebitP.add(bd_amtNotPosted);
							if (logger.isDebugEnabled()) {
								logger.debug("Adding Unposted Amount  "  + bd_amtNotPosted +"for ExecutionConstants.DEBIT_INTEREST "+amountToPostDebitP);
							}
						}// UNPOSTED DEBIT AMOUNT
						*/
						//check with minm debit amount

						if (amountToPostDebitP.compareTo(zero)!=0){
							//SUP_1079a starts
						    //if(amountToPostDebitP.compareTo(minAmtDebit)>=0){
						    //SUP_1079a ends
								// INT_1055 Start
								// sendingDate = (Date)hmFields.get("BVT_DEBITDELAY");
								// postingDate = (Date)hmFields.get("DAT_POST_DEBITDELAY");
							    if (postSettlementDate.compareTo(businessdate) < 0) {
							        //SUP_1082c starts
							        hmFields.put("DAT_VALUE",calcPostingDates[0]);
//							        actualPostingDate = (Date) hmFields.get("NEXT_DEBIT_DELAY");
//							        							        //CAL fix starts
//									//actualPostingDate = actualPostingDate != null ? actualPostingDate
//                                    //                                         : businessdate;
//									if(actualPostingDate == null){										
//										// Calculate the NEXT_DEBIT_DELAY and assign it to actualPostingDate 
//										actualPostingDate = calcPostingDates[4];
//										if(logger.isDebugEnabled()){
//											logger.debug(" calculated next_debit_delay is "+ actualPostingDate);
//										}
//									}
//									//CAL fix ends
										actualPostingDate = calcPostingDates[4];
							        //SUP_1082c ends
							    }
							    else
							        actualPostingDate = (Date) hmFields.get("DAT_POST_DEBITDELAY");

							    hmFields.put("DAT_POSTING",actualPostingDate);

								valueDate = (Date)hmFields.get("DAT_VALUE");
								// sendingDate = sendingDate!=null?sendingDate:businessdate;
								//hmFields.put("DAT_POSTING",postingDate);
								//hmFields.put("DAT_SENT",sendingDate);
								hmFields.put("DAT_SENT", addOffsetToDate(valueDate,1));
								// INT_1055 End

								//SUP_1082b starts
								hmAccrualFields = new HashMap(hmFields);
								hmAccrualFields.put(ExecutionConstants.PROCESSID,nbrProcessId); // Mashreq_AE_Hook
								hmAccrualFields.put(ExecutionConstants.BUSINESSDATE, businessdate); // Mashreq_AE_Hook fix
								if (accCache == null || accCache.get(poolId)== null) {
								    throw new LMSException("101302"," Error occured in forming the cache ");
					            }
								Date[] accDates = (Date[])accCache.get(poolId);
//								if(accDates[0] == null || accDates[0].compareTo(businessdate)<0){
//								    hmAccrualFields.put("DAT_SENT", accDates[2]);
//								    hmAccrualFields.put("DAT_POSTING",accDates[3]);
//								}
//								else {
								//Always consider the next cycle accrual dates
								    hmAccrualFields.put("DAT_SENT", accDates[0]);
								    hmAccrualFields.put("DAT_POSTING",accDates[1]);
//				                }								
								//SUP_1082b ends

								//SUP_1082b starts
								aEPostingAccrualId = accountingEntryId + "|" + processBVTEntry(acctDoObj,
   									amountToPostDebitP,ExecutionConstants.ACCRUAL,
									ExecutionConstants.BVT_DEBIT,
									ExecutionConstants.DEBIT_INTEREST,
									pstmtAccountingEntry,pstmtAcctUnpostedInsert,
									//SUP_1082b starts
									//poolId,hmFields);
									/** ****** JTEST FIX start ************  on 14DEC09*/
									poolId,hmAccrualFields, conn);
									/** ****** JTEST FIX end **************  on 14DEC09*/
									//SUP_1082b ends
								//SUP_1082b ends
								//SUP_1082b starts
								//accountingEntryId = accountingEntryId + "|" + processBVTEntry(acctDoObj,
                                accountingEntryId = aEPostingAccrualId + "|" + processBVTEntry(acctDoObj,
                                //SUP_1082b ends
									amountToPostDebitP,ExecutionConstants.POSTING,
									//ExecutionConstants.DEBIT,
									ExecutionConstants.BVT_DEBIT,//DrCrAllocation
									ExecutionConstants.DEBIT_INTEREST,
									pstmtAccountingEntry,pstmtAcctUnpostedInsert,
									/** ****** JTEST FIX start ************  on 14DEC09*/
//									poolId,hmFields);//INT_1039
                                	poolId,hmFields, conn);//INT_1039
									/** ****** JTEST FIX end **************  on 14DEC09*/
							//SUP_1079a starts
						    //}else{
								//addToAcctUnpostedTbl(pstmtAcctUnpostedInsert,acctDoObj.get_nbr_ownaccount(),
										//amountToPostDebitP,ExecutionConstants.POSTING,ExecutionConstants.DEBIT_INTEREST,"INTEREST LESS THAN MINIMUM AMOUNT INTEREST",poolId);//INT_1039
							//}//minDebitInterest
						    //SUP_1079a ends
						}else{
							logger.fatal("Debit Interest is 0 for"  + acctDoObj + "SO not posting the amount ");
						}

					}else{
						//call processEntry for the NetAmount --consolidated
						bd_amtNotPosted = setUnpostedAmounts(ExecutionConstants.NET_INTEREST,acctDoObj,
											pstmtAccountUnposted,businessdate,ExecutionConstants.POSTING);
						if (logger.isDebugEnabled()) {
						  logger.debug("Unposted Amount "  + bd_amtNotPosted +"for ExecutionConstants.NET_INTEREST");
						 }
						 //chk if any amount is left to be posted
						/*
						if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
							amountToPost = amountToPost.add(bd_amtNotPosted);
							if (logger.isDebugEnabled()) {
								logger.debug("Adding Unposted Amount  "  + bd_amtNotPosted +"for ExecutionConstants.NET_INTEREST "+amountToPost);
							 }
						 }// UNPOSTED NET INTEREST
						 */
						//	CHECK IF THE NET_INTEREST IS CREDIT(+ve) or DEBIT(-ve)
						//	if (CREDIT)
						if (amountToPost.compareTo(zero) > 0){
							// INT_1055 Start
						    if (postSettlementDate.compareTo(businessdate) < 0){
						        //SUP_1082c starts
						        hmFields.put("DAT_VALUE",calcPostingDates[0]);
//						        actualPostingDate = (Date)hmFields.get("NEXT_CREDIT_DELAY");
//						        //CAL fix starts
//								//actualPostingDate = actualPostingDate != null ? actualPostingDate
//						        //                                              : businessdate;
//								if(actualPostingDate == null){
//									// Calculate the NEXT_CREDIT_DELAY and assign it to actualPostingDate 
//									actualPostingDate = calcPostingDates[3];
//									if(logger.isDebugEnabled()){
//										logger.debug(" calculated next_credit_delay is "+ actualPostingDate);
//									}
//								}
//								//CAL fix ends
									actualPostingDate = calcPostingDates[3];
						        //SUP_1082c ends
						    }
						    else
						        actualPostingDate = (Date)hmFields.get("DAT_POST_CREDITDELAY");

							// postingDate = (Date)hmFields.get("DAT_POST_CREDITDELAY");
							// sendingDate = (Date)hmFields.get("BVT_CREDITDELAY");
						    // INT_1055 End
							//SUP_1079a starts
						    //minAmtNet =  minAmtCredit;
						    //SUP_1079a ends
						}else if (amountToPost.compareTo(zero) < 0){
							// INT_1055 Start
						    if (postSettlementDate.compareTo(businessdate) < 0){
						        //SUP_1082c starts
						        hmFields.put("DAT_VALUE",calcPostingDates[0]);
//
//						        actualPostingDate = (Date)hmFields.get("NEXT_DEBIT_DELAY");
//						        //CAL fix starts
//								//actualPostingDate = actualPostingDate != null ? actualPostingDate
//						        //                                              : businessdate;
//								if(actualPostingDate == null){
//									// Calculate the NEXT_DEBIT_DELAY  and assign it to actualPostingDate 
//									actualPostingDate = calcPostingDates[4];
//									if(logger.isDebugEnabled()){
//										logger.debug(" calculated next_debit_delay is "+ actualPostingDate);
//									}
//								}
//								//CAL fix ends
									actualPostingDate = calcPostingDates[4];
						        //SUP_1082c ends
						    }
						    else
						        actualPostingDate = (Date)hmFields.get("DAT_POST_DEBITDELAY");

						    // postingDate = (Date)hmFields.get("DAT_POST_DEBITDELAY");
							// sendingDate = (Date)hmFields.get("BVT_DEBITDELAY");
						    // INT_1055 End
							//SUP_1079a starts
						    //minAmtNet = minAmtDebit;
						    //SUP_1079a ends
						}else{
							logger.fatal("Net Interest is 0 for"  + acctDoObj + "SO not posting the amount ");
						}
						//INT_1055 Start
					    hmFields.put("DAT_POSTING",actualPostingDate);
						valueDate = (Date)hmFields.get("DAT_VALUE");
						hmFields.put("DAT_SENT", addOffsetToDate(valueDate,1));
						//postingDate = postingDate!=null?postingDate:businessdate;
						// sendingDate = sendingDate!=null?sendingDate:businessdate;
						// hmFields.put("DAT_POSTING",postingDate);
						// hmFields.put("DAT_SENT",sendingDate);
						// INT_1055 End

						//CONSIDERING MINM aMOUNT INTEREST
						//SUP_1079a starts
						//if((amountToPost).abs().compareTo(minAmtNet)>=0){//Changed for absolut of NET_INTEREST while posting
						//SUP_1079a ends
							/*
							if(combinedEntry){
								if (logger.isDebugEnabled()) {
									logger.debug("Calling combinedEntry ");
								}
								//IF bd_amtNotPosted IS CR ADD IT TO CREDIT INTEREST AMT ELSE DEBIT INTEREST AMT
								//if (bd_amtNotPosted.compareTo(zero) > 0){
									//amtToPostCreditP = amtToPostCreditP.add(bd_amtNotPosted);
								//}else if(bd_amtNotPosted.compareTo(zero) < 0){
									//amountToPostDebitP = amountToPostDebitP.add(bd_amtNotPosted);
								accountingEntryId = "" + processCombinedBVTEntry(acctDoObj,
																amtToPostCreditP,amountToPostDebitP,
																ExecutionConstants.POSTING,
																ExecutionConstants.NET,
																ExecutionConstants.NET_INTEREST,
																pstmtAccountingEntry,pstmtAcctUnpostedInsert,
																sbufUnpostedAmtInsr,poolId,hmFields);
								//}
							}else{
							*/
								//SUP_1082b starts
								hmAccrualFields = new HashMap(hmFields);
								hmAccrualFields.put(ExecutionConstants.PROCESSID,nbrProcessId); // Mashreq_AE_Hook
								hmAccrualFields.put(ExecutionConstants.BUSINESSDATE, businessdate); // Mashreq_AE_Hook fix
								if (accCache == null || accCache.get(poolId)== null) {
								    throw new LMSException("101302"," Error occured in forming the cache ");
					            }
								Date[] accDates = (Date[])accCache.get(poolId);
//								if(accDates[0] == null || accDates[0].compareTo(businessdate)<0){
//								    hmAccrualFields.put("DAT_SENT", accDates[2]);
//								    hmAccrualFields.put("DAT_POSTING",accDates[3]);
//								}
//								else {
								//Always consider the next cycle accrual dates
								    hmAccrualFields.put("DAT_SENT", accDates[0]);
								    hmAccrualFields.put("DAT_POSTING",accDates[1]);
//				                }								
								//SUP_1082b ends
								//SUP_1082b starts
								aEPostingAccrualId = "" + processBVTEntry(acctDoObj,
																amountToPost,ExecutionConstants.ACCRUAL,
																ExecutionConstants.NET,
																ExecutionConstants.NET_INTEREST,																
																pstmtAccountingEntry,pstmtAcctUnpostedInsert,
																//SUP_1082b Starts
																//poolId,hmFields);
																/** ****** JTEST FIX start ************  on 14DEC09*/
																poolId,hmAccrualFields, conn);
																/** ****** JTEST FIX end **************  on 14DEC09*/
																//SUP_1082b ends
								//accountingEntryId = "" + processBVTEntry(acctDoObj,
								accountingEntryId = aEPostingAccrualId+ "|" + processBVTEntry(acctDoObj,
								//SUP_1082b ends
																amountToPost,ExecutionConstants.POSTING,
																ExecutionConstants.NET,
																ExecutionConstants.NET_INTEREST,
																pstmtAccountingEntry,pstmtAcctUnpostedInsert,
																/** ****** JTEST FIX start ************  on 14DEC09*/
//																poolId,hmFields);//INT_1039
																poolId,hmFields, conn);//INT_1039
																/** ****** JTEST FIX end **************  on 14DEC09*/
							//}//combinedEntry
						//SUP_1079a starts
//						}else{
//							addToAcctUnpostedTbl(pstmtAcctUnpostedInsert,acctDoObj.get_nbr_ownaccount(),
//								amountToPost,ExecutionConstants.POSTING,ExecutionConstants.NET_INTEREST,"INTEREST LESS THAN MINIMUM AMOUNT INTEREST",poolId);//INT_1039
//						}//minNetInterest
						//SUP_1079a ends
					}//flgSeparate
					if (logger.isDebugEnabled()) {
							logger.debug("After processEntry accountingEntryId"  + accountingEntryId);
					}
					//Update the Prepared Statements for OPOOL_PARTYDETAILS
					updateInterestBVT(pstmtTxnPoolPartyDtls,accountingEntryId,poolId,acctDoObj.get_nbr_ownaccount(),ExecutionConstants.POSTING);					//Add to the Pool List
					//updatePoolList(poolId, hmPoolList);
					//postingEntries[ExecutionConstants.OLM_ACCOUNTINGENTRY_INSERT] = true;
					//postingEntries[ExecutionConstants.PARTY_DTLS_UPDATE] = true;

				}//end of while resultset
				if (!results){
					logger.fatal(" No records for Posting -participant interest" + sqlString);
				}
			rs.close();
			//FOR POSTING ALLOCATION ENTRIES
			//INT_1055 Starts
//			String sqlAllocPostingDtls = (new StringBuffer("SELECT ")
//			.append(" participant_id, dat_value, amount, tmp3.id_pool, tmp3.cod_ccy, nbr_coreaccount, cod_branch, tmp3.cod_gl, ")
//			.append(" cod_bnk_lgl, nbr_ibanaccnt, flg_consolidation, dat_post_settlement, dat_post_debitdelay, dat_post_creditdelay,  ")
//			.append("   (SELECT DISTINCT HDR.DAT_POST_DEBITDELAY FROM OPOOL_TXNPOOL_PARTYDTLS PAR, OPOOL_TXNPOOLHDR HDR WHERE ")
//			.append("   HDR.ID_POOL=PAR.ID_POOL AND HDR.DAT_BUSINESS_POOLRUN = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')  ) DEBIT_DELAY, ")
//			.append("   (SELECT DISTINCT DAT_POST_CREDITDELAY FROM OPOOL_TXNPOOL_PARTYDTLS PAR, OPOOL_TXNPOOLHDR HDR WHERE ")
//			.append("   HDR.ID_POOL=PAR.ID_POOL AND HDR.DAT_BUSINESS_POOLRUN = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')  )  CREDIT_DELAY ")
//			.append(" from opool_txnpoolhdr hdr, ")
//			.append(" (select tmp2.participant_id, dat_value, amount, tmp2.id_pool, cod_party_ccy cod_ccy, nbr_coreaccount, ")
//			.append(" cod_branch, cod_gl, cod_bnk_lgl, nbr_ibanaccnt ")
//			.append(" from opool_txnpool_partydtls par, ")
//			.append(" (select tmp.participant_id, dat_value, amount, id_pool ")
//			.append(" from opool_txnpool_bvtdiff bd, ")
//			.append(" (select participant_id, max(dat_business_poolrun) dat_value, sum(amt_diff) amount ")
//			.append(" from opool_txnpool_bvtdiff ")
//			.append(" where nbr_processid=" + nbrProcessId)
//			.append("   and dat_posting < TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')  ")
//			.append("   and amt_diff != 0 ")
//			.append("   and typ_participant = 'ACCOUNT' ")
//			.append("   and flg_realpool = 'Y' ")
//			.append("   and sub_typ_calculation != 'R' ")
//			.append("   group by participant_id) tmp ")
//			// All participant accounts for which posting needs to be done
//			.append("   where bd.participant_id = tmp.participant_id ")
//			.append("   and bd.dat_business_poolrun = dat_value ")
//			.append("   and bd.nbr_processid=" + nbrProcessId + ") tmp2 ")
//			// PoolIds of the accounts on the corresponding value date
//			.append("   where par.id_pool = tmp2.id_pool ")
//			.append("   and par.PARTICIPANT_ID = tmp2.participant_id ")
//			.append("   and par.NBR_PROCESSID = " + nbrProcessId + " and par.dat_business_poolrun = tmp2.dat_value ) tmp3 ")
//			// Details of these accounts
//			.append("   where hdr.id_pool = tmp3.id_pool ")
//			.append("   and hdr.dat_business_poolrun = tmp3.dat_value ")
//			.append("   and hdr.nbr_processid = " + nbrProcessId )
//			// Posting dates for these accounts
//			.append("   union ")
//			.append("   select participant_id, dat_value, amount, tmp3.id_pool, tmp3.cod_ccy, nbr_coreaccount, cod_branch, tmp3.cod_gl, ")
//			.append("   cod_bnk_lgl, nbr_ibanaccnt, flg_consolidation, dat_post_settlement, dat_post_debitdelay, dat_post_creditdelay, ")
//			.append("   (SELECT DISTINCT HDR.DAT_POST_DEBITDELAY FROM OPOOL_TXNPOOL_PARTYDTLS PAR, OPOOL_TXNPOOLHDR HDR WHERE ")
//			.append("   HDR.ID_POOL=PAR.ID_POOL AND HDR.DAT_BUSINESS_POOLRUN = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')  ) DEBIT_DELAY, ")
//			.append("   (SELECT DISTINCT DAT_POST_CREDITDELAY FROM OPOOL_TXNPOOL_PARTYDTLS PAR, OPOOL_TXNPOOLHDR HDR WHERE ")
//			.append("   HDR.ID_POOL=PAR.ID_POOL AND HDR.DAT_BUSINESS_POOLRUN = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')  )  CREDIT_DELAY ")
//			.append("   from opool_txnpoolhdr hdr, ")
//			.append("   (select participant_id, dat_value, amount, id_pool, cod_ccy, nbr_coreaccount, cod_branch, cod_gl, ")
//			.append("   cod_bnk_lgl, nbr_ibanaccnt ")
//			.append("   from orbicash_accounts acc, ")
//			.append("   (select tmp.participant_id, dat_value, amount, id_pool ")
//			.append("   from opool_txnpool_bvtdiff bd, ")
//			.append("   (select participant_id, max(dat_business_poolrun) dat_value, sum(amt_diff) amount ")
//			.append("   from opool_txnpool_bvtdiff ")
//			.append("   where nbr_processid= " + nbrProcessId)
//			.append("   and dat_posting < TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')  ")
//			.append("   and amt_diff != 0 and typ_participant = 'CENTRAL' and flg_realpool = 'Y' and sub_typ_calculation != 'R' ")
//			.append("   group by participant_id ) tmp ")
//			// All central accounts for which posting needs to be done
//			.append("   where bd.participant_id = tmp.participant_id and bd.dat_business_poolrun = dat_value and bd.nbr_processid= " + nbrProcessId + " ) tmp2 ")
//			// PoolIds of the accounts on the corresponding value date
//			.append("   where acc.nbr_ownaccount = tmp2.participant_id) tmp3 ")
//			// Details of these accounts
//			.append("   where hdr.id_pool = tmp3.id_pool ")
//			.append("   and hdr.dat_business_poolrun = tmp3.dat_value ")
//			.append("   and hdr.nbr_processid = " + nbrProcessId)).toString();
//			// Posting dates for these accounts
			sBuf.setLength(0);
			//sBuf.append(" SELECT participant_id, dat_value, amount, tmp3.id_pool, tmp3.cod_ccy, ");
			sBuf.append(" SELECT participant_id, dat_value, dr_sum, cr_sum,  tmp3.id_pool, tmp3.cod_ccy, ");//DrCrAllocation
			sBuf.append(" nbr_coreaccount, cod_branch, tmp3.cod_gl, cod_bnk_lgl, nbr_ibanaccnt, ");
			sBuf.append(" flg_consolidation, dat_post_settlement, dat_post_debitdelay,  ");
			sBuf.append(" dat_post_creditdelay, ");
			sBuf.append(" (SELECT dat_post_debitdelay ");
			sBuf.append(" FROM opool_txnpoolhdr h1 ");
			sBuf.append(" WHERE h1.id_pool = hdr.id_pool ");
			// INT_1069 Start
			//sBuf.append(" AND h1.dat_business_poolrun = dat_value + 1 ");
			sBuf.append(" AND h1.dat_business_poolrun = hdr.dat_posting + 1 ");
			// sBuf.append(" TO_DATE ('" + strbusinessdate + "', '" + dateFormat + "') ");
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" AND h1.nbr_processid = " + nbrProcessId);
			sBuf.append(" AND h1.nbr_processid = ?");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			// sBuf.append(" AND h1.nbr_processid = (SELECT MAX (NBR_PROCESSID) ");
			// sBuf.append(" FROM opool_txnpoolhdr h ");
			// sBuf.append(" WHERE h.id_pool = h1.id_pool ");
			// sBuf.append(" AND h.dat_business_poolrun = h1.dat_business_poolrun ");
			// sBuf.append(" )	");
			// INT_1069 End
			sBuf.append(" ) next_debit_delay, ");
			sBuf.append(" (SELECT dat_post_creditdelay ");
			sBuf.append(" FROM opool_txnpoolhdr h2 ");
			sBuf.append(" WHERE h2.id_pool = hdr.id_pool ");
			// INT_1069 Start
			//sBuf.append(" AND h2.dat_business_poolrun = dat_value + 1 ");
			sBuf.append(" AND h2.dat_business_poolrun = hdr.dat_posting + 1 ");
			// sBuf.append(" TO_DATE ('" + strbusinessdate + "', '" + dateFormat + "') ");
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" AND h2.nbr_processid = " + nbrProcessId);
			sBuf.append(" AND h2.nbr_processid = ?");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			// sBuf.append(" AND h2.nbr_processid = (SELECT MAX (NBR_PROCESSID) ");
			// sBuf.append(" FROM opool_txnpoolhdr h ");
			// sBuf.append(" WHERE h.id_pool = h2.id_pool ");
			// sBuf.append(" AND h.dat_business_poolrun = h2.dat_business_poolrun ");
			// sBuf.append(" )	");
			// INT_1069 End
			sBuf.append("  ) next_credit_delay, dat_posting ");
			sBuf.append(" FROM opool_txnpoolhdr hdr, ");
			//sBuf.append(" (SELECT tmp2.participant_id, dat_value, amount, tmp2.id_pool, ");
			sBuf.append(" (SELECT tmp2.participant_id, dat_value, dr_sum, cr_sum, tmp2.id_pool, ");//DrCrAllocation
			sBuf.append(" cod_party_ccy cod_ccy, nbr_coreaccount, cod_branch, cod_gl, ");
			sBuf.append(" cod_bnk_lgl, nbr_ibanaccnt ");
			sBuf.append(" FROM opool_txnpool_partydtls par, ");
			//sBuf.append(" (SELECT tmp.participant_id, dat_value, amount, id_pool ");
			sBuf.append(" (SELECT tmp.participant_id, dat_value, dr_sum, cr_sum, id_pool ");//DrCrAllocation
			sBuf.append(" FROM opool_txnpool_bvtdiff bd, ");
			sBuf.append(" (SELECT participant_id, ");
			sBuf.append(" MAX (dat_business_poolrun) dat_value, ");
			//sBuf.append(" SUM (amt_diff) amount ");
			sBuf.append(" SUM (amt_dr_diff) dr_sum, SUM (amt_cr_diff) cr_sum ");//DrCrAllocation
			//BVTAccrualBasedOnPosting Change Starts
			//sBuf.append(" FROM opool_txnpool_bvtdiff ");
			sBuf.append(" FROM opool_txnpool_bvtdiff diff");
			//BVTAccrualBasedOnPosting Change Ends

			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" WHERE nbr_processid = "+nbrProcessId);
			sBuf.append(" WHERE nbr_processid = ?");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			//BVTAccrualBasedOnPosting Change Starts
			//sBuf.append(" AND dat_posting < ");
			//sBuf.append(" TO_DATE ('" + strbusinessdate + "', '" + dateFormat + "') ");
			sBuf.append(" AND dat_accrual <=  ");
			sBuf.append(" (SELECT MAX(dat_posting) ");
			sBuf.append(" FROM opool_txnpool_bvtdiff d ");
			sBuf.append(" WHERE d.NBR_PROCESSID = diff.NBR_PROCESSID ");
			sBuf.append(" AND d.ID_POOL = diff.ID_POOL ");
			sBuf.append(" AND d.PARTICIPANT_ID = diff.PARTICIPANT_ID ");
			sBuf.append(" AND d.DAT_BUSINESS_POOLRUN = diff.DAT_BUSINESS_POOLRUN ");
			sBuf.append(" AND d.TYP_CALCULATION = diff.TYP_CALCULATION ");
			sBuf.append(" AND d.SUB_TYP_CALCULATION = diff.SUB_TYP_CALCULATION ");
			sBuf.append(" AND d.NBR_SEQUENCE = diff.NBR_SEQUENCE ");												
			sBuf.append(" AND d.DAT_POSTING < TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "') ");
			sBuf.append(" ) ");
			//BVTAccrualBasedOnPosting Change Ends
			//sBuf.append(" AND amt_diff != 0 ");//DrCrAllocation
			sBuf.append(" AND typ_participant = 'ACCOUNT' ");
			sBuf.append(" AND flg_realpool = 'Y' ");
			sBuf.append(" AND sub_typ_calculation != 'R' ");
			sBuf.append(" AND sub_typ_calculation != 'T' ");//allocation posting fix
			sBuf.append(" GROUP BY participant_id) tmp ");
			//All participant accounts for which posting needs to be done
			sBuf.append(" WHERE bd.participant_id = tmp.participant_id ");
			sBuf.append(" AND bd.dat_business_poolrun = dat_value ");
			sBuf.append(" AND (dr_sum != 0 OR cr_sum != 0) ");//DrCrAllocation
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" AND bd.nbr_processid = "+nbrProcessId+") tmp2 ");
			sBuf.append(" AND bd.nbr_processid = ?) tmp2 ");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			//PoolIds of the accounts on the corresponding value date
			sBuf.append(" WHERE par.id_pool = tmp2.id_pool ");
			sBuf.append(" AND par.participant_id = tmp2.participant_id ");
			/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
			//SUP_HSBC_041 Fix starts
			// It's possible that partcipants of type pool & account with same nbr_ownaccout/id_pool exists for a given root pool
			sBuf.append(" AND par.typ_participant = 'ACCOUNT'");
			//SUP_HSBC_041 Fix ends
			/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" AND par.nbr_processid = "+nbrProcessId);
			sBuf.append(" AND par.nbr_processid = ?");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			sBuf.append(" AND par.dat_business_poolrun = tmp2.dat_value) tmp3 ");
			//Details of these accounts
			sBuf.append(" WHERE hdr.id_pool = tmp3.id_pool ");
			sBuf.append(" AND hdr.dat_business_poolrun = tmp3.dat_value ");
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" AND hdr.nbr_processid = "+nbrProcessId);
			sBuf.append(" AND hdr.nbr_processid = ?");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
			//SUP_HSBC_025 fix 
			//Cod_gl is missing
			//BVT Adjsutmet Accounting Entries should be generated per host.
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" AND hdr.cod_gl = '"+hostId+"'");
			sBuf.append(" AND hdr.cod_gl = ?");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			//SUP_HSBC_025 fix
			/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
			//Posting dates for these accounts
			sBuf.append(" UNION ");
			//sBuf.append(" SELECT participant_id, dat_value, amount, tmp3.id_pool, tmp3.cod_ccy, ");
			sBuf.append(" SELECT participant_id, dat_value, dr_sum, cr_sum, tmp3.id_pool, tmp3.cod_ccy, ");//DrCrAllocation
			sBuf.append(" nbr_coreaccount, cod_branch, tmp3.cod_gl, cod_bnk_lgl, nbr_ibanaccnt, ");
			sBuf.append(" flg_consolidation, dat_post_settlement, dat_post_debitdelay,  ");
			sBuf.append(" dat_post_creditdelay, ");
			sBuf.append(" (SELECT dat_post_debitdelay ");
			sBuf.append(" FROM opool_txnpoolhdr h1 ");
			sBuf.append(" WHERE h1.id_pool = hdr.id_pool ");
			// INT_1069 Start
			//sBuf.append(" AND h1.dat_business_poolrun = dat_value + 1 ");
			sBuf.append(" AND h1.dat_business_poolrun = hdr.dat_posting + 1 ");
			// sBuf.append(" TO_DATE ('" + strbusinessdate + "', '" + dateFormat + "') ");
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" AND h1.nbr_processid = " + nbrProcessId);
			sBuf.append(" AND h1.nbr_processid = ? ");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			// sBuf.append(" AND h1.nbr_processid = (SELECT MAX (NBR_PROCESSID) ");
			// sBuf.append(" FROM opool_txnpoolhdr h ");
			// sBuf.append(" WHERE h.id_pool = h1.id_pool ");
			// sBuf.append(" AND h.dat_business_poolrun = h1.dat_business_poolrun ");
			// sBuf.append(" )	");
			// INT_1069 End
			sBuf.append(" ) next_debit_delay, ");
			sBuf.append(" (SELECT dat_post_creditdelay ");
			sBuf.append(" FROM opool_txnpoolhdr h2 ");
			sBuf.append(" WHERE h2.id_pool = hdr.id_pool ");
			// INT_1069 Start
			//sBuf.append(" AND h2.dat_business_poolrun = dat_value + 1 ");
			sBuf.append(" AND h2.dat_business_poolrun = hdr.dat_posting + 1 ");
			// sBuf.append(" TO_DATE ('" + strbusinessdate + "', '" + dateFormat + "') ");
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" AND h2.nbr_processid = " + nbrProcessId);
			sBuf.append(" AND h2.nbr_processid = ?");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			// sBuf.append(" AND h2.nbr_processid = (SELECT MAX (NBR_PROCESSID) ");
			// sBuf.append(" FROM opool_txnpoolhdr h ");
			// sBuf.append(" WHERE h.id_pool = h2.id_pool ");
			// sBuf.append(" AND h.dat_business_poolrun = h2.dat_business_poolrun ");
			// sBuf.append(" )	");
			// INT_1069 End
			sBuf.append(" ) next_credit_delay , dat_posting ");
			sBuf.append(" FROM opool_txnpoolhdr hdr, ");
			//sBuf.append(" (SELECT participant_id, dat_value, amount, id_pool, cod_ccy, ");
			sBuf.append(" (SELECT participant_id, dat_value, dr_sum, cr_sum, id_pool, cod_ccy, ");//DrCrAllocation
			sBuf.append(" nbr_coreaccount, cod_branch, cod_gl, cod_bnk_lgl, ");
			sBuf.append(" nbr_ibanaccnt ");
			sBuf.append(" FROM orbicash_accounts acc, ");
			// INT_1069 Start
			// sBuf.append(" (SELECT tmp.participant_id, dat_value, amount, id_pool ");
			// sBuf.append(" FROM opool_txnpool_bvtdiff bd, ");
			sBuf.append(" (SELECT participant_id, ");
			sBuf.append(" MAX (dat_business_poolrun) dat_value, ");
			//sBuf.append(" SUM (amt_diff) amount, id_pool ");
			sBuf.append(" SUM (amt_dr_diff) dr_sum, SUM (amt_cr_diff) cr_sum, id_pool ");//DrCrAllocation
			
			//BVTAccrualBasedOnPosting Change Starts
			//sBuf.append(" FROM opool_txnpool_bvtdiff ");
			sBuf.append(" FROM opool_txnpool_bvtdiff diff");
			//BVTAccrualBasedOnPosting Change Ends

			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" WHERE nbr_processid = "+nbrProcessId);
			sBuf.append(" WHERE nbr_processid = ? ");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			
			//BVTAccrualBasedOnPosting Change Starts
			//sBuf.append(" AND dat_posting < ");
			//sBuf.append(" TO_DATE ('" + strbusinessdate + "', '" + dateFormat + "') ");
			sBuf.append(" AND dat_accrual <=  ");
			sBuf.append(" (SELECT MAX(dat_posting) ");
			sBuf.append(" FROM opool_txnpool_bvtdiff d ");
			sBuf.append(" WHERE d.NBR_PROCESSID = diff.NBR_PROCESSID ");
			sBuf.append(" AND d.ID_POOL = diff.ID_POOL ");
			sBuf.append(" AND d.PARTICIPANT_ID = diff.PARTICIPANT_ID ");
			sBuf.append(" AND d.DAT_BUSINESS_POOLRUN = diff.DAT_BUSINESS_POOLRUN ");
			sBuf.append(" AND d.TYP_CALCULATION = diff.TYP_CALCULATION ");
			sBuf.append(" AND d.SUB_TYP_CALCULATION = diff.SUB_TYP_CALCULATION ");
			sBuf.append(" AND d.NBR_SEQUENCE = diff.NBR_SEQUENCE ");												
			sBuf.append(" AND d.DAT_POSTING < TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "') ");
			sBuf.append(" ) ");
			//BVTAccrualBasedOnPosting Change Ends
			
			//sBuf.append(" AND amt_diff != 0 ");//DrCrAllocation
			sBuf.append(" AND typ_participant = 'CENTRAL' ");
			sBuf.append(" AND flg_realpool = 'Y' ");
			sBuf.append(" AND sub_typ_calculation != 'T' ");//allocation posting fix
			sBuf.append(" AND sub_typ_calculation != 'R' ");
			sBuf.append(" GROUP BY participant_id, id_pool ");
			// sBuf.append(" ) tmp ");
			//All central accounts for which posting needs to be done
			// sBuf.append(" WHERE bd.participant_id = tmp.participant_id ");
			// sBuf.append(" AND bd.dat_business_poolrun = dat_value ");
			// sBuf.append(" AND bd.nbr_processid = "+nbrProcessId+"
			sBuf.append(" ) tmp2 ");
			// INT_1069 End
			//sBuf.append(" WHERE acc.nbr_ownaccount = tmp2.participant_id) tmp3 ");
			sBuf.append(" WHERE acc.nbr_ownaccount = tmp2.participant_id AND (dr_sum != 0 OR cr_sum != 0) ) tmp3 ");//DrCrAllocation
			sBuf.append(" WHERE hdr.id_pool = tmp3.id_pool ");
			sBuf.append(" AND hdr.dat_business_poolrun = tmp3.dat_value ");
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" AND hdr.nbr_processid = "+nbrProcessId);
			sBuf.append(" AND hdr.nbr_processid = ?");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			//ENH_10007 Starts
			//BVT Adjsutmet Accounting Entries should be generated per host.
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" AND hdr.cod_gl = '"+hostId+"'");
			sBuf.append(" AND hdr.cod_gl = ?");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			//ENH_10007 Ends
			String sqlAllocPostingDtls = sBuf.toString();

			//modified and replaced DEBIT_DELAY,CREDIT_DELAY with next_debit_delay, next_credit_delay
			//respectively. dat_posting added as last column
			//INT_1055 Ends
			if (logger.isDebugEnabled())
				logger.debug("Query POSTING ALLOCATION: " + sqlAllocPostingDtls);

			/** ****** JTEST FIX start ************  on 14DEC09*/
//			rs = stmt.executeQuery(sqlAllocPostingDtls);
			
			pStmt = conn.prepareStatement(sqlAllocPostingDtls);
			pStmt.setLong(1, nbrProcessId);
			pStmt.setLong(2, nbrProcessId);
			pStmt.setLong(3, nbrProcessId);
			pStmt.setLong(4, nbrProcessId);
			pStmt.setLong(5, nbrProcessId);
			pStmt.setLong(6, nbrProcessId);
			pStmt.setString(7, hostId);
			pStmt.setLong(8, nbrProcessId);
			pStmt.setLong(9, nbrProcessId);
			pStmt.setLong(10, nbrProcessId);
			pStmt.setLong(11, nbrProcessId);
			pStmt.setString(12, hostId);
			rs = pStmt.executeQuery();
			/** ****** JTEST FIX end **************  on 14DEC09*/


			if (logger.isDebugEnabled()) {
				logger.debug(" Executed Query  POSTING ALLOCATION ");
			}

			results = false;
			String details = "";
			while(rs.next()){
				results = true;
				acctDoObj = new AccountingDO();
				poolId = "";
				accountingEntryId = "0";
				//SUP_1082b starts
				aEPostingAccrualId = "0";
				//SUP_1082b ends
				details = "";
				poolId = rs.getString("ID_POOL");
				//amountToPost = new BigDecimal(rs.getString("AMOUNT"));
				//DrCrAllocation starts
				amtToPostCreditP = rs.getBigDecimal("CR_SUM");
				amountToPostDebitP = rs.getBigDecimal("DR_SUM");
				// Pass separate entries for both debit and credit allocation.
				b_flgSeperate = true;
				//DrCrAllocation ends

				//fill in the AccountingDo object's attributes.
				acctDoObj.set_nbr_ownaccount(rs.getString("PARTICIPANT_ID"));
				acctDoObj.set_nbr_coreaccount(rs.getString("NBR_COREACCOUNT"));
				acctDoObj.set_cod_branch(rs.getString("COD_BRANCH"));
				acctDoObj.set_cod_gl(rs.getString("COD_GL"));
				acctDoObj.set_cod_bnk_lgl(rs.getString("COD_BNK_LGL"));
				acctDoObj.set_nbr_ibanacct(rs.getString("NBR_IBANACCNT"));
				acctDoObj.set_cod_ccy(rs.getString("COD_CCY"));

				//BigDecimal amtForAllocation = rs.getBigDecimal("SUM");
				//populate Dates
				hmFields.put("DAT_POST_SETTLEMENT",rs.getDate("DAT_POST_SETTLEMENT"));
				hmFields.put("DAT_POST_CREDITDELAY",rs.getDate("dat_post_creditdelay"));
				hmFields.put("DAT_POST_DEBITDELAY",rs.getDate("dat_post_debitdelay"));
				//INT_1055 Start
				//hmFields.put("BVT_CREDITDELAY",rs.getDate("CREDIT_DELAY"));
				//hmFields.put("BVT_DEBITDELAY",rs.getDate("DEBIT_DELAY"));
				hmFields.put("NEXT_CREDIT_DELAY",rs.getDate("NEXT_CREDIT_DELAY"));
				hmFields.put("NEXT_DEBIT_DELAY",rs.getDate("NEXT_DEBIT_DELAY"));
				hmFields.put("DAT_VALUE",rs.getDate("DAT_POSTING"));
				postSettlementDate = (Date)hmFields.get("DAT_POST_SETTLEMENT");
				// INT_1055 End
				//CAL fix starts
				java.util.Date dbNextCreditDelay = rs.getDate("NEXT_CREDIT_DELAY");
				java.util.Date dbNextDebitDelay = rs.getDate("NEXT_DEBIT_DELAY");
				String codGL = rs.getString("COD_GL");
				java.util.Date[] calcPostingDates = null;
				//SUP_1082c starts
//				if (dbNextCreditDelay == null || dbNextDebitDelay == null) {
//				    calcPostingDates = calculateNextCyclePostingDates(conn, poolId, codGL, nbrProcessId, businessdate);
//					}
				if(postCache == null || postCache.get(poolId) == null){
				    if(logger.isWarnEnabled()){
				        logger.warn("Error forming the posting cache OR poolId ["+poolId+"] postingDates not present in the cache.");
				        logger.warn("So getting the dates from database for "+poolId+".");
				    }
					calcPostingDates = calculateNextCyclePostingDates(conn, poolId, codGL, nbrProcessId, businessdate);
				}
				else{
				    calcPostingDates = (Date[])postCache.get(poolId);
				}
				//SUP_1082c ends
				//CAL fix ends
				//put extraDetails and flagAllocation in fieldsHashMap
				//extraDetails = "ALLOCATOR IS " + rs.getString("ALLOCATOR")+ "TYP_SETTLE IS " + rs.getString("TYP_SETTLE");
				details = "ALLOCATION";

				if (logger.isDebugEnabled()) {
					logger.debug("Account Object for Allocation" + acctDoObj);
				}

				bd_amtNotPosted = setUnpostedAmounts(details,acctDoObj,
										pstmtAccountUnposted,businessdate,ExecutionConstants.POSTING);
				if (logger.isDebugEnabled()) {
				  logger.debug("Unposted Amount "  + bd_amtNotPosted +"for ALLOCATION");
				 }
				 //chk if any amount is left to be posted
				/*
				if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
					amountToPost = amountToPost.add(bd_amtNotPosted);
					if (logger.isDebugEnabled()) {
						logger.debug("Adding Unposted Amount  "  + bd_amtNotPosted +"for ALLOCATION "+amountToPost);
					 }
				 }// UNPOSTED ALLOCATION
				 */
				//based on amountToPost, decide if it is CREDIT/DEBIT DELAY DAYS FOR POSTING DATE
				//DrCrAllocation Starts
				//amtToPostCreditP, amountToPostDebitP should not be null
				amountToPost = amtToPostCreditP.subtract(amountToPostDebitP);
				//Separate entries
				if(b_flgSeperate){
					// Credit Allocation
					if (amtToPostCreditP.compareTo(zero) != 0){
						if (postSettlementDate.compareTo(businessdate) < 0){
					        //SUP_1082c starts
					        hmFields.put("DAT_VALUE",calcPostingDates[0]);
//					        actualPostingDate = (Date)hmFields.get("NEXT_CREDIT_DELAY");
//							if(actualPostingDate == null){
//								// Calculate the NEXT_CREDIT_DELAY  and assign it to actualPostingDate 
//								actualPostingDate = calcPostingDates[3];
//								if(logger.isDebugEnabled()){
//									logger.debug(" calculated next_credit_delay is "+ actualPostingDate);
//								}
//							}
								actualPostingDate = calcPostingDates[3];
					        //SUP_1082c ends
					    }
					    else
					        actualPostingDate = (Date)hmFields.get("DAT_POST_CREDITDELAY");
						//Call AE
						hmFields.put("DAT_POSTING",actualPostingDate);
						valueDate = (Date)hmFields.get("DAT_VALUE");
						hmFields.put("DAT_SENT", addOffsetToDate(valueDate, 1));
						
						//SUP_1082b starts
						hmAccrualFields = new HashMap(hmFields);
						hmAccrualFields.put(ExecutionConstants.PROCESSID,nbrProcessId); // Mashreq_AE_Hook
						hmAccrualFields.put(ExecutionConstants.BUSINESSDATE, businessdate); // Mashreq_AE_Hook fix
						if (accCache == null || accCache.get(poolId)== null) {
						    throw new LMSException("101302"," Error occured in forming the cache ");
			            }
						Date[] accDates = (Date[])accCache.get(poolId);
//						if(accDates[0] == null || accDates[0].compareTo(businessdate)<0){
//						    hmAccrualFields.put("DAT_SENT", accDates[2]);
//						    hmAccrualFields.put("DAT_POSTING",accDates[3]);
//						}
//						else {
						//Always consider the next cycle accrual dates
						    hmAccrualFields.put("DAT_SENT", accDates[0]);
						    hmAccrualFields.put("DAT_POSTING",accDates[1]);
//		                }								
						//SUP_1082b ends
						/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
						//SUP_HSBC_025 Starts
						details = ExecutionConstants.ALLOCATIONCR;
						//SUP_HSBC_025 Ends
						/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
						
						//SUP_1082b starts
						aEPostingAccrualId = "" + processBVTEntry(acctDoObj,
 							amtToPostCreditP,ExecutionConstants.ACCRUAL,
 							ExecutionConstants.BVT_CREDIT, details,pstmtAccountingEntry,
 							//SUP_1082b starts
 							//pstmtAcctUnpostedInsert,poolId,hmFields);
 							/** ****** JTEST FIX start ************  on 14DEC09*/
// 							pstmtAcctUnpostedInsert,poolId,hmAccrualFields);
							pstmtAcctUnpostedInsert,poolId,hmAccrualFields, conn);
 							/** ****** JTEST FIX end **************  on 14DEC09*/
							//SUP_1082b ends
						//accountingEntryId = "" + processBVTEntry(acctDoObj,
                        accountingEntryId = aEPostingAccrualId+ "|" + processBVTEntry(acctDoObj,
                        //SUP_1082b ends
							amtToPostCreditP,ExecutionConstants.POSTING,
							ExecutionConstants.BVT_CREDIT, details,pstmtAccountingEntry,
							/** ****** JTEST FIX start ************  on 14DEC09*/
//							pstmtAcctUnpostedInsert,poolId,hmFields);
                        	pstmtAcctUnpostedInsert,poolId,hmFields, conn);
							/** ****** JTEST FIX end **************  on 14DEC09*/
						
					}
					else{
						logger.fatal("Credit Allocation is 0 for"  + acctDoObj + "SO not posting the amount ");
					}
					// DEBIT Allocation
					if (amountToPostDebitP.compareTo(zero) != 0){
						if (postSettlementDate.compareTo(businessdate) < 0){
					        //SUP_1082c starts
					        hmFields.put("DAT_VALUE",calcPostingDates[0]);
//					        actualPostingDate = (Date)hmFields.get("NEXT_DEBIT_DELAY");
//							if(actualPostingDate == null){
//								// Calculate the NEXT_DEBIT_DELAY  and assign it to actualPostingDate 
//								actualPostingDate = calcPostingDates[4];
//								if(logger.isDebugEnabled()){
//									logger.debug(" calculated next_debit_delay is "+ actualPostingDate);
//								}
//							}
								actualPostingDate = calcPostingDates[4];
					        //SUP_1082c ends
					    }
					    else
					        actualPostingDate = (Date)hmFields.get("DAT_POST_DEBITDELAY");
						//Call AE
						hmFields.put("DAT_POSTING",actualPostingDate);
						valueDate = (Date)hmFields.get("DAT_VALUE");
						hmFields.put("DAT_SENT", addOffsetToDate(valueDate, 1));
						
						//SUP_1082b starts
						hmAccrualFields = new HashMap(hmFields);
						hmAccrualFields.put(ExecutionConstants.PROCESSID,nbrProcessId); // Mashreq_AE_Hook
						hmAccrualFields.put(ExecutionConstants.BUSINESSDATE, businessdate); // Mashreq_AE_Hook fix
						if (accCache == null || accCache.get(poolId)== null) {
						    throw new LMSException("101302"," Error occured in forming the cache ");
			            }
						Date[] accDates = (Date[])accCache.get(poolId);
//						if(accDates[0] == null || accDates[0].compareTo(businessdate)<0){
//						    hmAccrualFields.put("DAT_SENT", accDates[2]);
//						    hmAccrualFields.put("DAT_POSTING",accDates[3]);
//						}
//						else {
						//Always consider the next cycle accrual dates
						    hmAccrualFields.put("DAT_SENT", accDates[0]);
						    hmAccrualFields.put("DAT_POSTING",accDates[1]);
//		                }								
						//SUP_1082b ends
						/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
						//SUP_HSBC_025 Starts
						details = ExecutionConstants.ALLOCATIONDR;
						//SUP_HSBC_025 Ends
						/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
						
						//SUP_1082b starts
						aEPostingAccrualId = "" + processBVTEntry(acctDoObj,
  							amountToPostDebitP,ExecutionConstants.ACCRUAL,
							ExecutionConstants.BVT_DEBIT, details,pstmtAccountingEntry,
							//SUP_1082b starts
							//pstmtAcctUnpostedInsert,poolId,hmFields);
							/** ****** JTEST FIX start ************  on 14DEC09*/
							pstmtAcctUnpostedInsert,poolId,hmAccrualFields ,conn);
							/** ****** JTEST FIX end **************  on 14DEC09*/
							//SUP_1082b ends
						//accountingEntryId = "" + processBVTEntry(acctDoObj,
                        accountingEntryId = aEPostingAccrualId+ "|" + processBVTEntry(acctDoObj,
                        //SUP_1082b ends
							amountToPostDebitP,ExecutionConstants.POSTING,
							ExecutionConstants.BVT_DEBIT, details,pstmtAccountingEntry,
							/** ****** JTEST FIX start ************  on 14DEC09*/
							pstmtAcctUnpostedInsert,poolId,hmFields ,conn);
							/** ****** JTEST FIX end **************  on 14DEC09*/

					}
					else{
						logger.fatal("Debit Allocation is 0 for"  + acctDoObj + "SO not posting the amount ");
					}						
				}
//				if (amountToPost.compareTo(zero) != 0){
//					if (amountToPost.compareTo(zero) > 0){
//						//INT_1055 Start
//					    if (postSettlementDate.compareTo(businessdate) < 0){
//					        actualPostingDate = (Date)hmFields.get("NEXT_CREDIT_DELAY");
//					        //CAL fix starts
//							//actualPostingDate = actualPostingDate != null ? actualPostingDate
//					        //                                              : businessdate;
//							if(actualPostingDate == null){
//								// Calculate the NEXT_CREDIT_DELAY  and assign it to actualPostingDate 
//								actualPostingDate = calcPostingDates[3];
//								if(logger.isDebugEnabled()){
//									logger.debug(" calculated next_credit_delay is "+ actualPostingDate);
//								}
//							}
//							//CAL fix ends
//					    }
//					    else
//					        actualPostingDate = (Date)hmFields.get("DAT_POST_CREDITDELAY");
//					    //postingDate = (Date)hmFields.get("DAT_POST_CREDITDELAY");
//					    //sendingDate = (Date)hmFields.get("CREDIT_DELAY");
//					    //INT_1055 End
//					}else{
//						//INT_1055 Start
//					    if (postSettlementDate.compareTo(businessdate) < 0){
//					        actualPostingDate = (Date)hmFields.get("NEXT_DEBIT_DELAY");
//					        
//							//CAL fix starts
//							//actualPostingDate = actualPostingDate != null ? actualPostingDate
//                            //                      : businessdate;
//							if(actualPostingDate == null){
//								// Calculate the NEXT_DEBIT_DELAY  and assign it to actualPostingDate 
//								actualPostingDate = calcPostingDates[4];
//								if(logger.isDebugEnabled()){
//									logger.debug(" calculated next_debit_delay is "+ actualPostingDate);
//								}
//							}
//							//CAL fix ends
//					    }
//					    else
//					        actualPostingDate = (Date)hmFields.get("DAT_POST_DEBITDELAY");
//
//					    //postingDate = (Date)hmFields.get("DAT_POST_DEBITDELAY");
//					    //sendingDate = (Date)hmFields.get("DEBIT_DELAY");
//					    //INT_1055 End
//					}
//					//INT_1055 Start
//				    //sendingDate = sendingDate!=null?sendingDate:businessdate;
//				    //hmFields.put("DAT_POSTING",postingDate);
//					hmFields.put("DAT_POSTING",actualPostingDate);
//					valueDate = (Date)hmFields.get("DAT_VALUE");
//					// hmFields.put("DAT_SENT",sendingDate);
//					hmFields.put("DAT_SENT", addOffsetToDate(valueDate, 1));
//					// INT_1055 End
//					accountingEntryId = "" + processBVTEntry(acctDoObj,
//							amountToPost,ExecutionConstants.POSTING,
//							ExecutionConstants.NET, details,pstmtAccountingEntry,
//							pstmtAcctUnpostedInsert,poolId,hmFields);//INT_1039
//				}else{
//					logger.fatal(" Not Posting  Amount as amountTo Post = 0 amount for account " + acctDoObj);
//				}
				//DrCrAllocation Ends
				//update Allocation entries
				//postingEntries[ExecutionConstants.OLM_ACCOUNTINGENTRY_INSERT] = true;
				postingEntries2[ExecutionConstants.ALLOCATION_UPDATE] = true;
			}
			if (!results){
				logger.fatal(" No records for Posting -Allocation" + sqlAllocPostingDtls);
			}
			rs.close();

			/////////////pool settlement ///////////////////////////////////////////////////
//INT_1055 Starts
//			  String sqlSettlementPosting = new StringBuffer("SELECT  ")
//			  .append("   participant_id, dat_value, sum_dr_int, sum_cr_int, h.nbr_processid, h.id_pool, flg_def_drcr_consol, ")
//			  .append("   nbr_coreaccount,cod_ccy, cod_branch, cod_bnk_lgl, nbr_ibanaccnt,  NVL(amt_min_cr,0) amt_min_cr, NVL(amt_min_dr,0) amt_min_dr, acct_settle_cr, acct_settle_dr, ")
//			  .append("   h.dat_business_poolrun, h.nbr_lotid, h.cod_gl, dat_posting, dat_post_settlement, dat_post_debitdelay, ")
//			  .append("   dat_post_creditdelay, ")
//              .append("   (SELECT DISTINCT HDR.DAT_POST_DEBITDELAY FROM OPOOL_TXNPOOL_PARTYDTLS PAR, OPOOL_TXNPOOLHDR HDR WHERE ")
//			  .append("   HDR.ID_POOL=PAR.ID_POOL AND HDR.DAT_BUSINESS_POOLRUN = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')  ) DEBIT_DELAY, ")
//			  .append("   (SELECT DISTINCT DAT_POST_CREDITDELAY FROM OPOOL_TXNPOOL_PARTYDTLS PAR, OPOOL_TXNPOOLHDR HDR WHERE ")
//			  .append("   HDR.ID_POOL=PAR.ID_POOL AND HDR.DAT_BUSINESS_POOLRUN = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')  )  CREDIT_DELAY ")
//			  .append("   from opool_txnpoolhdr h, ")
//			  .append("	  (select p.participant_id, dat_value, sum_dr_int, sum_cr_int, nbr_processid, id_pool, flg_def_drcr_consol, ")
//			  .append("   nbr_coreaccount,COD_PARTY_CCY cod_ccy, cod_branch, cod_bnk_lgl, nbr_ibanaccnt, NVL(amt_min_cr,0) amt_min_cr, NVL(amt_min_dr,0) amt_min_dr ")
//			  .append("   from opool_txnpool_partydtls p, ")
//			  .append("   (select participant_id, max(dat_posting) dat_value, sum(tmp.dr_int) sum_dr_int, ")
//			  .append("   sum(tmp.cr_int) sum_cr_int ")
//			  .append("   from   ")
//			  .append("   (select participant_id, cod_party_ccy, dat_posting, ")
//			  .append("   (CASE WHEN (hdr.TYP_ACCTINT_DRACT = 'POST' OR ")
//			  .append("   (hdr.AMT_POOL_INTCALC_BALANCE < 0 AND hdr.TYP_ACCTINT_DRACT = 'SUPPDR') OR ")
//			  .append("   (hdr.AMT_POOL_INTCALC_BALANCE >= 0 AND hdr.TYP_ACCTINT_CRACT = 'SUPPCR')) ")
//			  .append("   THEN par.AMT_DR_INTEREST  ELSE 0 END  ) dr_int, ")
//			  .append("   (CASE WHEN (hdr.TYP_ACCTINT_CRACT = 'POST' OR (hdr.AMT_POOL_INTCALC_BALANCE < 0 AND hdr.TYP_ACCTINT_CRACT = 'SUPPDR') OR ")
//			  .append("   (hdr.AMT_POOL_INTCALC_BALANCE >= 0 AND hdr.TYP_ACCTINT_CRACT = 'SUPPCR')) ")
//			  .append("    THEN par.AMT_CR_INTEREST ELSE 0 END ) cr_int")
//			  .append("   from opool_txnpoolhdr hdr, opool_txnpool_partydtls par ")
//			  .append("   where hdr.nbr_processid = par.nbr_processid and hdr.id_pool = par.id_pool ")
//			  .append("	  and hdr.dat_business_poolrun = par.dat_business_poolrun  ")
//			  .append("   and hdr.nbr_processid= " + nbrProcessId)
//			  .append("   and dat_posting < TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')  ")
//			  .append("   and flg_realpool = 'Y'   ")
//			  .append("   and typ_posting = 'R' ")
//			  .append("   and cod_counter_account = 'S' ")
//			  .append("   and typ_participant = 'ACCOUNT' ")
//			  .append("   and (TYP_ACCTINT_CRACT <> 'SUPP' AND TYP_ACCTINT_DRACT <> 'SUPP') ")
//			  .append("   ) tmp group by participant_id ) reqd ")
//			  .append("   where p.dat_business_poolrun = dat_value ")
//			  .append("   and p.participant_id = reqd.participant_id ")
//			  .append("   and p.nbr_processid=" + nbrProcessId)
//			  .append("   ) most where h.nbr_processid = most.nbr_processid ")
//			  .append("   and h.dat_business_poolrun = most.dat_value ")
//			  .append("   and h.id_pool = most.id_pool ")
//			  .toString();
			sBuf.setLength(0);
			sBuf.append(" SELECT participant_id, dat_value, sum_dr_int, sum_cr_int, h.nbr_processid, ");
			sBuf.append(" h.id_pool, flg_def_drcr_consol, nbr_coreaccount, cod_ccy, cod_branch, ");
			sBuf.append(" cod_bnk_lgl, nbr_ibanaccnt, NVL (amt_min_cr, 0) amt_min_cr, ");
			sBuf.append(" NVL (amt_min_dr, 0) amt_min_dr, acct_settle_cr, acct_settle_dr, ");
			sBuf.append(" h.dat_business_poolrun, h.nbr_lotid, h.cod_gl, dat_posting, ");
			sBuf.append(" dat_post_settlement, dat_post_debitdelay, dat_post_creditdelay, ");
			sBuf.append(" (SELECT dat_post_debitdelay ");
			sBuf.append(" FROM opool_txnpoolhdr h1 ");
			sBuf.append(" WHERE h1.id_pool = h.id_pool ");
			// INT_1069 Start
			//sBuf.append(" AND h1.dat_business_poolrun = dat_value + 1 ");
			sBuf.append(" AND h1.dat_business_poolrun = h.dat_posting + 1 ");
			// sBuf.append(" TO_DATE ('" + strbusinessdate + "', '" + dateFormat + "') ");
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" AND h1.nbr_processid = " + nbrProcessId);
			sBuf.append(" AND h1.nbr_processid = ? ");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			// sBuf.append(" AND h1.nbr_processid = (SELECT MAX (NBR_PROCESSID) ");
			// sBuf.append(" FROM opool_txnpoolhdr h ");
			// sBuf.append(" WHERE h.id_pool = h1.id_pool ");
			// sBuf.append(" AND h.dat_business_poolrun = h1.dat_business_poolrun ");
			// sBuf.append(" )	");
			// INT_1069 End
			sBuf.append(" ) next_debit_delay, ");
			sBuf.append(" (SELECT dat_post_creditdelay ");
			sBuf.append(" FROM opool_txnpoolhdr h2 ");
			sBuf.append(" WHERE h2.id_pool = h.id_pool ");
			// INT_1069 Start
			//sBuf.append(" AND h2.dat_business_poolrun = dat_value + 1 ");
			sBuf.append(" AND h2.dat_business_poolrun = h.dat_posting + 1 ");
			// sBuf.append(" TO_DATE ('" + strbusinessdate + "', '" + dateFormat + "') ");
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" AND h2.nbr_processid = " + nbrProcessId);
			sBuf.append(" AND h2.nbr_processid = ? ");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			// sBuf.append(" AND h2.nbr_processid = (SELECT MAX (NBR_PROCESSID) ");
			// sBuf.append(" FROM opool_txnpoolhdr h ");
			// sBuf.append(" WHERE h.id_pool = h2.id_pool ");
			// sBuf.append(" AND h.dat_business_poolrun = h2.dat_business_poolrun ");
			// sBuf.append(" )	");
			// INT_1069 End
			sBuf.append(" ) next_credit_delay ");
			sBuf.append(" FROM opool_txnpoolhdr h, ");
			sBuf.append(" (SELECT p.participant_id, dat_value, sum_dr_int, sum_cr_int, ");
			sBuf.append(" nbr_processid, id_pool, flg_def_drcr_consol, nbr_coreaccount, ");
			sBuf.append(" cod_party_ccy cod_ccy, cod_branch, cod_bnk_lgl, nbr_ibanaccnt, ");
			sBuf.append(" NVL (amt_min_cr, 0) amt_min_cr, NVL (amt_min_dr, 0) amt_min_dr ");
			sBuf.append(" FROM opool_txnpool_partydtls p, ");
			//SUP_1079 starts
			//sBuf.append(" (SELECT participant_id, MAX (dat_posting) dat_value, ");
			sBuf.append(" (SELECT participant_id, MAX (dat_business_poolrun) dat_value, ");
			//SUP_1079 ends
			sBuf.append(" SUM (tmp.dr_int) sum_dr_int, ");
			sBuf.append(" SUM (tmp.cr_int) sum_cr_int ");
						//SUP_1079 fix starts
//			sBuf.append(" FROM (SELECT participant_id, cod_party_ccy, dat_posting, ");
//			sBuf.append(" (CASE ");
//			sBuf.append(" WHEN ( hdr.typ_acctint_dract = 'POST' ");
//			sBuf.append(" OR ( hdr.amt_pool_intcalc_balance < ");
//			sBuf.append(" 0 ");
//			sBuf.append(" AND hdr.typ_acctint_dract = ");
//			sBuf.append(" 'SUPPDR' ");
//			sBuf.append(" ) ");
//			sBuf.append(" OR ( hdr.amt_pool_intcalc_balance >= ");
//			sBuf.append(" 0 ");
//			sBuf.append(" AND hdr.typ_acctint_cract = ");
//			sBuf.append(" 'SUPPCR' ");
//			sBuf.append(" ) ");
//			sBuf.append(" ) ");
//			sBuf.append(" THEN par.amt_dr_interest ");
//			sBuf.append(" ELSE 0 ");
//			sBuf.append(" END ");
//			sBuf.append(" ) dr_int, ");
//			sBuf.append(" (CASE ");
//			sBuf.append(" WHEN ( hdr.typ_acctint_cract = 'POST' ");
//			sBuf.append(" OR ( hdr.amt_pool_intcalc_balance < ");
//			sBuf.append(" 0 ");
//			sBuf.append(" AND hdr.typ_acctint_cract = ");
//			sBuf.append(" 'SUPPDR' ");
//			sBuf.append(" ) ");
//			sBuf.append(" OR ( hdr.amt_pool_intcalc_balance >= ");
//			sBuf.append(" 0 ");
//			sBuf.append(" AND hdr.typ_acctint_cract = ");
//			sBuf.append(" 'SUPPCR' ");
//			sBuf.append(" ) ");
//			sBuf.append(" ) ");
//			sBuf.append(" THEN par.amt_cr_interest ");
//			sBuf.append(" ELSE 0 ");
//			sBuf.append(" END ");
//			sBuf.append(" ) cr_int ");
//			sBuf.append(" FROM opool_txnpoolhdr hdr, ");
//			sBuf.append(" opool_txnpool_partydtls par ");
//			sBuf.append(" WHERE hdr.nbr_processid = par.nbr_processid ");
//			sBuf.append(" AND hdr.id_pool = par.id_pool ");
//			sBuf.append(" AND hdr.dat_business_poolrun = ");
//			sBuf.append(" par.dat_business_poolrun ");
//			sBuf.append(" AND hdr.nbr_processid = " + nbrProcessId);
//			sBuf.append(" AND dat_posting < ");
//			sBuf.append(" TO_DATE ('" + strbusinessdate + "', '" + dateFormat + "') ");
//			sBuf.append(" AND flg_realpool = 'Y' ");
//			sBuf.append(" AND typ_posting = 'R' ");
//			sBuf.append(" AND cod_counter_account = 'S' ");
//			sBuf.append(" AND typ_participant = 'ACCOUNT' ");
//			sBuf.append(" AND ( typ_acctint_cract <> 'SUPP' ");
//			sBuf.append(" AND typ_acctint_dract <> 'SUPP' ");
//			sBuf.append(" )) tmp ");
			sBuf.append(" FROM (SELECT participant_id, par.CURRENCY, hdr.dat_business_poolrun, ");
			sBuf.append(" par.AMT_DR_INTEREST_DIFF dr_int, par.AMT_CR_INTEREST_DIFF cr_int ");
			sBuf.append(" FROM opool_txnpoolhdr hdr, opool_txnpool_bvtdiff_int par ");
			//SUP_1079 fix ends
			sBuf.append(" WHERE hdr.nbr_processid = par.nbr_processid ");
			sBuf.append(" AND hdr.id_pool = par.id_pool ");
			//SUP_1079 starts
			sBuf.append(" AND hdr.dat_business_poolrun = par.dat_business_poolrun ");
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" AND hdr.nbr_processid = "+nbrProcessId);
			sBuf.append(" AND hdr.nbr_processid = ? ");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			sBuf.append(" AND hdr.dat_posting < TO_DATE ('"+strbusinessdate+"', '" + dateFormat + "') ");
			sBuf.append(" AND hdr.flg_realpool = 'Y' ");
			//SUP_1079 ends
			sBuf.append(" AND typ_posting = 'R' ");
			sBuf.append(" AND cod_counter_account = 'S' ");
			//SUP_1079 fix starts
			sBuf.append(" AND typ_participant = 'ACCOUNT') tmp ");
			//SUP_1079 fix ends
			sBuf.append(" GROUP BY participant_id) reqd ");
			sBuf.append(" WHERE p.dat_business_poolrun = dat_value ");
			sBuf.append(" AND p.participant_id = reqd.participant_id ");
			/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
			//SUP_HSBC_041 Fix starts
			// It's possible that partcipants of type pool & account with same nbr_ownaccout/id_pool exists for a given root pool
			sBuf.append(" AND p.typ_participant = 'ACCOUNT'");
			//SUP_HSBC_041 Fix ends
			/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" AND p.nbr_processid = " + nbrProcessId+") most ");
			sBuf.append(" AND p.nbr_processid = ? ) most ");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			sBuf.append(" WHERE h.nbr_processid = most.nbr_processid ");
			sBuf.append(" AND h.dat_business_poolrun = most.dat_value ");
			sBuf.append(" AND h.id_pool = most.id_pool ");
			//SUP_1079 fix starts
			sBuf.append(" AND (sum_dr_int != 0 OR sum_cr_int != 0) ");
			//SUP_1079 fix ends
			//ENH_10007 Starts
			//BVT Adjsutmet Accounting Entries should be generated per host.
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" AND h.cod_gl = '"+hostId+"'");
			sBuf.append(" AND h.cod_gl = ?");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			//ENH_10007 Ends
			//modified and replaced DEBIT_DELAY,CREDIT_DELAY with next_debit_delay, next_credit_delay
			//respectively.
			String sqlSettlementPosting = sBuf.toString();
			//INT_1055 Ends

			  if (logger.isDebugEnabled())
				logger.debug("Query POSTING SETTLEMENT : " + sqlSettlementPosting);
			  /** ****** JTEST FIX start ************  on 14DEC09*/
//			  rs = stmt.executeQuery(sqlSettlementPosting);
			  
			  pStmt = conn.prepareStatement(sqlSettlementPosting);
			  pStmt.setLong(1, nbrProcessId);
			  pStmt.setLong(2, nbrProcessId);
			  pStmt.setLong(3, nbrProcessId);
			  pStmt.setLong(4, nbrProcessId);
			  pStmt.setString(5, hostId);
			  rs = pStmt.executeQuery();
			  /** ****** JTEST FIX end **************  on 14DEC09*/
			  if (logger.isDebugEnabled()) {
					logger.debug(" Executed Query  POSTING SETTLEMENT ");
			  }
			  results = false;
				while(rs.next()){
					results = true;
					acctDoObj = new AccountingDO();
					String flg_def_drcr_consol = rs.getString("FLG_DEF_DRCR_CONSOL");
					b_flgSeperate = true;
					accountingEntryId = "0";
					details = "";
					poolId = "";
					BigDecimal amtToPostCredit = new BigDecimal(rs.getString("SUM_CR_INT"));
					BigDecimal amountToPostDebit = new BigDecimal(rs.getString("SUM_DR_INT"));
					poolId = rs.getString("ID_POOL");
					if (logger.isDebugEnabled()) {
						logger.debug("Resultset flg_def_drcr_consol" + flg_def_drcr_consol);
						logger.debug("Resultset POOLID" + poolId);
					}
					if("Y".equalsIgnoreCase(flg_def_drcr_consol!=null
																?flg_def_drcr_consol.trim()
																:"" )){
						//Get the NetInterest,if consolidate
						amountToPost = amtToPostCredit.subtract(amountToPostDebit);
						// Set the seperate posting flag to False
						b_flgSeperate = false;
					}
					if (logger.isDebugEnabled()) {
						logger.debug(" Flag Separate" + b_flgSeperate);
					}
					//fill in the AccountingDo object's attributes.
					acctDoObj.set_cod_bnk_lgl(rs.getString("COD_BNK_LGL"));
					acctDoObj.set_cod_branch(rs.getString("COD_BRANCH"));
					acctDoObj.set_cod_gl(rs.getString("COD_GL"));
					acctDoObj.set_cod_ccy(rs.getString("COD_CCY"));
					acctDoObj.set_nbr_coreaccount(rs.getString("NBR_COREACCOUNT"));
					acctDoObj.set_nbr_ibanacct(rs.getString("NBR_IBANACCNT"));
					acctDoObj.set_nbr_ownaccount(rs.getString("PARTICIPANT_ID"));
					//get the settlement accounts too as the entries for this would be central and participant
					//fetch and set TXN.ACCT_SETTLE_CR,
					//TXN.ACCT_SETTLE_DR, this will be one of the legs
					acctDoObj.setCrBeneficiaryAccount(rs.getString("ACCT_SETTLE_CR"));
					acctDoObj.setDrBeneficiaryAccount(rs.getString("ACCT_SETTLE_DR"));
					//SUP_1079a starts
//					minAmtCredit = rs.getBigDecimal("AMT_MIN_CR");
//					minAmtDebit = rs.getBigDecimal("AMT_MIN_DR");
					//SUP_1079a ends
					hmFields.put("DAT_POST_SETTLEMENT",rs.getDate("DAT_POST_SETTLEMENT"));
					hmFields.put("DAT_POST_CREDITDELAY",rs.getDate("DAT_POST_CREDITDELAY"));
					hmFields.put("DAT_POST_DEBITDELAY",rs.getDate("DAT_POST_DEBITDELAY"));
					// INT_1055 Start
					// hmFields.put("BVT_CREDITDELAY",rs.getDate("CREDIT_DELAY"));
					// hmFields.put("BVT_DEBITDELAY",rs.getDate("DEBIT_DELAY"));
					hmFields.put("NEXT_CREDIT_DELAY",rs.getDate("NEXT_CREDIT_DELAY"));
					hmFields.put("NEXT_DEBIT_DELAY",rs.getDate("NEXT_DEBIT_DELAY"));
					hmFields.put("DAT_VALUE",rs.getDate("DAT_POSTING"));
					postSettlementDate = (Date)hmFields.get("DAT_POST_SETTLEMENT");
					// INT_1055 End
					//CAL fix starts
					java.util.Date dbNextCreditDelay = rs.getDate("NEXT_CREDIT_DELAY");
					java.util.Date dbNextDebitDelay = rs.getDate("NEXT_DEBIT_DELAY");
					String codGL = rs.getString("COD_GL");
					java.util.Date[] calcPostingDates = null;
				    //SUP_1082c starts
//					if(dbNextCreditDelay == null || dbNextDebitDelay == null){
//					    calcPostingDates = calculateNextCyclePostingDates(conn, poolId, codGL, nbrProcessId, businessdate);
//					}
					if(postCache == null || postCache.get(poolId) == null){
					    if(logger.isWarnEnabled()){
					        logger.warn("Error forming the posting cache OR poolId ["+poolId+"] postingDates not present in the cache.");
					        logger.warn("So getting the dates from database for "+poolId+".");
					    }
						calcPostingDates = calculateNextCyclePostingDates(conn, poolId, codGL, nbrProcessId, businessdate);
					}
					else{
					    calcPostingDates = (Date[])postCache.get(poolId);
					}
					//SUP_1082c ends
					//CAL fix ends
					if (logger.isDebugEnabled()) {
						logger.debug("Account Object" + acctDoObj);
					}
					// check is any amount is left due to be posted.
					// checkAmtNotPosted()
					if(b_flgSeperate){
						//POSTING CREDIT INTEREST
						/*bd_amtNotPosted = new BigDecimal(0);//RESETTING AMOUNT NOT POSTED
						bd_amtNotPosted = setUnpostedAmounts(ExecutionConstants.CENTRAL_INTEREST,acctDoObj,
							pstmtAccountUnposted,businessdate,ExecutionConstants.POSTING);*/

						if (amtToPostCredit.compareTo(zero)!=0){
							//also check for minm CR Interest
							//SUP_1079a starts
							//if(amtToPostCredit.compareTo(minAmtCredit)>=0){
							//SUP_1079a ends
								// INT_1055 Start
							// sendingDate = (Date)hmFields.get("CREDIT_DELAY");
							// postingDate = (Date)hmFields.get("DAT_POST_CREDITDELAY");
							 if (postSettlementDate.compareTo(businessdate) < 0){
						        //SUP_1082c starts
						        hmFields.put("DAT_VALUE",calcPostingDates[0]);
//							     actualPostingDate = (Date)hmFields.get("NEXT_CREDIT_DELAY");
//							     //CAL fix starts
//								 //actualPostingDate = actualPostingDate != null ? actualPostingDate
//							     //                                              : businessdate;
//								 if(actualPostingDate == null){
//									 // Calculate the NEXT_CREDIT_DELAY  and assign it to actualPostingDate 
//									 actualPostingDate = calcPostingDates[3];
//									 if(logger.isDebugEnabled()){
//									 	logger.debug(" calculated next_credit_delay is "+ actualPostingDate);
//									 }
//								 }
//								 //CAL fix ends
									 actualPostingDate = calcPostingDates[3];
						        //SUP_1082c ends
							 }
							 else
							     actualPostingDate = (Date)hmFields.get("DAT_POST_CREDITDELAY");

							 hmFields.put("DAT_POSTING",actualPostingDate);
							 valueDate = (Date)hmFields.get("DAT_VALUE");
							 // sendingDate = sendingDate!=null?sendingDate:businessdate;
							 // hmFields.put("DAT_POSTING",postingDate);
							 // hmFields.put("DAT_SENT",sendingDate);
							 hmFields.put("DAT_SENT", addOffsetToDate(valueDate, 1));
							 // INT_1055 End
								accountingEntryId = "" + processBVTEntry(acctDoObj,
										amtToPostCredit,ExecutionConstants.POSTING,
										ExecutionConstants.CREDIT,
										ExecutionConstants.CENTRAL_INTEREST,pstmtAccountingEntry,pstmtAcctUnpostedInsert,
										/** ****** JTEST FIX start ************  on 14DEC09*/
//										poolId,hmFields);//INT_1039
										poolId,hmFields, conn);//INT_1039
										/** ****** JTEST FIX end **************  on 14DEC09*/
							//SUP_1079a starts
//							}else{
//								addToAcctUnpostedTbl(pstmtAcctUnpostedInsert,acctDoObj.get_nbr_ownaccount(),
//									amtToPostCredit,ExecutionConstants.POSTING,ExecutionConstants.CENTRAL_INTEREST,"INTEREST LESS THAN MINIMUM AMOUNT INTEREST",poolId);//INT_1039
//							}//minCreditInterest
                            //SUP_1079a ends
						}else{
							logger.fatal("Credit Interest is 0 for"  + acctDoObj + "SO not posting the amount ");
						}//if (amtToPostCredit.compareTo(zero)!=0){

						if (amountToPostDebit.compareTo(zero)!=0){
							//SUP_1079a starts
							//if(amountToPostDebit.compareTo(minAmtCredit)>=0){
							//SUP_1079a ends
								// INT_1055 Start
							 // sendingDate = (Date)hmFields.get("DEBIT_DELAY");
							 // postingDate = (Date)hmFields.get("DAT_POST_DEBITDELAY");
							 if (postSettlementDate.compareTo(businessdate) < 0){
						        //SUP_1082c starts
						        hmFields.put("DAT_VALUE",calcPostingDates[0]);
//							     actualPostingDate = (Date)hmFields.get("NEXT_DEBIT_DELAY");
//							     //CAL fix starts
//								 //actualPostingDate = actualPostingDate != null ? actualPostingDate
//							     //                                              : businessdate;
//								if(actualPostingDate == null){
//									// Calculate the NEXT_DEBIT_DELAY  and assign it to actualPostingDate 
//									actualPostingDate = calcPostingDates[4];
//									if(logger.isDebugEnabled()){
//										logger.debug(" calculated next_debit_delay is "+ actualPostingDate);
//									}
//								}
//								//CAL fix ends
									actualPostingDate = calcPostingDates[4];
						        //SUP_1082c ends
							 }
							 else
							     actualPostingDate = (Date)hmFields.get("DAT_POST_DEBITDELAY");
							 hmFields.put("DAT_POSTING",actualPostingDate);
							 valueDate = (Date)hmFields.get("DAT_VALUE");
							 //sendingDate = sendingDate!=null?sendingDate:businessdate;
							 //hmFields.put("DAT_POSTING",postingDate);
							 // hmFields.put("DAT_SENT",sendingDate);
							 hmFields.put("DAT_SENT", addOffsetToDate(valueDate, 1));
							 // INT_1055 End
								accountingEntryId = accountingEntryId+"|" + processBVTEntry(acctDoObj,
								amountToPostDebit,ExecutionConstants.POSTING,
								ExecutionConstants.DEBIT,
								ExecutionConstants.CENTRAL_INTEREST,
								pstmtAccountingEntry,pstmtAcctUnpostedInsert,
								/** ****** JTEST FIX start ************  on 14DEC09*/
//								poolId,hmFields);//INT_1039
								poolId,hmFields, conn);//INT_1039
								/** ****** JTEST FIX end **************  on 14DEC09*/
							//SUP_1079a starts
//							}else{
//								addToAcctUnpostedTbl(pstmtAcctUnpostedInsert,acctDoObj.get_nbr_ownaccount(),
//								amountToPostDebit,ExecutionConstants.POSTING,ExecutionConstants.CENTRAL_INTEREST,"INTEREST LESS THAN MINIMUM AMOUNT INTEREST",poolId);//INT_1039
//						}//minDebitInterest
                        //SUP_1079a ends
						}else{
							logger.fatal("Debit Interest is 0 for"  + acctDoObj + "SO not posting the amount ");
						}

					}else{
						/*
							//chk if any amount is left to be posted
							if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
								amountToPost = amountToPost.add(bd_amtNotPosted);
								if (logger.isDebugEnabled()) {
									logger.debug("Adding Unposted Amount  "  + bd_amtNotPosted +"for ExecutionConstants.CENTRAL_INTEREST "+amountToPost);
								}
							}// UNPOSTED DEBIT AMOUNT
						*/
							//call processEntry for the NetAmount --consolidated
							//	CHECK IF THE NET_INTEREST IS CREDIT(+ve) or DEBIT(-ve)
							//	if (CREDIT)
							if (amountToPost.compareTo(zero) > 0){
								//INT_1055 Start
							    if (postSettlementDate.compareTo(businessdate) < 0){
							        //SUP_1082c starts
							        hmFields.put("DAT_VALUE",calcPostingDates[0]);
//							        actualPostingDate = (Date)hmFields.get("NEXT_CREDIT_DELAY");
//							        //CAL fix starts
//									//actualPostingDate = actualPostingDate != null ? actualPostingDate
//							        //                                              : businessdate;
//									if(actualPostingDate == null){
//										// Calculate the NEXT_CREDIT_DELAY  and assign it to actualPostingDate 
//										actualPostingDate = calcPostingDates[3];
//										if(logger.isDebugEnabled()){
//											logger.debug(" calculated next_credit_delay is "+ actualPostingDate);
//										}
//									}
//									//CAL fix ends
										actualPostingDate = calcPostingDates[3];
							        //SUP_1082c ends
							    }
							    else
							        actualPostingDate = (Date)hmFields.get("DAT_POST_CREDITDELAY");

							    //postingDate = (Date)hmFields.get("DAT_POST_CREDITDELAY");
							    //sendingDate = (Date)hmFields.get("CREDIT_DELAY");
							    //INT_1055 End
								//SUP_1079a starts
							    //minAmtNet = minAmtCredit;
							    //SUP_1079a ends
							}else if (amountToPost.compareTo(zero) < 0){
								//INT_1055 Start
							    if (postSettlementDate.compareTo(businessdate) < 0){
							        //SUP_1082c starts
							        hmFields.put("DAT_VALUE",calcPostingDates[0]);
//							        actualPostingDate = (Date)hmFields.get("NEXT_DEBIT_DELAY");
//							        //CAL fix starts
//									//actualPostingDate = actualPostingDate != null ? actualPostingDate
//							        //                                              : businessdate;
//									if(actualPostingDate == null){
//										// Calculate the NEXT_DEBIT_DELAY  and assign it to actualPostingDate 
//										actualPostingDate = calcPostingDates[4];
//										if(logger.isDebugEnabled()){
//											logger.debug(" calculated next_debit_delay is "+ actualPostingDate);
//										}
//									}
//									//CAL fix ends
										actualPostingDate = calcPostingDates[4];
							        //SUP_1082c ends
							    }
							    else
							        actualPostingDate = (Date)hmFields.get("DAT_POST_DEBITDELAY");

							    //postingDate = (Date)hmFields.get("DAT_POST_DEBITDELAY");
							    //sendingDate = (Date)hmFields.get("DEBIT_DELAY");
							    //INT_1055 End
								//SUP_1079a starts
							    //minAmtNet = minAmtDebit;
							    //SUP_1079a ends
							}else{
								logger.fatal("Net Interest is 0 for"  + acctDoObj +
										"SO not posting the amount ");
							}
							//INT_1055 Start
							hmFields.put("DAT_POSTING",actualPostingDate);
							valueDate = (Date)hmFields.get("DAT_VALUE");
							//sendingDate = sendingDate!=null?sendingDate:businessdate;
							//hmFields.put("DAT_POSTING",postingDate);
							// hmFields.put("DAT_SENT",sendingDate);
							hmFields.put("DAT_SENT", addOffsetToDate(valueDate, 1));
							// INT_1055 End

							//SUP_1079a starts
							//if((amountToPost).abs().compareTo(minAmtNet)>=0){//Changed for absolute of NET_INTEREST while posting
							//SUP_1079a ends
							accountingEntryId = "" + processBVTEntry(acctDoObj,
									amountToPost,ExecutionConstants.POSTING,
									ExecutionConstants.NET,//ENTRY
									ExecutionConstants.CENTRAL_INTEREST,//DETAIL
									pstmtAccountingEntry,pstmtAcctUnpostedInsert,
									/** ****** JTEST FIX start ************  on 14DEC09*/
//									poolId,hmFields);//INT_1039
									poolId,hmFields, conn);//INT_1039
									/** ****** JTEST FIX end **************  on 14DEC09*/
						//SUP_1079a starts
//						}else{
//							addToAcctUnpostedTbl(pstmtAcctUnpostedInsert,acctDoObj.get_nbr_ownaccount(),
//								amountToPost,ExecutionConstants.POSTING,ExecutionConstants.CENTRAL_INTEREST,"INTEREST LESS THAN MINIMUM AMOUNT INTEREST",poolId);//INT_1039
//						}//minNetInterest
                        //SUP_1079a ends
					}

					if (logger.isDebugEnabled()) {
							logger.debug("After processEntry accountingEntryId"  + accountingEntryId);
					}
					//postingEntries[ExecutionConstants.OLM_ACCOUNTINGENTRY_INSERT] = true;
					//postingEntries[ExecutionConstants.PARTY_DTLS_UPDATE] = true;
					//updateInterestBVT(pstmtTxnPoolPartyDtls,accountingEntryId,poolId,acctDoObj.get_nbr_ownaccount());
					}//end of while resultset
					if (!results){
						logger.fatal(" No records for Posting -participant" +
								" interest Settlement Account" + sqlSettlementPosting);
					}
				rs.close();

/////////////pool ReallocationPosting ////////////////////////////////////////////////////////////
			  				//INT_1055 Starts
//			  String sqlReallocationPosting = new StringBuffer("SELECT  ")
//					.append(" participant_id, dat_value, amount, tmp3.id_pool, flg_allocation, tmp3.cod_ccy, nbr_coreaccount, cod_branch,")
//					.append(" tmp3.cod_gl, cod_bnk_lgl, nbr_ibanaccnt, NVL(amt_min_cr,0) amt_min_cr, NVL(amt_min_dr,0) amt_min_dr, flg_consolidation, acct_settle_cr, acct_settle_dr, ")
//					.append(" dat_post_settlement, dat_post_debitdelay, dat_post_creditdelay, ")
//					.append("   (SELECT DISTINCT HDR.DAT_POST_DEBITDELAY FROM OPOOL_TXNPOOL_PARTYDTLS PAR, OPOOL_TXNPOOLHDR HDR WHERE ")
//					.append("   HDR.ID_POOL=PAR.ID_POOL AND HDR.DAT_BUSINESS_POOLRUN = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')  ) DEBIT_DELAY, ")
//					.append("   (SELECT DISTINCT DAT_POST_CREDITDELAY FROM OPOOL_TXNPOOL_PARTYDTLS PAR, OPOOL_TXNPOOLHDR HDR WHERE ")
//					.append("   HDR.ID_POOL=PAR.ID_POOL AND HDR.DAT_BUSINESS_POOLRUN = TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')  )  CREDIT_DELAY ")
//					.append(" from opool_txnpoolhdr hdr, ")
//					.append(" (select tmp2.participant_id, dat_value, amount, tmp2.id_pool, flg_allocation, cod_party_ccy cod_ccy, ")
//					.append(" nbr_coreaccount, cod_branch, cod_gl, cod_bnk_lgl, nbr_ibanaccnt, NVL(amt_min_cr,0) amt_min_cr, NVL(amt_min_dr,0) amt_min_dr ")
//					.append(" from opool_txnpool_partydtls par, ")
//					.append(" (select tmp.participant_id, dat_value, amount, id_pool, sub_typ_calculation flg_allocation ")
//					.append(" from opool_txnpool_bvtdiff bd, ")
//					.append(" (select participant_id, max(dat_business_poolrun) dat_value, sum(amt_diff) amount ")
//					.append(" from opool_txnpool_bvtdiff ")
//					.append(" where nbr_processid=" + nbrProcessId + " and dat_posting < TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')  ")
//					.append(" and amt_diff != 0 ")
//					.append(" and typ_participant = 'ACCOUNT' ")
//					.append(" and flg_realpool = 'Y' and sub_typ_calculation = 'R' ")
//					.append(" group by participant_id ) tmp ")
//					// All accounts for which reallocation needs to be posted
//					.append(" where bd.participant_id = tmp.participant_id ")
//					.append(" and bd.dat_business_poolrun = dat_value ")
//					.append(" and bd.nbr_processid= " + nbrProcessId)
//					.append(") tmp2 ")
//					// PoolIds of the accounts on the corresponding value date
//					.append(" where par.id_pool = tmp2.id_pool  ")
//					.append(" and par.PARTICIPANT_ID = tmp2.participant_id ")
//					.append(" and par.NBR_PROCESSID = " + nbrProcessId)
//					.append(" and par.dat_business_poolrun = tmp2.dat_value ")
//					.append(" ) tmp3 ")
//					// Details of these accounts
//					.append(" where hdr.id_pool = tmp3.id_pool ")
//					.append(" and hdr.dat_business_poolrun = tmp3.dat_value ")
//					.append(" and hdr.nbr_processid = " + nbrProcessId)
//					// Posting dates for these accounts
//					.toString();
				sBuf.setLength(0);
				//reallocation fix - flg_allocation removed
				//sBuf.append(" SELECT participant_id, dat_value, amount, tmp3.id_pool, flg_allocation, ");//reallocation fix
				//sBuf.append(" SELECT participant_id, dat_value, amount, tmp3.id_pool, ");//reallocation fix
				sBuf.append(" SELECT participant_id, dat_value, dr_sum, cr_sum, tmp3.id_pool, ");//reallocation fix , DrCrAllocation
				sBuf.append(" tmp3.cod_ccy, nbr_coreaccount, cod_branch, tmp3.cod_gl, cod_bnk_lgl, ");
				sBuf.append(" nbr_ibanaccnt, NVL (amt_min_cr, 0) amt_min_cr, ");
				sBuf.append(" NVL (amt_min_dr, 0) amt_min_dr, flg_consolidation, acct_settle_cr, ");
				sBuf.append(" acct_settle_dr, dat_post_settlement, dat_post_debitdelay, ");
				sBuf.append(" dat_post_creditdelay, ");
				sBuf.append(" (SELECT dat_post_debitdelay ");
				sBuf.append(" FROM opool_txnpoolhdr h1 ");
				sBuf.append(" WHERE h1.id_pool = hdr.id_pool ");
				// INT_1069 Start
				//sBuf.append(" AND h1.dat_business_poolrun = dat_value + 1 ");
				sBuf.append(" AND h1.dat_business_poolrun = hdr.dat_posting + 1 ");
				// sBuf.append(" TO_DATE ('" + strbusinessdate + "', '" + dateFormat + "') ");
				/** ****** JTEST FIX start ************  on 14DEC09*/
//				sBuf.append(" AND h1.nbr_processid = " + nbrProcessId);
				sBuf.append(" AND h1.nbr_processid = ? ");
				/** ****** JTEST FIX end **************  on 14DEC09*/
				// sBuf.append(" AND h1.nbr_processid = (SELECT MAX (NBR_PROCESSID) ");
				// sBuf.append(" FROM opool_txnpoolhdr h ");
				// sBuf.append(" WHERE h.id_pool = h1.id_pool ");
				// sBuf.append(" AND h.dat_business_poolrun = h1.dat_business_poolrun ");
				// sBuf.append(" )	");
				// INT_1069 End
				sBuf.append(" ) next_debit_delay, ");
				sBuf.append(" (SELECT dat_post_creditdelay ");
				sBuf.append(" FROM opool_txnpoolhdr h2 ");
				sBuf.append(" WHERE h2.id_pool = hdr.id_pool ");
				// INT_1069 Start
				//sBuf.append(" AND h2.dat_business_poolrun = dat_value + 1 ");
				sBuf.append(" AND h2.dat_business_poolrun = hdr.dat_posting + 1 ");
				// sBuf.append(" TO_DATE ('" + strbusinessdate + "', '" + dateFormat + "') ");
				/** ****** JTEST FIX start ************  on 14DEC09*/
//				sBuf.append(" AND h2.nbr_processid = " + nbrProcessId);
				sBuf.append(" AND h2.nbr_processid = ? ");
				/** ****** JTEST FIX end **************  on 14DEC09*/
				// sBuf.append(" AND h2.nbr_processid = (SELECT MAX (NBR_PROCESSID) ");
				// sBuf.append(" FROM opool_txnpoolhdr h ");
				// sBuf.append(" WHERE h.id_pool = h2.id_pool ");
				// sBuf.append(" AND h.dat_business_poolrun = h2.dat_business_poolrun ");
				// sBuf.append(" )	");
				sBuf.append(" ) next_credit_delay  , dat_posting");
				// INT_1069 End
				sBuf.append(" FROM opool_txnpoolhdr hdr, ");
				//sBuf.append(" (SELECT tmp2.participant_id, dat_value, amount, tmp2.id_pool, ");
				sBuf.append(" (SELECT tmp2.participant_id, dat_value, dr_sum, cr_sum, tmp2.id_pool, ");//DrCrAllocation
				//sBuf.append(" flg_allocation, cod_party_ccy cod_ccy, nbr_coreaccount, ");//reallocation fix
				sBuf.append(" cod_party_ccy cod_ccy, nbr_coreaccount, ");//reallocation fix
				sBuf.append(" cod_branch, cod_gl, cod_bnk_lgl, nbr_ibanaccnt, ");
				sBuf.append(" NVL (amt_min_cr, 0) amt_min_cr, NVL (amt_min_dr, 0) amt_min_dr ");
				sBuf.append(" FROM opool_txnpool_partydtls par, ");
				//sBuf.append(" (SELECT tmp.participant_id, dat_value, amount, id_pool, ");//reallocation fix
				//sBuf.append(" (SELECT tmp.participant_id, dat_value, amount, id_pool ");//reallocation fix
				sBuf.append(" (SELECT tmp.participant_id, dat_value, dr_sum, cr_sum, id_pool ");//reallocation fix, DrCrAllocation
				//sBuf.append(" sub_typ_calculation flg_allocation ");//reallocation fix
				sBuf.append(" FROM opool_txnpool_bvtdiff bd, ");
				sBuf.append(" (SELECT participant_id, ");
				sBuf.append(" MAX (dat_business_poolrun) dat_value, ");
				//sBuf.append(" SUM (amt_diff) amount ");
				sBuf.append(" SUM (amt_dr_diff) dr_sum, SUM (amt_cr_diff) cr_sum ");//DrCrAllocation

				//BVTAccrualBasedOnPosting Change Starts
				//sBuf.append(" FROM opool_txnpool_bvtdiff ");
				sBuf.append(" FROM opool_txnpool_bvtdiff diff");
				//BVTAccrualBasedOnPosting Change Ends

				/** ****** JTEST FIX start ************  on 14DEC09*/
//				sBuf.append(" WHERE nbr_processid = "+nbrProcessId);
				sBuf.append(" WHERE nbr_processid = ?");
				/** ****** JTEST FIX end **************  on 14DEC09*/
				
				//BVTAccrualBasedOnPosting Change Starts
				//sBuf.append(" AND dat_posting < ");
				//sBuf.append(" TO_DATE ('" + strbusinessdate + "', '" + dateFormat + "') ");
				sBuf.append(" AND dat_accrual <= ");
				sBuf.append(" (SELECT MAX(dat_posting) ");
				sBuf.append(" FROM opool_txnpool_bvtdiff d ");
				sBuf.append(" WHERE d.NBR_PROCESSID = diff.NBR_PROCESSID ");
				sBuf.append(" AND d.ID_POOL = diff.ID_POOL ");
				sBuf.append(" AND d.PARTICIPANT_ID = diff.PARTICIPANT_ID ");
				sBuf.append(" AND d.DAT_BUSINESS_POOLRUN = diff.DAT_BUSINESS_POOLRUN ");
				sBuf.append(" AND d.TYP_CALCULATION = diff.TYP_CALCULATION ");
				sBuf.append(" AND d.SUB_TYP_CALCULATION = diff.SUB_TYP_CALCULATION ");
				sBuf.append(" AND d.NBR_SEQUENCE = diff.NBR_SEQUENCE ");												
				sBuf.append(" AND d.DAT_POSTING < TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')");
				sBuf.append(" ) ");
				//BVTAccrualBasedOnPosting Change Ends

				//sBuf.append(" AND amt_diff != 0 ");//DrCrAllocation
				sBuf.append(" AND typ_participant = 'ACCOUNT' ");
				sBuf.append(" AND flg_realpool = 'Y' ");
				sBuf.append(" AND sub_typ_calculation = 'R' ");
				sBuf.append(" AND FLG_REALLOCATION = 'Y' ");//for Info pools don't reallocate
				sBuf.append(" GROUP BY participant_id) tmp ");
				sBuf.append(" WHERE bd.participant_id = tmp.participant_id ");
				sBuf.append(" AND bd.dat_business_poolrun = dat_value ");
				sBuf.append(" AND (dr_sum != 0 OR cr_sum != 0) ");//DrCrAllocation
				/** ****** JTEST FIX start ************  on 14DEC09*/
//				sBuf.append(" AND bd.nbr_processid = "+nbrProcessId+") tmp2 ");
				sBuf.append(" AND bd.nbr_processid = ?) tmp2 ");
				/** ****** JTEST FIX end **************  on 14DEC09*/
				sBuf.append(" WHERE par.id_pool = tmp2.id_pool ");
				sBuf.append(" AND par.participant_id = tmp2.participant_id ");
				/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
				//SUP_HSBC_041 Fix starts
				// It's possible that partcipants of type pool & account with same nbr_ownaccout/id_pool exists for a given root pool
				sBuf.append(" AND par.typ_participant = 'ACCOUNT'");
				//SUP_HSBC_041 Fix ends
				/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
				/** ****** JTEST FIX start ************  on 14DEC09*/
//				sBuf.append(" AND par.nbr_processid = "+nbrProcessId);
				sBuf.append(" AND par.nbr_processid = ?");
				/** ****** JTEST FIX end **************  on 14DEC09*/
				sBuf.append(" AND par.dat_business_poolrun = tmp2.dat_value) tmp3 ");
				sBuf.append(" WHERE hdr.id_pool = tmp3.id_pool ");
				sBuf.append(" AND hdr.dat_business_poolrun = tmp3.dat_value ");
				/** ****** JTEST FIX start ************  on 14DEC09*/
//				sBuf.append(" AND hdr.nbr_processid = "+nbrProcessId);
				sBuf.append(" AND hdr.nbr_processid = ?");
				/** ****** JTEST FIX end **************  on 14DEC09*/
				//ENH_10007 Starts
				//BVT Adjsutmet Accounting Entries should be generated per host.
				/** ****** JTEST FIX start ************  on 14DEC09*/
//				sBuf.append(" AND hdr.cod_gl = '"+hostId+"'");
				sBuf.append(" AND hdr.cod_gl = ?");
				/** ****** JTEST FIX end **************  on 14DEC09*/
				//ENH_10007 Ends
				//modified and replaced DEBIT_DELAY,CREDIT_DELAY with next_debit_delay, next_credit_delay
				//respectively. dat_posting added as last column
				String sqlReallocationPosting = sBuf.toString();
				//INT_1055 Ends

				  if (logger.isDebugEnabled())
					logger.debug("Query POSTING REALLOCATION : " + sqlReallocationPosting);
				  /** ****** JTEST FIX start ************  on 14DEC09*/
//				  rs = stmt.executeQuery(sqlReallocationPosting.toString());
				  
				  pStmt = conn.prepareStatement(sqlReallocationPosting);
				  pStmt.setLong(1, nbrProcessId);
				  pStmt.setLong(2, nbrProcessId);
				  pStmt.setLong(3, nbrProcessId);
				  pStmt.setLong(4, nbrProcessId);
				  pStmt.setLong(5, nbrProcessId);
				  pStmt.setLong(6, nbrProcessId);
				  pStmt.setString(7, hostId);
				  rs = pStmt.executeQuery();
				  /** ****** JTEST FIX end **************  on 14DEC09*/
				  if (logger.isDebugEnabled()) {
						logger.debug(" Executed Query  POSTING REALLOCATION ");
				  }
				results = false;
				while(rs.next()){
					results = true;
					acctDoObj = new AccountingDO();
					accountingEntryId = "";
					details = "";
					poolId = "";
					accountingEntryId = "";

					poolId = rs.getString("ID_POOL");
					//amountToPost = new BigDecimal(rs.getString("AMOUNT"));
					//DrCrAllocation starts
					amtToPostCreditP = rs.getBigDecimal("CR_SUM");
					amountToPostDebitP = rs.getBigDecimal("DR_SUM");
					// Pass separate entries for both debit and credit allocation.
					b_flgSeperate = true;
					//DrCrAllocation ends

					//fill in the AccountingDo object's attributes.
					acctDoObj.set_nbr_ownaccount(rs.getString("PARTICIPANT_ID"));
					acctDoObj.set_nbr_coreaccount(rs.getString("NBR_COREACCOUNT"));
					acctDoObj.set_cod_branch(rs.getString("COD_BRANCH"));
					acctDoObj.set_cod_gl(rs.getString("COD_GL"));
					acctDoObj.set_cod_bnk_lgl(rs.getString("COD_BNK_LGL"));
					acctDoObj.set_nbr_ibanacct(rs.getString("NBR_IBANACCNT"));
					acctDoObj.set_cod_ccy(rs.getString("COD_CCY"));

					hmFields.put("DAT_POST_SETTLEMENT",rs.getDate("DAT_POST_SETTLEMENT"));
					hmFields.put("DAT_POST_CREDITDELAY",rs.getDate("DAT_POST_CREDITDELAY"));
					hmFields.put("DAT_POST_DEBITDELAY",rs.getDate("DAT_POST_DEBITDELAY"));
					// INT_1055 Start
					// hmFields.put("BVT_CREDITDELAY",rs.getDate("CREDIT_DELAY"));
					// hmFields.put("BVT_DEBITDELAY",rs.getDate("DEBIT_DELAY"));
					hmFields.put("NEXT_CREDIT_DELAY",rs.getDate("NEXT_CREDIT_DELAY"));
					hmFields.put("NEXT_DEBIT_DELAY",rs.getDate("NEXT_DEBIT_DELAY"));
					hmFields.put("DAT_VALUE",rs.getDate("DAT_POSTING"));
					postSettlementDate = (Date)hmFields.get("DAT_POST_SETTLEMENT");
					// INT_1055 End
					//CAL fix starts
					java.util.Date dbNextCreditDelay = rs.getDate("NEXT_CREDIT_DELAY");
					java.util.Date dbNextDebitDelay = rs.getDate("NEXT_DEBIT_DELAY");
					String codGL = rs.getString("COD_GL");
					java.util.Date[] calcPostingDates = null;
					//SUP_1082c starts
//					if(dbNextCreditDelay == null || dbNextDebitDelay == null){
//					    calcPostingDates = calculateNextCyclePostingDates(conn, poolId, codGL, nbrProcessId, businessdate);
//					}
					if(postCache == null || postCache.get(poolId) == null){
					    if(logger.isWarnEnabled()){
					        logger.warn("Error forming the posting cache OR poolId ["+poolId+"] postingDates not present in the cache.");
					        logger.warn("So getting the dates from database for "+poolId+".");
					    }
						calcPostingDates = calculateNextCyclePostingDates(conn, poolId, codGL, nbrProcessId, businessdate);
					}
					else{
					    calcPostingDates = (Date[])postCache.get(poolId);
					}
					//SUP_1082c ends
					//CAL fix ends
					//BigDecimal amtForAllocation = rs.getBigDecimal("AMOUNT");//DrCrAllocation

					//get the settlement accounts too as the entries for this would be central and participant
					//fetch and set TXN.ACCT_SETTLE_CR,
					//TXN.ACCT_SETTLE_DR, this will be one of the legs
					acctDoObj.setCrBeneficiaryAccount(rs.getString("ACCT_SETTLE_CR"));
					acctDoObj.setDrBeneficiaryAccount(rs.getString("ACCT_SETTLE_DR"));
					//put extraDetails in HashMap
					details = "REALLOCATION";
					//extraDetails = "ALLOCATOR IS " + rs.getString("ALLOCATOR");
					if (logger.isDebugEnabled()) {
						logger.debug("Account Object for ReAllocation" + acctDoObj);
					}
					bd_amtNotPosted = setUnpostedAmounts(details,acctDoObj,
											pstmtAccountUnposted,businessdate,ExecutionConstants.POSTING);
					if (logger.isDebugEnabled()) {
					  logger.debug("Unposted Amount "  + bd_amtNotPosted +"for REALLOCATION");
					 }
					 //chk if any amount is left to be posted
					/*
					if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
						amountToPost = amountToPost.add(bd_amtNotPosted);
						if (logger.isDebugEnabled()) {
							logger.debug("Adding Unposted Amount  "  + bd_amtNotPosted +"for REALLOCATION "+amountToPost);
						 }
					 }// UNPOSTED REALLOCATION

					*/
					//based on amtForAllocation, decide if it is CREDIT/DEBIT DELAY DAYS FOR POSTING DATE
					//DrCrAllocation Starts
					//amtToPostCreditP, amountToPostDebitP should not be null
					amountToPost = amtToPostCreditP.subtract(amountToPostDebitP);
					//Separate entries
					if(b_flgSeperate){
						// Credit Allocation
						if (amtToPostCreditP.compareTo(zero) != 0){
							if (postSettlementDate.compareTo(businessdate) < 0){
						        //SUP_1082c starts
						        hmFields.put("DAT_VALUE",calcPostingDates[0]);
//								actualPostingDate = (Date)hmFields.get("NEXT_CREDIT_DELAY");
//								if(actualPostingDate == null){
//									// Calculate the NEXT_CREDIT_DELAY  and assign it to actualPostingDate 
//									actualPostingDate = calcPostingDates[3];
//									if(logger.isDebugEnabled()){
//										logger.debug(" calculated next_credit_delay is "+ actualPostingDate);
//									}
//								}
									actualPostingDate = calcPostingDates[3];
						        //SUP_1082c ends
							}
							else
								actualPostingDate = (Date)hmFields.get("DAT_POST_CREDITDELAY");
							//Call AE
							hmFields.put("DAT_POSTING",actualPostingDate);
							valueDate = (Date)hmFields.get("DAT_VALUE");
							hmFields.put("DAT_SENT", addOffsetToDate(valueDate, 1));
							accountingEntryId = "" + processBVTEntry(acctDoObj,
								amtToPostCreditP,ExecutionConstants.POSTING,
								ExecutionConstants.BVT_CREDIT, details,
								pstmtAccountingEntry,pstmtAcctUnpostedInsert,
								/** ****** JTEST FIX start ************  on 14DEC09*/
//								poolId,hmFields);	
								poolId,hmFields, conn);	
								/** ****** JTEST FIX end **************  on 14DEC09*/
						}
						else{
							logger.fatal("Credit ReAllocation is 0 for"  + acctDoObj + "SO not posting the amount ");
						}
						// DEBIT Allocation
						if (amountToPostDebitP.compareTo(zero) != 0){
							if (postSettlementDate.compareTo(businessdate) < 0){
						        //SUP_1082c starts
						        hmFields.put("DAT_VALUE",calcPostingDates[0]);
//								actualPostingDate = (Date)hmFields.get("NEXT_DEBIT_DELAY");
//								if(actualPostingDate == null){
//									// Calculate the NEXT_DEBIT_DELAY  and assign it to actualPostingDate 
//									actualPostingDate = calcPostingDates[4];
//									if(logger.isDebugEnabled()){
//										logger.debug(" calculated next_debit_delay is "+ actualPostingDate);
//									}
//								}
									actualPostingDate = calcPostingDates[4];
						        //SUP_1082c ends
							}
							else
								actualPostingDate = (Date)hmFields.get("DAT_POST_DEBITDELAY");
							//Call AE
							hmFields.put("DAT_POSTING",actualPostingDate);
							valueDate = (Date)hmFields.get("DAT_VALUE");
							hmFields.put("DAT_SENT", addOffsetToDate(valueDate, 1));
							accountingEntryId = "" + processBVTEntry(acctDoObj,
								amountToPostDebitP,ExecutionConstants.POSTING,
								ExecutionConstants.BVT_DEBIT, details,
								pstmtAccountingEntry,pstmtAcctUnpostedInsert,
								/** ****** JTEST FIX start ************  on 14DEC09*/
//								poolId,hmFields);
								poolId,hmFields, conn);
								/** ****** JTEST FIX end **************  on 14DEC09*/

						}
						else{
							logger.fatal("Debit ReAllocation is 0 for"  + acctDoObj + "SO not posting the amount ");
						}						
					}
//					if (amountToPost.compareTo(zero) != 0){
//						if (amtForAllocation.compareTo(zero) > 0){
//							//POSTING CREDIT INTEREST
//							//INT_1055 Start
//							//postingDate = rs.getDate("DAT_POST_CREDITDELAY");
//							//sendingDate = rs.getDate("CREDIT_DELAY");
//						    if (postSettlementDate.compareTo(businessdate) < 0){
//						        actualPostingDate = (Date)hmFields.get("NEXT_CREDIT_DELAY");
//						        //CAL fix starts
//								//actualPostingDate = actualPostingDate != null ? actualPostingDate
//						        //                                              : businessdate;
//								if(actualPostingDate == null){
//									 // Calculate the NEXT_CREDIT_DELAY  and assign it to actualPostingDate 
//									 actualPostingDate = calcPostingDates[3];
//									 if(logger.isDebugEnabled()){
//										logger.debug(" calculated next_credit_delay is "+ actualPostingDate);
//									 }
//								 }
//								//CAL fix ends
//						    }
//						    else
//						        actualPostingDate = (Date)hmFields.get("DAT_POST_CREDITDELAY");
//						    //INT_1055 End
//						}else{
//							//INT_1055 Start
//							//postingDate = rs.getDate("DAT_POST_DEBITDELAY");
//							//sendingDate = rs.getDate("DEBIT_DELAY");
//						    if (postSettlementDate.compareTo(businessdate) < 0){
//						        actualPostingDate = (Date)hmFields.get("NEXT_DEBIT_DELAY");
//						        //CAL fix starts
//								//actualPostingDate = actualPostingDate != null ? actualPostingDate
//						        //                                              : businessdate;
//								if(actualPostingDate == null){
//									 // Calculate the NEXT_DEBIT_DELAY  and assign it to actualPostingDate 
//									 actualPostingDate = calcPostingDates[4];
//									 if(logger.isDebugEnabled()){
//										logger.debug(" calculated next_debit_delay is "+ actualPostingDate);
//									 }
//								 }
//								 //CAL fix ends
//						    }
//						    else
//						        actualPostingDate = (Date)hmFields.get("DAT_POST_DEBITDELAY");
//						    //INT_1055 End
//						}
//						//INT_1055 Start
//						hmFields.put("DAT_POSTING",actualPostingDate);
//						valueDate = (Date)hmFields.get("DAT_VALUE");
//						//sendingDate = sendingDate!=null?sendingDate:businessdate;
//						//hmFields.put("DAT_POSTING",postingDate);
//						// hmFields.put("DAT_SENT",sendingDate);
//						hmFields.put("DAT_SENT", addOffsetToDate(valueDate, 1));
//						// INT_1055 End
//						accountingEntryId = "" + processBVTEntry(acctDoObj,
//								amountToPost,ExecutionConstants.POSTING,
//								//ExecutionConstants.NET, ExecutionConstants.CENTRAL_INTEREST,
//								ExecutionConstants.NET, details,
//								pstmtAccountingEntry,pstmtAcctUnpostedInsert,
//								poolId,hmFields);//INT_1039
//					}else{
//						logger.fatal(" fOR rEALLOCATION Not Posting  Amount as amountTo Post = 0 amount for account " + acctDoObj);
//					}
					
					//DrCrAllocation ends
					//postingEntries[ExecutionConstants.OLM_ACCOUNTINGENTRY_INSERT] = true;
					//postingEntries[ExecutionConstants.ALLOCATION_UPDATE] = true;
				}//end of while resultset
				if (!results){
					logger.fatal(" No records for Posting -REALLOCATION"+ sqlReallocationPosting);
				}
				/*INT_1039--Starts
				if (logger.isDebugEnabled()) {
					logger.debug("Buffer value for Inserting into ACCOUNT UNPOSTED TABLE =  " + sbufUnpostedAmtInsr);
				}
				postingEntries[ExecutionConstants.UNPOSTED_AMOUNT_INSERT] = (new Boolean(sbufUnpostedAmtInsr.toString())).booleanValue();
				//INT_1039--Ends
				*/
				if (logger.isDebugEnabled()) {
					logger.debug("postingEntries[ExecutionConstants.UNPOSTED_AMOUNT_INSERT] =  " + postingEntries2[ExecutionConstants.UNPOSTED_AMOUNT_INSERT]);
				}

				//BVI_BTS starts
				generateBVTBankShareTreasuryShareEntries(pstmtAccountingEntry,
				           pstmtAcctUnpostedInsert,businessdate,strbusinessdate,
                           //SUP_1082c starts
				           //ExecutionConstants.POSTING,std);
				           //For posting bvtDetails is not needed for generateBVTBankShareTreasuryShareEntries
				           //ENH_10007 Starts
				           //BVT Adjsutmet Accounting Entries should be generated per host.
						   //ExecutionConstants.POSTING,std,null);
						   ExecutionConstants.POSTING,std,null,hostId);
						   //ENH_10007 Ends
						   //SUP_1082c ends
				//BVI_BTS ends
			}

		/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//		catch(SQLException e){
		catch(SQLException e){
			logger.fatal("General Exception", e);
			throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, e.getMessage());
		}catch(NamingException e){
				logger.fatal("General Exception", e);
				throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, e.getMessage());
		/** ****** FindBugs Fix End ***********  on 14DEC09*/
			}finally{
				LMSConnectionUtility.close(rs);
				/** ****** JTEST FIX start ************  on 14DEC09*/
//				LMSConnectionUtility.close(stmt);
				LMSConnectionUtility.close(pStmt);				
				/** ****** JTEST FIX end **************  on 14DEC09*/
				LMSConnectionUtility.close(conn);
			}
			return postingEntries2;

	}

	public long processBVTEntry(AccountingDO account,BigDecimal amountToPost,
			String type,String entry, String detail,
			PreparedStatement pstmt,PreparedStatement pstmtAcctUnpostedInsert,
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			String poolId,HashMap hmFields)
			String poolId,HashMap hmFields, Connection conn)
			/** ****** JTEST FIX end **************  on 14DEC09*/
			throws LMSException,SQLException{//INT_1039
		if (logger.isDebugEnabled())
				logger.debug("Entering");
		/*
		o	getAmountToPost(amountToPost,ACCRUAL) for considering the Accrual amount by truncating/rounding till the decimals
		o	generateLegs(AccountDO,amountToPost,entry,detail,ACCRUAL)  which sets the values in AccountingEntry Leg object for the Dr and Cr legs and returns the list AELegs  which will return the AELegs.
		o	Call the method setPreparedStatement(AELegs) which can return the accountingEntryId
		o	return the AccountingEntryId*/
		//StringBuffer sbuf_carryFwdAmt = new StringBuffer("");	//INT_1039
		//CONSIDER THE DECIMALS WHILE POSTING
//		INT_1039--Starts
		//BigDecimal bd_precisionAmt = getAmountToPost(amountToPost,sbuf_carryFwdAmt,type,account.get_cod_ccy());
		BigDecimal carryFwdAmt=new BigDecimal(0);
		BigDecimal bd_precisionAmt=new BigDecimal(0);
//SUP_1031--Starts
		//ArrayList precision_carryfwd = getAmountToPost(amountToPost,type,account.get_cod_ccy());
		ExecutionUtility util=new ExecutionUtility();
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		BigDecimal[] precision_carryfwd = util.getTruncatedAmt(amountToPost,type,account.get_cod_ccy());
		BigDecimal[] precision_carryfwd = util.getTruncatedAmt(amountToPost,type,account.get_cod_ccy(), conn);
		/** ****** JTEST FIX end **************  on 14DEC09*/
		//BigDecimal bd_precisionAmt=(BigDecimal)precision_carryfwd.get(0);//Changed
		//BigDecimal carryFwdAmt=(BigDecimal)precision_carryfwd.get(1);//Chnaged
		bd_precisionAmt=precision_carryfwd[0];
		carryFwdAmt=precision_carryfwd[1];
		//carryFwdAmt=(BigDecimal)precision_carryfwd.get(1);
		//bd_precisionAmt=(BigDecimal)precision_carryfwd.get(0);
//		INT_1039--End
		//IF CARRYFWD AMOUNT IS PRESENT FOR POSTING ADD IT TO THE TABLE
		//if(carryFwdAmt.compareTo(new BigDecimal(0)) != 0  && (ExecutionConstants.POSTING).equals(type.trim())){//INT_1039
		if( carryFwdAmt.compareTo(new BigDecimal(0)) != 0){
//SUP_1031--Ends
			addToAcctUnpostedTbl(pstmtAcctUnpostedInsert,account.get_nbr_ownaccount(),
				carryFwdAmt,type,detail,"AMOUNT_CARRYFORWARD",poolId);//INT_1039
			//accrualEntries[ExecutionConstants.UNPOSTED_AMOUNT_INSERT] = true;

			/*INT-1039--Starts
			if("".equals(sbufUnpostedAmtInsr.toString())){
				sbufUnpostedAmtInsr.append("true");
			}
			if (logger.isDebugEnabled()) {
				logger.debug("Insert To Account_Unposted_Tbl_Flg set to ="+ sbufUnpostedAmtInsr);
			}INT_1039--ENds*/

		}
//INT_1039--Starts
//		If posting amount amount zero then return
		if(bd_precisionAmt.compareTo(new BigDecimal(0)) == 0){
			if (logger.isDebugEnabled()) {
				logger.debug("Precision amount is zero so returning");
			}
			return 0;

		}
//INT_1039--Ends
		//passing precision amount to Post
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		List AELegs = generateLegs(account,bd_precisionAmt,entry,detail,type);
		//List AELegs = generateLegs(account,bd_precisionAmt,entry,detail,type, conn);
		List AELegs = generateLegs(account,bd_precisionAmt,entry,detail,type, conn, hmFields); // Mashreq_AE_Hook
		/** ****** JTEST FIX end **************  on 14DEC09*/
		//List AELegs = generateLegs(account,amountToPost,entry,detail,type);

		if (logger.isDebugEnabled()) {
			logger.debug("setting values to prepared stmt");
		}

		long accId = setPreparedStatementValues(pstmt,AELegs,poolId,hmFields);

		if (logger.isDebugEnabled()){
				logger.debug("Leaving");
		}

		return accId;

	}





	private boolean[] generateAccrualEntriesBVT(PreparedStatement pstmtAccountingEntry,PreparedStatement pstmtTxnPoolPartyDtls,
			PreparedStatement pstmtAccountUnposted,
			//SUP_1082c starts
			//PreparedStatement pstmtAcctUnpostedInsert, Date businessdate, SweepsTriggerDetails std  )
			//ENH_10007 Starts
			//BVT Adjsutmet Accounting Entries should be generated per host.
			//PreparedStatement pstmtAcctUnpostedInsert, Date businessdate, SweepsTriggerDetails std, BVTDetails bvtDetails  )
			PreparedStatement pstmtAcctUnpostedInsert, Date businessdate,
            SweepsTriggerDetails std, BVTDetails bvtDetails, String hostId)
			//ENH_10007 Ends
			//SUP_1082c ends
			throws LMSException, SQLException{

		String accountingEntryId = null;
		String poolId = null;

		/** ****** JTEST FIX start ************  on 14DEC09*/
//		Statement stmt = null;
		PreparedStatement pStmt = null;
		/** ****** JTEST FIX end **************  on 14DEC09*/
		ResultSet rs=null;
		Connection conn = null;
		AccountingDO acctDoObj = null;
		boolean flgSeperate = true;
		BigDecimal amtToPostCredit = null;
		BigDecimal amountToPostDebit= null;
		BigDecimal amountToPost = null;
		Date valueDate = null;
		Date accrualDate = null;
		Date adjAccrualDate = null;//accrual fix change

		String dateFormat = LMSConstants.DATE_FORMAT;
		//StringBuffer sbufUnpostedAmtInsr = new StringBuffer("");//INT_1039

		/** ****** JTEST FIX start ************  on 14DEC09*/
//		SimpleDateFormat formatter = new SimpleDateFormat(LMSConstants.DATE_FORMAT);
		SimpleDateFormat formatter = new SimpleDateFormat(LMSConstants.DATE_FORMAT, Locale.US);
		/** ****** JTEST FIX end **************  on 14DEC09*/
		String strbusinessdate = formatter.format(businessdate);
		long nbrProcessId=std.getProcessId();
		//SUP_1082 starts
		Date bvtStartDate = bvtDetails.getBvtValueDate();
		Date priorToBVTStartDate = addOffsetToDate(bvtStartDate, -1);
		String strPriorBVTStartDate = formatter.format(priorToBVTStartDate);
		//SUP_1082 ends		
		if (logger.isDebugEnabled())
			logger.debug("Entering");

		// Get the relevant Accounts for accrural
//		String sqlSelectAccnts = (new StringBuffer()
//		    .append(" select p.participant_id, dat_value, sum_dr_int, sum_cr_int, nbr_processid, id_pool, cod_party_ccy, flg_def_drcr_consol, nbr_coreaccount, cod_branch, cod_bnk_lgl, nbr_ibanaccnt, dat_business_poolrun, cod_gl ")
//			.append(" from opool_txnpool_partydtls p, ")
//			.append(" (select participant_id, max(dat_accrual) dat_value, sum(tmp.dr_int) sum_dr_int, sum(tmp.cr_int) sum_cr_int ")
//			.append("  from ")
//			.append(" (select diff.dat_business_poolrun, diff.participant_id, diff.dat_posting, diff.dat_accrual, ")
//			.append(" (CASE WHEN (hdr.TYP_ACCTINT_DRACT = 'POST' OR ")
//			.append(" (hdr.AMT_POOL_INTCALC_BALANCE < 0 AND hdr.TYP_ACCTINT_DRACT = 'SUPPDR') OR  ")
//			.append(" (hdr.AMT_POOL_INTCALC_BALANCE >= 0 AND  hdr.TYP_ACCTINT_DRACT = 'SUPPCR') ")//BVI_BTS change to TYP_ACCTINT_DRACT= 'SUPPCR'
//			.append(" ) ")
//			.append(" THEN diff.AMT_DR_INTEREST_DIFF ")
//			.append(" ELSE 0 ")
//			.append(" END ")
//			.append(" ) dr_int, ")
//			.append(" (CASE WHEN (hdr.TYP_ACCTINT_CRACT = 'POST' OR ")
//			.append(" (hdr.AMT_POOL_INTCALC_BALANCE < 0 AND hdr.TYP_ACCTINT_CRACT = 'SUPPDR') OR ")
//			.append(" (hdr.AMT_POOL_INTCALC_BALANCE >= 0 AND hdr.TYP_ACCTINT_CRACT = 'SUPPCR') ")
//			.append(" ) ")
//			.append(" THEN diff.AMT_CR_INTEREST_DIFF ")
//			.append(" ELSE 0 ")
//			.append(" END ")
//			.append(" ) cr_int ")
//			.append(" from opool_txnpoolhdr hdr, opool_txnpool_bvtdiff_int diff ")
//			.append("  where hdr.nbr_processid = diff.nbr_processid  ")
//			.append(" and hdr.id_pool = diff.id_pool ")
//			.append(" and hdr.dat_business_poolrun = diff.dat_business_poolrun ")
//			.append(" and hdr.nbr_processid= "+nbrProcessId)
//			.append(" and diff.dat_posting >= TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "') ")
//			.append(" and diff.dat_accrual < TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "') ")
//			.append(" and diff.flg_realpool = 'Y' and typ_posting = 'R' and cod_counter_account = 'B'  ")
//			.append(" and typ_participant = 'ACCOUNT' and (TYP_ACCTINT_CRACT <> 'SUPP' AND TYP_ACCTINT_DRACT <> 'SUPP') ) tmp ")
//			.append(" group by participant_id ) reqd ")
//			.append(" where p.dat_business_poolrun = dat_value and p.participant_id = reqd.participant_id  and p.nbr_processid=" + nbrProcessId)
//			.append(" and (sum_dr_int != 0 OR sum_cr_int != 0) ")).toString();
			StringBuilder sBuf = new StringBuilder();
			sBuf.append(" SELECT participant_id, dat_value, sum_dr_int, sum_cr_int, h.nbr_processid, ");
			sBuf.append(" h.id_pool, cod_party_ccy, flg_def_drcr_consol, nbr_coreaccount, ");
			sBuf.append(" cod_branch, cod_bnk_lgl, nbr_ibanaccnt, h.dat_business_poolrun, ");
			sBuf.append(" h.cod_gl, h.dat_accrual, h.dat_adjustedaccrual ");
			sBuf.append(" FROM opool_txnpoolhdr h, ");
			sBuf.append(" (SELECT p.participant_id, dat_value, sum_dr_int, sum_cr_int, ");
			sBuf.append(" nbr_processid, id_pool, cod_party_ccy, flg_def_drcr_consol, ");
			sBuf.append(" nbr_coreaccount, cod_branch, cod_bnk_lgl, nbr_ibanaccnt, ");
			sBuf.append(" dat_business_poolrun, cod_gl ");
			sBuf.append(" FROM opool_txnpool_partydtls p, ");
			sBuf.append(" (SELECT   participant_id, MAX (dat_business_poolrun) dat_value, ");			
			sBuf.append(" SUM (tmp.dr_int) sum_dr_int, ");
			sBuf.append(" SUM (tmp.cr_int) sum_cr_int ");
			sBuf.append(" FROM (SELECT diff.dat_business_poolrun, ");
			sBuf.append(" diff.participant_id, diff.dat_posting, ");
			sBuf.append(" diff.dat_accrual, ");
			sBuf.append(" diff.amt_dr_interest_diff dr_int, ");
			sBuf.append(" diff.amt_cr_interest_diff cr_int ");
			sBuf.append(" FROM opool_txnpoolhdr hdr, ");
			sBuf.append(" opool_txnpool_bvtdiff_int diff ");
			sBuf.append(" WHERE hdr.nbr_processid = diff.nbr_processid ");
			sBuf.append(" AND hdr.id_pool = diff.id_pool ");
			sBuf.append(" AND hdr.dat_business_poolrun = ");
			sBuf.append(" diff.dat_business_poolrun ");
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" AND hdr.nbr_processid = "+nbrProcessId);
			sBuf.append(" AND hdr.nbr_processid = ? ");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			
			//BVTAccrualBasedOnPosting starts
			//sBuf.append(" AND diff.dat_posting >= ");
			//sBuf.append(" TO_DATE ('"+strbusinessdate+"', '" + dateFormat + "') ");
			sBuf.append(" AND diff.dat_accrual > ");
			//SUP_1082c starts
			//sBuf.append(" (SELECT MAX(dat_accrual) ");
			sBuf.append(" (SELECT NVL(MAX(dat_accrual),TO_DATE ('"+strPriorBVTStartDate+"', '" + dateFormat + "')) ");
			//SUP_1082c ends
			sBuf.append(" FROM opool_txnpool_bvtdiff_int di ");
			sBuf.append(" WHERE di.NBR_PROCESSID = diff.NBR_PROCESSID ");
			sBuf.append(" AND di.ID_POOL = diff.ID_POOL ");
			sBuf.append(" AND di.PARTICIPANT_ID = diff.PARTICIPANT_ID ");
			sBuf.append(" AND di.DAT_ACCRUAL <=  ");
			sBuf.append(" (SELECT MAX(dat_posting) ");
			sBuf.append(" FROM opool_txnpool_bvtdiff_int d ");
			sBuf.append(" WHERE d.NBR_PROCESSID = diff.NBR_PROCESSID ");
			sBuf.append(" AND d.ID_POOL = diff.ID_POOL ");
			sBuf.append(" AND d.PARTICIPANT_ID = diff.PARTICIPANT_ID ");
			sBuf.append(" AND d.DAT_POSTING < TO_DATE ('"+strbusinessdate+"', '" + dateFormat + "') ");
			sBuf.append(" ) ");
			sBuf.append(" ) ");
			//BVTAccrualBasedOnPosting ends


			sBuf.append(" AND diff.dat_accrual < ");
			sBuf.append(" TO_DATE ('"+strbusinessdate+"', '" + dateFormat + "') ");
			sBuf.append(" AND diff.flg_realpool = 'Y' ");
			//SUP_1079 Fix starts
			//sBuf.append(" AND typ_posting = 'R' ");
			//SUP_1079 Fix ends
			sBuf.append(" AND cod_counter_account = 'B' ");
			sBuf.append(" AND typ_participant = 'ACCOUNT' ");
			//SUP_1079 Fix starts
			//sBuf.append(" AND (    typ_acctint_cract <> 'SUPP' ");
			//sBuf.append(" AND typ_acctint_dract <> 'SUPP' ");
			//sBuf.append(" )) tmp ");
			sBuf.append(" ) tmp ");
			//SUP_1079 Fix ends
			sBuf.append(" GROUP BY participant_id) reqd ");
			sBuf.append(" WHERE p.dat_business_poolrun = dat_value ");
			sBuf.append(" AND p.participant_id = reqd.participant_id ");
			/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
			// SUP_HSBC_041 Starts
			// It's possible that partcipants of type pool & account with same nbr_ownaccout/id_pool exists for a given root pool
			sBuf.append(" AND p.typ_participant = 'ACCOUNT' ");
			// SUP_HSBC_041 Ends
			/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" AND p.nbr_processid = "+nbrProcessId);
			sBuf.append(" AND p.nbr_processid = ?");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			sBuf.append(" AND (sum_dr_int != 0 OR sum_cr_int != 0)) FINAL ");
			sBuf.append(" WHERE h.nbr_processid = FINAL.nbr_processid ");
			sBuf.append(" AND h.dat_business_poolrun = FINAL.dat_value ");
			sBuf.append(" AND h.id_pool = FINAL.id_pool ");
			//ENH_10007 Starts
			//BVT Adjsutmet Accounting Entries should be generated per host.
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" AND h.cod_gl = '"+hostId+"'");
			sBuf.append(" AND h.cod_gl = ?");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			//ENH_10007 Ends
			String sqlSelectAccnts = sBuf.toString();


		if (logger.isDebugEnabled())
			logger.debug("Query ACCRUAL PARTICIPANT POSTING" + sqlSelectAccnts);

		try{
			conn = LMSConnectionUtility.getConnection();
			//SUP_1082a starts
			//Load the accrual dates cache.
			calculateNextCycleAccrualDates(conn, nbrProcessId, businessdate);
			//SUP_1082a ends
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			stmt = conn.createStatement();
//			rs = stmt.executeQuery(sqlSelectAccnts);
			
			pStmt = conn.prepareStatement(sqlSelectAccnts);
			pStmt.setLong(1, nbrProcessId);
			pStmt.setLong(2, nbrProcessId);
			pStmt.setString(3, hostId);
			rs = pStmt.executeQuery();
			/** ****** JTEST FIX end **************  on 14DEC09*/

			if (logger.isDebugEnabled()) {
				logger.debug(" Executed Query ACCRUAL PARTICIPANT POSTING ");
			}
			HashMap hmFields = new HashMap();
			hmFields.put(ExecutionConstants.PROCESSID,nbrProcessId); // Mashreq_AE_Hook
			hmFields.put(ExecutionConstants.BUSINESSDATE,businessdate); // Mashreq_AE_Hook fix
//			hmFields.put("DAT_POSTING",businessdate);
//			hmFields.put("DAT_SENT",businessdate);
			boolean results = false;
			while(rs.next())
			{
				valueDate = rs.getDate("DAT_VALUE");
				//accrual fix starts
				//SUP_1082a starts
				poolId = rs.getString("ID_POOL");
				//accrualDate = rs.getDate("dat_accrual");
				//adjAccrualDate = rs.getDate("dat_adjustedaccrual");
				//SUP_1082a ends
				//hmFields.put("DAT_POSTING",valueDate);
				//hmFields.put("DAT_SENT",valueDate);
				//SUP_1082a starts
				
				if (accCache == null || accCache.get(poolId)== null) {
					//15.1.1_Resource_Leaks_Fix
					LMSConnectionUtility.close(rs);
					rs=null;
					LMSConnectionUtility.close(pStmt);
					pStmt=null;
					//15.1.1_Resource_Leaks_Fix end
				    throw new LMSException("101302"," Error occured in forming the cache ");
	            }
				Date[] accDates = (Date[])accCache.get(poolId);
//				if(accDates[0] == null || accDates[0].compareTo(businessdate)<0){
//				    accrualDate = accDates[2];
//				    adjAccrualDate = accDates[3];
//				}
//				else {
				//Always consider the next cycle accrual dates
				    accrualDate = accDates[0];
				    adjAccrualDate = accDates[1];
//                }
				//SUP_1082a ends
				hmFields.put("DAT_POSTING",adjAccrualDate);
				hmFields.put("DAT_SENT",accrualDate);
				//accrual fix ends

				results = true;
				acctDoObj = new AccountingDO();
				String flg_def_drcr_consol = rs.getString("FLG_DEF_DRCR_CONSOL");
				flgSeperate = true;
				accountingEntryId = "0";
				//SUP_1082a starts
				//poolId = "";
				//SUP_1082a ends
				amtToPostCredit = new BigDecimal(rs.getString("SUM_CR_INT"));
				amountToPostDebit = new BigDecimal(rs.getString("SUM_DR_INT"));

				//SUP_1082a starts
				//poolId = rs.getString("ID_POOL");
				//SUP_1082a ends

				if (logger.isDebugEnabled()) {
					logger.debug("Resultset flg_def_drcr_consol" + flg_def_drcr_consol);
					logger.debug("Resultset POOLID" + poolId);
				}
				if("Y".equalsIgnoreCase(flg_def_drcr_consol!=null
															?flg_def_drcr_consol.trim()
															:"" )){
					//Get the NetInterest,if consolidate credit and debit interests
					amountToPost = amtToPostCredit.subtract(amountToPostDebit);
					// Set the seperate posting flag to False
					flgSeperate = false;

					if(amtToPostCredit.compareTo(zero)!=0 && amountToPostDebit.compareTo(zero)!=0){
						flgSeperate = true;
						//if both debit and credit interest are present.pass separate entries. as there will be both payables and receivables in this case
					}

				}
				if (logger.isDebugEnabled()) {
					logger.debug(" Flag Separate" + flgSeperate);
				}
				//fill in the AccountingDo object's attributes.
				acctDoObj.set_cod_bnk_lgl(rs.getString("COD_BNK_LGL"));
				acctDoObj.set_cod_branch(rs.getString("COD_BRANCH"));
				acctDoObj.set_cod_gl(rs.getString("COD_GL"));
				acctDoObj.set_cod_ccy(rs.getString("COD_PARTY_CCY"));
				acctDoObj.set_nbr_coreaccount(rs.getString("NBR_COREACCOUNT"));
				acctDoObj.set_nbr_ibanacct(rs.getString("NBR_IBANACCNT"));
				acctDoObj.set_nbr_ownaccount(rs.getString("PARTICIPANT_ID"));

				if (logger.isDebugEnabled()) {
					logger.debug("Account Object" + acctDoObj);
				}
				if(flgSeperate){
					if (amtToPostCredit.compareTo(zero) != 0){
						accountingEntryId =  "" +processBVTEntry(acctDoObj,
								amtToPostCredit,ExecutionConstants.ACCRUAL,
								//ExecutionConstants.CREDIT, ExecutionConstants.CREDIT_INTEREST,
								ExecutionConstants.BVT_CREDIT, ExecutionConstants.CREDIT_INTEREST,//DrCrAllocation
								/** ****** JTEST FIX start ************  on 14DEC09*/
//								pstmtAccountingEntry,pstmtAcctUnpostedInsert,poolId,hmFields);//INT_1039
								pstmtAccountingEntry,pstmtAcctUnpostedInsert,poolId,hmFields, conn);//INT_1039
								/** ****** JTEST FIX end **************  on 14DEC09*/
					}
					else{
						logger.fatal("Credit Interest is 0 for"  + acctDoObj + "SO not accruing the amount ");
					}
					if (amountToPostDebit.compareTo(zero) != 0){
						accountingEntryId = accountingEntryId + "|" + processBVTEntry(acctDoObj,
							amountToPostDebit,ExecutionConstants.ACCRUAL,
							//ExecutionConstants.DEBIT,ExecutionConstants.DEBIT_INTEREST,
							ExecutionConstants.BVT_DEBIT,ExecutionConstants.DEBIT_INTEREST,//DrCrAllocation
							/** ****** JTEST FIX start ************  on 14DEC09*/
//							pstmtAccountingEntry,pstmtAcctUnpostedInsert,poolId,hmFields);//INT_1039
							pstmtAccountingEntry,pstmtAcctUnpostedInsert,poolId,hmFields, conn);//INT_1039
							/** ****** JTEST FIX end **************  on 14DEC09*/
					}
					else{
						logger.fatal("Debit Interest is 0 for"  + acctDoObj + "SO not accruing the amount ");
					}
				}
				else{
					//call processEntry for the NetAmount --consolidated
					if (amountToPost.compareTo(zero) != 0){
						accountingEntryId =  ""+processBVTEntry(acctDoObj,
							amountToPost,ExecutionConstants.ACCRUAL,ExecutionConstants.NET,
							ExecutionConstants.NET_INTEREST,pstmtAccountingEntry,
							/** ****** JTEST FIX start ************  on 14DEC09*/
//							pstmtAcctUnpostedInsert,poolId,hmFields);//INT_1039
							pstmtAcctUnpostedInsert,poolId,hmFields, conn);//INT_1039
							/** ****** JTEST FIX end **************  on 14DEC09*/
					}else{
						logger.fatal("Net Interest is 0 for"  + acctDoObj + "SO not accruing the amount ");
					}
				}

				//Add to preparedStmt for AccountingEntryId updation
				if (logger.isDebugEnabled()) {
					logger.debug("After processEntry s_accountingEntryId"  + accountingEntryId);
				}
				//Update the Prepared Statements for OPOOL_PARTYDETAILS
				updateInterestBVT(pstmtTxnPoolPartyDtls,accountingEntryId,poolId,acctDoObj.get_nbr_ownaccount(),ExecutionConstants.ACCRUAL);
				//Add to the Pool List
				//updatePoolList(poolId, hpPoolList);

				//accrualEntries[ExecutionConstants.PARTY_DTLS_UPDATE] = true;
			}
			if (!results){
				logger.fatal(" No records for Accrual -participant interest" + sqlSelectAccnts);
			}

			//FOR ALLOCATION
			//accrual fix starts
//			String sqlAllocAccrual = (new StringBuffer(" SELECT ")
//			.append(" par.participant_id, dat_value, amount, par.id_pool, cod_party_ccy cod_ccy, nbr_coreaccount, cod_branch, cod_gl, ")
//			.append(" cod_bnk_lgl, nbr_ibanaccnt, dat_accrual ")
//			.append(" from opool_txnpool_partydtls par, ")
//			.append(" (select diff2.participant_id, dat_value, amount, diff2.nbr_processid, id_pool, dat_accrual ")
//			.append(" from opool_txnpool_bvtdiff diff2, ")
//			.append(" (select participant_id, max(dat_business_poolrun) dat_value, sum(amt_diff) amount ")
//			.append(" from opool_txnpool_bvtdiff ")
//			.append(" where nbr_processid= " + nbrProcessId)
//			.append(" and dat_posting >= TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "') " )
//			.append(" and dat_accrual < TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "') " )
//			.append(" and amt_diff != 0 and typ_participant = 'ACCOUNT' " )
//			.append(" and flg_realpool = 'Y' and sub_typ_calculation != 'R' ")
//			.append(" group by participant_id ) tmp ")
//			//All participant accounts for which accrual needs to be done
//			.append(" where diff2.nbr_processid = " + nbrProcessId)
//			.append(" and diff2.participant_id = tmp.participant_id ")
//			.append(" and diff2.dat_business_poolrun = tmp.dat_value ")
//			.append(" ) tmp2 ")
//			//Details of these accounts
//			.append(" where par.nbr_processid = tmp2.nbr_processid ")
//			.append(" and par.participant_id = tmp2.participant_id ")
//			.append(" and par.dat_business_poolrun = tmp2.dat_value ")
//			.append(" and par.id_pool = tmp2.id_pool ")
//			//Accrual dates for these accounts
//			.append(" UNION ")
//			.append(" select participant_id, dat_value, amount, id_pool, cod_ccy, nbr_coreaccount, cod_branch, ")
//			.append(" cod_gl, cod_bnk_lgl, nbr_ibanaccnt, dat_accrual ")
//			.append(" from orbicash_accounts acc, ")
//			.append(" (select tmp.participant_id, dat_value, amount, id_pool, nbr_processid, dat_accrual ")
//			.append(" from opool_txnpool_bvtdiff bd, ")
//			.append(" (select participant_id, max(dat_business_poolrun) dat_value, sum(amt_diff) amount ")
//			.append(" from opool_txnpool_bvtdiff ")
//			.append(" where nbr_processid = "+nbrProcessId)
//			.append(" and dat_posting >= TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "') " )
//			.append(" and dat_accrual < TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "') " )
//			.append(" and amt_diff != 0 and typ_participant = 'CENTRAL' " )
//			.append(" and flg_realpool = 'Y' and sub_typ_calculation != 'R' ")
//			.append(" group by participant_id ) tmp ")
//			//All central accounts for which accrual needs to be done
//			.append(" where bd.participant_id = tmp.participant_id ")
//			.append(" and bd.dat_business_poolrun = dat_value ")
//			.append(" and bd.nbr_processid = " + nbrProcessId)
//			.append(" ) tmp2 ")
//			//PoolIds of the accounts on the corresponding value date
//			.append(" where acc.nbr_ownaccount = tmp2.participant_id ")).toString();
//			//Accrual dates for these accounts
			sBuf.setLength(0);
			//sBuf.append(" SELECT participant_id, dat_value, amount, h.id_pool, cod_ccy, nbr_coreaccount, ");
			sBuf.append(" SELECT participant_id, dat_value, dr_sum, cr_sum, h.id_pool, cod_ccy, nbr_coreaccount, ");//DrCrAllocation
			sBuf.append(" cod_branch, h.cod_gl, cod_bnk_lgl, nbr_ibanaccnt, h.dat_accrual, ");
			sBuf.append(" dat_adjustedaccrual ");
			sBuf.append(" FROM opool_txnpoolhdr h, ");
			//sBuf.append(" (SELECT par.participant_id, dat_value, amount, tmp2.nbr_processid, ");
			sBuf.append(" (SELECT par.participant_id, dat_value, dr_sum, cr_sum, tmp2.nbr_processid, ");//DrCrAllocation
			sBuf.append(" par.id_pool, ");
			sBuf.append(" cod_party_ccy cod_ccy, nbr_coreaccount, cod_branch, ");
			sBuf.append(" cod_gl, cod_bnk_lgl, nbr_ibanaccnt, dat_accrual ");
			sBuf.append(" FROM opool_txnpool_partydtls par, ");
			//sBuf.append(" (SELECT diff2.participant_id, dat_value, amount, ");
			sBuf.append(" (SELECT diff2.participant_id, dat_value, dr_sum, cr_sum, ");//DrCrAllocation
			sBuf.append(" diff2.nbr_processid, id_pool, dat_accrual ");
			sBuf.append(" FROM opool_txnpool_bvtdiff diff2, ");
			sBuf.append(" (SELECT   participant_id, ");
			sBuf.append(" MAX (dat_business_poolrun) dat_value, ");
			//sBuf.append(" SUM (amt_diff) amount ");
			sBuf.append(" SUM(amt_dr_diff) dr_sum, SUM(amt_cr_diff) cr_sum ");//DrCrAllocation

			//BVTAccrualBasedOnPosting starts
			//sBuf.append(" FROM opool_txnpool_bvtdiff ");
			sBuf.append(" FROM opool_txnpool_bvtdiff diff ");
			//BVTAccrualBasedOnPosting ends

			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" WHERE nbr_processid = "+nbrProcessId);
			sBuf.append(" WHERE nbr_processid = ? ");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			
			//BVTAccrualBasedOnPosting Starts
			//sBuf.append(" AND dat_posting >= ");
			//sBuf.append(" TO_DATE ('" + strbusinessdate + "', '" + dateFormat + "') ");
			sBuf.append(" AND diff.dat_accrual > ");
			//SUP_1082c starts
			//sBuf.append(" (SELECT MAX(dat_accrual) ");
			sBuf.append(" (SELECT NVL(MAX(dat_accrual),TO_DATE ('"+strPriorBVTStartDate+"', '" + dateFormat + "')) ");
			//SUP_1082c ends
			sBuf.append(" FROM opool_txnpool_bvtdiff di ");
			sBuf.append(" WHERE di.NBR_PROCESSID = diff.NBR_PROCESSID ");
			sBuf.append(" AND di.ID_POOL = diff.ID_POOL ");
			sBuf.append(" AND di.PARTICIPANT_ID = diff.PARTICIPANT_ID ");
			sBuf.append(" AND di.DAT_ACCRUAL <= ");
			sBuf.append(" (SELECT MAX(dat_posting) ");
			sBuf.append(" FROM opool_txnpool_bvtdiff d ");
			sBuf.append(" WHERE d.NBR_PROCESSID = diff.NBR_PROCESSID ");
			sBuf.append(" AND d.ID_POOL = diff.ID_POOL ");
			sBuf.append(" AND d.PARTICIPANT_ID = diff.PARTICIPANT_ID ");
			sBuf.append(" AND d.DAT_POSTING < TO_DATE ('" + strbusinessdate + "', '" + dateFormat + "') ");	
			sBuf.append(" ) ");
			sBuf.append(" ) ");
			//BVTAccrualBasedOnPosting ends

			sBuf.append(" AND dat_accrual < ");
			sBuf.append(" TO_DATE ('" + strbusinessdate + "', '" + dateFormat + "') ");
			//sBuf.append(" AND amt_diff != 0 ");//DrCrAllocation
			sBuf.append(" AND typ_participant = 'ACCOUNT' ");
			sBuf.append(" AND flg_realpool = 'Y' ");
			sBuf.append(" AND sub_typ_calculation != 'R' ");
			//SUP_1082 starts
			//No need to accrue for type reallocation T
			sBuf.append(" AND sub_typ_calculation != 'T' ");
			//SUP_1082 ends
			sBuf.append(" GROUP BY participant_id) tmp ");
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" WHERE diff2.nbr_processid = "+nbrProcessId);
			sBuf.append(" WHERE diff2.nbr_processid = ?");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			sBuf.append(" AND diff2.participant_id = tmp.participant_id ");
			sBuf.append(" AND (dr_sum != 0 OR cr_sum != 0) ");//DrCrAllocation
			sBuf.append(" AND diff2.dat_business_poolrun = tmp.dat_value) tmp2 ");
			sBuf.append(" WHERE par.nbr_processid = tmp2.nbr_processid ");
			sBuf.append(" AND par.participant_id = tmp2.participant_id ");
			/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
			//SUP_HSBC_041 Fix starts
			// It's possible that partcipants of type pool & account with same nbr_ownaccout/id_pool exists for a given root pool
			sBuf.append(" AND par.typ_participant = 'ACCOUNT'");
			//SUP_HSBC_041 Fix ends
			/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
			sBuf.append(" AND par.dat_business_poolrun = tmp2.dat_value ");
			sBuf.append(" AND par.id_pool = tmp2.id_pool) FINAL ");
			sBuf.append(" WHERE h.nbr_processid = FINAL.nbr_processid ");
			sBuf.append(" AND h.dat_business_poolrun = dat_value ");
			//ENH_10007 Starts
			//BVT Adjsutmet Accounting Entries should be generated per host.
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" AND h.cod_gl = '"+hostId+"'");
			sBuf.append(" AND h.cod_gl = ?");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			//ENH_10007 Ends
			sBuf.append(" AND h.id_pool = FINAL.id_pool ");
			sBuf.append(" UNION ");
			//sBuf.append(" SELECT participant_id, dat_value, amount, id_pool, cod_ccy, nbr_coreaccount, ");
			sBuf.append(" SELECT participant_id, dat_value, dr_sum, cr_sum, id_pool, cod_ccy, nbr_coreaccount, ");//DrCrAllocation
			sBuf.append(" cod_branch, cod_gl, cod_bnk_lgl, nbr_ibanaccnt, dat_accrual, ");
			sBuf.append(" dat_adjustedaccrual ");
			sBuf.append(" FROM orbicash_accounts acc, ");
			//sBuf.append(" (SELECT participant_id, dat_value, amount, h.id_pool, h.nbr_processid, ");
			sBuf.append(" (SELECT participant_id, dat_value, dr_sum, cr_sum, h.id_pool, h.nbr_processid, ");//DrCrAllocation
			sBuf.append(" h.dat_accrual, dat_adjustedaccrual ");
			sBuf.append(" FROM opool_txnpoolhdr h, ");
			//sBuf.append(" (SELECT tmp.participant_id, dat_value, amount, id_pool, ");
			sBuf.append(" (SELECT tmp.participant_id, dat_value, dr_sum, cr_sum, id_pool, ");//DrCrAllocation
			sBuf.append(" nbr_processid, dat_accrual ");
			sBuf.append(" FROM opool_txnpool_bvtdiff bd, ");
			sBuf.append(" (SELECT   participant_id, ");
			sBuf.append(" MAX (dat_business_poolrun) dat_value, ");
			//sBuf.append(" SUM (amt_diff) amount ");
			sBuf.append(" SUM (amt_dr_diff) dr_sum, SUM (amt_cr_diff) cr_sum ");//DrCrAllocation
			
			//BVTAccrualBasedOnPosting Starts
			//sBuf.append(" FROM opool_txnpool_bvtdiff ");
			sBuf.append(" FROM opool_txnpool_bvtdiff diff ");
			//BVTAccrualBasedOnPosting ends

			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" WHERE nbr_processid = "+nbrProcessId);
			sBuf.append(" WHERE nbr_processid = ?");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			
			//BVTAccrualBasedOnPosting Starts
			//sBuf.append(" AND dat_posting >= ");
			//sBuf.append(" TO_DATE ('" + strbusinessdate + "', '" + dateFormat + "') ");
			sBuf.append(" AND diff.dat_accrual >  ");
			//SUP_1082c starts
			//sBuf.append(" (SELECT MAX(dat_accrual) ");
			sBuf.append(" (SELECT NVL(MAX(dat_accrual),TO_DATE ('"+strPriorBVTStartDate+"', '" + dateFormat + "')) ");
			//SUP_1082c ends
			sBuf.append(" FROM opool_txnpool_bvtdiff di ");
			sBuf.append(" WHERE di.NBR_PROCESSID = diff.NBR_PROCESSID ");
			sBuf.append(" AND di.ID_POOL = diff.ID_POOL ");
			sBuf.append(" AND di.PARTICIPANT_ID = diff.PARTICIPANT_ID ");
			sBuf.append(" AND di.DAT_ACCRUAL <= ");
			sBuf.append(" (SELECT MAX(dat_posting) ");
			sBuf.append(" FROM opool_txnpool_bvtdiff d ");
			sBuf.append(" WHERE d.NBR_PROCESSID = diff.NBR_PROCESSID ");
			sBuf.append(" AND d.ID_POOL = diff.ID_POOL ");
			sBuf.append(" AND d.PARTICIPANT_ID = diff.PARTICIPANT_ID ");
			sBuf.append(" AND d.DAT_POSTING < TO_DATE ('" + strbusinessdate + "', '" + dateFormat + "') ");
			sBuf.append(" ) ");
			sBuf.append(" ) ");
			//BVTAccrualBasedOnPosting ends

			sBuf.append(" AND dat_accrual < ");
			sBuf.append(" TO_DATE ('" + strbusinessdate + "', '" + dateFormat + "') ");
			//sBuf.append(" AND amt_diff != 0 ");//DrCrAllocation
			sBuf.append(" AND typ_participant = 'CENTRAL' ");
			sBuf.append(" AND flg_realpool = 'Y' ");
			sBuf.append(" AND sub_typ_calculation != 'R' ");
			//SUP_1082 starts
			//No need to accrue for type reallocation T
			sBuf.append(" AND sub_typ_calculation != 'T' ");
			//SUP_1082 ends
			sBuf.append(" GROUP BY participant_id) tmp ");
			sBuf.append(" WHERE bd.participant_id = tmp.participant_id ");
			sBuf.append(" AND bd.dat_business_poolrun = dat_value ");
			sBuf.append(" AND (dr_sum != 0 OR cr_sum != 0) ");//DrCrAllocation
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" AND bd.nbr_processid = "+nbrProcessId+") tmp2 ");
			sBuf.append(" AND bd.nbr_processid = ?) tmp2 ");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			sBuf.append(" WHERE h.nbr_processid = tmp2.nbr_processid ");
			sBuf.append(" AND h.id_pool = tmp2.id_pool ");
			//ENH_10007 Starts
			//BVT Adjsutmet Accounting Entries should be generated per host.
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" AND h.cod_gl = '"+hostId+"'");
			sBuf.append(" AND h.cod_gl = ?");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			//ENH_10007 Ends
			sBuf.append(" AND h.dat_business_poolrun = tmp2.dat_value) FINAL ");
			sBuf.append(" WHERE acc.nbr_ownaccount = FINAL.participant_id ");
			String sqlAllocAccrual = sBuf.toString();
			//accrual fix ends

			if (logger.isDebugEnabled())
				logger.debug("Query ACCRUAL ALLOCATION: " + sqlAllocAccrual);

			/** ****** JTEST FIX start ************  on 14DEC09*/
//			rs = stmt.executeQuery(sqlAllocAccrual.toString());
			LMSConnectionUtility.close(pStmt); //15.1.1_Resource_Leaks_Fix
			pStmt = conn.prepareStatement(sqlAllocAccrual);
			pStmt.setLong(1, nbrProcessId);
			pStmt.setLong(2, nbrProcessId);
			pStmt.setString(3, hostId);
			pStmt.setLong(4, nbrProcessId);
			pStmt.setLong(5, nbrProcessId);
			pStmt.setString(6, hostId);
			LMSConnectionUtility.close(rs); //15.1.1_Resource_Leaks_Fix
			rs = pStmt.executeQuery();
			/** ****** JTEST FIX end **************  on 14DEC09*/

			if (logger.isDebugEnabled()) {
				logger.debug(" Executed Query  ACCRUAL ALLOCATION ");
			}
			results = false;
			String details = "";
			while(rs.next()){
				results = true;
				//bd_amountToPost = new BigDecimal(0);
				acctDoObj = new AccountingDO();
				details = "";
				poolId = "";
				accountingEntryId = "";

				//accrual fix starts
				//SUP_1082a starts
				poolId = rs.getString("ID_POOL");
				//accrualDate = rs.getDate("dat_accrual");
				//adjAccrualDate = rs.getDate("dat_adjustedaccrual");
				//SUP_1082a ends
				//hmFields.put("DAT_POSTING",accrualDate);
				//hmFields.put("DAT_SENT",valueDate);
				//SUP_1082a starts
				LMSConnectionUtility.close(rs); //15.1.1_Resource_Leaks_Fix
				LMSConnectionUtility.close(pStmt); //15.1.1_Resource_Leaks_Fix
				if (accCache == null || accCache.get(poolId)== null) {					
				    throw new LMSException("101302"," Error occured in forming the cache ");
	            }
				Date[] accDates = (Date[])accCache.get(poolId);
//				if(accDates[0] == null || accDates[0].compareTo(businessdate)<0){
//				    accrualDate = accDates[2];
//				    adjAccrualDate = accDates[3];
//				}
//				else {
				//Always consider the next cycle accrual dates
				    accrualDate = accDates[0];
				    adjAccrualDate = accDates[1];
//                }
				//SUP_1082a ends
				hmFields.put("DAT_POSTING",adjAccrualDate);
				hmFields.put("DAT_SENT",accrualDate);
				//accrual fix ends

				//SUP_1082a starts
				//poolId = rs.getString("ID_POOL");
				//SUP_1082a ends
				//DrCrAllocation Starts
				//amountToPost = new BigDecimal(rs.getString("AMOUNT"));
				amtToPostCredit = new BigDecimal(rs.getString("CR_SUM"));
				amountToPostDebit = new BigDecimal(rs.getString("DR_SUM"));
				// Pass separate entries for both debit and credit allocation.
				flgSeperate = true;
				//flgSeperate = false;
				//DrCrAllocation Ends
				//flagAllocation = rs.getString("FLG_ALLOCATION");

				//fill in the AccountingDo object's attributes.
				acctDoObj.set_nbr_ownaccount(rs.getString("PARTICIPANT_ID"));
				acctDoObj.set_nbr_coreaccount(rs.getString("NBR_COREACCOUNT"));
				acctDoObj.set_cod_branch(rs.getString("COD_BRANCH"));
				acctDoObj.set_cod_gl(rs.getString("COD_GL"));
				acctDoObj.set_cod_bnk_lgl(rs.getString("COD_BNK_LGL"));
				acctDoObj.set_nbr_ibanacct(rs.getString("NBR_IBANACCNT"));
				acctDoObj.set_cod_ccy(rs.getString("COD_CCY"));
				//put this in extraDetailsField HashMap
				//extraDetails = "ALLOCATOR" + rs.getString("ALLOCATOR")+ "TYP_SETTLE" + rs.getString("TYP_SETTLE");
				details = "ALLOCATION";//this maps to TXT_EXTINFO2

				if (logger.isDebugEnabled()) {
					logger.debug("Account Object for Allocation" + acctDoObj);
				}
				//DrCrAllocation Starts
				//amtToPostCredit, amtToPostDebit should not be null
				amountToPost = amtToPostCredit.subtract(amountToPostDebit);
				//Separate entries
				if(flgSeperate){
					if (amtToPostCredit.compareTo(zero) != 0){
						/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
						//SUP_HSBC_025 Starts
						details = ExecutionConstants.ALLOCATIONCR;
						//SUP_HSBC_025 Ends
						/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
						accountingEntryId =  "" +processBVTEntry(acctDoObj,
								amtToPostCredit,ExecutionConstants.ACCRUAL,
								ExecutionConstants.BVT_CREDIT, details,
								/** ****** JTEST FIX start ************  on 14DEC09*/
//								pstmtAccountingEntry,pstmtAcctUnpostedInsert,poolId,hmFields);
								pstmtAccountingEntry,pstmtAcctUnpostedInsert,poolId,hmFields, conn);
								/** ****** JTEST FIX end **************  on 14DEC09*/
					}
					else{
						logger.fatal("Credit Allocation is 0 for"  + acctDoObj + "SO not accruing the amount ");
					}
					if (amountToPostDebit.compareTo(zero) != 0){
						/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
						//SUP_HSBC_025 Starts
						details = ExecutionConstants.ALLOCATIONDR;
						//SUP_HSBC_025 Ends
						/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
						accountingEntryId = accountingEntryId + "|" + processBVTEntry(acctDoObj,
							amountToPostDebit,ExecutionConstants.ACCRUAL,
							ExecutionConstants.BVT_DEBIT, details,
							/** ****** JTEST FIX start ************  on 14DEC09*/
//							pstmtAccountingEntry,pstmtAcctUnpostedInsert,poolId,hmFields);
							pstmtAccountingEntry,pstmtAcctUnpostedInsert,poolId,hmFields, conn);
							/** ****** JTEST FIX end **************  on 14DEC09*/
					}
					else{
						logger.fatal("Debit Allocation is 0 for"  + acctDoObj + "SO not accruing the amount ");
					}						
				}
				//Consolidated entry i.e. prior to DrCrAllocation change
//				else {
//					if (amountToPost.compareTo(zero) != 0){
//						accountingEntryId = "" + processEntry(acctDoObj,
//								amountToPost,ExecutionConstants.ACCRUAL,
//								ExecutionConstants.NET, details,pstmtAccountingEntry,
//								pstmtAcctUnpostedInsert,poolId,hmFields);//INT_1039
//					}
//					else{
//						if (logger.isDebugEnabled()) {
//							logger.debug("Not Accruing alocated amount as it is 0 " + acctDoObj);
//						}
//					}
//				}
				//DrCrAllocation
				//accrualEntries[ExecutionConstants.OLM_ACCOUNTINGENTRY_INSERT] = true;
				//call Allocation update
				//updateAllocationAccrual(pstmtTxnPoolAllocation,acctDoObj.get_nbr_ownaccount(),s_accountingEntryId,s_poolId, s_flagAllocation);
				//accrualEntries[ExecutionConstants.ALLOCATION_UPDATE] = true;
			}
			if (!results){
				logger.fatal(" No records for Accrual -Allocation" + sqlAllocAccrual);
			}
			/*INT_1039--Starts
			if (logger.isDebugEnabled()) {
				logger.debug("Buffer value FOR INSERTING ACCOUNT UNPOSTED TABLE=  " + sbufUnpostedAmtInsr);
			}

			if(!("".equals(sbufUnpostedAmtInsr.toString()))){
				accrualEntries[ExecutionConstants.UNPOSTED_AMOUNT_INSERT] = (new Boolean(sbufUnpostedAmtInsr.toString())).booleanValue();
			}
			//INT_1039--Ends
			*/
			//BVI_BTS Starts
			//generateBVTBankShareTreasuryShareEntries(pstmtAccountingEntry, pstmtAcctUnpostedInsert,businessdate,strbusinessdate,ExecutionConstants.ACCRUAL,generateFor,generateForId,client);
			//SUP_1082c starts
			//generateBVTBankShareTreasuryShareEntries(pstmtAccountingEntry, pstmtAcctUnpostedInsert,businessdate,strbusinessdate,ExecutionConstants.ACCRUAL,std);
			//ENH_10007 Starts
			//BVT Adjsutmet Accounting Entries should be generated per host.
			//generateBVTBankShareTreasuryShareEntries(pstmtAccountingEntry, pstmtAcctUnpostedInsert,businessdate,strbusinessdate,ExecutionConstants.ACCRUAL,std,bvtDetails);
			generateBVTBankShareTreasuryShareEntries(pstmtAccountingEntry, pstmtAcctUnpostedInsert,businessdate,strbusinessdate,ExecutionConstants.ACCRUAL,std,bvtDetails,hostId);
			//ENH_10007 Ends
			//SUP_1082c ends
			//BVI_BTS ends

		/** ****** FindBugs Fix Start ***********  on 14DEC09*/
		}catch(SQLException e){
			logger.fatal("generateAccrualEntries: ", e);
			throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, e.getMessage());
		}catch(NamingException e){
			logger.fatal("generateAccrualEntries: ", e);
			throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, e.getMessage());
		/** ****** FindBugs Fix End ***********  on 14DEC09*/
		}finally{
			LMSConnectionUtility.close(rs);
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			LMSConnectionUtility.close(stmt);
			LMSConnectionUtility.close(pStmt);
			/** ****** JTEST FIX end **************  on 14DEC09*/
			LMSConnectionUtility.close(conn);
		}
		return accrualEntries;
	}

/**
 * @param pstmtTxnPoolPartyDtls
 * @param entryId
 * @param id
 * @param businessdate
 * @param _nbr_ownaccount
 * @throws SQLException
 * @throws LMSException
 */

private void updateInterestBVT(PreparedStatement pstmtTxnPoolPartyDtls, String acctEntryId, String poolId, String nbr_ownaccount,String type) throws LMSException {

	try {

		if (logger.isDebugEnabled()) {
			logger.debug("pstmtTxnPoolPartyDtls.setString(1,entryId); //  NBR_ACCTENTRYID"+acctEntryId);
			logger.debug("pstmtTxnPoolPartyDtls.setString(4,poolId); //" +poolId);
			logger.debug("pstmtTxnPoolPartyDtls.setString(3,nbr_ownaccount) "+nbr_ownaccount);
		}
		pstmtTxnPoolPartyDtls.setString(1,acctEntryId);
		pstmtTxnPoolPartyDtls.setString(3, nbr_ownaccount);
		pstmtTxnPoolPartyDtls.setString(4, poolId);

		pstmtTxnPoolPartyDtls.addBatch();
		if(ExecutionConstants.ACCRUAL.equals(type)){
			accrualEntries[ExecutionConstants.PARTY_DTLS_UPDATE] = true;
		}else{
			postingEntries[ExecutionConstants.PARTY_DTLS_UPDATE] = true;
		}



	} catch (SQLException e) {
		logger.fatal(
			  "SQL Exception in updating the PartyDetails Table", e);
			//e.printStackTrace();//SystemErrLogs issue
		  throw new LMSException("99024", e.toString());
		//e.printStackTrace();
	}
	catch (Exception e) {
		logger.fatal(
			  "SQL Exception in updating the PartyDetails Table", e);
			//e.printStackTrace();//SystemErrLogs issue
		  throw new LMSException("99024", e.toString());
		//e.printStackTrace();
	}
	if (logger.isDebugEnabled()) {
		logger.debug("Leaving ");
	}
}
	//BVT_TXN Ends here
//BVI Bank & Treasury Share BVI_BTS Starts
/**Created Method for passing Accrual and Posting entries of Bank Share and Treasury Share
 *
 * @param pstmt
 * @param pstmtAcctUnpostedInsert
 * @param businessDate
 * @param strbusinessdate
 * @param type
 * @param generateFor
 * @param generateForId
 * @param client
 * @throws LMSException
 */
	private void generateBVTBankShareTreasuryShareEntries(PreparedStatement pstmt,PreparedStatement pstmtAcctUnpostedInsert,Date businessDate,
			//SUP_1082c starts	
	        //String strbusinessdate,String type,SweepsTriggerDetails std) throws LMSException{
	        //ENH_10007 Starts
	        //BVT Adjsutmet Accounting Entries should be generated per host.
	    	//String strbusinessdate,String type,SweepsTriggerDetails std, BVTDetails bvtDetails) throws LMSException{
	    	String strbusinessdate,String type,SweepsTriggerDetails std, BVTDetails bvtDetails, String hostId) throws LMSException{
	    	//ENH_10007 Ends
	    	//SUP_1082c ends

		/** ****** JTEST FIX start ************  on 14DEC09*/
//		Statement stmt = null;
		PreparedStatement pStmt = null;
		/** ****** JTEST FIX end **************  on 14DEC09*/
		ResultSet rs=null;
		Connection conn = null;
		AccountingDO acctDoObj = null;
		BigDecimal amountToPost	= null;
		BigDecimal amtToPostCredit = null;//DrCrAllocation
		BigDecimal amtToPostDebit= null;////DrCrAllocation
		String acctShare = "";
		String acctShareCr = "";//DrCrAllocation
		String acctShareDr = "";//DrCrAllocation
		boolean flgSeperate = true;//DrCrAllocation
		String accountingEntryId = "";
		String shareType = "";
		HashMap hmFields = new HashMap();
		//SUP_1082b starts
		HashMap hmAccrualFields = new HashMap();
		//SUP_1082b ends
		String poolId = "";
		boolean results = false;
		long processId = std.getProcessId();
		//INT_1055 starts
		Date postSettlementDate = null;
		Date actualPostingDate = null;
		Date valueDate = null;
		Date accrualDate = null;
		//INT_1055 ends
		Date adjAccrualDate = null;
		StringBuilder sBuf = new StringBuilder();
		//SUP_1082a starts
		Date currentDate = std.getBusinessDate();
		//SUP_1082a ends
		//SUP_1082c starts
		Date bvtStartDate = null;
		Date priorToBVTStartDate = null;
		String strPriorBVTStartDate = null;
		hmFields.put(ExecutionConstants.PROCESSID, processId); // Mashreq_AE_Hook
		hmFields.put(ExecutionConstants.BUSINESSDATE, businessDate); // Mashreq_AE_Hook fix
		String dateFormat = LMSConstants.DATE_FORMAT;
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
		SimpleDateFormat formatter = new SimpleDateFormat(dateFormat, Locale.US);
		/** ****** JTEST FIX end **************  on 14DEC09*/
		if (ExecutionConstants.ACCRUAL.equals(type)){
		    if(bvtDetails==null){
		        if(logger.isFatalEnabled()){
			        logger.fatal(" BVTDetails object should not be null ");
			    }
		        throw new LMSException("200100"," BVTDetails should not be null in case of Accrual for BVTBankTreasuryShare Entries.");
		    }
		    bvtStartDate = bvtDetails.getBvtValueDate();
		    priorToBVTStartDate = addOffsetToDate(bvtStartDate, -1);
		    strPriorBVTStartDate = formatter.format(priorToBVTStartDate);
		}		 
		//SUP_1082c ends
		try {
			if (ExecutionConstants.ACCRUAL.equals(type)){
				//ACCRUAL
				/*
           		  SELECT dat_value, tmp.amount, hdr.id_pool, ble.acct_bankshare,
					   'BANKSHARE' share_type, dat_accrual
				  FROM opool_txnpoolhdr hdr,
					   opool_blelink ble,
					   (SELECT   id_pool, MAX (dat_business_poolrun) dat_value,
								 SUM (amt_diff) amount
							FROM opool_txnpool_bvtdiff
						   WHERE nbr_processid = 639
							 AND dat_posting >= '27-Jan-2006'                    --currentDate
							 AND dat_accrual < '27-Jan-2006'                     --currentDate
							 AND typ_participant = 'BANKSHARE'
							 AND flg_realpool = 'Y'
						GROUP BY id_pool) tmp
				 WHERE hdr.id_pool = tmp.id_pool
				   AND hdr.dat_business_poolrun = tmp.dat_value
				   AND hdr.nbr_processid = 639                           -- current Process Id
				   AND amount != 0
				   AND ble.cod_bnk_lgl = hdr.cod_base_bnk_lgl
				   AND ble.cod_ccy = hdr.cod_base_ccy_pool
				   AND ble.cod_gl = hdr.cod_gl
				UNION
				SELECT tmp.dat_value, tmp.amount, hdr.id_pool,
					   (CASE
						   WHEN amount >= 0
							  THEN acct_treasury_cr
						   ELSE acct_treasury_dr
						END) acct, 'TREASURYSHARE' share_type, dat_accrual
				  FROM opool_txnpoolhdr hdr,
					   (SELECT   id_pool, MAX (dat_business_poolrun) dat_value,
								 SUM (amt_diff) amount
							FROM opool_txnpool_bvtdiff
						   WHERE nbr_processid = 639
							 AND dat_posting >= '27-Jan-2006'                    --currentDate
							 AND dat_accrual < '27-Jan-2006'                     --currentDate
							 AND typ_participant = 'TREASURY'
							 AND flg_realpool = 'Y'
						GROUP BY id_pool) tmp
				 WHERE hdr.id_pool = tmp.id_pool
				   AND hdr.dat_business_poolrun = tmp.dat_value
				   AND hdr.nbr_processid = 639                           -- current Process Id
				   AND amount != 0

				*/
				sBuf.setLength(0);
				//sBuf.append(" SELECT dat_value, tmp.amount SUM_AMT, hdr.id_pool, ble.acct_bankshare ACCT, ");
				sBuf.append(" SELECT dat_value, tmp.dr_sum, tmp.cr_sum, hdr.id_pool, ble.acct_bankshare ACCT_CR, ble.acct_bankshare ACCT_DR, ");//DrCrAllocation
				sBuf.append(" 'BANKSHARE' share_type, dat_accrual, dat_adjustedaccrual ");
				sBuf.append(" FROM opool_txnpoolhdr hdr, ");
				sBuf.append(" opool_blelink ble, ");
				sBuf.append(" (SELECT id_pool, MAX (dat_business_poolrun) dat_value, ");
				//sBuf.append(" SUM (amt_diff) amount ");
				sBuf.append(" SUM (amt_dr_diff) dr_sum, SUM (amt_cr_diff) cr_sum ");//DrCrAllocation
				
				//BVTAccrualBasedOnPosting Starts
				//sBuf.append(" FROM opool_txnpool_bvtdiff ");
				sBuf.append(" FROM opool_txnpool_bvtdiff diff ");
				//BVTAccrualBasedOnPosting ends

				/** ****** JTEST FIX start ************  on 14DEC09*/
//				sBuf.append(" WHERE nbr_processid = "+processId);// -- processId
				sBuf.append(" WHERE nbr_processid = ? ");// -- processId
				/** ****** JTEST FIX end **************  on 14DEC09*/
				
				//BVTAccrualBasedOnPosting Starts
				//sBuf.append(" AND dat_posting >= TO_DATE('"+strbusinessdate+"','" + dateFormat + "') ");// --currentDate
				sBuf.append(" AND diff.dat_accrual > ");
				//SUP_1082c starts
				//sBuf.append(" (SELECT MAX(dat_accrual) ");
				sBuf.append(" (SELECT NVL(MAX(dat_accrual),TO_DATE ('"+strPriorBVTStartDate+"', '" + dateFormat + "')) ");
				//SUP_1082c ends
				sBuf.append(" FROM opool_txnpool_bvtdiff di ");
				sBuf.append(" WHERE di.NBR_PROCESSID = diff.NBR_PROCESSID ");
				sBuf.append(" AND di.ID_POOL = diff.ID_POOL ");
				sBuf.append(" AND di.PARTICIPANT_ID = diff.PARTICIPANT_ID ");
				sBuf.append(" AND di.DAT_ACCRUAL <=  ");
				sBuf.append(" (SELECT MAX(dat_posting) ");
				sBuf.append(" FROM opool_txnpool_bvtdiff d ");
				sBuf.append(" WHERE d.NBR_PROCESSID = diff.NBR_PROCESSID ");
				sBuf.append(" AND d.ID_POOL = diff.ID_POOL ");
				sBuf.append(" AND d.PARTICIPANT_ID = diff.PARTICIPANT_ID ");
				sBuf.append(" AND d.DAT_POSTING < TO_DATE('"+strbusinessdate+"','" + dateFormat + "') ");
				sBuf.append(" ) ");
				sBuf.append(" ) ");
				//BVTAccrualBasedOnPosting ends

				sBuf.append(" AND dat_accrual < TO_DATE('"+strbusinessdate+"','" + dateFormat + "') ");//--currentDate
				sBuf.append(" AND typ_participant = 'BANKSHARE' ");
				sBuf.append(" AND flg_realpool = 'Y' ");
				sBuf.append(" GROUP BY id_pool) tmp ");
				sBuf.append(" WHERE hdr.id_pool = tmp.id_pool ");
				sBuf.append(" AND hdr.dat_business_poolrun = tmp.dat_value ");
				/** ****** JTEST FIX start ************  on 14DEC09*/
//				sBuf.append(" AND hdr.nbr_processid = "+processId);//-- current Process Id
				sBuf.append(" AND hdr.nbr_processid = ?");//-- current Process Id
				/** ****** JTEST FIX end **************  on 14DEC09*/
				//sBuf.append(" AND amount != 0 ");
				sBuf.append(" AND (dr_sum != 0 OR cr_sum != 0) ");//DrCrAllocation
				sBuf.append(" AND ble.cod_bnk_lgl = hdr.cod_base_bnk_lgl ");
				sBuf.append(" AND ble.cod_ccy = hdr.cod_base_ccy_pool ");
				sBuf.append(" AND ble.cod_gl = hdr.cod_gl ");
				sBuf.append(" UNION ");
				//DrCrAllocation starts
				//sBuf.append(" SELECT tmp.dat_value, tmp.amount SUM_AMT, hdr.id_pool, ");
				sBuf.append(" SELECT tmp.dat_value, tmp.dr_sum, tmp.cr_sum, hdr.id_pool, ");
//				sBuf.append(" (CASE ");
//				sBuf.append(" WHEN amount >= 0 ");
//				sBuf.append(" THEN acct_treasury_cr ");
//				sBuf.append(" ELSE acct_treasury_dr ");
//				sBuf.append(" END) acct, 'TREASURYSHARE' share_type, dat_accrual, dat_adjustedaccrual ");
				sBuf.append(" acct_treasury_cr ACCT_CR, acct_treasury_dr ACCT_DR, 'TREASURYSHARE' share_type, dat_accrual, dat_adjustedaccrual ");
				//DrCrAllocation ends
				sBuf.append(" FROM opool_txnpoolhdr hdr, ");
				sBuf.append(" (SELECT id_pool, MAX (dat_business_poolrun) dat_value, ");
				//sBuf.append(" SUM (amt_diff) amount ");
				sBuf.append(" SUM (amt_dr_diff) dr_sum, SUM (amt_cr_diff) cr_sum ");//DrCrAllocation
				
				//BVTAccrualBasedOnPosting Starts
				//sBuf.append(" FROM opool_txnpool_bvtdiff ");
				sBuf.append(" FROM opool_txnpool_bvtdiff diff ");
				//BVTAccrualBasedOnPosting ends

				/** ****** JTEST FIX start ************  on 14DEC09*/
//				sBuf.append(" WHERE nbr_processid = "+processId);
				sBuf.append(" WHERE nbr_processid = ? ");
				/** ****** JTEST FIX end **************  on 14DEC09*/
				
				//BVTAccrualBasedOnPosting Starts
				//sBuf.append(" AND dat_posting >= TO_DATE('"+strbusinessdate+"','" + dateFormat + "') ");//--currentDate
				sBuf.append(" AND diff.dat_accrual >  ");
				//SUP_1082c starts
				//sBuf.append(" (SELECT MAX(dat_accrual) ");
				sBuf.append(" (SELECT NVL(MAX(dat_accrual),TO_DATE ('"+strPriorBVTStartDate+"', '" + dateFormat + "')) ");
				//SUP_1082c ends
				sBuf.append(" FROM opool_txnpool_bvtdiff di ");
				sBuf.append(" WHERE di.NBR_PROCESSID = diff.NBR_PROCESSID ");
				sBuf.append(" AND di.ID_POOL = diff.ID_POOL ");
				sBuf.append(" AND di.PARTICIPANT_ID = diff.PARTICIPANT_ID ");
				sBuf.append(" AND di.DAT_ACCRUAL <=  ");
				sBuf.append(" (SELECT MAX(dat_posting) ");
				sBuf.append(" FROM opool_txnpool_bvtdiff d ");
				sBuf.append(" WHERE d.NBR_PROCESSID = diff.NBR_PROCESSID ");
				sBuf.append(" AND d.ID_POOL = diff.ID_POOL ");
				sBuf.append(" AND d.PARTICIPANT_ID = diff.PARTICIPANT_ID ");
				sBuf.append(" AND d.DAT_POSTING < TO_DATE('"+strbusinessdate+"','" + dateFormat + "') ");
				sBuf.append(" ) ");
				sBuf.append(" ) ");
				//BVTAccrualBasedOnPosting ends

				sBuf.append(" AND dat_accrual < TO_DATE('"+strbusinessdate+"','" + dateFormat + "') ");//--currentDate
				sBuf.append(" AND typ_participant = 'TREASURY' ");
				sBuf.append(" AND flg_realpool = 'Y' ");
				sBuf.append(" GROUP BY id_pool) tmp ");
				sBuf.append(" WHERE hdr.id_pool = tmp.id_pool ");
				sBuf.append(" AND hdr.dat_business_poolrun = tmp.dat_value ");
				/** ****** JTEST FIX start ************  on 14DEC09*/
//				sBuf.append(" AND hdr.nbr_processid = "+processId);//-- current Process Id
				sBuf.append(" AND hdr.nbr_processid = ? ");//-- current Process Id
				/** ****** JTEST FIX end **************  on 14DEC09*/
				//sBuf.append(" AND amount != 0 ");
				sBuf.append(" AND (dr_sum != 0 OR cr_sum != 0) ");//DrCrAllocation
				//ENH_10007 Starts
				//BVT Adjsutmet Accounting Entries should be generated per host.
				/** ****** JTEST FIX start ************  on 14DEC09*/
//				sBuf.append(" AND hdr.cod_gl = '"+hostId+"'");
				sBuf.append(" AND hdr.cod_gl = ? ");
				/** ****** JTEST FIX end **************  on 14DEC09*/
				//ENH_10007 Ends
				if (logger.isDebugEnabled()) {
					logger.debug("Bank Share Treasury Share ");
				}
				//EXECUTE BANKSHARE TREASURY SHARE QUERY
				//FOR ACCRUAL
				if (logger.isDebugEnabled()){
					logger.debug("Executing Query Bank Share Treasury" + sBuf.toString());
				}

				conn = LMSConnectionUtility.getConnection();
				/** ****** JTEST FIX start ************  on 14DEC09*/
//				stmt = conn.createStatement();
//				rs = stmt.executeQuery(sBuf.toString());
				
				pStmt = conn.prepareStatement(sBuf.toString());
				pStmt.setLong(1, processId);
				pStmt.setLong(2, processId);
				pStmt.setLong(3, processId);
				pStmt.setLong(4, processId);
				pStmt.setString(5, hostId);
				rs = pStmt.executeQuery();
				/** ****** JTEST FIX end **************  on 14DEC09*/

				if (logger.isDebugEnabled()) {
					logger.debug(" Executed Query BANKSHARE ");
				}

				while(rs.next()){
					results = true;
					acctDoObj = new AccountingDO();
					accountingEntryId = "0";
					//DrCrAllocation Starts
					//amountToPost = rs.getBigDecimal("SUM_AMT");
					amtToPostCredit = rs.getBigDecimal("cr_sum");
					amtToPostDebit = rs.getBigDecimal("dr_sum");
					//DrCrAllocation Ends
					shareType = rs.getString("SHARE_TYPE");
					//DrCrAllocation Starts
					//acctShare = rs.getString("ACCT");
					acctShareCr = rs.getString("ACCT_CR");
					acctShareDr = rs.getString("ACCT_DR");
					//DrCrAllocation Ends
					poolId = rs.getString("ID_POOL");

					//SUP_1082a starts
					//accrualDate = rs.getDate("DAT_ACCRUAL");
					//SUP_1082a ends
					//valueDate = rs.getDate("DAT_VALUE");
					//hmFields.put("DAT_POSTING",accrualDate);
					//hmFields.put("DAT_SENT",valueDate);
					//SUP_1082a starts
					//adjAccrualDate = rs.getDate("dat_adjustedaccrual");
					//SUP_1082a ends
					//SUP_1082a starts
					if (accCache == null || accCache.get(poolId)== null) {
						//15.1.1_Resource_Leaks_Fix start
						LMSConnectionUtility.close(rs);
						rs=null;
						LMSConnectionUtility.close(pStmt);
						pStmt=null;
						//15.1.1_Resource_Leaks_Fix end
					    throw new LMSException("101302"," Error occured in forming the cache ");
		            }
					Date[] accDates = (Date[])accCache.get(poolId);
//					if(accDates[0] == null || accDates[0].compareTo(currentDate)<0){
//					    accrualDate = accDates[2];
//					    adjAccrualDate = accDates[3];
//					}
//					else {
					//Always consider the next cycle accrual dates
					    accrualDate = accDates[0];
					    adjAccrualDate = accDates[1];
//	                }
					//SUP_1082a ends
					
					hmFields.put("DAT_POSTING",adjAccrualDate);
					hmFields.put("DAT_SENT",accrualDate);
					//DrCrAllocation starts
//					if (amountToPost.compareTo(zero) != 0 && !(acctShare == null || "".equals(acctShare))){
//						acctDoObj = getAccount(acctShare);
//						accountingEntryId =  ""+processBVTEntry(acctDoObj,
//							amountToPost,type,ExecutionConstants.NET,
//							shareType,pstmt,pstmtAcctUnpostedInsert,poolId,hmFields);//INT_1039
//					}else{
//						logger.fatal("Bank Share /Treasury Share Amt is 0 for"  + acctDoObj + "SO not accruing the amount ");
//					}
					if(flgSeperate){
						if (amtToPostCredit.compareTo(zero) != 0){
							/** ****** JTEST FIX start ************  on 14DEC09*/
//							acctDoObj = getAccount(acctShareCr);
							acctDoObj = getAccount(acctShareCr, conn);
							/** ****** JTEST FIX end **************  on 14DEC09*/
							accountingEntryId =  "" +processBVTEntry(acctDoObj,
									amtToPostCredit,ExecutionConstants.ACCRUAL,
									ExecutionConstants.BVT_CREDIT, shareType,
									/** ****** JTEST FIX start ************  on 14DEC09*/
//									pstmt,pstmtAcctUnpostedInsert,poolId,hmFields);//INT_1039
									pstmt,pstmtAcctUnpostedInsert,poolId,hmFields,conn);//INT_1039
									/** ****** JTEST FIX end **************  on 14DEC09*/
						}
						else{
							logger.fatal("Credit B&T Allocation is 0 for"  + acctDoObj + "SO not accruing the amount ");
						}
						if (amtToPostDebit.compareTo(zero) != 0){
							/** ****** JTEST FIX start ************  on 14DEC09*/
//							acctDoObj = getAccount(acctShareDr);
							acctDoObj = getAccount(acctShareDr, conn);
							/** ****** JTEST FIX end **************  on 14DEC09*/
							accountingEntryId = accountingEntryId + "|" + processBVTEntry(acctDoObj,
								amtToPostDebit,ExecutionConstants.ACCRUAL,
								ExecutionConstants.BVT_DEBIT,shareType,
								/** ****** JTEST FIX start ************  on 14DEC09*/
//								pstmt,pstmtAcctUnpostedInsert,poolId,hmFields);//INT_1039
								pstmt,pstmtAcctUnpostedInsert,poolId,hmFields, conn);//INT_1039
								/** ****** JTEST FIX end **************  on 14DEC09*/
						}
						else{
							logger.fatal("Debit B&T Allocation is 0 for"  + acctDoObj + "SO not accruing the amount ");
						}
					}
					//DrCrAllocation ends

					if (logger.isDebugEnabled()) {
						logger.debug("Bank Share NBR_ACCTENTRYID"+accountingEntryId);
					}

				}
				if (!results){
					logger.fatal(" No records for BankShare Accrual" + sBuf.toString());
				}
				rs.close();
			}
			//POSTING
			if (ExecutionConstants.POSTING.equals(type)){
				/*
				SELECT dat_value, tmp.amount, hdr.id_pool, ble.acct_bankshare acct,
			   'BANKSHARE' share_type, dat_posting, dat_post_settlement,
			   dat_post_creditdelay, dat_post_debitdelay,
			   (SELECT dat_post_debitdelay
				  FROM opool_txnpoolhdr h1
				 WHERE h1.id_pool = hdr.id_pool
				   AND h1.nbr_processid = hdr.nbr_processid
				   AND h1.dat_business_poolrun = dat_value + 1) next_debit_delay,
			   (SELECT dat_post_creditdelay
				  FROM opool_txnpoolhdr h2
				 WHERE h2.id_pool = hdr.id_pool
				   AND h2.nbr_processid = hdr.nbr_processid
				   AND h2.dat_business_poolrun = dat_value + 1) next_credit_delay
		  FROM opool_txnpoolhdr hdr,
			   opool_blelink ble,
			   (SELECT   id_pool, MAX (dat_business_poolrun) dat_value,
						 SUM (amt_diff) amount
					FROM opool_txnpool_bvtdiff
				   WHERE nbr_processid = 639                     -- current Process Id
					 AND dat_posting < '27-Jan-2006'                     --currentDate
					 AND flg_realpool = 'Y'
					 AND typ_calculation = 'BANKSHARE'
					 AND sub_typ_calculation = 'A'    -- Cod_Allocation_Sum of poolhdr
				GROUP BY id_pool) tmp
		 WHERE hdr.id_pool = tmp.id_pool
		   AND hdr.dat_business_poolrun = tmp.dat_value
		   AND hdr.nbr_processid = 639                           -- current Process Id
		   AND amount != 0
		   AND ble.cod_bnk_lgl = hdr.cod_base_bnk_lgl
		   AND ble.cod_ccy = hdr.cod_base_ccy_pool
		   AND ble.cod_gl = hdr.cod_gl
		UNION
		SELECT dat_value, tmp.amount, hdr.id_pool,
			   (CASE
				   WHEN amount >= 0
					  THEN acct_treasury_cr
				   ELSE acct_treasury_dr
				END) acct, 'TREASURYSHARE' share_type, dat_posting,
			   dat_post_settlement, dat_post_creditdelay, dat_post_debitdelay,
			   (SELECT dat_post_debitdelay
				  FROM opool_txnpoolhdr h1
				 WHERE h1.id_pool = hdr.id_pool
				   AND h1.nbr_processid = hdr.nbr_processid
				   AND h1.dat_business_poolrun = dat_value + 1) next_debit_delay,
			   (SELECT dat_post_creditdelay
				  FROM opool_txnpoolhdr h2
				 WHERE h2.id_pool = hdr.id_pool
				   AND h2.nbr_processid = hdr.nbr_processid
				   AND h2.dat_business_poolrun = dat_value + 1) next_credit_delay
		  FROM opool_txnpoolhdr hdr,
			   (SELECT   id_pool, MAX (dat_business_poolrun) dat_value,
						 SUM (amt_diff) amount
					FROM opool_txnpool_bvtdiff
				   WHERE nbr_processid = 639                     -- current Process Id
					 AND dat_posting < '27-Jan-2006'                     --currentDate
					 AND flg_realpool = 'Y'
					 AND typ_calculation = 'TREASURY'
				GROUP BY id_pool) tmp
		 WHERE hdr.id_pool = tmp.id_pool
		   AND hdr.dat_business_poolrun = tmp.dat_value
		   AND hdr.nbr_processid = 639                           -- current Process Id
		   AND amount != 0

				--ignore
				SELECT tmp.amount, hdr.id_pool, ble.acct_bankshare, 'BANKSHARE' share_type, dat_posting, dat_post_settlement, dat_post_creditdelay, dat_post_debitdelay
				FROM OPOOL_TXNPOOLHDR hdr,OPOOL_BLELINK ble,
														(select id_pool, max(dat_business_poolrun) dat_value, sum(amt_diff) amount
														   from opool_txnpool_bvtdiff
														  where nbr_processid = 617
														    and dat_posting < '1-Feb-2006'  --currentDate
														    and amt_diff != 0
														    and flg_realpool = 'Y'
															and typ_calculation = 'BANKSHARE'
															and sub_typ_calculation = 'A'	 -- Cod_Allocation_Sum
														group by id_pool
													    ) tmp
				WHERE hdr.id_pool = tmp.id_pool
				  AND hdr.dat_business_poolrun = tmp.dat_value
				  AND hdr.nbr_processid = 617  -- current Process Id
				  AND ble.cod_bnk_lgl = hdr.cod_base_bnk_lgl
				  AND ble.cod_ccy = hdr.cod_base_ccy_pool
				  AND ble.cod_gl = hdr.cod_gl
				  */
				  		sBuf.setLength(0);
						//POSTING
						//sBuf.append(" SELECT dat_value, tmp.amount SUM_AMT, hdr.id_pool, ble.acct_bankshare acct, ");
						sBuf.append(" SELECT dat_value, tmp.dr_sum, tmp.cr_sum, hdr.id_pool, ble.acct_bankshare acct_dr, ble.acct_bankshare acct_cr,");//DrCrAllocation
						sBuf.append(" 'BANKSHARE' share_type, dat_posting, dat_post_settlement, ");
						sBuf.append(" dat_post_creditdelay, dat_post_debitdelay, ");
						sBuf.append(" (SELECT dat_post_debitdelay ");
						sBuf.append(" FROM opool_txnpoolhdr h1 ");
						sBuf.append(" WHERE h1.id_pool = hdr.id_pool ");
						sBuf.append(" AND h1.nbr_processid = hdr.nbr_processid ");
						//sBuf.append(" AND h1.dat_business_poolrun = dat_value + 1) next_debit_delay, ");
						sBuf.append(" AND h1.dat_business_poolrun = hdr.dat_posting + 1) next_debit_delay, ");
						sBuf.append(" (SELECT dat_post_creditdelay ");
						sBuf.append(" FROM opool_txnpoolhdr h2 ");
						sBuf.append(" WHERE h2.id_pool = hdr.id_pool ");
						sBuf.append(" AND h2.nbr_processid = hdr.nbr_processid ");
						//sBuf.append(" AND h2.dat_business_poolrun = dat_value + 1) next_credit_delay ");
						//sBuf.append(" AND h2.dat_business_poolrun = hdr.dat_posting + 1) next_credit_delay ");
						sBuf.append(" AND h2.dat_business_poolrun = hdr.dat_posting + 1) next_credit_delay, hdr.cod_gl ");//CAL fix
						sBuf.append(" FROM opool_txnpoolhdr hdr, ");
						sBuf.append(" opool_blelink ble, ");
						sBuf.append(" (SELECT id_pool, MAX (dat_business_poolrun) dat_value, ");
						//sBuf.append(" SUM (amt_diff) amount ");
						sBuf.append(" SUM (amt_dr_diff) dr_sum, SUM (amt_cr_diff) cr_sum ");//DrCrAllocation
						
						//BVTAccrualBasedOnPosting Starts
						//sBuf.append(" FROM opool_txnpool_bvtdiff ");
						sBuf.append(" FROM opool_txnpool_bvtdiff diff");
						//BVTAccrualBasedOnPosting Ends

						/** ****** JTEST FIX start ************  on 14DEC09*/
//						sBuf.append(" WHERE nbr_processid = "+processId);//-- current Process Id
						sBuf.append(" WHERE nbr_processid = ? ");//-- current Process Id
						/** ****** JTEST FIX end **************  on 14DEC09*/
						
						//BVTAccrualBasedOnPosting Starts
						//sBuf.append(" AND dat_posting < TO_DATE('"+strbusinessdate+"','" + dateFormat + "') ");//--currentDate
						sBuf.append(" AND dat_accrual <= ");
						sBuf.append(" (SELECT MAX(dat_posting) ");
						sBuf.append(" FROM opool_txnpool_bvtdiff d ");
						sBuf.append(" WHERE d.NBR_PROCESSID = diff.NBR_PROCESSID ");
						sBuf.append(" AND d.ID_POOL = diff.ID_POOL ");
						sBuf.append(" AND d.PARTICIPANT_ID = diff.PARTICIPANT_ID ");
						sBuf.append(" AND d.DAT_BUSINESS_POOLRUN = diff.DAT_BUSINESS_POOLRUN ");
						sBuf.append(" AND d.TYP_CALCULATION = diff.TYP_CALCULATION ");
						sBuf.append(" AND d.SUB_TYP_CALCULATION = diff.SUB_TYP_CALCULATION ");
						sBuf.append(" AND d.NBR_SEQUENCE = diff.NBR_SEQUENCE  ");												
						sBuf.append(" AND d.DAT_POSTING < TO_DATE('"+strbusinessdate+"','" + dateFormat + "') ");
						sBuf.append(" ) ");
						//BVTAccrualBasedOnPosting Ends

						sBuf.append(" AND flg_realpool = 'Y' ");
						/** ****** JTEST FIX start ************  on 14DEC09*/
//						sBuf.append(" AND typ_calculation = '"+ExecutionConstants.BANKSHARE+"' ");
						sBuf.append(" AND typ_calculation = ? ");
						/** ****** JTEST FIX end **************  on 14DEC09*/
						sBuf.append(" AND sub_typ_calculation = 'A' ");//-- Cod_Allocation_Sum of poolhdr
						sBuf.append(" GROUP BY id_pool) tmp ");
						sBuf.append(" WHERE hdr.id_pool = tmp.id_pool ");
						sBuf.append(" AND hdr.dat_business_poolrun = tmp.dat_value ");
						/** ****** JTEST FIX start ************  on 14DEC09*/
//						sBuf.append(" AND hdr.nbr_processid = "+processId);//-- current Process Id
						sBuf.append(" AND hdr.nbr_processid = ?");//-- current Process Id
						/** ****** JTEST FIX end **************  on 14DEC09*/
						//sBuf.append(" AND amount != 0 ");
						sBuf.append(" AND (dr_sum != 0 OR cr_sum != 0) ");//DrCrAllocation
						sBuf.append(" AND ble.cod_bnk_lgl = hdr.cod_base_bnk_lgl ");
						sBuf.append(" AND ble.cod_ccy = hdr.cod_base_ccy_pool ");
						sBuf.append(" AND ble.cod_gl = hdr.cod_gl ");
						sBuf.append(" UNION ");
						//DrCrAllocation Starts here
						//sBuf.append(" SELECT dat_value, tmp.amount SUM_AMT, hdr.id_pool, ");
						sBuf.append(" SELECT dat_value, tmp.dr_sum, tmp.cr_sum, hdr.id_pool, ");
						//sBuf.append(" (CASE ");
						//sBuf.append(" WHEN amount >= 0 ");
						//sBuf.append(" THEN acct_treasury_cr ");
						//sBuf.append(" ELSE acct_treasury_dr ");
						//sBuf.append(" END) acct, 'TREASURYSHARE' share_type, dat_posting, ");
						sBuf.append(" acct_treasury_dr acct_dr, acct_treasury_cr acct_cr, 'TREASURYSHARE' share_type, dat_posting, ");
						//DrCrAllocation ends
						sBuf.append(" dat_post_settlement, dat_post_creditdelay, dat_post_debitdelay, ");
						sBuf.append(" (SELECT dat_post_debitdelay ");
						sBuf.append(" FROM opool_txnpoolhdr h1 ");
						sBuf.append(" WHERE h1.id_pool = hdr.id_pool ");
						sBuf.append(" AND h1.nbr_processid = hdr.nbr_processid ");
						//sBuf.append(" AND h1.dat_business_poolrun = dat_value + 1) next_debit_delay, ");
						sBuf.append(" AND h1.dat_business_poolrun = hdr.dat_posting + 1) next_debit_delay, ");
						sBuf.append(" (SELECT dat_post_creditdelay ");
						sBuf.append(" FROM opool_txnpoolhdr h2 ");
						sBuf.append(" WHERE h2.id_pool = hdr.id_pool ");
						sBuf.append(" AND h2.nbr_processid = hdr.nbr_processid ");
						//sBuf.append(" AND h2.dat_business_poolrun = dat_value + 1) next_credit_delay ");
						//sBuf.append(" AND h2.dat_business_poolrun = hdr.dat_posting + 1) next_credit_delay ");
						sBuf.append(" AND h2.dat_business_poolrun = hdr.dat_posting + 1) next_credit_delay, hdr.cod_gl ");//CAL fix
						sBuf.append(" FROM opool_txnpoolhdr hdr, ");
						sBuf.append(" (SELECT id_pool, MAX (dat_business_poolrun) dat_value, ");
						//sBuf.append(" SUM (amt_diff) amount ");
						sBuf.append(" SUM (amt_dr_diff) dr_sum, SUM (amt_cr_diff) cr_sum ");//DrCrAllocation

						//BVTAccrualBasedOnPosting Starts
						sBuf.append(" FROM opool_txnpool_bvtdiff diff");
						//BVTAccrualBasedOnPosting Ends

						/** ****** JTEST FIX start ************  on 14DEC09*/
//						sBuf.append(" WHERE nbr_processid = "+processId);//-- current Process Id
						sBuf.append(" WHERE nbr_processid = ? ");//-- current Process Id
						/** ****** JTEST FIX end **************  on 14DEC09*/
						
						//BVTAccrualBasedOnPosting Starts
						//sBuf.append(" AND dat_posting < TO_DATE('"+strbusinessdate+"','" + dateFormat + "') ");// --currentDate
						sBuf.append(" AND dat_accrual <= ");
						sBuf.append(" (SELECT MAX(dat_posting) ");
						sBuf.append(" FROM opool_txnpool_bvtdiff d ");
						sBuf.append(" WHERE d.NBR_PROCESSID = diff.NBR_PROCESSID ");
						sBuf.append(" AND d.ID_POOL = diff.ID_POOL ");
						sBuf.append(" AND d.PARTICIPANT_ID = diff.PARTICIPANT_ID ");
						sBuf.append(" AND d.DAT_BUSINESS_POOLRUN = diff.DAT_BUSINESS_POOLRUN ");
						sBuf.append(" AND d.TYP_CALCULATION = diff.TYP_CALCULATION ");
						sBuf.append(" AND d.SUB_TYP_CALCULATION = diff.SUB_TYP_CALCULATION ");
						sBuf.append(" AND d.NBR_SEQUENCE = diff.NBR_SEQUENCE ");
						sBuf.append(" AND d.DAT_POSTING < TO_DATE('"+strbusinessdate+"','" + dateFormat + "') ");
						sBuf.append(" ) ");
						//BVTAccrualBasedOnPosting Ends

						sBuf.append(" AND flg_realpool = 'Y' ");
						/** ****** JTEST FIX start ************  on 14DEC09*/
//						sBuf.append(" AND typ_calculation = '"+ExecutionConstants.TREASURY+"' ");
						sBuf.append(" AND typ_calculation = ? ");
						/** ****** JTEST FIX end **************  on 14DEC09*/
						sBuf.append(" GROUP BY id_pool) tmp ");
						sBuf.append(" WHERE hdr.id_pool = tmp.id_pool ");
						sBuf.append(" AND hdr.dat_business_poolrun = tmp.dat_value ");
						/** ****** JTEST FIX start ************  on 14DEC09*/
//						sBuf.append(" AND hdr.nbr_processid = "+processId);//-- current Process Id
						sBuf.append(" AND hdr.nbr_processid = ?");//-- current Process Id
						/** ****** JTEST FIX end **************  on 14DEC09*/
						//sBuf.append(" AND amount != 0 ");
						sBuf.append(" AND (dr_sum != 0 OR cr_sum != 0) ");//DrCrAllocation
						//ENH_10007 Starts
						//BVT Adjsutmet Accounting Entries should be generated per host.
						/** ****** JTEST FIX start ************  on 14DEC09*/
//						sBuf.append(" AND hdr.cod_gl = '"+hostId+"'");
						sBuf.append(" AND hdr.cod_gl = ?");
						/** ****** JTEST FIX end **************  on 14DEC09*/
						//ENH_10007 Ends
						if (logger.isDebugEnabled()){
							logger.debug("Executing Query Bank Share Treasury Posting" + sBuf.toString());
						}

						conn = LMSConnectionUtility.getConnection();
						/** ****** JTEST FIX start ************  on 14DEC09*/
//						stmt = conn.createStatement();
//						//rs = stmt.executeQuery(queryPosting.toString());
//						rs = stmt.executeQuery(sBuf.toString());
						LMSConnectionUtility.close(pStmt); //15.1.1_Resource_Leaks_Fix
						pStmt = conn.prepareStatement(sBuf.toString());
						pStmt.setLong(1, processId);
						pStmt.setString(2, ExecutionConstants.BANKSHARE);
						pStmt.setLong(3, processId);
						pStmt.setLong(4, processId);
						pStmt.setString(5, ExecutionConstants.TREASURY);
						pStmt.setLong(6, processId);
						pStmt.setString(7, hostId);
						rs = pStmt.executeQuery();
						/** ****** JTEST FIX end **************  on 14DEC09*/

						if (logger.isDebugEnabled()) {
							logger.debug(" Executed Query BANKSHARE POSTING");
						}
						hmFields = new HashMap();
						hmFields.put(ExecutionConstants.PROCESSID, processId); // Mashreq_AE_Hook fix
						hmFields.put(ExecutionConstants.BUSINESSDATE, businessDate); // Mashreq_AE_Hook fix
						poolId = "";
						//Date postingDate = null;
						results = false;

						while(rs.next()){
							results = true;
							acctDoObj = new AccountingDO();
							accountingEntryId = "0";
							//DrCrAllocation starts
							//amountToPost = rs.getBigDecimal("SUM_AMT");
							amtToPostCredit = rs.getBigDecimal("cr_sum");
							amtToPostDebit = rs.getBigDecimal("dr_sum");
							//DrCrAllocation ends
							shareType = rs.getString("SHARE_TYPE");
							//DrCrAllocation Starts
							//acctShare = rs.getString("ACCT");
							acctShareCr = rs.getString("ACCT_CR");
							acctShareDr = rs.getString("ACCT_DR");
							flgSeperate = true;
							//DrCrAllocation Ends
							poolId = rs.getString("ID_POOL");
							//INT_1055 Starts
							hmFields.put("DAT_POST_SETTLEMENT",(Date)rs.getDate("DAT_POST_SETTLEMENT"));
							hmFields.put("DAT_POST_CREDITDELAY",(Date)rs.getDate("DAT_POST_CREDITDELAY"));
							hmFields.put("DAT_POST_DEBITDELAY",(Date)rs.getDate("DAT_POST_DEBITDELAY"));
							hmFields.put("NEXT_CREDIT_DELAY",rs.getDate("NEXT_CREDIT_DELAY"));
							hmFields.put("NEXT_DEBIT_DELAY",rs.getDate("NEXT_DEBIT_DELAY"));
							hmFields.put("DAT_VALUE",rs.getDate("DAT_POSTING"));
							postSettlementDate = (Date)hmFields.get("DAT_POST_SETTLEMENT");
							//INT_1055 Ends
							//CAL fix starts
							java.util.Date dbNextCreditDelay = rs.getDate("NEXT_CREDIT_DELAY");
							java.util.Date dbNextDebitDelay = rs.getDate("NEXT_DEBIT_DELAY");
							String codGL = rs.getString("COD_GL");
							java.util.Date[] calcPostingDates = null;
							//SUP_1082c starts
//							if(dbNextCreditDelay == null || dbNextDebitDelay == null){
//							    calcPostingDates = calculateNextCyclePostingDates(conn, poolId, codGL, nbrProcessId, businessdate);
//							}
							if(postCache == null || postCache.get(poolId) == null){
							    if(logger.isWarnEnabled()){
							        logger.warn("Error forming the posting cache OR poolId ["+poolId+"] postingDates not present in the cache.");
							        logger.warn("So getting the dates from database for "+poolId+".");
							    }
								calcPostingDates = calculateNextCyclePostingDates(conn, poolId, codGL, processId, businessDate);
							}
							else{
							    calcPostingDates = (Date[])postCache.get(poolId);
							}
							//SUP_1082c ends
							//CAL fix ends
							//DrCrAllocation starts
							if(flgSeperate){
								//CREDIT ALLOCATION
								if (amtToPostCredit.compareTo(zero) != 0){
									if (postSettlementDate.compareTo(businessDate) < 0){
								        //SUP_1082c starts
								        hmFields.put("DAT_VALUE",calcPostingDates[0]);
//										actualPostingDate = (Date)hmFields.get("NEXT_CREDIT_DELAY");
//										if(actualPostingDate == null){
//											actualPostingDate = calcPostingDates[3];
//											if(logger.isDebugEnabled()){
//												logger.debug(" calculated next_credit_delay is "+ actualPostingDate);
//											}
//										}
											actualPostingDate = calcPostingDates[3];
								        //SUP_1082c ends
									}
									else{
										actualPostingDate = (Date)hmFields.get("DAT_POST_CREDITDELAY");
									}
									
									hmFields.put("DAT_POSTING",actualPostingDate);
									valueDate = (Date)hmFields.get("DAT_VALUE");
									hmFields.put("DAT_SENT", addOffsetToDate(valueDate, 1));
									/** ****** JTEST FIX start ************  on 14DEC09*/
//									acctDoObj = getAccount(acctShareCr);
									acctDoObj = getAccount(acctShareCr, conn);
									/** ****** JTEST FIX end **************  on 14DEC09*/
									
									//SUP_1082b starts
									hmAccrualFields = new HashMap(hmFields);
									if (accCache == null || accCache.get(poolId)== null) {
									    throw new LMSException("101302"," Error occured in forming the cache ");
						            }
									Date[] accDates = (Date[])accCache.get(poolId);
//									if(accDates[0] == null || accDates[0].compareTo(currentDate)<0){
//									    hmAccrualFields.put("DAT_SENT", accDates[2]);
//									    hmAccrualFields.put("DAT_POSTING",accDates[3]);
//									}
//									else {
									//Always consider the next cycle accrual dates
									    hmAccrualFields.put("DAT_SENT", accDates[0]);
									    hmAccrualFields.put("DAT_POSTING",accDates[1]);
//					                }								
									//SUP_1082b ends
									
									//SUP_1082b starts
									accountingEntryId = "" +processBVTEntry(acctDoObj,
											amtToPostCredit,ExecutionConstants.ACCRUAL,ExecutionConstants.BVT_CREDIT,
											//shareType,pstmt,pstmtAcctUnpostedInsert,poolId,hmFields);
											
											//SUP_1082b starts
											//shareType,pstmt,pstmtAcctUnpostedInsert,poolId,hmFields);
											/** ****** JTEST FIX start ************  on 14DEC09*/
//											shareType,pstmt,pstmtAcctUnpostedInsert,poolId,hmAccrualFields);
											shareType,pstmt,pstmtAcctUnpostedInsert,poolId,hmAccrualFields, conn);
											/** ****** JTEST FIX end **************  on 14DEC09*/
											//SUP_1082b ends
									
									//accountingEntryId =  ""+processBVTEntry(acctDoObj,
		                            accountingEntryId = accountingEntryId+"|"+processBVTEntry(acctDoObj,
									//SUP_1082b ends
											amtToPostCredit,type,ExecutionConstants.BVT_CREDIT,
											/** ****** JTEST FIX start ************  on 14DEC09*/
//											shareType,pstmt,pstmtAcctUnpostedInsert,poolId,hmFields);
		                            		shareType,pstmt,pstmtAcctUnpostedInsert,poolId,hmFields, conn);
											/** ****** JTEST FIX end **************  on 14DEC09*/
								}
								else{
									logger.fatal("Credit B&T Allocation is 0 for"  + acctDoObj + "SO not posting the amount ");
								}
								//DEBIT ALLOCATION
								if (amtToPostDebit.compareTo(zero) != 0){
									if (postSettlementDate.compareTo(businessDate) < 0){
								        //SUP_1082c starts
								        hmFields.put("DAT_VALUE",calcPostingDates[0]);
//										actualPostingDate = (Date)hmFields.get("NEXT_DEBIT_DELAY");
//										if(actualPostingDate == null){
//											actualPostingDate = calcPostingDates[4];
//											if(logger.isDebugEnabled()){
//												logger.debug(" calculated next_debit_delay is "+ actualPostingDate);
//											}
//										}
											actualPostingDate = calcPostingDates[4];
								        //SUP_1082c ends
									}
									else{
										actualPostingDate = (Date)hmFields.get("DAT_POST_DEBITDELAY");
									}
									
									hmFields.put("DAT_POSTING",actualPostingDate);
									valueDate = (Date)hmFields.get("DAT_VALUE");
									hmFields.put("DAT_SENT", addOffsetToDate(valueDate, 1));
									/** ****** JTEST FIX start ************  on 14DEC09*/
//									acctDoObj = getAccount(acctShareDr);
									acctDoObj = getAccount(acctShareDr, conn);
									/** ****** JTEST FIX end **************  on 14DEC09*/
									
									//SUP_1082b starts
									hmAccrualFields = new HashMap(hmFields);
									if (accCache == null || accCache.get(poolId)== null) {
									    throw new LMSException("101302"," Error occured in forming the cache ");
						            }
									Date[] accDates = (Date[])accCache.get(poolId);
//									if(accDates[0] == null || accDates[0].compareTo(currentDate)<0){
//									    hmAccrualFields.put("DAT_SENT", accDates[2]);
//									    hmAccrualFields.put("DAT_POSTING",accDates[3]);
//									}
//									else {
									//Always consider the next cycle accrual dates
									    hmAccrualFields.put("DAT_SENT", accDates[0]);
									    hmAccrualFields.put("DAT_POSTING",accDates[1]);
//					                }								
									//SUP_1082b ends
									
									//SUP_1082b starts
									accountingEntryId =  "" +processBVTEntry(acctDoObj,
											amtToPostDebit,ExecutionConstants.ACCRUAL,ExecutionConstants.BVT_DEBIT,											
											//shareType,pstmt,pstmtAcctUnpostedInsert,poolId,hmFields);
											//SUP_1082b starts
											//shareType,pstmt,pstmtAcctUnpostedInsert,poolId,hmFields);
											/** ****** JTEST FIX start ************  on 14DEC09*/
//											shareType,pstmt,pstmtAcctUnpostedInsert,poolId,hmAccrualFields);
											shareType,pstmt,pstmtAcctUnpostedInsert,poolId,hmAccrualFields, conn);
											/** ****** JTEST FIX end **************  on 14DEC09*/
											//SUP_1082b ends
											
									//accountingEntryId =  ""+processBVTEntry(acctDoObj,
									accountingEntryId =  accountingEntryId+ "|" +processBVTEntry(acctDoObj,
									//SUP_1082b ends
											amtToPostDebit,type,ExecutionConstants.BVT_DEBIT,
											/** ****** JTEST FIX start ************  on 14DEC09*/
//											shareType,pstmt,pstmtAcctUnpostedInsert,poolId,hmFields);
											shareType,pstmt,pstmtAcctUnpostedInsert,poolId,hmFields, conn);
											/** ****** JTEST FIX end **************  on 14DEC09*/
								}
								else{
									logger.fatal("Debit B&T Allocation is 0 for"  + acctDoObj + "SO not posting the amount ");
								}
							}
//							if (amountToPost.compareTo(zero) != 0 && !(acctShare == null || "".equals(acctShare))){
//								if(amountToPost.compareTo(zero)>0){
//									//INT_1055 Starts
//									//postingDate = (Date)hmFields.get("DAT_POST_CREDITDELAY");
//								    if (postSettlementDate.compareTo(businessDate) < 0){
//								        actualPostingDate = (Date)hmFields.get("NEXT_CREDIT_DELAY");
//										//CAL fix starts
//								        //actualPostingDate = actualPostingDate != null ? actualPostingDate
//								        //                                              : businessDate;
//										if(actualPostingDate == null){
//											// Calculate the NEXT_CREDIT_DELAY and assign it to actualPostingDate 
//											actualPostingDate = calcPostingDates[3];
//											if(logger.isDebugEnabled()){
//												logger.debug(" calculated next_credit_delay is "+ actualPostingDate);
//											}
//										}
//										//CAL fix ends
//								    }
//								    else
//								        actualPostingDate = (Date)hmFields.get("DAT_POST_CREDITDELAY");
//								    //INT_1055 Ends
//								}else{
//									//INT 1055 Starts
//									//postingDate = (Date)hmFields.get("DAT_POST_DEBITDELAY");
//								    if (postSettlementDate.compareTo(businessDate) < 0){
//								        actualPostingDate = (Date)hmFields.get("NEXT_DEBIT_DELAY");
//								        //actualPostingDate = actualPostingDate != null ? actualPostingDate
//								        //                                              : businessDate;
//										if(actualPostingDate == null){
//											// Calculate the NEXT_DEBIT_DELAY and assign it to actualPostingDate 
//											actualPostingDate = calcPostingDates[4];
//											if(logger.isDebugEnabled()){
//												logger.debug(" calculated next_debit_delay is "+ actualPostingDate);
//											}
//										}
//									}
//									else
//								        actualPostingDate = (Date)hmFields.get("DAT_POST_DEBITDELAY");
//								    //INT_1055 Ends
//								}
//								//INT_1055 Starts
//								hmFields.put("DAT_POSTING",actualPostingDate);
//								valueDate = (Date)hmFields.get("DAT_VALUE");
//								//hmFields.put("DAT_POSTING",(Date)(postingDate!=null?postingDate:businessDate));
//								//BVT_TXN Inc Starts
//								//hmFields.put("DAT_SENT",businessDate);
//								//BVT_TXN Inc Ends
//								hmFields.put("DAT_SENT", addOffsetToDate(valueDate, 1));
//								//INT_1055 Ends
//								acctDoObj = getAccount(acctShare);
//								accountingEntryId =  ""+processBVTEntry(acctDoObj,
//									amountToPost,type,ExecutionConstants.NET,
//									shareType,pstmt,pstmtAcctUnpostedInsert,poolId,hmFields);//INT_1039
//							}else{
//								logger.fatal("Bank Share /Treasury Share Amt Posting is 0 for"  + acctDoObj + "SO not pOSTING the amount ");
//							}
							//DrCrAllocation ends
							if (logger.isDebugEnabled()) {
								logger.debug("Bank Share Posting NBR_ACCTENTRYID"+accountingEntryId);
							}

						}
						if (!results){
							logger.fatal(" No records for BankShare Trasury Share Posting" + sBuf.toString());
						}
						rs.close();
					}
			/** ****** FindBugs Fix Start ***********  on 14DEC09*/
		}catch(SQLException e){
			logger.fatal("generateAccrualEntries: ", e);
			//throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, e.getMessage());
		}catch(NamingException e){
			logger.fatal("generateAccrualEntries: ", e);
			//throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, e.getMessage());
			/** ****** FindBugs Fix End ***********  on 14DEC09*/
		}finally{
			LMSConnectionUtility.close(rs);
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			LMSConnectionUtility.close(stmt);
			LMSConnectionUtility.close(pStmt);
			/** ****** JTEST FIX end **************  on 14DEC09*/
			LMSConnectionUtility.close(conn);
		}
	}
//BVI Bank &  Treasury  Share BVI_BTS Ends
//INT_1039--Started
	/**
	 *  * @throws SQLException
		 * @throws LMSException
	 */
	private void populateUnpostedAmountsCache(long lotId) throws LMSException{
		Connection conn = null;
		PreparedStatement pstmt=null;
		String nbrAcctUnposted=null;
		String amtType=null;
		BigDecimal amtUnposted=new BigDecimal(0);
		ResultSet rs = null;

		if (logger.isDebugEnabled())
			logger.debug("Entering");

		try{

			StringBuilder sbufUnpostedAmt = new StringBuilder(" SELECT ")
						.append(" SUM(AMOUNT)SUM_AMT, NBR_OWNACCOUNT,AMOUNT_TYPE ")
						.append(" FROM OPOOL_ACCOUNT_UNPOSTED")
						.append(" WHERE USE_DATE IS NULL ")
						.append(" AND POSTING_TYPE = '"+ExecutionConstants.POSTING+"'")
						.append(" AND ID_POOL IN ")
						.append(" (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR ")
						.append(" WHERE NBR_LOTID= " + lotId + ")")
						//Also takes up the unposted amounts of BVT while normal execution
						.append(" GROUP BY NBR_OWNACCOUNT,AMOUNT_TYPE ");

			if (logger.isDebugEnabled()) {
				logger.debug("Amount Unposted Query  =  "+ sbufUnpostedAmt.toString());
			}

			//Preparing the Statements
			conn = LMSConnectionUtility.getConnection();
			pstmt = conn.prepareStatement(sbufUnpostedAmt.toString());

			rs=pstmt.executeQuery();
			//Populating Unposted Amount into the HashMaps
			while (rs.next()){
				nbrAcctUnposted = rs.getString("NBR_OWNACCOUNT");
				amtUnposted = rs.getBigDecimal("SUM_AMT");
				amtType=rs.getString("AMOUNT_TYPE");
				//Add this in the HashMap for AmountUnposted

				if(amtType.equalsIgnoreCase(ExecutionConstants.CREDIT_INTEREST)){
					_hmCR_INT_UNPOSTEDMap.put(nbrAcctUnposted,amtUnposted);
					if (logger.isDebugEnabled()) {
						logger.debug("Amount Unposted Details: AMOUNT = "+ amtUnposted +
								",Account = "+ nbrAcctUnposted +" for CREDIT_INTEREST");
					}
				}
				else if(amtType.equalsIgnoreCase(ExecutionConstants.DEBIT_INTEREST)){
					_hmDR_INT_UNPOSTEDMap.put(nbrAcctUnposted,amtUnposted);
					if (logger.isDebugEnabled()) {
						logger.debug("Amount Unposted Details: AMOUNT = "+ amtUnposted +
								",Account = "+ nbrAcctUnposted +" for DEBIT_INTEREST");
					}
				}
				else if(amtType.equalsIgnoreCase(ExecutionConstants.NET_INTEREST)){
					_hmNET_INT_UNPOSTEDMap.put(nbrAcctUnposted,amtUnposted);
					if (logger.isDebugEnabled()) {
						logger.debug("Amount Unposted Details: AMOUNT = "+ amtUnposted +
								",Account = "+ nbrAcctUnposted +" for NET_INTEREST");
					}
				}
				else if(amtType.equalsIgnoreCase(ExecutionConstants.CENTRAL_INTEREST)){
					_hmCENTRAL_INT_UNPOSTEDMap.put(nbrAcctUnposted,amtUnposted);
					if (logger.isDebugEnabled()) {
						logger.debug("Amount Unposted Details: AMOUNT = "+ amtUnposted +
								",Account = "+ nbrAcctUnposted+" for CENTRAL_INTEREST" );
					}
				}
//SUP_1031--Starts
				else if(amtType.equalsIgnoreCase(ExecutionConstants.ALLOCATIONCR)){
					_hmALLOCATIONCR_UNPOSTEDMap.put(nbrAcctUnposted,amtUnposted);
					if (logger.isDebugEnabled()) {
						logger.debug("Amount Unposted Details: AMOUNT = "+ amtUnposted +
								",Account = "+ nbrAcctUnposted+" for ALLOCATION" );
					}
				}
				else if(amtType.equalsIgnoreCase(ExecutionConstants.ALLOCATIONDR)){
					_hmALLOCATIONDR_UNPOSTEDMap.put(nbrAcctUnposted,amtUnposted);
//SUP_1031--Ends
					if (logger.isDebugEnabled()) {
						logger.debug("Amount Unposted Details: AMOUNT = "+ amtUnposted +
								",Account = "+ nbrAcctUnposted+" for ALLOCATION" );
					}
				}
				else if(amtType.equalsIgnoreCase(ExecutionConstants.REALLOCATION)){
					_hmREALLOCATION_UNPOSTEDMap.put(nbrAcctUnposted,amtUnposted);
					if (logger.isDebugEnabled()) {
						logger.debug("Amount Unposted Details: AMOUNT = "+ amtUnposted +
								",Account = "+ nbrAcctUnposted+" for REALLOCATION" );
					}
				}

			}//While

			/** ****** FindBugs Fix Start ***********  on 14DEC09*/
		}catch(SQLException e){
			logger.fatal(
				  "Exception setting values into OPOOL_ACCOUNT_UNPOSTED ", e);
			  throw new LMSException("101302", e.toString());
		}catch(NamingException e){
			logger.fatal(
				  "Exception setting values into OPOOL_ACCOUNT_UNPOSTED ", e);
			  throw new LMSException("101302", e.toString());
			/** ****** FindBugs Fix End ***********  on 14DEC09*/
		}finally{
			LMSConnectionUtility.close(rs);
			LMSConnectionUtility.close(pstmt);
			LMSConnectionUtility.close(conn);
		}//CLOSE CONNECTIN IN FINALLY

	}
//	INT_1039--Ends
	//SUP_1031--Starts
	/**
	 *  * @throws SQLException
		 * @throws LMSException
	 */
	private void populateUnaccruedAmountsCache(long lotId) throws LMSException{
		Connection conn = null;
		PreparedStatement pstmt=null;
		String nbrAcctUnaccrued=null;
		String amtType=null;
		BigDecimal amtUnaccrued=new BigDecimal(0);
		ResultSet rs = null;

		if (logger.isDebugEnabled())
			logger.debug("Entering");

		try{

			StringBuilder sbufUnpostedAmt = new StringBuilder(" SELECT ")
						.append(" SUM(AMOUNT)SUM_AMT, NBR_OWNACCOUNT,AMOUNT_TYPE ")
						.append(" FROM OPOOL_ACCOUNT_UNPOSTED")
						.append(" WHERE USE_DATE IS NULL ")
						.append(" AND POSTING_TYPE = '"+ExecutionConstants.ACCRUAL+"'")
						.append(" AND ID_POOL IN ")
						.append(" (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR ")
						.append(" WHERE NBR_LOTID= " + lotId + ")")
						//Also takes up the unposted amounts of BVT while normal execution
						.append(" GROUP BY NBR_OWNACCOUNT,AMOUNT_TYPE ");

			if (logger.isDebugEnabled()) {
				logger.debug("Amount Unaccrued Query  =  "+ sbufUnpostedAmt.toString());
			}

			//Preparing the Statements
			conn = LMSConnectionUtility.getConnection();
			pstmt = conn.prepareStatement(sbufUnpostedAmt.toString());

			rs=pstmt.executeQuery();
			//Populating Unposted Amount into the HashMaps
			while (rs.next()){
				nbrAcctUnaccrued = rs.getString("NBR_OWNACCOUNT");
				amtUnaccrued = rs.getBigDecimal("SUM_AMT");
				amtType=rs.getString("AMOUNT_TYPE");
				//Add this in the HashMap for AmountUnposted

				if(amtType.equalsIgnoreCase(ExecutionConstants.CREDIT_INTEREST)){
					_hmCR_INT_UNACCRUEDMap.put(nbrAcctUnaccrued,amtUnaccrued);
					if (logger.isDebugEnabled()) {
						logger.debug("Amount Unaccrued Details: AMOUNT = "+ amtUnaccrued +
								",Account = "+ nbrAcctUnaccrued +" for CREDIT_INTEREST");
					}
				}
				else if(amtType.equalsIgnoreCase(ExecutionConstants.DEBIT_INTEREST)){
					_hmDR_INT_UNACCRUEDMap.put(nbrAcctUnaccrued,amtUnaccrued);
					if (logger.isDebugEnabled()) {
						logger.debug("Amount Unaccrued Details: AMOUNT = "+ amtUnaccrued +
								",Account = "+ nbrAcctUnaccrued +" for DEBIT_INTEREST");
					}
				}
				else if(amtType.equalsIgnoreCase(ExecutionConstants.NET_INTEREST)){
					_hmNET_INT_UNACCRUEDMap.put(nbrAcctUnaccrued,amtUnaccrued);
					if (logger.isDebugEnabled()) {
						logger.debug("Amount Unaccrued Details: AMOUNT = "+ amtUnaccrued +
								",Account = "+ nbrAcctUnaccrued +" for NET_INTEREST");
					}
				}
//SUP_1031--Starts
				else if(amtType.equalsIgnoreCase(ExecutionConstants.ALLOCATIONCR)){
					_hmALLOCATIONCR_UNACCRUEDMap.put(nbrAcctUnaccrued,amtUnaccrued);
					if (logger.isDebugEnabled()) {
						logger.debug("Amount Unaccrued Details: AMOUNT = "+ amtUnaccrued +
								",Account = "+ nbrAcctUnaccrued+" for ALLOCATION" );
					}
				}
				else if(amtType.equalsIgnoreCase(ExecutionConstants.ALLOCATIONDR)){
					_hmALLOCATIONDR_UNACCRUEDMap.put(nbrAcctUnaccrued,amtUnaccrued);
//SUP_1031--Ends
					if (logger.isDebugEnabled()) {
						logger.debug("Amount Unaccrued Details: AMOUNT = "+ amtUnaccrued +
								",Account = "+ nbrAcctUnaccrued+" for ALLOCATION" );
					}
				}

			}//While

			/** ****** FindBugs Fix Start ***********  on 14DEC09*/
		}catch(SQLException e){
			logger.fatal(
				  "Exception setting values into OPOOL_ACCOUNT_UNPOSTED ", e);
			  throw new LMSException("101302", e.toString());
		}catch(NamingException e){
			logger.fatal(
				  "Exception setting values into OPOOL_ACCOUNT_UNPOSTED ", e);
			  throw new LMSException("101302", e.toString());
			/** ****** FindBugs Fix End ***********  on 14DEC09*/
		}finally{
			LMSConnectionUtility.close(rs);
			LMSConnectionUtility.close(pstmt);
			LMSConnectionUtility.close(conn);
		}//CLOSE CONNECTIN IN FINALLY

	}
	//SUP_1031--Ends


// INT_1055 Start
	/**
	 * This method provides date which equal to currentDate + offset.
	 *
	 * @param currentDate
	 * @param offset
	 * @return
	 */
	private Date addOffsetToDate(Date currentDate, int offset)
            throws LMSException {
        if (currentDate == null) {
            throw new LMSException("200100", "Current Business Date is NULL");
        }
        /** ****** FindBugs Fix Start ***********  on 14DEC09*/
        int milliSecPerDay = 1000 * 60 * 60 * 24;
        long MILLISECONDS_PER_DAY = 24L*3600*1000;
        long timeDiff = currentDate.getTime() + (offset * MILLISECONDS_PER_DAY);
        /** ****** FindBugs Fix End ***********  on 14DEC09*/
        Date prevDate = new Date(timeDiff);

        return prevDate;
    }
// INT_1055 End
//CAL Fix starts
	/**
	 * This method provides the nextCyclePostingDates given cod_gl, id_pool, nbr_processId
	 * If the bvt came just after the day of dat_posting, this helps in calculating the next_debit_delay
	 * and next_credit_delay.
	 *
	 * @param conn
	 * @param poolId
	 * @param codGL
	 * @param processId
     * @param businessDate
	 * @return
	 */
	//private Date[] calculateNextCyclePostingDates(Connection conn, String poolId, String codGL, long processId, Date businessDate)
	public Date[] calculateNextCyclePostingDates(Connection conn, String poolId, String codGL, long processId, Date businessDate)
            throws LMSException {
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		Statement stmt = null;
		PreparedStatement pStmt = null;
		/** ****** JTEST FIX end **************  on 14DEC09*/
		ResultSet rSet = null;
		StringBuilder sBuf = new StringBuilder();
		String callMode = "E";
		//Do Sanity checks for conn==null, poolId==null, codGL==null, processId=0, businessDate==null
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		SimpleDateFormat formatter = new SimpleDateFormat(LMSConstants.DATE_FORMAT);
		SimpleDateFormat formatter = new SimpleDateFormat(LMSConstants.DATE_FORMAT, Locale.US);
		/** ****** JTEST FIX end **************  on 14DEC09*/
		/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//		String bDate = formatter.format(businessDate);
		/** ****** FindBugs Fix End ***********  on 14DEC09*/
		java.util.Date[] postingDates = null;

		try{	
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			stmt = conn.createStatement();
			/** ****** JTEST FIX end **************  on 14DEC09*/
			sBuf.append(" SELECT DISTINCT (SELECT cod_calendar ");
			
			//ENH_LIQ_0005 Starts
			//sBuf.append(" FROM orbicash_glmst ");
			sBuf.append(" FROM olm_glmst ");
			//ENH_LIQ_0005 Ends
			
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" WHERE cod_gl = '"+codGL+"') calendar, dat_start, cod_post_cycle, ");
			
			sBuf.append(" WHERE cod_gl = ?) calendar, dat_start, cod_post_cycle, ");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			//sBuf.append(" dat_lastposting, dat_lastpostingcalc ");
			sBuf.append(" dat_posting, dat_adjustedposting ");
			sBuf.append(" FROM opool_txnpoolhdr ");
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" WHERE cod_gl = '"+codGL+"' AND nbr_processid = "+processId);
//			sBuf.append(" AND id_pool = '"+poolId+"' ");
			
			sBuf.append(" WHERE cod_gl = ? AND nbr_processid = ?");
			sBuf.append(" AND id_pool = ? ");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			sBuf.append(" ORDER BY DAT_POSTING DESC ");
			if(logger.isDebugEnabled()){
				logger.debug(" calculateNextCyclePostingDates -> "+sBuf.toString());
			}
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			rSet = stmt.executeQuery(sBuf.toString());
			pStmt = conn.prepareStatement(sBuf.toString());
			pStmt.setString(1, codGL);
			pStmt.setString(2, codGL);
			pStmt.setLong(3, processId);
			pStmt.setString(4, poolId);
			rSet = pStmt.executeQuery();
			/** ****** JTEST FIX end **************  on 14DEC09*/
			if(rSet!= null && rSet.next()){
				String calendarCode = rSet.getString(1);
				Date poolStartDate = rSet.getDate(2);
				String frequency = rSet.getString(3);
				Date lastPostingDate = rSet.getDate(4);
				Date lastCalcPostingDate = rSet.getDate(5);
				//TODO Invoke executionUtility method
//				if (lastPostingDate == null || lastCalcPostingDate == null) {
//					lastPostingDate = poolStartDate;
//					lastCalcPostingDate = poolStartDate;
//					callMode = "M";
//				} 
//				else{
					callMode = "E";
//				}
				// validateInputParameters is called in ExecutionUtility's getNextPostingCycleDate
				postingDates = ExecutionUtility.getNextPostingCycleDate(conn,frequency,lastPostingDate,lastCalcPostingDate,calendarCode,businessDate,callMode);
//				java.util.Date actualDate_post=postingDates[0];
//				java.util.Date adjustedDate_post=postingDates[1];
//				java.util.Date settlementDate_post=postingDates[2];
//				java.util.Date crDelayDate_post=postingDates[3];
//				java.util.Date drDelayDate_post=postingDates[4];
			}
			else{
				throw new LMSException("2001010 "," Unable to calculateNextCyclePostingDates ");
			}
		}
		catch(Exception e){
			logger.fatal("Exception in calculateNextCyclePostingDates ", e);
			  throw new LMSException("101302", e.toString());
		}
		finally{
			LMSConnectionUtility.close(rSet);
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			LMSConnectionUtility.close(stmt);
			LMSConnectionUtility.close(pStmt);
			/** ****** JTEST FIX end **************  on 14DEC09*/
		}
		return postingDates;
    }
//CAL fix ends

//SUP_1078--Starts
	public boolean[] generateAccrualEntriesForAccountClosure(PreparedStatement pstmtAccountingEntry,PreparedStatement pstmtTxnPoolPartyDtls
			,PreparedStatement pstmtAccountUnposted,PreparedStatement pstmtAcctUnpostedInsert, 
			java.util.Date businessdate,String participantId )
			throws LMSException{
		
		String sqlString;
		String sqlGroupAccntsSuddenClose;
		String sqlSelectAccntsSuddenClose;
		StringBuilder sbufWhereAccntsSuddenClose = new StringBuilder("");
		String accountingEntryId = null;
		String poolId = null;

		ResultSet rs=null;
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		Statement stmt=null;
		PreparedStatement pStmt=null;
		/** ****** JTEST FIX end **************  on 14DEC09*/
		Connection conn = null;
		AccountingDO acctDoObj = null;
		boolean flgSeperate = true;
		BigDecimal amtToPostCredit = null;
		BigDecimal amountToPostDebit= null;
		BigDecimal amountToPost = null;
		BigDecimal bd_amtNotPosted=new BigDecimal(0);

		//SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		//String strbusinessdate = formatter.format(businessdate);

		if (logger.isDebugEnabled())
			logger.debug("Entering");

		//Participants which have been closed
		sqlSelectAccntsSuddenClose= (new StringBuffer()
				//.append(" SELECT PARTY.ID_POOL,PARTY.PARTICIPANT_ID,COD_PARTY_CCY,")
				.append(" SELECT PARTY.ID_POOL,PARTY.PARTICIPANT_ID,COD_PARTY_CCY,TXN.NBR_PROCESSID, ") // Mashreq_AE_Hook
				//FOR CREDIT INTEREST CONSIDER TXN.TYP_ACCTINT_CRACT , if this is POST OR IF IT IS APPLY IF POOL POSITION IS CR SUPPCR
				.append(" SUM((CASE WHEN (  TXN.TYP_ACCTINT_DRACT = 'POST' OR ")
				.append(" (TXN.AMT_POOL_INTCALC_BALANCE < 0 AND TXN.TYP_ACCTINT_DRACT = 'SUPPDR') OR ")
				.append(" (TXN.AMT_POOL_INTCALC_BALANCE >= 0 AND TXN.TYP_ACCTINT_DRACT = 'SUPPCR')) ")
				.append(" THEN PARTY.AMT_DR_INTEREST ELSE 0 END )) SUM_DR_INT, ")
				.append(" SUM((CASE WHEN (  TXN.TYP_ACCTINT_CRACT = 'POST' OR ")
				.append(" (TXN.AMT_POOL_INTCALC_BALANCE < 0 AND TXN.TYP_ACCTINT_CRACT = 'SUPPDR') OR ")
				.append(" (TXN.AMT_POOL_INTCALC_BALANCE >= 0 AND    TXN.TYP_ACCTINT_CRACT = 'SUPPCR')) ")
				.append(" THEN PARTY.AMT_CR_INTEREST ELSE 0 END )) SUM_CR_INT,")
				.append(" NBR_COREACCOUNT, COD_BRANCH,PARTY.COD_GL,PARTY.FLG_DEF_DRCR_CONSOL CONSOLIDATE,COD_BNK_LGL, NBR_IBANACCNT ")
				.append(",(PRCIPANT.DAT_END + 1) DAT_ADJUSTEDACCRUAL,(PRCIPANT.DAT_END + 1) DAT_ACCRUAL ")
				.append(" FROM OPOOL_TXNPOOL_PARTYDTLS PARTY,OPOOL_TXNPOOLHDR TXN, OPOOL_POOLPARTICIPANT PRCIPANT ")).toString();

		/** ****** JTEST FIX start ************  on 14DEC09*/
//		sbufWhereAccntsSuddenClose.append(" WHERE PARTY.PARTICIPANT_ID='"+participantId+"' AND PRCIPANT.PARTICIPANT_ID='"+participantId+"' AND PARTY.ID_POOL = TXN.ID_POOL AND PRCIPANT.ID_POOL = TXN.ID_POOL ")
		sbufWhereAccntsSuddenClose.append(" WHERE PARTY.PARTICIPANT_ID= ? AND PRCIPANT.PARTICIPANT_ID= ? AND PARTY.ID_POOL = TXN.ID_POOL AND PRCIPANT.ID_POOL = TXN.ID_POOL ")
		/** ****** JTEST FIX end **************  on 14DEC09*/
		.append(" AND PRCIPANT.FLG_ACCOUNTCLS = 'Y' ")
		.append(" AND TXN.NBR_PROCESSID = PARTY.NBR_PROCESSID AND TXN.DAT_BUSINESS_POOLRUN = PARTY.DAT_BUSINESS_POOLRUN")
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		.append(" AND TXN.TXT_STATUS ='" + ExecutionConstants.ALLOCATED + "'")
		.append(" AND TXN.TXT_STATUS = ? ")
		/** ****** JTEST FIX end **************  on 14DEC09*/
		.append(" AND (TYP_POSTING  = 'R' )")//COR TYP_POSTING  = 'I' donotINSERT INTO ACCOUNTINGENTRY TABLE
		.append(" AND TXN.COD_COUNTER_ACCOUNT = 'B' ") //DO ACCRUAL ONLY FOR BANK ACCOUNT
		.append(" AND PARTY.TYP_PARTICIPANT = 'ACCOUNT' AND TXN.FLG_REALPOOL = 'Y' ")
		.append(" AND PARTY.ID_BVT = (SELECT MAX(TO_NUMBER(M.ID_BVT)) FROM OPOOL_TXNPOOLHDR M ")
		.append(" WHERE M.ID_POOL = TXN.ID_POOL AND M.DAT_BUSINESS_POOLRUN = TXN.DAT_BUSINESS_POOLRUN ")
		.append(" AND M.COD_GL = TXN.COD_GL)")//CONSIDERING THE RECORDS FOR THE LATEST BVT
		.append(" AND PARTY.DAT_BUSINESS_POOLRUN BETWEEN  NVL(TXN.DAT_LASTACCRUALCALC,TXN.DAT_START-1)+1 AND TXN.DAT_ACCRUAL  ");//SUP_1062
/*		
		if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
			sbufWhereAccntsSuddenClose.append(" AND TXN.ID_POOL IN (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR LOT ");
			sbufWhereAccntsSuddenClose.append(" WHERE LOT.NBR_LOTID =" + generateForId+")");
		}
*/
		//sqlGroupAccntsSuddenClose = " GROUP BY PARTY.ID_POOL, PARTY.PARTICIPANT_ID,COD_PARTY_CCY,NBR_COREACCOUNT,COD_BRANCH,PARTY.COD_GL,FLG_DEF_DRCR_CONSOL,COD_BNK_LGL,NBR_IBANACCNT,PRCIPANT.DAT_END";
		sqlGroupAccntsSuddenClose = " GROUP BY PARTY.ID_POOL, PARTY.PARTICIPANT_ID,COD_PARTY_CCY,NBR_COREACCOUNT,COD_BRANCH,PARTY.COD_GL,FLG_DEF_DRCR_CONSOL,COD_BNK_LGL,NBR_IBANACCNT,PRCIPANT.DAT_END,TXN.NBR_PROCESSID"; // Mashreq_AE_Hook
		
		//Combine the subParts of the query
		sqlString = sqlSelectAccntsSuddenClose+sbufWhereAccntsSuddenClose.toString()+sqlGroupAccntsSuddenClose;//INT_1017

		if (logger.isDebugEnabled())
			logger.debug("Query ACCRUAL PARTICIPANT POSTING" + sqlString);

		try{
			conn = LMSConnectionUtility.getConnection();
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			stmt = conn.createStatement();
//			rs = stmt.executeQuery(sqlString);
			
			pStmt = conn.prepareStatement(sqlString);
			pStmt.setString(1, participantId);
			pStmt.setString(2, participantId);
			pStmt.setString(3, ExecutionConstants.ALLOCATED);
			rs = pStmt.executeQuery();
			/** ****** JTEST FIX end **************  on 14DEC09*/

			if (logger.isDebugEnabled()) {
				logger.debug(" Executed Query ACCRUAL ACCOUNT CLOSURE "+sqlString);
			}
			HashMap hmFields = new HashMap();
			hmFields.put(ExecutionConstants.BUSINESSDATE,businessdate); // Mashreq_AE_Hook
			String flagAllocation = null;
			boolean results = false;
			while(rs.next()){
				results = true;
				hmFields.put("DAT_POSTING",(Date)rs.getDate("DAT_ADJUSTEDACCRUAL"));//SUP_1062
				hmFields.put("DAT_SENT",(Date)rs.getDate("DAT_ACCRUAL"));//INT_1017
				hmFields.put(ExecutionConstants.PROCESSID,rs.getLong("NBR_PROCESSID")); // Mashreq_AE_Hook 
				acctDoObj = new AccountingDO();
				/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
				//ENH_1110 Starts
				acctDoObj.setExpiringToday(true);
				//ENH_1110 Ends
				/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
				String flg_def_drcr_consol = rs.getString("CONSOLIDATE");
				flgSeperate = true;
				accountingEntryId = "0";
				poolId = "";
				amtToPostCredit = new BigDecimal(rs.getString("SUM_CR_INT"));
				amountToPostDebit = new BigDecimal(rs.getString("SUM_DR_INT"));

				poolId = rs.getString("ID_POOL");

				if (logger.isDebugEnabled()) {
					logger.debug("Resultset flg_def_drcr_consol" + flg_def_drcr_consol);
					logger.debug("Resultset POOLID" + poolId);
				}
				if("Y".equalsIgnoreCase(flg_def_drcr_consol!=null
															?flg_def_drcr_consol.trim()
															:"" )){
					//Get the NetInterest,if consolidate credit and debit interests
					amountToPost = amtToPostCredit.subtract(amountToPostDebit);
					// Set the seperate posting flag to False
					flgSeperate = false;

					if(amtToPostCredit.compareTo(zero)!=0 && amountToPostDebit.compareTo(zero)!=0){
						flgSeperate = true;
						//if both debit and credit interest are present.pass separate entries. as there will be both payables and receivables in this case
					}

				}
				if (logger.isDebugEnabled()) {
					logger.debug(" Flag Separate" + flgSeperate);
				}
				//fill in the AccountingDo object's attributes.
				acctDoObj.set_cod_bnk_lgl(rs.getString("COD_BNK_LGL"));
				acctDoObj.set_cod_branch(rs.getString("COD_BRANCH"));
				acctDoObj.set_cod_gl(rs.getString("COD_GL"));
				acctDoObj.set_cod_ccy(rs.getString("COD_PARTY_CCY"));
				acctDoObj.set_nbr_coreaccount(rs.getString("NBR_COREACCOUNT"));
				acctDoObj.set_nbr_ibanacct(rs.getString("NBR_IBANACCNT"));
				acctDoObj.set_nbr_ownaccount(rs.getString("PARTICIPANT_ID"));

				if (logger.isDebugEnabled()) {
					logger.debug("Account Object" + acctDoObj);
				}
				if(flgSeperate){
					bd_amtNotPosted = setUnpostedAmounts(ExecutionConstants.CREDIT_INTEREST,acctDoObj,
							pstmtAccountUnposted,businessdate,ExecutionConstants.ACCRUAL);

					if (logger.isDebugEnabled()) {
						logger.debug("Unaccrued Amount "  + bd_amtNotPosted +"for ExecutionConstants.CREDIT_INTEREST");
					}
					//chk if any amount is left to be posted
					if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
						amtToPostCredit = amtToPostCredit.add(bd_amtNotPosted);
						if (logger.isDebugEnabled()) {
							logger.debug("Adding Unaccrued Amount  "  + bd_amtNotPosted +"for ExecutionConstants.CREDIT_INTEREST "+amtToPostCredit);
						}
					}// UNPOSTED AMOUNT
					if (amtToPostCredit.compareTo(zero) != 0){
						accountingEntryId =  "" +processEntry(acctDoObj,
								amtToPostCredit,ExecutionConstants.ACCRUAL,
								ExecutionConstants.CREDIT, ExecutionConstants.CREDIT_INTEREST,
								/** ****** JTEST FIX start ************  on 14DEC09*/
//								pstmtAccountingEntry,pstmtAcctUnpostedInsert,poolId,hmFields);//INT_1039
								pstmtAccountingEntry,pstmtAcctUnpostedInsert,poolId,hmFields, conn);//INT_1039
								/** ****** JTEST FIX end **************  on 14DEC09*/
					}
					else{
						logger.fatal("Credit Interest is 0 for"  + acctDoObj + "SO not accruing the amount ");
					}
					bd_amtNotPosted = setUnpostedAmounts(ExecutionConstants.DEBIT_INTEREST,acctDoObj,
							pstmtAccountUnposted,businessdate,ExecutionConstants.ACCRUAL);

					if (logger.isDebugEnabled()) {
						logger.debug("Unaccrued Amount "  + bd_amtNotPosted +"for ExecutionConstants.DEBIT_INTEREST");
					}
					if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
						amountToPostDebit = amountToPostDebit.add(bd_amtNotPosted);
						if (logger.isDebugEnabled()) {
							logger.debug("Adding Unaccrued Amount  "  + bd_amtNotPosted +"for ExecutionConstants.DEBIT_INTEREST "+amountToPostDebit);
						}
					}// UNPOSTED AMOUNT
					if (amountToPostDebit.compareTo(zero) != 0){
						accountingEntryId = accountingEntryId + "|" + processEntry(acctDoObj,
							amountToPostDebit,ExecutionConstants.ACCRUAL,
							ExecutionConstants.DEBIT,ExecutionConstants.DEBIT_INTEREST,
							/** ****** JTEST FIX start ************  on 14DEC09*/
//							pstmtAccountingEntry,pstmtAcctUnpostedInsert,poolId,hmFields);//INT_1039
							pstmtAccountingEntry,pstmtAcctUnpostedInsert,poolId,hmFields, conn);//INT_1039
							/** ****** JTEST FIX end **************  on 14DEC09*/
					}
					else{
						logger.fatal("Debit Interest is 0 for"  + acctDoObj + "SO not accruing the amount ");
					}
				}
				else{
					//call processEntry for the NetAmount --consolidated
					bd_amtNotPosted = setUnpostedAmounts(ExecutionConstants.NET_INTEREST,acctDoObj,
							pstmtAccountUnposted,businessdate,ExecutionConstants.ACCRUAL);

					if (logger.isDebugEnabled()) {
						logger.debug("Unaccrued Amount "  + bd_amtNotPosted +"for ExecutionConstants.NET_INTEREST");
					}
					//chk if any amount is left to be posted
					if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
						amountToPost = amountToPost.add(bd_amtNotPosted);
						if (logger.isDebugEnabled()) {
							logger.debug("Adding Unaccrued Amount  "  + bd_amtNotPosted +"for ExecutionConstants.NET_INTEREST "+amountToPost);
						}
					}// UNPOSTED AMOUNT
					if (amountToPost.compareTo(zero) != 0){
						accountingEntryId =  ""+processEntry(acctDoObj,
							amountToPost,ExecutionConstants.ACCRUAL,ExecutionConstants.NET,
							ExecutionConstants.NET_INTEREST,pstmtAccountingEntry,
							/** ****** JTEST FIX start ************  on 14DEC09*/
//							pstmtAcctUnpostedInsert,poolId,hmFields);//INT_1039
							pstmtAcctUnpostedInsert,poolId,hmFields, conn);//INT_1039
							/** ****** JTEST FIX end **************  on 14DEC09*/
					}else{
						logger.fatal("Net Interest is 0 for"  + acctDoObj + "SO not accruing the amount ");
					}
				}

				//Add to preparedStmt for AccountingEntryId updation
				if (logger.isDebugEnabled()) {
					logger.debug("After processEntry s_accountingEntryId"  + accountingEntryId);
				}
				//Update the Prepared Statements for OPOOL_PARTYDETAILS
				updateInterest(pstmtTxnPoolPartyDtls,accountingEntryId,poolId,acctDoObj.get_nbr_ownaccount(),ExecutionConstants.ACCRUAL);

			}
			if (!results){
				logger.fatal(" No records for Accrual -participant interest" + sqlString);
			}
			//FOR ALLOCATION OF ACCOUNT CLOSURE
			//Participants which have been closed
			String sqlAllocAccrualDtlsSuddenClose=(new StringBuffer("SELECT ")
					//.append(" TXN.ID_POOL,TXN.FLG_CONSOLIDATION,PARTY.PARTICIPANT_ID, ")
					.append(" TXN.ID_POOL,TXN.FLG_CONSOLIDATION,PARTY.PARTICIPANT_ID, TXN.NBR_PROCESSID, ") // Mashreq_AE_Hook
					.append(" COD_CCY_ACCOUNT, TYP_SETTLE, ")
					.append(" SUM(EQV_AMT_ALLOCATION) SUM, ALLOCATOR, NBR_COREACCOUNT,  ")
					.append(" COD_BRANCH,PARTY.COD_GL, COD_BNK_LGL, NBR_IBANACCNT,ALLOC.FLG_ALLOCATION , ")
					.append(" (PRCIPANT.DAT_END + 1) DAT_ADJUSTEDACCRUAL,(PRCIPANT.DAT_END + 1) DAT_ACCRUAL ")
					.append(" FROM OPOOL_TXNPOOL_ALLOCATIONDTLS ALLOC, "))
					.append(" OPOOL_TXNPOOLHDR TXN, OPOOL_TXNPOOL_PARTYDTLS PARTY, OPOOL_POOLPARTICIPANT PRCIPANT ").toString();

			/** ****** JTEST FIX start ************  on 14DEC09*/
//			StringBuffer sqlAllocAccrualSuddenCloseWhere = new StringBuffer(" PARTY.PARTICIPANT_ID='"+participantId+"' AND PRCIPANT.PARTICIPANT_ID='"+participantId+"' AND TXN.ID_POOL = ALLOC.ID_POOL AND TXN.ID_POOL = PARTY.ID_POOL AND PRCIPANT.ID_POOL = TXN.ID_POOL ")
			StringBuilder sqlAllocAccrualSuddenCloseWhere = new StringBuilder(" PARTY.PARTICIPANT_ID= ? AND PRCIPANT.PARTICIPANT_ID= ? AND TXN.ID_POOL = ALLOC.ID_POOL AND TXN.ID_POOL = PARTY.ID_POOL AND PRCIPANT.ID_POOL = TXN.ID_POOL ")
			/** ****** JTEST FIX end **************  on 14DEC09*/
			//.append(" AND PARTY.PARTICIPANT_ID=PRCIPANT.PARTICIPANT_ID ")
			//.append(" AND PRCIPANT.DAT_END = (TO_DATE( '" + strbusinessdate + "', '" + dateFormat + "')-1 )")
			.append(" AND PRCIPANT.FLG_ACCOUNTCLS = 'Y'  ")
			.append(" AND TXN.NBR_PROCESSID = ALLOC.NBR_PROCESSID   ")
			.append(" AND TXN.DAT_BUSINESS_POOLRUN = ALLOC.DAT_BUSINESS_POOLRUN ")
			.append(" AND TXN.NBR_PROCESSID = PARTY.NBR_PROCESSID  ")
			.append(" AND TXN.DAT_BUSINESS_POOLRUN = PARTY.DAT_BUSINESS_POOLRUN ")
			.append(" AND PARTY.ID_POOL = ALLOC.ID_POOL ")
			.append(" AND PARTY.ID_BVT	 = ALLOC.ID_BVT ")
			.append(" AND PARTY.PARTICIPANT_ID = ALLOC.PARTICIPANT_ID      ")
			.append(" AND FLG_REAL = 'Y' ")
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			.append(" AND TXN.TXT_STATUS ='" + ExecutionConstants.ALLOCATED+ "'")
			.append(" AND TXN.TXT_STATUS =?")
			/** ****** JTEST FIX end **************  on 14DEC09*/
			.append(" AND FLG_ALLOCATION <> 'R' AND FLG_ALLOCATION <> 'T'   ")
			.append(" AND (ALLOC.TYP_PARTICIPANT = 'ACCOUNT' AND PARTY.TYP_PARTICIPANT = 'ACCOUNT' ) ")
			.append(" AND TXN.FLG_REALPOOL = 'Y'   ")
			.append(" AND  ALLOC.ID_BVT = (SELECT MAX(TO_NUMBER(M.ID_BVT)) ")
			.append(" FROM 	 OPOOL_TXNPOOLHDR M ")
			.append(" WHERE M.ID_POOL = TXN.ID_POOL ")
			.append(" AND M.DAT_BUSINESS_POOLRUN = TXN.DAT_BUSINESS_POOLRUN   ")
			.append(" AND M.COD_GL = TXN.COD_GL)")
			.append(" AND ALLOC.DAT_BUSINESS_POOLRUN BETWEEN  NVL(TXN.DAT_LASTACCRUALCALC,TXN.DAT_START-1)+1  AND TXN.DAT_ACCRUAL  ");//SUP_1062
/*			
			if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
				sqlAllocAccrualSuddenCloseWhere.append(" AND TXN.ID_POOL IN (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR LOT ");
				sqlAllocAccrualSuddenCloseWhere.append(" WHERE LOT.NBR_LOTID =" + generateForId+")");
			}
*/			
			String sqlAllocAccrualSuddenCloseGrpBy = (new StringBuffer("TXN.ID_POOL,")
					//.append(" FLG_CONSOLIDATION, PARTY.PARTICIPANT_ID, ")
					.append(" FLG_CONSOLIDATION, PARTY.PARTICIPANT_ID, TXN.NBR_PROCESSID, ") // Mashreq_AE_Hook
					.append(" COD_CCY_ACCOUNT, TYP_SETTLE, ALLOCATOR,  ")
					.append(" NBR_COREACCOUNT, COD_BRANCH, PARTY.COD_GL,")
					.append(" COD_BNK_LGL, NBR_IBANACCNT,FLG_ALLOCATION, ")
					.append(" PARTY_ALLOC_SEQUENCE  "))
					.append(",PRCIPANT.DAT_END ").toString();

			StringBuilder sqlAllocAccrualDtls = new StringBuilder("")
			.append(sqlAllocAccrualDtlsSuddenClose)
			.append(" WHERE ")
			.append(sqlAllocAccrualSuddenCloseWhere)
			.append(" GROUP BY ")
			.append(sqlAllocAccrualSuddenCloseGrpBy);

			if (logger.isDebugEnabled())
				logger.debug("Query ACCRUAL ALLOCATION FOR ACCOUNT CLOSURE : " + sqlAllocAccrualDtls);

			/** ****** JTEST FIX start ************  on 14DEC09*/
//			rs = stmt.executeQuery(sqlAllocAccrualDtls.toString());
			//15.1.1_Resource_Leaks_Fix start
			LMSConnectionUtility.close(rs);
			LMSConnectionUtility.close(pStmt);			
			//15.1.1_Resource_Leaks_Fix end
			pStmt = conn.prepareStatement(sqlAllocAccrualDtls.toString());
			pStmt.setString(1, participantId);
			pStmt.setString(2, participantId);
			pStmt.setString(3, ExecutionConstants.ALLOCATED);
			rs = pStmt.executeQuery();
			/** ****** JTEST FIX end **************  on 14DEC09*/

			if (logger.isDebugEnabled()) {
				logger.debug(" Executed Query  ACCRUAL ALLOCATION FOR ACCOUNT CLOSURE");
			}
			results = false;
			String details = "";
			String extraDetails = "";
			String typSettle="";
			while(rs.next()){
				results = true;
				hmFields.put("DAT_POSTING",(Date)rs.getDate("DAT_ADJUSTEDACCRUAL"));
				hmFields.put("DAT_SENT",(Date)rs.getDate("DAT_ACCRUAL"));
				acctDoObj = new AccountingDO();
				/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
				//ENH_1110 Starts
				acctDoObj.setExpiringToday(true);
				//ENH_1110 Ends
				/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
				extraDetails = "";
				details = "";
				poolId = "";
				accountingEntryId = "";

				poolId = rs.getString("ID_POOL");
				hmFields.put(ExecutionConstants.PROCESSID,rs.getLong("NBR_PROCESSID")); // Mashreq_AE_Hook
				amountToPost = new BigDecimal(rs.getString("SUM"));
				flagAllocation = rs.getString("FLG_ALLOCATION");

				//fill in the AccountingDo object's attributes.
				acctDoObj.set_nbr_ownaccount(rs.getString("PARTICIPANT_ID"));
				acctDoObj.set_nbr_coreaccount(rs.getString("NBR_COREACCOUNT"));
				acctDoObj.set_cod_branch(rs.getString("COD_BRANCH"));
				acctDoObj.set_cod_gl(rs.getString("COD_GL"));
				acctDoObj.set_cod_bnk_lgl(rs.getString("COD_BNK_LGL"));
				acctDoObj.set_nbr_ibanacct(rs.getString("NBR_IBANACCNT"));
				acctDoObj.set_cod_ccy(rs.getString("COD_CCY_ACCOUNT"));
				//put this in extraDetailsField HashMap
				extraDetails = "ALLOCATOR" + rs.getString("ALLOCATOR")+ "TYP_SETTLE" + rs.getString("TYP_SETTLE");
				hmFields.put("POOL_FLG_CONSOLIDATION",rs.getString("FLG_CONSOLIDATION"));
				if (logger.isDebugEnabled()) {
					logger.debug("Account Object for Allocation" + acctDoObj);
				}

				typSettle=rs.getString("TYP_SETTLE");
				if(typSettle.equalsIgnoreCase("PARTICIPANTCREDIT")||typSettle.equalsIgnoreCase("CENTRALCREDIT")){
					details = ExecutionConstants.ALLOCATIONCR;//this maps to TXT_EXTINFO2
				}else if(typSettle.equalsIgnoreCase("PARTICIPANTDEBIT")||typSettle.equalsIgnoreCase("CENTRALDEBIT")){
					details = ExecutionConstants.ALLOCATIONDR;//this maps to TXT_EXTINFO2
				}
				bd_amtNotPosted = setUnpostedAmounts(details,acctDoObj,
						pstmtAccountUnposted,businessdate,ExecutionConstants.ACCRUAL);

				if (logger.isDebugEnabled()) {
					logger.debug("Unposted Amount "  + bd_amtNotPosted +"for " + details);
				}
				//chk if any amount is left to be posted
				if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
					amountToPost = amountToPost.add(bd_amtNotPosted);
					if (logger.isDebugEnabled()) {
						logger.debug("Adding Unposted Amount  "  + bd_amtNotPosted +"for ExecutionConstants.ALLOCATION "+amountToPost);
					}
				}// UNPOSTED AMOUNT
				
				if (amountToPost.compareTo(zero) != 0){
					accountingEntryId = "" + processEntry(acctDoObj,
							amountToPost,ExecutionConstants.ACCRUAL,
							ExecutionConstants.NET, details,pstmtAccountingEntry,
							/** ****** JTEST FIX start ************  on 14DEC09*/
//							pstmtAcctUnpostedInsert,poolId,hmFields);
							pstmtAcctUnpostedInsert,poolId,hmFields, conn);
							/** ****** JTEST FIX end **************  on 14DEC09*/
				}else{
					if (logger.isDebugEnabled()) {
						logger.debug("Not Accruing alocated amount as it is 0 " + acctDoObj);
					}
				}
			}
			if (!results){
				logger.fatal(" No records for Accrual -Allocation" + sqlAllocAccrualDtls.toString());
			}
			/*--//Seems we dont require this
			generateBankShareTreasuryShareEntries(pstmtAccountingEntry,
						pstmtAcctUnpostedInsert,businessdate,strbusinessdate,ExecutionConstants.ACCRUAL,generateFor,generateForId,client);
            */
			/** ****** FindBugs Fix Start ***********  on 14DEC09*/
		}catch(SQLException e){
			logger.fatal("generateAccrualEntries: ", e);
			throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, e.getMessage());
		}catch(NamingException e){
			logger.fatal("generateAccrualEntries: ", e);
			throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, e.getMessage());
			/** ****** FindBugs Fix End ***********  on 14DEC09*/
		}finally{
			LMSConnectionUtility.close(rs);
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			LMSConnectionUtility.close(stmt);
			LMSConnectionUtility.close(pStmt);
			/** ****** JTEST FIX end **************  on 14DEC09*/
			LMSConnectionUtility.close(conn);
		}
		return accrualEntries;

	
	}
	
	public boolean[] generatePostingEntriesForAccountClosure(PreparedStatement pstmtAccountingEntry,PreparedStatement pstmtTxnPoolPartyDtls
			,PreparedStatement pstmtAccountUnposted,PreparedStatement pstmtAcctUnpostedInsert, 
			Date businessdate,String participantId)
			throws LMSException, SQLException{
		//SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		//String strbusinessdate = formatter.format(businessdate);

		String sqlString;
		String sqlGroupAccntsSuddenClose;
		String sqlSelectAccntsSuddenClose;
		StringBuilder sbufWhereAccntsSuddenClose = new StringBuilder("");
		String accountingEntryId = null;
		String poolId = null;

		/** ****** JTEST FIX start ************  on 14DEC09*/
//		Statement stmt = null;
		PreparedStatement pStmt = null;
		/** ****** JTEST FIX end **************  on 14DEC09*/
		ResultSet rs=null;
		Connection conn = null;
		AccountingDO acctDoObj = null;
		boolean b_flgSeperate = true;
		//BigDecimal amtToPostCredit = null;
		//BigDecimal amountToPostDebit= null;
		BigDecimal amountToPost = null;
		Date postingDate = null;
		Date valueDate = null;
		
		sqlSelectAccntsSuddenClose = (new StringBuffer("SELECT ")
				.append("   PARTY.ID_POOL,")
				.append("   PARTY.PARTICIPANT_ID,")
				.append("   TXN.NBR_PROCESSID,") // Mashreq_AE_Hook
				.append("   COD_PARTY_CCY, ")
				.append("   NBR_COREACCOUNT, ")
				.append("   COD_BRANCH,")
				.append("   PARTY.COD_GL, ")
				.append("   COD_BNK_LGL,")
				.append("   NBR_IBANACCNT, ")
				.append(" SUM((CASE WHEN (  TXN.TYP_ACCTINT_DRACT = 'POST' OR ")
				.append(" (TXN.AMT_POOL_INTCALC_BALANCE < 0 AND TXN.TYP_ACCTINT_DRACT = 'SUPPDR') OR ")
				.append(" (TXN.AMT_POOL_INTCALC_BALANCE >= 0 AND TXN.TYP_ACCTINT_DRACT = 'SUPPCR')) ")
				.append(" THEN PARTY.AMT_DR_INTEREST ELSE 0 END )) SUM_DR_INT, ")
				.append(" SUM((CASE WHEN (  TXN.TYP_ACCTINT_CRACT = 'POST' OR ")
				.append(" (TXN.AMT_POOL_INTCALC_BALANCE < 0 AND TXN.TYP_ACCTINT_CRACT = 'SUPPDR') OR ")
				.append(" (TXN.AMT_POOL_INTCALC_BALANCE >= 0 AND    TXN.TYP_ACCTINT_CRACT = 'SUPPCR')) ")
				.append(" THEN PARTY.AMT_CR_INTEREST ELSE 0 END )) SUM_CR_INT,")
				.append("   PRCIPANT.DAT_END DAT_POSTING,(PRCIPANT.DAT_END + 1) DAT_POST_SETTLEMENT,(PRCIPANT.DAT_END + 1) CREDITDELAY,")
				.append("   (PRCIPANT.DAT_END + 1) DEBITDELAY,")
				.append("   PARTY.FLG_DEF_DRCR_CONSOL CONSOLIDATE,")
				.append("   NVL(PARTY.AMT_MIN_CR,0) AMT_MIN_CR,")
				.append("   NVL(PARTY.AMT_MIN_DR,0) AMT_MIN_DR ")
				.append(" FROM OPOOL_TXNPOOL_PARTYDTLS PARTY,")
				.append("	 OPOOL_TXNPOOLHDR TXN, OPOOL_POOLPARTICIPANT PRCIPANT  ")).toString();
//		Takes care of all those accounts which are being closed
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		sbufWhereAccntsSuddenClose.append(" WHERE PARTY.PARTICIPANT_ID= '"+participantId+"' AND PRCIPANT.PARTICIPANT_ID= '"+participantId+"' AND PARTY.ID_POOL = TXN.ID_POOL AND PRCIPANT.ID_POOL = TXN.ID_POOL ")
		sbufWhereAccntsSuddenClose.append(" WHERE PARTY.PARTICIPANT_ID= ? AND PRCIPANT.PARTICIPANT_ID= ? AND PARTY.ID_POOL = TXN.ID_POOL AND PRCIPANT.ID_POOL = TXN.ID_POOL ")
		/** ****** JTEST FIX end **************  on 14DEC09*/
				.append(" AND PRCIPANT.FLG_ACCOUNTCLS = 'Y' ")
				.append(" AND	   TXN.NBR_PROCESSID = PARTY.NBR_PROCESSID   ")
				.append(" AND	   TXN.DAT_BUSINESS_POOLRUN = PARTY.DAT_BUSINESS_POOLRUN  ")
				.append(" AND	   TXN.TXT_STATUS <> 'POSTED' ")
				.append(" AND (TYP_POSTING  = 'R' ) AND TXN.FLG_REALPOOL = 'Y' ")
				.append(" AND TXN.COD_COUNTER_ACCOUNT = 'B' ")
				.append(" AND	 PARTY.TYP_PARTICIPANT = 'ACCOUNT'  ")
				.append(" AND	 PARTY.DAT_BUSINESS_POOLRUN BETWEEN  NVL(TXN.DAT_LASTPOSTINGCALC,TXN.DAT_START-1)+1  AND  TXN.DAT_ADJUSTEDPOSTING  ")
				.append(" AND	 PARTY.ID_BVT = (SELECT MAX(TO_NUMBER(M.ID_BVT))   ")
				.append("		 			  			FROM 	 OPOOL_TXNPOOLHDR M  ")
				.append("								WHERE M.ID_POOL = TXN.ID_POOL   ")
				.append("								AND M.COD_GL = TXN.COD_GL ")
				.append("								AND M.DAT_BUSINESS_POOLRUN = TXN.DAT_BUSINESS_POOLRUN 								")
				.append("					 )").toString();
/*
				if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
					sbufWhereAccntsSuddenClose.append(" AND TXN.ID_POOL IN (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR LOT ");
					sbufWhereAccntsSuddenClose.append(" WHERE LOT.NBR_LOTID =" + generateForId+")");
				}
*/
		sqlGroupAccntsSuddenClose = new StringBuffer(" GROUP BY ")
				.append("   PARTY.ID_POOL,")
				.append("   PARTY.PARTICIPANT_ID, ")
				.append("   TXN.NBR_PROCESSID, ") // Mashreq_AE_Hook
				.append("   COD_PARTY_CCY, ")
				.append("   NBR_COREACCOUNT, ")
				.append("   COD_BRANCH,")
				.append("   PARTY.COD_GL,")
				.append("   COD_BNK_LGL,")
				.append("   NBR_IBANACCNT, ")
				.append("   PRCIPANT.DAT_END, ")
				//.append("   DAT_POST_SETTLEMENT,TXN.DAT_POST_CREDITDELAY ,")
				//.append("   TXN.DAT_POST_DEBITDELAY ,")
				.append("   PARTY.FLG_DEF_DRCR_CONSOL,")
				.append("   PARTY.AMT_MIN_CR,")
				.append("   PARTY.AMT_MIN_DR ").toString();

		//Combine the subParts of the query
		sqlString = new StringBuffer(sqlSelectAccntsSuddenClose+sbufWhereAccntsSuddenClose.toString()+sqlGroupAccntsSuddenClose).toString();

		if (logger.isDebugEnabled())
			logger.debug("Query PARTICIPANT POSTING" + sqlString);

		try{
			conn = LMSConnectionUtility.getConnection();
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			stmt = conn.createStatement();
//			rs = stmt.executeQuery(sqlString);
			
			pStmt = conn.prepareStatement(sqlString);
			pStmt.setString(1, participantId);
			pStmt.setString(2, participantId);
			rs = pStmt.executeQuery();
			/** ****** JTEST FIX end **************  on 14DEC09*/
			if (logger.isDebugEnabled()) {
				logger.debug(" Executed Query PARTICIPANT POSTING ");
			}

			HashMap hmFields = new HashMap();
			hmFields.put(ExecutionConstants.BUSINESSDATE,businessdate); // Mashreq_AE_Hook

			BigDecimal amtToPostCreditP = new BigDecimal(0);
			BigDecimal amountToPostDebitP = new BigDecimal(0);
			BigDecimal minAmtCredit = new BigDecimal(0);
			BigDecimal minAmtDebit = new BigDecimal(0);
			BigDecimal bd_amtNotPosted = new BigDecimal(0);
			BigDecimal minAmtNet = new BigDecimal(0);
			boolean results = false;
			boolean combinedEntry = false;
			while(rs.next())
				{
					results = true;
					acctDoObj = new AccountingDO();
					/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
					//ENH_1110 Starts
					acctDoObj.setExpiringToday(true);					
					//ENH_1110 Ends
					/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
					String flg_def_drcr_consol = rs.getString("CONSOLIDATE");
					b_flgSeperate = true;
					accountingEntryId = "0";
					poolId = "";
					combinedEntry = false;
					amtToPostCreditP = rs.getBigDecimal("SUM_CR_INT");

					amountToPostDebitP = rs.getBigDecimal("SUM_DR_INT");

					poolId = rs.getString("ID_POOL");
					hmFields.put(ExecutionConstants.PROCESSID,rs.getLong("NBR_PROCESSID")); // Mashreq_AE_Hook
					if (logger.isDebugEnabled()) {
						logger.debug("Resultset flg_def_drcr_consol" + flg_def_drcr_consol);
						logger.debug("Resultset POOLID" + poolId);
					}
					if("Y".equalsIgnoreCase(flg_def_drcr_consol!=null
																?flg_def_drcr_consol.trim()
																:"" )){
						//Get the NetInterest,if consolidate
						amountToPost = amtToPostCreditP.subtract(amountToPostDebitP);
						// Set the seperate posting flag to False
						//If we are consolidating then we need to check if we have both Dr and Cr Interest and post them
						//CR CUST/DR CUST  depending on which is greater and divide it into 3 legs
						//and need to pass crAmt and DrAMt
						//DR PAYMENTS
						//CR RECE
						if(amtToPostCreditP.compareTo(zero)!=0 && amountToPostDebitP.compareTo(zero)!=0){
							combinedEntry = true;
							//if both debit and credit interest are present
						}
						b_flgSeperate = false;
					}
					if (logger.isDebugEnabled()) {
						logger.debug(" Flag Separate" + b_flgSeperate);
					}
					//fill in the AccountingDo object's attributes.
					acctDoObj.set_cod_bnk_lgl(rs.getString("COD_BNK_LGL"));
					acctDoObj.set_cod_branch(rs.getString("COD_BRANCH"));
					acctDoObj.set_cod_gl(rs.getString("COD_GL"));
					acctDoObj.set_cod_ccy(rs.getString("COD_PARTY_CCY"));
					acctDoObj.set_nbr_coreaccount(rs.getString("NBR_COREACCOUNT"));
					acctDoObj.set_nbr_ibanacct(rs.getString("NBR_IBANACCNT"));
					acctDoObj.set_nbr_ownaccount(rs.getString("PARTICIPANT_ID"));

					hmFields.put("DAT_VALUE",(Date)rs.getDate("DAT_POSTING"));
					hmFields.put("DAT_POST_SETTLEMENT",(Date)rs.getDate("DAT_POST_SETTLEMENT"));
					hmFields.put("DAT_POST_CREDITDELAY",(Date)rs.getDate("CREDITDELAY"));
					hmFields.put("DAT_POST_DEBITDELAY",(Date)rs.getDate("DEBITDELAY"));

					minAmtCredit = rs.getBigDecimal("AMT_MIN_CR");
					minAmtDebit = rs.getBigDecimal("AMT_MIN_DR");

					if (logger.isDebugEnabled()) {
						logger.debug("Value Date: " +hmFields.get("DAT_VALUE"));
						logger.debug("(Date)hmFields.get(DAT_POST_DEBITDELAY" +hmFields.get("DAT_POST_DEBITDELAY"));
						logger.debug("(Date)hmFields.get(DAT_POST_CREDITDELAY" +hmFields.get("DAT_POST_CREDITDELAY"));
						logger.debug("(Date)hmFields.get(DAT_POST_SETTLEMENT" +hmFields.get("DAT_POST_SETTLEMENT"));
						logger.debug("minAmtCredit" + minAmtCredit);
						logger.debug("minAmtDebit" + minAmtDebit);
					}
					// check is any amount is left due to be posted.
					bd_amtNotPosted = new BigDecimal(0);

					valueDate = (Date)hmFields.get("DAT_VALUE");
					valueDate = (valueDate == null ? businessdate : addOffsetToDate(valueDate, 1));
					hmFields.put("DAT_SENT", valueDate);
		
					if(b_flgSeperate){
						//POSTING CREDIT INTEREST, only if greater than minmCreditInterest
						//chk the order
						//1. add UnPostedAmount
						//2. !=0
						//3. > than mimnAmtInterest
						//then post
						bd_amtNotPosted = setUnpostedAmounts(ExecutionConstants.CREDIT_INTEREST,acctDoObj,
								pstmtAccountUnposted,businessdate,ExecutionConstants.POSTING);
						if (logger.isDebugEnabled()) {
							logger.debug("Unposted Amount "  + bd_amtNotPosted +"for ExecutionConstants.CREDIT_INTEREST");
						}
						//chk if any amount is left to be posted
						if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
							amtToPostCreditP = amtToPostCreditP.add(bd_amtNotPosted);
							if (logger.isDebugEnabled()) {
								logger.debug("Adding Unposted Amount  "  + bd_amtNotPosted +"for ExecutionConstants.CREDIT_INTEREST "+amtToPostCreditP);
							}
						}// UNPOSTED AMOUNT
						//CHECK FOR MINIMUM AMT INTEREST
						if (amtToPostCreditP.compareTo(zero) != 0){
							if(amtToPostCreditP.compareTo(minAmtCredit)>=0){
								postingDate = (Date)hmFields.get("DAT_POST_CREDITDELAY");
								postingDate = postingDate!=null?postingDate:businessdate;
								hmFields.put("DAT_POSTING",postingDate);
								
								accountingEntryId = "" + processEntry(acctDoObj,
									amtToPostCreditP,ExecutionConstants.POSTING,
									ExecutionConstants.CREDIT,
									ExecutionConstants.CREDIT_INTEREST,pstmtAccountingEntry,
									/** ****** JTEST FIX start ************  on 14DEC09*/
//									pstmtAcctUnpostedInsert,poolId,hmFields);//INT_1039
									pstmtAcctUnpostedInsert,poolId,hmFields, conn);//INT_1039
									/** ****** JTEST FIX end **************  on 14DEC09*/
							}else{
								addToAcctUnpostedTbl(pstmtAcctUnpostedInsert,acctDoObj.get_nbr_ownaccount(),
									amtToPostCreditP,ExecutionConstants.POSTING,ExecutionConstants.CREDIT_INTEREST,"INTEREST LESS THAN MINIMUM AMOUNT INTEREST",poolId);//INT_1039
							}//minCreditInterest
						}else{
							logger.fatal("Credit Interest is 0 for"  + acctDoObj + "SO not posting the amount ");
						}

					//POSTING DEBIT INTEREST
						bd_amtNotPosted = new BigDecimal(0);//RESETTING AMOUNT NOT POSTED
						bd_amtNotPosted = setUnpostedAmounts(ExecutionConstants.DEBIT_INTEREST,acctDoObj,
						pstmtAccountUnposted,businessdate,ExecutionConstants.POSTING);
						if (logger.isDebugEnabled()) {
							logger.debug("Unposted Amount "  + bd_amtNotPosted +"for ExecutionConstants.DEBIT_INTEREST");
						}
						//chk if any amount is left to be posted
						if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
							amountToPostDebitP = amountToPostDebitP.add(bd_amtNotPosted);
							if (logger.isDebugEnabled()) {
								logger.debug("Adding Unposted Amount  "  + bd_amtNotPosted +"for ExecutionConstants.DEBIT_INTEREST "+amountToPostDebitP);
							}
						}// UNPOSTED DEBIT AMOUNT
						//check with minm debit amount

						if (amountToPostDebitP.compareTo(zero)!=0){
							if(amountToPostDebitP.compareTo(minAmtDebit)>=0){
								postingDate = (Date)hmFields.get("DAT_POST_DEBITDELAY");
								postingDate = postingDate!=null?postingDate:businessdate;
								hmFields.put("DAT_POSTING",postingDate);
								
								accountingEntryId = accountingEntryId + "|" + processEntry(acctDoObj,
									amountToPostDebitP,ExecutionConstants.POSTING,
									ExecutionConstants.DEBIT,
									ExecutionConstants.DEBIT_INTEREST,
									pstmtAccountingEntry,pstmtAcctUnpostedInsert,
									/** ****** JTEST FIX start ************  on 14DEC09*/
//									poolId,hmFields);
									poolId,hmFields, conn);
									/** ****** JTEST FIX end **************  on 14DEC09*/
							}else{
								addToAcctUnpostedTbl(pstmtAcctUnpostedInsert,acctDoObj.get_nbr_ownaccount(),
										amountToPostDebitP,ExecutionConstants.POSTING,ExecutionConstants.DEBIT_INTEREST,"INTEREST LESS THAN MINIMUM AMOUNT INTEREST",poolId);//INT_1039
							}//minDebitInterest
						}else{
							logger.fatal("Debit Interest is 0 for"  + acctDoObj + "SO not posting the amount ");
						}

					}else{
						//call processEntry for the NetAmount --consolidated

						bd_amtNotPosted = setUnpostedAmounts(ExecutionConstants.NET_INTEREST,acctDoObj,
													pstmtAccountUnposted,businessdate,ExecutionConstants.POSTING);
						if (logger.isDebugEnabled()) {
						  logger.debug("Unposted Amount "  + bd_amtNotPosted +"for ExecutionConstants.NET_INTEREST");
						 }
						 //chk if any amount is left to be posted
						if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
							amountToPost = amountToPost.add(bd_amtNotPosted);
							if (logger.isDebugEnabled()) {
								logger.debug("Adding Unposted Amount  "  + bd_amtNotPosted +"for ExecutionConstants.NET_INTEREST "+amountToPost);
							 }
						 }// UNPOSTED NET INTEREST
						//	CHECK IF THE NET_INTEREST IS CREDIT(+ve) or DEBIT(-ve)
						//	if (CREDIT)
						if (amountToPost.compareTo(zero) > 0){
							postingDate = (Date)hmFields.get("DAT_POST_CREDITDELAY");
							minAmtNet =  minAmtCredit;
						}else if (amountToPost.compareTo(zero) < 0){
							postingDate = (Date)hmFields.get("DAT_POST_DEBITDELAY");
							minAmtNet = minAmtDebit;
						}else{
							logger.fatal("Net Interest is 0 for"  + acctDoObj + "SO not posting the amount ");
						}
						postingDate = postingDate!=null?postingDate:businessdate;
						hmFields.put("DAT_POSTING",postingDate);
						
						//CONSIDERING MINM aMOUNT INTEREST
						if((amountToPost).abs().compareTo(minAmtNet)>=0){//Changed for absolute of NET_INTEREST while posting
							if(combinedEntry){
								if (logger.isDebugEnabled()) {
									logger.debug("Calling combinedEntry ");
								}
								//IF bd_amtNotPosted IS CR ADD IT TO CREDIT INTEREST AMT ELSE DEBIT INTEREST AMT
								if (bd_amtNotPosted.compareTo(zero) > 0){
									amtToPostCreditP = amtToPostCreditP.add(bd_amtNotPosted);
								}else if(bd_amtNotPosted.compareTo(zero) < 0){
									amountToPostDebitP = amountToPostDebitP.add(bd_amtNotPosted);

								accountingEntryId = "" + processCombinedEntry(acctDoObj,
																amtToPostCreditP,amountToPostDebitP,
																ExecutionConstants.POSTING,
																ExecutionConstants.NET,
																ExecutionConstants.NET_INTEREST,
																pstmtAccountingEntry,pstmtAcctUnpostedInsert,
																/** ****** JTEST FIX start ************  on 14DEC09*/
//																poolId,hmFields);//INT_1039
																poolId,hmFields, conn);//INT_1039
																/** ****** JTEST FIX end **************  on 14DEC09*/
								}
							}else{
								accountingEntryId = "" + processEntry(acctDoObj,
																amountToPost,ExecutionConstants.POSTING,
																ExecutionConstants.NET,
																ExecutionConstants.NET_INTEREST,
																pstmtAccountingEntry,pstmtAcctUnpostedInsert,
																/** ****** JTEST FIX start ************  on 14DEC09*/
//																poolId,hmFields);//INT_1039
																poolId,hmFields, conn);//INT_1039
																/** ****** JTEST FIX end **************  on 14DEC09*/
							}//combinedEntry
						}else{
							addToAcctUnpostedTbl(pstmtAcctUnpostedInsert,acctDoObj.get_nbr_ownaccount(),
								amountToPost,ExecutionConstants.POSTING,ExecutionConstants.NET_INTEREST,"INTEREST LESS THAN MINIMUM AMOUNT INTEREST",poolId);	//INT_1039
						}//minNetInterest
					}//flgSeparate
					if (logger.isDebugEnabled()) {
							logger.debug("After processEntry accountingEntryId"  + accountingEntryId);
					}
					//Update the Prepared Statements for OPOOL_PARTYDETAILS
					updateInterest(pstmtTxnPoolPartyDtls,accountingEntryId,poolId,acctDoObj.get_nbr_ownaccount(),ExecutionConstants.POSTING);
					

				}//end of while resultset
				if (!results){
					logger.fatal(" No records for Posting -participant interest" + sqlString);
				}
							
			rs.close();
			//FOR POSTING ALLOCATION ENTRIES
			String sqlAllocAccrualSuddenAccountClose = (new StringBuffer("SELECT ")
					//.append(" TXN.ID_POOL, TXN.FLG_CONSOLIDATION, PARTY.PARTICIPANT_ID PARTICIPANT_ID, ")
					.append(" TXN.ID_POOL, TXN.FLG_CONSOLIDATION, PARTY.PARTICIPANT_ID PARTICIPANT_ID, TXN.NBR_PROCESSID NBR_PROCESSID, ") // Mashreq_AE_Hook
					.append(" COD_CCY_ACCOUNT, TYP_SETTLE, SUM(EQV_AMT_ALLOCATION) SUM, ALLOCATOR, NBR_COREACCOUNT,  ")
					.append(" COD_BRANCH,PARTY.COD_GL, COD_BNK_LGL, NBR_IBANACCNT,  ")
					.append(" PRCIPANT.DAT_END DAT_POSTING,(PRCIPANT.DAT_END + 1) DAT_POST_SETTLEMENT, (PRCIPANT.DAT_END + 1) CREDITDELAY,")
					.append(" (PRCIPANT.DAT_END + 1) DEBITDELAY ")
					.append(" FROM  OPOOL_TXNPOOL_ALLOCATIONDTLS ALLOC, OPOOL_TXNPOOLHDR TXN, OPOOL_TXNPOOL_PARTYDTLS PARTY, OPOOL_POOLPARTICIPANT PRCIPANT ")).toString();

			/** ****** JTEST FIX start ************  on 14DEC09*/
//			StringBuffer sqlAllocAccrualSuddenAccountCloseWhere = new StringBuffer(" PARTY.PARTICIPANT_ID='"+participantId+"' AND PRCIPANT.PARTICIPANT_ID='"+participantId+"' AND TXN.ID_POOL = ALLOC.ID_POOL AND TXN.ID_POOL = PARTY.ID_POOL AND PRCIPANT.ID_POOL = TXN.ID_POOL ")
			StringBuilder sqlAllocAccrualSuddenAccountCloseWhere = new StringBuilder(" PARTY.PARTICIPANT_ID= ? AND PRCIPANT.PARTICIPANT_ID= ? AND TXN.ID_POOL = ALLOC.ID_POOL AND TXN.ID_POOL = PARTY.ID_POOL AND PRCIPANT.ID_POOL = TXN.ID_POOL ")
			/** ****** JTEST FIX end **************  on 14DEC09*/
			.append(" AND PRCIPANT.FLG_ACCOUNTCLS = 'Y' ")
			.append(" AND TXN.NBR_PROCESSID = ALLOC.NBR_PROCESSID   ")
			.append(" AND TXN.DAT_BUSINESS_POOLRUN = ALLOC.DAT_BUSINESS_POOLRUN ")
			.append(" AND TXN.NBR_PROCESSID = PARTY.NBR_PROCESSID  ")
			.append(" AND TXN.DAT_BUSINESS_POOLRUN = PARTY.DAT_BUSINESS_POOLRUN ")
			.append(" AND PARTY.ID_POOL = ALLOC.ID_POOL ")
			.append(" AND PARTY.ID_BVT	 = ALLOC.ID_BVT ")
			.append(" AND PARTY.PARTICIPANT_ID = ALLOC.PARTICIPANT_ID      ")
			.append(" AND TXN.TXT_STATUS <> 'POSTED'  ")
			.append(" AND FLG_REAL = 'Y' ")
			.append(" AND FLG_ALLOCATION <> 'R' AND FLG_ALLOCATION <> 'T'  ")
			.append(" AND (ALLOC.TYP_PARTICIPANT = 'ACCOUNT' AND PARTY.TYP_PARTICIPANT = 'ACCOUNT' ) ")
			.append(" AND TXN.FLG_REALPOOL = 'Y'   ")
			.append(" AND  ALLOC.ID_BVT = (SELECT MAX(TO_NUMBER(M.ID_BVT)) ")
			.append(" FROM 	 OPOOL_TXNPOOLHDR M ")
			.append(" WHERE M.ID_POOL = TXN.ID_POOL ")
			.append(" AND M.DAT_BUSINESS_POOLRUN = TXN.DAT_BUSINESS_POOLRUN   ")
			.append(" AND M.COD_GL = TXN.COD_GL) ")
			.append(" AND ALLOC.DAT_BUSINESS_POOLRUN BETWEEN  NVL(TXN.DAT_LASTPOSTINGCALC,TXN.DAT_START-1)+1  AND TXN.DAT_ADJUSTEDPOSTING ");
/*
			if(generateFor.equalsIgnoreCase(ExecutionConstants.LOT)){
				sqlAllocAccrualSuddenAccountCloseWhere.append(" AND TXN.ID_POOL IN (SELECT ID_POOL FROM OPOOL_TXNPOOLHDR LOT ");
				sqlAllocAccrualSuddenAccountCloseWhere.append(" WHERE LOT.NBR_LOTID =" + generateForId+")");
			}
*/
			String sqlAllocAccrualSuddenClosegroupBy1 = (new StringBuffer("TXN.ID_POOL,")
					.append(" TXN.FLG_CONSOLIDATION, PARTY.PARTICIPANT_ID, ")
					.append(" COD_CCY_ACCOUNT, TYP_SETTLE, ALLOCATOR,  ")
					.append(" NARRATION ,NBR_COREACCOUNT, COD_BRANCH,")
					.append(" PARTY.COD_GL,COD_BNK_LGL, NBR_IBANACCNT,")
					.append(" PARTY_ALLOC_SEQUENCE, "))
					//.append("  PRCIPANT.DAT_END ")
					.append("  PRCIPANT.DAT_END, TXN.NBR_PROCESSID ") // Mashreq_AE_Hook
					//.append("  PRCIPANT.DAT_END, DAT_POST_SETTLEMENT, DAT_POST_CREDITDELAY,")
					//.append("  DAT_POST_DEBITDELAY ")
					.toString();

			StringBuilder sqlAllocDtls = new StringBuilder(sqlAllocAccrualSuddenAccountClose )
			.append(" WHERE ")
			.append(sqlAllocAccrualSuddenAccountCloseWhere.toString())
			.append(" GROUP BY ")
			.append(sqlAllocAccrualSuddenClosegroupBy1);

			if (logger.isDebugEnabled())
				logger.debug("Query POSTING ALLOCATION: " + sqlAllocDtls);

			/** ****** JTEST FIX start ************  on 14DEC09*/
//			rs = stmt.executeQuery(sqlAllocDtls.toString());
			LMSConnectionUtility.close(pStmt); //15.1.1_Resource_Leaks_Fix
			pStmt = conn.prepareStatement(sqlAllocDtls.toString());
			pStmt.setString(1, participantId);
			pStmt.setString(2, participantId);
			rs = pStmt.executeQuery();
			/** ****** JTEST FIX end **************  on 14DEC09*/


			if (logger.isDebugEnabled()) {
				logger.debug(" Executed Query  POSTING ALLOCATION ");
			}
			results = false;
			String details = "";
			String extraDetails = "";
			String typSettle="";
			while(rs.next()){
				results = true;
				acctDoObj = new AccountingDO();
				/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
				//ENH_1110 Starts
				acctDoObj.setExpiringToday(true);
				//ENH_1110 Ends
				/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
				poolId = "";
				accountingEntryId = "0";
				details = "";
				poolId = rs.getString("ID_POOL");
				hmFields.put(ExecutionConstants.PROCESSID,rs.getLong("NBR_PROCESSID")); // Mashreq_AE_Hook
				amountToPost = new BigDecimal(rs.getString("SUM"));

				//fill in the AccountingDo object's attributes.
				acctDoObj.set_nbr_ownaccount(rs.getString("PARTICIPANT_ID"));
				acctDoObj.set_nbr_coreaccount(rs.getString("NBR_COREACCOUNT"));
				acctDoObj.set_cod_branch(rs.getString("COD_BRANCH"));
				acctDoObj.set_cod_gl(rs.getString("COD_GL"));
				acctDoObj.set_cod_bnk_lgl(rs.getString("COD_BNK_LGL"));
				acctDoObj.set_nbr_ibanacct(rs.getString("NBR_IBANACCNT"));
				acctDoObj.set_cod_ccy(rs.getString("COD_CCY_ACCOUNT"));

				//BigDecimal amtForAllocation = rs.getBigDecimal("SUM");
				//populate Dates
				hmFields.put("DAT_VALUE",(Date)rs.getDate("DAT_POSTING"));
				hmFields.put("DAT_POST_SETTLEMENT",(Date)rs.getDate("DAT_POST_SETTLEMENT"));
				hmFields.put("DAT_POST_CREDITDELAY",(Date)rs.getDate("CREDITDELAY"));
				hmFields.put("DAT_POST_DEBITDELAY",(Date)rs.getDate("DEBITDELAY"));
				hmFields.put("POOL_FLG_CONSOLIDATION",rs.getString("FLG_CONSOLIDATION"));
				//put extraDetails and flagAllocation in fieldsHashMap
				extraDetails = "ALLOCATOR IS " + rs.getString("ALLOCATOR")+ "TYP_SETTLE IS " + rs.getString("TYP_SETTLE");
				typSettle=rs.getString("TYP_SETTLE");
				if(typSettle.equalsIgnoreCase("PARTICIPANTCREDIT")||typSettle.equalsIgnoreCase("CENTRALCREDIT")){
					details = ExecutionConstants.ALLOCATIONCR;
				}
				else if(typSettle.equalsIgnoreCase("PARTICIPANTDEBIT")||typSettle.equalsIgnoreCase("CENTRALDEBIT")){
					details = ExecutionConstants.ALLOCATIONDR;
				}

				if (logger.isDebugEnabled()) {
					logger.debug("Value Date: " +hmFields.get("DAT_VALUE"));
					logger.debug("Account Object for Allocation" + acctDoObj);
				}

				bd_amtNotPosted = setUnpostedAmounts(details,acctDoObj,
										pstmtAccountUnposted,businessdate,ExecutionConstants.POSTING);
				if (logger.isDebugEnabled()) {
				  logger.debug("Unposted Amount "  + bd_amtNotPosted +"for ALLOCATION");
				 }
				 //chk if any amount is left to be posted
				if(bd_amtNotPosted!=null && bd_amtNotPosted.compareTo(zero)!=0){
					amountToPost = amountToPost.add(bd_amtNotPosted);
					if (logger.isDebugEnabled()) {
						logger.debug("Adding Unposted Amount  "  + bd_amtNotPosted +"for ALLOCATION "+amountToPost);
					 }
				 }// UNPOSTED ALLOCATION
				//based on amountToPost, decide if it is CREDIT/DEBIT DELAY DAYS FOR POSTING DATE
				if (amountToPost.compareTo(zero) != 0){
					if (amountToPost.compareTo(zero) > 0){
						postingDate = (Date)rs.getDate("CREDITDELAY");
					}else{
						postingDate = (Date)rs.getDate("DEBITDELAY");
					}
					postingDate = postingDate!=null?postingDate:businessdate;
					hmFields.put("DAT_POSTING",postingDate);
					valueDate = (Date)hmFields.get("DAT_VALUE");
					valueDate = (valueDate == null ? businessdate : addOffsetToDate(valueDate, 1));
					hmFields.put("DAT_SENT", valueDate);
					accountingEntryId = "" + processEntry(acctDoObj,
							amountToPost,ExecutionConstants.POSTING,
							ExecutionConstants.NET, details,pstmtAccountingEntry,
							/** ****** JTEST FIX start ************  on 14DEC09*/
//							pstmtAcctUnpostedInsert,poolId,hmFields);//INT_1039
							pstmtAcctUnpostedInsert,poolId,hmFields, conn);//INT_1039
							/** ****** JTEST FIX end **************  on 14DEC09*/
				}else{
					logger.fatal(" Not Posting  Amount as amountTo Post = 0 amount for account " + acctDoObj);
				}
				//update Allocation entries
				//postingEntries[ExecutionConstants.OLM_ACCOUNTINGENTRY_INSERT] = true;
				postingEntries[ExecutionConstants.ALLOCATION_UPDATE] = true;
			}
			if (!results){
				logger.fatal(" No records for Posting -Allocation" + sqlAllocDtls.toString());
			}
			rs.close();
			if (logger.isDebugEnabled()) {
				logger.debug("postingEntries[ExecutionConstants.UNPOSTED_AMOUNT_INSERT] =  " + postingEntries[ExecutionConstants.UNPOSTED_AMOUNT_INSERT]);
			}
			/*
			//INT_1008 generate BankShare and Treasury Share Entries
			generateBankShareTreasuryShareEntries(pstmtAccountingEntry,
						pstmtAcctUnpostedInsert,businessdate,strbusinessdate,ExecutionConstants.POSTING,generateFor,generateForId,client);
			*/
			}
		/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//		catch(Exception e){
		catch(SQLException e){
			logger.fatal("General Exception", e);
			throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, e.getMessage());
		}catch(NamingException e){
			logger.fatal("General Exception", e);
			throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, e.getMessage());	
		/** ****** FindBugs Fix End ***********  on 14DEC09*/
			}finally{
				LMSConnectionUtility.close(rs);
				/** ****** JTEST FIX start ************  on 14DEC09*/
//				LMSConnectionUtility.close(stmt);
				LMSConnectionUtility.close(pStmt);
				/** ****** JTEST FIX end **************  on 14DEC09*/
				LMSConnectionUtility.close(conn);
			}
		return postingEntries;
	}
//SUP_1078--Ends
	//SUP_1082a starts
	/**
	 * This method provides the nextCycleAccrualDates given an array of cod_gl, an array of id_pool, nbr_processId
	 * This helps in calculating the next dat_accrual and dat_adjusted_accrual
	 *
	 * @param conn
	 * @param poolId
	 * @param codGL
	 * @param processId
	 * @param businessDate 
	 * @return
	 */
	public void calculateNextCycleAccrualDates(Connection conn, long processId,
            Date businessDate) throws LMSException {
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		Statement stmt = null;
		PreparedStatement pStmt = null;
		/** ****** JTEST FIX end **************  on 14DEC09*/
		ResultSet rSet = null;
		StringBuilder sBuf = new StringBuilder();
		String callMode = "E";
		
		//Do Sanity checks for conn==null,processId=0, businessDate==null
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		SimpleDateFormat formatter = new SimpleDateFormat(LMSConstants.DATE_FORMAT);
		SimpleDateFormat formatter = new SimpleDateFormat(LMSConstants.DATE_FORMAT, Locale.US);
		/** ****** JTEST FIX end **************  on 14DEC09*/
		/** ****** FindBugs Fix Start ***********  on 14DEC09*/
//		String bDate = formatter.format(businessDate);
		/** ****** FindBugs Fix End ***********  on 14DEC09*/
		
		ArrayList calendarCodes = null;

		try{	
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			stmt = conn.createStatement();
			/** ****** JTEST FIX end **************  on 14DEC09*/
			sBuf.append(" SELECT tmp.calendar, tmp.dat_start, tmp.cod_accr_cycle, tmp.dat_lastaccrual, ");
			sBuf.append(" tmp.dat_lastaccrualcalc, tmp.id_pool, tmp.cod_gl, ");
			sBuf.append(" CASE WHEN typ_accr = 'DAILY' THEN 1 WHEN typ_accr = 'WEEKLY' THEN 2 ");
			sBuf.append(" WHEN typ_accr = 'MONTHLY' THEN 3 WHEN typ_accr = 'YEARLY' THEN 4 ELSE 0 END typ_accr, ");
			sBuf.append(" CASE WHEN opt_accr IS NULL THEN 0 ELSE opt_accr + 0 END opt_accr, ");
			sBuf.append(" CASE WHEN unit1_accr IS NULL THEN -1 ELSE unit1_accr + 0 END unit1_accr, ");
			sBuf.append(" CASE WHEN unit2_accr IS NULL THEN -1 ELSE unit2_accr + 0 END unit2_accr, ");
			sBuf.append(" CASE WHEN cod_non_working_day = 'NEXT' THEN 'N'  ");
			sBuf.append(" WHEN cod_non_working_day = 'PREV' THEN 'P'ELSE 'C' END isholiday  ");
			sBuf.append(" FROM orate_accrual_cycle oac, (SELECT (SELECT cod_calendar ");
			
			//ENH_LIQ_0005 Starts
			//sBuf.append(" FROM orbicash_glmst WHERE cod_gl = hdr.cod_gl) calendar, dat_start, ");
			sBuf.append(" FROM olm_glmst WHERE cod_gl = hdr.cod_gl) calendar, dat_start, ");
			//ENH_LIQ_0005 Ends
			
			sBuf.append(" cod_accr_cycle, dat_lastaccrual, dat_lastaccrualcalc, id_pool, cod_gl ");
			sBuf.append(" FROM opool_poolhdr hdr WHERE id_pool IN  (SELECT DISTINCT id_pool  ");
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" FROM opool_txnpoolhdr h WHERE h.nbr_processid = "+processId+")) tmp ");
			sBuf.append(" FROM opool_txnpoolhdr h WHERE h.nbr_processid = ?)) tmp ");
			/** ****** JTEST FIX end **************  on 14DEC09*/
			sBuf.append(" WHERE oac.cod_accr_cycle = tmp.cod_accr_cycle ");

			if(logger.isDebugEnabled()){
				logger.debug(" calculateNextCycleAccrualDates -> "+sBuf.toString());
			}
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			rSet = stmt.executeQuery(sBuf.toString());
			pStmt = conn.prepareStatement(sBuf.toString());
			pStmt.setLong(1, processId);
			rSet = pStmt.executeQuery();
			/** ****** JTEST FIX end **************  on 14DEC09*/
			
			while(rSet.next()){
			    String calendarCode = rSet.getString(1);
				Date poolStartDate = rSet.getDate(2);
				String frequency = rSet.getString(3);
				Date lastAccrualDate = rSet.getDate(4);
				Date lastCalcAccrualDate = rSet.getDate(5);
				String idPool = rSet.getString(6);				
				String hostCode = rSet.getString(7);
				int typeFrequency = rSet.getInt(8);
				int frequencyInfo = rSet.getInt(9);
				int frequencyUnit1 = rSet.getInt(10);
				int frequencyUnit2 = rSet.getInt(11);
				String holidayFlag = rSet.getString(12);
				
				if(logger.isInfoEnabled()){
			        logger.info(" Adding information of pool ["+idPool+"] as [calendarCode] = ["+calendarCode+"]," +
			        " [poolStartDate] = ["+poolStartDate+"], [frequency] = ["+frequency+"], [lastAccrualDate] = ["+lastAccrualDate+"]" +
			        ", [lastCalcAccrualDate] = ["+lastCalcAccrualDate+"], [hostCode] = ["+hostCode+"]+" 
			        +"{ Frequency["+typeFrequency+"], FrequencyInfo["+frequencyInfo+"], "
					+"FrequencyUnit1["+frequencyUnit1+"], FrequencyUnit2["+frequencyUnit2+"], "
					+"HolidayFlag["+holidayFlag+"]");
			    }
				calendarCodes = new ArrayList();
				calendarCodes.add(calendarCode);

				//declaring a return parameter 
				java.util.Date[] nextActionDates = null;
				if(lastAccrualDate == null || lastCalcAccrualDate == null){
				    callMode = "M";
				}
				if(callMode.equals("E")){
				/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
				//ENH_IS686 changes made by satish for Posting Cycle added one argument null for Posting Calendar

					//nextActionDates = 
						//ExecutionUtility.getNextActionDate(calendarCodes, lastAccrualDate, lastCalcAccrualDate
										 //, typeFrequency, frequencyInfo, frequencyUnit1, frequencyUnit2
										 //, holidayFlag, businessDate, callMode);
					nextActionDates = 
						ExecutionUtility.getNextActionDate(calendarCodes, lastAccrualDate, lastCalcAccrualDate
										 , typeFrequency, frequencyInfo, frequencyUnit1, frequencyUnit2,null
										 , holidayFlag, businessDate, callMode);
				//ENH_IS686 Ends
				/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */

				}
				else{
				/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
				//ENH_IS686 changes made by satish for Posting Cycle added one argument null for Posting Calendar
				    //nextActionDates = 
						//ExecutionUtility.getNextActionDate(calendarCodes, poolStartDate, businessDate
										 //, typeFrequency, frequencyInfo, frequencyUnit1, frequencyUnit2
										 //, holidayFlag, businessDate, callMode);
				    nextActionDates = 
						ExecutionUtility.getNextActionDate(calendarCodes, poolStartDate, businessDate
										 , typeFrequency, frequencyInfo, frequencyUnit1, frequencyUnit2,null
										 , holidayFlag, businessDate, callMode);
				//ENH_IS686 Ends
				/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
				}
				
				Date dateArray[] = new Date[2];
//				if(callMode.equals("E")){
//				    dateArray[0] = lastAccrualDate;
//					dateArray[1] = lastCalcAccrualDate;
//				}
//				else{
//				    dateArray[0] = null;
//					dateArray[1] = null;
//				}
//				dateArray[2] = nextActionDates[0];
//				dateArray[3] = nextActionDates[1];
				dateArray[0] = nextActionDates[0];
				dateArray[1] = nextActionDates[1];
				accCache.put(idPool,dateArray);
				if (logger.isDebugEnabled()) {
                    logger.debug(" KEY:["+idPool+"] & VALUE:["+dateArray[0]+", "+dateArray[1]+"] added ");                                 
                }
			}
		}
		catch(Exception e){
			logger.fatal("Exception in calculateNextCycleAccrualDates ", e);
			  throw new LMSException("101302", e.toString());
		}
		finally{
			LMSConnectionUtility.close(rSet);
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			LMSConnectionUtility.close(stmt);
			LMSConnectionUtility.close(pStmt);
			/** ****** JTEST FIX end **************  on 14DEC09*/
		}
    }
	
	/**
	 * This method provides the nextCyclePostingDates given an array of cod_gl, an array of id_pool, nbr_processId
	 * This helps in calculating the next dat_posting and dat_adjusted_posting
	 *
	 * @param conn
	 * @param poolId
	 * @param processId
	 * @param businessDate 
	 * @return
	 */
	public void calculateNextCyclePostingDates(Connection conn, long processId,
            Date businessDate) throws LMSException {
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		Statement stmt = null;
		PreparedStatement pStmt = null;
		/** ****** JTEST FIX end **************  on 14DEC09*/
		ResultSet rSet = null;
		StringBuilder sBuf = new StringBuilder();
		String callMode = "E";
		
		//Do Sanity checks for conn==null,processId=0, businessDate==null
		/** ****** JTEST FIX start ************  on 14DEC09*/
//		SimpleDateFormat formatter = new SimpleDateFormat(LMSConstants.DATE_FORMAT);
		SimpleDateFormat formatter = new SimpleDateFormat(LMSConstants.DATE_FORMAT,Locale.US);
		/** ****** JTEST FIX end **************  on 14DEC09*/
		String bDate = formatter.format(businessDate);
		
		ArrayList calendarCodes = null;
		//java.util.Date[] postingDates = null;

		try{	
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			stmt = conn.createStatement();
			/** ****** JTEST FIX end **************  on 14DEC09*/
			sBuf.append(" SELECT tmp.calendar, tmp.dat_start, tmp.cod_post_cycle, tmp.dat_lastposting, ");
			sBuf.append(" tmp.dat_lastpostingcalc, tmp.id_pool, tmp.cod_gl, ");
			sBuf.append(" CASE WHEN typ_post = 'DAILY' THEN 1 WHEN typ_post = 'WEEKLY' THEN 2 ");
			/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
			//ENH_IS686 For posting calendar frequency = 5
			//sBuf.append(" WHEN typ_post = 'MONTHLY' THEN 3 WHEN typ_post = 'YEARLY' THEN 4 ELSE 0 END typ_post, ");
			sBuf.append(" WHEN typ_post = 'MONTHLY' THEN 3 WHEN typ_post = 'YEARLY' THEN 4 WHEN typ_post = 'CALENDAR'THEN 5 ELSE 0 END typ_post, ");
			//ENH_IS686 Ends
			/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
			sBuf.append(" CASE WHEN opt_post IS NULL THEN 0 ELSE opt_post + 0 END opt_post, CASE WHEN unit1_post IS NULL ");
			sBuf.append(" THEN -1 ELSE unit1_post + 0 END unit1_post, CASE WHEN unit2_post IS NULL THEN -1 ELSE unit2_post + 0 ");
			sBuf.append(" END unit2_post, CASE WHEN cod_non_working_day = 'NEXT' THEN 'N' WHEN cod_non_working_day = 'PREV' THEN 'P' ");
			sBuf.append(" ELSE 'C' END isholiday, nbr_settle_days, nbr_delay_days_cr, nbr_delay_days_dr ");
			sBuf.append(" FROM orate_posting_cycle opc, ( SELECT (SELECT cod_calendar ");
			
			//ENH_LIQ_0005 Starts
			//sBuf.append(" FROM orbicash_glmst WHERE cod_gl = hdr.cod_gl) calendar, dat_start, ");
			sBuf.append(" FROM olm_glmst WHERE cod_gl = hdr.cod_gl) calendar, dat_start, ");
			//ENH_LIQ_0005 Ends
			
			sBuf.append(" cod_post_cycle, dat_lastposting, dat_lastpostingcalc, id_pool, ");
			sBuf.append(" cod_gl FROM opool_poolhdr hdr WHERE id_pool IN  (SELECT DISTINCT id_pool  ");
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			sBuf.append(" FROM opool_txnpoolhdr h WHERE h.nbr_processid = "+processId+")) tmp WHERE opc.cod_post_cycle = tmp.cod_post_cycle ");
			sBuf.append(" FROM opool_txnpoolhdr h WHERE h.nbr_processid = ?)) tmp WHERE opc.cod_post_cycle = tmp.cod_post_cycle ");
			/** ****** JTEST FIX end **************  on 14DEC09*/

			if(logger.isDebugEnabled()){
				logger.debug(" calculateNextCyclePostingDates -> "+sBuf.toString());
			}
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			rSet = stmt.executeQuery(sBuf.toString());
			
			pStmt = conn.prepareStatement(sBuf.toString());
			pStmt.setLong(1, processId);
			rSet = pStmt.executeQuery();
			/** ****** JTEST FIX end **************  on 14DEC09*/
			
			while(rSet.next()){
				String idPool = rSet.getString(6);				
			    String calendarCode = rSet.getString(1);
				Date poolStartDate = rSet.getDate(2);
				String frequency = rSet.getString(3);
				Date lastPostingDate = rSet.getDate(4);
				Date lastCalcPostingDate = rSet.getDate(5);
				String hostCode = rSet.getString(7);
				int typeFrequency = rSet.getInt(8);
				int frequencyInfo = rSet.getInt(9);
				int frequencyUnit1 = rSet.getInt(10);
				int frequencyUnit2 = rSet.getInt(11);
				String holidayFlag = rSet.getString(12);
				int nbr_settle_days = rSet.getInt(13);
				int nbr_delay_days_cr = rSet.getInt(14);
				int nbr_delay_days_dr = rSet.getInt(15);
				/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
				String Cod_postcalendar = rSet.getString(16);//ENH_IS686 changes made by satish for Posting Cycle added one field Calendar code for Posting Calendar 
				/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
				
				if(logger.isInfoEnabled()){
			        logger.info(" Adding information of pool ["+idPool+"] as [calendarCode] = ["+calendarCode+"]," +
			        " [poolStartDate] = ["+poolStartDate+"], [frequency] = ["+frequency+"], [lastPostingDate] = ["+lastPostingDate+"]" +
			        ", [lastCalcPostingDate] = ["+lastCalcPostingDate+"], [hostCode] = ["+hostCode+"]+" 
			        +"{ Frequency["+typeFrequency+"], FrequencyInfo["+frequencyInfo+"], "
					+"FrequencyUnit1["+frequencyUnit1+"], FrequencyUnit2["+frequencyUnit2+"], "
					+"HolidayFlag["+holidayFlag+"]nbr_settle_days["+nbr_settle_days+"], nbr_delay_days_cr["+nbr_delay_days_cr
				/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
				//ENH_IS686 ends
					//+"],nbr_delay_days_dr["+nbr_delay_days_dr+"]");
					+"],nbr_delay_days_dr["+nbr_delay_days_dr+"],Cod_postcalendar["+Cod_postcalendar
					+"]");
				//ENH_IS686 ends
				/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
			    }
				calendarCodes = new ArrayList();
				calendarCodes.add(calendarCode);
				if(lastPostingDate == null || lastCalcPostingDate == null){
				    callMode = "M";
				}
				// declaring a return parameter 
				java.util.Date[] nextActionDates = null;
				if(callMode.equals("E")){
				/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
				//ENH_IS686 changes made by satish for Posting Calendar added an argument Cod_postcalendar for Posting Calendar
					/*nextActionDates = 
						ExecutionUtility.getNextActionDate(calendarCodes, lastPostingDate, lastCalcPostingDate
										 , typeFrequency, frequencyInfo, frequencyUnit1, frequencyUnit2
										 , holidayFlag, businessDate, callMode);*/
					nextActionDates = 
						ExecutionUtility.getNextActionDate(calendarCodes, lastPostingDate, lastCalcPostingDate
										 , typeFrequency, frequencyInfo, frequencyUnit1, frequencyUnit2,Cod_postcalendar
										 , holidayFlag, businessDate, callMode);
				//ENH_IS686 ends
				/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
				}
				else{
				/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
				//ENH_IS686 changes made by satish for Posting Calendar added an argument Cod_postcalendar for Posting Calendar
					/*nextActionDates = 
						ExecutionUtility.getNextActionDate(calendarCodes, poolStartDate, businessDate
										 , typeFrequency, frequencyInfo, frequencyUnit1, frequencyUnit2
										 , holidayFlag, businessDate, callMode);*/
					nextActionDates = 
						ExecutionUtility.getNextActionDate(calendarCodes, poolStartDate, businessDate
										 , typeFrequency, frequencyInfo, frequencyUnit1, frequencyUnit2,Cod_postcalendar
										 , holidayFlag, businessDate, callMode);
				//ENH_IS686 ends
				/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
				}
				
				java.util.Date tmp_date = nextActionDates[0];
				nextActionDates = null;
				nextActionDates = new java.util.Date[5];
				nextActionDates[0] = tmp_date;
				tmp_date = null;
				
				nextActionDates[1] = nextActionDates[0];
				GregorianCalendar temp = new GregorianCalendar();
				temp.setTime(nextActionDates[0]);
				temp.add(Calendar.DATE, nbr_settle_days);
				nextActionDates[2] = temp.getTime();
				temp = null;
				
				nextActionDates[3] = 
					ExecutionUtility.getAnotherWorkingDay((String)calendarCodes.get(0), nextActionDates[0], 
											holidayFlag.charAt(0), 0, 0, nbr_settle_days + nbr_delay_days_cr);
				
				nextActionDates[4] = 
				    ExecutionUtility.getAnotherWorkingDay((String)calendarCodes.get(0), nextActionDates[0], 
											holidayFlag.charAt(0), 0, 0, nbr_settle_days + nbr_delay_days_dr);
				
				if (logger.isDebugEnabled()) {
                    logger.debug(" KEY:["+idPool+"] & VALUE:["+nextActionDates[0]+", "+nextActionDates[1]
                                 +","+nextActionDates[2]+","+nextActionDates[3]+" ,"+nextActionDates[4]+"] processed");                                 
                }
				postCache.put(idPool, nextActionDates);				
			}
		}
		catch(Exception e){
			logger.fatal("Exception in calculateNextCyclePostingDates ", e);
			  throw new LMSException("101302", e.toString());
		}
		finally{
			LMSConnectionUtility.close(rSet);
			/** ****** JTEST FIX start ************  on 14DEC09*/
//			LMSConnectionUtility.close(stmt);
			LMSConnectionUtility.close(pStmt);
			/** ****** JTEST FIX end **************  on 14DEC09*/
		}		
    }
	
	//SUP_1082a ends
	/*SCF_RET_ISS_701 - Execution Retrofit STARTS ~ JAYESH SHARMA */
	//ENH_1105 Starts
	
	/*
	 * If the accounting entry generation is not mandatory, the internal accounts for the BLE
	 * will not be maintained in the OPOOL_BLELINK. So these accounts PAYABLES, EXPENSES, RECEIVABLES,
	 * INCOME should be hard coded 
	 */ 
	private HashMap getInternalAccountBLE() throws LMSException{

		if(logger.isDebugEnabled()){
			logger.debug("Entering");
		}
		HashMap hmInternalAccts = new HashMap();
		try{
			//Put hard coded account in the hashmap for payable
			AccountingDO acc = new AccountingDO();
			acc.set_cod_bnk_lgl(ExecutionConstants.SYSTEM_BLE);
			acc.set_cod_ccy(ExecutionConstants.SYSTEM_CCY);
			acc.set_cod_gl(ExecutionConstants.SYSTEM_GL);
			acc.set_cod_branch(ExecutionConstants.SYSTEM_BRANCH);
			acc.set_nbr_coreaccount(ExecutionConstants.SYSTEM_PAYABLES);
			acc.set_nbr_ownaccount(ExecutionConstants.SYSTEM_PAYABLES);
			hmInternalAccts.put("PAYABLES",acc);
			
			acc = new AccountingDO();
			//Put hard coded account in the hashmap for Expenses
			acc.set_cod_bnk_lgl(ExecutionConstants.SYSTEM_BLE);
			acc.set_cod_ccy(ExecutionConstants.SYSTEM_CCY);
			acc.set_cod_gl(ExecutionConstants.SYSTEM_GL);
			acc.set_cod_branch(ExecutionConstants.SYSTEM_BRANCH);
			acc.set_nbr_coreaccount(ExecutionConstants.SYSTEM_EXPENSES);
			acc.set_nbr_ownaccount(ExecutionConstants.SYSTEM_EXPENSES);
			hmInternalAccts.put("EXPENSES",acc);
			
			acc = new AccountingDO();
			//Put hard coded account in the hashmap for Receivables
			acc.set_cod_bnk_lgl(ExecutionConstants.SYSTEM_BLE);
			acc.set_cod_ccy(ExecutionConstants.SYSTEM_CCY);
			acc.set_cod_gl(ExecutionConstants.SYSTEM_GL);
			acc.set_cod_branch(ExecutionConstants.SYSTEM_BRANCH);
			acc.set_nbr_coreaccount(ExecutionConstants.SYSTEM_RECEIVABLES);
			acc.set_nbr_ownaccount(ExecutionConstants.SYSTEM_RECEIVABLES);
			hmInternalAccts.put("RECEIVABLES",acc);
			
			acc = new AccountingDO();
			//Put hard coded account in the hashmap for Income
			acc.set_cod_bnk_lgl(ExecutionConstants.SYSTEM_BLE);
			acc.set_cod_ccy(ExecutionConstants.SYSTEM_CCY);
			acc.set_cod_gl(ExecutionConstants.SYSTEM_GL);
			acc.set_cod_branch(ExecutionConstants.SYSTEM_BRANCH);
			acc.set_nbr_coreaccount(ExecutionConstants.SYSTEM_INCOME);
			acc.set_nbr_ownaccount(ExecutionConstants.SYSTEM_INCOME);
			hmInternalAccts.put("INCOME",acc);

			if(logger.isDebugEnabled()){
				logger.debug("Leaving");
			}
		}
		catch(Exception ex)
		{
			logger.fatal("generateLegs: ", ex);
			throw new LMSException(LMSConstants.APPLICATION_ERROR_CODE, ex.getMessage());
		}
		return hmInternalAccts;
	}
	//ENH_1105 Ends
	/*SCF_RET_ISS_701 - Execution Retrofit ENDS ~ JAYESH SHARMA */
}
