/**
 * *    These materials are confidential and proprietary to Intellect Design Arena Ltd.
 *  and no part of these materials should be reproduced, published, transmitted or
 *  distributed  in any form or by any means, electronic, mechanical, photocopying,
 *  recording or otherwise, or stored in any information storage or retrieval system
 *  of any nature nor should the materials be disclosed to third parties or used in
 *  any other manner for which this is not authorized, without the prior express
 *  written authorization of Intellect Design Arena Ltd.
 *
 * <p>Title       : Intellect Liquidity</p>
 * <p>Description : This class geneartes the the Account Summary Information and populates
 * 					 OPOOL_ACCOUNTDTLS,OPOOL_ACCOUNTSUMMARY AND OPOOL_POSTINGSUMMARY table </p>
 * <p>Copyright   : Copyright © 2005 Intellect Design Arena Limited. All rights reserved.</p>
 * <p>Company     : Intellect Design Arena Limited</p>
 * <p>Date of Creation : </p>
 * <p>Source      : PostingInfoDO.java </p>
 * <p>Package     : com.intellectdesign.cash.accountingentry  </p>
 * @author 
 * @version 1.0
 * <p>------------------------------------------------------------------------------------</p>
 * <p>MODIFICATION HISTORY:</p>
 * <p>------------------------------------------------------------------------------------</p>
 * <p>SERIAL      AUTHOR		    DATE		SCF	DESCRIPTION</p>
 **<p>1   Rahul Yerande		05-Apr-2016 		LMS_45_PERF_CERTIFICATION_PF24				Performance Changes	</p>
 * <p>------------------------------------------------------------------------------------</p>
 */
package com.intellectdesign.cash.accountingentry;

import java.math.BigDecimal;


public class PostingInfoDO {
	
	BigDecimal ZERO = BigDecimal.ZERO;
	private  String PARTICIPANT_ID ;
	

	private  String COD_TXN ;
	// NEW_ACCOUNTING_ENTRY_FORMAT for Standalone and INCO will be NA
	private String SOURCE_POOL_ID = "NA";
	private String TYP_PARTICIPANT;
	private java.sql.Date DAT_POSTING_START;
    private  java.math.BigDecimal AMT_ACCRUEDNOTPOSTED = ZERO;
	private  java.math.BigDecimal AMT_POSTED_CENTRAL_CCY = ZERO;
	private  java.sql.Date OLD_DAT_FINAL_SETTLEMENT ;
	private  java.sql.Date OLD_DAT_POSTING ;
	private  java.math.BigDecimal AMT_POSTED = ZERO;
    private  String ID_POOL ;
	
	private String TYP_ENTITY;
    private String COD_ENTITY;
    private String TXT_STATUS;
	//Setter Getter Start

    public BigDecimal getZERO() {
		return ZERO;
	}
	public void setZERO(BigDecimal zERO) {
		ZERO = zERO;
	}
	public String getPARTICIPANT_ID() {
		return PARTICIPANT_ID;
	}
	public void setPARTICIPANT_ID(String pARTICIPANT_ID) {
		PARTICIPANT_ID = pARTICIPANT_ID;
	}
	public String getCOD_TXN() {
		return COD_TXN;
	}
	public void setCOD_TXN(String cOD_TXN) {
		COD_TXN = cOD_TXN;
	}
	public String getSOURCE_POOL_ID() {
		return SOURCE_POOL_ID;
	}
	public void setSOURCE_POOL_ID(String sOURCE_POOL_ID) {
		SOURCE_POOL_ID = sOURCE_POOL_ID;
	}
	public String getTYP_PARTICIPANT() {
		return TYP_PARTICIPANT;
	}
	public void setTYP_PARTICIPANT(String tYP_PARTICIPANT) {
		TYP_PARTICIPANT = tYP_PARTICIPANT;
	}
	public java.sql.Date getDAT_POSTING_START() {
		return DAT_POSTING_START;
	}
	public void setDAT_POSTING_START(java.sql.Date dAT_POSTING_START) {
		DAT_POSTING_START = dAT_POSTING_START;
	}
	public java.math.BigDecimal getAMT_ACCRUEDNOTPOSTED() {
		return AMT_ACCRUEDNOTPOSTED;
	}
	public void setAMT_ACCRUEDNOTPOSTED(java.math.BigDecimal aMT_ACCRUEDNOTPOSTED) {
		AMT_ACCRUEDNOTPOSTED = aMT_ACCRUEDNOTPOSTED;
	}
	public java.math.BigDecimal getAMT_POSTED_CENTRAL_CCY() {
		return AMT_POSTED_CENTRAL_CCY;
	}
	public void setAMT_POSTED_CENTRAL_CCY(
			java.math.BigDecimal aMT_POSTED_CENTRAL_CCY) {
		AMT_POSTED_CENTRAL_CCY = aMT_POSTED_CENTRAL_CCY;
	}
	public java.sql.Date getOLD_DAT_FINAL_SETTLEMENT() {
		return OLD_DAT_FINAL_SETTLEMENT;
	}
	public void setOLD_DAT_FINAL_SETTLEMENT(java.sql.Date oLD_DAT_FINAL_SETTLEMENT) {
		OLD_DAT_FINAL_SETTLEMENT = oLD_DAT_FINAL_SETTLEMENT;
	}
	public java.sql.Date getOLD_DAT_POSTING() {
		return OLD_DAT_POSTING;
	}
	public void setOLD_DAT_POSTING(java.sql.Date oLD_DAT_POSTING) {
		OLD_DAT_POSTING = oLD_DAT_POSTING;
	}
	public java.math.BigDecimal getAMT_POSTED() {
		return AMT_POSTED;
	}
	public void setAMT_POSTED(java.math.BigDecimal aMT_POSTED) {
		AMT_POSTED = aMT_POSTED;
	}
	public String getID_POOL() {
		return ID_POOL;
	}
	public void setID_POOL(String iD_POOL) {
		ID_POOL = iD_POOL;
	}
	  public String getTYP_ENTITY() {
			return TYP_ENTITY;
		}
		public void setTYP_ENTITY(String tYP_ENTITY) {
			TYP_ENTITY = tYP_ENTITY;
		}
		public String getCOD_ENTITY() {
			return COD_ENTITY;
		}
		public void setCOD_ENTITY(String cOD_ENTITY) {
			COD_ENTITY = cOD_ENTITY;
		}
		public String getTXT_STATUS() {
			return TXT_STATUS;
		}
		public void setTXT_STATUS(String tXT_STATUS) {
			TXT_STATUS = tXT_STATUS;
		}
	//Setter Getter End
	

	@Override
	public String toString() {
		return "PostingInfoDO [ZERO=" + ZERO + ", PARTICIPANT_ID="
				+ PARTICIPANT_ID + ", COD_TXN=" + COD_TXN + ", SOURCE_POOL_ID="
				+ SOURCE_POOL_ID + ", TYP_PARTICIPANT=" + TYP_PARTICIPANT
				+ ", DAT_POSTING_START=" + DAT_POSTING_START
				+ ", AMT_ACCRUEDNOTPOSTED=" + AMT_ACCRUEDNOTPOSTED
				+ ", AMT_POSTED_CENTRAL_CCY=" + AMT_POSTED_CENTRAL_CCY
				+ ", OLD_DAT_FINAL_SETTLEMENT=" + OLD_DAT_FINAL_SETTLEMENT
				+ ", OLD_DAT_POSTING=" + OLD_DAT_POSTING + ", AMT_POSTED="
				+ AMT_POSTED + ", ID_POOL=" + ID_POOL + ", TYP_ENTITY="
				+ TYP_ENTITY + ", COD_ENTITY=" + COD_ENTITY + ", TXT_STATUS="
				+ TXT_STATUS + "]";
	}
	public void clearObject()
	{
		ID_POOL = "";
		PARTICIPANT_ID  = ""; 
		DAT_POSTING_START = null;
		COD_TXN = "";
		AMT_ACCRUEDNOTPOSTED = ZERO;
		AMT_POSTED_CENTRAL_CCY = ZERO;
		OLD_DAT_FINAL_SETTLEMENT = null;
		OLD_DAT_POSTING = null;
		SOURCE_POOL_ID ="NA";
		TYP_PARTICIPANT =null;
		AMT_POSTED =ZERO;
		TYP_ENTITY =null;
		COD_ENTITY =null;
		TXT_STATUS =null;
	}
}
