/**
 * *    These materials are confidential and proprietary to Intellect Design Arena Ltd.
 *  and no part of these materials should be reproduced, published, transmitted or
 *  distributed in any form or by any means, electronic, mechanical, photocopying,
 *  recording or otherwise, or stored in any information storage or retrieval system
 *  of any nature nor should the materials be disclosed to third parties or used in
 *  any other manner for which this is not authorized, without the prior express
 *  written authorization of Intellect Design Arena Ltd.
 *
 * <p>Title       : Intellect Liquidity</p>
 * <p>Description : This class geneartes the the Account Summary Information and populates
 * 					 OPOOL_ACCOUNTDTLS,OPOOL_ACCOUNTSUMMARY AND OPOOL_POSTINGSUMMARY table </p>
 * <p>Copyright   : Copyright 2016 Intellect Design Arena Limited. All rights reserved.</p>
 * <p>Company     : Intellect Design Arena Limited</p>
 * <p>Date of Creation : </p>
 * <p>Source      : AccountSummaryGenerator.java </p>
 * <p>Package     : com.intellectdesign.cash.accountingentry  </p>
 * @author 
 * @version 1.0
 * <p>------------------------------------------------------------------------------------</p>
 * <p>MODIFICATION HISTORY:</p>
 * <p>------------------------------------------------------------------------------------</p>
 * <p>SERIAL      AUTHOR		    DATE		SCF	DESCRIPTION</p>
 * <p>------------------------------------------------------------------------------------</p>
 * <p>1           Devendra Desai    11/06/2007 CCB_ENH_ IS687 Initial Version.</p> 
 * <p>2           Rohit Mankar      22/09/2008 ENH_IS1297     Changes Made For INTEREST POSTING CYCLE</p> 
 * <p>3		  Gargi Kekre	    14/09/2009	ENH_2521   	Retro Activity of JPM LIQ_RELEASE9.10_IUT013_27_9.
 * <p>4       Sampada Ranaware        13/04/2011     		SIT issue fix starts - ID_PARENT_POOL added 	Do not generate accrual entry of subpools if cod_counter_account is 'S' at subproduct level.
 * <p>5   Prateek Aole			11/05/2012		 		        		JTest Fixes</p>
 * <p>5 	   Nitin W					10-Sep-2012		NEW_ACCOUNTING_ENTRY_FORMAT   New accounting format according the allocation source pool ID </p>
 * <p>6 	   Sandeep M				31-Dec-2013		Reallocate to Self  changes </p>
 * <p>9   		Sandeep M			21-Jan-2014		LS114_21326_21336 </p>  
 * <p>10 06/08/2015		Joshi Manish 			SUP_ISS_III_7305		Inter Company BVT CR</p>
 * <p>11   		Sandeep M(Retro By Deepak D)			09-May-2016		SUP_ISS_IV_7602 	Incorrect bvt calculation issue</p>  
 */
package com.intellectdesign.cash.accountingentry;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * @author devendra.desai
 *
 */
public class AccountInfoDO {
	BigDecimal ZERO = new BigDecimal(0);
	private  String ID_POOL ;
	private  String ID_PARENT_POOL; //SIT issue fix starts - ID_PARENT_POOL added
	private  String PARTICIPANT_ID ;
	private  java.sql.Date POOL_DAT_START ;
	private  java.sql.Date POOL_DAT_END ;
	private  java.sql.Date PARTY_DAT_END ;
	private  java.sql.Date DAT_BUSINESS ;
	private  String COD_TXN ;
	private  String FLG_REALPOSTING ;
	private  String FLG_REALPOOL ;
	private  String FLG_POOL_CONSOLIDATED ;
	private  String FLG_ACC_CONSOLIDATED ;
	private  java.sql.Date DAT_FINAL_SETTLEMENT;
	private  java.sql.Date OLD_DAT_FINAL_SETTLEMENT;
	private  java.math.BigDecimal DR_INT = ZERO ;
	private  java.math.BigDecimal CR_INT = ZERO;
	private  java.math.BigDecimal MINAMT = ZERO;
	private  java.sql.Date DAT_ADJUSTEDACCRUAL ;
	private  java.sql.Date OLD_DAT_ADJUSTEDACCRUAL ;
	private  java.sql.Date DAT_ACCRUAL ;
	private  java.sql.Date OLD_DAT_ACCRUAL ;
	private  java.sql.Date DAT_POSTING ;
	private  java.sql.Date OLD_DAT_POSTING ;
	private  java.sql.Date DAT_LASTPOSTING ;
	private  java.sql.Date SETTLEMENT_DATE ;
	private  java.math.BigDecimal AMT_CALCULATED = ZERO;
	private  java.math.BigDecimal AMT_DIFF = ZERO;
	private  java.sql.Date DAT_VALUE;
	private  java.math.BigDecimal AMT_UNACCRUED = ZERO;
	private  java.math.BigDecimal AMT_ACCRUED = ZERO;
	private  java.math.BigDecimal AMT_ACCRUEDNOTPOSTED = ZERO;
	private  java.math.BigDecimal AMT_POSTED = ZERO;
	private String TXT_STATUS;
	private String COD_COUNTER_ACCOUNT;
	private java.math.BigDecimal AMT_TO_BE_POSTED = ZERO;
	private java.sql.Date DAT_CURRENT_BUSINESS;
	private String COD_CCY;
	private String FLG_SAMEVALDAT;
	private java.sql.Date DAT_POSTING_START;
	private java.sql.Date DAT_ACCRUAL_START;
	private java.math.BigDecimal AMT_CAL_SUM;
	private java.sql.Date DAT_LASTACCRUAL;
	private String FLG_ALLOCATION;
	private java.sql.Date DAT_POST_CREDITDELAY;
	private java.sql.Date DAT_POST_DEBITDELAY;
	private java.sql.Date DAT_POST_SETTLEMENT;//SUP_ISS_IV_7602
	private String TYP_PARTICIPANT;
	
	//ENH_IS1297 - INTEREST POSTING CYCLE Starts
	private String FLG_POSTCYCLE;
	//ENH_IS1297 - INTEREST POSTING CYCLE Ends
	
	// NEW_ACCOUNTING_ENTRY_FORMAT for Standalone and INCO will be NA
	private String SOURCE_POOL_ID = "NA";
	//Added For Calculating Settlement Date Starts
	private String TYP_INTPOSTOPTION;
	//Added For Calculating Settlement Date Ends
	
	//ENH_10.3_154 Starts
	private java.sql.Date DAT_INITIAL_POSTING;
	private String FLG_BACKVALUE_POSTING;
	//ENH_10.3_154 Ends	
	
	//Sandeep fix for ned to add interest type
	private String TYP_INTEREST;
	//ENH_IS1297 - INTEREST POSTING CYCLE Starts
	private long SimId;
	//Reallocate to Self start
	private  java.math.BigDecimal AMT_CAL_CENTRAL_CCY = ZERO;
	private  java.math.BigDecimal RATE_EXRATE_CENTRAL_CCY = ZERO;
	//Interest and Advantage Seperate Settlement Dates For JPMC
	private String accrualPricingInterfaceFeed = null;
	private  java.math.BigDecimal AMT_POSTED_CENTRAL_CCY = ZERO;
	private  java.math.BigDecimal AMT_CALCULATED_CENTRAL_CCY = ZERO;
	private java.math.BigDecimal AMT_CAL_SUM_CENTRAL_CCY = ZERO; // LS114_21326_21336
	//Reallocate to Self end
	//added by BOI issue 145
	private String TYP_REPAYMENT;
	//SUP_ISS_III_7305 starts
	private  java.sql.Date DAT_INITIAL_VALUE;
	
	/**
	 * @return the dAT_INITIAL_VALUE
	 */
	public java.sql.Date getDAT_INITIAL_VALUE() {
		return DAT_INITIAL_VALUE;
	}

	/**
	 * @param dAT_INITIAL_VALUE the dAT_INITIAL_VALUE to set
	 */
	public void setDAT_INITIAL_VALUE(java.sql.Date dAT_INITIAL_VALUE) {
		DAT_INITIAL_VALUE = dAT_INITIAL_VALUE;
	}
	//SUP_ISS_III_7305 ends
	public String getTYP_REPAYMENT() {
		return TYP_REPAYMENT;
	}
	
	public void setTYP_REPAYMENT(String typ_repayment) {
		TYP_REPAYMENT = typ_repayment;
	}
	//end by BOI isue 145
	public String getAccrualPricingInterfaceFeed() {
		return accrualPricingInterfaceFeed;
	}
	
	public void setAccrualPricingInterfaceFeed(String accrualPricingIntFeed) {
		accrualPricingInterfaceFeed = accrualPricingIntFeed;
	}
	//Interest and Advantage Seperate Settlement Dates For JPMC
	public String getFLG_POSTCYCLE() {
		return FLG_POSTCYCLE;
	}
	
	public void setFLG_POSTCYCLE(String flg_postcycle) {
		FLG_POSTCYCLE = flg_postcycle;
	}
	//ENH_IS1297 - INTEREST POSTING CYCLE Ends
	// NEW_ACCOUNTING_ENTRY_FORMAT Start
	/**
	 * @return the sOURCE_POOL_ID
	 */
	public String getSOURCE_POOL_ID() {
		return SOURCE_POOL_ID;
	}

	/**
	 * @param source_pool_id the sOURCE_POOL_ID to set
	 */
	public void setSOURCE_POOL_ID(String source_pool_id) {
		SOURCE_POOL_ID = source_pool_id;
	}
	
	// NEW_ACCOUNTING_ENTRY_FORMAT end


	public String getTYP_PARTICIPANT() {
		return TYP_PARTICIPANT;
	}

	public void setTYP_PARTICIPANT(String typ_participant) {
		TYP_PARTICIPANT = typ_participant;
	}

	public String getFLG_ALLOCATION() {
		return FLG_ALLOCATION;
	}

	public void setFLG_ALLOCATION(String flg_allocation) {
		FLG_ALLOCATION = flg_allocation;
	}

	public java.sql.Date getDAT_LASTACCRUAL() {
		return DAT_LASTACCRUAL;
	}

	public void setDAT_LASTACCRUAL(java.sql.Date dat_lastaccrual) {
		DAT_LASTACCRUAL = dat_lastaccrual;
	}

	public java.math.BigDecimal getAMT_CAL_SUM() {
		return AMT_CAL_SUM;
	}

	public void setAMT_CAL_SUM(java.math.BigDecimal amt_cal_sum) {
		AMT_CAL_SUM = amt_cal_sum;
	}
	//Reallocate to Self start
	public java.math.BigDecimal getAMT_CAL_SUM_CENTRAL_CCY() {
		return AMT_CAL_SUM_CENTRAL_CCY;
	}

	public void setAMT_CAL_SUM_CENTRAL_CCY(java.math.BigDecimal amt_cal_sum_central_ccy) {
		AMT_CAL_SUM_CENTRAL_CCY = amt_cal_sum_central_ccy;
	}
	//Reallocate to Self end
	//Added For Calculating Settlement Date Starts
	public String getTYP_INTPOSTOPTION() {
		return TYP_INTPOSTOPTION;
	}

	public void setTYP_INTPOSTOPTION(String typ_intpostoption) {
		TYP_INTPOSTOPTION = typ_intpostoption;
	}
	//Added For Calculating Settlement Date Ends
	public long getSimId() {
		return SimId;
	}

	public void setSimId(long simId) {
		SimId = simId;
	}
	public String  toString()
	{
		StringBuilder s=new StringBuilder(0); //JTest Fixes
		String newLine = (char) 13 + "" + (char) 10;
		s.append(newLine);
		s.append("ID_POOL = "+ ID_POOL);
		s.append(newLine);
		//SIT issue fix starts - ID_PARENT_POOL added
		s.append("ID_PARENT_POOL = "+ ID_PARENT_POOL);
		s.append(newLine);
		s.append("PARTICIPANT_ID  = "+ PARTICIPANT_ID );
		s.append(newLine);
		s.append("POOL_DAT_START = "+ (POOL_DAT_START != null ? POOL_DAT_START.toString():"" ));
		s.append(newLine);
		s.append("PARTY_DAT_END = "+(PARTY_DAT_END != null ?PARTY_DAT_END.toString() : ""));
		s.append(newLine);
		s.append("DAT_BUSINESS = "+ (DAT_BUSINESS != null ? DAT_BUSINESS.toString() :" "));
		s.append(newLine);
		s.append("COD_TXN = "+ COD_TXN);
		s.append(newLine);
		s.append("FLG_REALPOSTING = "+ FLG_REALPOSTING);
		s.append(newLine);
		s.append("FLG_REALPOOL = "+ FLG_REALPOOL);
		s.append(newLine);
		s.append("SOURCE_POOL_ID = "+ SOURCE_POOL_ID);  // NEW_ACCOUNTING_ENTRY_FORMAT
		s.append(newLine);
		s.append("FLG_POOLCONSOLIDATED = "+ FLG_POOL_CONSOLIDATED);
		s.append(newLine);
		s.append("FLG_ACC_CONSOLIDATED = "+ FLG_ACC_CONSOLIDATED);
		s.append(newLine);
		s.append("DAT_FINAL_SETTLEMENT = "+ (DAT_FINAL_SETTLEMENT != null ? DAT_FINAL_SETTLEMENT.toString() : ""));
		s.append(newLine);
		s.append("DR_INT = "+ DR_INT);
		s.append(newLine);
		s.append("CR_INT = "+ CR_INT);
		s.append(newLine);
		s.append("MINAMT = "+ MINAMT);
		s.append(newLine);
		s.append("DAT_ADJUSTEDACCRUAL = "+ (DAT_ADJUSTEDACCRUAL != null ? DAT_ADJUSTEDACCRUAL.toString() : ""));
		s.append(newLine);
		s.append("DAT_ACCRUAL = "+ (DAT_ACCRUAL != null ? DAT_ACCRUAL.toString(): ""));
		s.append(newLine);
		s.append("DAT_POSTING = "+ (DAT_POSTING != null ? DAT_POSTING.toString() : " "));
		s.append(newLine);
		s.append("DAT_LASTPOSTING = "+ (DAT_LASTPOSTING != null ? DAT_LASTPOSTING.toString() : ""));
		s.append(newLine);
		s.append("SETTLEMENT_DATE = "+ (SETTLEMENT_DATE != null ? SETTLEMENT_DATE.toString(): ""));
		s.append(newLine);
		s.append("AMT_CALCULATED = "+ AMT_CALCULATED);
		s.append(newLine);
		s.append("AMT_DIFF = "+ AMT_DIFF );
		s.append(newLine);
		s.append("DAT_VALUE = "+ (DAT_VALUE != null ? DAT_VALUE.toString(): ""));
		s.append(newLine);
		s.append("AMT_UNACCRUED = "+ AMT_UNACCRUED );
		s.append(newLine);
		s.append("AMT_ACCRUED = "+ AMT_ACCRUED);
		s.append(newLine);
		s.append("AMT_ACCRUEDNOTPOSTED = "+ AMT_ACCRUEDNOTPOSTED);
		s.append(newLine);
		s.append("AMT_POSTED = "+ AMT_POSTED);
		s.append(newLine);
		s.append("TXT_STATUS = "+ TXT_STATUS);
		s.append(newLine);
		s.append("COD_COUNTER_ACCOUNT = "+ COD_COUNTER_ACCOUNT);
		s.append(newLine);
		s.append("AMT_TO_BE_POSTED = "+ AMT_TO_BE_POSTED);
		s.append(newLine);
		s.append("DAT_CURRENT_BUSINESS = "+ (DAT_CURRENT_BUSINESS != null ? DAT_CURRENT_BUSINESS.toString() : ""));
		s.append(newLine);
		s.append("COD_CCY = "+ COD_CCY);
		s.append(newLine);
		s.append("FLG_SAMEVALDAT = "+ FLG_SAMEVALDAT);
		s.append(newLine);
		s.append("DAT_POSTING_START = "+ (DAT_POSTING_START != null ? DAT_POSTING_START.toString() : ""));
		s.append(newLine);
		s.append("DAT_ACCRUAL_START = "+ (DAT_ACCRUAL_START != null ? DAT_ACCRUAL_START.toString() : ""));
		s.append(newLine);
		s.append("AMT_CAL_SUM = "+ AMT_CAL_SUM);
		s.append(newLine);//OLD_DAT_FINAL_SETTLEMENT
		s.append("DAT_LASTACCRUAL = "+ (DAT_LASTACCRUAL != null ? DAT_LASTACCRUAL.toString() : ""));
		s.append(newLine);
		s.append("OLD_DAT_FINAL_SETTLEMENT = "+ (OLD_DAT_FINAL_SETTLEMENT != null ? OLD_DAT_FINAL_SETTLEMENT.toString() : ""));
		s.append(newLine);
		s.append("OLD_DAT_ACCRUAL = "+ (OLD_DAT_ACCRUAL != null ? OLD_DAT_ACCRUAL.toString() : ""));
		s.append(newLine);
		s.append("OLD_DAT_POSTING = "+ (OLD_DAT_POSTING != null ? OLD_DAT_POSTING.toString() : ""));
		s.append(newLine);
		s.append("OLD_DAT_ADJUSTEDACCRUAL = "+ (OLD_DAT_ADJUSTEDACCRUAL != null ? OLD_DAT_ADJUSTEDACCRUAL.toString() : ""));
		s.append(newLine);
		s.append("FLG_ALLOCATION = "+ FLG_ALLOCATION);
		s.append(newLine);
		s.append("DAT_POST_SETTLEMENT = "+ (DAT_POST_SETTLEMENT != null ? DAT_POST_SETTLEMENT.toString() : ""));//SUP_ISS_IV_7602
		s.append(newLine);//SUP_ISS_IV_7602
		s.append("DAT_POST_DEBITDELAY = "+ (DAT_POST_DEBITDELAY != null ? DAT_POST_DEBITDELAY.toString() : ""));
		s.append(newLine);
		s.append("DAT_POST_CREDITDELAY = "+ (DAT_POST_CREDITDELAY != null ? DAT_POST_CREDITDELAY.toString() : ""));
		s.append(newLine);
		s.append(newLine);
		s.append("TYP_PARTICIPANT = "+ TYP_PARTICIPANT);
		s.append(newLine);
		s.append("SOURCE_POOL_ID = "+ SOURCE_POOL_ID);
		s.append(newLine);
		//ENH_10.3_154 Starts
		s.append("DAT_INITIAL_POSTING = "+ (DAT_INITIAL_POSTING != null ? DAT_INITIAL_POSTING.toString() : ""));
		s.append(newLine);
		s.append("FLG_BACKVALUE_POSTING = "+ (FLG_BACKVALUE_POSTING != null ? FLG_BACKVALUE_POSTING.toString() : ""));
		s.append(newLine);
		s.append("SimId = " + SimId);
		//Reallocate to Self start
		s.append(newLine);
		s.append("AMT_CAL_CENTRAL_CCY = " + AMT_CAL_CENTRAL_CCY);
		s.append(newLine);
		s.append("AMT_POSTED_CENTRAL_CCY = " + AMT_POSTED_CENTRAL_CCY);
		s.append(newLine);
		s.append("AMT_CALCULATED_CENTRAL_CCY = "+ AMT_CALCULATED_CENTRAL_CCY);
		s.append(newLine);
		s.append("AMT_CAL_SUM_CENTRAL_CCY = "+ AMT_CAL_SUM_CENTRAL_CCY);
		s.append(newLine);
		s.append("RATE_EXRATE_CENTRAL_CCY = "+ RATE_EXRATE_CENTRAL_CCY);
		//Reallocate to Self end
		//ENH_10.3_154 Ends
		return s.toString();
	}
	
	public void clearObject()
	{
		ID_POOL = "";
		ID_PARENT_POOL = ""; //SIT issue fix starts - ID_PARENT_POOL added
		PARTICIPANT_ID  = ""; 
		POOL_DAT_START = null;
		PARTY_DAT_END = null;
		DAT_BUSINESS = null;
		COD_TXN = "";
		FLG_REALPOSTING = "";	
		FLG_REALPOOL = "";
		FLG_POOL_CONSOLIDATED = "";  
		FLG_ACC_CONSOLIDATED = "";
		DAT_FINAL_SETTLEMENT = null;
		DR_INT = ZERO;
		CR_INT = ZERO;
		MINAMT = ZERO;
		DAT_ADJUSTEDACCRUAL = null;
		DAT_ACCRUAL = null;
		DAT_POSTING = null;
		DAT_LASTPOSTING = null;
		SETTLEMENT_DATE = null;
		AMT_CALCULATED = ZERO;
		AMT_DIFF = ZERO;
		DAT_VALUE = null;
		AMT_UNACCRUED = ZERO; 
		AMT_ACCRUED = ZERO;
		AMT_ACCRUEDNOTPOSTED = ZERO;
		AMT_POSTED = ZERO;
		TXT_STATUS = "";
		COD_COUNTER_ACCOUNT ="";
		AMT_TO_BE_POSTED = ZERO;
		DAT_CURRENT_BUSINESS = null;
		COD_CCY = "";
		FLG_SAMEVALDAT = "";
		DAT_POSTING_START = null;
		DAT_ACCRUAL_START = null;
		AMT_CAL_SUM = ZERO;
		DAT_LASTACCRUAL = null;
		OLD_DAT_FINAL_SETTLEMENT= null;
		OLD_DAT_ACCRUAL = null;
		OLD_DAT_POSTING = null;
		OLD_DAT_ADJUSTEDACCRUAL = null;
		FLG_ALLOCATION = null;
		DAT_POST_CREDITDELAY = null;
		DAT_POST_DEBITDELAY = null;
		DAT_POST_SETTLEMENT = null;//SUP_ISS_IV_7602
		//ENH_10.3_154 Starts
		DAT_INITIAL_POSTING = null;
		FLG_BACKVALUE_POSTING = null;
		//ENH_10.3_154 Ends
		//Reallocate to Self start
		AMT_CAL_CENTRAL_CCY = ZERO;
		RATE_EXRATE_CENTRAL_CCY = ZERO;
		AMT_POSTED_CENTRAL_CCY = ZERO;
		AMT_CALCULATED_CENTRAL_CCY = ZERO;
		AMT_CAL_SUM_CENTRAL_CCY = ZERO;
		//Reallocate to Self end
	}
	public java.math.BigDecimal getAMT_TO_BE_POSTED() {
		return AMT_TO_BE_POSTED;
	}
	public void setAMT_TO_BE_POSTED(java.math.BigDecimal amt_to_be_posted) {
		AMT_TO_BE_POSTED = amt_to_be_posted;
	}
	public String getTXT_STATUS() {
		return TXT_STATUS;
	}
	public void setTXT_STATUS(String txt_status) {
		TXT_STATUS = txt_status;
	}
	public java.math.BigDecimal getCR_INT() {
		return CR_INT;
	}
	public void setCR_INT(java.math.BigDecimal cr_int) {
		CR_INT = cr_int;
	}
	public  String getCOD_TXN() {
		return COD_TXN;
	}
	public  void setCOD_TXN(String cod_txn) {
		COD_TXN = cod_txn;
	}
	public  Date getDAT_ACCRUAL() {
		return DAT_ACCRUAL;
	}
	public  void setDAT_ACCRUAL(Date dat_accrual) {
		DAT_ACCRUAL = dat_accrual;
	}
	public  Date getDAT_ADJUSTEDACCRUAL() {
		return DAT_ADJUSTEDACCRUAL;
	}
	public  void setDAT_ADJUSTEDACCRUAL(Date dat_adjustedaccrual) {
		DAT_ADJUSTEDACCRUAL = dat_adjustedaccrual;
	}
	public  Date getDAT_BUSINESS() {
		return DAT_BUSINESS;
	}
	public  void setDAT_BUSINESS(Date dat_business) {
		DAT_BUSINESS = dat_business;
	}
	public  Date getDAT_LASTPOSTING() {
		return DAT_LASTPOSTING;
	}
	public  void setDAT_LASTPOSTING(Date dat_lastposting) {
		DAT_LASTPOSTING = dat_lastposting;
	}
	public  Date getDAT_POSTING() {
		return DAT_POSTING;
	}
	public  void setDAT_POSTING(Date dat_posting) {
		DAT_POSTING = dat_posting;
	}
	public  String getFLG_ACC_CONSOLIDATED() {
		return FLG_ACC_CONSOLIDATED;
	}
	public  void setFLG_ACC_CONSOLIDATED(String flg_acc_consolidated) {
		FLG_ACC_CONSOLIDATED = flg_acc_consolidated;
	}
	public  String getFLG_POOL_CONSOLIDATED() {
		return FLG_POOL_CONSOLIDATED;
	}
	public  void setFLG_POOL_CONSOLIDATED(String flg_pool_consolidated) {
		FLG_POOL_CONSOLIDATED = flg_pool_consolidated;
	}
	public  String getFLG_REALPOOL() {
		return FLG_REALPOOL;
	}
	public  void setFLG_REALPOOL(String flg_realpool) {
		FLG_REALPOOL = flg_realpool;
	}
	public  String getFLG_REALPOSTING() {
		return FLG_REALPOSTING;
	}
	public  void setFLG_REALPOSTING(String flg_realposting) {
		FLG_REALPOSTING = flg_realposting;
	}
	public  String getID_POOL() {
		return ID_POOL;
	}
	public  void setID_POOL(String id_pool) {
		ID_POOL = id_pool;
	}
	public  java.math.BigDecimal getMINAMT() {
		return MINAMT;
	}
	public  void setMINAMT(java.math.BigDecimal minamt) {
		MINAMT = minamt;
	}
	public  String getPARTICIPANT_ID() {
		return PARTICIPANT_ID;
	}
	public  void setPARTICIPANT_ID(String participant_id) {
		PARTICIPANT_ID = participant_id;
	}
	public  Date getPARTY_DAT_END() {
		return PARTY_DAT_END;
	}
	public  void setPARTY_DAT_END(Date party_dat_end) {
		PARTY_DAT_END = party_dat_end;
	}
	public  Date getPOOL_DAT_END() {
		return POOL_DAT_END;
	}
	public  void setPOOL_DAT_END(Date pool_dat_end) {
		POOL_DAT_END = pool_dat_end;
	}
	public  Date getPOOL_DAT_START() {
		return POOL_DAT_START;
	}
	public  void setPOOL_DAT_START(Date pool_dat_start) {
		POOL_DAT_START = pool_dat_start;
	}
	public  Date getSETTLEMENT_DATE() {
		return SETTLEMENT_DATE;
	}

	public  void setSETTLEMENT_DATE(Date settlement_date) {
		SETTLEMENT_DATE = settlement_date;
	}
	public java.math.BigDecimal getDR_INT() {
		return DR_INT;
	}
	public void setDR_INT(java.math.BigDecimal dr_int) {
		DR_INT = dr_int;
	}
	public Date getDAT_FINAL_SETTLEMENT() {
		return DAT_FINAL_SETTLEMENT;
	}
	public void setDAT_FINAL_SETTLEMENT(Date dat_final_settlement) {
		DAT_FINAL_SETTLEMENT = dat_final_settlement;
	}
	
	 
	public java.math.BigDecimal getAMT_CALCULATED() {
		return AMT_CALCULATED;
	}
	public void setAMT_CALCULATED(java.math.BigDecimal amt_calculated) {
		AMT_CALCULATED = amt_calculated;
	}
	//Reallocate to Self start
	public java.math.BigDecimal getAMT_CALCULATED_CENTRAL_CCY() {
		return AMT_CALCULATED_CENTRAL_CCY;
	}
	public void setAMT_CALCULATED_CENTRAL_CCY(java.math.BigDecimal amt_calculated_central_ccy) {
		AMT_CALCULATED_CENTRAL_CCY = amt_calculated_central_ccy;
	}
	//Reallocate to Self end
	public java.math.BigDecimal getAMT_DIFF() {
		return AMT_DIFF;
	}
	public void setAMT_DIFF(java.math.BigDecimal amt_diff) {
		AMT_DIFF = amt_diff;
	}
	public Date getDat_value() {
		return DAT_VALUE;
	}
	public void setDat_value(Date dat_value) {
		DAT_VALUE = dat_value;
	}
	public java.math.BigDecimal getAMT_ACCRUED() {
		return AMT_ACCRUED;
	}
	public void setAMT_ACCRUED(java.math.BigDecimal amt_accrued) {
		AMT_ACCRUED = amt_accrued;
	}
	public java.math.BigDecimal getAMT_POSTED() {
		return AMT_POSTED;
	}
	public void setAMT_POSTED(java.math.BigDecimal amt_posted) {
		AMT_POSTED = amt_posted;
	}
	public java.math.BigDecimal getAMT_UNACCRUED() {
		return AMT_UNACCRUED;
	}
	public void setAMT_UNACCRUED(java.math.BigDecimal amt_unaccrued) {
		AMT_UNACCRUED = amt_unaccrued;
	}
	
	public java.math.BigDecimal getAMT_ACCRUEDNOTPOSTED() {
		return AMT_ACCRUEDNOTPOSTED;
	}
	public void setAMT_ACCRUEDNOTPOSTED(java.math.BigDecimal amt_accruednotposted) {
		AMT_ACCRUEDNOTPOSTED = amt_accruednotposted;
	}
	public java.sql.Date getDAT_VALUE() {
		return DAT_VALUE;
	}
	public void setDAT_VALUE(java.sql.Date dat_value) {
		DAT_VALUE = dat_value;
	}
	public String getCOD_COUNTER_ACCOUNT() {
		return COD_COUNTER_ACCOUNT;
	}
	public void setCOD_COUNTER_ACCOUNT(String cod_counter_account) {
		COD_COUNTER_ACCOUNT = cod_counter_account;
	}

	public java.sql.Date getDAT_CURRENT_BUSINESS() {
		return DAT_CURRENT_BUSINESS;
	}

	public void setDAT_CURRENT_BUSINESS(java.sql.Date dat_current_business) {
		DAT_CURRENT_BUSINESS = dat_current_business;
	}

	public String getCOD_CCY() {
		return COD_CCY;
	}

	public void setCOD_CCY(String cod_ccy) {
		COD_CCY = cod_ccy;
	}

	public String getFLG_SAMEVALDAT() {
		return FLG_SAMEVALDAT;
	}

	public void setFLG_SAMEVALDAT(String flg_samevaldat) {
		FLG_SAMEVALDAT = flg_samevaldat;
	}

	public java.sql.Date getDAT_ACCRUAL_START() {
		return DAT_ACCRUAL_START;
	}

	public void setDAT_ACCRUAL_START(java.sql.Date dat_accrual_start) {
		DAT_ACCRUAL_START = dat_accrual_start;
	}

	public java.sql.Date getDAT_POSTING_START() {
		return DAT_POSTING_START;
	}

	public void setDAT_POSTING_START(java.sql.Date dat_posting_start) {
		DAT_POSTING_START = dat_posting_start;
	}

	public java.sql.Date getOLD_DAT_FINAL_SETTLEMENT() {
		return OLD_DAT_FINAL_SETTLEMENT;
	}

	public void setOLD_DAT_FINAL_SETTLEMENT(java.sql.Date old_dat_final_settlement) {
		OLD_DAT_FINAL_SETTLEMENT = old_dat_final_settlement;
	}

	public java.sql.Date getOLD_DAT_ACCRUAL() {
		return OLD_DAT_ACCRUAL;
	}

	public void setOLD_DAT_ACCRUAL(java.sql.Date old_dat_accrual) {
		OLD_DAT_ACCRUAL = old_dat_accrual;
	}

	public java.sql.Date getOLD_DAT_POSTING() {
		return OLD_DAT_POSTING;
	}

	public void setOLD_DAT_POSTING(java.sql.Date old_dat_posting) {
		OLD_DAT_POSTING = old_dat_posting;
	}

	public java.sql.Date getOLD_DAT_ADJUSTEDACCRUAL() {
		return OLD_DAT_ADJUSTEDACCRUAL;
	}

	public void setOLD_DAT_ADJUSTEDACCRUAL(java.sql.Date old_dat_adjustedaccrual) {
		OLD_DAT_ADJUSTEDACCRUAL = old_dat_adjustedaccrual;
	}

	public java.sql.Date getDAT_POST_CREDITDELAY() {
		return DAT_POST_CREDITDELAY;
	}

	public void setDAT_POST_CREDITDELAY(java.sql.Date dat_post_creditdelay) {
		DAT_POST_CREDITDELAY = dat_post_creditdelay;
	}

	public java.sql.Date getDAT_POST_DEBITDELAY() {
		return DAT_POST_DEBITDELAY;
	}

	public void setDAT_POST_DEBITDELAY(java.sql.Date dat_post_debitdelay) {
		DAT_POST_DEBITDELAY = dat_post_debitdelay;
	}

	//ENH_10.3_154 Starts
	public java.sql.Date getDAT_INITIAL_POSTING() {
		return DAT_INITIAL_POSTING;
	}

	public void setDAT_INITIAL_POSTING(java.sql.Date dat_initial_posting) {
		DAT_INITIAL_POSTING = dat_initial_posting;
	}

	public String getFLG_BACKVALUE_POSTING() {
		return FLG_BACKVALUE_POSTING;
	}

	public void setFLG_BACKVALUE_POSTING(String flg_backvalue_posting) {
		FLG_BACKVALUE_POSTING = flg_backvalue_posting;
	}
	//ENH_10.3_154 Ends

	//SIT issue fix starts - txn.ID_PARENT_POOL added
	public String getID_PARENT_POOL() {
		return ID_PARENT_POOL;
	}

	public void setID_PARENT_POOL(String id_parent_pool) {
		ID_PARENT_POOL = id_parent_pool;
	}


	public void setTYP_INTEREST(String typ_interest) {
		// TODO Auto-generated method stub
		this.TYP_INTEREST = typ_interest;
	}

	public String getTYP_INTEREST() {
		// TODO Auto-generated method stub
		return TYP_INTEREST;
	}
//Reallocate to Self  start
	/**
	 * @return the aMT_CAL_CENTRAL_CCY
	 */
	public java.math.BigDecimal getAMT_CAL_CENTRAL_CCY() {
		return AMT_CAL_CENTRAL_CCY;
	}

	/**
	 * @param aMT_CAL_CENTRAL_CCY the aMT_CAL_CENTRAL_CCY to set
	 */
	public void setAMT_CAL_CENTRAL_CCY(java.math.BigDecimal aMT_CAL_CENTRAL_CCY) {
		AMT_CAL_CENTRAL_CCY = aMT_CAL_CENTRAL_CCY;
	}
	
	 
	
	/**
	 * @return the rATE_EXRATE_CENTRAL_CCY
	 */
	public java.math.BigDecimal getRATE_EXRATE_CENTRAL_CCY() {
		return RATE_EXRATE_CENTRAL_CCY;
	}

	/**
	 * @param rATE_EXRATE_CENTRAL_CCY the rATE_EXRATE_CENTRAL_CCY to set
	 */
	public void setRATE_EXRATE_CENTRAL_CCY(
			java.math.BigDecimal rATE_EXRATE_CENTRAL_CCY) {
		RATE_EXRATE_CENTRAL_CCY = rATE_EXRATE_CENTRAL_CCY;
	}

	/**
	 * @return the aMT_POSTED_CENTRAL_CCY
	 */
	public java.math.BigDecimal getAMT_POSTED_CENTRAL_CCY() {
		return AMT_POSTED_CENTRAL_CCY;
	}

	/**
	 * @param aMT_POSTED_CENTRAL_CCY the aMT_POSTED_CENTRAL_CCY to set
	 */
	public void setAMT_POSTED_CENTRAL_CCY(
			java.math.BigDecimal aAMT_POSTED_CENTRAL_CCY) {
		AMT_POSTED_CENTRAL_CCY = aAMT_POSTED_CENTRAL_CCY;
	}
//Reallocate to Self end
	//SIT issue fix ends
	//SUP_ISS_IV_7602 starts
	/**
	 * @return the dAT_POST_SETTLEMENT
	 */
	public java.sql.Date getDAT_POST_SETTLEMENT() {
		return DAT_POST_SETTLEMENT;
	}

	/**
	 * @param dAT_POST_SETTLEMENT the dAT_POST_SETTLEMENT to set
	 */
	public void setDAT_POST_SETTLEMENT(java.sql.Date dAT_POST_SETTLEMENT) {
		DAT_POST_SETTLEMENT = dAT_POST_SETTLEMENT;
	}
	//SUP_ISS_IV_7602 ends
	
	

}
