/**
 * *    These materials are confidential and proprietary to Intellect Design Arena Ltd.
 *  and no part of these materials should be reproduced, published, transmitted or
 *  distributed  in any form or by any means, electronic, mechanical, photocopying,
 *  recording or otherwise, or stored in any information storage or retrieval system
 *  of any nature nor should the materials be disclosed to third parties or used in
 *  any other manner for which this is not authorized, without the prior express
 *  written authorization of Intellect Design Arena Ltd.
 *
 * <p>Title       : Intellect Liquidity</p>
 * <p>Description : This class geneartes the the Account Summary Information and populates
 * 					 OPOOL_ACCOUNTDTLS,OPOOL_ACCOUNTSUMMARY AND OPOOL_POSTINGSUMMARY table </p>
 * <p>Copyright   : Copyright © 2005 Intellect Design Arena Limited. All rights reserved.</p>
 * <p>Company     : Intellect Design Arena Limited</p>
 * <p>Date of Creation : </p>
 * <p>Source      : AccountDtlsInfoDO.java </p>
 * <p>Package     : com.intellectdesign.cash.accountingentry  </p>
 * @author 
 * @version 1.0
 * <p>------------------------------------------------------------------------------------</p>
 * <p>MODIFICATION HISTORY:</p>
 * <p>------------------------------------------------------------------------------------</p>
 * <p>SERIAL      AUTHOR		    DATE		SCF	DESCRIPTION</p>
 * <p>------------------------------------------------------------------------------------</p>
*<p>1   Rahul Yerande		05-Apr-2016 		LMS_45_PERF_CERTIFICATION_PF24				Performance Changes	</p>

 */
package com.intellectdesign.cash.accountingentry;

import java.math.BigDecimal;


public class AccountDtlsInfoDO {
	
	BigDecimal ZERO = BigDecimal.ZERO;
	
	private  String COD_TXN ;
	// NEW_ACCOUNTING_ENTRY_FORMAT for Standalone and INCO will be NA
	private String SOURCE_POOL_ID = "NA";
	private String TYP_PARTICIPANT;
	private java.sql.Date DAT_BUSINESS;
	private  java.sql.Date DAT_CURRENT_BUSINESS ;
    private  String ID_POOL ;
	private  java.math.BigDecimal AMT_CALCULATED = ZERO;

	
	//Setter Getter
	private  String PARTICIPANT_ID ;
	public BigDecimal getZERO() {
		return ZERO;
	}
	public void setZERO(BigDecimal zERO) {
		ZERO = zERO;
	}
	public String getPARTICIPANT_ID() {
		return PARTICIPANT_ID;
	}
	public void setPARTICIPANT_ID(String pARTICIPANT_ID) {
		PARTICIPANT_ID = pARTICIPANT_ID;
	}
	public String getCOD_TXN() {
		return COD_TXN;
	}
	public void setCOD_TXN(String cOD_TXN) {
		COD_TXN = cOD_TXN;
	}
	public String getSOURCE_POOL_ID() {
		return SOURCE_POOL_ID;
	}
	public void setSOURCE_POOL_ID(String sOURCE_POOL_ID) {
		SOURCE_POOL_ID = sOURCE_POOL_ID;
	}
	public String getTYP_PARTICIPANT() {
		return TYP_PARTICIPANT;
	}
	public void setTYP_PARTICIPANT(String tYP_PARTICIPANT) {
		TYP_PARTICIPANT = tYP_PARTICIPANT;
	}
	public java.sql.Date getDAT_BUSINESS() {
		return DAT_BUSINESS;
	}
	public void setDAT_BUSINESS(java.sql.Date dAT_BUSINESS) {
		DAT_BUSINESS = dAT_BUSINESS;
	}
	public java.sql.Date getDAT_CURRENT_BUSINESS() {
		return DAT_CURRENT_BUSINESS;
	}
	public void setDAT_CURRENT_BUSINESS(java.sql.Date dAT_CURRENT_BUSINESS) {
		DAT_CURRENT_BUSINESS = dAT_CURRENT_BUSINESS;
	}
	public String getID_POOL() {
		return ID_POOL;
	}
	public void setID_POOL(String iD_POOL) {
		ID_POOL = iD_POOL;
	}
	public java.math.BigDecimal getAMT_CALCULATED() {
		return AMT_CALCULATED;
	}
	public void setAMT_CALCULATED(java.math.BigDecimal aMT_CALCULATED) {
		AMT_CALCULATED = aMT_CALCULATED;
	}
	//Setter Getter
	@Override
	public String toString() {
		return "AccountDtlsInfoDO [ZERO=" + ZERO + ", COD_TXN=" + COD_TXN
				+ ", SOURCE_POOL_ID=" + SOURCE_POOL_ID + ", TYP_PARTICIPANT="
				+ TYP_PARTICIPANT + ", DAT_BUSINESS=" + DAT_BUSINESS
				+ ", DAT_CURRENT_BUSINESS=" + DAT_CURRENT_BUSINESS
				+ ", ID_POOL=" + ID_POOL + ", AMT_CALCULATED=" + AMT_CALCULATED
				+ ", PARTICIPANT_ID=" + PARTICIPANT_ID + "]";
	}

	public void clearObject()
	{
		ID_POOL = "";
		PARTICIPANT_ID  = ""; 
		DAT_BUSINESS = null;
		COD_TXN = "";
		AMT_CALCULATED = ZERO;
		DAT_CURRENT_BUSINESS = null;
		SOURCE_POOL_ID ="NA";
		TYP_PARTICIPANT =null;
	}
	
	

}
