/**
 * *    These materials are confidential and proprietary to Intellect Design Arena Ltd.
 *  and no part of these materials should be reproduced, published, transmitted or
 *  distributed  in any form or by any means, electronic, mechanical, photocopying,
 *  recording or otherwise, or stored in any information storage or retrieval system
 *  of any nature nor should the materials be disclosed to third parties or used in
 *  any other manner for which this is not authorized, without the prior express
 *  written authorization of Intellect Design Arena Ltd.
 *
 * <p>Title       : Intellect Liquidity</p>
 * <p>Description : This class geneartes the the Account Summary Information and populates
 * 					 OPOOL_ACCOUNTDTLS,OPOOL_ACCOUNTSUMMARY AND OPOOL_POSTINGSUMMARY table </p>
 * <p>Copyright   : Copyright © 2005 Intellect Design Arena Limited. All rights reserved.</p>
 * <p>Company     : Intellect Design Arena Limited</p>
 * <p>Date of Creation : </p>
 * <p>Source      : AccuralInfoDO.java </p>
 * <p>Package     : com.intellectdesign.cash.accountingentry  </p>
 * @author 
 * @version 1.0
 * <p>------------------------------------------------------------------------------------</p>
 * <p>MODIFICATION HISTORY:</p>
 * <p>------------------------------------------------------------------------------------</p>
 * <p>SERIAL      AUTHOR		    DATE		SCF	DESCRIPTION</p>
 * *<p>1   Rahul Yerande		05-Apr-2016 		LMS_45_PERF_CERTIFICATION_PF24				Performance Changes	</p>
 * <p>------------------------------------------------------------------------------------</p>
 */
package com.intellectdesign.cash.accountingentry;

import java.math.BigDecimal;


public class AccrualInfoDO {
	
	BigDecimal ZERO = BigDecimal.ZERO;
	private  String PARTICIPANT_ID ;
	private  String COD_TXN ;
	// NEW_ACCOUNTING_ENTRY_FORMAT for Standalone and INCO will be NA
	private String SOURCE_POOL_ID = "NA";
	private String TYP_PARTICIPANT;
	private java.sql.Date DAT_ACCRUAL_START;
	private  java.math.BigDecimal AMT_UNACCRUED = ZERO;
	private  java.sql.Date OLD_DAT_ACCRUAL ;
	private  java.sql.Date OLD_DAT_ADJUSTEDACCRUAL ;
	private  java.math.BigDecimal AMT_ACCRUED = ZERO;
    private  String ID_POOL ;
	
    private String TYP_ENTITY;
    private String COD_ENTITY;
    private String TXT_STATUS;
	
	//Setters Getters Start
    
	public String getTYP_ENTITY() {
		return TYP_ENTITY;
	}
	public void setTYP_ENTITY(String tYP_ENTITY) {
		TYP_ENTITY = tYP_ENTITY;
	}
	public String getCOD_ENTITY() {
		return COD_ENTITY;
	}
	public void setCOD_ENTITY(String cOD_ENTITY) {
		COD_ENTITY = cOD_ENTITY;
	}
	public String getTXT_STATUS() {
		return TXT_STATUS;
	}
	public void setTXT_STATUS(String tXT_STATUS) {
		TXT_STATUS = tXT_STATUS;
	}
	public String getID_POOL() {
		return ID_POOL;
	}
	public void setID_POOL(String iD_POOL) {
		ID_POOL = iD_POOL;
	}
	public String getPARTICIPANT_ID() {
		return PARTICIPANT_ID;
	}
	public void setPARTICIPANT_ID(String pARTICIPANT_ID) {
		PARTICIPANT_ID = pARTICIPANT_ID;
	}
	public String getCOD_TXN() {
		return COD_TXN;
	}
	public void setCOD_TXN(String cOD_TXN) {
		COD_TXN = cOD_TXN;
	}
	public String getSOURCE_POOL_ID() {
		return SOURCE_POOL_ID;
	}
	public void setSOURCE_POOL_ID(String sOURCE_POOL_ID) {
		SOURCE_POOL_ID = sOURCE_POOL_ID;
	}
	public String getTYP_PARTICIPANT() {
		return TYP_PARTICIPANT;
	}
	public void setTYP_PARTICIPANT(String tYP_PARTICIPANT) {
		TYP_PARTICIPANT = tYP_PARTICIPANT;
	}
	public java.sql.Date getDAT_ACCRUAL_START() {
		return DAT_ACCRUAL_START;
	}
	public void setDAT_ACCRUAL_START(java.sql.Date dAT_ACCRUAL_START) {
		DAT_ACCRUAL_START = dAT_ACCRUAL_START;
	}
	public java.math.BigDecimal getAMT_UNACCRUED() {
		return AMT_UNACCRUED;
	}
	public void setAMT_UNACCRUED(java.math.BigDecimal aMT_UNACCRUED) {
		AMT_UNACCRUED = aMT_UNACCRUED;
	}
	public java.sql.Date getOLD_DAT_ACCRUAL() {
		return OLD_DAT_ACCRUAL;
	}
	public void setOLD_DAT_ACCRUAL(java.sql.Date oLD_DAT_ACCRUAL) {
		OLD_DAT_ACCRUAL = oLD_DAT_ACCRUAL;
	}
	public java.sql.Date getOLD_DAT_ADJUSTEDACCRUAL() {
		return OLD_DAT_ADJUSTEDACCRUAL;
	}
	public void setOLD_DAT_ADJUSTEDACCRUAL(java.sql.Date oLD_DAT_ADJUSTEDACCRUAL) {
		OLD_DAT_ADJUSTEDACCRUAL = oLD_DAT_ADJUSTEDACCRUAL;
	}
	public java.math.BigDecimal getAMT_ACCRUED() {
		return AMT_ACCRUED;
	}
	public void setAMT_ACCRUED(java.math.BigDecimal aMT_ACCRUED) {
		AMT_ACCRUED = aMT_ACCRUED;
	}
	
	//Setters Getters End

	@Override
	public String toString() {
		return "AccrualInfoDO [ZERO=" + ZERO + ", PARTICIPANT_ID="
				+ PARTICIPANT_ID + ", COD_TXN=" + COD_TXN + ", SOURCE_POOL_ID="
				+ SOURCE_POOL_ID + ", TYP_PARTICIPANT=" + TYP_PARTICIPANT
				+ ", DAT_ACCRUAL_START=" + DAT_ACCRUAL_START
				+ ", AMT_UNACCRUED=" + AMT_UNACCRUED + ", OLD_DAT_ACCRUAL="
				+ OLD_DAT_ACCRUAL + ", OLD_DAT_ADJUSTEDACCRUAL="
				+ OLD_DAT_ADJUSTEDACCRUAL + ", AMT_ACCRUED=" + AMT_ACCRUED
				+ ", ID_POOL=" + ID_POOL + ", TYP_ENTITY=" + TYP_ENTITY
				+ ", COD_ENTITY=" + COD_ENTITY + ", TXT_STATUS=" + TXT_STATUS
				+ "]";
	}
	
	
	
	public void clearObject()
	{
		ID_POOL = "";
		PARTICIPANT_ID  = ""; 
		DAT_ACCRUAL_START = null;
		COD_TXN = "";
		AMT_UNACCRUED = ZERO;
		OLD_DAT_ACCRUAL = null;
		OLD_DAT_ADJUSTEDACCRUAL = null;
		AMT_ACCRUED = ZERO;
		SOURCE_POOL_ID ="NA";
		TYP_PARTICIPANT =null;
		TYP_ENTITY =null;
		COD_ENTITY =null;
		TXT_STATUS =null;
	}
	
	

}
