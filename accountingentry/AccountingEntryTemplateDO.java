/**
 *  Copyright  2010   Intellect Design Arena Ltd. All rights reserved.
 *
 *  These materials are confidential and proprietary to Intellect Design Arena Ltd.
 *  and no part of these materials should be reproduced, published, transmitted or
 *  distributed in any form or by any means, electronic, mechanical, photocopying,
 *  recording or otherwise, or stored in any information storage or retrieval system
 *  of any nature nor should the materials be disclosed to third parties or used in
 *  any other manner for which this is not authorized, without the prior express
 *  written authorization of Intellect Design Arena Ltd.
 *
 * <p>Title       					: Intellect Liquidity</p>
 * <p>Description 					: This class is the processor for Balance Tracking.</p>
 * <p>SCF NO      					: </p>
 * <p>Copyright   					: Copyright 2016 Intellect Design Arena Limited. All rights reserved.</p>
 * <p>Company     					: Intellect Design Arena Limited</p>
 * <p>Date of Creation 					: 12-May-2010</p>
 * <p>Source      					: AccountingEntryExplode.java </p>
 * <p>Package     					: com.intellectdesign.cash.accountingentry</p>
 * 
 * @author Jitendra Behera
 * @version 1.0
 * 
 * <p>--------------------------------------------------------------------------------------</p>
 * <p>MODIFICATION HISTORY:</p>
 * <p>--------------------------------------------------------------------------------------</p>
 * <p>SERIAL	AUTHOR				DATE					SCF				DESCRIPTION		</p>
 * <p>--------------------------------------------------------------------------------------</p>
 * <p>1        	Jitendra Behera 	       19/06/2010  		Initial Version.	</p>
 * <p>2        	Jitendra Behera 	       04/10/2010  		BILATERALSETTLEMENT 	Changes Added for BI-Lateral Settlement.</p>
 * <p>3         Ravikumar Maladi           04/01/2011       Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005.</p>
 * <p>4         Sandeep M.           		01/02/2011       JTest changes on 01-feb-11.</p>
 * <p>5   Prateek Aole			11/05/2012		 		        		JTest Fixes</p>
 **/

package com.intellectdesign.cash.accountingentry;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Date;

import com.intellectdesign.cash.lms.constants.LMSConstants;
import com.intellectdesign.cash.lms.execution.bvp.entity.BVPProcessContext;
import com.intellectdesign.cash.lms.interco.dataobject.PositionDO;
import com.intellectdesign.exception.LMSException;

/**
 * 
 * @author jitendra.behera
 *
 */
public class AccountingEntryTemplateDO implements Cloneable{
    
    String nbrTemplateId;
    String nbrContractNo;
    String nbrSeqNo;
    String flgDrcrSet;
    long nbrLegid;
    String nbrOwnAccount;
    String nbrCoreAccount;
    String codBic;
    String codBnkLglEnt;
    String codBranch;
    String codCcy;
    String codGl;
    String codCountry;
    String flgAcctType;
    String flgDrcr;
    String flgSrcTgtTyp;
    String txtIbanAcct;
    String flgSettlementEntry;
    String nbrSettlementDays;
    Date dateEffective;
    Date dateEnd;
    String flgPersist;
    String codTriggeringGl;
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
    String flgSettlementPath;//BILATERALSETTLEMENT
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
    
    AccountingEntryTemplateDO(){
	nbrTemplateId = "";
	nbrContractNo = "";;
	nbrSeqNo = "";;
	flgDrcrSet = "";;
	nbrLegid = 0;
	nbrOwnAccount = "";;
	nbrCoreAccount = "";;
	codBic = "";;
	codBnkLglEnt = "";;
	codBranch = "";;
	codCcy = "";;
	codGl = "";;
	codCountry = "";;
	flgAcctType = "";;
	flgDrcr = "";;
	flgSrcTgtTyp = "";;
	txtIbanAcct = "";;
	flgSettlementEntry = "";
	nbrSettlementDays = "";
	//dateEffective = new Date();
	dateEffective = null;
	dateEnd = null;
	flgPersist = "";    
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
	flgSettlementPath = "";//BILATERALSETTLEMENT
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
    }
    
    public String toString(){
	StringBuilder sbf = new StringBuilder("");
	sbf.append("nbrTemplateId : "+nbrTemplateId);
	sbf.append(" :: nbrContractNo : "+nbrContractNo);
	sbf.append(" :: nbrSeqNo : "+nbrSeqNo);
	sbf.append(" :: flgDrcrSet : "+flgDrcrSet);
	sbf.append(" :: nbrLegid : "+nbrLegid);
	sbf.append(" :: nbrOwnAccount : "+nbrOwnAccount);
	sbf.append(" :: nbrCoreAccount : "+nbrCoreAccount);
	sbf.append(" :: codBic : "+codBic);
	sbf.append(" :: codBnkLglEnt : "+codBnkLglEnt);
	sbf.append(" :: codBranch : "+codBranch);
	sbf.append(" :: codCcy : "+codCcy);
	sbf.append(" :: codGl : "+codGl);
	sbf.append(" :: codCountry : "+codCountry);
	sbf.append(" :: flgAcctType : "+flgAcctType);
	sbf.append(" :: flgDrcr : "+flgDrcr);
	sbf.append(" :: flgSrcTgtTyp : "+flgSrcTgtTyp);
	sbf.append(" :: txtIbanAcct : "+txtIbanAcct);
	sbf.append(" :: flgSettlementEntry : "+flgSettlementEntry);
	sbf.append(" :: nbrSettlementDays : "+nbrSettlementDays);
	sbf.append(" :: dateEffective : "+dateEffective);
	sbf.append(" :: dateEnd : "+dateEnd);
	sbf.append(" :: flgPersist : "+flgPersist);
	sbf.append(" :: codTriggeringGl : "+codTriggeringGl);
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
	sbf.append(" :: flgSettlementPath : "+flgSettlementPath);//BILATERALSETTLEMENT
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
	
	return sbf.toString();

    }

    /**
     * @return the codBic
     */
    public String getCodBic() {
        return codBic;
    }

    /**
     * @param codBic the codBic to set
     */
    public void setCodBic(String codBic) {
        this.codBic = codBic;
    }

    /**
     * @return the codBnkLglEnt
     */
    public String getCodBnkLglEnt() {
        return codBnkLglEnt;
    }

    /**
     * @param codBnkLglEnt the codBnkLglEnt to set
     */
    public void setCodBnkLglEnt(String codBnkLglEnt) {
        this.codBnkLglEnt = codBnkLglEnt;
    }

    /**
     * @return the codBranch
     */
    public String getCodBranch() {
        return codBranch;
    }

    /**
     * @param codBranch the codBranch to set
     */
    public void setCodBranch(String codBranch) {
        this.codBranch = codBranch;
    }

    /**
     * @return the codCcy
     */
    public String getCodCcy() {
        return codCcy;
    }

    /**
     * @param codCcy the codCcy to set
     */
    public void setCodCcy(String codCcy) {
        this.codCcy = codCcy;
    }

    /**
     * @return the codCountry
     */
    public String getCodCountry() {
        return codCountry;
    }

    /**
     * @param codCountry the codCountry to set
     */
    public void setCodCountry(String codCountry) {
        this.codCountry = codCountry;
    }

    /**
     * @return the codGl
     */
    public String getCodGl() {
        return codGl;
    }

    /**
     * @param codGl the codGl to set
     */
    public void setCodGl(String codGl) {
        this.codGl = codGl;
    }

    /**
     * @return the dateEffective
     */
    public Date getDateEffective() {
        return dateEffective;
    }

    /**
     * @param dateEffective the dateEffective to set
     */
    public void setDateEffective(Date dateEffective) {
        this.dateEffective = dateEffective;
    }

    /**
     * @return the dateEnd
     */
    public Date getDateEnd() {
        return dateEnd;
    }

    /**
     * @param dateEnd the dateEnd to set
     */
    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    /**
     * @return the flgAcctType
     */
    public String getFlgAcctType() {
        return flgAcctType;
    }

    /**
     * @param flgAcctType the flgAcctType to set
     */
    public void setFlgAcctType(String flgAcctType) {
        this.flgAcctType = flgAcctType;
    }

    /**
     * @return the flgDrcr
     */
    public String getFlgDrcr() {
        return flgDrcr;
    }

    /**
     * @param flgDrcr the flgDrcr to set
     */
    public void setFlgDrcr(String flgDrcr) {
        this.flgDrcr = flgDrcr;
    }

    /**
     * @return the flgDrcrSet
     */
    public String getFlgDrcrSet() {
        return flgDrcrSet;
    }

    /**
     * @param flgDrcrSet the flgDrcrSet to set
     */
    public void setFlgDrcrSet(String flgDrcrSet) {
        this.flgDrcrSet = flgDrcrSet;
    }

    /**
     * @return the flgPersist
     */
    public String getFlgPersist() {
        return flgPersist;
    }

    /**
     * @param flgPersist the flgPersist to set
     */
    public void setFlgPersist(String flgPersist) {
        this.flgPersist = flgPersist;
    }

    /**
     * @return the flgSettlementEntry
     */
    public String getFlgSettlementEntry() {
        return flgSettlementEntry;
    }

    /**
     * @param flgSettlementEntry the flgSettlementEntry to set
     */
    public void setFlgSettlementEntry(String flgSettlementEntry) {
        this.flgSettlementEntry = flgSettlementEntry;
    }

    /**
     * @return the flgSrcTgtTyp
     */
    public String getFlgSrcTgtTyp() {
        return flgSrcTgtTyp;
    }

    /**
     * @param flgSrcTgtTyp the flgSrcTgtTyp to set
     */
    public void setFlgSrcTgtTyp(String flgSrcTgtTyp) {
        this.flgSrcTgtTyp = flgSrcTgtTyp;
    }

    /**
     * @return the nbrContractNo
     */
    public String getNbrContractNo() {
        return nbrContractNo;
    }

    /**
     * @param nbrContractNo the nbrContractNo to set
     */
    public void setNbrContractNo(String nbrContractNo) {
        this.nbrContractNo = nbrContractNo;
    }

    /**
     * @return the nbrCoreAccount
     */
    public String getNbrCoreAccount() {
        return nbrCoreAccount;
    }

    /**
     * @param nbrCoreAccount the nbrCoreAccount to set
     */
    public void setNbrCoreAccount(String nbrCoreAccount) {
        this.nbrCoreAccount = nbrCoreAccount;
    }

    /**
     * @return the nbrLegid
     */
    public long getNbrLegid() {
        return nbrLegid;
    }

    /**
     * @param nbrLegid the nbrLegid to set
     */
    public void setNbrLegid(long nbrLegid) {
        this.nbrLegid = nbrLegid;
    }

    /**
     * @return the nbrOwnAccount
     */
    public String getNbrOwnAccount() {
        return nbrOwnAccount;
    }

    /**
     * @param nbrOwnAccount the nbrOwnAccount to set
     */
    public void setNbrOwnAccount(String nbrOwnAccount) {
        this.nbrOwnAccount = nbrOwnAccount;
    }

    /**
     * @return the nbrSeqNo
     */
    public String getNbrSeqNo() {
        return nbrSeqNo;
    }

    /**
     * @param nbrSeqNo the nbrSeqNo to set
     */
    public void setNbrSeqNo(String nbrSeqNo) {
        this.nbrSeqNo = nbrSeqNo;
    }

    /**
     * @return the nbrSettlementDays
     */
    public String getNbrSettlementDays() {
        return nbrSettlementDays;
    }

    /**
     * @param nbrSettlementDays the nbrSettlementDays to set
     */
    public void setNbrSettlementDays(String nbrSettlementDays) {
        this.nbrSettlementDays = nbrSettlementDays;
    }

    /**
     * @return the nbrTemplateId
     */
    public String getNbrTemplateId() {
        return nbrTemplateId;
    }

    /**
     * @param nbrTemplateId the nbrTemplateId to set
     */
    public void setNbrTemplateId(String nbrTemplateId) {
        this.nbrTemplateId = nbrTemplateId;
    }

    /**
     * @return the txtIbanAcct
     */
    public String getTxtIbanAcct() {
        return txtIbanAcct;
    }

    /**
     * @param txtIbanAcct the txtIbanAcct to set
     */
    public void setTxtIbanAcct(String txtIbanAcct) {
        this.txtIbanAcct = txtIbanAcct;
    }

    public Object clone() throws CloneNotSupportedException{
	AccountingEntryTemplateDO accountingEntryTemplateDO = null;
        try {
            accountingEntryTemplateDO = (AccountingEntryTemplateDO) super.clone();
        } catch (CloneNotSupportedException e) {
            throw e;
        } 
        return accountingEntryTemplateDO;
    }

    /**
     * @return the codTriggeringGl
     */
    public String getCodTriggeringGl() {
        return codTriggeringGl;
    }

    /**
     * @param codTriggeringGl the codTriggeringGl to set
     */
    public void setCodTriggeringGl(String codTriggeringGl) {
        this.codTriggeringGl = codTriggeringGl;
    }

//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Starts
    /**
     * BILATERALSETTLEMENT
     * @return the flgSettlementPath
     */
    public String getFlgSettlementPath() {
        return flgSettlementPath;
    }

    /**
     * BILATERALSETTLEMENT
     * @param flgSettlementPath the flgSettlementPath to set
     */
    public void setFlgSettlementPath(String flgSettlementPath) {
        this.flgSettlementPath = flgSettlementPath;
    }
//Retro From LIQ_RELEASE10.2_SIT013 to LIQ_RELEASE10.4_SIT005 Ends
}
